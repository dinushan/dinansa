<link href="<?php echo base_url() ?>skin_admin/global/plugins/jquery.event.drag-2.2/style.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery.event.drag-2.2/jquery.event.drag-2.2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery.event.drag-2.2/jquery.event.drag.live-2.2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery.event.drag-2.2/jquery.event.drop-2.2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery.event.drag-2.2/jquery.event.drop.live-2.2.js" type="text/javascript"></script>

<!-- BEGIN CONTAINER -->

<div class="page-container">
    <?php $this->load->view('counter/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <?php if ($page_action == ''): ?>
                <h3 class="page-title">Counter bookings</h3>

                <div class="page-bar">
                    <div class="page-toolbar">
                        <div class="btn-group pull-right">
                            <?php if(
                            $this->counterauth->has_role('201')
                            ):?>
                                <a onclick="setLocation('<?php echo site_url('counters/' . $slug . '/add') ?>')" class="btn green"><i
                                        class="fa fa-plus-circle"></i> Add New</a>
                            <?php endif; ?>
                        </div>
                    </div>
                </div>




            <?php elseif ($page_action == 'add' || $page_action == 'edit'): ?>
                <h3 class="page-title"><?php echo $page_action == "add" ? 'Add New' : 'Edit' ?> Booking</h3>
                <div class="page-bar">
                    <div class="page-toolbar">
                        <div class="btn-group pull-right">
                            <a class="btn default" href="<?php echo site_url('counters/' . $slug . '/') ?>"><i
                                    class="fa fa-chevron-left"></i> Back</a>
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-12"> <?php echo get_message() ?>
                        <!-- BEGIN VALIDATION STATES-->
                        <div class="portlet light bordered">

                            <div class="portlet-body form">
                                <!-- BEGIN FORM-->
                                <?php
                                $attr = array("id" => "edit_form", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                                echo form_open(current_url(), $attr);
                                ?>


                                <h4 class="form-section">General</h4>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Total booked<span class="req"></span></label>

                                    <div class="control-element col-md-4">

                                        <input readonly name="total_seats" type="text" id=""
                                               class="form-control required-entry"
                                               value="<?php echo $seat_count;?>"/>
                                        <?php echo form_error('total'); ?>
                                    </div>
                                </div>

                                <?php foreach($price_object as $price_obj):?>
                                <div class="form-group">
                                    <label class="control-label col-md-3"><?php echo $price_obj->category_name;?><span class="req"></span></label>

                                    <div class="control-element col-md-4">
                                        <input  name="category_id[]" type="hidden" id="category_id"
                                                class="form-control required-entry"
                                                value="<?php echo set_value('category_id[]', $price_obj->category_id) ?>"/>

                                        <input  name="price[]" type="hidden" id="price"
                                                class="form-control required-entry price"
                                                value="<?php echo set_value('price[]', $price_obj->price) ?>"/>

                                        <input  name="category_seat_no[]" type="text" id="category_seat_no"
                                                class="form-control required-entry category_seat_no"
                                                value=""/>

                                        <input readonly name="category_total[]" type="text" id="category_total"
                                                class="form-control required-entry category_total"
                                                value=""/>
                                        <?php echo form_error('category_seat_no[]'); ?>
                                    </div>
                                </div>

                                <?php endforeach;?>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Total<span class="req"></span></label>

                                    <div class="control-element col-md-4">

                                        <input readonly name="total" type="text" id="total"
                                               class="form-control required-entry"
                                               value=""/>
                                        <?php echo form_error('total'); ?>
                                    </div>
                                </div>

                                <?php if (
                                    $page_action == 'add'
                                    || ($page_action == 'edit' && $this->counterauth->has_role('201') && $status == 0)
                                    || ($page_action == 'edit' && $this->counterauth->has_role('202'))
                                ): ?>
                                    <div class="form-actions">
                                        <div class="row">
                                            <div class="col-md-offset-3 col-md-9">
                                                <div class="btn-group">
                                                    <input type="hidden" id="hidden-action" name="hidden-action" value="1">
                                                    <button type="submit" data-id="1" class="btn btn-submit green">Save Booking
                                                    </button>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                <?php endif; ?>




                                <script type="text/javascript">
                                    var total_seats='<?php echo $seat_count;?>';


                                    $(".category_seat_no").on('keyup', function(e) {
                                        var no=$(this).val().trim();
                                        //alert(no);
                                        var price=$(this).prev('.price').val().trim();
                                        $(this).next('.category_total').val(no*price);
                                        calculate_total();
                                        return false;
                                    });

                                    function calculate_total() {
                                        var total=0;
                                        $('.category_total').each(function(){
                                            total += Number($(this).val());
                                        })
                                        $('#total').val(total);
                                    }

                                </script>
                                </form>



















                            </div>
                        </div>
                    </div>
                </div>



            <?php endif;//$page_action ?>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->