<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE') OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE') OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE') OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ') OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE') OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE') OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE') OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE') OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT') OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT') OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/

defined('EXIT_SUCCESS') OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR') OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG') OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE') OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS') OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT') OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE') OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN') OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX') OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('SYSTEM_NAME', 'Elevate');
define('CURRENCY_CODE', 'LKR');
define('CURRENCY_SYMBOL', 'Rs.');
define('YESNO', serialize(array('No', 'Yes')));

define('PROFILE_IMG_PATH', 'assets/images/user/');
define('PROFILE_IMG_W', 125);
define('PROFILE_IMG_H', 125);

define('SUCCESS_MSG_ADD', 'Record has been saved.');
define('SUCCESS_MSG_UPDATE', 'Record has been updated.');
define('SUCCESS_MSG_DELETED', 'Record has been deleted.');
define('INVALID_PARAM', 'Invalid Parameter(s).');
define('LIST_PER_PAGE_ADMIN', 20); // for admin grid paging

define('titles', serialize(array(
    'Mr.',
    'Mrs.',
    'Ms.'
)));

define('user_types', serialize(array(
    1 => 'Hotel Manager',
    2 => 'Accountant',
    3 => 'Front Desk Clerk',
    4 => 'Head Chef',
    5 => 'Chef',
    6 => 'Food Runner',
    7 => 'Kitchen Manager',
    8 => 'Kitchen Team Member',
    9 => 'Restaurant Manager',
    10 => 'Waiter',
    11 => 'Membership Manager',
    12 => 'Marketing Agent'
)));

define('setting_types', serialize(array(
    'PERCENTAGE' => 'Percentage',
    'INT' => 'Integer',
    'DECIMAL' => 'Decimal',
    'BOOLEAN' => 'Boolean',
    'TEXT' => 'TEXT'
)));

define('locations', serialize([
    '' => 'Select Option',
    '1' => 'Japanese Kitchen',
    '2' => 'Sri lankan Kitchen',
    '3' => 'Cocktail Table'
]));

define('BANKCARDS', serialize([
    'ALL' => 'All Card',
    'VISA' => 'VISA',
    'MASTER' => 'MASTER',
    'AMEX' => 'AMEX'
]));


define('UOM', serialize([

    1 => ['UNIT' => 'mg', 'UMO' => 'Milligram', 'RELATIONS' => []],
    2 => ['UNIT' => 'g', 'UMO' => 'GRAMS', 'RELATIONS' => [
        ['index' => 1, 'factor' => 1000]
    ]],

    3 => ['UNIT' => 'kg', 'UMO' => 'Kilogram', 'RELATIONS' => [
        ['index' => 1, 'factor' => 1000000],
        ['index' => 2, 'factor' => 1000]
    ]],


    4 => ['UNIT' => 'ml', 'UMO' => 'Milliliter', 'RELATIONS' => []],
    5 => ['UNIT' => 'l', 'UMO' => 'Liter', 'RELATIONS' => [
        ['index' => 4, 'factor' => 1000]
    ]],

    6 => ['UNIT' => 'QTY', 'UMO' => 'Quantity', 'RELATIONS' => []]


]));

define('WEEKDAYS', serialize([
    '0' => 'Sunday',
    '1' => 'Monday',
    '2' => 'Tuesday',
    '3' => 'Wednesday',
    '4' => 'Thursday',
    '5' => 'Friday',
    '6' => 'Saturday',
]));

define('PROMOTION_STATUS', serialize([
    'ACTIVE' => 'Active',
    'INACTIVE' => 'Inactive',
    'PENDING' => 'Pending',
]));

define('requisition_status', serialize([
    '0' => 'Pending',
    '1' => 'Completed',
]));

define('onloan_status', serialize([
    '0' => 'Awaiting',
    '1' => 'Items Returned',
]));

define('order_status', serialize([
    '0' => 'Available',
    '1' => 'Reserved',
    '2' => 'On Going',
    '3' => 'Completed'
]));

define('item_status', serialize([
    '0' => 'Void',
    '1' => 'Pending',
    '2' => 'Processing',
    '3' => 'Item Ready',
    '4' => 'Done',
    '5' => 'On Hold'
]));

define('item_priority', serialize([
    '0' => 'Medium',
    '1' => 'High',
    '2' => 'Low'
]));

define('meal_type', serialize([
    '1' => 'Starter',
    '2' => 'Appetizer',
    '3' => 'Main course',
    '4' => 'Dessert',


    '10' => 'Beverage',
    '11' => 'Tobacco',
    '12' => 'Wine',
    '13' => 'Food',
]));

define('table_status', serialize([
    '0' => 'Available',
    '1' => 'Reserved',
    '2' => 'On Hold'
]));

define('pos_payment_method', serialize([
    '1' => 'Cash',
    '2' => 'VISA / MASTER / AMEX',
    //'3' => 'Master',
    //'4' => 'American Express',

    '9' => 'Complimentary',
    '10' => 'Credit Individual',
    '11' => 'Credit Corporate',
    //'12' => 'Automatic Deduct Individual',
    //'13' => 'Automatic Deduct Corporate'
]));


define('BANKS', serialize([
    //'ALL' => 'All Banks',
    'SAMPATH_BANK' => 'Sampath Bank',
    'NATIONS_TRUST_BANK' => 'Nation Trust',
    'HATTON_NATIONAL_BANK' => 'Hatton National Bank',
    'BOC' => 'BOC',
    'NDB' => 'NDB',
    'HSBC' => 'HSBC',
    'CITY' => 'City Bank'
]));

define('pos_card_type', serialize([
    '1' => 'Visa',
    '2' => 'Master',
    '3' => 'American Express'
]));


/********Discount Setting********/

define('by_item_discount_setting', serialize([
    '1' => 'CARD',
    '2' => 'HAPPY_HOUR',
    '3' => 'MEMBER',
    '4' => 'ALL'
]));

define('discount_type', serialize([
    '1' => 'Item Vice',
    '2' => 'Total',
    '3' => 'Both'
]));

define('PROMOTION_DISCOUNT_TYPES', serialize([
    '1' => 'CARD',
    '2' => 'HAPPY_HOUR',
    '3' => 'MEMBER'
]));

define('membership_status', serialize([
    '0' => 'Pending Approval',
    '1' => 'Active',
    '2' => 'Deactivated'
]));


define('WEB_URL', 'http://dev.appslanka.com/elevate-web/api/');

define('floor1', serialize([
    '29' => [
        "Lounge" => [2, 7],
        "Restaurant" => [1, 7, 8],
        "Private Dining" => [1, 7, 8],
        "Teppanyaki Room" => [1, 7, 8]
    ],
    '30' => [
        "Altitude" => [5, 7, 8],
        "Altitude Signature" => [5, 7, 8],
    ]
]));

define('floor', serialize([

    "Lounge" => [2, 7],
    "Restaurant" => [1, 7, 8],
    "Private Dining" => [1, 7, 8],
    "Teppanyaki Room" => [1, 7, 8],
    "Altitude" => [5, 7, 8],
    "Altitude Signature" => [5, 7, 8],
]));

define('item_icon', serialize([
    "shot" => 'shot',
    "half_bottle" => 'half_bottle',
    "bottle" => 'bottle'
]));

define('menu_icon', serialize([
    "food" => 'food',
    "beverage" => 'beverage',
    "wine" => 'wine'
]));


