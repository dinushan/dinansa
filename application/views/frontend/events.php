<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer">
   <div class="row">
      <div class="col-xs-12 breadcrumbCont">
		  <?php include(APPPATH.'views/frontend/inc/breadcrumb.php'); ?>
      </div>
     <!--  <div class="col-xs-12 headWrap">
         <h2 class="heading">Advertise</h2>
      </div> -->
      
     </div>
</div>
<div class="container-fluid pageMiddleContainer eventsPage">
	<div class="row">
		<?php
		$i = 0;
		foreach ($events_list as $add) {
			$oddEven =($i % 2) ? 'odd':'even';
		?>
		<div class="col-xs-12 eventsSec">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 media eventsMedia">
			<?php
			if ($oddEven == 'even')
			{
				?>
					  <div class="media-left media-side media-middle">
					    <a class="media-object-wrap">
					      <img class="media-object" src="<?php echo base_url(EVENTS_IMG_PATH . $add['image']) ?>" alt="...">
					    </a>
					  </div>
				<?php
			}
			?>
					  <div class="media-body media-middle">
					    <h3 class="media-heading"><?php echo $add['title']; ?></h3>
					    <div class="mediaDesc">
					    <p><?php echo $add['description']; ?></p>
					    </div>
					  </div>

						<?php
						if ($oddEven == 'odd')
						{
							?>
							<div class="media-right media-side media-middle">
								<a class="media-object-wrap">
									<img class="media-object" src="<?php echo base_url(EVENTS_IMG_PATH . $add['image']) ?>" alt="...">
								</a>
							</div>
							<?php
						}
						?>

					</div>
				</div>
			</div>
		</div>
			<?php
			$i++;
		}
		?>
	</div>
</div>
<div class="container-fluid pageBottomContainer eventsPage">
	<div class="row">
		<!-- <div class="col-xs-12 headWrap">
			<h2 class="heading">Flexi packages </h2>
		</div>
		<div class="col-xs-12 col-md-10 col-md-offset-1 packagesDesc">
			<p>In the end, costing matters. The best part in advertising in cinema apart from the above details, offers inexpensive screening spots and good return on investment compared to other mediums. You can also check out the various packages offered to every budget before you decide.</p>
			<p>There are gold , silver and bronze packages across all Scope cinema halls advertising options to reach your end consumer.
			</p>
			<p>Contact us on <a class="contact" href="tel:0777332123"> 0777 332123 </a> or email us at <a class="contact" href="mailto:waruna@scopecinema.lk"> waruna@scopecinema.lk </a> to find out more.</p>
		</div> -->
		<!-- <div class="col-xs-12 col-md-4 packageWrap">
			BRONZE
		</div>
		<div class="col-xs-12 col-md-4 packageWrap">
			SILVER
		</div>
		<div class="col-xs-12 col-md-4 packageWrap">
			GOLD
		</div> -->
		<div class="gatheringFormWrap">
			<?php
			echo get_message();
			$attributes = array('id' => 'frm-contact', 'class'=>'gatheringForm');
			echo form_open(current_url(), $attributes);
			?>
			 		<div class="form-group col-xs-12 col-sm-6">
					    <label for="name">Name</label>
						<input type="text" value="<?php echo set_value('name') ?>" class="form-control input-scope" name="name" id="name" placeholder="Name">
						<?php echo form_error('name'); ?>
					  </div>
					  <div class="form-group col-xs-12 col-sm-6">
					    <label for="company">Company/Group</label>
						  <input type="text" value="<?php echo set_value('company') ?>" class="form-control input-scope" name="company" id="company" placeholder="Company">
						  <?php echo form_error('company'); ?>
					  </div>
					  <div class="form-group col-xs-12 col-sm-6">
					    <label for="email">Email</label>
						  <input type="email" value="<?php echo set_value('email') ?>" class="form-control input-scope" name="email" id="email" placeholder="Email">
						  <?php echo form_error('email'); ?>
					  </div>
					  <div class="form-group col-xs-12 col-sm-6">
					    <label for="contact">Contact No</label>
						  <input type="text" value="<?php echo set_value('contact') ?>" class="form-control input-scope" name="contact" id="contact" placeholder="Contact No">
						  <?php echo form_error('contact'); ?>
					  </div>
				 	<div class="form-group col-xs-12">
					    <label for="comment">Details of the gathering </label>
						<textarea  class="form-control input-scope" value="<?php echo set_value('comment') ?>" name="comment" id="comment" placeholder="Comment"></textarea>
						<?php echo form_error('comment'); ?>
					</div>
				   <div class="form-group buttonWrap col-xs-12">
					  <button type="submit" class="btn btn-lg btn-block btn-blue">SUBMIT</button>
					</div>
				</form>
		</div>
	</div>
</div>

	<script>
		var jvalidate = $("#frm-contact").validate({
			ignore: [],
			rules: {
				name: {
					required: true
				},
				company: {
					required: true
				},
				email: {
					required: true,
					email: true
				},
				contact: {
					required: true
				},
				comment: {
					required: true
				}
			}
		});
	</script>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>