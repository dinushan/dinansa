<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<style>
    .online-booking .pageLoaderCont{
        background-color: rgba(0,0,0,0.5);
    }
</style>

<script src="<?php echo base_url() ?>angular/angular.js?v=2.1"></script>
<script src="<?php echo base_url() ?>angular/angular-ui-router.min.js"></script>
<script src="<?php echo $this->config->item('socket_url')?>/socket.io/socket.io.js"></script>
<script src="<?php echo base_url() ?>skin_admin/global/plugins/jquery-alerts/jquery.alerts.js?v=2.1"></script>

<script>

    window.selected_mv       = <?php echo json_encode($selected_movie); ?>;
    window.selected_date     = <?php echo json_encode($selected_date); ?>;
    window.selected_slot     = <?php echo json_encode($selected_slot); ?>;

    window.movies_data       = <?php echo json_encode($movies_data); ?>;
    window.selected_theater  = <?php echo json_encode($selected_theater); ?>;
    window.siteconf          = <?php echo json_encode($siteConfig); ?>;

    try{
        //window.socket            = io(window.socket_url,{secure: true});


    }catch(err){

    }


</script>
<script src="<?php echo base_url(); ?>angular/moment.js?v=2.1.1" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/angular-moment.min.js?v=2.1.1" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>angular/buy-tickets/app.js?v=2.1.1" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/buy-tickets/controller/MainController.js?v=2.1.1" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/buy-tickets/directives/filterPlane.js?v=2.1.1" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/buy-tickets/controller/categoryController.js?v=2.1.1" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/buy-tickets/controller/filterController.js?v=2.1.1" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/buy-tickets/controller/seatsController.js?v=2.1.1" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/buy-tickets/controller/summeryController.js?v=2.1.1" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/buy-tickets/services/seatplanServcie.js?v=2.1.1" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/buy-tickets/services/service.js?v=2.1.1" type="text/javascript"></script>


<!-- BEGIN CONTAINER -->

<div class="pageContainer portlet" ng-controller="mainController">
	<div class="container">
		<div class="row">
			<div class="col-xs-12 breadcrumbCont breadcrumbByTicketsCont">
				<?php include(APPPATH.'views/frontend/inc/breadcrumb.php'); ?>
                <h2 class="pageTitle buyTicketTitle" ng-cloak>{{Htitle}}</h2>
			</div>
			<div class="col-xs-12 visible-xs">
				<h2 class="pageTitle buyTicketTitle" ng-cloak>{{Htitle}}</h2><!-- for the mobile -->
			</div>
		</div>
	</div>

    <filter-plane ng-show="showFilter">

    </filter-plane>

    <seat-plane ng-show="showSeatPlane" seats="seats">

    </seat-plane>

    <arrange-plane ng-show="showCategory">

    </arrange-plane>

    <summery-page ng-show="showSummery">

    </summery-page>

</div>


<script src="<?php echo base_url()?>skin_admin/global/plugins/jquery-alerts/jquery.alerts.js" type="text/javascript"></script>
<link href="<?php echo base_url()?>skin_admin/global/plugins/jquery-alerts/jquery.alerts.css" rel="stylesheet" type="text/css"/>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>
