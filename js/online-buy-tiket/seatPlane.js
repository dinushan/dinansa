
//console.log(window.moviedata);

$("#time_slot").on('change', function (e) {
    var movie = $("#movie").val().trim();
    var movie_date = $("#movie_date").val().trim();
    var theater = $("#theater").val().trim();
    var time_slot = $("#time_slot").val().trim();
    if (movie != '' && movie_date != '' && theater != '' && time_slot != '') {
        $.ajax({
                type: "POST",
                url: window.moviedata.siteurl+'counter/'+window.moviedata.slug+'/seat_plan_data',//'<?php echo site_url('counter/' . $slug . '/seat_plan_data') ?>',
            data: {
            movie: movie,
                movie_date: movie_date,
                theater: theater,
                time_slot: time_slot
        },
        dataType: "json",
            cache: false,
            success: function (data) {
            //  console.log(data.seat_plan);
            //  set_validation(data.seat_plan);
            if (data.seat_plan.length > 0) {
                set_validation(data);
                $('#available_seats').html(data.available_seat_count);
            }

        }
    });// you have missed this bracket
    }
    return false;
});


var el = $("#box-seat-plan");
var tab_st = 'draw';

$('.required-entry').attr('required', 'required');


var ctrl = false;
var shift = false;
var $seats_with = 26;
var $space = 30;
var $seats = [];


function set_validation(_seat_obj) {

    var $sc = 0;
    $seats = [];

    $('.tbl-block-seat').find('input[type="text"]').each(function () {
        $sc += Number($(this).val());

        $seats.push($(this).val());
    })

    generate_seat_plan(_seat_obj);

}

function generate_seat_plan(data1) {
    var data = data1.seat_plan;
    var seat_plan_booked = data1.seat_plan_booked;
    var seat_plan_hold = data1.seat_plan_hold;
    var seat_plan_selected = data1.seat_plan_selected;
    //  console.log(seat_plan_booked);
    // var data= data1.seat_plan;

    $('.block-plan-container').html('');
    $('#item-container').html('');


    $html_seat_lbl = '';
    $html_seat = '';


    var str = 'A';

    if (data == '') {
        for ($i = 0; $i < $seats.length; $i++) {


            $top = $i * ($space);

            var s = $seats[$i];
            for ($j = 0; $j < $seats[$i]; $j++) {

                $seat_no = str + s;
                $left = $j * ($seats_with);
                $html_seat += '<div data-id="" data-row="' + $i + '" data-seat_no="' + $seat_no + '" class="row_' + $i + ' seat_item" style="top:' + $top + 'px;left:' + $left + 'px;" title="' + $seat_no + '">' + $seat_no + '</div>';


                s--;
            }


            str = nextChar(str);

        }
    }
    else {

        for ($i = 0; $i < data.length; $i++) {
            //console.log(data[$i]);
            $seat_id = data[$i].id;
            $row = data[$i].row;
            $top = data[$i].y;
            $left = data[$i].x;
            $seat_no = data[$i].label;
            $status = data[$i].status;

            if ($status == 0) {
                $seal_class = ' hide_seat';
            } else {
                $seal_class = '';
            }

            // $html_seat += '<div data-id="'+$seat_id+'" data-row="'+$row+'" data-seat_no="'+$seat_no+'" class="row_'+$row+$seal_class+' seat_item" style="top:'+$top+'px;left:'+$left+'px;" title="'+$seat_no+'">'+$seat_no+'</div>';

            // console.log($seat_no);
            // console.log(seat_plan_booked);

            if ($.inArray($seat_no, seat_plan_booked) != -1) {
                $html_seat += '<div data-id="' + $seat_id + '" data-row="' + $row + '" data-seat_no="' + $seat_no + '" class="row_' + $row + $seal_class + ' seat_item booked_seat" style="top:' + $top + 'px;left:' + $left + 'px;" title="' + $seat_no + '">' + $seat_no + '</div>';
            }

            else if ($.inArray($seat_no, seat_plan_hold) != -1) {
                $html_seat += '<div data-id="' + $seat_id + '" data-row="' + $row + '" data-seat_no="' + $seat_no + '" class="row_' + $row + $seal_class + ' seat_item hold_seat" style="top:' + $top + 'px;left:' + $left + 'px;" title="' + $seat_no + '">' + $seat_no + '</div>';
            }
            else if ($.inArray($seat_no, seat_plan_selected) != -1) {
                $html_seat += '<div data-id="' + $seat_id + '" data-row="' + $row + '" data-seat_no="' + $seat_no + '" class="row_' + $row + $seal_class + ' seat_item selected_seat" style="top:' + $top + 'px;left:' + $left + 'px;" title="' + $seat_no + '">' + $seat_no + '</div>';
            }


            else {
                $html_seat += '<div data-id="' + $seat_id + '" data-row="' + $row + '" data-seat_no="' + $seat_no + '" class="row_' + $row + $seal_class + ' seat_item" style="top:' + $top + 'px;left:' + $left + 'px;" title="' + $seat_no + '">' + $seat_no + '</div>';
            }
        }
    }


    $maxSeatCount = Math.max.apply(Math, $seats) + 100;


    $('#item-container').html($html_seat);
    setAvailabilityCount();

    $('.block-plan-container').width(($seats_with * $maxSeatCount));
    $('#item-container').height($seats_with * $seats.length + 500);


    $('#item-container')
        .drag("start", function (ev, dd) {
            return $('<div class="selection" />')
                .css('opacity', .65)
                .appendTo(document.body);
        })
        .drag(function (ev, dd) {
            if (tab_st == 'draw') {
                $(dd.proxy).css({
                    top: Math.min(ev.pageY, dd.startY),
                    left: Math.min(ev.pageX, dd.startX),
                    height: Math.abs(ev.pageY - dd.startY),
                    width: Math.abs(ev.pageX - dd.startX)
                });
            }
        })
        .drag("end", function (ev, dd) {
            $(dd.proxy).remove();
        });


    $('.seat_item')
        .click(function () {

            if (!$(this).hasClass("booked_seat")) {
                $(this).toggleClass("selected_seat");


                var movie = $("#movie").val().trim();
                var movie_date = $("#movie_date").val().trim();
                var theater = $("#theater").val().trim();
                var time_slot = $("#time_slot").val().trim();


                var selected = '', status = '';
                if ($(this).hasClass('selected_seat') && !$(this).hasClass('hold_seat')) {
                    $offset = $(this).position();
                    selected = $(this).data('seat_no');
                    status = 2;

                }

                if (!$(this).hasClass('selected_seat') && !$(this).hasClass('hold_seat')) {
                    selected = $(this).data('seat_no');
                    status = 0;
                }


                //var movie=$("#movie").val().trim();
                var booking_id = $("#booking_id").val().trim();
                if (movie != '' && movie_date != '' && theater != '' && time_slot != '' && selected != '') {
                    $.ajax({
                            type: "POST",
                            url: window.moviedata.siteurl+'counter/'+window.moviedata.slug+'/selected_seats',//'<?php echo site_url('counter/' . $slug . '/selected_seats') ?>',
                        data: {
                        selected: selected,
                            booking_id: booking_id,
                            movie: movie,
                            movie_date: movie_date,
                            theater: theater,
                            status: status,
                            time_slot: time_slot
                    },
                    dataType: "json",
                        cache: false,
                        success: function (data) {
                        $("#booking_id").val(data);
                        // console.log(data);
                    }
                });
                }
            }


        });


}

