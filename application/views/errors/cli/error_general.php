<?php
defined('BASEPATH') OR exit('No direct scripts access allowed');

echo "\nERROR: ",
	$heading,
	"\n\n",
	$message,
	"\n\n";