<!-- BEGIN CONTAINER -->

<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title"><?php echo $page_title ?></h3>

            <?php echo get_message() ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-container">

                        <table class="table table-striped table-bordered table-hover" id="datatable_ajax"
                               data-order-col = "0"
                               data-order-type = "desc"
                               data-ajax_url="<?php echo base_url('admin/audit_log/ajax_list/'.$type) ?>">
                            <thead>
                            <tr role="row" class="heading">
                                <th width="200">
                                    Date
                                </th>
                                <th width="20%">
                                    User
                                </th>
                                <th class="sorting_disabled">
                                    Description
                                </th>
                                <th width="150" class="sorting_disabled">
                                    Actions
                                </th>
                            </tr>
                            <tr role="row" class="filter">
                                <td>
                                    <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control form-filter input-sm" readonly name="date_from" placeholder="From">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
                                    </div>
                                    <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control form-filter input-sm" readonly name="date_to" placeholder="To">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
											</span>
                                    </div>
                                </td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="user_full_name">
                                </td>
                                <td>
                                </td>
                                <td>
                                    <div class="margin-bottom-5">
                                        <button class="btn btn-sm yellow filter-submit margin-bottom"><i
                                                class="fa fa-search"></i> Search
                                        </button>
                                    </div>
                                    <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset
                                    </button>
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>
                    </div>

                </div>
            </div>


        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
<script>
    jQuery(document).ready(function() {
        TableAjax.init();
    });
</script>
