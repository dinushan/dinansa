        <div class="page-container">
            <?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

            <div class="page-content-wrapper" ng-controller="sendSmsController">
                <div class="page-content">
                    <h3 class="page-title">Send SMS</h3>

                    <div class="portlet-body">
                        <form role="form">
                            <div class="form-body">

                                <div class="form-group">
                                    <label></label>
                                    <div class="mt-checkbox-inline">
                                        <label class="mt-checkbox">
                                            <input type="checkbox" ng-model="selectAll" value="option1" /> To ALL
                                            <span></span>
                                        </label>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label>Mobile Number(s)</label>
                                    <div class="input-group">
                                        <span class="input-group-addon">
                                            <i class="fa fa-phone"></i>
                                        </span>
                                        <input type="text" class="form-control" placeholder="Mobile No" ng-disabled="selectAll"/>
                                    </div>
                                </div>

                               <div class="form-group">
                                    <label>Message </label>
                                    <textarea class="form-control" rows="3"></textarea>
                                </div>


                            <div class="form-actions">
                                <button type="button" class="btn blue" ng-click="sendMessage()">Submit</button>
                                <button type="button" class="btn default">Cancel</button>
                            </div>
                        </form>



                    </div>

                    <hr/>
                    <!--- Table --->

                    <div class="portlet light bordered">
                        <div class="portlet-title">
                            <div class="caption font-dark">
                                <span class="caption-subject bold uppercase">SMS History</span>
                            </div>
                            <div class="tools"> </div>
                        </div>
                        <div class="portlet-body">
                            <div id="sample_1_wrapper" class="dataTables_wrapper no-footer">

                                <div class="row">
                                    <div class="col-md-6 col-sm-12">
                                        <div class="dataTables_length" id="sample_1_length">
                                            <label>
                                                <select name="sample_1_length" aria-controls="sample_1" class="form-control input-sm input-xsmall input-inline">
                                                    <option value="5">5</option>
                                                    <option value="10">10</option>
                                                    <option value="15">15</option>
                                                    <option value="20">20</option>
                                                    <option value="-1">100</option>
                                                </select> entries
                                            </label>
                                        </div>
                                    </div>
                                    <!--div-- class="col-md-6 col-sm-12">
                                        <div id="sample_1_filter" class="dataTables_filter">
                                            <label>Search:<input type="search" class="form-control input-sm input-small input-inline" placeholder="" aria-controls="sample_1"></label>
                                        </div>
                                    </div-->
                                </div>
                                <div class="table-scrollable">
                                    <table class="table table-striped table-bordered table-hover dt-responsive dataTable no-footer dtr-inline collapsed" width="100%" id="sample_1" role="grid" aria-describedby="sample_1_info" style="width: 100%;">
                                        <thead>
                                        <tr role="row">
                                            <th class="all sorting_asc" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1"  aria-sort="ascending" aria-label="First name: activate to sort column descending">
                                                Send Date
                                            </th>
                                            <th class="min-phone-l sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Last name: activate to sort column ascending">
                                                message
                                            </th>
                                            <th class="min-tablet sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending">
                                                Number of Users
                                            </th>
                                            <th class="none sorting" tabindex="0" aria-controls="sample_1" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending">

                                            </th>

                                        </tr>
                                        </thead>
                                        <tbody>

                                        <tr role="row" class="odd">
                                            <td tabindex="0" class="sorting_1">Airi</td>
                                            <td>Satou</td>
                                            <td></td>
                                            <td>
                                                <div class="btn-group pull-right">
                                                    <button class="btn green btn-xs btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                        <i class="fa fa-angle-down"></i>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="fa fa-print"></i> Print </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                        <tr role="row" class="even">
                                            <td class="sorting_1" tabindex="0">Angelica</td>
                                            <td>Ramos</td>
                                            <td></td>
                                            <td>
                                                <div class="btn-group pull-right">
                                                    <button class="btn green btn-xs btn-outline dropdown-toggle" data-toggle="dropdown">Tools
                                                        <i class="fa fa-angle-down"></i>
                                                    </button>
                                                    <ul class="dropdown-menu pull-right">
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="fa fa-print"></i> Print </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="fa fa-file-pdf-o"></i> Save as PDF </a>
                                                        </li>
                                                        <li>
                                                            <a href="javascript:;">
                                                                <i class="fa fa-file-excel-o"></i> Export to Excel </a>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>

                                        </tbody>
                                    </table>

                                </div>
                                <div class="row">
                                    <div class="col-md-5 col-sm-12">
                                        <div class="dataTables_info" id="sample_1_info" role="status" aria-live="polite">Showing 1 to 10 of 57 entries</div>
                                    </div>
                                    <div class="col-md-7 col-sm-12">
                                        <div class="dataTables_paginate paging_bootstrap_number" id="sample_1_paginate">
                                            <ul class="pagination" style="visibility: visible;">
                                                <li class="prev disabled">
                                                    <a href="#" title="Prev">
                                                        <i class="fa fa-angle-left"></i>
                                                    </a>
                                                </li>
                                                <li class="active">
                                                    <a href="#">1</a>
                                                </li>
                                                <li>
                                                    <a href="#">2</a>
                                                </li>
                                                <li>
                                                    <a href="#">3</a>
                                                </li>
                                                <li>
                                                    <a href="#">4</a>
                                                </li>
                                                <li>
                                                    <a href="#">5</a>
                                                </li>
                                                <li class="next">
                                                    <a href="#" title="Next">
                                                        <i class="fa fa-angle-right"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <!-- End table -->






                </div>
            </div>
        </div>

        <script>
            (function (window) {
                var app = angular.module('cinema');

                app.controller('sendSmsController', sendSmsController);

                sendSmsController.$inject = ['$scope', '$window'];

                function sendSmsController($scope, $window) {
                    $scope.selectAll=true;


                    $scope.sendMessage = function (){
                        console.log("Send SMS");
                        $window.$.blockUI({ message: '<h6>Just a moment...</h6>' });
                    }

                }
            }(window));
        </script>
