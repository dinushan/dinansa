<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title">Advance Booking Report</h3>


            <form class="form-horizontal" method="POST" action="<?php echo current_url()?>">
                <div class="portlet box blue portlet-filter">
                    <div class="portlet-title">
                        <div class="caption">Filter</div>
                        <div class="tools">
                            <button type="submit" class="btn btn-danger btn-sm">Show Report</button>
                        </div>
                    </div>
                    <div class="portlet-body ">
                        <div class="form-body clearfix" style="max-width:500px;">

                            <div class="form-group">
                                <div class="col-md-3"><label class="control-label" for="sales_report_period_type">Movie</label></div>
                                <div class="col-md-9">
                                    <select class="form-control input-sm" name="movie" id="movie368">
                                        <option value="-1">--any--</option>
                                        <?php foreach ($movies as $movie):?>
                                            <option value="<?php echo $movie->movie_id;?>" <?php echo ($selected_movie == $movie->movie_id)?'selected':''?> ><?php echo $movie->movie_name;?></option>
                                        <?php endforeach; ?>
                                    </select>

                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-md-3"><label class="control-label" for="sales_report_period_type">Theater</label></div>
                                <div class="col-md-9" id="match-div">
                                    <select class="form-control input-sm" name="theater" id="theater368">
                                        <option value="-1">--any--</option>
                                        <?php foreach ($theaters as $theater):?>
                                            <option value="<?php echo $theater->theater_id;?>" <?php echo ($selected_theater == $theater->theater_id)?'selected':''?> ><?php echo $theater->theater_name;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3"><label class="control-label" for="sales_report_period_type">Movie Time</label></div>
                                <div class="col-md-9" id="match-div">
                                    <select class="form-control input-sm" name="movie_time" >
                                        <option value="-1">--any--</option>
                                        <?php foreach ($time_slots as $time_slot):?>
                                            <option value="<?php echo $time_slot->time_slot;?>" <?php echo ($selected_time_slot == $time_slot->time_slot)?'selected':''?> ><?php echo date("h:i A",strtotime($time_slot->time_slot));?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-md-3"><label class="control-label">Payment Type</label></div>
                                <div class="col-md-9" id="match-div">
                                    <select class="form-control input-sm" name="payment_method" >
                                        <option value="-1">--any--</option>
                                        <?php foreach ($payment_methods as $payment_method):?>
                                            <option value="<?php echo $payment_method->id;?>" <?php echo ($selected_method == $payment_method->id)?'selected':''?> ><?php echo $payment_method->name;?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </div>
                            </div>

                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="trans_sw" <?php echo $filter_trans_switch == 'trans_sw'? 'checked':'' ?> name="trans_switch" id="trans_switch_856"> Trans On
                                </label>
                            </div>

                            <div class="form-group" id="trans_from_193">
                                <div class="col-md-3"><label class="control-label" for="report_from">Trans From<span class="required">*</span></label></div>
                                <div class="col-md-9"><input id="trans_from" type="text" class="form-control form-control-inline input-medium date-picker input-sm" name="trans_from" placeholder="From" value="<?php echo $from?>"></div>
                            </div>

                            <div class="form-group" id="trans_to_193">
                                <div class="col-md-3"><label class="control-label" for="report_from">Trans To <span class="required">*</span></label></div>
                                <div class="col-md-9"><input id="trans_to" type="text" class="form-control form-control-inline input-medium date-picker input-sm" name="trans_to" placeholder="To" value="<?php echo $to?>"></div>
                            </div>


                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" value="movie_sw" <?php echo $filter_movie_switch == 'movie_sw'? 'checked':'' ?> name="movie_switch" id="movie_switch_856"> Movie on
                                </label>
                            </div>

                            <div class="form-group" id="moive_from_group_193" >
                                <div class="col-md-3"><label class="control-label" for="report_to">Movie From<span class="required">*</span></label></div>
                                <div class="col-md-9"><input type="text" class="form-control form-control-inline input-medium date-picker  input-sm" name="moive_from" placeholder="To" value="<?php echo $moive_from ?>"></div>
                            </div>

                            <div class="form-group" id="moive_to_group_193" >
                                <div class="col-md-3"><label class="control-label" for="report_to">Movie To<span class="required">*</span></label></div>
                                <div class="col-md-9"><input type="text" class="form-control form-control-inline input-medium date-picker  input-sm" name="moive_to" placeholder="To" value="<?php echo $moive_to ?>"></div>
                            </div>




                        </div>
                    </div>

                </div>
            </form>

            <div class="portlet light bordered">

                <div class="portlet-body">
                    <div class="dataTables_wrapper no-footer">
                        <div class="row">
                            <div class="col-md-12">
                                <a class="btn btn-default pull-right" id="exportCVS1910" download>Export To Excel</a>
                            </div>
                        </div>
                        <div class="table-scrollable">

                            <table class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline">
                                <thead>
                                    <tr role="row">

                                        <th>Trans Date</th>
                                        <th>Movie</th>
                                        <th>Movie Date</th>
                                        <th>Movie Time</th>
                                        <th>Theater</th>

                                        <th>Total Seats</th>
                                        <th>DR Collection</th>
                                        <th>Total Amount</th>
                                    </tr>
                                </thead>

                                <tbody>

                                    <?php foreach($results as $result):?>
                                        <?php


                                        ?>
                                        <tr>
                                            <td><?php echo $result->trans_date; ?></td>
                                            <td><?php echo $result->movie_name; ?></td>
                                            <td><?php echo $result->movie_date; ?></td>
                                            <td><?php echo $result->movie_time; ?></td>
                                            <td><?php echo $result->theater_name; ?></td>
                                            <td><?php echo $result->total_seats; ?></td>
                                            <td><?php echo number_format($result->dr_collection,2); ?></td>
                                            <td><?php echo number_format($result->total_amount,2)?></td>
                                        </tr>
                                    <?php endforeach; ?>
                                </tbody>
                                <tfoot>
                                <?php
                                    $total_seats = 0 ;
                                    $total_amt = 0;

                                    if ($total_amount != null){
                                        $total_seats = $total_amount->total_seats;
                                        $total_amt   = $total_amount->total_amount;
                                    }
                                ?>
                                <tr>

                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                 
                                    <th><?php echo $total_seats; ?></th>
                                    <th><?php echo number_format($dr_total,2); ?></th>
                                    <th><?php echo number_format($total_amt,2); ?></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>

<script>


    var el = $(".portlet-body");

    $('#tournament').change(function(){

        Metronic.blockUI({
            target: el,
            animate: true,
            overlayColor: 'none'
        });
        var tournament_id = $(this).val();

        $.post('<?php echo base_url('reports/municipal_council/ajax_get_matches') ?>',{tournament_id:tournament_id},function(html)
        {
            $('#match-div').html(html);
            Metronic.unblockUI(el);
        });


    });


    $('.required-entry').attr('required', 'required');
    $('#edit_form').validate({
        errorElement: 'span',
        errorClass: 'validation-advice',
        ignore: "",
        submitHandler: function (form) {
            form.submit();
        }
    });


    $('.date-picker').datepicker({
        format: "yyyy-mm-dd",
        autoclose: true
    });

    $("#trans_switch_856").change(function() {
        if(this.checked) {
            $('#trans_from_193').removeAttr('style');
            $('#trans_to_193').removeAttr('style');
        }else{
            $('#trans_from_193').css('display','none');
            $('#trans_to_193').css('display','none');
        }
    });

    $("#movie_switch_856").change(function() {
        if(this.checked) {
            $('#moive_from_group_193').removeAttr('style');
            $('#moive_to_group_193').removeAttr('style');
        }else{
            $('#moive_from_group_193').css('display','none');;
            $('#moive_to_group_193').css('display','none');;
        }
    });

</script>
<script>
    window.param ={
        'theater':'<?php echo $selected_theater; ?>',
        'movie' : '<?php echo $selected_movie; ?>',
        'moive_from':'<?php echo $moive_from;?>',
        'movie_to' : '<?php echo $moive_to; ?>',
        'trans_from' :'<?php echo $from; ?>',
        'trans_to' : '<?php echo $to; ?>',
        'filter_movie_switch': '<?php echo $filter_movie_switch; ?>',
        'filter_trans_switch':'<?php echo $filter_trans_switch; ?>',
        'movie_time':'<?php echo $selected_time_slot; ?>',
        'payment_method':'<?php echo $selected_method; ?>',

    };
    (function (window,$) {
        $('#exportCVS1910').click(function(){
            var urlenc = [];
            if (window.param.theater.trim() != ''){
                urlenc.push('theater='+window.param.theater);
            }
            if (window.param.movie.trim() != ''){
                urlenc.push('movie='+window.param.movie);
            }

             if (window.param.filter_movie_switch.trim() != ''){
                 urlenc.push('movie_switch='+window.param.filter_movie_switch);
             }

             if (window.param.moive_from.trim() != ''){
                    urlenc.push('moive_from='+window.param.moive_from);
             }

             if (window.param.movie_to.trim() != ''){
                    urlenc.push('moive_to='+window.param.movie_to);
             }


            if (window.param.filter_trans_switch.trim() != ''){
                urlenc.push('trans_switch='+window.param.filter_trans_switch);
             }


             if (window.param.trans_from.trim() != ''){
                    urlenc.push('trans_from='+window.param.trans_from);
             }

             if (window.param.trans_to.trim() != ''){
                    urlenc.push('trans_to='+window.param.trans_to);
             }
            if (window.param.movie_time.trim() != ''){
                urlenc.push('movie_time='+window.param.movie_time);
            }
            if (window.param.payment_method.trim() != ''){
                urlenc.push('payment_method='+window.param.payment_method);
            }


            var myWindow = window.open(window.base_url+'admin/reports/advance-booking/export?'+urlenc.join('&'));
        });
    }(window,$))
</script>