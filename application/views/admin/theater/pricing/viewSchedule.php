<div class="page-container">

    <?php $this->load->view('admin/inc_sidebar_navigation.php');?>
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title"><?php echo $theater->name.' : '.$movie->movie_name;?></h3>

            <div>

                <?php foreach ($schedule as $sch):?>
                <div class="portlet box blue">
                    <div class="portlet-title">
                        <div class=""caption>
                            <h4><?php

                                $dt = new DateTime($sch->time_slot);
                                echo $dt->format('Y M d , h:m a');
                                ?></h4>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                            <tr>
                                <th ng-repeat ="timeslot in selectedSlots">
                                    <i class="fa fa-clock-o" aria-hidden="true"></i>

                                </th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                                <?php
                                $prices_object = json_decode($sch->prices_object);

                                foreach ($prices_object as $priceings):
                                    ?>
                                <td>
                                    <div class="form-horizontal">

                                        <div class="form-group">
                                            <div class="control-element col-md-12">
                                                <div class="input-group">
                                                    <span class="input-group-addon"><?php echo $priceings->category_name; ?></span>
                                                    <p><?php echo $priceings->price; ?></p>
                                                </div>
                                            </div>
                                        </div>
                                    </div> <!-- end form-->
                                    <?php ?>
                                </td>
                                <?php endforeach;?>
                            </tr>
                            </tbody>
                        </table>
                    </div><!-- end portlet-body -->
                </div>
                <?php endforeach;?>

            </div>
        </div>
    </div>
</div>
