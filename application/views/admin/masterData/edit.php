
<div class="page-container">

    <?php $this->load->view('admin/inc_sidebar_navigation.php');?>

    <div class="page-content-wrapper">
		<div class="page-content">
<?php if(isset($_SESSION['error'])){$error =$_SESSION['error']; } ?>
            <h3 class="page-title">Master Data </h3>
    <div class="panel panel-default">
        
        <div class="panel-body portlet-body form">

            <?php echo get_message() ?>

            <?php echo form_open('admin/master-data/update',['class'=>'form-horizontal']); ?>
                <div class="form-body ">
                    <?php /*
                    <div class="form-group">
                        <label class="control-label col-md-3">Premium Price</label>

                        <div class="control-element col-md-4">
                            <input type="number" class="form-control" value="<?php echo $masterData->premium_price ?>" name="premiumprice" aria-describedby="premiumprice" placeholder="0.00">
                        </div>
                        <?php if(isset($error['primiumprice'])): ?>
                            <small class="form-text text-danger"><?php echo $error['primiumprice'] ?>.</small>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Maintenance</label>

                        <div class="control-element col-md-4">
                            <input type="number" class="form-control" value="<?php echo $masterData->maintainance ?>" name="maintanance" aria-describedby="maintanance" placeholder="0.00"/>
                        </div>
                        <?php if(isset($error['maintanace'])): ?>
                            <small class="form-text text-danger"><?php echo $error['maintanace'] ?>.</small>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">SMS Cost</label>

                        <div class="control-element col-md-4">
                            <input type="number" class="form-control" value="<?php echo $masterData->sms_cost ?>" name="smscost" aria-describedby="smscost" placeholder="0.00"/>
                        </div>
                        <?php if(isset($error['smscost'])): ?>
                            <small class="form-text text-danger"><?php echo $error['smscost'] ?>.</small>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">IPG Rate</label>

                        <div class="control-element col-md-4">
                            <input type="number" class="form-control" value="<?php echo $masterData->ipg_rate ?>" name="ipgrate" aria-describedby="ipgrate" placeholder="0.00"/>
                        </div>
                        <?php if(isset($error['ipgrate'])): ?>
                            <small class="form-text text-danger"><?php echo $error['ipgrate'] ?>.</small>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Booking Time</label>

                        <div class="control-element col-md-4">
                            <input type="number" class="form-control" value="<?php echo $masterData->booing_time ?>" name="bookingtime" aria-describedby="bookingtime" placeholder="0.00" />
                        </div>
                        <?php if(isset($error['booingtime'])): ?>
                            <small class="form-text text-danger"><?php echo $error['booingtime'] ?>.</small>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Advance Booking</label>

                        <div class="control-element col-md-4">
                            <input type="number" class="form-control" value="<?php echo $masterData->advance_booking ?>" name="advancebooking" aria-describedby="advancebooking" placeholder="0.00" />
                        </div>
                        <?php if(isset($error['advance_booking'])): ?>
                            <small class="form-text text-danger"><?php echo $error['advance_booking'] ?>.</small>
                        <?php endif; ?>
                    </div>
                    */ ?>

                    <div class="form-group">
                        <label class="control-label col-md-3">Film Levy <span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <input type="text" class="form-control" value="<?php echo $masterData->film_leavy ?>" name="film_leavy" aria-describedby="advancebooking" placeholder="0.00" />
                        </div>
                        <?php if(isset($error['film_leavy'])): ?>
                            <small class="form-text text-danger"><?php echo $error['film_leavy'] ?>.</small>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Government Tax (%)<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <input type="number" class="form-control" value="<?php echo $masterData->government_tax ?>" name="government_tax" aria-describedby="advancebooking" placeholder="0.00" />
                        </div>
                        <?php if(isset($error['government_tax'])): ?>
                            <small class="form-text text-danger"><?php echo $error['government_tax'] ?>.</small>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Film importer/supplier (%)<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <input type="number" class="form-control" value="<?php echo $masterData->film_importer ?>" name="film_importer" aria-describedby="advancebooking" placeholder="0.00" />
                        </div>
                        <?php if(isset($error['film_importer'])): ?>
                            <small class="form-text text-danger"><?php echo $error['film_importer'] ?>.</small>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Film Distributor (%)<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <input type="number" class="form-control" value="<?php echo $masterData->film_distributor ?>" name="film_distributor" aria-describedby="advancebooking" placeholder="0.00" />
                        </div>
                        <?php if(isset($error['film_distributor'])): ?>
                            <small class="form-text text-danger"><?php echo $error['film_distributor'] ?>.</small>
                        <?php endif; ?>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Film exciter/hall (%)<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <input type="text" class="form-control" value="<?php echo $masterData->film_exibiter ?>" name="film_exibiter" aria-describedby="advancebooking" placeholder="0.00" />
                        </div>
                        <?php if(isset($error['film_exibiter'])): ?>
                            <small class="form-text text-danger"><?php echo $error['film_exibiter'] ?>.</small>
                        <?php endif; ?>
                    </div>

                </div>
                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <button type="submit" class="btn btn-success">Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
    </div>
    </div>
</div>