<?php include(APPPATH . 'views/frontend/inc/head.php'); ?>
<?php include(APPPATH . 'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer theatresPage" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div class="col-xs-12 breadcrumbCont">
            <?php include(APPPATH . 'views/frontend/inc/breadcrumb.php'); ?>
            <div class="backBtnWrap">
                <a href="#" class="btn btn-blue">Back to careers</a>
            </div>
            
        </div>

      <!--   <div class="col-xs-12 headWrap">
            <h2 class="heading"><?php echo $sitePageData->menu; ?></h2>
        </div> -->
    </div>
</div>

<div class="container">
    <div class="row">
        <div class="col-xs-12 col-md-8 col-md-offset-2 careereDerailWrap">
             <h3 class="careereDerailTitle"><?php echo $career->title?></h3>

                <div class="desc">
                    <?php echo $career->content?>
                </div>
                <div class="careerDetailLabel">
                    Please send your CV to <b>hr@scopecinema.com</b> and our Careers Team will get in touch with you.
                </div>
        </div>
    </div>
    
</div>

<?php include(APPPATH . 'views/frontend/inc/footer.php'); ?>