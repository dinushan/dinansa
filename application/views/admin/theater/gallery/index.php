<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">

    <h3 class="page-title"><?php echo $theater_data->name ?> : Upload Banner Images</h3>
    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn default" href="<?php echo site_url('admin/'.$slug.'/edit/'.$enc_id) ?>"><i class="fa fa-chevron-left"></i> Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12"> <?php echo get_message() ?>
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light bordered">
                <div class="portlet-body">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><form>
                                    <input id="file_upload" name="file_upload" type="file" multiple>
                                </form>
                                <span class="help-block red">Image Size:<?php echo THEATER_GALLERY_IMG_W ?>px x <?php echo THEATER_GALLERY_IMG_H ?>px, Image Type:jpg</span>
                                <div id="queue"></div></td>
                        </tr>
                    </table>

                    <script type="text/javascript">
                        <?php $timestamp = time();?>
                        $sel_numImg = 0;
                        $com_numImg = 0;
                        jQuery('#file_upload').uploadify({
                            'formData'     : {
                                'user_id'     : '<?php echo $this->encrypt_lib->encode($this->adminauth->user_id) ?>',
                            },
                            'buttonText' : 'Browse Images',
                            'queueID'  : 'queue',
                            'fileTypeDesc' : 'jpg Files',
                            'fileTypeExts' : '*.jpg',
                            'debug'	: false,
                            'swf'      : '<?php echo base_url() ?>skin_admin/global/plugins/uploadify/uploadify.swf',
                            'uploader' : '<?php echo site_url('admin/'.$slug.'/picture_upload/'.$enc_id); ?>',
                            'onSelect' : function(file) {
                                $sel_numImg++;
                            },
                            'onUploadSuccess' : function(file, data, response) {
                                $com_numImg++;
                                if($sel_numImg == $com_numImg ){
                                    window.location.reload();
                                }
                            }
                        });

                    </script>
                    <div class="grid">
                        <div id="thumbContent">
                            <div style="height:600px;overflow:scroll; position:relative; padding:0;margin: 0;">
                                <ul id="list">
                                    <?php
                                    $i = 1;
                                    foreach($imageList as $row):
                                        $id = $row['id'];
                                        $image = $row['image'];

                                        ?>
                                        <li id="recordsArray_<?php echo $id ?>">
                                            <div class="handle"><img src="<?php echo base_url().THEATER_GRID_IMG_PATH.$image ?>" width="118" /></div>
                                            <div class="del"></div>
                                            <div class="number"><?php echo $i ?></div>
                                        </li>
                                        <?php
                                        $i++;
                                    endforeach;
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">

                        jQuery("#list").sortable({ opacity: 0.6, cursor: 'move',handle : '.handle', update: function() {
                            var order = jQuery(this).sortable("serialize") + '&action=updateRecordsListings';
                            jQuery.post("<?php echo site_url('admin/'.$slug.'/sort_images/') ?>", order, function(theResponse){
                                jQuery('#thumbContent').find('li').each(function(i){
                                    jQuery(this).find('.number').html(i+1);
                                });
                            });
                        }});


                        jQuery('#list .del').click(function(event){
                            var _self = jQuery(this);
                            var _id = _self.parent().attr('id');
                            message = 'Are you sure you want to delete?';
                            jConfirm('Are you sure you want to delete?','Confirmation',function(r){
                                if( r == true ) {
                                    jQuery('#'+_id).remove();
                                    jQuery.post("<?php echo site_url('admin/'.$slug.'/delete_images/') ?>", {'id':_id}, function(theResponse){	});
                                    jQuery('#thumbContent').find('li').each(function(i){
                                        jQuery(this).find('.number').html(i+1);
                                    });
                                }
                            });

                        });


                    </script>

                </div>
            </div>
        </div>
    </div>

</div>
<!-- END PAGE CONTENT-->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->