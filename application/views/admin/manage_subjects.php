<!-- BEGIN CONTAINER -->

<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<h3 class="page-title">Manage Subjects</h3>
			
			<div class="page-bar">				
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<a onclick="setLocation('<?php echo site_url('admin/'.$slug.'/add') ?>')" class="btn green"><i class="fa fa-plus-circle"></i> Add New</a>
					</div>
				</div>
			</div>
			
			
			<?php echo get_message() ?>

			
			
			<h3 class="page-title">Manage Subjects</h3>
			<div class="page-bar">				
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<a class="btn default" href="<?php echo site_url('admin/'.$slug.'/') ?>"><i class="fa fa-chevron-left"></i> Back</a>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12"> <?php echo get_message() ?> 
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet light bordered">
						<div class="portlet-body form"> 
							<!-- BEGIN FORM-->
							<form method="post" action="<?php echo base_url('SubjectController/create')?>">
                <div class="modal-body">
                    <div class="form-body form-horizontal">

                        <div class="form-group">
                            <label class="col-md-3">Subject Name<b class="text-danger">*</b></label>
                            <div class="col-md-9">
                                <input  type="text" class="form-control"  name="name" value="<?php echo  set_value('name')?> " />
                                <?php if ( form_error('item_code') ): ?>

                                    <?php echo form_error('item_code'); ?>
                                <?php endif; ?>
                            </div>


                        </div>

    


                    </div>
                </div>
                <div class="modal-footer">
                    <button type="submit" class="btn btn-sm green" name="save" value="1">Save and Exit</button>

                    <a href="<?php echo base_url('inventory/manageInventoryItems')?>" class="btn btn-default" >Back</a>
                </div>
            </form>
						</div>
					</div>
				</div>
			</div>						
			
		</div>
		<!-- END PAGE CONTENT--> 
	</div>
	<!-- END CONTENT --> 
	
</div>
<!-- END CONTAINER --> 
