// jQuery Alert Dialogs Plugin
//
// Version 1.1
//
// Cory S.N. LaViska
// A Beautiful Site (http://abeautifulsite.net/)
// 14 May 2009
//
// Visit http://abeautifulsite.net/notebook/87 for more information
//
// Usage:
//		jAlert( message, [title, callback] )
//		jConfirm( message, [title, callback] )
//		jPrompt( message, [value, title, callback] )
// 
// History:
//
//		1.00 - Released (29 December 2008)
//
//		1.01 - Fixed bug where unbinding would destroy all resize events
//
// License:
// 
// This plugin is dual-licensed under the GNU General Public License and the MIT License and
// is copyright 2008 A Beautiful Site, LLC. 
//
(function($) {
	
	$.alerts = {
		
		// These properties can be read/written by accessing $.alerts.propertyName from your scripts at any time
		
		verticalOffset: -75,                // vertical offset of the dialog from center screen, in pixels
		horizontalOffset: 0,                // horizontal offset of the dialog from center screen, in pixels/
		repositionOnResize: true,           // re-centers the dialog on window resize
		overlayOpacity: .01,                // transparency level of overlay
		overlayColor: '#FFF',               // base color of overlay
		draggable: true,                    // make the dialogs draggable (requires UI Draggables plugin)
		okButton: '&nbsp;OK&nbsp;',         // text for the OK button
		cancelButton: '&nbsp;Cancel&nbsp;', // text for the Cancel button
		dialogClass: null,                  // if specified, this class will be applied to all dialogs
		
		// Public methods
		
		alert: function(message, title, callback) {
			if( title == null ) title = 'Alert';
			$.alerts._show(title, message, null, 'alert', function(result) {
				if( callback ) callback(result);
			});
		},
		warning: function(message, title, callback) {
			if( title == null ) title = 'Warning!';
			$.alerts._show(title, message, null, 'warning', function(result) {
				if( callback ) callback(result);
			});
		},
		confirm: function(message, title, callback) {
			if( title == null ) title = 'Confirm';
			$.alerts._show(title, message, null, 'confirm', function(result) {
				if( callback ) callback(result);
			});
		},
			
		prompt: function(message, value, title, callback) {
			if( title == null ) title = 'Prompt';
			$.alerts._show(title, message, value, 'prompt', function(result) {
				if( callback ) callback(result);
			});
		},
		caption: function(value1,value2, title, callback) {
			if( title == null ) title = 'Caption';
			$.alerts._caption_show(title, value1,value2, 'caption', function(result1,result2) {
				if( callback ) callback(result1,result2);
			});
		},
		home_banner_caption: function(value1,value2,value3,value4,value5, title, callback) {
			if( title == null ) title = 'Image Details';
			$.alerts._home_banner_caption_show(title, value1,value2,value3,value4,value5, 'caption', function(result1,result2,result3,value4,value5) {
				if( callback ) callback(result1,result2,result3,value4,value5);
			});
		},
		
		editor: function(ele, callback) {			
			$.alerts._show_editor(ele, function(result) {
				if( callback ) callback(result);
			});
		},
		// Private methods		
		_show_editor: function(ele, callback) {
			
			$.alerts._hide();
			$.alerts._overlay('show');
			
			$("BODY").append(
			  '<div id="popup_container" class="editor">' +			   
			    '<div id="popup_content">' +
			      '<div id="popup_editor"></div>' +
				'</div>' +
			  '</div>');
			
			if( $.alerts.dialogClass ) $("#popup_container").addClass($.alerts.dialogClass);
			
			// IE6 Fix
			var pos = ($.browser.msie && parseInt($.browser.version) <= 6 ) ? 'absolute' : 'fixed'; 
			
			$("#popup_container").css({
				position: pos,
				zIndex: 999,
				padding: 0,
				margin: 0
			});
			
	
			
			$("#popup_container").css({
				minWidth: $("#popup_container").outerWidth(),
				maxWidth: $("#popup_container").outerWidth()
			});
			
			$.alerts._reposition();
			$.alerts._maintainPosition(true);
			
			var editor, html = '';
			
			html = $('#'+ele).val();
			
			$("#popup_editor").append('<div id="editor"></div>').after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" /> <input type="button" value="' + $.alerts.cancelButton + '" id="popup_cancel" /></div>');
			
			if ( editor )
				return;

			// Create a new editor inside the <div id="editor">, setting its value to html
			var config = {};
			editor = CKEDITOR.appendTo( 'editor', config, html );
			editor.config.toolbar = "popup";
			editor.config.resize_enabled = false;
			editor.config.height = '330px';
			
			
			$("#popup_ok").click( function() {
				var val = editor.getData();
				$('#'+ele).val(val);
				$.alerts._hide();
				if( callback ) callback( val );
			});
			$("#popup_cancel").click( function() {
				
				if ( editor ){	
					editor.destroy();// Destroy the editor.
					editor = null;
				}
				$.alerts._hide();
				if( callback ) callback( null );
			});
			
			$("#popup_ok, #popup_cancel").keypress( function(e) {
				if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
				if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
			});
							
			
			// Make draggable
			if( $.alerts.draggable ) {
				try {
					//$("#popup_container").draggable({ handle: $("#popup_title") });
					//$("#popup_title").css({ cursor: 'move' });
				} catch(e) { /* requires jQuery UI draggables */ }
			}
		},
		_show: function(title, msg, value, type, callback) {
			
			$.alerts._hide();
			$.alerts._overlay('show');
			
			$("BODY").append(
			  '<div id="popup_container">' +
			    '<h1 id="popup_title"></h1>' +
			    '<div id="popup_content">' +
			      '<div id="popup_message"></div>' +
				'</div>' +
			  '</div>');
			
			if( $.alerts.dialogClass ) $("#popup_container").addClass($.alerts.dialogClass);
			
			// IE6 Fix
			var pos = ($.browser.msie && parseInt($.browser.version) <= 6 ) ? 'absolute' : 'fixed'; 
			
			$("#popup_container").css({
				position: pos,
				zIndex: 999,
				padding: 0,
				margin: 0
			});
			
			$("#popup_title").text(title);
			$("#popup_content").addClass(type);
			$("#popup_message").text(msg);
			$("#popup_message").html( $("#popup_message").text().replace(/\n/g, '<br />') );
			
			$("#popup_container").css({
				minWidth: $("#popup_container").outerWidth(),
				maxWidth: $("#popup_container").outerWidth()
			});
			
			$.alerts._reposition();
			$.alerts._maintainPosition(true);
			
			switch( type ) {
				case 'alert':
					$("#popup_message").after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" /></div>');
					$("#popup_ok").click( function() {
						$.alerts._hide();
						callback(true);
					});
					$("#popup_ok").focus().keypress( function(e) {
						if( e.keyCode == 13 || e.keyCode == 27 ) $("#popup_ok").trigger('click');
					});
				break;
				
				case 'warning':
					$("#popup_message").after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" /></div>');
					$("#popup_ok").click( function() {
						$.alerts._hide();
						callback(true);
					});
					$("#popup_ok").focus().keypress( function(e) {
						if( e.keyCode == 13 || e.keyCode == 27 ) $("#popup_ok").trigger('click');
					});
				break;
				
				case 'confirm':
					$("#popup_message").after('<div id="popup_panel"><input type="button" value=" Yes " id="popup_ok" /> <input type="button" value=" No " id="popup_cancel" /></div>');
					$("#popup_ok").click( function() {
						$.alerts._hide();
						if( callback ) callback(true);
					});
					$("#popup_cancel").click( function() {
						$.alerts._hide();
						if( callback ) callback(false);
					});
					$("#popup_ok").focus();
					$("#popup_ok, #popup_cancel").keypress( function(e) {
						if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
						if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
					});
				break;
				case 'prompt':
					$("#popup_message").append('<br /><input type="text" size="30" id="popup_prompt" />').after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" /> <input type="button" value="' + $.alerts.cancelButton + '" id="popup_cancel" /></div>');
					$("#popup_prompt").width( $("#popup_message").width() );
					$("#popup_ok").click( function() {
						var val = $("#popup_prompt").val();
						//$.alerts._hide();
						if( callback ) callback( val );
					});
					$("#popup_cancel").click( function() {
						$.alerts._hide();
						if( callback ) callback( null );
					});
					$("#popup_prompt, #popup_ok, #popup_cancel").keypress( function(e) {
						if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
						if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
					});
					if( value ) $("#popup_prompt").val(value);
					$("#popup_prompt").focus().select();
				break;	
						
			}
			
			// Make draggable
			if( $.alerts.draggable ) {
				try {
					$("#popup_container").draggable({ handle: $("#popup_title") });
					$("#popup_title").css({ cursor: 'move' });
				} catch(e) { /* requires jQuery UI draggables */ }
			}
		},
		_caption_show: function(title, value1,value2, type, callback) {
			
			$.alerts._hide();
			$.alerts._overlay('show');
			
			$("BODY").append(
			  '<div id="popup_container" class="image_info">' +
			    '<h1 id="popup_title"></h1>' +
			    '<div id="popup_content">' +
			      '<div id="popup_message"></div>' +
				'</div>' +
			  '</div>');
			
			if( $.alerts.dialogClass ) $("#popup_container").addClass($.alerts.dialogClass);
			
			// IE6 Fix
			var pos = ($.browser.msie && parseInt($.browser.version) <= 6 ) ? 'absolute' : 'fixed'; 
			
			$("#popup_container").css({
				position: pos,
				zIndex: 999,
				padding: 0,
				margin: 0
			});
			
			$("#popup_title").text(title);
			$("#popup_content").addClass(type);

			
			$("#popup_container").css({
				minWidth: $("#popup_container").outerWidth(),
				maxWidth: $("#popup_container").outerWidth()
			});
			
			$.alerts._reposition();
			$.alerts._maintainPosition(true);
			
			switch( type ) {
				
				case 'caption':
					$("#popup_message").append('Enter image title - &lt; img title="<strong>XXXXX</strong>" /&gt; <br /><input type="text" size="60" name="caption" id="popup_caption" /><br /><br />Enter image alt - &lt; img alt="<strong>XXXXX</strong>" /&gt;<br /><input type="text" size="60" name="alt" id="popup_alt" />').after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" /> <input type="button" value="' + $.alerts.cancelButton + '" id="popup_cancel" /></div>');
					//$("#popup_caption").width( $("#popup_message").width() );
					$("#popup_ok").click( function() {
						var val1 = $("#popup_caption").val();
						var val2 = $("#popup_alt").val();
						
						//$.alerts._hide();
						if( callback ) callback( val1,val2);
					});
					$("#popup_cancel").click( function() {
						$.alerts._hide();
						if( callback ) callback( '','' );
					});
					$("#popup_caption, #popup_alt, #popup_ok, #popup_cancel").keypress( function(e) {
						if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
						if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
					});
					if( value1 ) $("#popup_caption").val(value1);
					if( value2 ) $("#popup_alt").val(value2);
					//$("#popup_caption").focus().select();
					
				break;
			}
			
			// Make draggable
			if( $.alerts.draggable ) {
				try {
					$("#popup_container").draggable({ handle: $("#popup_title") });
					$("#popup_title").css({ cursor: 'move' });
				} catch(e) { /* requires jQuery UI draggables */ }
			}
		},
		
		_home_banner_caption_show: function(title,value1,value2,value3,value4,value5,type,callback) {
			
			$.alerts._hide();
			$.alerts._overlay('show');
			
			$("BODY").append(
			  '<div id="popup_container" class="image_info">' +
			    '<h1 id="popup_title"></h1>' +
			    '<div id="popup_content">' +
			      '<div id="popup_message"></div>' +
				'</div>' +
			  '</div>');
			
			if( $.alerts.dialogClass ) $("#popup_container").addClass($.alerts.dialogClass);
			
			// IE6 Fix
			var pos = ($.browser.msie && parseInt($.browser.version) <= 6 ) ? 'absolute' : 'fixed'; 
			
			$("#popup_container").css({
				position: pos,
				zIndex: 999,
				padding: 0,
				margin: 0
			});
			
			$("#popup_title").text(title);
			$("#popup_content").addClass(type);

			
			$("#popup_container").css({
				minWidth: $("#popup_container").outerWidth(),
				maxWidth: $("#popup_container").outerWidth()
			});
			
			$.alerts._reposition();
			$.alerts._maintainPosition(true);
			
			switch( type ) {

				case 'caption':
				
					 var _html = 'Enter Banner Title 1<br />'+
					 '<input type="text" size="60" name="title" id="popup_btitle" class="form-control" /><span class="help-block">Ex: BUY TICKETS ONLINE</b></span><br />'+
					'Enter Banner Title 1(Link)<br />'+
					'<input type="text" size="60" name="imgtitle" id="popup_imgtitle" class="form-control" /><span class="help-block">Ex: '+base_url+'<b>URL KEY</b></span><br />'+
					 'Enter Banner Title 2<br />'+
					 '<input type="text" size="60" name="caption" id="popup_caption" class="form-control" /><span class="help-block">Ex: GREAT WALLS</b></span><br />'+
					 'Enter Banner Title 3<br />'+
					'<input type="text" size="60" name="position" id="popup_btitle_position" class="form-control" /><span class="help-block">Ex: NOW SHOWING AT SAVOY CINEMAS</b></span><br />'+
					 /*'Enter image alter text<br />'+*/
					 'Enter Banner URL'+
					'<input type="text" size="60" name="alt" id="popup_alt" class="form-control" /><span class="help-block">Ex: '+base_url+'<b>URL KEY</b></div>';
				
					$("#popup_message").append(_html).after('<div id="popup_panel"><input type="button" value="' + $.alerts.okButton + '" id="popup_ok" /> <input type="button" value="' + $.alerts.cancelButton + '" id="popup_cancel" /></div>');
					//$("#popup_caption").width( $("#popup_message").width() );
					
					$("#popup_ok").click( function()
					{
						var val1 = $("#popup_btitle").val();
						var val2 = $("#popup_caption").val();
						var val3 = $("#popup_alt").val();
						var val4 = $("#popup_btitle_position").val();
						var val5 = $("#popup_imgtitle").val();
												
						//$.alerts._hide();
						if( callback ) callback( val1,val2,val3,val4,val5);
					});
					$("#popup_cancel").click( function() {
						$.alerts._hide();
						if( callback ) callback( '','','' );
					});
					$("#popup_caption, #popup_alt, #popup_imgtitle, #popup_ok, #popup_cancel").keypress( function(e) {
						if( e.keyCode == 13 ) $("#popup_ok").trigger('click');
						if( e.keyCode == 27 ) $("#popup_cancel").trigger('click');
					});
					if( value1 ) $("#popup_btitle").val(value1);
					if( value2 ) $("#popup_caption").val(value2);
					if( value3 ) $("#popup_alt").val(value3);
					if( value4 ) $("#popup_btitle_position").val(value4);
					if( value5 ) $("#popup_imgtitle").val(value5);
					//$("#popup_caption").focus().select();
					
				break;
			}
			
			// Make draggable
			if( $.alerts.draggable ) {
				try {
					$("#popup_container").draggable({ handle: $("#popup_title") });
					$("#popup_title").css({ cursor: 'move' });
				} catch(e) { /* requires jQuery UI draggables */ }
			}
		},
		_hide: function() {
			$("#popup_container").remove();
			$.alerts._overlay('hide');
			$.alerts._maintainPosition(false);
		},
		
		_overlay: function(status) {
			switch( status ) {
				case 'show':
					$.alerts._overlay('hide');
					$("BODY").append('<div id="popup_overlay"></div>');
					$("#popup_overlay").css({
						position: 'absolute',
						zIndex: 998,
						top: '0px',
						left: '0px',
						width: '100%',
						height: $(document).height(),
						background: $.alerts.overlayColor,
						opacity: $.alerts.overlayOpacity
					});
				break;
				case 'hide':
					$("#popup_overlay").remove();
				break;
			}
		},
		
		_reposition: function() {
			var top = (($(window).height() / 2) - ($("#popup_container").outerHeight() / 2)) + $.alerts.verticalOffset;
			var left = (($(window).width() / 2) - ($("#popup_container").outerWidth() / 2)) + $.alerts.horizontalOffset;
			if( top < 0 ) top = 0;
			if( left < 0 ) left = 0;
			
			// IE6 fix
			if( $.browser.msie && parseInt($.browser.version) <= 6 ) top = top + $(window).scrollTop();
			
			$("#popup_container").css({
				top: top + 'px',
				left: left + 'px'
			});
			$("#popup_overlay").height( $(document).height() );
		},
		
		_maintainPosition: function(status) {
			if( $.alerts.repositionOnResize ) {
				switch(status) {
					case true:
						$(window).bind('resize', $.alerts._reposition);
					break;
					case false:
						$(window).unbind('resize', $.alerts._reposition);
					break;
				}
			}
		}
		
	}
	
	// Shortuct functions
	jAlert = function(message, title, callback) {
		$.alerts.alert(message, title, callback);
	}
	jWarning = function(message, title, callback) {
		$.alerts.warning(message, title, callback);
	}
	jConfirm = function(message, title, callback) {
		$.alerts.confirm(message, title, callback);
	};
		
	jPrompt = function(message, value, title, callback) {
		$.alerts.prompt(message, value, title, callback);
	};
	
	jCaption = function(value1, value2, title, callback) {
		$.alerts.caption(value1, value2, title, callback);
	};
	
	jHomeBannerCaption = function(value1, value2, value3, value4, value5, title, callback) {
		$.alerts.home_banner_caption(value1, value2, value3, value4, value5, title, callback);
	};
	
	jEditor = function(ele, callback) {
		$.alerts.editor(ele, callback);
	};
	
})(jQuery);