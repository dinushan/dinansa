<?php 

	$detect = new Mobile_Detect;
	$deviceType = ($detect->isMobile() ? ($detect->isTablet() ? 'tablet' : 'phone') : 'computer');
	$scriptVersion = $detect->getScriptVersion();
 
	if($deviceType !="computer"){
		$forceLayout	= 'mobile';//desktop
		$this->session->set_userdata(array('am_force_theme_layout'=>$forceLayout));
		
		if(!$this->session->userdata('force_param')){	
			$this->session->set_userdata(array('force_param'=>$forceLayout));
		}
	}
	
	if (isset($_GET['force_param'])){
		if ($_GET['force_param'] == 'mobile'){
			$forceLayout	= 'mobile';
			$this->session->set_userdata(array('force_param'=>$forceLayout));
		} elseif ($_GET['force_param'] == 'desktop'){
			$forceLayout	= 'desktop';
			$this->session->set_userdata(array('force_param'=>$forceLayout));
		}
		
		redirect(current_url());
	}
?>