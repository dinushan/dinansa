/**
 * Created by LAYOUTindex on 11/29/2017.
 */
(function(window){
    var app = angular.module('inventoryitems',[]);

    app.directive('datatable', function ($window){

        return {
            templateUrl:$window.baseurl+'assets/js/inventory_items/datatable.html',

            controller:function ($scope,$window) {

                $window.locations.unshift({'id':-1,'stock_name':'any'});
                $window.categories.unshift({'id':-1,'category_name':'any'});
                $window.mesureunits.unshift({'id':-1,'unit_name':'any'});

                $scope.views        = [
                    {id:10,val:10},
                    {id:20,val:20},
                    {id:50,val:50},
                    {id:100,val:100},
                    {id:150,val:150},
                    {id:-1,val:'All'}
                ];

                $scope.itemcode     = null;
                $scope.itemname     = null;
                $scope.page         = 1;
                $scope.totalPages   = 0;
                $scope.view         = $scope.views[0];
                $scope.locations    = $window.locations;
                $scope.categories   = $window.categories;
                $scope.mesureunits  = $window.mesureunits;
                $scope.location     = $window.locations[0];
                $scope.category     = $window.categories[0];
                $scope.mesureunit   = $window.mesureunits[0];
                $scope.totalrecods  = 0;
                $scope.results = [];

               $scope.getStyle = function (ob){
                     var curval = parseFloat(ob[7]), minval = parseFloat(ob[8]);

                    if (curval < minval+10 || curval == minval){

                        return {'background-color':'#F59DA8','color':'#fff'};
                    }else if (curval < minval){
                        return {'background-color':'#F53D46','color':'#fff'};

                     }else{
                        return null;
                    }

                };


               $scope.prev = function (){
                   if ($scope.page <= 1){
                       $scope.page = 1;
                   }else{
                       $scope.page -= 1;

                   }
                   sendfilter();
               };

               $scope.next = function (){
                   $scope.page += 1;
                   sendfilter();
               };

                var sendfilter = function () {
                    window.jQuery.ajax({
                        'method':'POST',
                        'url':$window.ajaxUrl,
                        'data':{
                            'item_code':$scope.itemcode,
                            'item_name':$scope.itemname,
                            'location':$scope.location.id,
                            'units':$scope.mesureunit.id,
                            'category':$scope.category.id,
                            'length':$scope.view.id,
                            'start':$scope.page
                        }
                    }).done( function (resp) {
                        $scope.$apply(function(){
                            $scope.results = resp.data;
                            $scope.totalrecods = resp.recordsTotal;
                            if ($scope.view.id != -1){
                                $scope.totalPages = Math.ceil(resp.recordsTotal/$scope.view.val);
                            }else{
                                $scope.totalPages = 1;
                            }

                        });
                    }).error( function (){

                    })
                };

                sendfilter();

                $scope.filter = function (){
                    sendfilter();
                };


            }

        }

    });
}(window));