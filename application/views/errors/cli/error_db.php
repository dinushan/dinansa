<?php
defined('BASEPATH') OR exit('No direct scripts access allowed');

echo "\nDatabase error: ",
	$heading,
	"\n\n",
	$message,
	"\n\n";