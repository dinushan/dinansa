<!-- BEGIN CONTAINER -->

<?php 
$yesno = unserialize(YESNO);
?>

<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">

			<h3 class="page-title">Add Counter User</h3>
			<div class="page-bar">				
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<a class="btn default" href="<?php echo site_url('admin/counterusers') ?>"><i class="fa fa-chevron-left"></i> Back</a>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div> <?php echo get_message() ?> </div>
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet light bordered">
						<div class="portlet-body form"> 
							<!-- BEGIN FORM-->
							<?php                    
							$attr = array("id"=>"edit_form","class"=>"form-horizontal","enctype"=>"multipart/form-data");
							echo form_open(current_url(),$attr);
							?>
							<div class="form-group">
								<label class="control-label col-md-3">Username<span class="req">*</span></label>
								<div class="control-element col-md-4">
									<input class="form-control required-entry" type="text" value="<?php echo set_value('username') ?>" name="username" />
									<?php echo form_error('username'); ?>
								</div>
							</div>

                          <!--  <div class="form-group">
                                <label class="control-label col-md-3">Employee ID</label>
                                <div class="control-element col-md-4">
                                    <input class="form-control" type="text" value="<?php echo set_value('emp_id') ?>" name="emp_id" />
                                    <?php echo form_error('emp_id'); ?> </div>
                            </div>-->

							<div class="form-group">
								<label class="control-label col-md-3">Full Name<span class="req">*</span></label>
								<div class="control-element col-md-4">
									<input class="form-control required-entry" type="text" value="<?php echo set_value('fullname') ?>" name="fullname" />
									<?php echo form_error('fullname'); ?> </div>
							</div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Profile Picture <span class="req">*</span></label>

                                <div class="control-element col-md-4"><input type="file" class=" input-file" name="image">

                                <span class="help-block font-green">Image Size:<?php echo PROFILE_IMG_W ?>
                                    px &times; <?php echo PROFILE_IMG_H ?>px, Image Type: png</span>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="control-label col-md-3">NIC<span class="req">*</span></label>
                                <div class="control-element col-md-4">
                                    <input class="form-control required-entry" type="text" value="<?php echo set_value('nic') ?>" name="nic" />
                                    <?php echo form_error('nic'); ?> </div>
                            </div>
							<div class="form-group">
								<label class="control-label col-md-3">Email</label>
								<div class="control-element col-md-4">
									<input class="form-control" type="text" value="<?php echo set_value('email') ?>" name="email" />
									<?php echo form_error('email'); ?> </div>
							</div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Phone 1<span class="req">*</span></label>
                                <div class="control-element col-md-4">
                                    <input class="form-control required-entry" type="text" value="<?php echo set_value('phone1') ?>" name="phone1" />
                                    <?php echo form_error('phone1'); ?> </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Phone 2</label>
                                <div class="control-element col-md-4">
                                    <input class="form-control" type="text" value="<?php echo set_value('phone2') ?>" name="phone2" />
                                    <?php echo form_error('phone2'); ?> </div>
                            </div>

                            <div class="form-group">

                                <label class="control-label col-md-3">Cinema</label>
                                <div class="control-element col-md-4">
                                    <?php echo form_dropdown('cinema', $cinema_list, set_value('cinema'),'class="form-control"') ?>
                                    <?php echo form_error('cinema'); ?></div>
                            </div>


                            <div class="form-group">
                                <?php
                                $groupArr = array();
                                foreach($groups as $k=>$_group)
                                {
                                    //if($k != 1){
                                        $groupArr[$k] = $groups[$k];
                                   // }

                                }
                                ?>
                                <label class="control-label col-md-3">Groups</label>
                                <div class="control-element col-md-4"><?php echo form_dropdown('groups[]', $groupArr, set_value('groups[]'),'class="form-control"') ?> </div>
                            </div>
														
							<div class="form-group">
								<label class="control-label col-md-3">Password<span class="req">*</span></label>
								<div class="control-element col-md-4">
									<input class="form-control required-entry" type="password" value="" name="password" />
									<?php echo form_error('password'); ?></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Confirm Password<span class="req">*</span></label>
								<div class="control-element col-md-4">
									<input class="form-control required-entry" type="password" value="" name="password_conf" />
									<?php echo form_error('password_conf'); ?></div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
									</div>
								</div>
							</div>
							<script type="text/javascript">							 
								$('.required-entry').attr('required','required');					
								$('#edit_form').validate({
									errorElement: 'span', 
									errorClass: 'validation-advice', 
									submitHandler: function (form) {
										form.submit();										
									}
								});									
							</script>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT--> 
	</div>
	<!-- END CONTENT --> 
	
</div>
<!-- END CONTAINER --> 
