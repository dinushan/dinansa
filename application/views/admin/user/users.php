<!-- BEGIN CONTAINER -->

<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title">Users</h3>

            <div class="page-bar">
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <?php if(
                        $this->adminauth->has_role('102')
                        ):?>
                            <a onclick="setLocation('<?php echo site_url("admin/users/add") ?>')" class="btn green"><i
                                    class="fa fa-plus-circle"></i> Add User</a>
                        <?php endif; ?>
                        <?php if(
                        $this->adminauth->has_role('11')
                        ):?>
                            <a onclick="setLocation('<?php echo site_url("admin/users/audit_log") ?>')" class="btn btn-info"><i
                                    class="fa fa-file-o"></i> Audit Log</a>
                        <?php endif; ?>
                    </div>
                </div>
            </div>

            <?php echo get_message() ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-container">
                        <?php if(
                            $this->adminauth->has_role('101') ||
                            $this->adminauth->has_role('102') ||
                            $this->adminauth->has_role('103') ||
                            $this->adminauth->has_role('104')
                        ):?>
                            <table class="table table-striped table-bordered table-hover" id="datatable_ajax"
                                   data-order-col = "0"
                                   data-order-type = "asc"
                                   data-ajax_url="<?php echo base_url('admin/users/ajax_list') ?>">
                                <thead>
                                <tr role="row" class="heading">
                                    <th width="2%">
                                        ID
                                    </th>
                                    <th>
                                        Full Name
                                    </th>
                                    <th width="20%">
                                        Username
                                    </th>
                                    <th width="20%">
                                        Phone
                                    </th>
                                    <th width="150" class="sorting_disabled">
                                        Actions
                                    </th>
                                </tr>
                                <tr role="row" class="filter">
                                    <td>
                                    </td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="fullname">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="username">
                                    </td>
                                    <td>
                                        <input type="text" class="form-control form-filter input-sm" name="phone1">
                                    </td>
                                    <td>
                                        <div class="margin-bottom-5">
                                            <button class="btn btn-sm yellow filter-submit margin-bottom"><i
                                                    class="fa fa-search"></i> Search
                                            </button>
                                        </div>
                                        <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset
                                        </button>
                                    </td>
                                </tr>
                                </thead>
                                <tbody>
                                </tbody>
                            </table>
                        <?php endif; ?>
                    </div>

                </div>
            </div>


        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
<script>
    jQuery(document).ready(function() {
        TableAjax.init();
    });
</script>
