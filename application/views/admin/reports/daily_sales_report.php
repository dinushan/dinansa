<!-- BEGIN CONTAINER -->

<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title">Sales Report</h3>

            <?php if(
            $this->adminauth->has_role('1103')
            ):?>

                <div class="row">
                    <div class="col-md-12">
                        <form action="<?php echo current_url();?>" method="GET" class="form-horizontal">
                            <div class="portlet box blue">
                                <div class="portlet-title">
                                    <div class="caption">Filter</div>
                                    <div class="tools">
                                        <button type="submit" class="btn btn-danger btn-sm">Show Report</button>
                                    </div>
                                </div>
                                <div class="portlet-body ">
                                    <div class="form-body clearfix" style="max-width:500px;">

                                        <div class="form-group">
                                            <div class="col-md-3"><label class="control-label" for="sales_report_from">Date <span class="required">*</span></label></div>
                                            <div class="col-md-9"><input id="report_from" type="text" class="form-control form-control-inline input-medium date-picker required-entry  input-sm" name="from" placeholder="date" value="<?php echo $this->input->get('date')?>"></div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-3"><label class="control-label" for="theater">Theater</label></div>
                                            <div class="col-md-9">
                                                <select class="form-control input-sm" name="theater">
                                                    <option value="-1">any</option>
                                                    <?php foreach ($theaters as $th): ?>
                                                        <option value="<?php echo $th->theater_id?>" <?php echo ($theater==$th->theater_id)?'selected':'' ?> >
                                                            <?php echo $th->name; ?>
                                                        </option>
                                                    <?php endforeach;?>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-3"><label class="control-label" for="movie">Movie</label></div>
                                            <div class="col-md-9">
                                                <select class="form-control input-sm" name="movie">
                                                    <option value="-1">any</option>
                                                    <?php foreach ($movies as $mv): ?>
                                                        <option value="<?php echo $mv->movie_id?>" <?php echo ($movie==$mv->movie_id)?'selected':'' ?> >
                                                            <?php echo $mv->movie_name; ?>
                                                        </option>
                                                    <?php endforeach;?>
                                                </select>

                                            </div>
                                        </div>



                                        <div class="form-group">
                                            <div class="col-md-3"><label class="control-label" for="time_slot">Time Slot</label></div>
                                            <div class="col-md-9">
                                                <select class="form-control input-sm" name="time_slot">
                                                    <option value="-1" <?php echo ($time_slot == -1)?'selected':'' ?>>any</option>
                                                    <?php foreach ($time_slots as $mv): ?>
                                                        <option value="<?php echo strtotime ($mv->time_slot) ?>" <?php echo ($time_slot == strtotime($mv->time_slot))?'selected':'' ?> >
                                                            <?php echo $mv->show_slot; ?>
                                                        </option>
                                                    <?php endforeach;?>
                                                </select>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-3"><label class="control-label" for="tiket_type">Tiket Type</label></div>
                                            <div class="col-md-9">

                                                <select class="form-control input-sm" name="tiket_type">
                                                    <option value="1" <?php echo $tiket_type == 1 ? "selected":"" ?> >All</option>
                                                    <option value="2" <?php echo $tiket_type == 2 ? "selected":"" ?> >Complimentory</option>
                                                    <option value="3" <?php echo $tiket_type == 3 ? "selected":"" ?> >Normal</option>
                                                </select>

                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <div class="col-md-3"><label class="control-label" for="movie">Method</label></div>
                                            <div class="col-md-9">
                                                <select class="form-control input-sm" name="booking_method">
                                                    <option value="-1" <?php echo $booking_method==-1 ? "selected":"" ?> >All</option>
                                                    <option value="1" <?php echo $booking_method== 1 ? "selected":"" ?> >Normal</option>
                                                    <option value="2" <?php echo $booking_method== 2 ? "selected":"" ?> >Advance</option>
                                                </select>

                                            </div>
                                        </div>
                                        <!--div class="form-group">
                                            <div class="col-md-3"><label class="control-label" for="time_slot">Time slot</label></div>
                                            <div class="col-md-9">

                                            </div>
                                        </div-->


                                        <div class="form-group">
                                            <div class="col-md-3"><label class="control-label" for="show_booking_types">Booking type</label></div>
                                            <div class="col-md-9">
                                                <div class="mt-checkbox-inline">
                                                    <label class="mt-checkbox">
                                                        <input type="checkbox" name="counter" <?php echo ($counter) ?'checked':''; ?> /> Counter
                                                    </label>

                                                    <label class="mt-checkbox">
                                                        <input type="checkbox" name="online" <?php echo ($online) ?'checked':''; ?> /> Online
                                                    </label>

                                                    <label class="mt-checkbox">
                                                        <input type="checkbox" name="mobile" <?php echo ($fyi) ?'checked':''; ?> /> FYI
                                                    </label>


                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </div>

                            </div>
                        </form>

                        <div class="table-container">
                            <div class="row">
                                <div class="col-md-12">
                                    <button id="exportCVS1910" type="button" class="btn btn-default pull-right">Export to Excel</button>
                                </div>
                            </div>

                            <table id="tbl-sales" class="table table-striped table-bordered table-hover">
                                <thead>
                                <tr class="headings">
                                    <th>Period</th>
                                    <th>Theater</th>
                                    <th>Movie</th>
                                    <th>Time slot</th>
                                    <th>Booking Type</th>
                                    <th>Booking Method</th>
                                    <th>Tickets bought</th>
                                    <th>Category</th>
                                    <th>Total</th>
                                    <th>Discount</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php foreach($results as $result): ?>

                                    <tr>
                                        <td>
                                            <?php
                                            $format = new DateTime($result->period);
                                                echo $format->format('Y-m-d');


                                            ?>
                                        </td>
                                        <td><?php
                                            if ($theater == null){
                                                echo "Any";
                                            }else{
                                                echo $result->theater_name;
                                            }
                                            ?>
                                        </td>
                                        <td><?php

                                            if ($movie == null){
                                                echo "Any";
                                            }else{
                                                echo $result->movie_name;
                                            }

                                            ?></td>
                                        <td><?php echo $result->time_slot; ?></td>
                                        <td> <?php
                                            $method_types = [
                                                1=>'Counter',
                                                2=>'Online',
                                                3=>'Mobile'
                                            ];
                                            if ($counter != null || $online != null || $mobile != null){
                                                echo $method_types[$result->booking_type];
                                            }else{
                                                echo "*";
                                            }
                                            ?></td>
                                        <td><?php
                                            $booking_method_arr = [
                                                1=>'Normal',2=>'Advance'
                                            ];

                                            if ($booking_method != null && $booking_method > 0){
                                                echo $booking_method_arr[$result->booking_method];
                                            }else{
                                                echo "All";
                                            }

                                            ?></td>
                                        <td><?php echo $result->tickets_bought; ?></td>
                                        <td><?php echo $result->rate_category; ?></td>
                                        <td><?php echo number_format($result->total_amount,2); ?></td>
                                        <td><?php echo number_format($result->total_discount_amount,2); ?></td>


                                    </tr>

                                <?php  endforeach; ?>
                                <?php
                                if(count($results) == 0):
                                    ?>
                                    <tr>
                                        <td colspan="9" align="center">No records found.</td>
                                    </tr>
                                    <?php
                                endif;
                                ?>
                                </tbody>
                                <tfoot>
                                <tr>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th></th>
                                    <th><?php echo $total->tickets_bought?></th>
                                    <td></td>
                                    <th><?php echo number_format($total->total_amount,2) ?></th>
                                    <th><?php echo number_format($total->total_discount_amount,2) ?></th>
                                </tr>
                                </tfoot>
                            </table>
                        </div>
                    </div>
                </div>
                <script>

                    var el=$('body');

                    $('#show_booking_types').on('change',function(){

                        if($(this).val() == 1){
                            $('#div_show_booking_types').show();
                        }else{
                            $('#div_show_booking_types').hide();
                        }

                    });

                    <?php

                    if($this->input->get('booking_type') == 1){
                        echo "$('#div_show_booking_types').show();";
                    }
                    ?>

                    $("#btn-export-csv").click(function(){
                        $("#tbl-sales").tableToCSV();
                    });

                    $('.date-picker').datepicker({
                        format:'yyyy-mm-dd'
                    });

                    $('.required-entry').attr('required', 'required');
                    $('#edit_form').validate({
                        errorElement: 'span',
                        errorClass: 'validation-advice',
                        ignore: "",
                        submitHandler: function (form) {
                            form.submit();
                        }
                    });

                    function select_movie() {

                        var theater = $("#theater").val().trim();

                        var report_from= $("#report_from").val().trim();
                        var report_to= $("#report_to").val().trim();

                        if (report_from != '' && report_to != '' && theater != '') {

                            Metronic.blockUI({
                                target: el,
                                animate: true,
                                overlayColor: 'none'
                            });

                            $.ajax({
                                type: "POST",
                                url: '<?php echo site_url('admin/' . $slug . '/select_movie') ?>',
                                data: {theater: theater, report_from: report_from, report_to: report_to},
                                dataType: "json",
                                cache: false,
                                success: function (data) {

                                    Metronic.unblockUI(el);

                                    $('#time_slot').empty();
                                    var opt = $('<option />');
                                    opt.val('');
                                    opt.text('Select time slot');

                                    $('#movie').empty();
                                    /*var opt = $('<option />');
                                     opt.val('');
                                     opt.text('Select movie');
                                     $('#movie').append(opt);*/


                                    $.each(data, function (id, val) {
                                        var opt = $('<option />');
                                        opt.val(id);
                                        opt.text(val);
                                        $('#movie').append(opt);
                                    });
                                }
                            });// you have missed this bracket
                            return false;
                        }
                        else {
                            $('#time_slot').empty();
                            var opt = $('<option />');
                            opt.val('');
                            opt.text('Select time slot');

                            $('#movie').empty();
                            var opt = $('<option />');
                            opt.val('');
                            opt.text('Select movie');
                            $('#movie').append(opt);
                        }
                    }

                    $("#theater").on('change', function (e) {
                        select_movie();
                    });



                    $("#movie").on('change', function (e) {
                        var movie = $("#movie").val().trim();
                        var theater = $("#theater").val().trim();

                        var report_from= $("#report_from").val().trim();
                        var report_to= $("#report_to").val().trim();

                        Metronic.blockUI({
                            target: el,
                            animate: true,
                            overlayColor: 'none'
                        });

                        $.ajax({
                            type: "POST",
                            url: '<?php echo site_url('admin/' . $slug . '/select_time_slots') ?>',
                            data: {movie: movie, theater: theater, report_from: report_from, report_to: report_to},
                            dataType: "json",
                            cache: false,
                            success: function (data) {

                                Metronic.unblockUI(el);

                                $('#time_slot').empty();
                                /* var opt = $('<option />');
                                 opt.val('');
                                 opt.text('Select time slot');
                                 $('#time_slot').append(opt);*/

                                $.each(data, function (id, val) {
                                    var opt = $('<option />');
                                    opt.val(id);
                                    opt.text(val);
                                    $('#time_slot').append(opt);
                                });
                            }
                        });// you have missed this bracket
                        return false;
                    });
                </script>

            <?php endif; ?>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->