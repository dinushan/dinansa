<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

   <div class="container pageContainer mainPageContainer myBookingPage">
      <div class="row">
         <div class="col-xs-12 breadcrumbCont">
            <div class="breadcrumbWrap">
               <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                  <li class="active"><a href="#">My Bookings</a></li>
               </ol>
            </div>
            <div class="socialWrap">
               <ul class="social-links">
                  <li><a href="#" class="facebook"></a></li>
                  <li><a href="#" class="twitter"></a></li>
                  <li><a href="#" class="pinterest"></a></li>
                  <li><a href="#" class="google"></a></li>
                  <li><a href="#" class="instragram"></a></li>
               </ul>
            </div>
         </div>
         <div class="col-xs-12 headWrap">
            <h2 class="heading">My Bookings</h2>
         </div>
<?php
if(count($user_details['current']) == 0 &&  count($user_details['past']) == 0)
{
         echo '<div class="col-xs-12 bookingsPage">
            <h3 class="noDataMessage">There are no bookings</h3>
        </div>';
} 
 ?>
         
         <div class="col-xs-12 col-md-10 col-md-offset-1 myBookingCont">

            <?php
            foreach ($user_details['current'] as $current) {

               if (isset($current['time_slot'])) {
                  $date_time = new DateTime($current['time_slot']);
                  $date = $date_time->format('d S F Y');
                  $time = $date_time->format('h:i A');
               }

               ?>
               <div class="row bookingRow myBookingRow">
                  <div class="col-xs-12 col-sm-6 movieDetailSec">
                     <div class="movieBanner">
                        <picture class="img-responsive moviebannerImg">
                           <source srcset="<?php echo $current['portrait_image']; ?>"
                                   media="(min-width: 800px)">
                           <img srcset="<?php echo $current['portrait_image']; ?>" alt="…">
                        </picture>
                        <span class="bookingStatus current">Current</span>
                     </div>
                     <div class="movieDetail">
                        <h3 class="movieTitle"><?php echo $current['movie_name']; ?></h3>
                        <ul class="list-group scope-list-group detailList">
                           <?php
               if (isset($current['time_slot'])) {
                  ?>
                  <li class="list-group-item list"><?php echo $date; ?> <span class="time"><?php echo $time; ?></span>
                  </li>
                  <?php
               }
                  ?>
                           <li class="list-group-item list">In <?php echo $current['theater_name']; ?></li>
                           <li class="list-group-item list">Ref No - <?php echo $current['reference_no']; ?></li>
                           <?php
                           if (isset($current['all_labels'])) {
                              ?>
                              <li class="list-group-item list"><span class="title">No of seats -</span> <span
                                     class="count"><?php echo count($current['all_labels']); ?></span>
                                 <div class="seatsList">(<?php
                                    $label_array = array();
                                    foreach ($current['all_labels'] as $lab) {
                                       $label_array[] = '<span class="seat">' . $lab['label'] . '</span>';

                                    }
                                    echo implode(',', $label_array);
                                    ?>)
                                 </div>
                              </li>
                              <?php
                           }
                              ?>
                        </ul>
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 ticketDetailSec">
                     <div class="movieDetail">
                        <h4 class="ticketTitle">Ticket Type</h4>
                        <ul class="list-group scope-list-group detailList">
                           <?php
                           $tot = 0;
                           foreach ($current['price_details'] as $pri) {

                              $tot += $pri['qty'] * $pri['price'];

                              ?>
                              <li class="list-group-item list">
                                 <span class="listleft"><?php echo $pri['category']; ?> (<?php echo $pri['price'].' x '.$pri['qty']; ?>)</span><span class="listRight"><?php echo $pri['qty'] * $pri['price']; ?> LKR</span>
                              </li>
                              <?php
                           }
                           ?>
                           <li class="list-group-item list totalList">
                              <span class="listleft">Discount</span><span class="listRight"><?php echo $current['total_discount_amount']; ?> LKR</span>
                           </li>
                           <li class="list-group-item list totalList">
                              <span class="listleft">Total</span><span class="listRight"><?php echo ($tot - $current['total_discount_amount']); ?> LKR</span>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <?php
            }
               ?>

         </div>
      </div>
   </div>
   <div class="pageBottomSection">
      <div class="container pageBottomContainer pastBooking">
         <div class="row">
            <div class="col-xs-12 col-md-10 col-md-offset-1 pastBookingCont">

            <?php
            foreach ($user_details['past'] as $past) {

               if (isset($past['time_slot'])) {
                  $date_time = new DateTime($past['time_slot']);
                  $date = $date_time->format('d S F Y');
                  $time = $date_time->format('h:i A');
               }
               ?>
               <div class="row bookingRow pastBookingRow">
                  <div class="col-xs-12 col-sm-6 movieDetailSec">
                     <div class="movieBanner">
                        <picture class="img-responsive moviebannerImg">
                           <source srcset="<?php echo $past['portrait_image']; ?>" media="(min-width: 800px)">
                           <img srcset="<?php echo $past['portrait_image']; ?>" alt="…">
                        </picture>
                        <span class="bookingStatus past">Past</span>
                     </div>
                     <div class="movieDetail">
                        <h3 class="movieTitle"><?php echo $past['movie_name']; ?></h3>
                        <ul class="list-group scope-list-group detailList">
                           <?php
                           if (isset($past['time_slot'])) {
                              ?>
                              <li class="list-group-item list"><?php echo $date; ?> <span
                                     class="time"><?php echo $time; ?></span></li>
                              <?php
                           }
                           ?>
                           <li class="list-group-item list">In <?php echo $past['theater_name']; ?></li>
                           <li class="list-group-item list">Ref No - <?php echo $past['reference_no']; ?></li>
               <?php
               if (isset($past['all_labels'])) {
                  ?>
                  <li class="list-group-item list"><span class="title">No of seats -</span> <span
                         class="count"><?php echo count($past['all_labels']); ?></span>
                     <div class="seatsList">(
                        <?php
                        $label_array_past = array();
                        foreach ($past['all_labels'] as $lab_past) {
                           $label_array_past[] = '<span class="seat">' . $lab_past['label'] . '</span>';

                        }
                        echo implode(',', $label_array_past);
                        ?>)
                     </div>
                  </li>
                  <?php
               }
                  ?>
                        </ul>
                     </div>
                  </div>
                  <div class="col-xs-12 col-sm-6 ticketDetailSec">
                     <div class="movieDetail">
                        <h4 class="ticketTitle">Ticket Type</h4>
                        <ul class="list-group scope-list-group detailList">
                           <?php
                           $tot_past = 0;
                           foreach ($past['price_details'] as $pri_past) {

                              $tot_past += $pri_past['qty'] * $pri_past['price'];

                              ?>
                              <li class="list-group-item list">
                                 <span class="listleft"><?php echo $pri_past['category']; ?> (<?php echo $pri_past['price'].' x '.$pri_past['qty']; ?>)</span><span class="listRight"><?php echo $pri_past['qty'] * $pri_past['price']; ?> LKR</span>
                              </li>
                              <?php
                           }
                           ?>
                           <li class="list-group-item list totalList">
                              <span class="listleft">Discount</span><span class="listRight"><?php echo $past['total_discount_amount']; ?> LKR</span>
                           </li>
                           <li class="list-group-item list totalList">
                              <span class="listleft">Total</span><span class="listRight"><?php echo ($tot_past - $past['total_discount_amount']); ?> LKR</span>
                           </li>
                        </ul>
                     </div>
                  </div>
               </div>
               <?php
            }
            ?>


            </div>
         </div>
      </div>
   </div>






<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>