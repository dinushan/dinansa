/**
 * Created by LAYOUTindex on 3/21/2017.
 */

(function (window,$) {

    var socketMonitor ={};

    var socket = io(window.socket_url,{secure: true});
    //var socket = io('http://dev2.appslanka.com:3000');


    socket.on('connect', function (client) {
        socket.emit('handshake','hwllo');

    });


    socket.on('disconnec',function (client) {
        socketMonitor.onDisconnect()
    });

    socket.on('reconnect',function (client) {
        socket.emit('handshake',window.booking_id);
    });

    socket.on('sendReserverSeats',function (data) {

        socketMonitor.onRecive(data);
    });

    socket.on('Test',function (data) {
        console.log(data);
    });

    socketMonitor.seatHold = function(booking_id,movie_id,theater_id,time_slot,label,status){

        socket.emit('seatReserve',{
            'booking_id' :booking_id,
            'movie_id'   :movie_id,
            'theater_id' :theater_id,
            'label'      :label,
            'time_slot'  :time_slot,
            'status'     :status
        });
    };

    socketMonitor.getReservedSeats = function (booking_id,movie_id,theater_id,time_slot) {

        socket.emit('getsReservedSeats',{
            'booking_id' :booking_id,
            'movie_id'  :movie_id,
            'theater_id':theater_id,
            'time_slot' :time_slot
        });
    };


    socketMonitor.onRecive = function (param){

    };

    socketMonitor.onDisconnect = function(){

    };


    window.socketMonitor = socketMonitor;

}(window,$))
