<!-- BEGIN CONTAINER -->

<?php 
$yesno = unserialize(YESNO);
?>

<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title">Add User Group</h3>
			<div class="page-bar">				
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<a class="btn default" href="<?php echo site_url('admin/users/groups') ?>"><i class="fa fa-chevron-left"></i> Back</a>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div> <?php echo get_message() ?> </div>
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet light bordered">
						<div class="portlet-body form"> 
							<!-- BEGIN FORM-->
							<?php                    
							$attr = array("id"=>"edit_form","class"=>"form-horizontal","enctype"=>"multipart/form-data");
							echo form_open(current_url(),$attr);
							?>
							<div class="form-group">
								<label class="control-label col-md-3">Group Name<span class="req">*</span></label>
								<div class="control-element col-md-4">
									<input type="text" class="form-control required-entry" value="<?php echo set_value('name') ?>" name="name" />
									<?php echo form_error('name'); ?></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Description</label>
								<div class="control-element col-md-4">
									<textarea class="form-control" name="description" cols="40" rows="4"><?php echo set_value('description') ?></textarea>									
									<?php echo form_error('description'); ?></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Roles</label>
								<div class="control-element col-md-9">									
									<?php echo form_multiple_checkbox('roles[]', $roles, set_value('roles[]'),'class="list-unstyled roles multiple_checkbox"') ?>									
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Members</label>
								<div class="control-element col-md-4"><?php echo form_multiselect('members[]', $users, set_value('members[]'),'class="form-control"') ?></div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
									</div>
								</div>
							</div>
							<script type="text/javascript">							 
								$('.required-entry').attr('required','required');					
								$('#edit_form').validate({
									errorElement: 'span', 
									errorClass: 'validation-advice', 
									submitHandler: function (form) {
										form.submit();										
									}
								});									
							</script>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT--> 
	</div>
	<!-- END CONTENT --> 
	
</div>
<!-- END CONTAINER -->
