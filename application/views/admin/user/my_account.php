<?php 
$yesno = unserialize(YESNO);
?>
<!-- BEGIN CONTAINER -->

<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title">My Account</h3>
			<div class="row">
				<div class="col-md-12"> <?php echo get_message() ?> 
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet light bordered">
						<div class="portlet-body form"> 
							<!-- BEGIN FORM-->
							<?php if( ! empty($user)):
                                $image = $user->picture;
							$attr = array("id"=>"edit_form","class"=>"form-horizontal","enctype"=>"multipart/form-data");
							echo form_open(current_url(),$attr);
															
							?>
							<div class="form-group">
								<label class="control-label col-md-3">Username</label>
								<div class="control-element col-md-4">
									<label class="control-label"><?php echo $user->username ?></label>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Full Name</label>
								<div class="control-element col-md-4">
									<label class="control-label"><?php echo $user->fullname ?></label>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Email</label>
								<div class="control-element col-md-4">
									<label class="control-label"><?php echo $user->email ?></label>
								</div>
							</div>
                                <?php if ( $image != ''): ?>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Profile Picture <span class="req">*</span></label>

                                    <div class="control-element col-md-4">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="width: 100px; background-color: #fff;"><img
                                                        src="<?php echo base_url() . PROFILE_IMG_PATH . $image ?>?<?php echo time() ?>"
                                                        alt="" /></td>
                                                <td>&nbsp;</td>
                                                <td><?php list($img_width, $img_height) = getimagesize(PROFILE_IMG_PATH . $image); ?>
                                                    <strong>Image Info</strong><br/>
                                                    width:<?php echo $img_width ?>px<br/>
                                                    height:<?php echo $img_height ?>px
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">&nbsp;
                                                    <input type="hidden" name="image" value="<?php echo $image ?>"/></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"><label>
                                                        <input name="img_del" type="checkbox" value="1"/>
                                                        Image Delete </label>

                                                    <div class="help-block font-green">If you want to upload a image, first
                                                        delete the existing file.
                                                    </div>
                                            </tr>
                                            <tr>
                                                <td colspan="3">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Profile Picture <span class="req">*</span></label>

                                    <div class="control-element col-md-4"><input type="file" class=" input-file" name="image">

                                <span class="help-block font-green">Image Size:<?php echo PROFILE_IMG_W ?>
                                    px &times; <?php echo PROFILE_IMG_H ?>px, Image Type: png</span>
                                    </div>
                                </div>
                            <?php endif; ?>

							<div class="form-group">
								<div class="control-element col-md-offset-3 col-md-9"><strong>Only enter a password if you would like to set a new one</strong></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">New Password</label>
								<div class="control-element col-md-4">
									<input class="form-control" type="password" value="" name="password" />
									<?php echo form_error('password'); ?></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Confirm New Password</label>
								<div class="control-element col-md-4">
									<input class="form-control" type="password" value="" name="password_conf" />
									<?php echo form_error('password_conf'); ?></div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
                                        <input type="hidden" name="action" value="submit" />
										<button type="submit" class="btn green">Submit</button>
									</div>
								</div>
							</div>
							<?php endif; ?>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT--> 
	</div>
	<!-- END CONTENT --> 
	
</div>
<!-- END CONTAINER --> 

