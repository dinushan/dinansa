<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer moviesPage">
	<div class="row">
		<div class="col-xs-12 breadcrumbCont">
			<?php include(APPPATH.'views/frontend/inc/breadcrumb.php'); ?>
		</div>

		<?php
        if(count($arr_now_showing_movies) >0)
        {
        ?>
        <div class="col-xs-12 headWrap">
            <h2 class="heading">In Theaters now</h2>
        </div>
        <?php
            foreach ($arr_now_showing_movies as $movie)
            {
		?>
			<div class="col-xs-12 col-sm-6 col-md-4 moviesCont">
				<div class="moviesWrap">
					<picture class="img-responsive coverImage">
						<source srcset="<?php echo $movie['portrait_image'] ?>"
								media="(min-width: 800px)">
						<img srcset="<?php echo $movie['portrait_image'] ?>" alt="…">
					</picture>
					<a href="<?php echo base_url('movie/'.$movie['url_key']); ?>" class="imageOverlay"></a>
					<div class="captionWidgetWrap">
						<div class="captionTitleWrap">
							<h3 class="captionTitle"><?php echo $movie['movie_name'] ?></h3>
						</div>
						<div class="releaseDate">Now screening </div>

						<div class="iconContainer clearfix">
							<div class="iconWrapper">
								<ul class="iconWrap">
									<li><a href="<?php echo $movie['trailer'] ?>" class="youTubePopUp"><i class="icon fa fa-youtube-play"
																			aria-hidden="true"></i><span
												class="icoText">Trailer</span></a></li>
									<li><a href="<?php echo base_url('movie/'.$movie['url_key']); ?>"><i class="icon fa fa-info" aria-hidden="true"></i><span
												class="icoText">Info</span></a></li>
									<li><a onclick="return getMovieBookButton()" href="<?php echo base_url('buy-tickets-online/').'?movie='.$this->encrypt_lib->encode($movie['movie_id'])  ?>" class="btn btn-scope">Book</a></li>
								</ul>
								<ul class="linksWrap">
									<?php
                                    //print_r($movie);

                                    foreach ($movie['theater'] as $theater) {
										?>
                                        <li><span><?php echo $theater['cinema_name']; ?></span></li>
										<?php
									}
									?>
								</ul>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php
            }
        }
        ?>

	</div>
</div>

<?php if(count($arr_upcoming_movies) > 0): ?>
<div class="pageBottomSection">
	<div class="pageBottomContainer container">
		<div class="row">
			<div class="col-xs-12 headWrap">
				<h2 class="heading">Coming to theaters</h2>
			</div>

		<?php
		foreach ($arr_upcoming_movies as $movie)
		{
		?>
			<div class="col-xs-12 col-sm-6 col-md-4 moviesCont comingMoviesCont">
				<div class="moviesWrap">
					<picture class="img-responsive coverImage">
						  <source srcset="<?php echo $movie['portrait_image'] ?>" media="(min-width: 800px)">
						  <img srcset="<?php echo $movie['portrait_image'] ?>" alt="…">
					</picture>
					<a href="<?php echo base_url('movie/'.$movie['url_key']); ?>" class="imageOverlay"></a>
						<div class="captionWidgetWrap">
							<div class="captionTitleWrap">
								<h3 class="captionTitle"><?php echo $movie['movie_name'] ?></h3>
							</div>
                           
							<div class="releaseDate">In cinemas <?php
								$timestamp = strtotime($movie['date_release']);
								$newDate = date('d S F', $timestamp);
								echo $newDate;  ?></div>
							<div class="iconContainer clearfix">
								<div class="iconWrapper">
									<ul class="iconWrap">
										<li><a href="<?php echo $movie['trailer'] ?>" class="youTubePopUp"><i class="icon fa fa-youtube-play" aria-hidden="true"></i><span class="icoText">Trailer</span></a> </li>
										<li><a href="<?php echo base_url('movie/'.$movie['url_key']); ?>"><i class="icon fa fa-info" aria-hidden="true"></i><span class="icoText">Info</span></a> </li>
										<?php
										if($movie['booking_start_date'] != null && $movie['booking_start_date'] <= date('Y-m-d')) {
											?>
											<li>
												<a onclick="return getMovieBookButtonComing()" href="<?php echo base_url('buy-tickets-online/') . '?movie=' . $this->encrypt_lib->encode($movie['movie_id']) ?>"
												   class="btn btn-scope">Book</a></li>
											<?php
										}
											?>
									</ul>

								</div>
							</div>
						</div>
					</div>
				</div>
		<?php
			}
		?>
				
			</div>
	</div>
</div>
<?php endif; ?>
<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>