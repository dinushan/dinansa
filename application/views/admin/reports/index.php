<!-- BEGIN CONTAINER -->

<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title"><?php echo $title ?></h3>
            <?php if($this->adminauth->has_role('1102')):?>

            <div class="row">
                <div class="col-md-12">

                    <form class="form-horizontal" method="POST" action="<?php echo current_url()?>">
                        <div class="portlet box blue portlet-filter">
                            <div class="portlet-title">
                                <div class="caption">Filter</div>
                                <div class="tools">
                                    <button type="submit" class="btn btn-danger btn-sm">Show Report</button>
                                </div>
                            </div>
                            <div class="portlet-body ">
                                <div class="form-body clearfix" style="max-width:500px;">

                                    <div class="form-group">
                                        <div class="col-md-3"><label class="control-label" for="sales_report_period_type">Movie</label></div>
                                        <div class="col-md-9">
                                            <select class="form-control input-sm" name="movie" id="movie368">
                                                <option value="-1">any</option>
                                                <?php foreach ($movies as $mv):?>
                                                    <option value="<?php echo $mv->id;?>" <?php echo ($movie == $mv->id)?'selected':''?> ><?php echo $mv->movie_name;?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3"><label class="control-label" for="sales_report_period_type">Theater</label></div>
                                        <div class="col-md-9" id="match-div">
                                            <select class="form-control input-sm" name="theater">
                                                <option value="-1">any</option>
                                                <?php foreach ($theaters as $th): ?>
                                                    <option value="<?php echo $th->id?>" <?php echo ($theater==$th->id)?'selected':'' ?> >
                                                        <?php echo $th->name; ?>
                                                    </option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-3"><label class="control-label" for="report_from">From <span class="required">*</span></label></div>
                                        <div class="col-md-9"><input id="report_from" type="text" class="form-control form-control-inline input-medium date-picker input-sm" name="from" placeholder="From" value="<?php echo $from?>"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-3"><label class="control-label" for="report_to">To <span class="required">*</span></label></div>
                                        <div class="col-md-9"><input id="report_to" type="text" class="form-control form-control-inline input-medium date-picker  input-sm" name="to" placeholder="To" value="<?php echo $to ?>"></div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </form>

                    <div class="portlet light bordered">
                        <div class="dataTables_wrapper no-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <button id="exportCVS1910" type="button" class="btn btn-default pull-right">Export to Excel</button>
                                </div>
                            </div>
                            <div class="table-container">
                                <?php $tot_tax_amount = 0; ?>

                                <div class="clearfix"></div>
                                <div class="table-scrollable">
                                    <table id="tbl-report" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline">
                                        <thead>
                                        <tr class="headings">
                                            <th>DATE</th>
                                            <th>SCREEN</th>
                                            <th>MOVIE NAME</th>
                                            <th>TOTAL PATRONS</th>
                                            <th>TOTAL AMOUNT</th>
                                            <th>DR COLLECTION</th>
                                            <th>SUPPLIER NAME</th>
                                            <th>THEATER SHARE</th>
                                            <th>DISTRIBUTION COMM</th>
                                            <th>SUPLIER SHARE</th>
                                            <th>FILM LEVY</th>
                                            <th>ENT TAX</th>
                                            <th>FILM LEVY 3.4</th>
                                            <th>ADDITIONAL 50</th>
                                            <th>COMPLIMENTORY TICKET</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php foreach($results as $result):?>

                                            <?php

                                            $entertainment     = ($result->film_levy * $result->entartainment_tax)/110;

                                            $theater_share     = ($result->dr_collection * $result->movie_hall_rate)/100;
                                            $distributor_comm  = ($result->dr_collection * $result->distributor_rate)/100;
                                            $supplier_share    = ($result->dr_collection * $result->supplier_rate)/100;

                                            $ent_tax           = ($result->dr_collection * $result->supplier_rate)/100;





                                            $theater_share      < 0 ? $theater_share      = 0 : $theater_share;
                                            $distributor_comm   < 0 ? $distributor_comm   = 0 : $distributor_comm;
                                            $supplier_share     < 0 ? $supplier_share     = 0 : $supplier_share;
                                            $entertainment      < 0 ? $entertainment      = 0 : $entertainment;

                                            ?>
                                            <tr>
                                                <td><?php echo $result->date; ?></td>
                                                <td><?php echo $result->theater_name; ?></td>
                                                <td><?php echo $result->movie_name; ?></td>
                                                <td><?php echo $result->total_patrons; ?></td>
                                                <td><?php echo number_format($result->ticket_total,2); ?></td>
                                                <td><?php echo number_format($result->dr_collection,2); ?></td>
                                                <td><?php echo $result->suplier_name; ?></td>
                                                <td><?php echo number_format($theater_share,2); ?></td>
                                                <td><?php echo number_format($distributor_comm,2); ?></td>
                                                <td><?php echo number_format($supplier_share,2); ?></td>
                                                <td><?php echo number_format($result->film_levy,2); ?></td>
                                                <td><?php echo number_format($entertainment,2); ?></td>
                                                <td><?php echo number_format($result->film_levy_3_4,2); ?></td>
                                                <td><?php echo number_format($result->online_charge,2); ?></td>
                                                <td><?php echo ($result->complimentory); ?></td>
                                            </tr>
                                            <?php
                                        endforeach;
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div><!-- End Table Container-->
                        </div>
                    </div>
                </div>
            </div>
            <script>
                var el = $(".portlet-body");

                $('#tournament').change(function(){

                    Metronic.blockUI({
                        target: el,
                        animate: true,
                        overlayColor: 'none'
                    });
                    var tournament_id = $(this).val();

                    $.post('<?php echo base_url('reports/municipal_council/ajax_get_matches') ?>',{tournament_id:tournament_id},function(html)
                    {
                        $('#match-div').html(html);
                        Metronic.unblockUI(el);
                    });


                });


                $('.required-entry').attr('required', 'required');
                $('#edit_form').validate({
                    errorElement: 'span',
                    errorClass: 'validation-advice',
                    ignore: "",
                    submitHandler: function (form) {
                        form.submit();
                    }
                });


                $('.date-picker').datepicker({
                    format: "yyyy-mm-dd",
                    autoclose: true
                });


            </script>
        </div>

        <script>
            jQuery(document).ready(function() {
                TableAjax.init();
            });
        </script>

        <?php endif; ?>

        <!-- END PAGE CONTENT-->
        <!-- END CONTENT -->

    </div>
    <!-- END CONTAINER -->
    <script>
        window.param ={
            'theater':'<?php echo $theater; ?>',
            'movie' : '<?php echo $movie; ?>',
            'from' :'<?php echo $from; ?>',
            'to' :'<?php echo $to; ?>'
        };
        (function (window,$) {
            $('#exportCVS1910').click(function(){
                var urlenc = [];
                if (window.param.theater.trim() != ''){
                    urlenc.push('theater='+window.param.theater);
                }
                if (window.param.movie.trim() != ''){
                    urlenc.push('movie='+window.param.movie);
                }
                if (window.param.from.trim() != ''){
                    urlenc.push('from='+window.param.from);
                }
                if (window.param.to.trim() != ''){
                    urlenc.push('to='+window.param.to);
                }


                var myWindow = window.open(window.base_url+'admin/reports/financial-report/export?'+urlenc.join('&'));
            });
        }(window,$))
    </script>