<?php

$lang['login_failed'] = "Wrong username or password.";
$lang['account_disabled'] = "Your account is disabled. Please contact administrator.";

$lang['error_top'] = "Please fix the following errors.";

$lang['pos_user_created'] = "User has created successfully.";
$lang['pos_user_updated'] = "User has updated successfully.";
$lang['pos_user_enabled'] = "User has enabled successfully.";
$lang['pos_user_disabled'] = "User has disabled successfully.";
$lang['password_updated'] = "Your password has changed successfully.";

$lang['settings_updated'] = "App setting has updated successfully.";

$lang['messageUpdate'] = 'Update Successful';
$lang['messageAdd'] = 'Added Successful';
$lang['messageDelete'] = 'Deleted Successful';
$lang['messageSomethingWentWrong'] = 'Something went wrong. Please contact your administrator';
$lang['messageNoPermission'] = 'You don\'t have permission to proceed';


$lang['user_group_create_success'] = 'User group created successfully!';
$lang['user_group_create_error'] = 'User group create failed!';
$lang['user_group_update_success'] = 'User group updated successfully!';
$lang['user_group_update_error'] = 'User group update failed!';
$lang['user_group_delete_success'] = 'User group deleted successfully!';
$lang['user_group_delete_error'] = 'User group delete failed!';

$lang['requisition_inserted'] = "Storeroom requisition has added successfully!";
$lang['requisition_updated'] = "Storeroom requisition has updated successfully!";
$lang['requisition_completed'] = "Storeroom requisition has completed successfully!";

$lang['onloan_item_inserted'] = "On-loan item has added successfully!";
$lang['onloan_item_updated'] = "On-loan item has updated successfully!";
$lang['onloan_items_lent'] = "On-loan items has lent successfully!";
$lang['onloan_items_returned'] = "On-loan items has returned successfully!";

$lang['member_activated'] = "Member activated successfully!";
$lang['member_deactivated'] = "Member deactivated successfully!";



