/**
 * Created by LAYOUTindex on 12/4/2017.
 */
(function(window) {
    var app = angular.module('restaurant');

    app.controller('reportController', reportController);
    reportController.$inject = ['$scope','$window','$http','reportService'];

    function reportController($scope,$window,$http,reportService) {

        $scope.store_res = [];
        $scope.dep_res = [];

        $scope.loctypes = [
            {'id':-1,'name':'Any'},
            {'id':1,'name':'Store Rooms'},
            {'id':2,'name':'Department'}
        ];

        $scope.loctype  = $scope.loctypes[0];

        $scope.locations  = [];
        $scope.categories = [];
        $scope.items      = [];



        $scope.location   = {'id':-1,'name':'Any'};
        $scope.category   = {'id':-1,'category_name':'Any'};
        $scope.item       = {'id':-1,'item_name':'Any','stok_keep_unit':'A000000'};

        $scope.exportlink = $window.filterurl+'/export?type='+$scope.loctype.id+'&location='+$scope.location.id+'&category='+$scope.category.id+'&item='+$scope.item.id;


        $scope.changeType     = function (){

            if (this.loctype.id == -1){

                $scope.categories   = [];
                $scope.category     = {'id':-1,'category_name':'Any'};

                $scope.locations    = [];
                $scope.location     = {'id':-1,'name':'Any'};

                $scope.items        = [];
                $scope.item         = {'id':-1,'item_name':'Any','stok_keep_unit':'A000000'};
                return;
            }
            reportService.selecttype(this.loctype.id,function (data) {


                    $scope.locations  = data;
                    $scope.location   = data[0];
                    $scope.categories = [];
                    $scope.items      = [];

                    $scope.category = {'id':-1,'category_name':'Any'};
                    $scope.item     = {'id':-1,'item_name':'Any','stok_keep_unit':'A000000'};

            });


        };

        $scope.changeLocation = function (){

            if (this.location == null){
                return;
            }

            if ($scope.loctype.id == -1 || this.location.id == -1){

                $scope.items      = [];
                $scope.item       = {'id':-1,'item_name':'Any','stok_keep_unit':'A000000'};
                return;
            }

            $scope.location  = this.location;
            reportService.selectLocation($scope.loctype.id,this.location.id,function(data){

                    $scope.categories   = data;
                    $scope.category     = $scope.categories[0];
                    $scope.items        = [];
                    $scope.item         = {'id':-1,'item_name':'Any','stok_keep_unit':'A000000'};

            });


        };

        $scope.changeCategory = function (){


            if (this.category == null ||$scope.loctype.id == -1|| $scope.location.id == -1||this.category.id == -1) {

                $scope.items      = [];
                $scope.item     = {'id':-1,'item_name':'Any','stok_keep_unit':'A000000'};
                return;
            }
            reportService.selectCategory($scope.loctype.id,$scope.location.id,this.category.id,function(data){


                    $scope.items      = data;
                    $scope.item     = $scope.items[0];
            });



        };



        $scope.filter     = function (){


            $http({
               'method':'POST',
                'url':$window.filterurl,
                'headers':{'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'},
                'data':$.param({
                   'type'     : $scope.loctype.id,
                   'location' : $scope.location.id,
                   'category' : $scope.category.id,
                   'item'     : $scope.item.id
                })
            })
            .then(function(response){

                $scope.store_res = response.data.stores;
                $scope.dep_res   = response.data.departments;

                $scope.exportlink = $window.filterurl+'/export?type='+$scope.loctype.id+'&location='+$scope.location.id+'&category='+$scope.category.id+'&item='+$scope.item.id;
            })
            .catch(function (err) {

            });
        };
    }
}(window));