<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>
<script>
    window.theaters_and_time = <?php echo json_encode($theaters_and_times)?>;
    window.movie = <?php echo "'".$this->encrypt_lib->encode($movieData->id)."'"?>;

</script>


<script src="<?php echo base_url().'angular/moment.js'?>"></script>
<div class="container pageContainer detailPageContainer">
   <div class="row">
      <div class="col-xs-12 thetresCont padding-zero-mobile">

            <div class="col-xs-12 col-sm-7 col-md-8 col-lg-9">
           <div class="row">
                  <div class="col-xs-12 padding-zero-mobile">
                      <?php
                      if(count($movieImages) == '1')
                      {
                      ?>
                      <div class="singleImage">
                          <img src="<?php echo base_url(MOVIE_IMG_PATH . $movieImages_single->image);?>" alt="">
                      </div>
                          <?php

                      }
                      else {
                          ?>
                     <div class="movieSliderWrapper camera_wrap">

                         <?php
                             foreach ($movieImages as $row):
                                 ?>
                                 <div data-src="<?php echo base_url(MOVIE_IMG_PATH . $row['image']) ?>"></div>
                                 <?php
                             endforeach;
                         ?>
                     </div>
                      <?php
                      }
                      ?>
                  </div>
                  <div class="col-xs-12 visible-xs">
                     <div class="movieDetailWrap">

                        <h1 class="movieTitle"><?php echo $movieData->movie_name ?> <span class="year"><?php echo $movieData->movie_year ?></span></h1>
                        <div class="movieDuration"><?php
                            if($movieData->duration_h != 0){
                                echo $movieData->duration_h.' hour ';
                            }
                            if($movieData->duration_m != 0){
                                echo $movieData->duration_m.' minutes';
                            }
                            ?></div>
                        <div class="availablity">In theatres now</div>
                     </div>

                      <?php
                      //var_dump($movies_theater_details['theater']);exit;
                      $count= count($movies_theater_details['theater']);
                      $i = 0;
                      foreach ($movies_theater_details['theater'] as $n_slots) {
                          ?>
                <div class="theatresDetailWrap">
                    <?php
                    if($i == 0)
                    {
                        echo ' <h1 class="showingTitle">Now showing at</h1>';
                    }
                    ?>
                    <img class="theatresLogo img-responsive" src="<?php echo $n_slots['image']; ?>" alt="majestic"/>
                </div>
                <div class="showtimeWrap">
                    <h2 class="theatresTitle"><?php echo $n_slots['cinema_name'] ?></h2>
                    <h3 class="showTimeTitle">Available show times</h3>
                    <ul class="clearfix showingTimeWrap">
                        <?php
                        foreach ($n_slots['show_time'] as $time) {
                            ?>
                            <li class="showTime"><?php echo $time['time_slot']; ?></li>
                            <?php
                        }
                        ?>
                    </ul>
                    <?php
                    if($i == $count - 1) {
                        echo '<a href="'.$movies_theater_details['trailer'].'" title="trailer"
                                 class="btn btn-scope btn-gray watchTrailer">Watch official trailer now</a>';
                    }
                    ?>
                </div>
                <?php
            }
            ?>

                     <div class="ratingWrap">
                        <span class="ratingTitle">IMDB Rating</span><span class="starIcon"><i class="fa fa-star" aria-hidden="true"></i></span><span class="ratingVal"><?php echo $movies_theater_details['imdb_rate'] ?>/10</span>
                     </div>
                      <?php if (strtotime($n_slots['start_date']) <= strtotime("now")):?>
                      <div class="ratingWrap">
                          <a href="<?php echo base_url('buy-tickets-online/') . '?movie=' . $this->encrypt_lib->encode($movieData->id); ?>" class="btn btn-scope">
                              Buy Tickets
                          </a>
                      </div>
                      <?php endif; ?>
                  </div>
                  <div class="col-xs-12 breadcrumbCont">
                      <div class="breadcrumbWrap">
                          <ol class="breadcrumb">
                              <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                              <li><a href="<?php echo $sitePageData->url_key; ?>"><?php echo $sitePageData->menu; ?></a></li>
                              <li class="active"><a href="<?php echo current_url(); ?>"><?php echo $movieData->movie_name; ?></a></li>
                          </ol>
                      </div>
                      
                  </div>
                  <div class="col-xs-12 headWrap">
                     <h2 class="heading secHeading">Storyline</h2>
                  </div>
                  <div class="col-xs-12 detailWrap">
                     <div class="storyDetail">
                        <p><?php echo $movieData->movie_content ?></p>
                     </div>
                  </div>
                  <div class="col-xs-12">
                     <div class="genresWrap">
                        <span class="genresTitle">Genres:</span>
                        <ul class="genresList">
                            <?php foreach($movieGenres as $row):?>
                            <li class="genres"><?php echo $row['genre'] ?></li>
                            <?php endforeach; ?>
                        </ul>
                     </div>
                  </div>
                  <div class="col-xs-12 castListDetail">
                     <table class="castListTable">
                        <thead>
                           <tr>
                              <th colspan="2">Cast</th>
                           </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td><strong>Actor</strong></td>
                            <td><strong>Character</strong></td>
                        </tr>
                        </tr>
                            <?php
                            $arr_cast = unserialize($movieData->cast);

                            foreach($arr_cast as $r):
                            ?>
                           <tr>
                              <td><?php echo $r['actor'] ?></td>
                              <td><?php echo $r['character'] ?></td>
                           </tr>
                            <?php
                            endforeach;
                            ?>
                        </tbody>
                     </table>
                  </div>

                   <div class="col-xs-12 castListDetail">
                       <table class="castListTable">
                           <thead>
                           <tr>
                               <th colspan="2">TEAM</th>
                           </tr>
                           </thead>
                           <tbody>
                           <?php
                           $directed_by = $movieData->directed_by;
                           $produced_by = $movieData->produced_by;
                           $written_by = $movieData->written_by;
                           $music_by = $movieData->music_by;
                           $array = array(array($directed_by,"Directed by"),array($produced_by,'Produced by'),array($written_by,'Written by'),array($music_by,'Music by'));


                           foreach($array as $v):
                               $u = unserialize($v[0]);
                           if(count($u) > 0):
                               ?>
                               <tr>
                                   <td><strong><?php echo $v[1] ?></strong></td>
                                   <td>
                                       <?php
                                       foreach($u as $r){
                                           echo $r['name'].'<br />';
                                       }
                                       ?>
                                   </td>
                               </tr>
                           <?php
                           endif;
                           endforeach;
                           ?>


                           </tbody>
                       </table>
                   </div>
             </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-4 col-lg-3 pageRightSide">
             <div class="row">
                  <div class="col-xs-12 hidden-xs">
                     <div class="movieDetailWrap">
                        <h1 class="movieTitle"><?php echo $movieData->movie_name ?> <span class="year"><?php echo $movieData->movie_year ?></span></h1>
                        <div class="movieDuration"><?php
                            if($movieData->duration_h != 0){
                                echo $movieData->duration_h.' hour ';
                            }
                            if($movieData->duration_m != 0){
                                echo $movieData->duration_m.' minutes';
                            }
                            ?></div>
                         <?php
                        if (isset($movies_theater_details['theater'][0]["start_date"])):
                         $start_date = $movies_theater_details['theater'][0]["start_date"];
                            if (strtotime($start_date) <= strtotime("now")): ?>
                                <div class="availablity">In theatres now</div>
                        <?php
                            endif;
                         endif;
                        ?>
                     </div>

                      <?php
                     
                      $count= count($movies_theater_details['theater']);
                      $i = 0;
                      foreach ($movies_theater_details['theater'] as $n_slots) {
                          ?>
                          <div class="theatresDetailWrap">
                              <?php
                              if($i == 0)
                              {
                                  if (strtotime($n_slots['start_date']) <= strtotime("now")){
                                      echo ' <h1 class="showingTitle">Now showing at</h1>';
                                  }else{
                                      echo ' <h1 class="showingTitle">Coming to</h1>';
                                  }

                              }
                              ?>
                              <img class="theatresLogo img-responsive" src="<?php echo $n_slots['image']; ?>" alt=""/>
                          </div>
                          <div class="showtimeWrap">
                              <h2 class="theatresTitle"><?php echo $n_slots['cinema_name'] ?></h2>
                              <h3 class="showTimeTitle">Available show times</h3>
                              <ul class="clearfix showingTimeWrap">
                                  <?php
                                  foreach ($n_slots['show_time'] as $time) {
                                      ?>
                                      <li class="showTime"><?php echo $time['time_slot']; ?></li>
                                      <?php
                                 }
                                  ?>
                              </ul>

                              <?php
                              if($i == $count - 1) {
                                  echo '<a href="'.$movies_theater_details['trailer'].'" title="trailer"
                                 class="btn btn-scope btn-gray watchTrailer">Watch official trailer now</a>';
                              }
                              ?>
                          </div>
                          <?php
                        $i++;
                      }
                      ?>

                     <div class="ratingWrap">
                        <span class="ratingTitle">IMDB Rating</span><span class="starIcon"><i class="fa fa-star" aria-hidden="true"></i></span><span class="ratingVal"><?php echo $movies_theater_details['imdb_rate'] ?>/10</span>
                     </div>
                      <?php

                      if ($movies_theater_details['booking_start_date'] !== null && strtotime($movies_theater_details['booking_start_date']) <= strtotime("now")): ?>
                      <div class="ratingWrap">

                          <a href="<?php echo base_url('buy-tickets-online/') . '?movie=' . $this->encrypt_lib->encode($movieData->id); ?>" class="btn btn-scope">
                              Buy Tickets
                          </a>

                      </div>
                      <?php endif;?>
                  </div>
                  <div class="col-xs-12">
                      <?php
                      foreach ($advertisement_images as $add) {
                          $image = $add['web_view_image'];
                          ?>
                          <div class="advertisementWrap">
                              <img class="advertisement" src="<?php echo base_url(ADVERTISEMENT_IMG_PATH.$image); ?>"/>
                          </div>
                          <?php
                      }
                      ?>

                  </div>
             </div>
            </div>


      </div>

       <?php
       
       if(count($other_movies) > 0)
       {
       ?>

          <div class="col-xs-12 otherMoviesAtTheatre padding-zero-mobile">
               <div class="col-xs-12 headWrap">
                  <h3 class="heading otherMoviesTitle">Other movies now showing at theatre</h3>
               </div>

              <?php
              foreach ($other_movies as $other) {
                  ?>
                  <div class="col-xs-12 col-sm-6 col-md-4 moviesCont">
                      <div class="moviesWrap">
                          <picture class="img-responsive coverImage">
                              <source srcset="<?php echo $other['portrait_image'] ?>" media="(min-width: 800px)">
                              <img srcset="<?php echo $other['portrait_image'] ?>" alt="…">
                          </picture>
                          <a href="#" class="imageOverlay"></a>
                          <div class="captionWidgetWrap">
                              <div class="captionTitleWrap">
                                  <h3 class="captionTitle"><?php echo $other['movie_name'] ?></h3>
                              </div>
                              <div class="releaseDate">Release <?php
                                  $timestamp = strtotime($other['date_release']);
                                  $newDate = date('d S F Y', $timestamp);
                                  echo $newDate; ?></div>
                              <div class="iconContainer clearfix">
                                  <div class="iconWrapper">
                                      <ul class="iconWrap">
                                          <li><a href="<?php echo $other['trailer'] ?>" class="youTubePopUp"><i class="icon fa fa-youtube-play" aria-hidden="true"></i><span class="icoText">Trailer</span></a></li>
                                          <li><a href="<?php echo base_url('movie/'.$other['url_key']); ?>"><i class="icon fa fa-info" aria-hidden="true"></i><span class="icoText">Info</span></a></li>
                                          <li><a href="<?php echo base_url('buy-tickets-online/'.$other['url_key']) ?>" class="btn btn-scope">Book</a></li>
                                      </ul>
                                      <ul class="linksWrap">
                                          <?php
                                          foreach ($other['theater'] as $theater) {
                                              ?>
                                              <li><a href="#"><?php echo $theater['name']; ?></a></li>
                                              <?php
                                          }
                                          ?>
                                      </ul>
                                  </div>
                              </div>
                          </div>
                      </div>
                  </div>

                  <?php
              }
              ?>

               </div>

       <?php
       }
       ?>

   </div>
</div>



<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>
<script>
   $(function(){
      $('.movieSliderWrapper').camera({
         navigation:false,
         playPause:false,
         time:10000,
         transPeriod: 2000,
         pauseOnClick: true,
         loader:'none',
         fx:'simpleFade'
      });


      $('.watchTrailer,.youTubePopUp').magnificPopup({
      disableOn: 700,
      type: 'iframe',
      mainClass: 'mfp-fade',
      removalDelay: 160,
      preloader: false,

      fixedContentPos: false,
        beforeShow:function(input, ins) {
                datePickerBeforeShow();
        },
        onClose:function(input, ins) {
                datePickerOnClose(input);
        }
   });
      $('.selectDate').datepicker({
            minDate: 0,
            dateFormat:'dd MM yy',
            beforeShow:function(input, ins) {
                    datePickerBeforeShow();
            },
            onClose:function(input, ins) {
                    datePickerOnClose(input);
            }

        }).focus(function () {
            $(this).blur()
          });
   });
</script>


<script>
    (function (window,$) {

        var ajax ={
            post:function (url,param,error,success) {
               $.ajax({
                   url:url,
                   data:param,
                   error:function(jqXHR,textStatus,errorThrown){
                       error(jqXHR,textStatus,errorThrown);
                       //render();
                   },
                   success :function (data,textStatus,jqXHR) {

                       success(data,textStatus,jqXHR);
                       //render();
                   }
               });
            } ,
            get:function (url,param,success,error) {
                $.get(url)
                .done(function (data,textStatus,jqXHR) {

                    success($.parseJSON(data),textStatus,jqXHR);
                    //render();
                }).fail(function (err) {
                    error(err);
                });
            }
        };

        $('#date_picker_689').change(function () {
            var url =window.base_url+'MovieDetailsController/onChangeDate/'+$('#date_picker_689').val()+'/'+window.movie;
            ajax.get(url,[],
                function (data,textStatus,jqXHR) {

                        

                    if (jqXHR.status == 200){

                        if (data.theaters.length > 0){

                            var html ="";

                            for (var i in data.theaters){
                                html += "<option value="+data.theaters[i].theater_id+">"+data.theaters[i].name+"</option> ";
                            }

                            $('#theater_selector_689').html(html);
                        }

                        var html2 ='';

                        for (var j in data.time_slots){
                            html2 += '<option value="'+data.time_slots[j].display+'">'+data.time_slots[j].display+'</option>';
                        }

                        $('#time_slot_689').html(html2);
                    }else{

                        console.log("No theaters available!");
                    }
                },
                function (jqXHR,textStatus,errorThrown) {

                }
            );
        });

        $('#theater_selector_689').change(function () {

            //console.log($('#theater_selector_689').val());
            var url =window.base_url+'MovieDetailsController/onChangeTheater/'+$('#date_picker_689').val()+'/'+window.movie+'/'+$('#theater_selector_689').val();

            ajax.get(url,[],function(data,textStatus,jqXHR){

                   if (jqXHR.status == 200){

                     var html ="";
                     for (var j in data.time_slots){
                        html += "<option value="+data.time_slots[j].display+">"+data.time_slots[j].display+"</option> ";
                      }

                       $('#time_slot_689').html(html);
                   }else if (jqXHR.status == 201){
                       var html ="<option value=''>-- No any time slot--</option>";
                       $('#time_slot_689').html(html);
                   }


            });
        });

    }(window,$));
</script>