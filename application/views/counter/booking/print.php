<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('counter/inc_sidebar_navigation.php'); ?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">
<?php if ($page_action == ''): ?>
    <h3 class="page-title">Counter bookings</h3>

    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <?php if(
                $this->counterauth->has_role('201')
                ):?>
                <a onclick="setLocation('<?php echo site_url('counters/' . $slug . '/add') ?>')" class="btn green"><i
                        class="fa fa-plus-circle"></i> Add New</a>
                <?php endif; ?>
            </div>
        </div>
    </div>





<?php endif;//$page_action ?>
</div>
<!-- END PAGE CONTENT-->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER --> 