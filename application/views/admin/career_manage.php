<link href="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.css" rel="stylesheet">
<script src="http://cdnjs.cloudflare.com/ajax/libs/summernote/0.8.8/summernote.js"></script>

<?php 
  $header      = null;
  $content     = null;
  $id          = null;
  $ispublished = 0;
  $summery  = null;
  
  if(isset($career) && $career){
    $header  = $career->title;
    $content = $career->content;
    $id      = $career->id;
    $ispublished =  $career->ispublished;
    $summery  = $career->summery;
  }
?>

<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <h3 class="page-title">Career Manage</h3>

            <div class="page-bar">
                <div class="page-toolbar">
                    <div class="btn-group pull-right">

                        <a onclick="setLocation('<?php echo site_url('admin/' . $slug ) ?>')" class="btn green">
                            <i class="fa fa-arrow-left"></i> Back</a>
                    </div>
                </div>
            </div>

            <div class="row">
                <form method="post" action="<?php echo base_url('admin/'. $slug.'/manage')?>" id="form_01">
                    <input type="hidden" value="<?php echo set_value('id', $id) ?>" name="id" />

                    <div class="col-md-6 col-xs-6">
                        <div class="form-inline">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" name="publish" value="1"<?php  echo $ispublished == 1? 'checked' :''?> /> Publish
                                </label>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6 col-xs-6">
                        <div class="btn-group pull-right">
                            <button type="submit" class="btn btn-lg red">Save</button>
                        </div>
                    </div>

                    <div class="col-md-12 col-xs-12">
                        <div class="col-md-1">Heading</div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <input type="text" value="<?php echo set_value('heading', $header) ?>" class="form-control" placeholder="Heading/Title" name="heading">
                            </div>
                        </div>
                    </div>


                        <div class="col-md-12 col-xs-12">
                             <div class="col-md-1">Summery</div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <textarea class="form-control" name="summery"><?php echo set_value('summery', $summery) ?></textarea>
                                </div>
                            </div>
                        </div>


                    
                    <div class="col-md-4 col-xs-4">
                        <?php echo form_error('heading'); ?>
                    </div>

                    <br/>
                    <div class="col-md-12 col-xs-12">
                        <div class="form-group">
                            <textarea id="summernote" name="contains">
                              <?php echo set_value('heading', $content) ?>
                            </textarea>
                        </div>
                    </div>


                </form>
            </div>

        </div>
    </div>
</div>

<script>
    $(document).ready(function() {
        $('#summernote').summernote({
            height: 400,
            focus: true,
            toolbar: [
                ['style', ['bold', 'italic', 'underline', 'clear']],
                ['font', ['strikethrough', 'superscript', 'subscript']],
                ['fontsize', ['fontsize']],
                ['color', ['color']],
                ['para', ['ul', 'ol', 'paragraph','height']],
                ['height', ['height']]
            ]
        }); 

    });
</script>