<style>
    .ps_3 td{
        border-color: #E08283 !important;
        background-image: none !important;
        background-color: #E08283 !important;
        color: #FFFFFF !important;
    }
</style>
<!-- BEGIN CONTAINER -->

<div class="page-container">
    <?php $this->load->view('counter/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <?php if ($page_action == ''):
                ?>
                <h3 class="page-title">Ticket Printing</h3>

                <div class="row">
                    <div class="col-md-12">
                        <?php echo get_message(); ?>
                        <div class="portlet light bordered">

                            <div class="portlet-body form clearfix">

                                <div class="row">
                                    <div class="col-md-3">

                                        <h3>Enter Invoice number:</h3>

                                        <form action="" id="edit_form" method="get">

                                            <div class="row">
                                                <label class="control-label col-md-12">Invoice number<span
                                                        class="req">*</span></label>

                                                <div class="control-element col-md-12" style="padding-bottom: 10px;">
                                                    <input name="invoice" type="text" class="form-control"
                                                           required="required" value="<?php echo $this->input->get('invoice') ?>"></div>
                                                <div class="col-md-12" style="padding-bottom: 10px;">
                                                    <button type="submit" class="btn btn-submit green">Submit</button>
                                                    <?php
                                                    if($invoice != ''):
                                                        $booking_id = $reservation_info->id;
                                                        $user_id = $this->counterauth->user_id;
                                                        $enc_id = $this->encrypt_lib->encode($booking_id.'||0||'.$user_id);
                                                    ?>
                                                    <a href="http://localhost/scope-cinema/print/ticket_print.php?invoice=<?php echo $invoice ?>&tid=<?php echo $enc_id ?>" class="btn blue">
                                                        <i class="fa fa-print"></i> Print All Tickets</a>
                                                    <?php endif; ?>

                                                </div>
                                            </div>


                                        </form>
                                    </div>

                                    <div class="col-md-9">

                                        <?php if($invoice != ''): ?>

                                        <div class="col-xs-12">
                                            <h3>Reservation Information:</h3>
                                            <ul class="list-unstyled">
                                                <li>
                                                    <strong>Customer:</strong> <?php echo $reservation_info->customer ?>
                                                </li>
                                                <li>
                                                    <strong>Mobile No:</strong> <?php echo $reservation_info->mobile_no ?>
                                                </li>
                                                <li>
                                                    <strong>Date of Purchase:</strong> <?php echo date('M d, Y h:i A',strtotime($reservation_info->purchase_date)) ?>
                                                </li>
                                                <li>
                                                    <strong>Mode of Purchase:</strong> <?php echo $reservation_info->payment_method ?>
                                                </li>
                                                <li>
                                                    <strong>Theater:</strong> <?php echo $reservation_info->theater_name ?>
                                                </li>
                                                <li>
                                                    <strong>Movie:</strong> <?php echo $reservation_info->movie_name ?>
                                                </li>
                                                <li>
                                                    <strong>Show Time:</strong> <?php echo date('M d, Y h:i A',strtotime($reservation_info->time_slot)) ?>
                                                </li>
                                            </ul>

                                            <h3>Ticket Information:</h3>
                                            <table class="table table-striped table-hover">
                                                <thead>
                                                <tr>
                                                    <th width="40">#</th>
                                                    <th width="100" class="hidden-480">Print Status</th>
                                                    <th width="62" class="hidden-480">Type</th>
                                                    <th width="62" class="hidden-480">Seat</th>
                                                    <th width="150" class="hidden-480">Price</th>
                                                    <th>Action</th>
                                                </tr>
                                                </thead>
                                                <tbody>

                                                <?php
                                                //$user_cinema_id = $this->counterauth->cinema;


                                                $printStatus = unserialize(TICKET_PRINT_STATUS);

                                                foreach($reservation_ticket_info as $k=>$r):

                                                    $id = $r['id'];
                                                    $counter_user_id = $r['counter_user_id'];
                                                    $print_status = $r['print_status'];


                                                    $enc_id = $this->encrypt_lib->encode($booking_id.'||'.$id.'||'.$user_id);

                                                ?>
                                                <tr>
                                                    <td><?php echo $r['serial_number'] ?></td>
                                                    <td class="hidden-480"><?php echo $printStatus[$r['print_status']] ?></td>
                                                    <td class="hidden-480"><?php echo $r['type'] ?></td>
                                                    <td class="hidden-480"><?php echo $r['label'] ?></td>
                                                    <td class="hidden-480">Rs. <?php echo number_format($r['price'],2) ?></td>
                                                    <td>
                                                        <a href="http://localhost/scope-cinema/print/ticket_print.php?invoice=<?php echo $invoice ?>&tid=<?php echo $enc_id ?>" class="btn btn-xs blue">
                                                            <i class="fa fa-print"></i> Print </a></td>
                                                    </td>

                                                </tr>

                                                <?php endforeach; ?>

                                                </tbody>
                                            </table>
                                        </div>

                                        <?php endif; ?>

                                    </div>
                                </div>

                                <script type="text/javascript">
                                    function reload()
                                    {
                                        document.location.reload(true);
                                        return true;
                                    }

                                    $('#edit_form').validate({
                                        errorElement: 'span',
                                        errorClass: 'validation-advice',
                                        ignore: "",
                                        submitHandler: function (form) {
                                            form.submit();
                                        }
                                    });
                                </script>

                            </div>
                        </div>


                    </div>
                </div>

                <script type="text/javascript">

                </script>

            <?php endif;//$page_action ?>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->


<div class="modal fade" id="loginModal" tabindex="-1" role="dialog" aria-labelledby="loginModalLabel">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title" id="loginModalLabel">Ticket Regenerate</h4>
            </div>
            <div class="modal-body">

                <div id="message-box"></div>
                <div class="form-group">
                    <label for="manager-username" class="control-label">Manager Username:</label>
                    <input type="text" class="form-control" id="manager-username" name="manager-username" value="" >
                </div>
                <div class="form-group">
                    <label for="manager-password" class="control-label">Manager Password:</label>
                    <input type="password" class="form-control" id="manager-password" name="manager-password" value="" >
                </div>

                <div class="form-group">
                    <label for="manager-username" class="control-label">Comment for Regenerate:</label>
                    <textarea class="form-control" id="comment" name="comment"></textarea>
                </div>

                <input type="hidden" id="t_id" name="t_id" value="">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                <button type="button" id="btn-login" class="btn btn-primary">Regenerate</button>
            </div>
        </div>
    </div>
</div>

<script type="text/javascript">
    var user_key = '';
    $('.btn-regenerate').click(function(){
        $('#loginModal').modal('show');
        $('#t_id').val($(this).data('id'));
    });

    $('#loginModal').on('hidden.bs.modal', function (e) {
        $('#manager-username').val('');
        $('#manager-password').val('');
    })

    $('#btn-login').on('click',function()
    {
        $.post(base_url+'ajax/common/ticket_regenerate',{'t_id':$('#t_id').val(),'comment':$('#comment').val(),'username':$('#manager-username').val(),'password':$('#manager-password').val()},function(jsonData)
        {

            if(jsonData.status)
            {
                window.location.reload();
            }else
            {
                $('#message-box').html('<div class="alert alert-danger">'+jsonData.message+'</div>');
            }

        },'json');

    });


</script>