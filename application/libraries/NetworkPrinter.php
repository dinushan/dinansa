<?php

/**
 * Created by PhpStorm.
 * User: LAYOUTindex
 * Date: 1/4/2018
 * Time: 3:19 PM
 */
//

require_once FCPATH.'vendor/autoload.php';


use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\Printer;


class NetworkPrinter
{

    private $pheanstalk;

    public function __construct()
    {

        //$this->pheanstalk = new Pheanstalk('127.0.0.1');

    }

    public function printOrder($kitchen_id,$data = []){

//        if ( !$this->pheanstalk->getConnection()->isServiceListening() ){
//            // other process
//            return;
//        }
//
//        $this->pheanstalk
//            ->useTube('testtube')
//            ->put("job payload goes here\n");




        return false;
    }

    private function run($data){

       // if ( $this->pheanstalk->getConnection()->isServiceListening() ) {


//            $job = $this->pheanstalk
//                ->watch('testtube')
//                ->ignore('default')
//                ->reserve();
//
//            $data = $job->getData();
//
//            $this->pheanstalk->delete($job);

            try {
                $connector = new NetworkPrintConnector("192.168.1.34", 9100);
                $printer = new Printer($connector);

                $printer->setColor(2);
                $printer->setTextSize(4, 4);
                $printer->text("Kitchen Printer Start");

                $print_val = "";

                foreach ($data as $key => $value) {
                    foreach ($value as $k => $v) {
                        $printer->setTextSize(4, 4);
                        $printer->setColor(2);
                        $printer->text($v['order_id'] . ' ' . $v['item_name'] . ' ' . $v['quantity']);
                    }
                }

                $printer->cut();
                $printer->close();

                return true;

            } catch (Exception $e) {
                return false;
            }
        //}
    }
}