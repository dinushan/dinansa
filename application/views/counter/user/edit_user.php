<!-- BEGIN CONTAINER -->

<?php 
$yesno = unserialize(YESNO);
?>

<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">

			<h3 class="page-title">Edit Counter User</h3>
			<div class="page-bar">				
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<a class="btn default" href="<?php echo site_url('counter/counterusers') ?>"><i class="fa fa-chevron-left"></i> Back</a>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12">
					<div> <?php echo get_message() ?> </div>
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet light bordered">
						<div class="portlet-body form"> 
							<!-- BEGIN FORM-->
							<?php
                            $image = $user->picture;
							$attr = array("id"=>"edit_form","class"=>"form-horizontal","enctype"=>"multipart/form-data");
							echo form_open(current_url(),$attr);
							?>
							<div class="form-group">
								<label class="control-label col-md-3">Username</label>
								<div class="control-element col-md-4">
									<label class="control-label"><?php echo $user->username ?></label>
								</div>
							</div>
                          <!--  <div class="form-group">
                                <label class="control-label col-md-3">Employee ID</label>
                                <div class="control-element col-md-4">
                                    <input class="form-control" type="text" value="<?php echo set_value('emp_id', $user->emp_id) ?>" name="emp_id" />
                                    <?php echo form_error('emp_id'); ?> </div>
                            </div>-->
							<div class="form-group">
								<label class="control-label col-md-3">Full Name<span class="req">*</span></label>
								<div class="control-element col-md-4">
									<input class="form-control" type="text" value="<?php echo set_value('fullname', $user->fullname) ?>" name="fullname" />
									<?php echo form_error('fullname'); ?> </div>
							</div>
                            <?php if ( $image != ''): ?>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Profile Picture <span class="req">*</span></label>

                                    <div class="control-element col-md-4">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td style="width: 100px; background-color: #fff;"><img
                                                        src="<?php echo base_url() . COUNTER_IMG_PATH . $image ?>?<?php echo time() ?>"
                                                        alt="" /></td>
                                                <td>&nbsp;</td>
                                                <td><?php list($img_width, $img_height) = getimagesize(COUNTER_IMG_PATH . $image); ?>
                                                    <strong>Image Info</strong><br/>
                                                    width:<?php echo $img_width ?>px<br/>
                                                    height:<?php echo $img_height ?>px
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3">&nbsp;
                                                    <input type="hidden" name="picture" value="<?php echo $image ?>"/></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"><label>
                                                        <input name="img_del" type="checkbox" value="1"/>
                                                        Image Delete </label>

                                                    <div class="help-block font-green">If you want to upload a image, first
                                                        delete the existing file.
                                                    </div>
                                            </tr>
                                            <tr>
                                                <td colspan="3">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Profile Picture <span class="req">*</span></label>

                                    <div class="control-element col-md-4"><input type="file" class=" input-file" name="image">

                                <span class="help-block font-green">Image Size:<?php echo COUNTER_IMG_W ?>
                                    px &times; <?php echo COUNTER_IMG_H ?>px, Image Type: png</span>
                                    </div>
                                </div>
                            <?php endif; ?>
                            <div class="form-group">
                                <label class="control-label col-md-3">NIC<span class="req">*</span></label>
                                <div class="control-element col-md-4">
                                    <input class="form-control" type="text" value="<?php echo set_value('nic', $user->nic) ?>" name="nic" />
                                    <?php echo form_error('nic'); ?> </div>
                            </div>
							<div class="form-group">
								<label class="control-label col-md-3">Email<span class="req">*</span></label>
								<div class="control-element col-md-4">
									<input class="form-control required-entry" type="text" value="<?php echo set_value('email', $user->email) ?>" name="email" />
									<?php echo form_error('email'); ?> </div>
							</div>
                            <div class="form-group">
                                <label class="control-label col-md-3">Phone 1<span class="req">*</span></label>
                                <div class="control-element col-md-4">
                                    <input class="form-control  required-entry" type="text" value="<?php echo set_value('phone1', $user->phone1) ?>" name="phone1" />
                                    <?php echo form_error('phone1'); ?> </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Phone 2</label>
                                <div class="control-element col-md-4">
                                    <input class="form-control" type="text" value="<?php echo set_value('phone2', $user->phone2) ?>" name="phone2" />
                                    <?php echo form_error('phone2'); ?> </div>
                            </div>

							<div class="form-group">
								<label class="control-label col-md-3">Active</label>
								<div class="control-element col-md-4"><?php echo form_dropdown('active', $yesno, set_value('active', $user->active),'class="form-control"') ?> </div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Enabled</label>
								<div class="control-element col-md-4"><?php echo form_dropdown('enabled', $yesno, set_value('enabled', $user->enabled),'class="form-control"') ?> </div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Password Never Expires</label>
								<div class="control-element col-md-4"><?php echo form_dropdown('password_never_expires', $yesno, set_value('password_never_expires', $user->password_never_expires),'class="form-control"') ?> </div>
							</div>

                            <div class="form-group">

                                <label class="control-label col-md-3">Cinema</label>
                                <div class="control-element col-md-4">
                                    <?php echo form_dropdown('cinema', $cinema_list, set_value('cinema',$user->cinema),'class="form-control"') ?>
                                    <?php echo form_error('cinema'); ?></div>
                                </div>


                            <?php if($user->user_id != 1):?>
							<div class="form-group">
								<?php 
							$groupArr = array();
							foreach($groups as $k=>$_group)
							{
								//if($k != 1){
									$groupArr[$k] = $groups[$k];
								//}
								
							}							
							?>
								<label class="control-label col-md-3">Groups</label>
								<div class="control-element col-md-4"><?php echo form_dropdown('groups[]', $groupArr, set_value('groups[]', $user->groups),'class="form-control"') ?> </div>
							</div>
                            <?php endif; ?>

							<div class="form-group">
								<div class="control-element col-md-offset-3 col-md-9"><strong>Only enter a password if you would like to set a new one</strong></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">New Password</label>
								<div class="control-element col-md-4">
									<input class="form-control" type="password" value="" name="password" />
									<?php echo form_error('password'); ?></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Confirm New Password</label>
								<div class="control-element col-md-4">
									<input class="form-control" type="password" value="" name="password_conf" />
									<?php echo form_error('password_conf'); ?></div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
									</div>
								</div>
							</div>
							<script type="text/javascript">							 
								$('.required-entry').attr('required','required');					
								$('#edit_form').validate({
									errorElement: 'span', 
									errorClass: 'validation-advice', 
									submitHandler: function (form) {
										form.submit();										
									}
								});									
							</script>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT--> 
	</div>
	<!-- END CONTENT --> 
	
</div>
<!-- END CONTAINER --> 
