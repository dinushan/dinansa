<!-- BEGIN CONTAINER -->
<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">


            <h3 class="page-title">Manage Facilities</h3>

            <div class="page-bar">
                <div class="page-toolbar">
                    <div class="btn-group pull-right">

                        <a onclick="setLocation('<?php echo site_url('admin/' . $slug . '/add') ?>')" class="btn green"><i
                                class="fa fa-plus-circle"></i> Add New</a>

                    </div>
                </div>
            </div>


            <?php echo get_message() ?>
            <div class="row">
                <div class="col-md-12">

                    <div class="table-container">

                        <table class="table table-striped table-bordered table-hover" id="datatable_ajax"
                               data-order-col="0"
                               data-order-type="asc"
                               data-ajax_url="<?php echo base_url('admin/' . $slug . '/ajax_list') ?>">
                            <colgroup>
                                <col width="40"/>
                                <col>
                                <col width="300">
                            </colgroup>
                            <thead>
                            <tr role="row" class="heading">
                                <th>ID</th>
                                <th>Cinema</th>
                                <th class="sorting_disabled">Action</th>
                            </tr>
                            <tr role="row" class="filter">
                                <td>
                                </td>
                                <td>
                                    <input type="text" class="form-control form-filter input-sm" name="theater">
                                </td>
                                <td>
                                    <div class="margin-bottom-5">
                                        <button class="btn btn-sm yellow filter-submit margin-bottom"><i
                                                class="fa fa-search"></i> Search
                                        </button>
                                    </div>
                                    <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset
                                    </button>
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>

            <script>
                jQuery(document).ready(function () {
                    TableAjax.init();
                });
            </script>

        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
