<!-- BEGIN CONTAINER -->

<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<?php if($page_action == ''):?>
			<h3 class="page-title"><?php echo $pData->menu ?></h3>
			<div class="page-bar">
				<div class="page-toolbar">
					<div class="btn-group pull-right"> <a onclick="setLocation('<?php echo site_url('admin/'.$slug.'/pages'); ?>')" class="btn default"><i class="fa fa-chevron-left"></i> Back</a>
						<?php if($pg_id != 2):?>
						<a onclick="setLocation('<?php echo site_url('admin/'.$slug.'/page_items/add/'.$enc_pg_id); ?>')" class="btn green"><i class="fa fa-plus-circle"></i>Add New</a>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<?php echo get_message() ?>
			<div class="row">
				<div class="col-md-12">
					<div class="table-scrollable">
						<?php if($this->adminauth->has_role('142')):?>
						<table id="tbl" class="table table-striped table-bordered table-advance table-hover">
							<colgroup>
							<col width="40" />
							<col />
							<col width="150"  />
							</colgroup>
							<thead>
								<tr class="headings nodrop nodrag">
									<!--<th></th>-->
									<th colspan="2">Title</th>
									<th>Action</th>
								</tr>
							</thead>
							<tbody>
								<?php
									$edit_url = site_url('admin/'.$slug.'/page_items/edit/'.$enc_pg_id);
									$delete_url = site_url('admin/'.$slug.'/page_items/delete/'.$enc_pg_id);

										foreach($itemList as $_page):
										$id = $_page['id'];
                                    	$encry_hcId = $this->encrypt_lib->encode($id);
                                   ?>
								<tr id="<?php echo $encry_hcId ?>">
								<td colspan="2"><?php echo $_page['title'] ?></td>
									<td><a class="btn btn-sm default btn-xs" href="<?php echo $edit_url.'/'.$encry_hcId ?>"><i class="fa fa-edit"></i> Edit</a>
</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<script type="text/javascript">	
			jQuery(document).ready(function(e) {
				jQuery('#tbl').tableDnD({
				onDragStart:function(table){
					jQuery(table).find('tr').removeClass('even');		
				},
				onDrop: function(table, row) {					
					var order = jQuery(table).tableDnDSerialize(); 
					jQuery.post("<?php echo site_url('admin/site_content/page_items/sorting') ?>", order, function(theResponse){		
					
					});
					jQuery('#tbl1 tbody tr:even').addClass('even');					 
				},
				dragHandle:'.dragHandle'
				});	
			});	
			
			</script>
				
			<?php elseif($page_action == 'add' || $page_action == 'edit'): ?>
			<h3 class="page-title"><?php echo $page_action == "add" ?'Add New : ' :'Edit : ' ?> <?php echo $pData->menu ?></h3>
			<div class="page-bar">
				<div class="page-toolbar">
					<div class="btn-group pull-right"> <a class="btn default" href="<?php echo site_url('admin/'.$slug.'/page_items/index/'.$enc_pg_id) ?>"><i class="fa fa-chevron-left"></i> Back</a> </div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12"> <?php echo get_message() ?> 
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet light bordered"> 
						<!--<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-pencil"></i>Settings
							</div>							
						</div>-->
						<div class="portlet-body form popup-gallery">
							<!-- BEGIN FORM-->
							<?php                    
							$attr = array("id"=>"edit_form","class"=>"form-horizontal","enctype"=>"multipart/form-data");
							echo form_open(current_url(),$attr);
							?>
							<?php
								if($page_action == 'edit'){
									$title = $itemData->title;
									$title1 = $itemData->title1;
									$id = $itemData->id;
									$year = $itemData->year;
									$company_name = $itemData->company_name;
									$location = $itemData->location;
									$description = $itemData->description;
									$short_description = $itemData->short_description;
									$image = $itemData->image;
									$image_thumbnail = $itemData->image_thumbnail;
									$image1 = $itemData->image1;
									$image2 = $itemData->image2;
									$image3 = $itemData->image3;
									$image4 = $itemData->image4;
									$image5 = $itemData->image5;
									$image6 = $itemData->image6;
									$image7 = $itemData->image7;
									$image8 = $itemData->image8;
									$image9 = $itemData->image9;
									$image10 = $itemData->image10;
									
									$url_key = $itemData->url_key;	
									$meta_title = $itemData->meta_title;	
									$meta_keywords = $itemData->meta_keywords;	
									$meta_description = $itemData->meta_description;	
									$og_type = $itemData->og_type;	
									$og_title = $itemData->og_title;	
									$og_description = $itemData->og_description;
									
								}else{
									$title = '';
									$title1 = '';
									$year = '';
									$company_name = '';
									$location = '';
									$description = '';
									$short_description = '';
									$image = '';
									$image_thumbnail = '';
									$image1 = '';
									$image2 = '';
									$image3 = '';
									$image4 = '';
									$image5 = '';
									$image6 = '';
									$image7 = '';
									$image8 = '';
									$image9 = '';
									$image10 = '';
									
									$url_key = '';	
									$meta_title = '';	
									$meta_keywords = '';	
									$meta_description = '';	
									$og_type = '';	
									$og_title = '';	
									$og_description = '';
									
								}
								
							?>

							<h4 class="form-section">General</h4>

							<?php if($pg_id == 1):?>
								<div class="form-group">
									<label class="control-label col-md-3">Year<span class="req">*</span></label>
									<div class="control-element col-md-4">
										<input name="year" type="number" class="form-control required-entry" value="<?php echo set_value('year',$year) ?>" />
										<?php echo form_error('year'); ?> </div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Title<span class="req">*</span></label>
									<div class="control-element col-md-4">
										<input name="title"
											   type="text" class="form-control req-entry"
											   value="<?php echo $page_action == "add" ?
												   set_value('title') :
												   set_value('title',$title) ?>" size="15" />
										<?php echo form_error('title'); ?></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Company Name<span class="req">*</span></label>
									<div class="control-element col-md-4">
										<input name="company_name" type="text" class="form-control required-entry" value="<?php echo set_value('company_name',$company_name) ?>" />
										<?php echo form_error('companyName'); ?> </div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Location<span class="req">*</span></label>
									<div class="control-element col-md-4">
										<input name="location" type="text" class="form-control" value="<?php echo set_value('location',$location) ?>" />
										<?php echo form_error('location'); ?> </div>
								</div>
								<h4 class="form-section">Read More Section</h4>
								<div class="form-group">
									<label class="control-label col-md-3">Description<span class="req">*</span></label>
									<div class="control-element col-md-9">
									<textarea id="description_textarea" cols="15" rows="10" class="tinymce_editor" name="description"><?php echo $page_action == "add" ?
												set_value('description') :
												set_value('description',$description) ?></textarea>
										<?php echo form_error('description'); ?> </div>
								</div>
							<?php endif; ?>

							<?php if($pg_id == 2):?>
								<div class="form-group">
									<label class="control-label col-md-3">Name<span class="req">*</span></label>
									<div class="control-element col-md-4">
										<input name="title"
											   type="text" class="form-control req-entry"
											   value="<?php echo $page_action == "add" ?
												   set_value('title') :
												   set_value('title',$title) ?>" size="15" />
										<?php echo form_error('title'); ?></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">URL key<span class="req">*</span></label>
									<div class="control-element col-md-4">
										<input name="url_key" type="text"
											   class="form-control req-entry required-entry"
											   value="<?php echo $page_action == "add" ?
												   set_value('url_key') :
												   set_value('url_key',$url_key) ?>" size="15" />
										<?php echo form_error('url_key'); ?>
										<div class="help-block font-green">"Note: If the site is LIVE changing the URL may harm your ranking on search engines" </div>
									</div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Company<span class="req">*</span></label>
									<div class="control-element col-md-4">
										<input name="title1"
											   type="text" class="form-control req-entry"
											   value="<?php echo $page_action == "add" ?
												   set_value('title1') :
												   set_value('title1',$title1) ?>" size="15" />
										<?php echo form_error('title1'); ?></div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Short Description<span class="req">*</span></label>
									<div class="control-element col-md-9">
									<textarea cols="15" rows="4" class="form-control" name="short_description"><?php echo $page_action == "add" ?
											set_value('short_description') :
											set_value('short_description',$short_description) ?></textarea>
										<?php echo form_error('short_description'); ?> </div>
								</div>
								<div class="form-group">
									<label class="control-label col-md-3">Description<span class="req">*</span></label>
									<div class="control-element col-md-9">
									<textarea id="_description_textarea" cols="15" rows="10" class="req-entry form-control tinymce_editor" name="description"><?php echo $page_action == "add" ?
											set_value('description') : set_value('description',$description) ?></textarea><?php echo form_error('description1'); ?></div>
								</div>

								<div class="form-group">
									<label class="control-label col-md-3">Cover Image</label>
									<div class="control-element col-md-4">
										<input type="file" class=" input-file" name="image">
										<span class="help-block font-green">Image Size:<?php echo SECTOR_APPAREL_IMG_W ?>px &times; <?php echo SECTOR_APPAREL_IMG_H ?>px, Image Type: jpg</span> </div>
								</div>
								<?php if($page_action == 'edit' && $image!= ''):?>
									<div class="form-group">
										<div class="control-label col-md-3"></div>
										<div class="control-element col-md-4">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><a href="<?php echo base_url().SECTOR_APPAREL_IMG_PATH.$image ?>"><img src="<?php echo base_url().SECTOR_APPAREL_IMG_PATH.$image ?>?<?php echo time() ?>" alt="" width="100" height="70" /></a></td>
													<td>&nbsp;</td>
													<td><?php list($img_width, $img_height) = getimagesize(SECTOR_APPAREL_IMG_PATH.$image);?>
														<strong>Image Info</strong><br />
														width:<?php echo $img_width ?>px<br />
														height:<?php echo $img_height ?>px</td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;
														<input type="hidden" name="image" value="<?php echo $image ?>" /></td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
											</table>
										</div>
									</div>
							<?php endif; ?>

								<div class="form-group">
									<label class="control-label col-md-3">Thumbnail Image for Home Page</label>
									<div class="control-element col-md-4">
										<input type="file" class=" input-file" name="imageHA">
										<span class="help-block font-green">Image Size:<?php echo HOME_SECTOR_MOB_APPAREL_IMG_W ?>px &times; <?php echo HOME_SECTOR_MOB_APPAREL_IMG_H ?>px, Image Type: jpg</span> </div>
								</div>
								<?php if($page_action == 'edit' && $image_thumbnail!= ''):?>
									<div class="form-group">
										<div class="control-label col-md-3"></div>
										<div class="control-element col-md-4">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><a href="<?php echo base_url().HOME_SECTOR_APPAREL_IMG_PATH.$image_thumbnail ?>"><img src="<?php echo base_url().HOME_SECTOR_APPAREL_IMG_PATH.$image_thumbnail ?>?<?php echo time() ?>" alt="" width="100" height="70" /></a></td>
													<td>&nbsp;</td>
													<td><?php list($img_width, $img_height) = getimagesize(HOME_SECTOR_APPAREL_MOB_IMG_PATH.$image_thumbnail);?>
														<strong>Image Info</strong><br />
														width:<?php echo $img_width ?>px<br />
														height:<?php echo $img_height ?>px</td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;
														<input type="hidden" name="images" value="<?php echo $image_thumbnail ?>" /></td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
											</table>
										</div>
									</div>
								<?php endif; ?>

								<h4 class="form-section">Images for the Detailed Pages</h4>
								<div class="form-group">
									<label class="control-label col-md-3">Left Image 1</label>
									<div class="control-element col-md-4">
										<input type="file" class=" input-file" name="image1">
										<span class="help-block font-green">Image Size:<?php echo APPAREL_READMORE_PAGE_MOB_IMG_L1_W ?>px &times; <?php echo APPAREL_READMORE_PAGE_MOB_IMG_L1_H ?>px, Image Type: jpg</span> </div>
								</div>
								<?php if($page_action == 'edit' && $image1!= ''):?><!--LEFT IMAGE 1-->
									<div class="form-group">
										<div class="control-label col-md-3"></div>
										<div class="control-element col-md-4">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><a href="<?php echo base_url().APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image1 ?>"><img src="<?php echo base_url().APPAREL_READMORE_PAGE_IMG_PATH.$image1 ?>?<?php echo time() ?>" alt="" width="100" height="70" /></a></td>
													<td>&nbsp;</td>
													<td><?php list($img_width, $img_height) = getimagesize(APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image1);?>
														<strong>Image Info</strong><br />
														width:<?php echo $img_width ?>px<br />
														height:<?php echo $img_height ?>px</td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;
														<input type="hidden" name="image1" value="<?php echo $image1 ?>" /></td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
											</table>
										</div>
									</div>
								<?php endif; ?>

								<div class="form-group">
									<label class="control-label col-md-3">Left Image 2</label>
									<div class="control-element col-md-4">
										<input type="file" class=" input-file" name="image2">
										<span class="help-block font-green">Image Size:<?php echo APPAREL_READMORE_PAGE_MOB_IMG_L2_W ?>px &times; <?php echo APPAREL_READMORE_PAGE_MOB_IMG_L2_H ?>px, Image Type: jpg</span> </div>
								</div>
								<?php if($page_action == 'edit' && $image2!= ''):?><!--LEFT IMAGE 2-->
									<div class="form-group">
										<div class="control-label col-md-3"></div>
										<div class="control-element col-md-4">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><a href="<?php echo base_url().APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image2 ?>"><img src="<?php echo base_url().APPAREL_READMORE_PAGE_IMG_PATH.$image2 ?>?<?php echo time() ?>" alt="" width="100" height="70" /></a></td>
													<td>&nbsp;</td>
													<td><?php list($img_width, $img_height) = getimagesize(APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image2);?>
														<strong>Image Info</strong><br />
														width:<?php echo $img_width ?>px<br />
														height:<?php echo $img_height ?>px</td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;
														<input type="hidden" name="image2" value="<?php echo $image2 ?>" /></td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
											</table>
										</div>
									</div>
								<?php endif; ?>

								<div class="form-group">
									<label class="control-label col-md-3">Left Image 3</label>
									<div class="control-element col-md-4">
										<input type="file" class=" input-file" name="image3">
										<span class="help-block font-green">Image Size:<?php echo APPAREL_READMORE_PAGE_MOB_IMG_L3_W ?>px &times; <?php echo APPAREL_READMORE_PAGE_MOB_IMG_L3_H ?>px, Image Type: jpg</span> </div>
								</div>
								<?php if($page_action == 'edit' && $image3!= ''):?><!--LEFT IMAGE 3-->
									<div class="form-group">
										<div class="control-label col-md-3"></div>
										<div class="control-element col-md-4">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><a href="<?php echo base_url().APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image3 ?>"><img src="<?php echo base_url().APPAREL_READMORE_PAGE_IMG_PATH.$image3 ?>?<?php echo time() ?>" alt="" width="100" height="70" /></a></td>
													<td>&nbsp;</td>
													<td><?php list($img_width, $img_height) = getimagesize(APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image3);?>
														<strong>Image Info</strong><br />
														width:<?php echo $img_width ?>px<br />
														height:<?php echo $img_height ?>px</td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;
														<input type="hidden" name="image3" value="<?php echo $image3 ?>" /></td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
											</table>
										</div>
									</div>
								<?php endif; ?>

								<div class="form-group">
									<label class="control-label col-md-3">Left Image 4</label>
									<div class="control-element col-md-4">
										<input type="file" class=" input-file" name="image4">
										<span class="help-block font-green">Image Size:<?php echo APPAREL_READMORE_PAGE_MOB_IMG_L4_W ?>px &times; <?php echo APPAREL_READMORE_PAGE_MOB_IMG_L4_H ?>px, Image Type: jpg</span> </div>
								</div>
								<?php if($page_action == 'edit' && $image4!= ''):?><!--LEFT IMAGE 4-->
									<div class="form-group">
										<div class="control-label col-md-3"></div>
										<div class="control-element col-md-4">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><a href="<?php echo base_url().APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image4 ?>"><img src="<?php echo base_url().APPAREL_READMORE_PAGE_IMG_PATH.$image4 ?>?<?php echo time() ?>" alt="" width="100" height="70" /></a></td>
													<td>&nbsp;</td>
													<td><?php list($img_width, $img_height) = getimagesize(APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image4);?>
														<strong>Image Info</strong><br />
														width:<?php echo $img_width ?>px<br />
														height:<?php echo $img_height ?>px</td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;
														<input type="hidden" name="image4" value="<?php echo $image4 ?>" /></td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
											</table>
										</div>
									</div>
								<?php endif; ?>

								<div class="form-group">
									<label class="control-label col-md-3">Right Image 1</label>
									<div class="control-element col-md-4">
										<input type="file" class=" input-file" name="image5">
										<span class="help-block font-green">Image Size:<?php echo APPAREL_READMORE_PAGE_MOB_IMG_R1_W ?>px &times; <?php echo APPAREL_READMORE_PAGE_MOB_IMG_R1_H ?>px, Image Type: jpg</span> </div>
								</div>
								<?php if($page_action == 'edit' && $image5!= ''):?><!--RIGHT IMAGE 1-->
									<div class="form-group">
										<div class="control-label col-md-3"></div>
										<div class="control-element col-md-4">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><a href="<?php echo base_url().APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image5 ?>"><img src="<?php echo base_url().APPAREL_READMORE_PAGE_IMG_PATH.$image5 ?>?<?php echo time() ?>" alt="" width="100" height="70" /></a></td>
													<td>&nbsp;</td>
													<td><?php list($img_width, $img_height) = getimagesize(APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image5);?>
														<strong>Image Info</strong><br />
														width:<?php echo $img_width ?>px<br />
														height:<?php echo $img_height ?>px</td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;
														<input type="hidden" name="image5" value="<?php echo $image5 ?>" /></td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
											</table>
										</div>
									</div>
								<?php endif; ?>

								<div class="form-group">
									<label class="control-label col-md-3">Right Image 2</label>
									<div class="control-element col-md-4">
										<input type="file" class=" input-file" name="image6">
										<span class="help-block font-green">Image Size:<?php echo APPAREL_READMORE_PAGE_MOB_IMG_R2_W ?>px &times; <?php echo APPAREL_READMORE_PAGE_MOB_IMG_R2_H ?>px, Image Type: jpg</span> </div>
								</div>
								<?php if($page_action == 'edit' && $image6!= ''):?><!--RIGHT IMAGE 2-->
									<div class="form-group">
										<div class="control-label col-md-3"></div>
										<div class="control-element col-md-4">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><a href="<?php echo base_url().APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image6 ?>"><img src="<?php echo base_url().APPAREL_READMORE_PAGE_IMG_PATH.$image6 ?>?<?php echo time() ?>" alt="" width="100" height="70" /></a></td>
													<td>&nbsp;</td>
													<td><?php list($img_width, $img_height) = getimagesize(APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image6);?>
														<strong>Image Info</strong><br />
														width:<?php echo $img_width ?>px<br />
														height:<?php echo $img_height ?>px</td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;
														<input type="hidden" name="image6" value="<?php echo $image6 ?>" /></td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
											</table>
										</div>
									</div>
								<?php endif; ?>

								<div class="form-group">
									<label class="control-label col-md-3">Right Image 3</label>
									<div class="control-element col-md-4">
										<input type="file" class=" input-file" name="image7">
										<span class="help-block font-green">Image Size:<?php echo APPAREL_READMORE_PAGE_MOB_IMG_R3_W ?>px &times; <?php echo APPAREL_READMORE_PAGE_MOB_IMG_R3_H ?>px, Image Type: jpg</span> </div>
								</div>
								<?php if($page_action == 'edit' && $image7!= ''):?><!--RIGHT IMAGE 3-->
									<div class="form-group">
										<div class="control-label col-md-3"></div>
										<div class="control-element col-md-4">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><a href="<?php echo base_url().APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image7 ?>"><img src="<?php echo base_url().APPAREL_READMORE_PAGE_IMG_PATH.$image7 ?>?<?php echo time() ?>" alt="" width="100" height="70" /></a></td>
													<td>&nbsp;</td>
													<td><?php list($img_width, $img_height) = getimagesize(APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image7);?>
														<strong>Image Info</strong><br />
														width:<?php echo $img_width ?>px<br />
														height:<?php echo $img_height ?>px</td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;
														<input type="hidden" name="image7" value="<?php echo $image7 ?>" /></td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
											</table>
										</div>
									</div>
								<?php endif; ?>

								<div class="form-group">
									<label class="control-label col-md-3">Right Image 4</label>
									<div class="control-element col-md-4">
										<input type="file" class=" input-file" name="image8">
										<span class="help-block font-green">Image Size:<?php echo APPAREL_READMORE_PAGE_MOB_IMG_R4_W ?>px &times; <?php echo APPAREL_READMORE_PAGE_MOB_IMG_R4_H ?>px, Image Type: jpg</span> </div>
								</div>
								<?php if($page_action == 'edit' && $image8!= ''):?><!--RIGHT IMAGE 4-->
									<div class="form-group">
										<div class="control-label col-md-3"></div>
										<div class="control-element col-md-4">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><a href="<?php echo base_url().APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image8 ?>"><img src="<?php echo base_url().APPAREL_READMORE_PAGE_IMG_PATH.$image8 ?>?<?php echo time() ?>" alt="" width="100" height="70" /></a></td>
													<td>&nbsp;</td>
													<td><?php list($img_width, $img_height) = getimagesize(APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image8);?>
														<strong>Image Info</strong><br />
														width:<?php echo $img_width ?>px<br />
														height:<?php echo $img_height ?>px</td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;
														<input type="hidden" name="image8" value="<?php echo $image8 ?>" /></td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
											</table>
										</div>
									</div>
								<?php endif; ?>

								<div class="form-group">
									<label class="control-label col-md-3">Right Image 5</label>
									<div class="control-element col-md-4">
										<input type="file" class=" input-file" name="image9">
										<span class="help-block font-green">Image Size:<?php echo APPAREL_READMORE_PAGE_MOB_IMG_R5_W ?>px &times; <?php echo APPAREL_READMORE_PAGE_MOB_IMG_R5_H ?>px, Image Type: jpg</span> </div>
								</div>
								<?php if($page_action == 'edit' && $image9!= ''):?><!--RIGHT IMAGE 5-->
									<div class="form-group">
										<div class="control-label col-md-3"></div>
										<div class="control-element col-md-4">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><a href="<?php echo base_url().APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image9 ?>"><img src="<?php echo base_url().APPAREL_READMORE_PAGE_IMG_PATH.$image9 ?>?<?php echo time() ?>" alt="" width="100" height="70" /></a></td>
													<td>&nbsp;</td>
													<td><?php list($img_width, $img_height) = getimagesize(APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image9);?>
														<strong>Image Info</strong><br />
														width:<?php echo $img_width ?>px<br />
														height:<?php echo $img_height ?>px</td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;
														<input type="hidden" name="image9" value="<?php echo $image9 ?>" /></td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
											</table>
										</div>
									</div>
								<?php endif; ?>

								<div class="form-group">
									<label class="control-label col-md-3">Right Image 6</label>
									<div class="control-element col-md-4">
										<input type="file" class=" input-file" name="image10">
										<span class="help-block font-green">Image Size:<?php echo APPAREL_READMORE_PAGE_MOB_IMG_R6_W ?>px &times; <?php echo APPAREL_READMORE_PAGE_MOB_IMG_R6_H ?>px, Image Type: jpg</span> </div>
								</div>
								<?php if($page_action == 'edit' && $image10!= ''):?><!--RIGHT IMAGE 6-->
									<div class="form-group">
										<div class="control-label col-md-3"></div>
										<div class="control-element col-md-4">
											<table width="100%" border="0" cellspacing="0" cellpadding="0">
												<tr>
													<td><a href="<?php echo base_url().APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image10 ?>"><img src="<?php echo base_url().APPAREL_READMORE_PAGE_IMG_PATH.$image10 ?>?<?php echo time() ?>" alt="" width="100" height="70" /></a></td>
													<td>&nbsp;</td>
													<td><?php list($img_width, $img_height) = getimagesize(APPAREL_READMORE_PAGE_MOB_IMG_PATH.$image10);?>
														<strong>Image Info</strong><br />
														width:<?php echo $img_width ?>px<br />
														height:<?php echo $img_height ?>px</td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;
														<input type="hidden" name="image10" value="<?php echo $image10 ?>" /></td>
												</tr>
												<tr>
													<td colspan="3">&nbsp;</td>
												</tr>
											</table>
										</div>
									</div>
								<?php endif; ?>
								

							<?php endif; ?>
							
					<?php if($pg_id == 2):?>
							<h4 class="form-section">Meta Settings</h4>
							<div class="form-group">
								<label class="control-label col-md-3">Meta Title</label>
								<div class="control-element col-md-4">
									<input type="text" class="form-control" value="<?php echo 
								$page_action == "add" ? 
								set_value('meta_title') : 
								set_value('meta_title',$meta_title) ?>" 
								name="meta_title" />
								</div>
							</div>
							
							<?php if(SHOW_META_CONTENT === TRUE):?>							
							<div class="form-group">
								<label class="control-label col-md-3">Meta Keywords</label>
								<div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="meta_keywords"><?php echo $page_action == "add" ? 
								set_value('meta_keywords') : 
								set_value('meta_keywords',$meta_keywords) ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Meta Description</label>
								<div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="meta_description"><?php echo $page_action == "add" ? 
								set_value('meta_description') : 
								set_value('meta_description',$meta_description) ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">OG Type</label>
								<div class="control-element col-md-4">
									<input type="text" class="form-control" value="<?php echo 
								$page_action == "add" ? 
								set_value('og_type') : 
								set_value('og_type',$og_type) ?>" 
								name="og_type" />
									<span class="help-block font-green">The type of the object, such as 'article'</span></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">OG Title</label>
								<div class="control-element col-md-4">
									<input type="text" class="form-control" value="<?php echo 
								$page_action == "add" ? 
								set_value('og_title') : 
								set_value('og_title',$og_title) ?>" 
								name="og_title" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">OG Description</label>
								<div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="og_description"><?php echo $page_action == "add" ? 
								set_value('og_description') : 
								set_value('og_description',$og_description) ?></textarea>
								</div>
							</div>
							<?php endif;?>
							<?php endif; ?>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
									</div>
								</div>
							</div>
							<script type="text/javascript">							 
								$('.required-entry').attr('required','required');					
								$('#edit_form').validate({
									errorElement: 'span', 
									errorClass: 'validation-advice', 								
									ignore: "",
									submitHandler: function (form) {
										form.submit();										
									}
								});									
							</script>
					</form>
						</div>
					</div>
				</div>
			</div>
			<?php endif;//$page_action ?>
		</div>
		<!-- END PAGE CONTENT--> 
	</div>
	<!-- END CONTENT --> 
	
</div>
<!-- END CONTAINER --> 

