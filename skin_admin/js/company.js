jQuery(document).ready(function(e) {
	
	jQuery("#edit_form").validate({
		ignore: '',
		rules: {
			'industry[]': {
                required: true
            }
		},
		messages: {
            'industry[]': {
                required: "You must check at least 1 Business Category"
            }
        },
		errorPlacement: function(error, element) {
			if (element.attr("name") == "industry[]") {
				error.insertAfter("#biz_cat");
			} else {
				error.insertAfter(element);
			}
		},
		
		//company: "required",
		//location_id: "required",
		//location: "required",
		submitHandler: function(form) {
			form.submit();			
		}
	});
	
	
});	
