<?php

class Migration_Add_Zone_Table extends CI_Migration
{
  public function up()
  {
      // Create as pure mysql query
      //  $this->db->query('CREATE TABLE tablename (
      //     id INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
      //     column1 VARCHAR(30) NOT NULL,
      //     column2 VARCHAR(30) NOT NULL
      //     )');

      $this->dbforge->add_field(
          array(
             'id' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'auto_increment' => true
             ),
             'name' => array(
                'type' => 'VARCHAR',
                'constraint' => '100',
             ),
             'restaurant_id' => array(
               'type' => 'INT',
               'constraint' => 11,
               'unsigned' => true,
             ),
             'table_count' => array(
               'type' => 'INT',
               'constraint' => 11,
               'unsigned' => true,
             ),
             'chair_count' => array(
               'type' => 'INT',
               'constraint' => 11,
               'unsigned' => true,
             ),
          )
       );

       $this->dbforge->add_key('id', TRUE);
       $this->dbforge->create_table('zones');


  }

  public function down()
  {
      $this->dbforge->drop_table('zones');
  }
}
?>
