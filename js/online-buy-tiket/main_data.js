/**
 * Created by LAYOUTindex on 3/22/2017.
 */
(function (window,$) {

    var all = document.getElementsByTagName("*");
    var el = $(".portlet");
    // for (var i=0, max=all.length; i < max; i++) {
    //     console.log(all[i].hasChildNodes()+':'+all[i].tagName+' : '+all[i].parentNode.nodeName);
    // }
    var today = new Date();
    $('#seatSelection699').hide();
    $('#bookingSummery645').hide();

    var elements = {
        'datePicket':$('#selectDate'),
        'theaterSelector': $('#selectTheatre'),
        'movieSelector': $('#selectMovie'),
        'showTimeSelector':$('#selectShowTime'),
        'reqSeatCount':$('#req_seat_count')
    };

    var dataObjects = {
        "movies":[],
        "timeSlots" :[],
        "priceObject":[],
        "theaters" :window.moviedata.theaters
    };

   var dt = new Date( window.moviedata.selectedValus.date);
   window.moviedata.selectedValus.date = dt.getFullYear()+'-'+(dt.getMonth()+1)+'-'+(dt.getDate());
   window.moviedata.selectedValus.tot_selected_seat_count =0 ;
   window.moviedata.selectedValus.frm_2_action ='' ;



    var post = function (url,data,success,error){

        $.ajax({
            url:window.base_url+url,
            method:'POST',
            data:data,
            dataType: "json",
            cache: false,
            success:function(data,text,xhr){success(data,text,xhr);},
            error:function(err){error(err);},

        });
    };

    var get = function (url,success,error) {
        $.get(window.base_url+url,function (data) {
            success(jQuery.parseJSON(data));
        });
    };

    var onDateChange = function () {
          
        elements.theaterSelector.find('select').prop('disabled', true);
        elements.movieSelector.find('select').prop('disabled', true);
        elements.showTimeSelector.find('select').prop('disabled',true);
        $('#submitbtn366').prop('disabled',true);

        $('#selectTheatre').css('display','none');
        $('#selectMovie').css('display','none');
        $('#selectShowTime').css('display','none');
        $('#seat_selector_6654').css('display','none');
        $('#next_3678_btn').css('display','none');

        get('buy-tickets-online/get-theaters/'+moviedata.selectedValus.date,function (data) {

            if(data.theaters.length>0){
                 $('#selectTheatre').css('display','block');
                var content="<option value=''>--Select--</option>";
                
            }
            else{
                var content="<option value=''>--No data--</option>";
            }
            dataObjects.theaters = data.theaters;

            for (var i in data.theaters){
                content+="<option value='"+data.theaters[i].theater_id+"'>"+data.theaters[i].name+"</option>";
            }
            if (dataObjects.theaters.length > 0){
                var dt = new Date(elements.datePicket.val());
                var formt = dt.getUTCFullYear()+'-'+(dt.getUTCMonth()+1)+'-'+(dt.getUTCDate());

               window.moviedata.selectedValus.theater = dataObjects.theaters[0].theater_id;
                
            }





            elements.theaterSelector.find('select').prop('disabled', false);
            elements.theaterSelector.find('select').html(content);


        },function (data,text,xhr) {

        });

    };

    var onTheaterSelect = function () {


        $('#selectMovie').css('display','none');
        $('#selectShowTime').css('display','none');
        $('#next_3678_btn').css('display','none');
        $('#seat_selector_6654').css('display','none');

        elements.movieSelector.find('select').prop('disabled', true);
        elements.showTimeSelector.find('select').prop('disabled',true);
        $('#submitbtn366').prop('disabled',true);

       if( moviedata.selectedValus.theater!=null &&  moviedata.selectedValus.theater!='' && moviedata.selectedValus.date && moviedata.selectedValus.date!=null){
           get('buy-tickets-online/get-movie/'+moviedata.selectedValus.theater+'/'+moviedata.selectedValus.date,function (data) {
               //var content="<option value=''>--Select--</option>";

               if(data.movies.length>0){
                   $('#selectMovie').css('display','block');
                   var content="<option value=''>--Select--</option>";
               }
               else{
                   var content="<option value=''>--No data--</option>";
               }

               dataObjects.movies = data.movies;

               for (var i in data.movies){
                   content+="<option value='"+data.movies[i].movie_id+"'>"+data.movies[i].movie_name+"</option>";
               }

               if (dataObjects.movies.length > 0){
                   //  moviedata.selectedValus.movie = dataObjects.movies[0].movie_id;
                   //onMovieSelect();
               }

               elements.movieSelector.find('select').prop('disabled', false);
               elements.movieSelector.find('select').html(content);
           });
       }



    };

    var onMovieSelect = function (data) {

        
        $('#selectShowTime').css('display','none');
        $('#next_3678_btn').css('display','none');
        $('#seat_selector_6654').css('display','none');

        elements.showTimeSelector.find('select').prop('disabled',true);
        $('#submitbtn366').prop('disabled',true);

        if(moviedata.selectedValus.theater!=null && moviedata.selectedValus.theater!='' && moviedata.selectedValus.date!='' && moviedata.selectedValus.date!=null && moviedata.selectedValus.movie!='' && moviedata.selectedValus.movie!=null) {
            post('buy-tickets-online/select_time_slots',
                {
                    'theater': moviedata.selectedValus.theater,
                    'movie_date': moviedata.selectedValus.date,
                    'movie': moviedata.selectedValus.movie
                }, function (data, text, xhr) {

                    $('.theatreTitle').html(data.name);
                    $('.theatrelogo').attr("src", data.theater_logo);
                    $('.movieName').html(data.movie_data.movie_name);
                    $('.year').html(data.movie_data.movie_year);
                    $('.movieDuration').html(data.movie_data.duration);

                    if(data.time_slot_data.length>0){
                        $('#selectShowTime').css('display','block');
                        $('#next_3678_btn').css('display','block');
                        $('#seat_selector_6654').css('display','block');
                        var content="<option value=''>--Select--</option>";
                    }
                    else{
                        var content="<option value=''>--No data--</option>";
                    }

                    dataObjects.timeSlots = data.time_slot_data;

                    for (var i in data.time_slot_data) {
                        var dt = new Date(data.time_slot_data[i]);

                        format = (dt.getHours() < 13 ? dt.getHours() : dt.getHours() - 12) + ':' + dt.getMinutes() + ' ' + (dt.getHours() < 13 ? 'AM' : 'PM');
                        content += "<option value='" + data.time_slot_data[i][0] + "'>" + data.time_slot_data[i][1] + "</option>";
                    }



                    elements.showTimeSelector.find('select').prop('disabled', false);
                    elements.showTimeSelector.find('select').html(content);

                }, function (err) {

                });
        }


    };

    var select_tab = function (tab) {

        $('#movie_select_tab').removeClass('current');
        $('#seat_select_tab').removeClass('current');
        $('#booking_select_tab').removeClass('current');

        $('#movie_select_tab_div').hide();
        $('#seat_select_tab_div').hide();
        $('#booking_select_tab_div').hide();

        $('#'+tab).addClass('current');
        $('#'+tab+'_div').show();
    }



    $('#selectedShowTime').on('change', function () {

        moviedata.selectedValus.timeSlot = $(this).val();
        elements.showTimeSelector.find('select').val($(this).val());
        moviedata.selectedValus.tot_selected_seat_count = 0;
        seatplanfunction();
    });



    var set_reservation_tbody =  function (price_object) {

        var reservation_tbody = '';
        $k = 1;
        for (i = 0; i < price_object.length; i++) {

            if (Number(price_object[i].price) != 0) {

                var cbox = '<select name="category_seat_no[]" class="form-control input-sm input-num-seat category_seat_no cs_'+$k+'">';
                for (var j = 0; j <= elements.reqSeatCount.val(); j++) {
                    cbox += '<option data-price="' + Number(price_object[i].price+moviedata.additional_online_fee) + '" value="' + j + '">' + j + '</option>';
                }
                cbox += '</select>';
                $k++;

                reservation_tbody += '<tr><td class="category__name"  style="vertical-align:middle;">' + price_object[i].category_name + ' (' + Number(price_object[i].price+moviedata.additional_online_fee) + ')</td>' +
                    '<td  style="text-align:center;"><input  name="category_id[]" type="hidden" style="width:60px" id="category_id" value="' + price_object[i].category_id + '"/><input style="display: none" type="text" value="' + Number(price_object[i].price+moviedata.additional_online_fee) + '"  readonly class="price" name="price[]" />' + cbox + '</td>' +
                    '<td  style="widtd:70px;vertical-align:middle;"><input readonly style="width:80px" class="category_total form-control" name="category_total[]" type="text" id="category_total" value=""/></td>' +

                    '</tr>';
            }
        }



        $('.summerytable tbody').html(reservation_tbody);


        $('.category_seat_no').on('change', function () {

            var no = $(this).val().trim();



            if($(this).hasClass("cs_1")){

            }else{
                var cs_1_v = $('.category_seat_no.cs_1').val() - no;
                if(cs_1_v < 0){
                    cs_1_v = 0;
                }
                $('.category_seat_no.cs_1').val(cs_1_v);
                var price = $('.category_seat_no.cs_1').prev('.price').val().trim();
                $('.category_seat_no.cs_1').parents('tr').find('.category_total').val(cs_1_v * price);
            }

            var price = $(this).prev('.price').val().trim();
            $(this).parents('tr').find('.category_total').val(no * price);
            calculate_total();
        });

        $('.category_seat_no:first').val(elements.reqSeatCount.val());
        $('.category_seat_no:first').change();


        $('#selectedShowTime').html('');

        $('#selectedShowTime').html(elements.showTimeSelector.find('select').html());
        $('#selectedShowTime').val(moviedata.selectedValus.timeSlot);



        $('#btn_submit').on('click', function () {

            ga('send', 'event', 'sales', 'click', 'seat_selection');

            moviedata.selectedValus.frm_2_action = 'submit';
            $('#form366').submit();

        });
    }

    $('#btn_back').on('click', function () {
        select_tab('movie_select_tab');
    });

    function reset_booking(){
        select_tab('movie_select_tab');

        moviedata.selectedValus.tot_selected_seat_count = 0;
        $('#time').html('05:00');
        moviedata.booking_id='';
        moviedata.token='';


        $('.selected_seat').removeClass('selected_seat');
        clearInterval(ci);

        $('#summeryModal .modal-body').html('');
        $('#summeryModal .modal-footer').remove('btn-proceed');
    }

    /*
     * Reservation cancel
     * ---------------------------------------------
     * */
    function cancel_reservation() {
        $('#summeryModal .modal-body').html('');
        $('#summeryModal .modal-footer').remove('btn-proceed');
        $('.modelBackground').fadeOut();
        $('#summeryModal').hide();
        $('body').removeClass('modal-open');
        post('buy-tickets-online/ajax_cancellation', {
            booking_id: moviedata.booking_id}, function (jsonData) {
            jAlert(jsonData.message, 'Transaction Status', function () {

                reset_booking();

            });


        }, 'json');
    }


    /*
     * Reservation save
     * ---------------------------------------------
     * */
    function save_reservation() {
        moviedata.selectedValus.frm_2_action = 'save';
        $('#form366').submit();
        $('.modelBackground').fadeOut();
        $('#summeryModal').hide();
        $('body').removeClass('modal-open');

    }

    /*
     * Reservation cancel
     * ---------------------------------------------
     * */
    function transaction_details() {
        var html_price = '', html_selected_seat = '';


        $('.category_seat_no').each(function () {


            var block_name = $(this).parents('tr').find('.category__name').text();
            var unit_price = $(this).prev('.price').val().trim();
            var qty = $(this).val().trim();
            var total = $(this).parents('tr').find('.category_total').val();
            if (Number(qty) > 0) {
                html_price += "<tr>" +
                    "<th>Ticket Type</th>" +
                    "<td>" + block_name + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Unit Price (Rs)</th>" +
                    "<td>" + unit_price + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Qty</th>" +
                    "<td>" + qty + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Amount (Rs)</th>" +
                    "<td>" + total + "</td>" +
                    "</tr>";
            }


        });

        $('.selected_seat').each(function (i) {
            if (i == 0) {
                html_selected_seat += $(this).data('seat_no');
            }
            else {
                html_selected_seat += ', ' + $(this).data('seat_no');
            }
        });

        var theater = elements.theaterSelector.find('select').find("option:selected").text();
        var movie = elements.movieSelector.find('select').find("option:selected").text();
        var time_slot = elements.showTimeSelector.find('select').find("option:selected").text();
        // var theater = $("#selectDate option:selected").text();
        var movie_date = $('#selectDate').val().trim();
        var total = $('#total').html().trim();
        var paid_by = "";
        var discount=$('#discount').html().trim();
        var paid_by_by = $("#paid_by option:selected").text();

        if (paid_by == 1) {
            var total = 'Free';
        }

        var html = "<table class='table table-striped'>" +
            "<tr>" +
            "<th>Theater</th>" +
            "<td>" + theater + "</td>" +
            "</tr>" +
            "<tr>" +
            "<th>Movie</th>" +
            "<td>" + movie + "</td>" +
            "</tr>" +
            "<tr>" +
            "<th>Time slot</th>" +
            "<td>" + time_slot + "</td>" +
            "</tr>" +
            "<tr>" +
            "<th>Movie date</th>" +
            "<td>" + movie_date + "</td>" +
            "</tr>" +
            "<tr>" +
            "<th>Selected Seats</th>" +
            "<td>" + html_selected_seat + "</td>" +
            "</tr>" +
            html_price +
            "";

        if(discount >0){
            html += "<tr>" +
                "<th>Discount (Rs)</th>" +
                "<td>" + Number(discount) + "</td>" +
                "</tr>" ;
        }
        html += "<tr class='totalRow'>" +
            "<th>Total Amount (Rs)</th>" +
            "<td>" + Number(total) + "</td>" +
            "</tr>" +

            "</table>";

            if($('#summeryModal').is(':hidden')){
                $('.modelBackground').fadeIn();
                $('#summeryModal').toggle("slide", { direction: "right" }, 1000);//show model
                $('body').addClass('modal-open');
            }
        
        $('#summeryModal .modal-body').html(html);
        $('#summeryModal .modal-footer #btn-proceed').remove();
        $('#summeryModal .modal-footer').append('<button id="btn-proceed" type="button" class="btn btn-blue">Proceed With Transaction</button>');

        $('#btn-proceed').removeAttr('disabled');
        $('#btn-proceed').on('click', function () {

            ga('send', 'event', 'sales', 'click', 'purchase_confirmation');

            $(this).attr('disabled', 'disabled');
            save_reservation();
        });

        $('#btn-cancel').on('click', function () {
            cancel_reservation();
        });


    }


    function validation_reservation(jsonData) {
        var details = jsonData.details;
        jAlert(details, 'Validation');
    }



    $('#form366').validate({
        errorElement: 'span',
        errorClass: 'validation-advice',
        ignore: "",
        rules: {
            paid_by: {
                required: true
            }
        },
        submitHandler: function (form) {

            if(moviedata.selectedValus.frm_2_action == 'submit'){
                var cs_tot = 0;
                $('.category_seat_no').each(function(){
                    cs_tot += Number($(this).val());
                });

                if(elements.reqSeatCount.val() != cs_tot)
                {
                    jAlert('Invalid ticket type count. You have to select '+elements.reqSeatCount.val()+' seat(s)','Validation');
                }
                else if(moviedata.selectedValus.tot_selected_seat_count != cs_tot || cs_tot==0 ){
                    jAlert('Please select '+moviedata.selectedValus.tot_selected_seat_count+' seat(s)','Validation');
                }else{transaction_details();}
            }
            if(moviedata.selectedValus.frm_2_action == 'save'){



                $('.pageLoaderCont').fadeIn();

                frm_1_data = $('#form366').serialize()+ '&time_slot=' + moviedata.selectedValus.timeSlot+ '&movie=' + moviedata.selectedValus.movie+ '&movie_date=' + moviedata.selectedValus.date+ '&theater=' + moviedata.selectedValus.theater+ '&booking_id=' +moviedata.booking_id;
                $.post(base_url + 'buy-tickets-online/save_reservation', frm_1_data, function (jsonData) {

                    $('.pageLoaderCont').fadeOut();
                    if (jsonData.status) {
                        window.location = base_url+"buy-tickets-online/proceed_payment/"+jsonData.invoice_no;

                    }
                    else {
                        validation_reservation(jsonData);
                    }

                }, 'json');
            }
        }
    });



    function calculate_total() {

        var total = 0;var discount=0;
        $('.category_total').each(function () {
            total += Number($(this).val());
        });

        var offer_code_type= moviedata.offer_code_type,offer_code_value= moviedata.offer_code_value;
        if(Number(offer_code_type)>0 && Number(offer_code_value)>0 && Number(total)>0 ){
            if(offer_code_type==2){
                discount=offer_code_value;
            }
            else if(offer_code_type==1){
                discount=total*offer_code_value/100;
            }
        }

        $('#discount').html(Number(discount));
        $('#total').html(Number(total-discount));
    }

    var seatplanfunction = function(){

        $('.pageLoaderCont').fadeIn();
        post('buy-tickets-online/seat_plan_data',
            {
            'theater':moviedata.selectedValus.theater,
            'movie_date':moviedata.selectedValus.date,
            'time_slot':moviedata.selectedValus.timeSlot,
            'booking_id':moviedata.booking_id,
            'movie':moviedata.selectedValus.movie
        },function (data,text,xhr) {
                $('.pageLoaderCont').fadeOut();
            if(data.message){
                jAlert(data.message, 'Validation' );
            }
            else if (data.seat_plan.length > 0) {


                $('.btn-timer').show();
                select_tab('seat_select_tab');
                set_validation(data);
                set_reservation_tbody(data.price_object);


                $('#theater-img6969').attr("src", window.base_url+'images/theater/cinema/'+data.theater.logo);

            }

            socketMonitor.getReservedSeats(
                null,
                moviedata.selectedValus.movie,
                moviedata.selectedValus.theater,
                moviedata.selectedValus.timeSlot
            );


        },function (err) {

        });
    }



    var onTimeSlotSelect = function () {

        if (moviedata.selectedValus.movie !='' && moviedata.selectedValus.timeSlot !='' && moviedata.selectedValus.theater !=''){
            //moviedata.selectedValus.timeSlot = dataObjects.timeSlots[0];
            $('#submitbtn366').prop('disabled',false);
        }
        else{
            $('#submitbtn366').prop('disabled',true);
        }

    }

    elements.theaterSelector.find('select').change(function (event) {
        moviedata.selectedValus.theater = $(this).val();
        var dt = new Date(elements.datePicket.val());

        var formt = dt.getUTCFullYear()+'-'+(dt.getUTCMonth()+1)+'-'+(dt.getUTCDate());
        onTheaterSelect();
    });

    elements.movieSelector.find('select').change(function (event) {
        moviedata.selectedValus.movie = $(this).val();
        onMovieSelect();

    });

    elements.datePicket.change(function () {
        var dt = new Date($(this).val());
        var formt = dt.getUTCFullYear()+'-'+(dt.getUTCMonth()+1)+'-'+(dt.getUTCDate());
        
        moviedata.selectedValus.date = formt;
        onDateChange();
    });

    elements.showTimeSelector.find('select').change(function (event) {
        moviedata.selectedValus.timeSlot = $(this).val();
        onTimeSlotSelect();
    });

    $('#submitbtn366').on('click',function (event) {
       // moviedata.selectedValus.automate();

        ga('send', 'event', 'sales', 'click', 'ticket_selection');

        var dtat= {'theater':'','movie_date':'','time_slot':'','movie':''}
        event.preventDefault();

        $('#seatSelection699').show();

        var dt = new Date(moviedata.selectedValus.date);

        var months = ['JAN','FEB','MAR','APR','MAY','JUN','JUL','AUG','SEP','OCT','NOV','DEC'];

        var full_dt = ( months[dt.getMonth()])+' '+dt.getDate()+' ,'+dt.getFullYear();

        $('#seat_plane_date_234').text(full_dt);

      $('#selectedShowTime').val( moviedata.selectedValus.timeSlot);
        seatplanfunction();
    });

    var validateCoupon = function () {
        var offer_code=$('#offer_code').val();
        if(offer_code !=''){
            post('buy-tickets-online/validate_coupon', {
                'offer_code':offer_code,
                'movie':moviedata.selectedValus.movie,
                'theater':moviedata.selectedValus.theater
            },function (data,text,xhr) {
               // console.log(data);
                if(data.details){
                    moviedata.offer_code_type=0;
                    moviedata.offer_code_value=0;
                    jAlert(data.details, 'Validation' );
                }
                else {
                    moviedata.offer_code_type=Number(data.discount_type);
                    moviedata.offer_code_value=Number(data.discount_value);
                    jAlert("Coupon code successfully added.", 'Success' );
                    calculate_total();
                   // console.log(moviedata.offer_code_type+'  '+moviedata.offer_code_value);
                }

            },function (err) {

            });
        }

    }

    var removeCoupon = function () {
        moviedata.offer_code_type=0;
        moviedata.offer_code_value=0;
        $('#offer_code').val('');
        calculate_total();
    }

    $('.coupon-code').on('click',function (event) {
        validateCoupon();
    });

    $('.coupon-code-remove').on('click',function (event) {
        removeCoupon();
    });

    socketMonitor.onRecive = function (signal) {

        moviedata.selectedValus.movie,
            moviedata.selectedValus.theater,
            moviedata.selectedValus.timeSlot


        if (signal.info == undefined || signal.data == undefined){
            return;
        }

        if (signal.info.movie_id != moviedata.selectedValus.movie ||
            signal.info.theater_id != moviedata.selectedValus.theater ||
            signal.info.time_slot != moviedata.selectedValus.timeSlot){ return;}

        $('#item-container').find('.hold_seat').removeClass('hold_seat');

        window.selected_seates = [];
        $('.seat_item.selected_seat').each(function ()
        {
            window.selected_seates.push($(this).data('seat_no'))
        });

        var disabled = signal.data.disabled;

        for (var di in disabled){

            if (disabled[di].label.trim() == ''){continue;}

            var elem = $( "div[data-seat_no="+disabled[di].label+"]" );

            if (elem == undefined || elem == null){
                continue;
            }

            elem.addClass('hold_seat');
        }


        for (var i in signal.data.seats){

            if(signal.data.seats[i].label.trim() == ''){continue;}



            var elem = $( "div[data-seat_no="+signal.data.seats[i].label+"]" );


            for (var di in disabled){

                if (disabled[di].label == signal.data.seats[i].label){
                    elem.addClass('hold_seat');
                    isBlocked =true;
                    break;
                }
            }



            if (signal.data.seats[i].label !== null){

                var r = window.selected_seates.indexOf(signal.data.seats[i].label);


                if(r == -1 && (signal.data.seats[i].status == 2 || signal.data.seats[i].status == -5)){//select pending seat except the current user
                    elem.addClass('hold_seat');
                }
                else if(r != -1 && signal.data.seats[i].status == 2){//select selected seat from the current user
                    elem.addClass('selected_seat');
                }

                if(signal.data.seats[i].status == 1){
                    elem.addClass('booked_seat');
                }

            }

        }


    };


    var el = $("#box-seat-plan");
    var tab_st = 'draw';

    $('.required-entry').attr('required', 'required');


    var ctrl = false;
    var shift = false;
    var $seats_with = 26;
    var $space = 30;
    var $seats = [];


    function set_validation(_seat_obj) {

        var $sc = 0;
        $seats = [];

        $('.tbl-block-seat').find('input[type="text"]').each(function () {
            $sc += Number($(this).val());

            $seats.push($(this).val());
        })

        generate_seat_plan(_seat_obj);

    }

    function generate_seat_plan(data1) {

        var data = data1.seat_plan;
        var online_disabled = data1.online_disabled;

        var seat_plan_booked = data1.seat_plan_booked;
        var seat_plan_hold = data1.seat_plan_hold;
        var seat_plan_selected = data1.seat_plan_selected;
        var dataxy = data1.seat_plan_x_y;

        $('.block-plan-container').html('');
        $('#item-container').html('');


        $html_seat_lbl = '<div class="screenImage"></div><div id="item-container"></div>';
        $html_seat = '';



        for ($i = 0; $i < data.length; $i++) {
            //console.log(data[$i]);
            $seat_id = data[$i].id;
            $row = data[$i].row;
            $top = data[$i].y;
            $left = data[$i].x;
            $seat_no = data[$i].label;
            $status = data[$i].status;

            if ($status == 0) {
                $seal_class = ' hide_seat';
            } else {
                $seal_class = '';
            }

            if ($.inArray($seat_no, seat_plan_booked) != -1) {
                $html_seat += '<div data-id="' + $seat_id + '" data-row="' + $row + '" data-seat_no="' + $seat_no + '" class="row_' + $row + $seal_class + ' seat_item booked_seat" style="top:' + $top + 'px;left:' + $left + 'px;" title="' + $seat_no + '">' + $seat_no + '</div>';
            }

            else if ($.inArray($seat_no, seat_plan_hold) != -1 || $.inArray($seat_no, online_disabled) != -1) {
                $html_seat += '<div data-id="' + $seat_id + '" data-row="' + $row + '" data-seat_no="' + $seat_no + '" class="row_' + $row + $seal_class + ' seat_item hold_seat" style="top:' + $top + 'px;left:' + $left + 'px;" title="' + $seat_no + '">' + $seat_no + '</div>';
            }
            else if ($.inArray($seat_no, seat_plan_selected) != -1) {
                $html_seat += '<div data-id="' + $seat_id + '" data-row="' + $row + '" data-seat_no="' + $seat_no + '" class="row_' + $row + $seal_class + ' seat_item selected_seat" style="top:' + $top + 'px;left:' + $left + 'px;" title="' + $seat_no + '">' + $seat_no + '</div>';
            }


            else {
                $html_seat += '<div data-id="' + $seat_id + '" data-row="' + $row + '" data-seat_no="' + $seat_no + '" class="row_' + $row + $seal_class + ' seat_item" style="top:' + $top + 'px;left:' + $left + 'px;" title="' + $seat_no + '">' + $seat_no + '</div>';
            }
        }


        $('.block-plan-container').html($html_seat_lbl);
        $('#item-container').html($html_seat);

        var screenImgMinWidth=Number(dataxy.X) + 30;
        $('.screenImage').css('min-width',screenImgMinWidth+'px');
        $('#item-container').width(Number(dataxy.X) + 30);
        $('#item-container').height(Number(dataxy.Y) + 30)


        $('.seat_item')
            .click(function () {

                if(!$(this).hasClass("booked_seat") && !$(this).hasClass('hold_seat') && !$(this).hasClass('selected_seat') && moviedata.selectedValus.tot_selected_seat_count == elements.reqSeatCount.val()){
                    jAlert("You can only select "+moviedata.selectedValus.tot_selected_seat_count+" seat(s) only");
                }
                else{

                    if (!$(this).hasClass("booked_seat") && !$(this).hasClass('hold_seat')) {
                        $(this).toggleClass("selected_seat");
                        var seat= $(this);

                        var selected = '', status = '';
                        if ($(this).hasClass('selected_seat') && !$(this).hasClass('hold_seat')) {
                            $offset = $(this).position();
                            selected = $(this).data('seat_no');
                            status = 2;

                        }

                        if (!$(this).hasClass('selected_seat') && !$(this).hasClass('hold_seat')) {
                            selected = $(this).data('seat_no');
                            status = 0;
                        }


                        var booking_id = moviedata.booking_id;



                        if( Number($('.seat_item.selected_seat').length-1) == moviedata.ticket_per_user ){

                            seat.removeClass("selected_seat");
                            jAlert("Maximum tickets per user is "+moviedata.ticket_per_user, 'Validation');
                        }

                        else {
                            if (moviedata.selectedValus.timeSlot != '' && moviedata.selectedValus.theater != '' && moviedata.selectedValus.date != '' && moviedata.selectedValus.timeSlot != '' && moviedata.selectedValus.movie != '') {

                                $('.pageLoaderCont').fadeIn();
                                post('buy-tickets-online/selected_seats',
                                    {
                                        selected: selected,
                                        status: status,
                                        booking_id: booking_id,
                                        'time_slot': moviedata.selectedValus.timeSlot,
                                        'theater': moviedata.selectedValus.theater,
                                        'movie_date': moviedata.selectedValus.date,
                                        'movie': moviedata.selectedValus.movie
                                    }, function (data, text, xhr) {
                                        $('.pageLoaderCont').fadeOut();
                                        if (moviedata.booking_id == '') {
                                            startTimer();
                                        }

                                        moviedata.selectedValus.tot_selected_seat_count = $('.seat_item.selected_seat').length;


                                        window.selected_seates = [];
                                        $('.seat_item.selected_seat').each(function () {
                                            window.selected_seates.push(seat.data('seat_no'))
                                        });

                                        moviedata.booking_id = data.booking_id;
                                        moviedata.token = data.token;


                                        socketMonitor.getReservedSeats(data.token, data.movie_id, data.theater_id, data.time_slot);

                                        //console.log(data);

                                    }, function (err) {



                                    });


                            }
                        }
                    }
                }
            });


    }

    function startTimer() {
        var fiveMinutes = 60 * moviedata.BOOKING_CLOSE;
        var display = $('#time');
        startCountDown(fiveMinutes, display);
    }

    function startCountDown(duration, display) {
        var timer = duration, minutes, seconds;

        ci = setInterval(function () {
            minutes = parseInt(timer / 60, 10)
            seconds = parseInt(timer % 60, 10);

            minutes = minutes < 10 ? "0" + minutes : minutes;
            seconds = seconds < 10 ? "0" + seconds : seconds;

            display.html(minutes + ":" + seconds);

            if (--timer < 0) {
                clearInterval(ci);
                reset_booking();
                $('.pageLoaderCont').fadeOut();

            }
        }, 1000);
    }

    function setAvailabilityCount(count,booked,hold){
        $('#available_seats').html("Availabale seat count : "+Number(count));
        window.moviedata.available_seats_for_booking=Number(count);
    }

})(window,$);

