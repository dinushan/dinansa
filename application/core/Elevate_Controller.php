<?php
/**
 * Created by PhpStorm.
 * User: LAYOUTindex
 * Date: 10/10/2017
 * Time: 11:47 AM
 */
class Elevate_Controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        // loading App Settings from the DB
        AppSettings::getAllAppSettingConstants();

        $this->form_validation->set_error_delimiters('<span class="validation-advice">', '</span>');

        if( ! $this->adminauth->logged_in())
        {

            $this->session->set_userdata('redir', current_url());
            redirect('login');
        }

    }
}

