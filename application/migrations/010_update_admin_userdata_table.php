<?php

class Migration_Update_Admin_Userdata_Table extends CI_Migration
{
    public function up()
    {
        $fields = array(
            'zone' => array(
                'type' => 'TEXT',
                'null' => true,
            )
        );

        $this->dbforge->add_column('auth_restaurant_admin_userdata', $fields);
    }

    public function down()
    {
    }
}
