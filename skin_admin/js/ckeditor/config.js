/**
 * @license Copyright (c) 2003-2013, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.html or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
	config.filebrowserBrowseUrl = '/travagoo/js/file_browser/index.php?editor=ckeditor';
	config.filebrowserImageBrowseUrl = '/travagoo/js/file_browser/index.php?editor=ckeditor&filter=image';
	config.filebrowserFlashBrowseUrl = '/travagoo/js/file_browser/index.php?editor=ckeditor&filter=flash';
	

	config.toolbar_popup =
	[
		{ name: 'clipboard',   items : [ 'Source','-','Cut','Copy','Paste','PasteText','PasteFromWord','-','Undo','Redo' ] },
		{ name: 'basicstyles', items : [ 'Bold','Italic','Underline','Strike','Subscript','Superscript','-','RemoveFormat' ] },		
		{ name: 'paragraph',   items : [ 'NumberedList','BulletedList','-','Outdent','Indent','-','Blockquote','-','JustifyLeft','JustifyCenter','JustifyRight','JustifyBlock','-','BidiLtr','BidiRtl' ] },
		'/',		
		{ name: 'styles', items : ['FontSize' , 'TextColor','BGColor' ] },		
		{ name: 'links', items : [ 'Link','Unlink','Anchor' ] },
		{ name: 'insert', items : [ 'Image','Table','HorizontalRule','SpecialChar','Iframe' ] },
		{ name: 'tools', items : ['ShowBlocks' ] }
	];
	
	config.toolbar_Basic =
	[
	 	{ name: 'styles', items : ['Source','/'] },
		{ name: 'basicstyles', items : [ 'Format','Bold','Italic','Underline','-', 'NumberedList','BulletedList','-','RemoveFormat' ] },
		{ name: 'insert', items : ['HorizontalRule' ] },
	];
	
	config.toolbar = 'Basic';
	CKEDITOR.config.format_tags = 'p;h1;h2;h3;h4;h5;h6';
	
};
