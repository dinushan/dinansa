<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>
    <div class="container pageContainer mainPageContainer moviesPage">
        <div class="row">
            <div class="col-xs-12 breadcrumbCont">
                <?php include(APPPATH.'views/frontend/inc/breadcrumb.php'); ?>
            </div>

            <div class="col-xs-12 headWrap">
                <h2 class="heading">Select Movie</h2>
            </div>

            <?php foreach ($movies as $movie):?>
                <div class="col-xs-12 col-sm-6 col-md-4 moviesCont">
                    <div class="moviesWrap">
                        <picture class="img-responsive coverImage">
                            <source srcset="<?php echo base_url('images/movie/'.$movie->landscape_image); ?>" />
                            <img srcset="<?php echo base_url('images/movie/'.$movie->landscape_image); ?>" />
                        </picture>
                        <a href="<?php echo base_url('movie/'.$movie->url_key); ?>" class="imageOverlay"></a>

                        <div class="captionWidgetWrap">
                            <div class="captionTitleWrap">
                                <h3 class="captionTitle"><?php echo $movie->movie_name ?></h3>
                            </div>
                            <div class="releaseDate">In cinemas
                                <?php
                                $timestamp = strtotime($movie->date_release);
                                $newDate = date('d S F', $timestamp);
                                echo $newDate;
                                ?>
                            </div>
                            <div class="iconContainer clearfix">
                                <div class="iconWrapper">
                                    <ul class="iconWrap">
                                        <li>
                                            <a onclick="return getMovieBookButton()" href="<?php echo base_url('buy-tickets-online/').'?movie='.$this->encrypt_lib->encode($movie->id)  ?>" class="btn btn-scope">
                                                Book
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    </div>
<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>