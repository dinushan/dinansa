<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer contactUsPage">
   <div class="row">
      <div class="col-xs-12 breadcrumbCont">
		  <?php include(APPPATH.'views/frontend/inc/breadcrumb.php'); ?>
      </div>
      <div class="col-md-6 contactSide">
        <div class="contactForm">
            <?php
            echo get_message();
            $attributes = array('id' => 'frm-contact');
            echo form_open(current_url(), $attributes);
            ?>
          <div class="form-group formDesc">
            If you wish to contact us via email please fill the following form and we will get in touch with you at the earliest
          </div>
             <div class="form-group">
                <label for="name">Name</label>
                 <input type="text" value="<?php echo set_value('name') ?>" class="form-control input-scope name" name="name" id="name" placeholder="Name">
                 <?php echo form_error('name'); ?>
              </div>
              <div class="form-group">
                  <label for="email">Email</label>
                  <input type="email" value="<?php echo set_value('email') ?>" class="form-control input-scope email" name="email" id="email" placeholder="Email">
                  <?php echo form_error('email'); ?>
              </div>
            <div class="form-group">
                 <label for="phone">Phone</label>
                <input type="text" value="<?php echo set_value('phone') ?>" class="form-control input-scope phone" name="phone" id="phone" placeholder="Contact">
                <?php echo form_error('phone'); ?>
              </div>
              <div class="form-group">
                 <label for="message">Message</label>
               <!--    <input type="text" value="<?php //echo set_value('message') ?>" class="form-control input-scope phone" name="message" id="message" placeholder="Message"> -->
                  <textarea rows="4" value="<?php echo set_value('message') ?>" class="form-control input-scope phone" name="message" id="message" placeholder="Message"></textarea>
                  <?php echo form_error('message'); ?>
              </div>
             <div class="form-group">
              <input class="btn btn-lg btn-block btn-blue contactSubmit" type="submit" value="NEXT">
              </div>                  
          </form>
        </div>
      </div>
      <div class="col-md-6 contactSide">
          <div class="contactDetailsWrap">
              <h6 class="detailTitle">Marketing and advertising</h6>
              <ul class="detailList">
                <li class="detail phone"><a href="tel:<?php echo $sitePageData->marketing_phone_number; ?>" class="detailLabel"><?php echo $sitePageData->marketing_phone_number; ?></a></li>
                <li class="detail email"><a href="mailto:<?php echo $sitePageData->marketing_email; ?>" class="detailLabel"><?php echo $sitePageData->marketing_email; ?></a></li>
              </ul>
          </div>


          <div class="contactDetailsWrap">
              <h6 class="detailTitle">Theater Contact Details</h6>
              <ul class="detailList">
                <li class="detail phone"><a href="tel:<?php echo $sitePageData->theater_phone_number; ?>" class="detailLabel"><?php echo $sitePageData->theater_phone_number; ?></a></li>
                  <li class="detail email"><a href="mailto:<?php echo $sitePageData->theater_email; ?>" class="detailLabel"><?php echo $sitePageData->theater_email; ?></a></li>
              </ul>
          </div>

      </div>
      
     </div>
</div>

    <script>
        var jvalidate = $("#frm-contact").validate({
            ignore: [],
            rules: {
                name: {
                    required: true
                },
                email: {
                    required: true,
                    email: true
                },
                phone: {
                    required: true
                },
                message: {
                    required: true
                }
            }
        });
    </script>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>