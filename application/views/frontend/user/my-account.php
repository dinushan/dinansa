<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer">
	<div class="row">
		<div class="col-xs-12 breadcrumbCont">

            <div class="breadcrumbWrap">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                    <li><a href="<?php echo base_url('user/dashboard'); ?>">Dashboard</a></li>
                </ol>
            </div>

		</div>
		<div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 loginPanelCont">

		</div>
	</div>
</div>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>