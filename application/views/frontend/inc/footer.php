<div class="animatedParent footerParent" data-appear-top-offset='-100'>
<div class="animated fadeInUp">
<section class="pageBottom">

		<div class="triangleShapeWrap">
		
		</div>

	<div class="container">
		<div class="row">
		
			<div class="col-xs-12 col-md-10-col-md-offset-1 partners">
					<div class="logoTopSec">
						<img src="<?php echo base_url() ?>skin/images/scope-cinemas-logo.png" alt="" class="img-responsive logoImg">
					</div>
					<h4 class="logoText">Our Theater Partners</h4>

					<div class="parterSliderWrap">
						<ul class="parterSlider">
							<?php
							$theater_partners = $this->common_model->query("SELECT * FROM theater_partners ORDER BY id");
							foreach ($theater_partners as $theater)
							{
								$title = $theater['title'];
								$image = $theater['image'];
								?>
								<li><img src="<?php echo base_url(THEATER_PARTNER_IMG_PATH.$image); ?>" alt="<?php echo $title; ?>"></li>
								<?php
							}
							?>
						</ul>
					</div>
			</div>

			<?php
			$recent_news = $this->query->query("SELECT * FROM `news` ORDER BY `published_date` DESC LIMIT 3");
			if(count($recent_news) > 0) {
				?>
				<div class="col-xs-12 newsCont">
					<div class="container">
						<div class="row">
							<div class="col-xs-12 padding-zero-mobile">
								<h3 class="newsHead">NEWS</h3>
							</div>
							<?php
							$news_page_url = $this->common_model->get_site_page(array('pg_id' => 9));
							foreach ($recent_news as $news) {
								$title = $news['title'];
								$short_description = $news['short_description'];
								$published_date = $news['published_date'];
								?>

								<div class="col-xs-12 col-md-4 newsWrap padding-zero-mobile">
									<a href="<?php echo base_url($news['url_key']); ?>"
									   class="newsTitle"><?php echo $title; ?></a>
									<div class="newsDate"><?php echo $published_date; ?></div>
									<div class="newsDesc">
										<p><?php echo $short_description; ?></p>
									</div>
								</div>

								<?php
							}
							?>
							<div class="col-xs-12 moreNewsWrap padding-zero-mobile">
								<a class="moreNewsLink" href="<?php echo base_url($news_page_url->url_key); ?>">More
									News</a>
							</div>
						</div>
					</div>
				</div>
				<?php
			}
			?>

		</div>
	</div>
	
</section>

<footer class="clearfix">
	<div class="footerMainSection">
			<div class="container">
				<div class="row">
					<div class="col-xs-12 newsLetterWrap" >

						<div class="newsLetterFormWrap" data-appear-top-offset='-100'>
							<form id="news-subscribe" action="<?php echo site_url('ajax/email/newssubscribe') ?>" method="post" class="newsLetterForm form-inline">
							<h4 class="newsLetterText">Sign up for news letter</h4>
							  <div class="form-group">
							    <div class="input-group">
							      <input type="text" id="nl_email" name="nl_email" class="form-control input-scope" placeholder="YOUR EMAIL">
							      <div class="input-group-addon"> <input type="submit" class="btn btn-submit btn-subscribe" data-wait="...." data-btn_value="Sign Up" value="SUBSCRIBE"/></div>
							    </div>
							    <div class="w-form-done">
						           <p class="alert">You have been successfully added to our mailing list.</p>
						          </div>
								  <div class="w-form-fail">
									  <p class="alert">Email sending failed. Please try again.</p>
								  </div>
							  	</div>

							</form>
						</div>
					</div>
				</div>
			</div>
	</div>

	<div class="footerNavSection">
		<div class="container-fluid">
			<div class="row">
				<div class="col-xs-12 col-sm-3 col-md-2 footerNavWrap">
	                <ul class="footerNav">
						<?php
						$home_page_deatils = $this->common_model->query_row("SELECT * FROM home_page WHERE pg_id = 1");
						?>
						<li><a href="<?php echo base_url(); ?>"><?php echo $home_page_deatils->menu; ?></a></li>

						<?php
						$site_pages = $this->query->query("SELECT * FROM site_page ORDER BY p_order LIMIT 0,4");
						foreach ($site_pages as $page) {
							$url_key = $page['url_key'];
							$menu = $page['menu'];
							?>
							<li><a href="<?php echo base_url($url_key); ?>"><?php echo $menu; ?></a></li>
							<?php
						}
						?>
	                </ul>
	            </div>
	            <div class="col-xs-12 col-sm-3 col-md-2 footerNavWrap">
	                <ul class="footerNav">
						<?php
						$other_pages = $this->query->query("SELECT * FROM site_page WHERE pg_id IN(5,6,7,8,10) ORDER BY p_order");
						foreach ($other_pages as $other) {
							$url_key = $other['url_key'];
							$menu = $other['menu'];
							?>
							<li><a href="<?php echo base_url($url_key); ?>"><?php echo $menu; ?></a></li>
							<?php
						}
						?>
	                </ul>
	            </div>
				<div class="col-xs-12 col-sm-3 col-md-2 footerNavWrap">
					<ul class="footerNav">
						<?php
						$other_pages1 = $this->query->query("SELECT * FROM site_page WHERE pg_id IN(11,12) ORDER BY p_order");
						foreach ($other_pages1 as $other1) {
							$url_key1 = $other1['url_key'];
							$menu1 = $other1['menu'];
							?>
							<li><a href="<?php echo base_url($url_key1); ?>"><?php echo $menu1; ?></a></li>
							<?php
						}
						?>
					</ul>
				</div>
	            <div class="col-xs-12 col-sm-3 col-md-2 footerNavWrap">

	            	<ul class="footerNav social-links">
						<li><a target="_blank" href="<?php echo $setting_data['facebook_url']; ?>" class="facebook"></a></li>
						<li><a target="_blank" href="<?php echo $setting_data['twitter_url']; ?>" class="twitter"></a></li>
						<li><a target="_blank" href="<?php echo $setting_data['pinterest_url']; ?>" class="pinterest"></a></li>
						<li><a target="_blank" href="<?php echo $setting_data['google_plus_url']; ?>" class="google"></a></li>
						<li><a target="_blank" href="<?php echo $setting_data['instagram_url']; ?>" class="instragram"></a></li>
	                </ul>
	            </div>
	            <div class="col-xs-12 col-sm-3 col-md-2 col-md-offset-2 footerNavWrap">
	            	<div class="footerNav footerLogoWrap">
	            		<img class="img-responsive footerLogoImg" src="<?php echo base_url() ?>skin/images/scope-cinemas-logo.png" />
	            	</div>
	            	
	            	<div class="footerNav footerLogoWrap foterPartner">
	            		<img class="img-responsive footerLogoImg" src="<?php echo base_url() ?>skin/images/tnl-logo.png" />
	            	</div>
	            	<div class="footerNav partnerText">
	            	Official Radio Partner
	            	</div>
	            </div>
           
			</div>
		</div>
	</div>
    <div class="footer_baseline">
            <span class="copyright">
                <span class="copy">© <?php echo date("Y"); ?> Scope Cinemas, All Rights Reserved.</span>
            </span> 
            <span class="solution">
                <a href="https://www.layoutindex.com/services/web-design-development" target="_blank" title="Web Design & Mobile App development Company Sri Lanka">Web</a>,
                <a href="https://www.layoutindex.com/services/mobile-app-development" target="_blank" title="Web Design & Mobile App development Company Sri Lanka">Mobile Apps</a> & Ticketing Platform  by
                <a class="layoutIndex" href="http://www.layoutindex.com" target="_blank" title="Design Company Sri Lanka LAYOUTindex">LAYOUTindex</a>
            </span>
        </div>
</footer>
</div>
</div>
<script type="text/javascript">
    /* <![CDATA[ */
    var google_conversion_id = 857372284;
    var google_custom_params = window.google_tag_params;
    var google_remarketing_only = true;
    /* ]]> */
</script>
<script type="text/javascript" src="//www.googleadservices.com/pagead/conversion.js">
</script>
 <script type="text/javascript" src="<?php echo base_url() ?>skin/js/css3-animate-it.js">
</script> 
<noscript>
    <div style="display:inline;">
        <img height="1" width="1" style="border-style:none;" alt="" src="//googleads.g.doubleclick.net/pagead/viewthroughconversion/857372284/?guid=ON&amp;script=0"/>
    </div>
</noscript>

</body>
</html>
