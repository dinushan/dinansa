<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\IOFactory;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style;

class Excel {
	
    public function Spreadsheet() {


    }

    public static function export_excel($title="", $headers=[], $data=[], $filters=[]){

        $title = $title . " - " . date('dS M, Y');

        function increment($val, $increment = 2)
        {
            for ($i = 1; $i <= $increment; $i++) {
                $val++;
            }

            return $val;
        }

        // Create new Spreadsheet object
        $spreadsheet = new Spreadsheet();
        $spreadsheet->getProperties()->setTitle($title);
        $sheet = $spreadsheet->getActiveSheet();

        $header_style_array = [
            'fill' => [
                'fillType' => Style\Fill::FILL_SOLID,
                'color' => array('rgb' => '3598dc')
            ],
            'font'  => [
                'bold' => true,
                'color' => array('rgb' => 'ffffff'),
                'size' => 14
            ],
            'alignment' => [
                'horizontal' => Style\Alignment::HORIZONTAL_CENTER
            ]
        ];

        $sheet->setCellValue('A1', strtoupper($title));
        $sheet->mergeCells("A1:".increment('A', count($headers)-1)."1");
        $sheet->getStyle('A1')->applyFromArray($header_style_array);



        $header_style_array = [
            'fill' => [
                'fillType' => Style\Fill::FILL_SOLID,
                'color' => array('rgb' => '26a69a')
            ],
            'font'  => [
                'bold' => true,
                'color' => array('rgb' => 'ffffff')
            ],
            'alignment' => [
                'horizontal' => Style\Alignment::HORIZONTAL_CENTER
            ],
            'borders' => array(
                'top' => array(
                    'borderStyle' => Style\Border::BORDER_THIN,
                    'color' => array('rgb' => '555555')
                ),
                'bottom' => array(
                    'borderStyle' => Style\Border::BORDER_THIN,
                    'color' => array('rgb' => '555555')
                ),
                'right' => array(
                    'borderStyle' => Style\Border::BORDER_THIN,
                    'color' => array('rgb' => '555555')
                )
            )
        ];

        $char = 'A';
        $count = 3;
        foreach ($headers as $header) {
            $sheet->setCellValue($char.$count, strtoupper($header));
            $sheet->getStyle($char.$count)->applyFromArray($header_style_array);
            $sheet->getColumnDimension($char)->setAutoSize(true);
            $char++;
        }

        $count = 4;
        foreach ($data as $key => $cell_data) {
            $char = 'A';

            foreach ($headers as $col => $header) {
                $cell_value = (isset($cell_data[$col]))? $cell_data[$col] : [ 'value' => "" ];
                $sheet->SetCellValue($char . $count, $cell_value['value']);
                $sheet->getColumnDimension($char)->setAutoSize(true);
                if(isset($cell_data[$col]['styles'])){
                    $styles_arr = [];
                    $styles_arr['borders'] = array(
                        'right' => array(
                            'borderStyle' => Style\Border::BORDER_THIN,
                            'color' => array('rgb' => '555555')
                        ),
                        'bottom' => array(
                            'borderStyle' => Style\Border::BORDER_THIN,
                            'color' => array('rgb' => '555555')
                        )
                    );
                    foreach($cell_data[$col]['styles'] as $style){
                        $split_styles = explode(':', $style);
                        $case = (count($split_styles) == 2)? $split_styles[0]: $style;
                        switch($case){
                            case "top-border": $styles_arr['borders'] = array(
                                'top' => array(
                                    'borderStyle' => Style\Border::BORDER_THIN,
                                    'color' => array('rgb' => '555555')
                                ),
                            ); break;
                            case "bottom-border": $styles_arr['borders'] = array(
                                'bottom' => array(
                                    'borderStyle' => Style\Border::BORDER_THIN,
                                    'color' => array('rgb' => '555555')
                                ),
                            ); break;
                            case "right-border": $styles_arr['borders'] = array(
                                'right' => array(
                                    'borderStyle' => Style\Border::BORDER_THIN,
                                    'color' => array('rgb' => '555555')
                                ),
                            ); break;
                            case "width":
                                $sheet->getColumnDimension($char)->setWidth(explode(':', $style)[1]);
                                break;
                            case "outline": $styles_arr['borders'] = array(
                                'allborders' => array(
                                    'borderStyle' => Style\Border::BORDER_THIN,
                                    'color' => array('rgb' => '555555')
                                ),
                            ); break;
                            case "top-bottom-border": $styles_arr['borders'] =  array(
                                'top' => array(
                                    'borderStyle' => Style\Border::BORDER_MEDIUM,
                                    'color' => array('rgb' => '555555')
                                ),
                                'bottom' => array(
                                    'borderStyle' => Style\Border::BORDER_DOUBLE,
                                    'color' => array('rgb' => '555555')
                                ),
                                'right' => array(
                                    'borderStyle' => Style\Border::BORDER_THIN,
                                    'color' => array('rgb' => '555555')
                                ),
                                'left' => array(
                                    'borderStyle' => Style\Border::BORDER_THIN,
                                    'color' => array('rgb' => '555555')
                                ),
                            ); break;
                            case "bold": $styles_arr['font'] = (isset($styles_arr['font'])? $styles_arr['font'] : []) + [
                                'bold' => true
                            ]; break;
                            case "size": $styles_arr['font'] = (isset($styles_arr['font'])? $styles_arr['font'] : []) + [
                                'size' => explode(':', $style)[1]
                            ]; break;
                            case "center": $styles_arr['alignment'] = [
                                'horizontal' => Style\Alignment::HORIZONTAL_CENTER
                            ]; break;
                            case "decimal":
                                $sheet->getStyle($char.$count)->getNumberFormat()->setFormatCode('#,##0.00');
                                break;
                            case "cellspan": $sheet->mergeCells($char.$count . ":" . increment($char, explode(':', $style)[1]).$count);
                                break;
                            case "color": $styles_arr['font'] = (isset($styles_arr['font'])? $styles_arr['font'] : []) + [
                                'color' => array('rgb' => explode(':', $style)[1])
                            ]; break;
                            case "fill": $styles_arr['fill'] = [
                                'fillType' => Style\Fill::FILL_SOLID,
                                'color' => array('rgb' => explode(':', $style)[1])
                            ]; break;
                        }
                    }

                    $sheet->getStyle($char.$count)->applyFromArray($styles_arr);
                }
                $char++;
            }
            $count++;
        }

        // show filters
        if(count($filters) > 0) {
            $char = 'A';
            $row = $count + 3;
            $label = $char;
            $char++;
            $label_value = $char;

            $sheet->SetCellValue(
                $label . $row, "FILTERS"
            );
            $sheet->mergeCells($label.$row . ":" . increment($label, 3).$row);
            $sheet->getStyle($label . $row)->applyFromArray([
                'font'  => [
                    'bold' => true,
                    'color' => array('rgb' => 'ffffff')
                ],
                'fill' => [
                    'fillType' => Style\Fill::FILL_SOLID,
                    'color' => array('rgb' => 'cb5a5e')
                ],
                'borders' => array(
                    'outline' => array(
                        'borderStyle' => Style\Border::BORDER_THIN,
                        'color' => array('rgb' => '555555')
                    ),
                ),
                'alignment' => [
                    'horizontal' => Style\Alignment::HORIZONTAL_CENTER
                ]
            ]);
            $sheet->getColumnDimension($label)->setAutoSize(true);

            for($i=0;$i<3;$i++){
                $sheet->SetCellValue(
                    increment($label_value, $i) . $row,
                    ""
                );
                $sheet->getColumnDimension(increment($label_value, $i))->setAutoSize(true);
                $sheet->getStyle(increment($label_value, $i) . $row)->applyFromArray([
                    'font'  => [
                        'bold' => true,
                        'color' => array('rgb' => 'ffffff')
                    ],
                    'fill' => [
                        'fillType' => Style\Fill::FILL_SOLID,
                        'color' => array('rgb' => 'cb5a5e')
                    ],
                    'borders' => array(
                        'outline' => array(
                            'borderStyle' => Style\Border::BORDER_THIN,
                            'color' => array('rgb' => '555555')
                        ),
                    ),
                    'alignment' => [
                        'horizontal' => Style\Alignment::HORIZONTAL_CENTER
                    ]
                ]);
            }

            $row = $count + 4;
            $c = 0;
            foreach ($filters as $fill_key => $filter){
                $sheet->SetCellValue(
                    $label . $row,
                    ucfirst(str_replace("_", " ", $fill_key))
                );
                $sheet->getStyle($label . $row)->applyFromArray([
                    'font'  => [
                        'bold' => true,
                        'color' => array('rgb' => '000000'),
                    ],
                    'fill' => [
                        'fillType' => Style\Fill::FILL_SOLID,
                        'color' => array('rgb' => 'eeeeee')
                    ],
                    'alignment' => [
                        'vertical' => Style\Alignment::VERTICAL_TOP
                    ],
                    'borders' => array(
                        'outline' => array(
                            'borderStyle' => Style\Border::BORDER_THIN,
                            'color' => array('rgb' => '555555')
                        ),
                    ),
                ]);
                $sheet->getColumnDimension($label)->setAutoSize(true);
                $filter_value = (is_array($filter)? join(", ", $filter) : $filter);
                $sheet->SetCellValue(
                    $label_value . $row, $filter_value
                );
                $sheet->getStyle($label_value . $row)->applyFromArray([
                    'alignment' => [
                    'vertical' => Style\Alignment::VERTICAL_TOP
                ]
                ]);
                $sheet->getStyle($label_value . $row)->getAlignment($row)->setWrapText(true);
                $sheet->mergeCells($label_value . $row . ":".increment($label_value, 2).$row);
                $sheet->getColumnDimension($label_value)->setAutoSize(true);
                $sheet->getRowDimension($row)->setRowHeight(($c == 0)? 20 : 50);
                $row += 1;
                $c++;
            }
        }



        // Redirect output to a client’s web browser (Xlsx)
        header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
        header('Content-Disposition: attachment;filename="'.$title.'.xlsx"');
        header('Cache-Control: max-age=1');

        header('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header('Last-Modified: ' . gmdate('D, d M Y H:i:s') . ' GMT'); // always modified
        header('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header('Pragma: public'); // HTTP/1.0


        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer->save('php://output');
        exit;


    }

}