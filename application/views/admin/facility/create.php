<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <div class="page-content">

        <h3 class="page-title"><?php echo $page_action == "add" ? 'Add New' : 'Edit' ?> Facility</h3>
        <div class="page-bar">
            <div class="page-toolbar">
                <div class="btn-group pull-right">
                    <a class="btn default" href="<?php echo site_url('admin/' . $slug . '/') ?>"><i
                            class="fa fa-chevron-left"></i> Back</a>

                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12"> <?php echo get_message() ?>
                <!-- BEGIN VALIDATION STATES-->
                <div class="portlet light bordered">

                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <?php
                        $attr = array("id" => "edit_form", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                        echo form_open(current_url(), $attr);
                        ?>
                        <?php
                        if ($page_action == 'edit')
                        {
                            $facility_name = $facility_data->name;
                            $facility_icons = $facility_data->icons;
                        } else {
                            $facility_name  = '';
                            $facility_icons = '';
                        }
                        ?>

                        <h4 class="form-section">General</h4>

                        <div class="form-group">
                            <label class="control-label col-md-3">Name<span class="req">*</span></label>

                            <div class="control-element col-md-4"><input name="name" type="text"
                                                                         class="form-control required-entry"
                                                                         value="<?php echo set_value('name', $facility_name) ?>"/>
                                <?php echo form_error('name'); ?>
                            </div>
                        </div>

                        <?php if ($page_action == 'edit' && $facility_icons != ''): ?>
                            <div class="form-group">
                                <label class="control-label col-md-3">Facility Icon <span class="req">*</span></label>

                                <div class="control-element col-md-4">
                                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                        <tr>
                                            <td><img
                                                    src="<?php echo base_url() . THEATER_FACILITY_IMG_PATH . $facility_icons ?>?<?php echo time() ?>"
                                                    alt="" width="100"/></td>
                                            <td>&nbsp;</td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"></td>
                                        </tr>
                                        <tr>
                                            <td colspan="3"><label>
                                                    <input name="image_del" type="checkbox" value="1"/>
                                                    Image Delete </label>

                                                <div class="help-block font-green">Image Size:<?php echo THEATER_FACILITY_IMG_W ?>
                                                    px &times; <?php echo THEATER_FACILITY_IMG_H ?>px, Image Type: png<br />
                                                    If you want to upload a image, first delete the existing file.
                                                </div>
                                        </tr>
                                        <tr>
                                            <td colspan="3">&nbsp;</td>
                                        </tr>
                                    </table>
                                </div>
                            </div>
                        <?php else: ?>
                            <div class="form-group">
                                <label class="control-label col-md-3">Facility Icon <span class="req">*</span></label>

                                <div class="control-element col-md-4"><input type="file" class=" input-file" name="facility_icons">

                                <span class="help-block font-green">Image Size:<?php echo THEATER_FACILITY_IMG_W ?>
                                    px &times; <?php echo THEATER_FACILITY_IMG_W ?>px, Image Type: png</span>
                                </div>
                            </div>
                        <?php endif; ?>

                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <div class="btn-group">
                                        <input type="hidden" id="hidden-action" name="hidden-action" value="1">
                                        <button type="submit" data-id="1" class="btn btn-submit green">Save & Return
                                        </button>
                                        <button type="submit" data-id="2" class="btn btn-submit btn-default">Save & New
                                        </button>
                                        <button type="submit" data-id="3" class="btn btn-submit btn-default">Save & Edit
                                        </button>
                                    </div>
                                </div>
                         </div>

                        </div>


                        <script type="text/javascript">

                            $('.required-entry').attr('required', 'required');
                            $('#edit_form').validate({
                                errorElement: 'span',
                                errorClass: 'validation-advice',
                                ignore: "",
                                submitHandler: function (form) {
                                    form.submit();
                                }
                            });
                        </script>
                        </form>
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END PAGE CONTENT-->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->