<!-- BEGIN HEADER -->
<div class="page-header navbar navbar-fixed-top">
    <!-- BEGIN HEADER INNER -->
    <div class="page-header-inner">
        <!-- BEGIN LOGO -->
        <div class="page-logo">
            <a href="<?php echo base_url() ?>counter" class="page-logo-text" style="
    color: #fff;
    font-size: 16px;
    padding-top: 12px;
">                <span class="logo-default">SCOPE CINEMAS</span>
            </a>
            <div class="menu-toggler sidebar-toggler">
            </div>
        </div>
        <!-- END LOGO -->



        <!-- BEGIN RESPONSIVE MENU TOGGLER -->
        <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
        </a>
        <!-- END RESPONSIVE MENU TOGGLER -->
        <!-- BEGIN TOP NAVIGATION MENU -->
        <div class="top-menu">
            <?php
            $class_my_account = '';
            $class_user = '';
            if($slug == 'my_account'){$class_my_account = 'active';}
            if($slug == 'users'){$class_user = 'active';}

            if($this->counterauth->logged_in()):
                $userData = $this->counterauth->get_user_by_id($this->counterauth->user_id);
                ?>
                <ul class="nav navbar-nav pull-right">

                    <?php if(
                        $this->counterauth->has_role('101') ||
                        $this->counterauth->has_role('102') ||
                        $this->counterauth->has_role('103') ||
                        $this->counterauth->has_role('104') ||
                        $this->counterauth->has_role('105') ||
                        $this->counterauth->has_role('106')
                    ):?>
                        <li class="dropdown dropdown-system-users">
                            <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true"><i class="fa fa-users"></i></a>
                            <ul class="dropdown-menu dropdown-menu-default">
                                <li class="users"><a href="<?php echo site_url("counter/users") ?>">Users</a></li>
                            </ul>

                        </li>
                    <?php endif; ?>


                    <!-- BEGIN USER LOGIN DROPDOWN -->
                    <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                    <li class="dropdown dropdown-user">
                        <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                            <img alt="" class="img-circle" src="<?php
                            if($userData->picture == ''){
                                echo base_url(PROFILE_IMG_PATH."profile_user.jpg");
                            }else{
                                echo base_url(PROFILE_IMG_PATH.$userData->picture);
                            }

                            ?>"/>
                            <span class="username username-hide-on-mobile"><?php echo $userData->fullname; ?></span>
                            <i class="fa fa-angle-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-menu-default">
                            <li>
                                <a href="<?php echo site_url('counter/my_account') ?>">
                                    <i class="icon-user"></i> My Profile </a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a href="<?php echo site_url('counter/logout') ?>">
                                    <i class="icon-key"></i> Log Out </a>
                            </li>
                            
                        </ul>
                    </li>
                </ul>
            <?php endif; ?>
        </div>
        <!-- END TOP NAVIGATION MENU -->
    </div>
    <!-- END HEADER INNER -->
</div>
<!-- END HEADER -->
<div class="clearfix">
</div>
<!-- BEGIN CONTAINER -->
<div class="clearfix"></div>