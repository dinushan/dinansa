<!-- BEGIN CONTAINER -->
<style>
    @media print{
        .no-print{
            display: none;
        }
        .print-only{
            display: block;
        }
    }

    @media screen {
        .print-only {
            display: none;
        }
    }
</style>

<?php
 $movie_selected    = "All";
 $theater_selected  = "All";

 $grand_total_partron = 0;
 $grand_total_dr      = 0;
 $grand_total_ticket  = 0;
 $grand_total_levy    = 0;
 $grand_total_ent     = 0;

 $print_result = [];
?>
<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title"><?php echo $title ?></h3>
            <?php if($this->adminauth->has_role('1106')):?>

            <div class="row no-print">
                <div class="col-md-12">

                    <form class="form-horizontal " method="POST" action="<?php echo current_url()?>">
                        <div class="portlet box blue portlet-filter">
                            <div class="portlet-title">
                                <div class="caption">Filter</div>
                                <div class="tools">
                                    <button type="submit" class="btn btn-danger btn-sm">Show Report</button>
                                </div>
                            </div>
                            <div class="portlet-body ">
                                <div class="form-body clearfix" style="max-width:500px;">

                                    <div class="form-group">
                                        <div class="col-md-3"><label class="control-label" for="sales_report_period_type">Movie</label></div>
                                        <div class="col-md-9">
                                            <select class="form-control input-sm" name="movie" id="movie368">
                                                <option value="-1">any</option>
                                                <?php foreach ($movies as $mv):?>
                                                    <option value="<?php echo $mv->id;?>" <?php  if($movie == $mv->id) { $movie_selected = $mv->movie_name;echo'selected';} else {echo '';}?> ><?php echo $mv->movie_name;?></option>
                                                <?php endforeach; ?>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-md-3"><label class="control-label" for="sales_report_period_type">Theater</label></div>
                                        <div class="col-md-9" id="match-div">
                                            <select class="form-control input-sm" name="theater">
                                                <option value="-1">any</option>
                                                <?php foreach ($theaters as $th): ?>
                                                    <option value="<?php echo $th->id?>" <?php if($theater == $th->id){ $theater_selected =$th->name;echo 'selected';}else{echo '';} ?> >
                                                        <?php echo $th->name; ?>
                                                    </option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-3"><label class="control-label" for="report_from">From <span class="required">*</span></label></div>
                                        <div class="col-md-9"><input id="report_from" type="text" class="form-control form-control-inline input-medium date-picker input-sm" name="from" placeholder="From" value="<?php echo $from?>"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-3"><label class="control-label" for="report_to">To <span class="required">*</span></label></div>
                                        <div class="col-md-9"><input id="report_to" type="text" class="form-control form-control-inline input-medium date-picker  input-sm" name="to" placeholder="To" value="<?php echo $to ?>"></div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </form>

                    <div class="portlet light bordered">
                        <div class="dataTables_wrapper no-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <button id="exportCVS1910" type="button" class="btn btn-default pull-right">Export to Excel</button>
                                    <button type="button" class="btn btn-default pull-right" onclick="window.print()">Print</button>
                                </div>
                            </div>
                            <div class="table-container">
                                <?php $tot_tax_amount = 0; ?>

                                <div class="clearfix"></div>
                                <div class="table-scrollable">
                                    <table id="tbl-report" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline">
                                        <thead>
                                        <tr class="headings">
                                            <th>DATE</th>
                                            <th>TOTAL PATRONS</th>
                                            <th>TICKET TOTAL</th>
                                            <th>DR COLLECTION</th>
                                            <th>FILM LEVY</th>
                                            <th>ENT TAX</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php foreach($results as $result):?>

                                            <?php
                                            $entertainment     = ($result->film_levy * $result->entartainment_tax)/110;

                                            $theater_share     = ($result->dr_collection * $result->movie_hall_rate)/100;
                                            $distributor_comm  = ($result->dr_collection * $result->distributor_rate)/100;
                                            $supplier_share    = ($result->dr_collection * $result->supplier_rate)/100;
                                            $film_levy_3_4     = ($result->total_patrons * 3.4);
                                            $ent_tax           = ($result->dr_collection * $result->supplier_rate)/100;


                                            //$total_ticket = $result->total_ticket_count;

                                            if($result->payment_method_id < 2){
                                                $film_levy_3_4     = 0;

                                            }


                                            $theater_share      < 0 ? $theater_share      = 0 : $theater_share;
                                            $distributor_comm   < 0 ? $distributor_comm   = 0 : $distributor_comm;
                                            $supplier_share     < 0 ? $supplier_share     = 0 : $supplier_share;
                                            $entertainment      < 0 ? $result->entartain      = 0 : $result->entartain =$entertainment;

                                            ?>
                                            <tr>
                                                <td><?php echo $result->date; ?></td>
                                                <td><?php echo $result->total_patrons; ?></td>
                                                <td><?php echo number_format($result->ticket_total,2); ?></td>
                                                <td><?php echo number_format($result->dr_collection,2); ?></td>
                                                <td><?php echo number_format($result->film_levy,2); ?></td>
                                                <td><?php echo number_format($result->entartain,2); ?></td>
                                            </tr>
                                            <?php
                                        endforeach;
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div><!-- End Table Container-->
                        </div>
                    </div>
                </div>
            </div>

            <div class="container print-only" id="print_summery">
                <div class="row">
                    <p>Theater : <b><?php echo $theater_selected;?></b></p>
                    <p>Movie : <b><?php  echo $movie_selected; ?></b></p>
                    <p>Date: <b><?php echo $from." to ".$to ?></b> </p>
                </div>
                <?php foreach($results as $result):?>
                    <?php

                    if (isset( $print_result[$result->movie_id])){

                        array_push($print_result[$result->movie_id]['data'],[
                            'date'          => $result->date,
                            'patrons'       => $result->total_patrons,
                            'total_ticket'  => $result->ticket_total,
                            'dr_collection' => $result->dr_collection,
                            'levy'          => $result->film_levy,
                            'ent_tax'       => $result->entartain
                        ]);
                    }else{
                    $print_result[$result->movie_id] =['movie'=>$result->movie_name,'data'=> [[
                           'date'           => $result->date,
                           'patrons'        => $result->total_patrons,
                           'total_ticket'   =>  $result->ticket_total,
                           'dr_collection'  => $result->dr_collection,
                           'levy'           => $result->film_levy,
                           'ent_tax'        => $result->entartain
                        ]
                        ]];
                    }
                    ?>
                <?php endforeach; ?>
                <br/>
                <div class="row">
                    <div class="col-xs-2"><b>Film Name</b></div>

                    <div class="col-xs-2"><b>Date</b></div>
                    <div class="col-xs-1 text-right"><b>Patrons</b></div>
                    <div class="col-xs-1 text-right"><b>Total Ticket</b></div>
                    <div class="col-xs-2 text-right"><b>DR Collection</b></div>
                    <div class="col-xs-2 text-right"><b>Levy 6.50</b></div>
                    <div class="col-xs-2 text-right"><b>ENT Tax 10%</b></div>
                </div>
                <div class="row">
                    <?php foreach($print_result as $result){
                          $total_patrons = 0;
                          $total_ticket  = 0;
                          $total_dr      = 0;
                          $total_levy    = 0;
                          $total_ent     = 0;
                        ?>
                        <div class="row">
                            <div class="col-xs-2"><?php echo $result['movie'];?></div>
                            <div class="col-xs-2"></div>
                            <div class="col-xs-1"></div>
                            <div class="col-xs-1"></div>
                            <div class="col-xs-2"></div>
                            <div class="col-xs-2"></div>
                            <div class="col-xs-2"></div>
                        </div>

                            <?php foreach ($result['data'] as $value){
                            $total_patrons += $value['patrons'];
                            $total_ticket  += $value['total_ticket'];
                            $total_dr      += $value['dr_collection'];
                            $total_levy    += $value['levy'];
                            $total_ent     += $value['ent_tax'];
                                ?>
                            <div class="row">
                                <div class="col-xs-2"></div>
                                <div class="col-xs-2"><?php echo $value['date'];?></div>
                                <div class="col-xs-1 text-right"><?php echo $value['patrons'];?></div>
                                <div class="col-xs-1 text-right"><?php echo number_format($value['total_ticket'],2);?></div>
                                <div class="col-xs-2 text-right"><?php echo number_format($value['dr_collection'],2);?></div>
                                <div class="col-xs-2 text-right"><?php echo number_format($value['levy'],2);?></div>
                                <div class="col-xs-2 text-right"><?php echo number_format($value['ent_tax'],2);?></div>
                            </div>
                            <?php }
                                $grand_total_partron += $total_patrons;
                                $grand_total_dr      += $total_dr;
                                $grand_total_ticket  += $total_ticket;
                                $grand_total_levy    += $total_levy;
                                $grand_total_ent     += $total_ent;
                            ?>
                        <hr/>
                        <div class="row">
                            <div class="col-xs-2"></div>
                            <div class="col-xs-2"><b>Total</b></div>
                            <div class="col-xs-1 text-right"><b><?php echo number_format($total_patrons,2);?></b></div>
                            <div class="col-xs-1 text-right"><b><?php echo number_format($total_ticket,2);?></b></div>
                            <div class="col-xs-2 text-right"><b><?php echo number_format($total_dr,2);?></b></div>
                            <div class="col-xs-2 text-right"><b><?php echo number_format($total_levy,2);?></b></div>
                            <div class="col-xs-2 text-right"><b><?php echo number_format($total_ent,2);?></b></div>

                        </div>
                        <hr/>
                    <?php } ?>
                    <div class="row">
                        <div class="col-xs-2"></div>

                        <div class="col-xs-2"><b>Grand Total</b></div>
                        <div class="col-xs-1 text-right"><b><?php echo number_format($grand_total_partron,2);?></b></div>
                        <div class="col-xs-1 text-right"><b><?php echo number_format($grand_total_ticket,2);?></b></div>
                        <div class="col-xs-2 text-right"><b><?php echo number_format($grand_total_dr,2);?></b></div>
                        <div class="col-xs-2 text-right"><b><?php echo number_format($grand_total_levy,2);?></b></div>
                        <div class="col-xs-2 text-right"><b><?php echo number_format($grand_total_ent,2);?></b></div>
                    </div>
                    <hr/>
                </div>
            </div>

            <script>
                var el = $(".portlet-body");

                $('#tournament').change(function(){

                    Metronic.blockUI({
                        target: el,
                        animate: true,
                        overlayColor: 'none'
                    });
                    var tournament_id = $(this).val();

                    $.post('<?php echo base_url('reports/municipal_council/ajax_get_matches') ?>',{tournament_id:tournament_id},function(html)
                    {
                        $('#match-div').html(html);
                        Metronic.unblockUI(el);
                    });


                });


                $('.required-entry').attr('required', 'required');
                $('#edit_form').validate({
                    errorElement: 'span',
                    errorClass: 'validation-advice',
                    ignore: "",
                    submitHandler: function (form) {
                        form.submit();
                    }
                });


                $('.date-picker').datepicker({
                    format: "yyyy-mm-dd",
                    autoclose: true
                });


            </script>
        </div>

        <script>
            jQuery(document).ready(function() {
                TableAjax.init();
            });

            function printPopup(){

                var styles = '<link href="<?php echo base_url() ?>skin_admin/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>'+
                    '<link href="<?php echo base_url() ?>skin_admin/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>'+
                        '<link href="<?php echo base_url() ?>skin_admin/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>';
                var mywindow = window.open('', 'PRINT', 'height=400,width=800');

                mywindow.document.write('<html><head><title></title>');
                //mywindow.document.write(styles);
                mywindow.document.write('<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">');
                mywindow.document.write('</head><body >');
                mywindow.document.write(document.getElementById('print_summery').innerHTML);
                mywindow.document.write('</body></html>');

                mywindow.document.close(); // necessary for IE >= 10
                mywindow.focus(); // necessary for IE >= 10*/

                mywindow.print();
            }

        </script>

        <?php endif; ?>

        <!-- END PAGE CONTENT-->
        <!-- END CONTENT -->

    </div>
</div>
    <!-- END CONTAINER -->
    <script>
        window.param ={
            'theater':'<?php echo $theater; ?>',
            'movie' : '<?php echo $movie; ?>',
            'from' :'<?php echo $from; ?>',
            'to' :'<?php echo $to; ?>'
        };
        (function (window,$) {
            $('#exportCVS1910').click(function(){
                var urlenc = [];
                if (window.param.theater.trim() != ''){
                    urlenc.push('theater='+window.param.theater);
                }
                if (window.param.movie.trim() != ''){
                    urlenc.push('movie='+window.param.movie);
                }
                if (window.param.from.trim() != ''){
                    urlenc.push('from='+window.param.from);
                }
                if (window.param.to.trim() != ''){
                    urlenc.push('to='+window.param.to);
                }


                var myWindow = window.open(window.base_url+'admin/reports/dr-report/export?'+urlenc.join('&'));
            });
        }(window,$))
    </script>