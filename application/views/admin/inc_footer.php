<!-- BEGIN FOOTER -->
<div class="page-footer">
	<div class="page-footer-inner">
		 <?php echo date('Y')?> © <a href="http://www.layoutindex.com">LAYOUTindex</a>. Admin Panel.
	</div>
	<div class="scroll-to-top">
		<i class="icon-arrow-up"></i>
	</div>
</div>
<!-- END FOOTER -->

<script>
	jQuery(document).ready(function() {    
	Metronic.init(); // init metronic core components
	Layout.init(); // init current layout
	QuickSidebar.init(); // init quick sidebar
	Demo.init(); // init demo features
});
</script>

<script language="javascript">
jQuery('#nav li.<?php echo $nav_class ?>').addClass('active');
jQuery('#nav li:first').addClass('first');
jQuery('#nav li:last').addClass('last');
jQuery('#nav li li.active').parents('li').addClass('active');
</script>

<!-- END JAVASCRIPTS -->
</body>
<!-- END BODY -->
</html>