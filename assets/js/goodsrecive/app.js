/**
 * Created by LAYOUTindex on 11/23/2017.
 */
(function(window){
    var app = angular.module('restaurant',[]);



    app.factory('purchaseorderService',function($window){

        $('.date-picker').datepicker({format:'yyyy-mm-dd'});




        for (var i in $window.purchase_invoices){

            for (var j in $window.purchase_invoices[i].items){
                $window.purchase_invoices[i].items[j].reciveqty = $window.purchase_invoices[i].items[j].qty_in_hand;
                $window.purchase_invoices[i].items[j].ischeckd = false;
                for (var k in $window.goodReciveNoteItems){
                    if ($window.goodReciveNoteItems[k].item_id == $window.purchase_invoices[i].items[j].item_id){

                        $window.purchase_invoices[i].items[j].reciveqty = $window.goodReciveNoteItems[k].accepted_qty;
                        if ($window.goodReciveNoteItems[k].accepted_qty == 0 && $window.goodReciveNoteItems[k].qty > 0){
                            $window.purchase_invoices[i].items[j].ischeckd = true;
                        }
                    }
                }

            }
        }
        var selectedOrder = function (order){

            if (!order){
                return [];
            }
            for (var i in order.items){

                order.items[i].reciveqty    = parseInt(order.items[i].qty_in_hand);
                order.items[i].discount_val = parseFloat(order.items[i].discount_val);
                order.items[i].uniq_price   = parseFloat(order.items[i].uniq_price);
                order.items[i].ischeckd     = false;
            }


            return order;

        };

        var getPurchaseinvoices = function () {

            return $window.purchase_invoices;
        }

        return {
            'selectedOrder':selectedOrder,
            'getPurchaseinvoices':getPurchaseinvoices
        };

    });

    app.directive('select2', function (){
        return{
            scope:{
                'selected':"=ngModel",
                'options':'@ngOptions'
            },
            compile:function compile(tElement, tAttrs, transclude) {

                return {
                    post: function preLink(scope, iElement, iAttrs, controller) {


                        var result = scope.options.split(/(track by|in|for|as)/);
                        //console.log(result[4],result[8],result[6]);

                        var objlist = scope.$parent.purchase_invoices;



                        $(iElement).select2({ width: 'auto' });

                        $(iElement).on('change',function () {

                            var selecteid = $(iElement).val();

                            for (var i in objlist){
                                if (objlist[i].id == selecteid){
                                    scope.$apply(function(){

                                        scope.selected = objlist[i];
                                    });

                                    break;
                                }
                            }
                        });

                    }
                }
            }
        }
    });

    app.service('httpService',function($http,$q){

        this.headerObj = {'Content-Type': 'application/json','X-Requested-With':'XMLHttpRequest'};
        var headerObj = this.headerObj;

        this.post = function(route,param){

            return $http({
                method:'POST',
                url:window.base_url+route,
                headers:headerObj,
                data:param
            });

        };

        this.get = function(route,param){

            options ={
                method:"GET",
                url:window.base_url+route,
                headers:headerObj
            };

            if (param !== undefined || param !== null){
                options.params = param;
            }

            return $http(options);

        };
    });


}(window));