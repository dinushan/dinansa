/**
 * Created by LAYOUTindex on 11/23/2017.
 */
(function(window) {
    var app = angular.module('restaurant');

    app.controller('purchaseorderController', purchaseorderController);

    purchaseorderController.$inject = ['$scope','$window','purchaseOrderService'];

    function purchaseorderController($scope,$window,purchaseOrderService) {
        $('.date-picker').datepicker({format:'yyyy-mm-dd'});

        $scope.suppliers = $window.suppliers;
        $scope.selected_supplier = purchaseOrderService.setSupplier();

        $scope.items = $window.items;

        $scope.selectedItems = purchaseOrderService.getSelectedItems();



        $scope.addItem = function () {

            var obj = purchaseOrderService.additem();


            $scope.selectedItems.push(obj);

        };

        $scope.removeItem = function (){

            purchaseOrderService.removeItem(this);

            $scope.selectedItems.splice(this.$index,1);

        }
    }
}(window));