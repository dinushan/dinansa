<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
use Dompdf\Dompdf;

class Auth extends CI_Controller {

    public function __construct(){
        parent::__construct();
        //$this->output->enable_profiler(TRUE);
        $this->load->model('PosUserModel');
    }

    public function index(){
        $this->login();
    }

    public function login(){

        if ($this->session->has_userdata('loggedUser')) {
            $this->redirectUserByType($this->session->userdata('loggedUser'));
        }

        if($this->input->post()){
            // set validation rules
            $this->form_validation->set_rules('username', 'Username', 'required|alpha_numeric');
            $this->form_validation->set_rules('password', 'Password', 'required');

            if ($this->form_validation->run() == false) {

                // validation not ok, send validation errors to the view
                $this->session->set_flashdata('error', $this->lang->line('login_failed'));
                // send error to the view
                redirect(base_url('auth/login'));

            } else {

                // set variables from the form
                $username = $this->input->post('username');
                $password = $this->input->post('password');

                $userData = $this->PosUserModel->resolveUserLogin($username, $password);

                if ($userData['state'] == true) {

                    $userData = $this->PosUserModel->getPosUserByUsername($username);

                    // set session user datas
                    $this->session->set_userdata('loggedUser', $userData);

                    $pos_modules = array_values(loadTableData("pos_modules", "id", ["module_base_url"], ['is_enabled' => 1]));

                    $pos_modules_urls = array_combine(
                        array_column(array_values($pos_modules), 'id'),
                        array_column(array_values($pos_modules), 'module_base_url')
                    );
                    $allowed_modules =  array_map(function($x) use ($pos_modules_urls) { return $pos_modules_urls[$x]; }, json_decode($userData->user_modules));
                    if($userData->id == 1){ // add settings to admin
                        $allowed_modules[] = "settings";
                    }
                    $this->session->set_userdata('allowed_modules', $allowed_modules);

                    $this->redirectUserByType($userData);

                }
                else if($userData['type'] == 'inactive'){
                    $this->session->set_flashdata('error', $this->lang->line('account_disabled'));
                    // send error to the view
                    redirect(base_url('auth/login'));
                }
                else {
                    $this->session->set_flashdata('error', $this->lang->line('login_failed'));
                    // send error to the view
                    redirect(base_url('auth/login'));
                }
            }
        }

        $this->load->view('admin/pos_user/login');
    }

    public function logout() {
        if ($this->session->has_userdata('loggedUser')) {
            $this->session->unset_userdata('loggedUser');
        }
        redirect(base_url('auth/login'));
	}

    public function profile(){
        is_authorized();

        $data['user_data'] = $this->PosUserModel->getUser($this->session->userdata('loggedUser')->id);

        $data['pos_modules'] = loadTableData("pos_modules", "id", ["module_name"], ['is_enabled' => 1]);
        $data['is_edit'] = true;
        if($this->input->post()){

            if($this->input->post('password') || $this->input->post('current_password')){
                $this->form_validation->set_rules('current_password', 'current password', 'trim|required|min_length[6]|callback_check_password');
                $this->form_validation->set_rules('password', 'password', 'trim|required|min_length[6]');
                $this->form_validation->set_rules('password_confirm', 'confirm password', 'trim|required|min_length[6]|matches[password]');
            }

            if ($this->form_validation->run() === false) {

                $this->session->set_flashdata('error', $this->lang->line('error_top'));
                // send error to the view

            } else {
                $this->PosUserModel->updatePassword($this->session->userdata('loggedUser')->id);
                $this->session->set_flashdata('success', $this->lang->line('password_updated'));
                redirect(base_url('/auth/profile'));
            }
        }

        $data['page_top_nav'] = '';
        $data['page_content'] = $this->load->view('admin/pos_user/profile', $data, true);
        $this->parser->parse('common/master_template', $data);

    }

    public function check_password($current=""){
        if (!password_verify($current, $this->session->userdata('loggedUser')->password)){
            $this->form_validation->set_message('check_password', 'Incorrect {field}.');
            return false;
        }
        else{
            return true;
        }
    }

	private function redirectUserByType($userData){
        switch($userData->user_type){
            case 11: redirect(base_url('member/waitingQueue')); break;
            default: redirect(base_url('admin/dashboard'));
        }
    }

}