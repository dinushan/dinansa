<div class="loginPanelWrap white-popup-block">
			<!-- <h4 class="loginDesc">Fill below form to login</h4> -->
			<h1 class="loginTitle">Login</h1>
			<div class="loginPanel">

                <div id="message_signIn"><?php echo get_message(); ?></div>

                <?php
                $attr = array("id" => "login_form", "class" => "loginPanelForm");
                echo form_open(current_url(), $attr);
                ?>
                    <fieldset>
			    	  	<div class="form-group">
			    		    <input class="form-control input-scope phone" placeholder="Your mobile no. Ex: 07XXXXXXXX" name="phone" type="text" id="mobile_699">
			    		</div>
			    		<div class="form-group">
			    			<input class="form-control input-scope password" placeholder="Password" name="password" type="password" id="password_699">
			    		</div>
			    		<div class="form-group">
			    			<a href="<?php echo base_url('user/forgot_password') ?>" class="forgot-pwd">Forgot Password ?</a>
			    		</div>
			    		<div class="form-group loginBtnWrap">
			    		<input class="btn btn-lg btn-block btn-blue loginBtn" onclick="loginSubmit()" type="submit" value="Login">
			    		</div>
			    	</fieldset>
			      	</form>
                  	<div class="form-group fbBtnWrap">
                  		<a onclick="return loginWithFacebookSubmit()" href="<?php echo base_url('auth_oa2/session/facebook') ?>" class="fbBtn btn btn-lg btn-primary btn-block ">
                  			<span class="fbIcon"></span>
                  			<span class="fbBtnText">Login with facebook</span>
                  		</a>
                  	</div>
                   
                    <div class="signUpCont">
                    	<div class="signUpWrap">Don't you have account yet ? <a class="signUpLink" onclick="return signUpButton()" href="<?php echo base_url('user/register')?>"> SIGN UP</a> </div>
                    </div>
      </div>
</div>

<script>
    $('#mobile_699').keyup(function () {

        var elem = $('#mobile_699');
        var ln = elem.val().length;
        if (ln > 0){


            if ( elem.val().slice(0, 1) != 0){

                var starr = elem.val().split("");

                starr.unshift(0);
                elem.val(starr.join(''));
            }
        }

        if (ln > 10){
            var st = elem.val().slice(0, 10);
            elem.val(st);
            return;
        }
    });
</script>


