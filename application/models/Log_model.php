<?php if ( ! defined('BASEPATH')) exit('No direct scripts access allowed');

class Log_model extends CI_Model
{

	public function add($data=array())
    {
        $data['log_date'] = date('Y-m-d H:i:s');
        if(isset($data['user_full_name']))
        {
            $data['user_full_name'] = $data['user_full_name'];
        }else
        {
            if( $this->adminauth->logged_in())
            {
                $data['user_full_name'] = $this->adminauth->fullname;
			    $data['user_id'] = $this->adminauth->user_id;
                $data['user_type'] = 'admin';
            }
            else if( $this->counterauth->logged_in())
            {
                $data['user_full_name'] = $this->counterauth->fullname;
                $data['user_id'] = $this->counterauth->user_id;
                $data['user_type'] = 'counter';
            }

        }

        $insert_id = $this->query->add('audit_log',$data);
		return $insert_id;
    }
}