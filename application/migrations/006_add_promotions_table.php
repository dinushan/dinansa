<?php

class Migration_Add_Promotions_Table extends CI_Migration
{
    public function up()
    {


        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => true,
                    'auto_increment' => true
                ),
                'type' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                ),
                'status' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '15',
                    'default' => 'ACTIVE'
                ),
                'title' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                ),
                'start_date' => array(
                    'type' => 'DATE',
                ),
                'end_date' => array(
                    'type' => 'DATE',
                ),
                'description' => array(
                    'type' => 'TEXT',
                    'null' => true,
                ),
                'card_type' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    'null' => true,
                ),
                'bank' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    'null' => true,
                ),
                'restaurant_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => true,
                    'null' => true,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'on update' => 'NOW()',
                    'null' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
            )
        );

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('promotions');
    }

    public function down()
    {
        $this->dbforge->drop_table('promotions');
    }
}
