

<div class="page-container">

    <?php $this->load->view('admin/inc_sidebar_navigation.php');?> 

    <div class="page-content-wrapper">
		<div class="page-content">
			<?php echo form_open('admin/plane/update',['class'=>'form']); ?>
				<div class="panel panel-default form-horizontal">
					<div class="panel-body">

                        <input type="text" class="form-control" name="id" value="<?php echo $plane->id ?>" required>

						<div class="form-group">
							<label class="control-label col-md-3" for="name">Name<span class="req">*</span></label>
							<div class="control-element col-md-4">
								<p class="text-danger" ></p>
								<input type="text" class="form-control" name="name" value="<?php echo $plane->auth_admin_groups ?>" required>
							</div>    
						</div>	
						
						<div class="form-group">
							<label class="control-label col-md-3" for="name">Description<span class="req">*</span></label>
							<div class="control-element col-md-4">
								<p class="text-danger" ></p>
								<textarea class="form-control" name="description"><?php echo $plane->description ?></textarea>
							</div>    
						</div>	

						<div class="form-group">
							<label class="control-label col-md-3" for="name">Theaters</label>
							<div class="control-element col-md-4">
                                
								<p class="text-danger" ></p>
								<select class="form-control" name="theater">
									<?php foreach ($theaters as $theater) { ?>
									  <option value="<?php echo $theater->id ?>" selected="<?php $plane->theater_id == $theater->id?>"><?php echo $theater->name ?></option>
									<?php } ?>
								</select>
							</div>
						</div>						

					</div>
					<div class="panal-footer clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button type="submit" class="btn btn-primary">Save Edit</button>
                            <a href="<?php echo base_url()?>/admin/plane">Back</a>
						</div>    
					</div>


				</div>
			</form>
		</div>
	</div>
</div>