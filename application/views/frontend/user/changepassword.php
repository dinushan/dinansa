<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer">
	<div class="row">
		<div class="col-xs-12 breadcrumbCont">

            <div class="breadcrumbWrap">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                    <li><a href="<?php echo base_url('user/forgot_password'); ?>">Forgot Password</a></li>
                </ol>
            </div>

		</div>
		<div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 loginPanelCont">

            <div class="page-main-title text-center">
                <h2 class="heading text-gray">Change your password</h2>
            </div>


            <div class="block-form padding-top-20">
                <div><?php echo get_message(); ?></div>

                <?php
                $attributes = array('id' => 'frm-forgotpassword', 'class'=>'form-type-1');
                echo form_open(current_url(), $attributes);
                ?>
                <div class="form-group">
                    <label for="password">Mobile Number</label>
                    <input type="text" class="form-control" id="phone" name="phone" value="<?php echo set_value('phone') ?>" autocomplete="off" placeholder="Mobile Number">
                    <?php echo form_error('phone'); ?>
                </div>
                <div class="form-group">
                    <label for="password">Verification Code</label>
                    <input type="text" class="form-control" id="verification_code" name="verification_code" value="<?php echo set_value('verification_code') ?>" autocomplete="off" placeholder="Verification Code">
                    <?php echo form_error('verification_code'); ?>
                </div>

                <div class="form-group">
                    <label for="password">New Password</label>
                    <input type="password" class="form-control" id="password" name="password" value="<?php echo set_value('password') ?>" autocomplete="off" placeholder="Password">
                    <span class="help-block">Required minimum 6 characters for the passwords</span>
                    <?php echo form_error('password'); ?>
                </div>
                <div class="form-group">
                    <label for="password_conf">New Confirmation Password</label>
                    <input type="password" placeholder="Confirmation Password" name="password_conf" id="password_conf" autocomplete="off" value="<?php echo set_value('password_conf') ?>" class="form-control biz_reg_input">

                    <?php echo form_error('password_conf'); ?>
                </div>
                <div class="form-group">
                    <input type="hidden" name="action" value="forgotPassword" />
                    <button type="submit" class="btn btn-fw button--light">SUBMIT</button>
                </div>

                </form>

            </div>

		</div>
	</div>
</div>

    <script>
        $('#phone').keyup(function () {
            var elem = $('#phone');
            var ln =elem.val().length;

            if (ln > 0){


                if ( elem.val().slice(0, 1) != 0){

                    var starr = elem.val().split("");

                    starr.unshift(0);
                    elem.val(starr.join(''));
                }
            }


            if (ln > 10){
                var st =elem.val().slice(0, 10);
                elem.val(st);
                return;
            }
        });
    </script>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>