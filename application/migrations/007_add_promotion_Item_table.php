<?php

class Migration_Add_Promotion_Item_Table extends CI_Migration
{
    public function up()
    {


        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => true,
                    'auto_increment' => true
                ),
                'promotion_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => true,
                ),
                'item_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => true,
                ),
                'discount_type' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '20',
                    'null' => true,
                ),
                'amount' => array(
                    'type' => 'DECIMAL',
                    'constraint' => '10,2',
                    'null' => true,
                    'default' => 0.00
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'on update' => 'NOW()',
                    'null' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
            )
        );

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('promotion_item');
    }

    public function down()
    {
        $this->dbforge->drop_table('promotion_item');
    }
}
