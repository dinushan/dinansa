<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Query
{
    private $ci;
	
	function __construct($config=array()) {
		$this->ci = &get_instance();
	}
	
	public function count_all_results($tbl,$data=array())
    {
		$this->ci->db->where($data);
		$this->ci->db->from($tbl);
		return $this->ci->db->count_all_results();
    }
	
	public function get($tbl,$data=array())
    {
		$this->ci->db->where($data);
        $sql = $this->ci->db->get($tbl,1);		
		$data = $sql->row();
		
		return $data;
    }
	
	public function set($tbl,$whereData=array(),$data=array())
    {
        $this->ci->db->where($whereData);		
		$this->ci->db->update($tbl,$data);
    }
	public function update($tbl,$whereData=array(),$data=array())
    {
       $this->set($tbl,$whereData,$data);
    }	
	
	public function add($tbl,$data=array(),$isMax = 0,$order_filed ='p_order')
    {			
		if($isMax == 1){
			$this->ci->db->select_max($order_filed);
			$query = $this->ci->db->get($tbl);
			$row = $query->row();
			$p_order = ($row->p_order)+1;
			
			$data[$order_filed] = $p_order;
		}				
		
        $this->ci->db->insert($tbl, $data); 
		$insert_id = $this->ci->db->insert_id();
		return $insert_id;
    }
	
	public function delete($tbl,$whereData=array())
    {				
        $this->ci->db->where($whereData);	
		$this->ci->db->delete($tbl);
    }
	
	public function get_list($tbl,$whereData=array(),$like=array(),$limit=0,$offset=0,$order_by='')
    {
		if(sizeof($like) > 0 ){
			$this->ci->db->like($like); 
		}
		
		$this->ci->db->where($whereData);
		if($order_by != ''){
			$this->ci->db->order_by($order_by);	
		}
		if($limit !=0){
			$this->ci->db->limit($limit,$offset);
		}
		
        $sql = $this->ci->db->get($tbl);
		
		$data = $sql->result_array();
		return $data;
    }	
	
	public function query($sql)
    {				
        $query= $this->ci->db->query($sql);	
		$data = $query->result_array();
		return $data;
    }
	public function query_row($sql)
    {				
        $query= $this->ci->db->query($sql);	
		$data = $query->row();
		return $data;
    }
	
	
	public function get_opt($sql,$key,$lbl,$all=1,$all_lbl="")
    {
		$arr = array();	
		if($all == 1)
			$arr[' '] = $all_lbl;
		else if($all == -1)
			$arr['-1'] = $all_lbl;
						
		$result = $this->query($sql);	
		foreach($result as $r)
		{					
			$arr[$r[$key]] = $r[$lbl];	
		}			
		
		return $arr;
    }
	
	public function get_ids($sql,$id)
    {
		$arr = array();	
		$result = $this->query($sql);
		
		foreach($result as $row){
			array_push($arr,$row[$id]);	
		}
		return $arr;
    }
	
	
	public function get_opt_num($start,$end,$all=1,$all_lbl="")
    {
		$arr = array();	
		if($all == 1)
			$arr[' '] = $all_lbl;
		else if($all == -1)
			$arr['-1'] = $all_lbl;
			

		for($i=$start; $i<=$end;$i++)
		{					
			$arr[$i] = $i;	
		}			
		
		return $arr;
    }
}