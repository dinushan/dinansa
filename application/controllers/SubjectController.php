<?php

defined('BASEPATH') OR exit('No direct script access allowed');
use Illuminate\Database\Capsule\Manager as DB;
use Illuminate\Support\Collection;

/**
 * Created by PhpStorm.
 * User: LAYOUTindex
 * Date: 10/16/2017
 * Time: 6:21 PM
 */



class SubjectController extends CI_Controller{

    private $pageDate = [];

    public function __construct()
    {
        
        parent::__construct();

        $this->pageData['slug'] = 'subject';
        $this->pageData['title'] = 'Manage Subjects';
        $this->pageData['nav_class'] = 'cinema';

        $this->load->model('Subject');
    }

    public function index(){
        $this->load->view('admin/inc_head', $this->pageData);
        $this->load->view('admin/manage_subjects', $this->pageData);
        $this->load->view('admin/inc_footer');
    }

    public function create(){

       $name = $this->input->post('name');

        $subject = new Subject;
        $subject->subject_name = $name;
        $subject->save();

    }

    public function edit(){

        $id = $this->input->post('id');
        $name= $this->input->post('name');

        $subject =Subject::find($id);
        $subject->subject_name = $name;
        $subject->save();
    }

    public function remove(){

    }
}
