var $j = jQuery.noConflict();

function setLocation(url){
    window.location.href = url;
}

function confirmSetLocation(message, url){
    if( confirm(message) ) {
        setLocation(url);
    }
    return false;
}

function deleteConfirm(message, url) {
    confirmSetLocation(message, url);
}

function setElementDisable(element, disable){
    if(jQuery(element)){
        jQuery(element).disabled = disable;
    }
}

function MM_openBrWindow(theURL,winName,features) { //v2.0
  window.open(theURL,winName,features);
}

$j(document).ready(function(){		
	
	$j('.grid tbody tr').hover(function(){
		$j(this).addClass('on-mouse');						
	},
	function(){
		$j(this).removeClass('on-mouse');
	});
	
	$j(".grid tbody tr:even").addClass('even');	
	
	//package details
	$j('.tab_nav li a').click(function(){
		$j('.tab_nav li').removeClass('active');
		$j(this).parent().addClass('active');
		var currentTab = $j(this).attr('href');
		$j('.tab-info div.tabs').hide();
		$j(currentTab).show();
		return false;
	});
	$j('.tab_nav li:first a').click();
	
	//package details
	$j('.tab_cat_nav li a').click(function(){
		$j('.tab_cat_nav li').removeClass('active');
		$j(this).parent().addClass('active');
		var currentTab = $j(this).attr('href');
		$j('.tab_cat-info div.tabs').hide();
		$j(currentTab).show();
		//return false;
	});
	if(location.hash==""){
		$j('.tab_cat_nav li:first a').click();
	}else{
		$j('.tab_cat_nav li a[href="'+location.hash+'"]').click();
	}
	
	
});

var cropzoom;	
function cropZoom(file,crop_ele,imgW,imgH,imgCW,imgCH){
	
	var minW = Math.round(imgCW/9.5);
	var minH = Math.round(imgCH/9.5);	
	
	var selH = Math.round(660/(imgCW/imgCH));
	
	//console.log(minW+" "+minH+" "+selH);
	
	cropzoom = jQuery(crop_ele+' .crop_container').cropzoom({
		width:660,
		height:510,
		bgColor: '#000',
		zoomSteps:10,
		rotationSteps:10,
		selector:{      
		   w : minW,
		   h : minH,
		  centered:true,
		  borderColor:'#ae4646',
		  borderColorHover:'#2f628c'
		},
		image:{
			source:file,
			width:imgW,
			height:imgH,
			minZoom:10,
			maxZoom:150,
		}
	});
	cropzoom.setSelector(0,0,660,selH,true);	
}
function doCrop(file,crop_ele,imgCW,imgCH){
	jQuery('.crop-block .loader').show();	
	jQuery(crop_ele+' .cimg').load(function () {		
		jQuery('.crop-block .loader').hide();
		cropZoom(file,crop_ele,jQuery(this).width(), jQuery(this).height(),imgCW,imgCH);
	}).error(
	  function() {
		
	  }
	).attr('src', file);
	
}


var up;
function imageUploader(_ele_upload,_frm_upload,_ajax_url,_base_url){
	var imgArr = [];
	if(up != undefined){
		up.destroy();
	}
	
	var imgCW;
	var imgCH;
			
	up = $j(_frm_upload).fileUploadUI({
		uploadTable: $j(_ele_upload+' #edit_files'),
		downloadTable: $j(_ele_upload+' #edit_files'),
		dropZone: $j('.frm_uploader-block'),
		sequentialUploads: true,
		buildUploadRow: function (files, index) {
			return $j('<tr><td class="file_upload_preview_td"><div class="file_upload_preview"><\/div><\/td>' +  
                '<td class="file_upload_progress"><div><\/div><\/td>' +                
                '<td class="file_upload_cancel">' +
                '<button class="upload-cancel ui-state-default ui-corner-all" title="Cancel">Cancel' +
                '<\/button><\/td><\/tr>');
		},
		buildDownloadRow: function (file) {
			imgCW = file.img_w;
			imgCH = file.img_h;
			
			var w = file.w;
			var h = file.h;
			
   			var factor = Math.min ( 80 / w, 80 / h);   
    		var vW = Math.round (w * factor);
    		var vH = Math.round (h * factor);
				
			return $j('<tr><td class="file_upload_preview_td"><div class="file_upload_preview"><img src="'+_base_url+'media/temp/'+file.filename+'" width="'+vW+'" height="'+vH+'" \/><\/div><\/td>');
		},		
		beforeSend: function (event, files, index, xhr, handler, callBack) {
        	var regexp = /\.(jpeg)|(jpg)$/i;
			if (!regexp.test(files[index].name)) {
				
				$j('#messages').html('<ul class="messages"><li class="err-msg"><ul><li>Only Images Allowed.</li></ul></li></ul>');
				
				handler.uploadRow.find('.file_upload_progress').html('');
				setTimeout(function () {
					handler.removeNode(handler.uploadRow);
				}, 10000);
				return;
			}				
			callBack();
		}
		
		,onComplete:function (event, files, index, xhr, handler) {
			var json = handler.response;
			
			if(json.status == "success"){
					imgArr.push( json.filename );		
			}else if(json.status == "error-maximumfiles"){
					$j(_ele_upload+' #edit_files tr').remove();					
			}else if(json.status == "error"){
				//'Edting Error try to save again.'
				$j('#messages').html('<ul class="messages"><li class="err-msg"><ul><li>'+json.message+'</li></ul></li></ul>');
				
			}
			
			
		}
		,onCompleteAll:function (files) {
			
			doCrop(_base_url+'media/temp/'+imgArr[0],'.crop-block',imgCW,imgCH);
			
			$j(".file_upload_preview img").unbind().bind("click",function(){
				cropzoom.restore();													 
				doCrop($j(this).attr('src'),'.crop-block',imgCW,imgCH);
			});
			
			$j(_ele_upload+' .crop').unbind().bind('click',function(){
					var _self = $j(this);
					
					if($j('.crop_container').html() != ''){ 
						
						_self.hide();						
						_self.parent().append('<img src="'+_base_url+'skin/adminhtml/images/ajax-loader.gif" />');
						
						cropzoom.send(_ajax_url,'POST',{'param':'upload'},function(data){
							var rta = jQuery.parseJSON(data);
							
							_self.show();
							_self.parent().find('img').remove();
							
							if(rta.status == "success"){
								
								$j('#messages').html('<ul class="messages"><li class="success-msg"><ul><li>The image has been croped.</li></ul></li></ul>');
								
							}else if(rta.status == "error-maximumfiles"){	
								$j('#messages').html('<ul class="messages"><li class="err-msg"><ul><li>'+rta.message+'</li></ul></li></ul>');
							}
							
						});
				
					}else{
						$j('#messages').html('<ul class="messages"><li class="err-msg"><ul><li>Please upload and select a image.</li></ul></li></ul>');
					}
			});
		}
		
	});	
}


function setImageToCanvas(_canvasId,_image,_w,_h){

	// opens the image file and displays it on the canvas
	
	var c=document.getElementById(_canvasId);
	var ctx=c.getContext("2d");
	
	var img = new Image();
	img.src = _image;
	img.onload = function() {
		ctx.drawImage(img,0,0,_w,_h);
	};
}

function setExtraOptions(val){
	$j("tr.extOp").hide();
	$j("tr."+val+"EO").css('display','table-row');
}
function setExtraOptionsFilter(val){
	$j("tr.extOpF").hide();
	$j("tr."+val+"_fEO").css('display','table-row');
}