var $j = jQuery.noConflict();

function page_loader(_v){
	if(_v == 'hide'){
		$('.page_loader').hide();
	}else{
		$('.page_loader').show();
	}
}

function setLocation(url){
    window.location.href = url;
}
function popupWindow(url){
	var ww = $j(window).width();
	var left = (ww - 980)/2;
   window.open(''+url,'mywin',
'left='+left+',top=0,width=980,height=600,toolbar=0,resizable=1,scrollbars=1');
}

function redirectConfirm(message,title, url) {
 	jConfirm( message, title,function(result){
		if(result){			
			setLocation(url);
		}
	});
}

function parseDate(str,format) {
	if(typeof(format) == 'undefined'){// dd/mm/yyyy
    	var mdy = str.split('/')
    	return new Date(mdy[0], mdy[1]-1,mdy[2]);
		
	}else if(format == 'dd-mm-yyyy'){
		var mdy = str.split('-')
    	return new Date(mdy[2], mdy[1]-1,mdy[0]);
	}
	else if(format == 'yy-mm-dd'){
		var mdy = str.split('-')
    	return new Date(mdy[0], mdy[1]-1,mdy[2]);
	}
}

function daydiff(first, second) {
    return Math.abs(second-first)/(1000*60*60*24)
}

function bookingRedirectPopup(message,title, url) {
	$j('#frmBookingReject').attr('action',url);
	
	jConfirm( message, title,function(result){
		if(result){	
			$j('#element_tour_reject_pop_up').bPopup({
				modalClose: true,
				opacity: 0.6,
				positionStyle: 'fixed', //'fixed' or 'absolute'
				onClose: function() { 
					$j('#element_tour_reject_pop_up').find('.validation-advice').remove()
				}
			},	
			function() {
				var vform = new varienForm('frmBookingReject');
			});
		}
	});
}
function viewRejectReason(url) {	
	
	$j('#element_tour_reject_reason_pop_up').bPopup({
		modalClose: true,
		opacity: 0.6,
		contentContainer:'#element_tour_reject_reason_pop_up .content',
        loadUrl: url,
		positionStyle: 'fixed'
    });
}

function deleteConfirm(message,title, url,ispost,ele)
{
 	jConfirm( message, title,function(result)
	{
		if(result){
			if(ispost == true){
				jQuery(ele).fadeOut('slow')
				jQuery.post(url,function(data){
									 
				});
			}else{
				setLocation(url);
			}
		}
	});
}

function set_select2(){
	var cb_s2_placeholder = '';
	if(jQuery('.cb_s2').has('placeholder')){
		cb_s2_placeholder = jQuery('.cb_s2').attr('placeholder');
	}
	jQuery('.cb_s2').select2({ placeholder: cb_s2_placeholder});
}

function createEditor(ele){
	jEditor(ele);
}

jQuery(document).ready(function(e) {
    jQuery('.subNav-tab').find('li:first').addClass('first');
	jQuery('.subNav-tab').find('li:last').addClass('last');
	set_select2();
	jQuery('table.oe tbody tr:even').addClass('even');
	jQuery('table.oe tbody tr:odd').addClass('odd');
	
	jQuery('table.fl tbody tr:first').addClass('first');
	jQuery('table.fl tbody tr:last').addClass('last');
		
	$j('.tab-info div.tabs').hide();
	$j('.tab_nav li a').click(function(){
		$j('.tab_nav li').removeClass('active');
		$j(this).parent().addClass('active');
		var currentTab = $j(this).attr('href');
		$j('.tab-info').find('div.tabs').hide();
		$j('.tab-info').find(currentTab+'-tab').show();
	
		//return false;
	});
	var w_hash = window.location.hash;

	if(w_hash != ''){
		if(w_hash == '#upload_images'){			
			$j($j('.tab_nav li:first a').attr('href')+'-tab').show();
		}else{
			$j('.tab_nav li').find('a[href="'+w_hash+'"]').click();
		}
	}else{
		$j('.tab_nav li:first a').click();
	}
});

function setTabErrors(ele){
	$j(ele+' li').removeClass('tab-error');
					
	$j(ele).find('a').each(function(i){
		_self = $j(this);									   
		_link = $j(this).attr('href');	 						
								
		_length = $j(_link+'-tab').find('.validation-advice').filter(function() {
			return $j(this).css('display') !== 'none';
		}).length;
		
		if(_length > 0 ){
			$j(this).parent().addClass('tab-error');							
		}						
	 });	
}
function setReqFill(ele){
	$j(ele+' li').removeClass('not-completed');
					
	$j(ele).find('a').each(function(i){
		_self = $j(this);									   
		_link = $j(this).attr('href');	 						

		_length = $j(_link+'-tab').find('.req-entry').filter(function() {
			if($j(this).val() == ""){
			return $j(this).css('display') !== 'none';
			}
						
		}).length;
		
		if(_length > 0 ){
			$j(this).parent().addClass('not-completed');							
		}						
	 });	
}

function get_mag_gray_style(){
	var featureOpts = [
		{
		  featureType: 'all',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}
		,{
		  featureType: 'landscape',
		  stylers: [
			{ visibility: 'on'},{ color: '#ffffff' }	
		  ]
		}
		,{
		  featureType: 'landscape',
		  elementType: 'labels',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}
		
		,{
		  featureType: 'administrative.country',
		  elementType : 'labels',
		  stylers: [
			{ visibility: 'off'}
	
		  ]
		}
		,{
		  featureType: 'administrative.country',
		  elementType : 'geometry.stroke',
		  stylers: [
			{ visibility: 'on'},
			{ color: '#888888' },
			{ weight: 1 }
	
		  ]
		}
		,{
		  featureType: 'water',
		  stylers: [
			{ visibility: 'on' },
			{ color: '#d6d6d6' }
		  ]
		}
		,{
		  featureType: 'water',
		  elementType: 'labels',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}	
	  ];
	  
	  return featureOpts;
}

function get_mag_style(){
	var featureOpts = [
		{
		  featureType: 'all',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}
		,{
		  featureType: 'landscape',
		  stylers: [
			{ visibility: 'on'},
			{ color: '#ffffff' }
	
		  ]
		}
		,{
		  featureType: 'landscape',
		  elementType: 'labels',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}
		,{
		  featureType: 'administrative.country',
		  elementType : 'labels',
		  stylers: [
			{ visibility: 'off'}
	
		  ]
		}
		,{
		  featureType: 'administrative.country',
		  elementType : 'geometry.stroke',
		  stylers: [
			{ visibility: 'on'},
			{ color: '#888888' },
			{ weight: 1 }
	
		  ]
		}
		,{
		  featureType: 'water',
		  stylers: [
			{ visibility: 'on' },
			{ color: '#80d1f1' }
		  ]
		}
		,{
		  featureType: 'water',
		  elementType: 'labels',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}	
	  ];
	  
	  return featureOpts;
}

var map;


function setMarkers(map, locations,type,zoom) {
	var marker_pin = base_url+"skin/images/pin.png";
	var marker_pin_selected = base_url+"skin/images/pin_selected.png";
	var marker_pin_airport = base_url+"skin/images/pin_airport.png";

	 var geocoder = new google.maps.Geocoder();

     var pin_image = new google.maps.MarkerImage(marker_pin);
     var pin_airport_image = new google.maps.MarkerImage(marker_pin_airport);
	 
     var shape = {
          coord: [1, 1, 1, 20, 18, 20, 18 , 1],
          type: 'poly'
     };
	 
     for (var i = 0; i < locations.length; i++) {
          	var loc = locations[i];
			var markerLat = loc[0];
			var markerLng = loc[1];
		  	var country = loc[2];
			var state = loc[3];
			var city = loc[4];
		
			icon = pin_image;
			
			
			var address = '';
			var lbl = '';
			
			if(type == 'world'){
				 address = country;
				 lbl  = country;
			}else if(type == 'country'){
				 address = country;
				 lbl  = country;
			}else if(type == 'state'){
				 address = state+','+country;
				 lbl  = state;
			}else if(type == 'city'){
				 if(state == ''){
					 address = city+','+country;
				 }else{
					 address = city+','+state+','+country;
				 }
				 
				 lbl  = city;
			}
						
			//console.log(type);
			//console.log(lbl);
			
			var center_map = new google.maps.LatLng(markerLat,markerLng);
			
			var marker = new google.maps.Marker({
				position: center_map,
				map: map,
				icon: icon
			});
			
			 var label = new Label({map: map});
			  label.set('zIndex', 1234);
			  label.bindTo('position', marker, 'position');
			  label.set('text', lbl);
			  
			   	
     }
}
function country_map(ele,locations,type,centerLat,centerLng,zoom,style,panControlSt)
{
	var center_map = new google.maps.LatLng(centerLat,centerLng);
	var MY_MAPTYPE_ID = 'custom_style';	
	var featureOpts = get_mag_style();
	
	if(typeof(style) != 'undefined'){
		if(style == 'gray'){
			var featureOpts = get_mag_gray_style();	
		}		
	}
	if(typeof(panControlSt) == 'undefined'){
		panControlSt = true;
	}
		
	if(type == 'world')
	{
		var mapOptions = {
			zoom: zoom,
			center: center_map,
			disableDefaultUI: true,
			panControl: panControlSt,
			panControlOptions: {
			  position: google.maps.ControlPosition.LEFT_CENTER
			},
			draggable: true,
			zoomControl: true,	
			zoomControlOptions: {
			  style:google.maps.ZoomControlStyle.SMALL
			  ,position: google.maps.ControlPosition.LEFT_CENTER
			},
			scrollwheel: false,
			mapTypeId: MY_MAPTYPE_ID
		};
		
		map = new google.maps.Map(document.getElementById(ele), mapOptions);
		
		
		var styledMapOptions = {name: 'Custom Style'};	
		var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);	
		map.mapTypes.set(MY_MAPTYPE_ID, customMapType);				
		setMarkers(map, locations,type,zoom);
		
		
	}
	else{	
				
		var mapOptions = {
			zoom: zoom,
			center: center_map,
			disableDefaultUI: true,
			panControl: panControlSt,
			panControlOptions: {
			  position: google.maps.ControlPosition.LEFT_CENTER
			},
			draggable: true,
			zoomControl: true,	
			zoomControlOptions: {
			  style:google.maps.ZoomControlStyle.SMALL
			  ,position: google.maps.ControlPosition.LEFT_CENTER
			},
			scrollwheel: false,
			mapTypeId: MY_MAPTYPE_ID
		};
		
		map = new google.maps.Map(document.getElementById(ele), mapOptions);
		
		var styledMapOptions = {name: 'Custom Style'};	
		var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);	
		map.mapTypes.set(MY_MAPTYPE_ID, customMapType);	
			
		setMarkers(map, locations,type,zoom);	 
	}
	
	google.maps.event.addListener(map, 'center_changed', function() {
		var posMapC = this.getCenter();	
		//console.log(posMapC.lat());
		if(this.b.id == 'map-v-canvas'){	
			$j('#map_v_lat').val(posMapC.lat());
			$j('#map_v_lng').val(posMapC.lng());
		}
		else if(this.b.id == 'map-t-canvas'){								
			$j('#map_t_lat').val(posMapC.lat());
			$j('#map_t_lng').val(posMapC.lng());
		}
		else{			
			$j('#map_lat').val(posMapC.lat());
			$j('#map_lng').val(posMapC.lng());
		}
	});
	
	 google.maps.event.addListener(map, 'zoom_changed', function() {
		var zoomLevel = this.getZoom();	 
		if(this.b.id == 'map-v-canvas'){								
			$j('#map_v_zoom').val(zoomLevel);
		}
		else if(this.b.id == 'map-t-canvas'){								
			$j('#map_t_zoom').val(zoomLevel);
		}
		else{
			$j('#map_zoom').val(zoomLevel);
		}
	});	
}

function word_map(ele,locations,type,lat,lng,zoom){
	var map;
	//console.log(lat);
	var center_map = new google.maps.LatLng(lat,lng);
		

	//var center_map = new google.maps.LatLng(7.284459, 80.637459);
	
	var MY_MAPTYPE_ID = 'custom_style';	
	var featureOpts = get_mag_style();
	
	var mapOptions = {
		zoom: zoom,
		center: center_map,
		disableDefaultUI: true,
		zoomControl: true,
		scrollwheel: false,
		mapTypeId: MY_MAPTYPE_ID
	};
	
	map = new google.maps.Map(document.getElementById(ele), mapOptions);
	
	var styledMapOptions = {name: 'Custom Style'};	
	var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);	
	map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
	setMarkers(map, locations,type,zoom);	 
	
	var geocoder = new google.maps.Geocoder();
    var image = new google.maps.MarkerImage(base_url+"skin/adminhtml/images/pin.png");
	
	google.maps.event.addListener(map, 'center_changed', function() {
		var posMapC = map.getCenter();
		//console.log(posMapC.lb);
		$j.post(base_url+'admin/settings/settings/set_world_map_position',{'world_map_lat':posMapC.lb
				,'world_map_lng':posMapC.mb});
	  });
	
	for (var i = 0; i < locations.length; i++)
	{
		var loc = locations[i];
		var country = loc[0];
		var state = loc[1];
		var city = loc[2];
		var address = '';
		var lbl = '';
		if(type == 'country'){
			 address = country;
			 lbl  = country;
		}else if(type == 'state'){
			 address = state+','+country;
			 lbl  = state;
		}else if(type == 'city'){
			 if(state == ''){
				 address = city+','+country;
			 }else{
				 address = city+','+state+','+country;
			 }
			 
			 lbl  = city;
		}
		
		geocoder.geocode({ 'address': address}, (function(name,lbl) {
				return function(results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						
						//if(maker_on == true){
							var marker = new google.maps.Marker({
								position: results[0].geometry.location,
								map: map,
								icon: image
							});
						//}
						
						
						//if(label_on == true){
						  var label = new Label({map: map});
						  label.set('zIndex', 1234);
						  label.bindTo('position', marker, 'position');
						  label.set('text', lbl);
						  
						  google.maps.event.addListener(map, 'zoom_changed', function() {
						    var zoomLevel = map.getZoom();
							//if(type != 'country'){
								if(zoomLevel > zoom - 2){
									label.set('text', lbl);
								}else{
									label.set('text', '');
								}
							//}
							
							if(map.b.id == 'map-v-canvas'){								
								$j('#map_v_zoom').val(zoomLevel);
							}else{
								$j('#map_zoom').val(zoomLevel);
							}
							
							$j.post(base_url+'admin/settings/settings/set_world_map_position',{'world_map_zoom':zoomLevel});
							
						  });
						  
						  
						  //label.bindTo('text', marker, 'position');	
						//}
					}
				};
			}(address,lbl)));
		}
}

function base64_decode (data) {
  // http://kevin.vanzonneveld.net
  // +   original by: Tyler Akins (http://rumkin.com)
  // +   improved by: Thunder.m
  // +      input by: Aman Gupta
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   bugfixed by: Onno Marsman
  // +   bugfixed by: Pellentesque Malesuada
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +      input by: Brett Zamir (http://brett-zamir.me)
  // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // *     example 1: base64_decode('S2V2aW4gdmFuIFpvbm5ldmVsZA==');
  // *     returns 1: 'Kevin van Zonneveld'
  // mozilla has this native
  // - but breaks in 2.0.0.12!
  //if (typeof this.window['atob'] == 'function') {
  //    return atob(data);
  //}
  var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
  var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
    ac = 0,
    dec = "",
    tmp_arr = [];

  if (!data) {
    return data;
  }

  data += '';

  do { // unpack four hexets into three octets using index points in b64
    h1 = b64.indexOf(data.charAt(i++));
    h2 = b64.indexOf(data.charAt(i++));
    h3 = b64.indexOf(data.charAt(i++));
    h4 = b64.indexOf(data.charAt(i++));

    bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;

    o1 = bits >> 16 & 0xff;
    o2 = bits >> 8 & 0xff;
    o3 = bits & 0xff;

    if (h3 == 64) {
      tmp_arr[ac++] = String.fromCharCode(o1);
    } else if (h4 == 64) {
      tmp_arr[ac++] = String.fromCharCode(o1, o2);
    } else {
      tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
    }
  } while (i < data.length);

  dec = tmp_arr.join('');

  return dec;
}

(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html( limit - chars );
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);

var isCurrency_re    = /^\s*(\+|-)?((\d+(\.\d\d)?)|(\.\d\d))\s*$/;

function isCurrency (s) {
   //return String(s).search (isCurrency_re) != -1
   return (s.match(/^\d+(?:\.\d+)?$/));
}
var isEmail_re       = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
function isEmail (s) {
   return String(s).search (isEmail_re) != -1;
}

function extractEmails ( text ){
    return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
}

function extractNumber ( text ){
    return text.match(/([0-9_-]+){3}/gi);
}

/**
 *  Version 2.1
 *      -Contributors: "mindinquiring" : filter to exclude any stylesheet other than print.
 *  Tested ONLY in IE 8 and FF 3.6. No official support for other browsers, but will
 *      TRY to accomodate challenges in other browsers.
 *  Example:
 *      Print Button: <div id="print_button">Print</div>
 *      Print Area  : <div class="PrintArea"> ... html ... </div>
 *      Javascript  : <script>
 *                       $("div#print_button").click(function(){
 *                           $("div.PrintArea").printArea( [OPTIONS] );
 *                       });
 *                     </script>
 *  options are passed as json (json example: {mode: "popup", popClose: false})
 *
 *  {OPTIONS} | [type]    | (default), values      | Explanation
 *  --------- | --------- | ---------------------- | -----------
 *  @mode     | [string]  | ("iframe"),"popup"     | printable window is either iframe or browser popup
 *  @popHt    | [number]  | (500)                  | popup window height
 *  @popWd    | [number]  | (400)                  | popup window width
 *  @popX     | [number]  | (500)                  | popup window screen X position
 *  @popY     | [number]  | (500)                  | popup window screen Y position
 *  @popTitle | [string]  | ('')                   | popup window title element
 *  @popClose | [boolean] | (false),true           | popup window close after printing
 *  @strict   | [boolean] | (undefined),true,false | strict or loose(Transitional) html 4.01 document standard or undefined to not include at all (only for popup option)
 */
(function($) {
    var counter = 0;
    var modes = { iframe : "iframe", popup : "popup" };
    var defaults = { mode     : modes.iframe,
                     popHt    : 600,
                     popWd    : 950,
                     popX     : 200,
                     popY     : 200,
                     popTitle : '',
                     popClose : false };

    var settings = {};//global settings

    $.fn.printArea = function( options )
        {
            $.extend( settings, defaults, options );

            counter++;
            var idPrefix = "printArea_";
            $( "[id^=" + idPrefix + "]" ).remove();
            var ele = getFormData( $(this) );

            settings.id = idPrefix + counter;

            var writeDoc;
            var printWindow;

            switch ( settings.mode )
            {
                case modes.iframe :
                    var f = new Iframe();
                    writeDoc = f.doc;
                    printWindow = f.contentWindow || f;
                    break;
                case modes.popup :
                    printWindow = new Popup();
                    writeDoc = printWindow.doc;
            }

            writeDoc.open();
            writeDoc.write( docType() + "<html>" + getHead() + getBody(ele) + "</html>" );
            writeDoc.close();

            printWindow.focus();
            printWindow.print();

            if ( settings.mode == modes.popup && settings.popClose )
                printWindow.close();
        }

    function docType()
    {
        if ( settings.mode == modes.iframe || !settings.strict ) return "";

        var standard = settings.strict == false ? " Trasitional" : "";
        var dtd = settings.strict == false ? "loose" : "strict";

        return '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01' + standard + '//EN" "http://www.w3.org/TR/html4/' + dtd +  '.dtd">';
    }

    function getHead()
    {
        var head = "<head><title>" + settings.popTitle + "</title>";
        $(document).find("link")
            .filter(function(){
                    return $(this).attr("rel").toLowerCase() == "stylesheet";
                })
            .filter(function(){ // this filter contributed by "mindinquiring"
                    var media = $(this).attr("media");
                    return (media.toLowerCase() == "" || media.toLowerCase() == "print")
                })
            .each(function(){
                    head += '<link type="text/css" rel="stylesheet" href="' + $(this).attr("href") + '" >';
                });
        head += "</head>";
        return head;
    }

    function getBody( printElement )
    {
        return '<body><div class="' + $(printElement).attr("class") + '">' + $(printElement).html() + '</div></body>';
    }

    function getFormData( ele )
    {
        $("input,select,textarea", ele).each(function(){
            // In cases where radio, checkboxes and select elements are selected and deselected, and the print
            // button is pressed between select/deselect, the print screen shows incorrectly selected elements.
            // To ensure that the correct inputs are selected, when eventually printed, we must inspect each dom element
            var type = $(this).attr("type");
            if ( type == "radio" || type == "checkbox" )
            {
                if ( $(this).is(":not(:checked)") ) this.removeAttribute("checked");
                else this.setAttribute( "checked", true );
            }
            else if ( type == "text" )
                this.setAttribute( "value", $(this).val() );
            else if ( type == "select-multiple" || type == "select-one" )
                $(this).find( "option" ).each( function() {
                    if ( $(this).is(":not(:selected)") ) this.removeAttribute("selected");
                    else this.setAttribute( "selected", true );
                });
            else if ( type == "textarea" )
            {
                var v = $(this).attr( "value" );
                if ($.browser.mozilla)
                {
                    if (this.firstChild) this.firstChild.textContent = v;
                    else this.textContent = v;
                }
                else this.innerHTML = v;
            }
        });
        return ele;
    }

    function Iframe()
    {
        var frameId = settings.id;
        var iframeStyle = 'border:0;position:absolute;width:0px;height:0px;left:0px;top:0px;';
        var iframe;

        try
        {
            iframe = document.createElement('iframe');
            document.body.appendChild(iframe);
            $(iframe).attr({ style: iframeStyle, id: frameId, src: "" });
            iframe.doc = null;
            iframe.doc = iframe.contentDocument ? iframe.contentDocument : ( iframe.contentWindow ? iframe.contentWindow.document : iframe.document);
        }
        catch( e ) { throw e + ". iframes may not be supported in this browser."; }

        if ( iframe.doc == null ) throw "Cannot find document.";

        return iframe;
    }

    function Popup()
    {
        var windowAttr = "location=yes,statusbar=no,directories=no,menubar=no,titlebar=no,toolbar=no,dependent=no";
        windowAttr += ",width=" + settings.popWd + ",height=" + settings.popHt;
        windowAttr += ",resizable=yes,screenX=" + settings.popX + ",screenY=" + settings.popY + ",personalbar=no,scrollbars=no";

        var newWin = window.open( "", "_blank",  windowAttr );

        newWin.doc = newWin.document;

        return newWin;
    }
})(jQuery);

(function( $ ){
    $.fn.resetValue = function() {  
        return this.each(function() {
            var $this = $(this); 
            var node = this.nodeName.toLowerCase(); 
            var type = $this.attr( "type" ); 

            if( node == "input" && ( type == "text" || type == "password" ) ){
                this.value = this.defaultValue; 
            }
            else if( node == "input" && ( type == "radio" || type == "checkbox" ) ){
                this.checked = this.defaultChecked; 
            }
            else if( node == "input" && ( type == "button" || type == "submit" || type == "reset" ) ){ 
                // we really don't care 
            }
            else if( node == "select" ){
                this.selectedIndex = $this.find( "option" ).filter( function(){
                    return this.defaultSelected == true; 
                } ).index();
            }
            else if( node == "textarea" ){
                this.value = this.defaultValue; 
            }
            // not good... unknown element, guess around
            else if( this.hasOwnProperty( "defaultValue" ) ){
                this.value = this.defaultValue; 
            }
            else{
                // panic! must be some html5 crazyness
            }
        });
    }
} )(jQuery);

/*
tinymce.init({
    selector: "textarea.tinymce_editor",
    theme: "modern",

    height: 300,
    plugins: [
         "advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | forecolor backcolor"
 }); 
 */
 
 tinymce.init({
    selector: "textarea.tinymce_editor",
    theme: "modern",

    height: 300,
   plugins: [
         "autolink link lists textcolor paste hr fullscreen code"
   ],
   style_formats: [  
        {title: 'Heading', block: 'h2'},
		{title: 'Bold', inline: 'b'},
		{title: 'Italic', inline: 'i'},
		{title: 'Superscript', inline: 'sup'},
		{title: 'Subscript', inline: 'sub'},
		{title: 'Center',inline: 'span', classes: 'text-center'},
    ],
   content_css: "css/content.css",
   toolbar: "undo redo | styleselect | bold italic underline | alignleft aligncenter alignright alignjustify | bullist numlist | link | forecolor | removeformat code"
 }); 