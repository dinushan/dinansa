<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">
<?php if ($page_action == ''): ?>
    <h3 class="page-title">Offers</h3>

    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <?php if(
                $this->adminauth->has_role('701')
                ):?>
                <a onclick="setLocation('<?php echo site_url('admin/' . $slug . '/add') ?>')" class="btn green"><i
                        class="fa fa-plus-circle"></i> Add New</a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php if(
    $this->adminauth->has_role('701')
    ):?>
    <?php echo get_message() ?>
    <div class="row">
        <div class="col-md-12">

            <div class="table-container">

                <table class="table table-striped table-bordered table-hover" id="datatable_ajax"
                       data-order-col = "0"
                       data-order-type = "asc"
                       data-ajax_url="<?php echo base_url('admin/' . $slug . '/ajax_list') ?>">
                    <colgroup>
                        <col width="40"/>
                        <col width="100">
                        <col width="300">
                        <col width="300">
                        <col width="300">
                    </colgroup>
                    <thead>
                    <tr role="row" class="heading">
                        <th>ID</th>
                        <th>Offer code</th>
                        <th>Title</th>
                        <th>Published</th>
                        <th class="sorting_disabled">Action</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="offer_code">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="title">
                        </td>
                        <td>
                            <?php
                            if(isset($_GET['status'])){
                                $status = $_GET['status'];
                            }else{
                                $status = '';
                            }
                            echo form_dropdown('status',array(''=>'All','1'=>'YES','2'=>'NO'),$status,'class="form-control form-filter input-sm"'); ?>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-sm yellow filter-submit margin-bottom"><i
                                        class="fa fa-search"></i> Search
                                </button>
                            </div>
                            <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset
                            </button>
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function() {
            TableAjax.init();
        });
    </script>

    <?php endif; ?>


<?php elseif ($page_action == 'add' || $page_action == 'edit'): ?>
    <h3 class="page-title"><?php echo $page_action == "add" ? 'Add New' : 'Edit' ?> Offer</h3>
    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn default" href="<?php echo site_url('admin/' . $slug . '/') ?>"><i
                        class="fa fa-chevron-left"></i> Back</a>


            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12"> <?php echo get_message() ?>
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light bordered">

                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <?php
                    $attr = array("id" => "edit_form", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                    echo form_open(current_url(), $attr);
                    ?>
                    <?php
                    if ($page_action == 'edit')
                    {
                        $offer_code = $offer_data->offer_code;
                        $start_date = $offer_data->start_date;
                        $end_date = $offer_data->end_date;
                        $created_date = $offer_data->created_date;
                        $modified_date = $offer_data->modified_date;
                        $type = $offer_data->type;
                        $limit = $offer_data->limit;
                        $status = $offer_data->status;
                        $title = $offer_data->title;
                        $description = $offer_data->description;
                        $offer_image = $offer_data->image;
                        $discount_value=$offer_data->discount_value;
                        $discount_type=$offer_data->discount_type;

                    } else {
                        $offer_code = $offer_code;
                        $start_date = '';
                        $end_date = '';
                        $created_date = '';
                        $modified_date = '';
                        $type = '1';
                        $limit = '';
                        $title = '';
                        $status = 2;
                        $description='';
                        $offer_image ='';
                        $movies_selected=array();
                        $discount_value='';
                        $discount_type='';
                    }
                    ?>

                    <h4 class="form-section">General</h4>

                    <div class="form-group">
                        <label class="control-label col-md-3">Offer code<span class="req">*</span></label>

                        <div class="control-element col-md-4"><input name="offer_code" type="text"
                                                                     class="form-control required-entry"
                                                                     value="<?php echo set_value('offer_code', $offer_code) ?>"/>
                            <?php echo form_error('offer_code'); ?>
                        </div>
                    </div>

					<div class="form-group">
                        <label class="control-label col-md-3">Start Date<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <div class="input-group date date-picker " data-date-format="yyyy-mm-dd">
								<input type="text" class="form-control" name="start_date" value="<?php echo set_value('start_date',$start_date) ?>">
									<span class="input-group-btn">
									<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
									</span>
							</div>
							<?php echo form_error('start_date'); ?>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3">End Date<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <div class="input-group date date-picker " data-date-format="yyyy-mm-dd">
                                <input type="text" class="form-control" name="end_date" value="<?php echo set_value('end_date',$end_date) ?>">
                                <span class="input-group-btn">
									<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
									</span>
                            </div>
                            <?php echo form_error('end_date'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Title<span class="req">*</span></label>

                        <div class="control-element col-md-4"><input name="title" type="text"
                                                                     class="form-control required-entry"
                                                                     value="<?php echo set_value('title', $title) ?>"/>
                            <?php echo form_error('title'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Discount Type<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <?php echo form_dropdown('discount_type', array("1"=>'Percentage',"2"=>'Flat'), set_value('discount_type', $discount_type), 'id="discount_type" placeholder="Select" class="form-control required-entry" required="required"'); ?>
                            <?php echo form_error('discount_type'); ?></div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3">Discount Value<span class="req">*</span></label>

                        <div class="control-element col-md-4"><input name="discount_value" type="text"
                                                                     class="form-control required-entry"
                                                                     value="<?php echo set_value('discount_value', $discount_value) ?>"/>
                            <?php echo form_error('discount_value'); ?>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3">Description<span class="req">*</span></label>

                        <div class="control-element col-md-4"><textarea name="description" type="text" maxlength="185"
                                                                     class="form-control required-entry" ><?php echo set_value('description', $description) ?></textarea>
                            <?php echo form_error('description'); ?>
                        </div>
                    </div>



                    <div class="form-group">
                        <label class="control-label col-md-3">Type<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <?php echo form_dropdown('type', array("1"=>'Limited',"2"=>'Unlimited'), set_value('type', $type), 'id="type" placeholder="Select" class="form-control required-entry" required="required"'); ?>
                            <?php echo form_error('type'); ?></div>
                    </div>

                    <div class="form-group" id="limit_div" style="<?php echo ($type=='2')?'display:none':'' ?>" >
                        <label class="control-label col-md-3">Limit<span class="req">*</span></label>

                        <div class="control-element col-md-4"><input name="limit" type="text"
                                                                     class="form-control "
                                                                     value="<?php echo set_value('limit', $limit,'id="limit"') ?>"/>
                            <?php echo form_error('limit'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Movies<span class="req">*</span></label>
                        <div class="control-element col-md-4">
                            <?php echo form_dropdown('movies[]', $movies, set_value('movies[]', $movies_selected), 'class="form-control required-entry cb_s2" multiple'); ?>
                            <?php echo form_error('movies[]'); ?>
                        </div>
                    </div>

                    <?php if ($page_action == 'edit' && $offer_image != ''): ?>
                        <div class="form-group">
                            <label class="control-label col-md-3">Offer Image <span class="req">*</span></label>

                            <div class="control-element col-md-4">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td><img
                                                    src="<?php echo base_url() . OFFER_IMG_PATH . $offer_image ?>?<?php echo time() ?>"
                                                    alt="" width="200"/></td>
                                        <td>&nbsp;</td>
                                        <td><?php list($img_width, $img_height) = getimagesize(OFFER_2X_IMG_PATH . $offer_image); ?>
                                            <strong>Image Info</strong><br/>
                                            width:<?php echo $img_width ?>px<br/>
                                            height:<?php echo $img_height ?>px
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><label>
                                                <input name="image_del" type="checkbox" value="1"/>
                                                Image Delete </label>

                                            <div class="help-block font-green">Image Size:<?php echo OFFER_IMG_W ?>
                                                px &times; <?php echo OFFER_IMG_H ?>px, Image Type: jpg<br />
                                                If you want to upload a image, first delete the existing file.
                                            </div>
                                    </tr>
                                    <tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="form-group">
                            <label class="control-label col-md-3">Offer Image <span class="req">*</span></label>

                            <div class="control-element col-md-4"><input type="file" class=" input-file required-entry" name="image">
                                <?php echo form_error('image'); ?>
                                <span class="help-block font-green">Image Size:<?php echo OFFER_IMG_W ?>
                                    px &times; <?php echo OFFER_IMG_H ?>px, Image Type: jpg</span>
                            </div>
                        </div>
                    <?php endif; ?>

                    <?php if ($this->adminauth->has_role('702')): ?>
                        <div class="form-group">
                            <label class="control-label col-md-3">Is Published<span class="req">*</span></label>

                            <div class="control-element col-md-4">
                                <?php echo form_dropdown('status', unserialize(PUBLISHED), set_value('status', $status), 'placeholder="Select" class="form-control required-entry" required="required"'); ?>
                                <?php echo form_error('status'); ?></div>
                        </div>
                    <?php endif; ?>


                    <?php if (
                        $page_action == 'add'
                        || ($page_action == 'edit' && $this->adminauth->has_role('701') && $status == 0)
                        || ($page_action == 'edit' && $this->adminauth->has_role('702'))
                      ): ?>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <div class="btn-group">
                                    <input type="hidden" id="hidden-action" name="hidden-action" value="1">
                                    <button type="submit" data-id="1" class="btn btn-submit green">Save & Return
                                    </button>
                                    <button type="submit" data-id="2" class="btn btn-submit btn-default">Save & New
                                    </button>
                                    <button type="submit" data-id="3" class="btn btn-submit btn-default">Save & Edit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>

                    <script type="text/javascript">


                       /* $('.addBtn').on('click', function () {
                            addTableRow($(this).parents('table'));


                        });

                        $('.addBtnRemove').click(function () {
                            $(this).closest('tr').remove();
                        });*/

                         $('#type').change(function () {
                             if($(this).val()=='1'){
                                 $('#limit_div').show();
                             }
                            else{
                                 $('#limit_div').hide();
                             }
                        });


                      /*  function addTableRow(tbl) {
                            $i = tbl.find('tbody').find('tr').length;

                            if (tbl.hasClass('tbl-cast'))
                            {
                                if ($i == 0) {

                                } else {
                                    $_h = '<tr>' +
                                        '<td><input type="text"  name="actor[]" class="form-control input-md"/></td>' +
                                        '<td><input type="text"  name="character[]" class="form-control input-md"/></td>' +
                                        '<td><span class="glyphicon glyphicon-minus addBtnRemove"></span></td>' +
                                        '</tr>';
                                }

                            } else if (tbl.hasClass('tbl-directed-by'))
                            {
                                if ($i == 0) {

                                } else {
                                    $_h = '<tr>' +
                                        '<td><input type="text"  name="directer_name[]" class="form-control input-md"/></td>' +
                                        '<td><span class="glyphicon glyphicon-minus addBtnRemove"></span></td>' +
                                        '</tr>';
                                }
                            }
                            else if (tbl.hasClass('tbl-produced-by'))
                            {
                                if ($i == 0) {

                                } else {
                                    $_h = '<tr>' +
                                        '<td><input type="text"  name="producer_name[]" class="form-control input-md"/></td>' +
                                        '<td><span class="glyphicon glyphicon-minus addBtnRemove"></span></td>' +
                                        '</tr>';
                                }
                            }
                            else if (tbl.hasClass('tbl-written-by'))
                            {
                                if ($i == 0) {

                                } else {
                                    $_h = '<tr>' +
                                        '<td><input type="text"  name="writer_name[]" class="form-control input-md"/></td>' +
                                        '<td><span class="glyphicon glyphicon-minus addBtnRemove"></span></td>' +
                                        '</tr>';
                                }
                            }
                            else if (tbl.hasClass('tbl-music-by'))
                            {
                                if ($i == 0) {

                                } else {
                                    $_h = '<tr>' +
                                        '<td><input type="text"  name="musician_name[]" class="form-control input-md"/></td>' +
                                        '<td><span class="glyphicon glyphicon-minus addBtnRemove"></span></td>' +
                                        '</tr>';
                                }
                            }

                            var tempTr = $_h;
                            tbl.append(tempTr);

                            tbl.find("tr:last input:first").focus();


                            $(document.body).on('click', '.addBtnRemove', function (e) {
                                $(this).closest('tr').remove();
                            });
                        }*/



						$('.date-picker').datepicker({
								format: "yyyy-mm-dd",
								startDate: '1d',
								autoclose: true
							});
								
                        $('.required-entry').attr('required', 'required');
                        $('#edit_form').validate({
                            errorElement: 'span',
                            errorClass: 'validation-advice',
                            ignore: "",
                            submitHandler: function (form) {
                                form.submit();
                            }
                        });
                    </script>
                    </form>
                </div>
            </div>
        </div>
    </div>


<?php endif;//$page_action ?>
</div>
<!-- END PAGE CONTENT-->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER --> 