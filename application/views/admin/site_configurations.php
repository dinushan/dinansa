<!-- BEGIN CONTAINER -->
<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>  
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title">Site Configurations</h3>
			
			<div class="row">
				<div class="col-md-12">
					<?php echo get_message() ?>
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet light bordered">
						<!--<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-pencil"></i>Settings
							</div>							
						</div>-->
						<div class="portlet-body form">
							<!-- BEGIN FORM-->
							<?php                    
							$attr = array("id"=>"edit_form","class"=>"form-horizontal");
							echo form_open(current_url(),$attr);
							?>
							  <?php 
								$site_name 				= $configurationData->site_name;
								$address 				= $configurationData->address;
								$site_email 			= $configurationData->site_email;
								$support_email 			= $configurationData->support_email;
								$phone 					= $configurationData->phone;
								$phone2 				= $configurationData->phone2;
								$hotline 				= $configurationData->hotline;
								$fax 					= $configurationData->fax;
								$google_analytics_code 	= $configurationData->google_analytics_code;
								$facebook_url 			= $configurationData->facebook_url;
								$twitter_url 			= $configurationData->twitter_url;
								$instagram_url 			= $configurationData->instagram_url;
							  	$pinterest_url 			= $configurationData->pinterest_url;
								$linkedin_url 			= $configurationData->linkedin_url;
								$google_plus_url 		= $configurationData->google_plus_url;
								$youtube_url 			= $configurationData->youtube_url;
								
								$meta_title 			= $configurationData->meta_title;
								$meta_keywords 			= $configurationData->meta_keywords;
								$meta_description 		= $configurationData->meta_description;
								$og_type 				= $configurationData->og_type;
								$og_title 				= $configurationData->og_title;
								$og_description 		= $configurationData->og_description;

                                $online_booking_end 	= $configurationData->online_booking_end;
                                $additional_online_fee 	= $configurationData->additional_online_fee;
								?> 
								<div class="form-body">
									<h4 class="form-section">General Settings</h4>
									<div class="form-group">
										<label class="control-label col-md-3">Site Name<span class="req">*</span></label>
										<div class="control-element col-md-4">
												<input type="text" class="form-control required-entry" 
											  value="<?php echo set_value('site_name',$site_name) ?>" name="site_name" />
												<?php echo form_error('site_name', '<div class="validation-advice">', '</div>'); ?>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Address</label>
										<div class="control-element col-md-4">
												<input type="text" class="form-control" 
												  value="<?php echo set_value('address',$address) ?>" name="address" />
													<?php echo form_error('address', '<div class="validation-advice">', '</div>'); ?>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Site Email<span class="req">*</span></label>
										<div class="control-element col-md-4">
												<input type="text" class="form-control required-entry" 
                                  value="<?php echo set_value('site_email',$site_email) ?>" name="site_email" />
                                    <?php echo form_error('site_email', '<div class="validation-advice">', '</div>'); ?>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Support Email<span class="req">*</span></label>
										<div class="control-element col-md-4">
												<input type="text" class="form-control required-entry" 
                                  value="<?php echo set_value('support_email',$support_email) ?>" name="support_email" />
                                    <?php echo form_error('support_email', '<div class="validation-advice">', '</div>'); ?>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Phone<span class="req">*</span></label>
										<div class="control-element col-md-4">
												<input type="text" class="form-control required-entry" 
                                  value="<?php echo set_value('phone',$phone) ?>" name="phone" />
                                    <?php echo form_error('phone', '<div class="validation-advice">', '</div>'); ?>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Hotline</label>
										<div class="control-element col-md-4">
												<input type="text" class="form-control" 
                                  value="<?php echo set_value('hotline',$hotline) ?>" name="hotline" />
                                    <?php echo form_error('hotline', '<div class="validation-advice">', '</div>'); ?>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Fax</label>
										<div class="control-element col-md-4">
												<input type="text" class="form-control" 
                                  value="<?php echo set_value('fax',$fax) ?>" name="fax" />
                                    <?php echo form_error('fax', '<div class="validation-advice">', '</div>'); ?>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Google Analytics Code</label>
										<div class="control-element col-md-4">											
												<input type="text" class="form-control" 
                                  value="<?php echo set_value('google_analytics_code',$google_analytics_code) ?>" name="google_analytics_code" />
                                    <?php echo form_error('google_analytics_code', '<div class="validation-advice">', '</div>'); ?>											
										</div>
									</div>

									<h4 class="form-section">Social Media Links</h4>

									<div class="form-group">
										<label class="control-label col-md-3">Facebook</label>
										<div class="control-element col-md-4"><input name="facebook_url" type="text"
																					 class="form-control"
																					 value="<?php echo set_value('facebook_url', $facebook_url) ?>"/>
											<?php echo form_error('facebook_url'); ?>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-3">Twitter</label>
										<div class="control-element col-md-4"><input name="twitter_url" type="text"
																					 class="form-control"
																					 value="<?php echo set_value('twitter_url', $twitter_url) ?>"/>
											<?php echo form_error('twitter_url'); ?>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-3">Pinterest</label>
										<div class="control-element col-md-4"><input name="pinterest_url" type="text"
																					 class="form-control"
																					 value="<?php echo set_value('pinterest_url', $pinterest_url) ?>"/>
											<?php echo form_error('pinterest_url'); ?>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-3">Google Plus</label>
										<div class="control-element col-md-4"><input name="google_plus_url" type="text"
																					 class="form-control"
																					 value="<?php echo set_value('google_plus_url', $google_plus_url) ?>"/>
											<?php echo form_error('google_plus_url'); ?>
										</div>
									</div>

									<div class="form-group">
										<label class="control-label col-md-3">Instagram</label>
										<div class="control-element col-md-4"><input name="instagram_url" type="text"
																					 class="form-control"
																					 value="<?php echo set_value('instagram_url', $instagram_url) ?>"/>
											<?php echo form_error('instagram_url'); ?>
										</div>
									</div>

                                    <h4 class="form-section">Online Movie Reservation</h4>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Additional fee for online<span class="req">*</span></label>
                                        <div class="control-element col-md-4">
                                            <div class="input-group"><span class="input-group-addon">Rs</span>
                                                <input type="text" class="form-control required-entry"
                                                       value="<?php echo set_value('additional_online_fee ',$additional_online_fee) ?>" name="additional_online_fee" />
                                                <?php echo form_error('additional_online_fee', '<div class="validation-advice">', '</div>'); ?>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label col-md-3">Online Suspension<span class="req">*</span></label>
                                        <div class="control-element col-md-4">
                                            <div class="input-group">
                                                <input type="text" class="form-control required-entry"
                                                       value="<?php echo set_value('online_booking_end',$online_booking_end) ?>" name="online_booking_end" />
															<span class="input-group-addon">minutes</span>
                                            </div>
                                            <?php echo form_error('online_booking_end', '<div class="validation-advice">', '</div>'); ?>
                                        </div>
                                    </div>

									
									<h4 class="form-section">Default Meta Settings</h4>
									<div class="form-group">
										<label class="control-label col-md-3">Meta Title</label>
										<div class="control-element col-md-4">
											<input type="text" class="form-control" value="<?php echo 
											$page_action == "add" ? 
											set_value('meta_title') : 
											set_value('meta_title',$meta_title) ?>" 
											name="meta_title" />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Meta Keywords</label>
										<div class="control-element col-md-4">
											<textarea cols="15" rows="2" class="form-control" name="meta_keywords"><?php echo $page_action == "add" ? 
											set_value('meta_keywords') : 
											set_value('meta_keywords',$meta_keywords) ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Meta Description</label>
										<div class="control-element col-md-4">
											<textarea cols="15" rows="2" class="form-control" name="meta_description"><?php echo $page_action == "add" ? 
											set_value('meta_description') : 
											set_value('meta_description',$meta_description) ?></textarea>
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">OG Type</label>
										<div class="control-element col-md-4">
											<input type="text" class="form-control" value="<?php echo 
											$page_action == "add" ? 
											set_value('og_type') : 
											set_value('og_type',$og_type) ?>" 
											name="og_type" />
											<span class="help-block font-green">The type of the object, such as 'article'</span>
										</div>
										
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">OG Title</label>
										<div class="control-element col-md-4">
											<input type="text" class="form-control" value="<?php echo 
											$page_action == "add" ? 
											set_value('og_title') : 
											set_value('og_title',$og_title) ?>" 
											name="og_title" />
										</div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">OG Description</label>
										<div class="control-element col-md-4">
											<textarea cols="15" rows="2" class="form-control" name="og_description"><?php echo $page_action == "add" ? 
											set_value('og_description') : 
											set_value('og_description',$og_description) ?></textarea>
										</div>
									</div>
									
									
								</div>
								<div class="form-actions">
									<div class="row">
										<div class="col-md-offset-3 col-md-9">
											<button type="submit" class="btn green">Submit</button>
										</div>
									</div>
								</div>
							</form>
							 <script type="text/javascript">							 
								$('.required-entry').attr('required','required');					
								$('#edit_form').validate({
									errorElement: 'span', 
									errorClass: 'validation-advice', 								
									ignore: "",
									submitHandler: function (form) {
										form.submit();										
									}
								});		
								
											
							</script>
							<!-- END FORM-->
						</div>
					</div>
					<!-- END VALIDATION STATES-->
				</div>
			</div>
			
		</div>
		<!-- END PAGE CONTENT-->
	</div>
	<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
