<script src="<?php echo base_url() ?>js/bootstrap-typeahead.js" type="text/javascript"></script>

<div class="page-container">
<?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title">Booking Detail Report</h3>

            <div class="table-container">
                <div class="row">
                    <div class="col-md-12">

                        <button id="exportCVS1910" type="button" class="btn btn-green pull-right" onclick="exportCVS()">Export to Excel</button>
                    </div>
                </div>
                <br/>

                <table id="datatable_ajax" style="overflow: scroll;" class="table table-striped table-bordered table-hover table-responsive"
                       data-order-col = "0"
                       data-order-type = "asc"
                       data-ajax_url="<?php echo base_url('admin/reports/fyi_reports/ajax_list') ?>">
                    <thead>
                        <tr role="row" class="heading">
                            <th>Reference No</th>
                            <th>Date of purches</th>
                            <th>Movie</th>
                            <th>Theater</th>
                            <th>Booking Type</th>
                            <th>Movie Date</th>
                            <th>Movie Time</th>
                            <th>Source</th>
                            <th>Total Ticket</th>
                            <th>Total Ticket price</th>

                            <th>Convenience Fee</th>
                            <th>Total price</th>
                            <th>Actions</th>
                        </tr>
                        <tr>
                            <th widht="100"><input type="text" class="form-control form-filter input-sm" name="reference_no"></th>
                            <th widht="100">
                                <div class="form-group">
                                    <input type="text" value="<?php echo date('Y-m-d');?>" class="form-control form-filter date-picker input-sm" name="from" placeholder="from">

                                </div>
                                <div class="form-group">
                                    <input type="text"  value="<?php echo date('Y-m-d');?>" class="form-control form-filter date-picker input-sm" name="to" placeholder="to">
                                </div>

                            </th>
                            <th widht="100">
                                <div class="form-group">
                                    <input type="text" id="movie_name_01" value ="" autocomplete="off" placeholder="Search Movies..." class="form-control form-filter input-sm" name="movie"/>
                                </div>
                            </th>
                            <th widht="100">
                                <div class="form-group">
                                    <select class="form-control form-filter" name="theater">
                                        <option value="">Any</option>
                                        <?php foreach ($theaters as $theater):?>
                                            <option value="<?php echo $theater->id?>"><?php echo $theater->name;?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </th>
                            <th>
                                <div class="form-group">
                                    <select class="form-control form-filter" name="bookingtype">
                                        <option value="">Any</option>
                                        <?php foreach ($booking_type as $sr):?>
                                            <option value="<?php echo $sr->id?>"><?php echo $sr->name;?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </th>
                            <th>
                                <div class="form-group">
                                    <input type="text"  value="" class="form-control form-filter date-picker input-sm" name="movie_date" placeholder="Date">
                                </div>
                            </th>
                            <th>
                                <div class="form-group">
                                    <input type="text"  value="" class="form-control form-filter timepicker input-sm" name="movie_time" placeholder="Time">
                                </div>
                            </th>
                            <th widht="100">
                                <div class="form-group">
                                    <select class="form-control form-filter" name="source">
                                        <option value="">Any</option>
                                        <?php foreach ($source as $sr):?>
                                            <option value="<?php echo $sr->id?>"><?php echo $sr->name;?></option>
                                        <?php endforeach;?>
                                    </select>
                                </div>
                            </th>
                            <th widht="100"></th>
                            <th widht="100"></th>

                            <th widht="100"></th>
                            <th widht="100"></th>
                            <th><button class="btn btn-sm filter-submit">Search</button></th>
                        </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<iframe name="sales_report_download" style="display: none;"></iframe>

<script>
    $('.date-picker').datepicker({format:'yyyy-mm-dd'});
    $('.timepicker').timepicker();
    TableAjax.init();


    $('#movie_name_01').typeahead({
        ajax: {
            url:'<?php echo base_url('admin/reports/fyi_reports/searchMovie')?>',
            triggerLength:1
        },
        displayField: 'movie_name',
        val:'id'
    });

    function exportCVS() {
        window.open("<?php echo base_url('admin/reports/fyi_reports/export')?>","sales_report_download");

    }
</script>