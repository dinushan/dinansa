<?php
/**
 * Created by PhpStorm.
 * User: LAYOUTindex
 * Date: 1/4/2018
 * Time: 4:22 PM
 */

require __DIR__ . '\..\..\vendor\autoload.php';

use Mike42\Escpos\PrintConnectors\NetworkPrintConnector;
use Mike42\Escpos\Printer;
use Pheanstalk\Pheanstalk;





function networkPrinter($kitchen_id,$data = []){



    file_put_contents('this_test_file_content_'.time().'.txt',print_r($data));


    $connector = new NetworkPrintConnector("192.168.1.34", 9100);
    $printer = new Printer($connector);
    try {


        foreach ($data as $key=> $value){

            foreach ($value as $k=>$v){

                $printer -> setTextSize(2, 2);
                $printer->setColor(0);

                $text = $k." ".$v['order_id'].' '.$v['item_name'].' '.$v['quantity']."\n";
                $printer -> text( $text );
            }
        }



    }catch(Exception $e){
        echo $e->getMessage();
    }finally {
        $printer -> cut();
        $printer -> close();
    }
}

function testPrinter(){


    $connector = new NetworkPrintConnector("192.168.1.34", 9100);
    $printer = new Printer($connector);

    $printer -> setTextSize(1, 1);
    $printer->setColor(1);

    $text = "Printer Tested ! Print connector was not finalized. Did you forget to close the printer";
    $printer -> text( $text );

    $printer -> cut();
    $printer -> close();


}

function processor(){
    $pheanstalk = new Pheanstalk('127.0.0.1');

    $pheanstalk
        ->useTube('testtube')
        ->put("job payload goes here\n");
}

function workor(){

    $pheanstalk = new Pheanstalk('127.0.0.1');

    $job = $pheanstalk
        ->watch('testtube')
        ->ignore('default')
        ->reserve();

    echo $job->getData();

    $pheanstalk->delete($job);
}