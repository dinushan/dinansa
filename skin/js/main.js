var winWidth='';
var winheight='';
var currentScroll='';

$(window).scroll(function() {
    winWidth=$(window).width();
    winheight=$(window).height();
    var windscroll = $(window).scrollTop();
    if ((windscroll >0)) {
        $('.navbar-top').addClass('navbarScroll');
    }else{
        $('.navbar-top').removeClass('navbarScroll');
    }
});
$(function(){

     mainNavHeightSet();

    // $('.navbar-toggle').click(function(){
    //     $(this).toggleClass('open');
    //     if($(this).hasClass('open')){
    //         $('.navbar-collapse').animate({left:'10%'},350);
    //         $('.navOverlay').animate({left:'0%'},350);
    //         $('body').css('overflow','hidden');
    //     }else{
    //         $('.navbar-collapse').animate({left:'120%'},350);
    //         $('.navOverlay').animate({left:'120%'},350);
    //         $('body').css('overflow','auto');
    //     }
    // });

    //  $('.navbar-toggle').click(function(){
    //     $(this).toggleClass('open');
    //     if($(this).hasClass('open')){
    //         $('.navbar-collapse').animate({top:'60px'},350);
            
    //         $('body').css('overflow','hidden');
    //     }else{
    //           winWidth=$(window).width();
    //             winheight=$(window).height();
    //             var topVal=(-1*(winheight-60));
    //         $('.navbar-collapse').animate({top:topVal+'px'},350);
            
    //         $('body').css('overflow','auto');
    //     }
    // });
    $('.navbar-toggle').click(function(){
        $(this).toggleClass('open');
        var navHeight=winheight-60;
        if($(this).hasClass('open')){
            $('.navbar-collapse').animate({height:navHeight+'px'},350);
            
            $('body').css('overflow','hidden');
        }else{
              
            $('.navbar-collapse').animate({height:'0px'},350);
            
            $('body').css('overflow','auto');
        }
    });
    $('.navOverlay').click(function(){
        $('.navbar-toggle').trigger('click');
    });
    if($('.data-image').length>0){
        $('.data-image').dataImagebackground();
    }

        if($('.form-group').length>0){
            $('.form-group').matchHeight();
        }
    if($('.selectDate').length>0){
        $('.selectDate').datepicker({
            minDate: 0,
            dateFormat:'yy-mm-dd',
            showButtonPanel: true,
            beforeShow:function(input, ins) {
                    datePickerBeforeShow();
            },
            onClose:function(input, ins) {
                    datePickerOnClose(input);

            }
        }).focus(function () {
            $(this).blur()
        }).datepicker('widget').wrap('<div class="ll-skin-melon"/>');
    }

    if($('.youTubePopUp').length>0){
        $('.youTubePopUp').magnificPopup({
            disableOn: 700,
            type: 'iframe',
            mainClass: 'mfp-fade',
            removalDelay: 160,
            preloader: false,
            fixedContentPos: false
        });
    }

    if($('.calendar-icon').length>0){
        $('.calendar-icon').click(function(){

            var datePicker=$(this).parents('.calendarWrap').find('.hasDatepicker');

            if($('#ui-datepicker-div').is(":visible")){
                $(datePicker).datepicker("hide");
            }else{
                $(datePicker).datepicker("show");
            }

        });
    }



//get date and get the movies related to date (movie times page)
    $('.searchMovieDate').datepicker({

        minDate: 0,
        dateFormat:'dd MM yy',
        showButtonPanel: true,

        onSelect: function(dateText, inst) {
            var date = $(this).val();
            getMoviesOnDate(date);

        },
            beforeShow:function(input, ins) {
                    datePickerBeforeShow();
            },
            onClose:function(input, ins) {
                    datePickerOnClose(input);

            }

    }).focus(function () {
        $(this).blur()
    }).datepicker('widget').wrap('<div class="ll-skin-melon"/>');



    $("#news-subscribe").validate({
        rules: {
            nl_email: {
                required: true,
                email: true
            }
        },
        submitHandler: function(form) {
            $('.btn-subscribe',$(form)).attr('data-btn_value',$('.btn-subscribe',$(form)).val());
            var btn_data_wait = $('.btn-subscribe',$(form)).attr('data-wait');
            var btn_value = $('.btn-subscribe',$(form)).attr('data-btn_value');

            $('.btn-subscribe',$(form)).val(btn_data_wait);

            $.post($(form).attr('action'),$(form).serialize(),
                function(jsonData){

                    $('.btn-subscribe',$(form)).val(btn_value);
                    $(".w-form-fail").hide();
                    $(".w-form-done").hide();
                    if(jsonData.status == 'error'){
                        $(".w-form-fail").show();
                    }else{
                        $('#nl_email',$(form)).val('');
                        $(".w-form-done").show();
                    }
                },'json');
        }
    });


    //appstore link javascript
    if( /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ) {
           
                if (document.cookie.indexOf("close_appstore=") < 0) {
                       $('.appStoreWrap').show();
                    }
                  $(".closer").click(function() {
                    $('.appStoreWrap').hide();
                    document.cookie = "close_appstore=yes; expires=Fri, 31 Dec 9999 23:59:59 UTC";
              });
          }


    $('.selectMovieDate').datepicker({
        minDate: 0,
        dateFormat:'dd MM yy',
        showButtonPanel: true,
        onSelect: function(dateText, inst) {
            var date = $(this).val();
            var theatre_id=''+$(this).data('theatre-id')+'';
            var movie_id=''+$(this).data('movie-id')+'';
            getTimeSlots(theatre_id,movie_id,date);
        },
            beforeShow:function(input, ins) {
                    datePickerBeforeShow();
            },
            onClose:function(input, ins) {
                    datePickerOnClose(input);

            }

    }).focus(function () {
        $(this).blur()
    }).datepicker('widget').wrap('<div class="ll-skin-melon"/>');

    $(function () {
        $('.popup-modal').magnificPopup({
            type: 'inline',
            preloader: false,
            callbacks: {
                beforeOpen: function () {
                    var magnificPopup = $.magnificPopup.instance;
                    var cur = magnificPopup.st.el;
                    var dealId = $(cur).data('deal-id');
                    getDealDetails(dealId);
                    $('.dealsBody').css('opacity','0');
                    $('.pageLoaderCont').show();
                }
            }
        });
        $(document).on('click', '.popup-modal-dismiss', function (e) {
            e.preventDefault();
            $.magnificPopup.close();
        });
    });
    

});

$(window).resize(function(){
    mainNavHeightSet();
});

function mainNavHeightSet(){
    winWidth=$(window).width();
    winheight=$(window).height();
    var topVal=(-1*(winheight-60));
    if(winWidth<=992){
        $('.navbar-collapse').css({
            'height':'0',
            'overflow-y':'auto',
            'overflow-x':'hidden'
        });
    }else{
        $('.navbar-collapse').css({
            'height':'inherit'
        });
    }
}

   function datePickerBeforeShow(){
       //if($(window).width()<768){
        currentScroll=$(window).scrollTop();
        $('.datePickerBackground').show();
        $('body,html').addClass('scrollFix');
            
       //}
        
    }

     function datePickerOnClose(input){
        
        //if($(window).width()<768){
            $('.datePickerBackground').hide();
             $('body,html').removeClass('scrollFix').scrollTop(currentScroll);
        //}
       
    }
//get month and filter by month news page
function getMonth(month) {
    url = (news_url+month);
    window.location=url;
}

//get date and get the time slots thtaer page
function getTimeSlots(theater_id,movie_id,selected_date) {

    var parent = $('#div-'+theater_id+'-'+movie_id);
    var date = selected_date;
    var loader='<div class="loaderCont">'+
        '<div class="loaderWrap">'+
        '<div class="cssload-thecube">'+
        '<div class="cssload-cube cssload-c1"></div>'+
        '<div class="cssload-cube cssload-c2"></div>'+
        '<div class="cssload-cube cssload-c4"></div>'+
        '<div class="cssload-cube cssload-c3"></div>'+
        '</div>'+
        '</div>'+
        '</div>';
    parent.find(".detailRow").html(loader);
    $.ajax({
        url: base_url + "ajax/website_common_ajax/get_time_slots",
        type: 'GET',
        data: {
            "theater_id"    : theater_id,
            "movie_id"      : movie_id,
            "date"          : date
        },
        success: function(jsonData) {
            parent.find(".detailRow").html(jsonData);
        }
    });

}


//get details from movie time page
function getMovieTimesDetails(theater_id,movie_id,full_time_slot) {

    $.ajax({
        url: base_url + "ajax/website_common_ajax/get_ticket_category_price",
        type: 'GET',
        data: {
            "theater_id"        : theater_id,
            "movie_id"          : movie_id,
            "full_time_slot"    : full_time_slot
        },
        success: function(jsonData) {
            $(".popUpBoxWap").empty();
            $(".popUpBoxWap").append(jsonData);
        }
    });

}


function getSearchUrl() {

    var selected_date   = $(".searchMovieDate").val();
    var movie_id        = $(".selectMovie option:selected").val();
    var theater_id      = $(".selectTheater option:selected").val();


     if(movie_id == '' && theater_id == '')
    {
        window.location.href = movie_times_url+'?date='+selected_date;
    }
    else if(theater_id == '')
    {
        window.location.href = movie_times_url+'?date='+selected_date+'&movie='+movie_id;
    }
    else
    {
        window.location.href = movie_times_url+'?date='+selected_date+'&movie='+movie_id+'&theater='+theater_id;
    }

}


//get deals details according to deal_id
function getDealDetails(deal_id) {

    $.ajax({
        url: base_url + "ajax/website_common_ajax/get_deal_details",
        type: 'GET',
        data: {
            "deal_id"  : deal_id
        },
        success: function(jsonData) {
            $('.dealsBody').css('opacity','1');
            $('.pageLoaderCont').slideUp();
            $(".dealsBody").html(jsonData);
        }
    });

}


$(document).ready(function(){
    var winWidth=$(window).width();

    if($('body').hasClass('cms-page-home'))
    {


        if(winWidth<768){
            $('.camera_wrap').camera({
                navigation:false,
                playPause:false,
                time:4000,
                transPeriod: 2000,
                pauseOnClick: true,
                hover: false,
                loader:'none',
                fx:'simpleFade',
                onStartLoading: function() {
                    // console.log('onStartLoading');
                },
                onLoaded: function() {
                    //console.log('onLoaded');
                },
                onStartTransition: function() {
                    // console.log('onStartTransition');
                },
                onEndTransition: function(t) {
                    //console.log('onEndTransition');
                    var ind = $('.camera_target .cameraSlide.cameracurrent').index();
                    var captionHeight=$('.camera_target .cameraSlide.cameracurrent').find('.captionContainer').height();

                }
            });

        }else{
            $('.camera_wrap').camera({
                navigation:false,
                playPause:false,
                time:4000,
                transPeriod: 2000,
                pauseOnClick: true,
                hover: false,
                loader:'none',
                minHeight:'600px',
                fx:'simpleFade'
            });
        }
      
      

        // $('.home-tab-1').find('li:first').addClass('active');
        // $('.home-tab-content-1').find('.tab-pane:first').addClass('active');

        $('.home-tab-1').find('li:last').addClass('active');
        $('.home-tab-content-1').find('.upcomming').addClass('active');

        $('.advertisement_camera_wrap').camera({
            navigation:false,
            playPause:false,
            time:3000,
            transPeriod: 2000,
            pauseOnClick: false,
            loader:'none',
            minHeight:'100px',
            height:'230px',
            pagination:false,
            portrait:true,
            fx:'simpleFade'
        });

        $('.offerDescWrap,.offerBannerWrap').matchHeight();

    }

    $('#btn-print-summary').on('click',function()
    {
        PrintElem('summary-print');
    });
    $('.descHeadWrap').matchHeight();
    $('.footerNavWrap').matchHeight();
    
    if($('body').hasClass('cms-page-theater-details'))
    {
        $('.movieSliderWrapper').camera({
            navigation: false,
            playPause: false,
            time: 10000,
            transPeriod: 2000,
            pauseOnClick: true,
            loader: 'none',
            minHeight: '400px',
            fx:'simpleFade'
        });

        var uluru = {lat: lat, lng: lng};
        var map = new google.maps.Map(document.getElementById('map'), {
            zoom: 18,
            center: uluru
        });
        var marker = new google.maps.Marker({
            position: uluru,
            map: map
        });
    }

    if($('body').hasClass('cms-page-career')){
        $('.careerCaption').matchHeight();
    }


        $('.nav_link').hover(function(){
                    if(!$(this).hasClass('active')){
                        navHoverAnimation($(this));

                    }
                }, 
                function(){
                    navLeaveAnimation($(this));
                });
    if($('.careerHeadGrid')){
       $('.careerHeadGrid').matchHeight(); 
    }
});


function navHoverAnimation(e){
    var offset = $(e).offset();
    var leftVal=offset.left;

    var navWidth=$(e).outerWidth();

    var hoverFillWidth=leftVal+navWidth;
    $('.hoverFill').width(hoverFillWidth+'px');
    
}
function navLeaveAnimation(e){
    $('.hoverFill').width('0px');
}
function PrintElem(elem)
{
    var mywindow = window.open('', 'PRINT');
    mywindow.document.write('<html class="print"><head><title>' + document.title  + '</title>');
    mywindow.document.write('</head><body >');
    mywindow.document.write('<h1>' + document.title  + '</h1>');
    mywindow.document.write(document.getElementById(elem).innerHTML);
    mywindow.document.write('</body></html>');

    mywindow.document.close(); // necessary for IE >= 10
    mywindow.focus(); // necessary for IE >= 10*/

    mywindow.print();
    mywindow.close();

    return true;
}

//today date calling function
function getMoviesOnDate(date){

    $.ajax({
        url: base_url + "ajax/website_common_ajax/get_movies",
        type: 'GET',
        data: {
            "date" : date
        },
        success: function(jsonData) {
            $(".movie_select").empty();
            $(".movie_select").append(jsonData);



            //get theaters according to the movie id (movie time page)
            $('.selectMovie').on('change', function() {
                var movie_id=$(this).val();


                $.ajax({
                    url: base_url + "ajax/website_common_ajax/getTheaters",
                    type: 'GET',
                    data: {
                        "movie_id" : movie_id
                    },
                    success: function(jsonData) {
                        $(".theater_select").empty();
                        $(".theater_select").append(jsonData);
                    }
                });

            });
        }
    });
}


$(window).load(function(){

    $('.pageLoaderCont').slideUp();
});


//////// FUNCTIONS for SEO and href //////////

//home page banner pink link
function getHomeBannerPink()
{
    ga('send', 'event', 'banner', 'click', 'buy_online');
}

//home page banner blue link
function getHomeBannerBlue()
{
    ga('send', 'event', 'banner', 'click', 'now_showing');
}

//navigation buy online tickets button
function getNavBuyButton()
{
    ga('send', 'event', 'menu', 'click', 'buy_tickets');
}

//movie page book now button(In Theaters now)
function getMovieBookButton()
{
    ga('send', 'event', 'movies', 'click', 'book_tickets');
}

//movie page book now button(Coming to theaters)
function getMovieBookButtonComing()
{
    ga('send', 'event', 'movies', 'click', 'book_tickets');
}

//movie detailed page(Reserve Your Seat)
function getReserveYourSeatButton()
{
    ga('send', 'event', 'movie_detail', 'click', 'book_ticket');
    document.reserve_your_seat.submit();
}

//theater page onload and append book button(theater detailed page book button included)
function getTheaterBookButton()
{
    ga('send', 'event', 'theaters', 'click', 'book_tickets');
}

//login page login submit
function loginSubmit()
{
    ga('send', 'event', 'login', 'click', 'login');
    $('#login_form').submit();
}

//login page login with facebook button
function loginWithFacebookSubmit()
{
    ga('send', 'event', 'login', 'click', 'login_fb');
}

//login page sign up button
function signUpButton()
{
    ga('send', 'event', 'sign_up', 'click', 'sign_up');
}

//user registration next button
function userRegistrationSubmit()
{
    $('#frm-signup').submit();
}

//mobile validation verify button
function userMobileVerify()
{
    ga('send', 'event', 'sign_up', 'click', 'verify_number');
    document.user_mobile_verify.submit();
}
