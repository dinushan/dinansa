<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer contactUsPage">
   <div class="row">
      <div class="col-xs-12 breadcrumbCont">
		  <?php include(APPPATH.'views/frontend/inc/breadcrumb.php'); ?>
      </div>
		<div class="col-xs-12 headWrap">
	         <h2 class="heading">Terms and Conditions</h2>
	  </div>
		<div class="col-xs-12">
	  		<div class="termsDescWrap">
	  			<ol class="termsList">
	  				<li>
	  					<h3 class="termsHead">Introduction</h3>
	  					<div class="termsDesc">
	  						<p>www.scopecinemas.com  is owned and operated by Scope Cinemas Private Limited, a company registered in Sri Lanka (referred to as "SCOPE CINEMAS”, “www.scopecinemas.com”, "we," "us," or "our" herein).
							<strong>PLEASE READ THESE TERMS OF USE CAREFULLY BEFORE USING THIS SITE.</strong>
							By using this Site or by clicking a box that states that you accept or agree to these terms, you signify your agreement to these terms of use. If you do not agree to these terms of use, you may not use this Site.
							You acknowledge that these terms of use are supported by reasonable and valuable consideration, the receipt and adequacy of which are hereby acknowledged. Without limiting the generality of the foregoing, you acknowledge that such consideration includes your use of this Site and receipt of data, materials and information available at or through this Site, the possibility of our use or display of your Submissions (as defined below in Section 3, entitled "SUBMISSIONS") and the possibility of the publicity and promotion from our use or display of your Submissions.</p>
	  					</div>
	  				</li>
	  				
	  				<li>
	  					<h3 class="termsHead">Using the Site and Content</h3>
	  					<div class="termsDesc">
	  						<p>The Site is only for your personal use. You may not use the Site for commercial purposes or in any way that is unlawful, or harms us or any other person or entity, as determined in our sole discretion.</p>
	  						<p>The following are the terms and conditions applicable to access and use of this website (or ‘site’). By gaining access to and using this site, you agree to comply with these terms and conditions and all applicable laws, rules and regulations.</p>
	  						<p>If you are not agreeable with these Terms of Use, kindly refrain from accessing and using the service provided by www.scopecinemas.com.</p>
	  						<p>Registering with and/or the use of the www.scopecinemas.com services and products, including and without limitation the www.scopecinemas.com web site, purchase of movie and/or DVD –CD purchasing online, and other similar products or services offered by www.scopecinemas.com, is indicative that you have read and understood the terms and conditions set forth below and agree to be legally bound by the said terms and conditions.</p>
	  						<p>We reserve the right to change these terms of use from time to time by posting the new version on the website. It is your responsibility to check regularly this page for the latest terms and conditions.</p>
	  					</div>
	  				</li>
	  				<li>
	  					<h3 class="termsHead">Permitted Use</h3>
	  					<div class="termsDesc">
	  						<p>The service provided by www.scopecinemas.com  is for personal & non-commercial use.
						You hereby agree that you will not duplicate, download, publish, modify, distribute and/or use any material included in www.scopecinemas.com for any purpose, unless and otherwise you have obtained prior written permission from www.scopecinemas.com.</p>
						<p>You also agree that you will not use the services provided by www.scopecinemas.com for any purpose other than to subscribe and review the information in www.scopecinemas.com and purchase movie tickets and /or DVD – CD’s for your personal use.</p><p>
						Use of the material and / or content contained in www.scopecinemas.com for any purpose not set forth in these Terms of Use is strictly prohibited.</p>
	  					</div>
	  				</li>
	  				<li>
	  					<h3 class="termsHead">Suppliers Terms and Conditions for the Sale of Items</h3>
	  					<div class="termsDesc">
	  						<p>
Where applicable, terms and conditions will apply to individual items or groups of items in your transaction based upon who is supplying those items. The supplier of each item or group of items will be clearly displayed at time of booking and on your confirmation of purchase documents. Please make sure to read and understand the individual supplier’s terms and conditions in addition to these general conditions.</p>
	  					</div>
	  				</li>
	  				<li>
	  					<h3 class="termsHead">Payment</h3>
	  					<div class="termsDesc">
	  					<p>All payments are subject to the terms and conditions of each bank and we have no authority over any payments made through the bank payment gateway. We do not retain your payment details or any other significant information such as your credit card number, passwords, pin number etc. Details of your credit / debit card may be securely held by the bank which processes your transaction.</p>
	  						<p>All prices include Value Added Tax (VAT) and Nation Building Tax as applicable.</p>
	  					</div>
	  				</li>
	  				<li>
	  					<h3 class="termsHead">All Sales are Final</h3>
	  					<div class="termsDesc">
	  						<p>Cancellations or exchanges / amendments will NOT be entertained for any reason. (Date or time changes, partial performances, or lost tickets etc.) Therefore make sure to check your payment details before you make your payments.</p>
	  						<p>www.scopecinemas.com reserves the right to NOT to screen a movie ( and due to unavoidable circumstances) and in such cases refunds will be made to valid ticket holders.</p>
	  					</div>
	  				</li>
	  				<li>
	  					<h3 class="termsHead">Change or Suspension of Site</h3>
	  					<div class="termsDesc">
	  						<p>We reserve the right to modify or discontinue, temporarily or permanently, this Site or any part of this Site with or without notice. You agree that we shall not be liable to you or any third party for any modification, suspension or discontinuance of the Site and / or any Services under this agreement, for any reason. We do not guarantee continuous, uninterrupted or secure access to our service, and operation of our Site which may be interfered with by numerous factors outside of our control. In addition, the Site could be unavailable during certain periods of time while it is being updated and modified. During this time, the Site will be temporarily unavailable.</p>
	  					</div>
	  				</li>
	  				<li>
	  					<h3 class="termsHead"> Duplication of tickets (False ticket generation)</h3>
	  					<div class="termsDesc">
	  						<p>It is your responsibility to ensure that you carry the original confirmation receipt to theater with you.</p>
	  					</div>
	  				</li>
	  				<li>
	  					<h3 class="termsHead"> Registration, Accounts and Passwords</h3>
	  					<div class="termsDesc">
	  						<p>You agree to provide accurate, current and complete data about yourself on the  www.scopecinemas.com registration form and keep your profile updated with accurate, current and complete information.</p>
	  						<p>You also understand that failure to maintain accurate, current and complete information in your profile will give www.scopecinemas.com adequate cause to suspend or terminate your use of its services.</p>
	  						<p>Upon registration you will receive a User Name and Password. Continued maintenance of confidentiality with regard to your User Name and Password will always remain your sole responsibility. You agree to bring to the immediate notice of www.scopecinemas.com any unauthorized use of your password or account or any other breach of security. You also agree to ensure that you exit from your account each time you use www.scopecinemas.com. Access to and use of password protected and/or secure areas of www.scopecinemas.com is restricted to its registered users only. Unauthorized attempts to access these areas of www.scopecinemas.com may be subject to prosecution.</p>
	  					</div>
	  				</li>
	  				<li>
	  					<h3 class="termsHead">Privacy Policy</h3>
	  					<div class="termsDesc">
	  						<p>Your use of www.scopecinemas.com is governed by its Privacy Policy. You hereby agree to www.scopecinemas.com collection, use and sharing of your information as set forth in the www.scopecinemas.com privacy Policy.</p>
	  					</div>
	  				</li>
	  				<li>
	  					<h3 class="termsHead">User Conduct</h3>
	  					<div class="termsDesc">
	  						<p>You agree that you will not use www.scopecinemas.com to upload, post, or otherwise distribute or facilitate distribution of any material that is,</p>
	  						<ul class="innerList">
	  							<li>Libelous, defamatory or slanderous.</li>
								<li>Sexually suggestive or contains explicit sexual content (including nudity) and Child Pornography.</li>
								<li>Does or may denigrate or offend any individual or group on the basis of religion, gender, sexual orientation, race, ethnicity, age, or disability.</li>
								<li>Does or may threaten, abuse, harass, or invade the privacy of any third party.</li>
								<li>Infringes the rights of any third party, including, without limitation, patent, trademark, trade secret, copyright, right of publicity, or other proprietary rights.</li>
								<li>Condones, promotes, contains or links to cracks, hacks or similar utilities or programs.</li>
								<li>Constitutes unauthorized or unsolicited advertising, junk or bulk e-mail (also known as “spam”), chain letters, any other form of unauthorized solicitation, or any form of lottery or gambling.</li>
								<li>Contains a software virus or any other computer code that is designed or intended to disrupt, damage, or limit the functioning of any software, hardware, or telecommunications equipment, or to damage or obtain unauthorized access to any data or other information of any third party.</li>
								<li>Impersonates any person or entity, including any employee or representative of www.scopecinemas.com.</li>
								<li>Violates any applicable law or these Terms of Use.</li>
	  						</ul>
	  						<p>You also agree that you will not collect information about third parties via www.scopecinemas.com or use any such information for the purpose of transmitting and / or facilitating transmission of unauthorized or unsolicited advertising, junk or bulk e-mail, chain letters, or any other form of unauthorized solicitation.</p>
	  						<p>You agree that you will not engage in the systematic retrieval of data or other content from www.scopecinemas.com to create or compile, directly or indirectly, a collection, compilation, database or directory, without www.scopecinemas.com’s prior written consent. You agree not to take any actions for the purpose of manipulating or distorting, or that may undermine the integrity and accuracy of, any ratings or reviews of any movie or other entertainment program, service or product that may be presented by www.scopecinemas.com.</p>
	  					</div>
	  				</li>
	  				<li>
	  					<h3 class="termsHead">Copyrights</h3>
	  					<div class="termsDesc">
	  						<p>www.scopecinemas.com respects the intellectual property of others, and we ask our users to do the same. If notified of allegedly infringing, defamatory, damaging, illegal, or offensive content, www.scopecinemas.com may, in its sole discretion, investigate the allegation and/or edit, remove or request the removal of such content. Notwithstanding the foregoing, www.scopecinemas.com does not ensure that any such content will be edited or removed. www.scopecinemas.com may terminate the accounts of users who infringe the intellectual property rights of others.</p>
	  					</div>
	  				</li>
	  				<li>
	  					<h3 class="termsHead">Proprietary Rights</h3>
	  					<div class="termsDesc">
	  					<p>www.scopecinemas.com owns all right, title and interest in and to this service and all materials and content contained in www.scopecinemas.com, including, without limitation, all content, site design, logos, button icons, images, digital downloads, data compilations, text, and graphics are protected by copyright, trademark and other intellectual property laws. Any unauthorized use of the materials provided as part of the www.scopecinemas.com is strictly prohibited.
</p>	
	  						<p>
Permission is granted to individual consumers to electronically copy and to print hard copy portions of the Site solely for personal use. Any other use of materials on the site, including reproduction for purposes other than those noted above, modification, distribution, or republication, any form of data extraction or data mining, or other commercial exploitation of any kind, without prior written permission of an authorized officer of www.scopecinemas.com is strictly prohibited. You agree that you will not use any robot, spider, other automatic device, or manual process to monitor or copy our Web pages or the content contained therein without prior written permission of an authorized officer of www.scopecinemas.com.</p>
	  					</div>
	  				</li>
	  				<li>
	  					<h3 class="termsHead">Termination</h3>
	  					<div class="termsDesc">
	  						<p>In its sole and absolute discretion, with or without notice to youwww.scopecinemas.com may.</p>
	  						<ul class="innerList">
	  							<li> Suspend or terminate your use of the services provided by www.scopecinemas.com.</li>
	  							<li>Terminate your account at www.scopecinemas.com.</li>
	  						</ul>
	  						<p>You may terminate your account by informing in writing to www.scopecinemas.com about your decision to terminate your account and www.scopecinemas.com will de-activate your account upon receipt of such notification.  www.scopecinemas.com shall not be liable to you or any third party for any claims or damages arising out of any termination or suspension of its service.</p>
	  					</div>
	  				</li>
	  				<li>
	  					<h3 class="termsHead">General</h3>
	  					<div class="termsDesc">
	  						<p>The Terms of Use and the relationship between you and www.scopecinemas.com shall be governed by the laws of the Republic of Sri Lanka. The section titles in these Terms of Use are for convenience only and have no legal or contractual effect.</p>
	  						<p>These terms and conditions are binding upon and shall ensure the benefit of both parties, their respective successors, heirs, executor, administrators, personal representatives and permitted assigns.</p>
	  						<p>You shall not assign your rights or obligations hereunder without www.scopecinemas.com’s prior written consent.
</p>
	  					</div>
	  				</li>

	  			</ol>
	  		</div>

    	 </div>
    </div>
</div>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>