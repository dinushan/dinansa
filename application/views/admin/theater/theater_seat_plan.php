<link href="<?php echo base_url() ?>skin_admin/global/plugins/jquery.event.drag-2.2/style.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery.event.drag-2.2/jquery.event.drag-2.2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery.event.drag-2.2/jquery.event.drag.live-2.2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery.event.drag-2.2/jquery.event.drop-2.2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery.event.drag-2.2/jquery.event.drop.live-2.2.js" type="text/javascript"></script>

<!-- BEGIN CONTAINER -->
<div class="page-container">
<?php $this->load->view('admin/inc_sidebar_navigation.php');?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">


    <h3 class="page-title"><?php echo $theater_data->name ?></h3>
    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn default" href="<?php echo site_url('admin/theater/') ?>"><i
                        class="fa fa-chevron-left"></i> Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12"> <?php echo get_message() ?>
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light bordered">

                <div class="portlet-body form">


                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a id="btn-draw">Draw </a>
                        </li>
                        <li class="">
                            <a id="btn-label">Label</a>
                        </li>
                    </ul>

                    <!-- BEGIN FORM-->
                    <?php
                    $attr = array("id" => "edit_form", "class" => "form-horizontal");
                    echo form_open(current_url(), $attr);
                    ?>

                    <div id="div-add-rows">
                        <div class="form-group">
                            <label class="control-label col-md-3">Add Rows</label>
                            <div class="control-element col-md-4">

                                <table class="table table-bordered table-hover tbl-block-seat">
                                    <thead>
                                    <tr>
                                        <th>Row</th>
                                        <th>Seats</th>
                                        <th style="width:10px"><span class="glyphicon glyphicon-plus addBtn" id="addBtn_0"></span></th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php
                                    if(count($seat_arrangement) == 0): ?>
                                        <tr id="tr_0">
                                            <td>A</td>
                                            <td>
                                                <input type="text" name='seats[]' class="form-control required-entry"/>
                                            </td>
                                            <td></td>
                                        </tr>
                                    <?php
                                    else:
                                        $str = 'A';
                                        foreach($seat_arrangement as $k=>$count):
                                            ?>
                                            <tr id='tr_<?php echo $k ?>'>
                                                <td><?php echo  $str++ ?></td>
                                                <td>
                                                    <input type="text" name='seats[]' value="<?php echo $count ?>" class="form-control required-entry"/>
                                                </td>
                                                <td><span class="glyphicon glyphicon-minus addBtnRemove" id="addBtnRemove_<?php echo $k ?>"></span></td>
                                            </tr>

                                        <?php
                                        endforeach;
                                    endif;
                                    ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <?php
                        //if($stadium_ground_plan->status != 2):
                        ?>
                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <div class="btn-group">
                                        <?php if(count($seat_arrangement) > 0): ?>
                                        <span class="help-block">Note:</span>
                                        <?php endif; ?>
                                        <button type="submit" data-id="3" class="btn btn-submit green">Generate</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php //endif; ?>
                    </div>

                    <div id="box-seat-plan" class="form-actions">

                        <div class="block-plan-wrapper"><div class="block-plan-container"></div></div>

                                    <span class="help-block">
                                       - For move multiple seats - select seats and use "a" key or "d" key move<br />
                                       - For disable seat - select a seat and press "Delete" key<br />
                                    </span>
                    </div>
                    <?php
                    //if($stadium_ground_plan->status != 2):
                    ?>
                    <div id="form-actions-save-seat" class="form-actions" style="display: none;">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <div class="btn-group">
                                    <button type="button" class="btn btn-save green">Save</button>
                                    <?php if(count($seat_arrangement) > 0): ?>
                                    <button type="button" class="btn btn-update">Update</button>
                                    <?php endif; ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php //endif; ?>

                    </form>
                </div>



            </div>
        </div>
    </div>




    <script type="text/javascript">
    var el = $("#box-seat-plan");
    var tab_st = 'draw';

    $('#btn-draw').on('click', function () {
        $('.nav-tabs').find('.active').removeClass('active');
        $(this).parent('li').addClass('active');

        $('#div-add-rows').show();

        $('.block-plan-container').removeClass('editor-label');
        $('.block-plan-container').addClass('editor-draw');

        //$('.block-plan-container').find('.seat_item').addClass('d');
        tab_st = 'draw';

        //$( '.seat_item').removeAttr("contenteditable");
    });
    $('#btn-draw').click();

    $('#btn-label').on('click', function () {
        $('.nav-tabs').find('.active').removeClass('active');
        $(this).parent('li').addClass('active');

        $('#div-add-rows').hide();

        $('.block-plan-container').addClass('editor-label');
        $('.block-plan-container').removeClass('editor-draw');

        //$('.block-plan-container').find('.seat_item').removeClass('d');
        tab_st = 'label';
        $( '.seat_item' ).removeClass("selected_seat");

        //$( '.seat_item').attr("contenteditable","true");

    });



    $('.addBtn').on('click', function () {
        addTableRow($(this).parents('table'));
    });

    $('.addBtnRemove').click(function () {
        $(this).closest('tr').remove();
    })


    function addTableRow(tbl)
    {
        $i = tbl.find('tbody').find('tr').length;

        if($i == 0){
            $_h = '<tr>' +
                '<td>1</td>' +
                '<td><input type="text"  name="seats[]" class="form-control input-md required-entry"/></td>' +
                '<td></td>' +
                '</tr>';
        }else{
            $_h = '<tr>' +
                '<td></td>' +
                '<td><input type="text"  name="seats[]" class="form-control input-md required-entry"/></td>' +
                '<td><span class="glyphicon glyphicon-minus addBtnRemove" id="addBtn_' + $i + '"></span></td>' +
                '</tr>';
        }


        var tempTr = $_h;
        tbl.append(tempTr);

        var str = 'A';
        tbl.find('tbody').find('tr').each(function(index){
            //$(this).find('td:first').html(index+1);
            $(this).find('td:first').html(str);

            str = nextChar(str);
        });



        $(document.body).on('click', '.addBtnRemove', function (e) {
            $(this).closest('tr').remove();
        });
    }


    $('.required-entry').attr('required', 'required');

    $('#edit_form').validate({
        errorElement: 'span',
        errorClass: 'validation-advice',
        ignore: "",
        submitHandler: function (form) {
            set_validation('');
        }
    });

    var ctrl = false;
    var shift = false;
    var $seats_with = 26;
    var $space = 30;
    var $seats = [];

    var $seat_obj = <?php echo json_encode($seat_plan) ?>;

    function set_validation(_seat_obj)
    {

        var $sc =  0;
        $seats = [];

        $('.tbl-block-seat').find('input[type="text"]').each(function(){
            $sc += Number($(this).val());

            $seats.push($(this).val());
        })


        $('#form-actions-save-seat').show();
        generate_seat_plan(_seat_obj);

    }

    if($seat_obj.length > 0){
        set_validation($seat_obj);
    }

    <?php
    //if($stadium_ground_plan->status != 2):
    ?>
    function ajax_send_seat_plan()
    {
        jsonObj = [];

        $( '#item-container' ).find('.seat_item').each(function(index, element)
        {
            $offset = $(this).position();


            $seat_id = $(this).data('id');
            $seat_row = $(this).data('row');
            $seal_no = $(this).html();
            $top = Number($offset.top);
            $left = Number($offset.left);
            if ( $( this ).hasClass('hide_seat') ){
                $seal_status = 0;
            }else{
                $seal_status = 1;
            }

            item = {}
            //item ["seat_id"] = $seat_id;
            item ["seat_row"] = $seat_row;
            item ["seal_no"] = $seal_no;
            item ["top"] = $top;
            item ["left"] = $left;
            item ["status"] = $seal_status;
            jsonObj.push(item);

        });

        var obj_seat = JSON.stringify($seats);
        var obj_seat_plan = JSON.stringify(jsonObj);

        Metronic.blockUI({
            target: el,
            animate: true,
            overlayColor: 'none'
        });

        $.post(base_url+'admin/theaterSeatPlan/ajax_save_seat_plan',{
            theater_id:<?php echo $theater_data->id ?>,
            seat:obj_seat,
            seat_plan:obj_seat_plan
        },function(jsonData){
            Metronic.unblockUI(el);
            if(jsonData['status'] == true){
                jAlert('Seat Plan has been updated','Success');
            }else{
                jAlert(jsonData['message'],'Validation');
            }
        },'json');
    }

    function ajax_update_seat_plan()
    {
        jsonObj = [];

        $( '#item-container' ).find('.seat_item').each(function(index, element)
        {
            $offset = $(this).position();


            $seat_id = $(this).data('id');
            $seat_row = $(this).data('row');
            $seal_no = $(this).html();
            $top = Number($offset.top);
            $left = Number($offset.left);
            if ( $( this ).hasClass('hide_seat') ){
                $seal_status = 0;
            }else{
                $seal_status = 1;
            }

            item = {}
            item ["seat_id"] = $seat_id;
            item ["seat_row"] = $seat_row;
            item ["seal_no"] = $seal_no;
            item ["top"] = $top;
            item ["left"] = $left;
            item ["status"] = $seal_status;
            jsonObj.push(item);

        });

        var obj_seat = JSON.stringify($seats);
        var obj_seat_plan = JSON.stringify(jsonObj);

        Metronic.blockUI({
            target: el,
            animate: true,
            overlayColor: 'none'
        });

        $.post(base_url+'admin/theaterSeatPlan/ajax_update_seat_plan',{
            theater_id:<?php echo $theater_data->id ?>,
            seat:obj_seat,
            seat_plan:obj_seat_plan
        },function(jsonData){
            Metronic.unblockUI(el);
            if(jsonData['status'] == true){
                jAlert('Seat Plan has been updated','Success');
            }else{
                jAlert(jsonData['message'],'Validation');
            }
        },'json');
    }

    $('.btn-save').on('click',function()
    {
        ajax_send_seat_plan();
    });

    $('.btn-update').on('click',function()
    {
        ajax_update_seat_plan();
    });
    <?php
    //endif;
    ?>

    function generate_seat_plan(data)
    {

        $('.block-plan-container').html('');
        $('#item-container').html('');


        $html_seat_lbl = '<div id="item-container"></div>';
        $html_seat = '';


        var str = 'A';

        if(data == '')
        {
            for($i = 0; $i < $seats.length; $i++)
            {



                $top = $i * ($space);

                var s = $seats[$i];
                for($j = 0; $j < $seats[$i]; $j++)
                {

                    $seal_no = str + s;
                    $left = $j * ($seats_with);
                    $html_seat += '<div data-id="" data-row="'+$i+'" data-seal_no="'+$seal_no+'" class="row_'+$i+' seat_item" style="top:'+$top+'px;left:'+$left+'px;" title="'+$seal_no+'">'+$seal_no+'</div>';


                    s--;
                }


                str = nextChar(str);

            }
        }
        else{

            for($i = 0; $i < data.length; $i++)
            {
                //console.log(data[$i]);
                $seat_id = data[$i].id;
                $row = data[$i].row;
                $top = data[$i].y;
                $left = data[$i].x;
                $seal_no = data[$i].label;
                $status = data[$i].status;

                if ( $status == 0){
                    $seal_class = ' hide_seat';
                }else{
                    $seal_class = '';
                }

                $html_seat += '<div data-id="'+$seat_id+'" data-row="'+$row+'" data-seal_no="'+$seal_no+'" class="row_'+$row+$seal_class+' seat_item" style="top:'+$top+'px;left:'+$left+'px;" title="'+$seal_no+'">'+$seal_no+'</div>';


            }
        }


        $maxSeatCount = Math.max.apply(Math,$seats)+100;

        //row lable
        /*for($i = 0; $i < $seats.length; $i++)
        {
            $top = $i * ($seats_with);
            $html_seat_lbl += '<div class="seat_lbl" style="top:'+($seats_with+$top)+'px;left:0px;">'+($i+1)+'</div>';
        }*/
        //colum lable
        /*for($j = 0; $j < $maxSeatCount; $j++)
        {
            $left = $j * ($seats_with);
            $html_seat_lbl += '<div class="seat_lbl" style="left:'+($seats_with+$left)+'px;top:0px;">'+($j+1)+'</div>';
        }*/

        $('.block-plan-container').html($html_seat_lbl);
        $('#item-container').html($html_seat);

        $('.block-plan-container').width(($seats_with*$maxSeatCount));
        $('#item-container').height($seats_with*$seats.length+500);

        $( '#item-container' )
            .drag("start",function( ev, dd ){
                return $('<div class="grid_selection" />')
                    .css('opacity', .65 )
                    .appendTo( document.body );
            })
            .drag(function( ev, dd )
            {
                if(tab_st == 'draw')
                {
                    $( dd.proxy ).css({
                        top: Math.min( ev.pageY, dd.startY ),
                        left: Math.min( ev.pageX, dd.startX ),
                        height: Math.abs( ev.pageY - dd.startY ),
                        width: Math.abs( ev.pageX - dd.startX )
                    });
                }
            })
            .drag("end",function( ev, dd ){
                $( dd.proxy ).remove();
            });


        $('.seat_item')
            .click(function(){
                if(tab_st == 'draw')
                {
                    $( this ).toggleClass("selected_seat");
                }else{
                    $( this).attr('contenteditable','true');
                    $( this ).focus();
                }
            })
            .drag("init",function(){
                if(tab_st == 'draw')
                {
                    if ( $( this ).is('.selected_seat') )
                        return $('.selected_seat');
                }
            })
            .drag(function( ev, dd ){
               /* if(tab_st == 'draw')
                {
                    $( this ).css({
                        top: ( dd.offsetY / 2 ) * 2,
                        left: ( dd.offsetX / 2 ) * 2
                    });
                }*/
            }
            ,{ relative:true })
            .mouseout(function() {
                $( this).removeAttr('contenteditable');
            });




        $('.seat_item')
            .drop("init",function(){
                if(tab_st == 'draw')
                {
                    if(!ctrl){
                        $( '.seat_item' ).removeClass("selected_seat");
                    }
                }
            })
            .drop("start",function(){
                  //$( this ).addClass("selected");
            })
            .drop(function( ev, dd ){
                if(tab_st == 'draw')
                {
                    $( this ).toggleClass("selected_seat");
                }
            });

        $.drop({ multi: true });

        $(document).keydown(function(e)
        {
            console.log(e.which);
            ctrl = false;
            shift = false;

            if (e.shiftKey)
            {
                shift = true;
            }

            switch (e.which) {
                case 65://a key
                    move('left');
                    break;
                case 68://d key
                    move('right');
                    break;
                case 87://w key
                    move('top');
                    break;
                case 90://z key
                    move('bottom');
                    break;
                case 46://delete key
                    hide_seat();
                    break;
                case 17://ctrl key
                    ctrl = true;
                    break;
            }

        })

    }


    function move(p){

        if(tab_st == 'draw')
        {
            var t = 1;
            if(shift){
                t = 12;
            }

            console.log(t);
            $( '#item-container' ).find('.selected_seat').each(function(index, element) {
                $offset = $(this).position();

                $left = Number($offset.left);
                $top = Number($offset.top);

                if(p == 'left')
                {
                    $left -= t;
                }
                if(p == 'right'){
                    $left += t;
                }

                if(p == 'top')
                {
                    $top -= t;
                }
                if(p == 'bottom'){
                    $top += t;
                }

                $(this).css('left',$left+'px');
                $(this).css('top',$top+'px');
            });
        }
    }

    function hide_seat()
    {
        if(tab_st == 'draw')
        {
            $( '#item-container' ).find('.selected_seat').each(function(index, element) {

                if ( $( this ).hasClass('hide_seat') ){
                    $( this ).removeClass('hide_seat');
                }else{
                    $( this ).addClass('hide_seat');
                }

            });
        }
    }

    </script>


</div>
<!-- END PAGE CONTENT-->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
