<!-- BEGIN CONTAINER -->

<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

<h3 class="page-title"><?php echo $page_action == "add" ? 'Add New' : 'Edit' ?> Theater</h3>
<div class="page-bar">
    <div class="page-toolbar">
        <div class="btn-group pull-right">
            <a class="btn default" href="<?php echo site_url('admin/' . $slug . '/') ?>"><i
                    class="fa fa-chevron-left"></i> Back</a>

            <?php if ($page_action == 'edit'): ?>
                <a class="btn blue" href="<?php echo site_url('admin/' . $slug . '/photos/' . $enc_id) ?>"><i
                        class="fa fa-plus"></i> Photos</a>
            <?php endif; ?>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12"> <?php echo get_message() ?>
        <!-- BEGIN VALIDATION STATES-->
        <div class="portlet light bordered">

            <div class="portlet-body form">
                <!-- BEGIN FORM-->
                <?php
                $attr = array("id" => "edit_form", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                echo form_open(current_url(), $attr);
                ?>
                <?php
                if ($page_action == 'edit')
                {
                    $theater_name       = $theater_data->name;
                    $description        = $theater_data->description;
                    $url_key            = $theater_data->url_key;
                    $logo               = $theater_data->logo;
                    $cinema             = $theater_data->cinema_id;
                    $image              = $theater_data->image;
                    $mobile_image       = $theater_data->image;

                    $meta_title 		= $theater_data->meta_title;
                    $meta_keywords 		= $theater_data->meta_keywords;
                    $meta_description 	= $theater_data->meta_description;
                    $og_type 			= $theater_data->og_type;
                    $og_title 			= $theater_data->og_title;
                    $og_description 	= $theater_data->og_description;

                } else
                {
                    $theater_name = '';
                    $description = '';
                    $url_key = '';
                    $logo = '';
                    $cinema = '';
                    $image = '';
                    $mobile_image = '';
                    $facilities_selected = array();

                    $meta_title 		= '';
                    $meta_keywords 		= '';
                    $meta_description 	= '';
                    $og_type 			= '';
                    $og_title 			= '';
                    $og_description 	= '';
                }
                ?>

                <h4 class="form-section">General</h4>

                <div class="form-group">
                    <label class="control-label col-md-3">Name<span class="req">*</span></label>

                    <div class="control-element col-md-4"><input name="theater_name" type="text"
                                                                 class="form-control required-entry"
                                                                 value="<?php echo set_value('theater_name', $theater_name) ?>"/>
                        <?php echo form_error('theater_name'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Description<span class="req">*</span></label>

                    <div class="control-element col-md-4"><textarea name="description"
                                                                 class="form-control required-entry"><?php echo set_value('description', $description) ?></textarea>
                        <?php echo form_error('description'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">URL Key<span class="req">*</span></label>

                    <div class="control-element col-md-4"><input name="url_key" type="text"
                                                                 class="form-control required-entry"
                                                                 value="<?php echo set_value('url_key', $url_key) ?>"/>
                        <?php echo form_error('url_key'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Cinema<span class="req">*</span></label>

                    <div class="control-element col-md-4">
                        <?php echo form_dropdown('cinema', $cinema_opt, set_value('cinema', $cinema), 'class="form-control required-entry cb_s2"'); ?>
                        <?php echo form_error('cinema'); ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3">Facilities<span class="req">*</span></label>

                    <div class="control-element col-md-4">
                        <?php echo form_dropdown('facility[]', $facilities_opt, set_value('facility[]', $facilities_selected), 'class="form-control required-entry cb_s2" multiple'); ?>
                        <?php echo form_error('facility[]'); ?>
                    </div>
                </div>

                <?php if ($page_action == 'edit' && $logo != ''): ?>
                    <div class="form-group">
                        <label class="control-label col-md-3">Logo <span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><img
                                            src="<?php echo base_url() . CINEMA_LOGO_IMG_PATH . $logo ?>?<?php echo time() ?>"
                                            alt="" width="200"/></td>
                                    <td>&nbsp;</td>
                                    <td><?php list($img_width, $img_height) = getimagesize(CINEMA_LOGO_2X_IMG_PATH . $logo); ?>
                                        <strong>Image Info</strong><br/>
                                        width:<?php echo $img_width ?>px<br/>
                                        height:<?php echo $img_height ?>px
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><label>
                                            <input name="logo_del" type="checkbox" value="1"/>
                                            Image Delete </label>

                                        <div class="help-block font-green">Image Size:<?php echo CINEMA_LOGO_IMG_W ?>
                                            px &times; <?php echo CINEMA_LOGO_IMG_H ?>px, Image Type: png<br />
                                            If you want to upload a image, first delete the existing file.
                                        </div>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="form-group">
                        <label class="control-label col-md-3">Logo <span class="req">*</span></label>

                        <div class="control-element col-md-4"><input type="file" class=" input-file" name="logo">

                                <span class="help-block font-green">Image Size:<?php echo CINEMA_LOGO_IMG_W ?>
                                    px &times; <?php echo CINEMA_LOGO_IMG_H ?>px, Image Type: png</span>
                        </div>
                    </div>
                <?php endif; ?>

                <?php if ($page_action == 'edit' && $image != ''): ?>
                    <div class="form-group">
                        <label class="control-label col-md-3">Image <span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                <tr>
                                    <td><img
                                            src="<?php echo base_url() . THEATER_IMG_PATH . $image ?>?<?php echo time() ?>"
                                            alt="" width="200"/></td>
                                    <td>&nbsp;</td>
                                    <td><?php list($img_width, $img_height) = getimagesize(THEATER_2X_IMG_PATH . $image); ?>
                                        <strong>Image Info</strong><br/>
                                        width:<?php echo $img_width ?>px<br/>
                                        height:<?php echo $img_height ?>px
                                    </td>
                                </tr>
                                <tr>
                                    <td colspan="3"></td>
                                </tr>
                                <tr>
                                    <td colspan="3"><label>
                                            <input name="landscape_image_del" type="checkbox" value="1"/>
                                            Image Delete </label>

                                        <div class="help-block font-green">Image Size:<?php echo THEATER_LANDSCAPE_IMG_W ?>
                                            px &times; <?php echo THEATER_LANDSCAPE_IMG_H ?>px, Image Type: jpg<br />
                                            If you want to upload a image, first delete the existing file.
                                        </div>
                                </tr>
                                <tr>
                                    <td colspan="3">&nbsp;</td>
                                </tr>
                            </table>
                        </div>
                    </div>
                <?php else: ?>
                    <div class="form-group">
                        <label class="control-label col-md-3">Image <span class="req">*</span></label>

                        <div class="control-element col-md-4"><input type="file" class=" input-file" name="landscape_image">

                                <span class="help-block font-green">Image Size:<?php echo THEATER_LANDSCAPE_IMG_W ?>
                                    px &times; <?php echo THEATER_LANDSCAPE_IMG_H ?>px, Image Type: jpg</span>
                        </div>
                    </div>
                <?php endif; ?>

                <h4 class="form-section">Meta Settings</h4>
                <div class="form-group">
                    <label class="control-label col-md-3">Meta Title</label>
                    <div class="control-element col-md-4">
                        <input type="text" class="form-control" value="<?php echo
                        $page_action == "add" ?
                            set_value('meta_title') :
                            set_value('meta_title',$meta_title) ?>"
                               name="meta_title" />
                    </div>
                </div>

                <?php if(SHOW_META_CONTENT === TRUE):?>
                    <div class="form-group">
                        <label class="control-label col-md-3">Meta Keywords</label>
                        <div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="meta_keywords"><?php echo $page_action == "add" ?
                                            set_value('meta_keywords') :
                                            set_value('meta_keywords',$meta_keywords) ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">Meta Description</label>
                        <div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="meta_description"><?php echo $page_action == "add" ?
                                            set_value('meta_description') :
                                            set_value('meta_description',$meta_description) ?></textarea>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">OG Type</label>
                        <div class="control-element col-md-4">
                            <input type="text" class="form-control" value="<?php echo
                            $page_action == "add" ?
                                set_value('og_type') :
                                set_value('og_type',$og_type) ?>"
                                   name="og_type" />
                            <span class="help-block font-green">The type of the object, such as 'article'</span></div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">OG Title</label>
                        <div class="control-element col-md-4">
                            <input type="text" class="form-control" value="<?php echo
                            $page_action == "add" ?
                                set_value('og_title') :
                                set_value('og_title',$og_title) ?>"
                                   name="og_title" />
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">OG Description</label>
                        <div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="og_description"><?php echo $page_action == "add" ?
                                            set_value('og_description') :
                                            set_value('og_description',$og_description) ?></textarea>
                        </div>
                    </div>
                <?php endif; ?>



                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <div class="btn-group">
                                <input type="hidden" id="hidden-action" name="hidden-action" value="1">
                                <button type="submit" data-id="1" class="btn btn-submit green">Save & Return
                                </button>
                                <button type="submit" data-id="2" class="btn btn-submit btn-default">Save & New
                                </button>
                                <button type="submit" data-id="3" class="btn btn-submit btn-default">Save & Edit
                                </button>
                            </div>
                        </div>
                    </div>
                </div>


                <script type="text/javascript">

                    $('.required-entry').attr('required', 'required');
                    $('#edit_form').validate({
                        errorElement: 'span',
                        errorClass: 'validation-advice',
                        ignore: "",
                        submitHandler: function (form) {
                            form.submit();
                        }
                    });
                </script>
                </form>
            </div>
        </div>
    </div>
</div>

</div>
<!-- END PAGE CONTENT-->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->