<?php

class Migration_Update_Promotion_Table extends CI_Migration
{
    public function up()
    {
        $fields = array(
            'apply_for_all' => array(
                'type' => 'BOOLEAN ',
                'after' => 'bank',
                'default' => 0
            ),
            'discount_type_for_all' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
                'after' => 'apply_for_all',
                'null' => true,
            ),
            'amount_for_all' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
                'after' => 'discount_type_for_all',
                'null' => true,
                'default' => 0.00
            )
        );

        $this->dbforge->add_column('promotions', $fields);
    }

    public function down()
    {
    }
}
