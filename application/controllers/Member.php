<?php if (!defined('BASEPATH')) exit('No direct script access allowed');
use Dompdf\Dompdf;
use Illuminate\Support\Collection;

class Member extends Elevate_Controller
{

    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');

        $this->load->model('MemberModel');
        $this->load->model('MemberChildModel');
        $this->load->model('MemberPreferenceCategoryModel');
        $this->load->model('MemberPreferenceItemsModel');
        $this->load->model('MemberPreferenceCategoryItemModel');
        $this->load->model('MemberRenewalModel');
        $this->load->model('CompanyModel');
    }

    public function index()
    {

        $pageData['slug'] = 'member';
        $pageData['title'] = 'Waiting Members';
        $pageData['sub_page1'] = 'Waiting members';
        // $pageData['users'] = MemberModel::get();

        $companies = CompanyModel::where("status", 1)->get();
        $array = [];
        $array[''] = 'Select Option';
        foreach ($companies as $raw) {
            $array[$raw->id] = $raw->company_name;
        }
        $pageData['companies'] = $array;
        $pageData['months'] = [
            "0" => "Select Option",
            "1" => "January", "2" => "February", "3" => "March", "4" => "April",
            "5" => "May", "6" => "June", "7" => "July", "8" => "August",
            "9" => "September", "10" => "October", "11" => "November", "12" => "December",
        ];


        //dd($pageData['users']);
        $data['page_content'] = $this->load->view('admin/member/members', $pageData, true);
        $this->parser->parse('common/master_template', $data);

    }

    public function waitingQueue()
    {

        $pageData['slug'] = 'waitingQueue';
        $pageData['title'] = 'Waiting Members';
        $pageData['sub_page1'] = 'Waiting members';
        $pageData['users'] = MemberModel::get();

        //dd($pageData['users']);
        $data['page_content'] = $this->load->view('admin/member/waiting_members', $pageData, true);
        $this->parser->parse('common/master_template', $data);


    }

    public function delete($id)
    {
        $id = $this->encrypt_lib->decode($id);
        MemberModel::where(['id' => $id])->delete();
        MemberChildModel::where(['user_id' => $id])->delete();
        MemberPreferenceCategoryItemModel::where(['member_id' => $id])->delete();

        redirect('member/waitingQueue');
    }

    public function downloadPDF($id)
    {
        $id = $this->encrypt_lib->decode($id);
        $memberData = MemberModel::where(['id' => $id])
            ->select('title', 'first_name', 'last_name', 'email', 'mobile', 'office_number', 'dob', 'authorize_signature', 'company_name', 'occupation', 'home_address', 'office_address', 'nationality', 'created_at', 'is_corporate', 'corporate_id')
            ->first();

        $MemberChildModel = new Collection(MemberChildModel::where(['user_id' => $id])
            ->select('title', 'user_id', 'member_id', 'user_type', 'first_name', 'last_name', 'email', 'mobile', 'office_number', 'dob', 'authorize_signature', 'company_name', 'occupation', 'home_address', 'office_address', 'nationality')
            ->get());

        $memberNominee = $MemberChildModel->where("user_type", 1)->first();
        $memberSpouse = $MemberChildModel->where("user_type", 2)->first();
        $memberChildren = $MemberChildModel->where("user_type", 3)->all();

        $this->load->database();
        $this->db->from('member_renewal');
        $this->db->where('member_id', $id);
        $this->db->order_by("id", "desc");
        $membership = $this->db->get()->row();

        $pageData['profile'] = [
            'memberData' => $memberData,
            'memberNominee' => $memberNominee,
            'memberSpouse' => $memberSpouse,
            'memberChildren' => $memberChildren,
            'membership' => $membership,
            'signatures' => [
                'nominee' => null,
                'spouse' => null,
                'child1' => null,
                'child2' => null,
                'child3' => null,
                'child4' => null,
            ],
        ];

        // dd($membership);
        // $this->load->view('admin/member/profile_pdf', $pageData);
        //return 1;

        $dompdf = new Dompdf();
        $html = $this->load->view('admin/member/profile_pdf', $pageData, true);

        // $this->load->view('admin/user/pdf_test', ['hi' => "0779233884"], true);
        $dompdf->loadHtml($html);
        // (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');

        // Render the HTML as PDF
        $dompdf->render();
        $dompdf->stream("Certificate of Membership -" . $memberNominee->member_id . ".pdf");
        // Output the generated PDF to Browser
        $dompdf->stream();
    }

    public function emailPDF($id)
    {
        $id = $this->encrypt_lib->decode($id);
        $certificateHolder = MemberModel::where('id', $id)->first();
        $memberData = MemberChildModel::where([
            'user_id' => $id,
            'user_type' => 1
        ]);
        $memberData->update(['status' => 1]);
        $memberData = $memberData->first();
        $this->load->database();
        //$oneYearOn = date('Y-m-d', strtotime(date("Y-m-d", mktime()) . " + 365 day"));
        $oneYearOn = date('Y-m-d', strtotime('+1 year'));
        $this->db->insert('member_renewal', ['member_id' => $id, "start_date" => date('Y-m-d'), "end_date" => $oneYearOn, "is_active" => 1, 'approved_by' => 1]);


        if ($memberData->title == "Mr" || $memberData->title == "Mrs" || $memberData->title == "Prof" || $memberData->title == "Dr") {
            $gender = "male";
        } else {
            $gender = "female";
        }
        $password = "El1".substr(str_shuffle(str_repeat("0123456789abcdefghijklmnopqrstuvwxyz", 4)), 0, 4);

        $userData = [
            "title" => $memberData->title,
            "first_name" => $memberData->first_name,
            "last_name" => $memberData->last_name,
            "email" => $memberData->email,
            "phone" => $memberData->mobile,
            "password" => $password,
            "dob" => $memberData->dob,
            "gender" => $gender,
            "is_member_app" => "1"
        ];
        //dd($userData);
        $this->sendRequest('user/register', $userData);

        //        $MemberChildModel = new Collection(MemberChildModel::where(['user_id' => $id])->get());
        //        $memberSpouse = $MemberChildModel->where("user_type", 1)->first();
        //        $memberChildren = $MemberChildModel->where("user_type", 2)->all();
        //        $memberNominee = $MemberChildModel->where("user_type", 3)->first();
        //
        //        $pageData['profile'] = [
        //            'memberData' => $memberData,
        //            'memberNominee' => $memberNominee,
        //            'memberSpouse' => $memberSpouse,
        //            'memberChildren' => $memberChildren,
        //            'signatures' => [
        //                'nominee' => null,
        //                'spouse' => null,
        //                'child1' => null,
        //                'child2' => null,
        //                'child3' => null,
        //                'child4' => null,
        //            ],
        //        ];
        //        $domPdf = new Dompdf();
        //        $html = $this->load->view('admin/member/profile_pdf', $pageData, true);
        //        $domPdf->loadHtml($html);
        //        $domPdf->setPaper('A4', 'portrait');
        //        $domPdf->render();
        //        $output = $domPdf->output();
        $certificateHolder_to = $certificateHolder->email;
        $certificateHolder_name = $certificateHolder->first_name;

        $memberData_to = $memberData->email;
        $memberData_name = $memberData->first_name;
        $memberData_fullname = $memberData->first_name . ' ' . $memberData->last_name;
        $memberData_ID = $memberData->member_id;

        $this->load->library('email');
        $this->email->from('info@elevate.lk');
        $this->email->subject('Elevate Application Submitted');
        $this->email->to($memberData_to);
        $this->email->bcc('nu1.rock@gmail.com');
        $this->email->message('Hi ' . $memberData_name . ',<br><p>You’ve been approved as an Elevate member.</p><p>Your member ID is : ' . $memberData_ID . '</p><p>We hope you enjoy the experience.</p><br>Thank You,<br>Elevate Staff');
        $this->email->set_mailtype("html");
        //$this->email->attach($output, 'application/pdf', "Pdf File " . date("m-d H-i-s") . ".pdf", false);
        $this->email->send();

        if ($certificateHolder_to == $memberData_to) {
            return;
        }

        $this->email->from('info@elevate.lk');
        $this->email->subject('Elevate Application Submitted');
        $this->email->to($certificateHolder_to);
        $this->email->bcc('nu1.rock@gmail.com');
        $this->email->message('Hi ' . $certificateHolder_name . ',<br><p>We’d like to let you know that ' . $memberData_fullname . ' has just been approved as an Elevate member under your company certificate.</p><br>Thank you,<br>Elevate Staff');
        $this->email->set_mailtype("html");
        //$this->email->attach($output, 'application/pdf', "Pdf File " . date("m-d H-i-s") . ".pdf", false);
        $this->email->send();

    }

    public function viewProfile($id)
    {
        $id = $this->encrypt_lib->decode($id);
        $pageData['slug'] = 'waitingQueue';
        $pageData['title'] = 'Waiting Members';
        $pageData['sub_page1'] = 'Waiting members';

        $preferences = MemberPreferenceCategoryModel::all();

        $array = [];
        $array['0'] = 'Select Preferences';
        foreach ($preferences as $raw) {
            $array[$raw->id] = $raw->name;
        }

        $certificateHolder = MemberModel::where(['id' => $id])->first();
        $MemberChildModel = new Collection(MemberChildModel::where(['user_id' => $id])->get());
        $memberData = $MemberChildModel->where("user_type", 1)->first();
        $memberSpouse = $MemberChildModel->where("user_type", 2)->first();
        $memberChildren = $MemberChildModel->where("user_type", 3)->all();


        $this->load->database();
        $this->db->from('member_renewal');
        $this->db->where('member_id', $id);
        $this->db->order_by("id", "desc");
        $membership = $this->db->get()->row();


        if ($this->input->post()) {
            $preference = ($this->input->post('preference'));
            foreach ($preference as $key => $value) {
                $status = MemberPreferenceCategoryItemModel::where([
                    "member_id" => $id,
                    "preference_id" => $this->input->post('preference_id'),
                    "item_id" => $key,
                ])->update([
                    "comment" => isset($_POST['pref_check'][$key]) ? $_POST['pref_check'][$key] : "",
                    'is_checked' => isset($_POST['pref_check'][$key])]);

                if (!$status) {
                    $memberPreferenceCategoryItemModel = new MemberPreferenceCategoryItemModel();
                    $memberPreferenceCategoryItemModel->member_id = $id;
                    $memberPreferenceCategoryItemModel->preference_id = $this->input->post('preference_id');
                    $memberPreferenceCategoryItemModel->item_id = $key;
                    $memberPreferenceCategoryItemModel->comment = (isset($_POST['pref_check'][$key]) ? $_POST['pref_check'][$key] : "");
                    $memberPreferenceCategoryItemModel->is_checked = isset($_POST['pref_check'][$key]);
                    $memberPreferenceCategoryItemModel->save();
                }
            }

            $this->session->set_flashdata('success', $this->lang->line('messageAdd'));
        }


        $pageData['profile'] = [
            'memberData' => $memberData,
            'certificateHolder' => $certificateHolder,
            'memberSpouse' => $memberSpouse,
            'memberChildren' => $memberChildren,
            'membership' => $membership
        ];

        $getMemberPreferences = new Collection(MemberPreferenceCategoryItemModel::where(["member_id" => $id])->get());
        $pageData['getMemberPreferences'] = $getMemberPreferences;


        $pageData['preferences'] = $array;
        $data['page_content'] = $this->load->view('admin/member/profile_old', $pageData, true);
        $this->parser->parse('common/master_template', $data);

    }

    public function editProfile($uid)
    {
        $id = $this->encrypt_lib->decode($uid);
        $memberData = MemberModel::where(['id' => $id])
            ->select('title', 'first_name', 'last_name', 'email', 'mobile', 'office_number', 'dob', 'authorize_signature', 'occupation', 'home_address', 'office_address', 'nationality', 'created_at', 'is_corporate', 'corporate_id')
            ->first();
        $MemberChildModel = new Collection(MemberChildModel::where(['user_id' => $id])
            ->select('id', 'title', 'user_id', 'member_id', 'user_type', 'first_name', 'last_name', 'email', 'mobile', 'office_number', 'dob', 'authorize_signature', 'occupation', 'home_address', 'office_address', 'nationality')
            ->get());

        $memberNominee = $MemberChildModel->where("user_type", 1)->first();
        $memberSpouse = $MemberChildModel->where("user_type", 2)->first();
        $memberChildren = $MemberChildModel->where("user_type", 3);

        $this->load->database();
        $this->db->from('member_renewal');
        $this->db->where('member_id', $id);
        $this->db->order_by("id", "desc");
        $membership = $this->db->get()->row();

        $pageData['profile'] = [
            'memberData' => $memberData,
            'memberNominee' => $memberNominee,
            'memberSpouse' => $memberSpouse,
            'memberChildren' => $memberChildren,
            'membership' => $membership,
            'signatures' => [
                'nominee' => null,
                'spouse' => null,
                'child1' => null,
                'child2' => null,
                'child3' => null,
                'child4' => null,
            ],
        ];


        if ($this->input->post()) {

            $mem_array = preg_grep_keys('/^mem_/', $this->input->post());
            $nom_array = preg_grep_keys('/^nom_/', $this->input->post());
            $spo_array = preg_grep_keys('/^spo_/', $this->input->post());
            $child_array = $this->input->post()['child'];


            $mem_data = $this->prepData("mem", $mem_array);
            $where = ['id' => $id];
            MemberModel::where($where)->update($mem_data);

            $nom_data = $this->prepData("nom", $nom_array);
            $where = ['user_type' => 1, 'user_id' => $id];
            $nominee = MemberChildModel::firstOrCreate($where);
            $nominee->update($nom_data);

            $spo_data = $this->prepData("spo", $spo_array);
            $where = ['user_type' => 2, 'user_id' => $id];
            $spouse = MemberChildModel::firstOrCreate($where);
            $spouse->update($spo_data);

            if (isset($child_array)) {
                foreach ($child_array as $child) {

                    $data__ = [
                        'first_name' => isset($child['fname']) ? ucwords($child['fname']) : '',
                        'last_name' => isset($child['lname']) ? ucwords($child['lname']) : '',
                        'dob' => (isset($child['dob']) && strlen(trim($child['dob'])) > 0) ? $child['dob'] : null,
                    ];

                    $where = ['user_type' => 3, 'user_id' => $id, 'id' => isset($child['id']) ? $child['id'] : ''];
                    $child = MemberChildModel::firstOrCreate($where);
                    $child->update($data__);
                }
            }

            redirect(base_url('member/editProfile/' . $uid));

        }

        $pageData['slug'] = 'member';
        $pageData['title'] = 'Edit Members';

        $getMemberPreferences = new Collection(MemberPreferenceCategoryItemModel::where(["member_id" => $id])->get());
        $pageData['getMemberPreferences'] = $getMemberPreferences;

        $data['page_content'] = $this->load->view('admin/member/profile_edit', $pageData, true);
        $this->parser->parse('common/master_template', $data);

    }

    public function deletePreference()
    {
        MemberPreferenceModel::where('id', $this->input->post('item_id'))->delete();
    }

    public function addPreference($id)
    {
        $user_id = $this->encrypt_lib->decode($id);
        $item_id = $this->input->post('item_id');
        $data = [
            'preference_id' => $this->input->post('preferences_id'),
            'preference' => trim($this->input->post('preference')),
            'user_id' => $user_id
        ];

        $dataIn = MemberPreferenceModel::where(['user_id' => $user_id, 'preference_id' => $this->input->post('preferences_id')])->get();

        if (count($dataIn) > 0) {
            $result = MemberPreferenceModel::where(['user_id' => $user_id, 'preference_id' => $this->input->post('preferences_id')]);
            $result->update(['preference' => trim($this->input->post('preference'))]);

            $result = $result->first();
        } else {
            $result = MemberPreferenceModel::create($data);
            //  dd($result);
        }
        $this->output->set_content_type('application/json');
        $this->output->set_output(json_encode($result));

    }

    public function ajax_list_waiting_members()
    {
        $records = array();
        $records["data"] = array();
        if ($this->input->post()) {
            $this->session->set_userdata('waiting_member_filters', $this->input->post());
        }

        $sql = "SELECT * FROM member_dependencies WHERE status =0 AND user_type=1";

        $iTotalRecords = $this->db->query($sql)->num_rows();
        $iDisplayLength = intval($this->input->post('length'));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($this->input->post('start'));
        $sEcho = intval($this->input->post('draw'));


        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


        $filter_fullname = $this->input->post('name');
        $filter_username = $this->input->post('email');
        $filter_phone1 = $this->input->post('mobile');
        if ($filter_fullname) {
            $sql .= " AND first_name like '%" . $filter_fullname . "%'";
        }
        if ($filter_username) {
            $sql .= " AND email like '%" . $filter_username . "%'";
        }
        if ($filter_phone1) {
            $sql .= " AND mobile like '%" . $filter_phone1 . "%'";
        }

        $sort_array = array(
            'id',
            'first_name',
            'email',
            'mobile',
        );
        $order = $this->input->post('order');
        if ($order) {
            $column = $order[0]['column'];
            $dir = $order[0]['dir'];
            if (isset($sort_array[$column])) {
                $f = $sort_array[$column];

                if ($dir == 'asc')
                    $sql .= " ORDER BY $f ASC";
                else
                    $sql .= " ORDER BY $f DESC";
            }

        }


        $sql2 = $sql . " LIMIT " . $iDisplayStart . "," . $iDisplayLength;
        $list = $this->db->query($sql2);


        foreach ($list->result() as $row) {

            $encry_emId = $this->encrypt_lib->encode($row->user_id);

            $action_html = '';
            $status = '';

            $action_html = '';
            $view_url = "window.location='" . base_url('member/viewProfile/' . $encry_emId) . "'";
            $delete_url = "window.location='" . base_url('member/delete/' . $encry_emId) . "'";

            $id = "'" . $encry_emId . "'";
//            $action_html .= '<button
//                    onclick="approveMember(' . $id . ')"
//                    class="btn btn-sm default btn-xs"><i class="fa fa-edit"></i> Approve
//                </button>';

            $action_html .= '<button
                    onclick="' . $view_url . '"
                    class="btn btn-sm default btn-xs"><i class="fa fa-user"></i> View
                </button>';
            $action_html .= '<button
                    onclick="' . $delete_url . '"
                    class="btn btn-sm red  btn-xs"><i class="fa fa-trash"></i> Delete
                </button>';

            $records["data"][] = array(
                $row->id,
                $row->first_name,
                $row->email,
                $row->mobile,
                $action_html
            );
        }


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);

    }

    public function exportWaiting()
    {
        if ($this->session->has_userdata('waiting_member_filters')) {
            $_POST = $this->session->userdata('waiting_member_filters');
        }

        $sql = "SELECT * FROM member_dependencies WHERE status =0 AND user_type=1";

        $iTotalRecords = $this->db->query($sql)->num_rows();
        $iDisplayLength = intval($this->input->post('length'));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($this->input->post('start'));
        $sEcho = intval($this->input->post('draw'));


        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


        $filter_fullname = $this->input->post('name');
        $filter_username = $this->input->post('email');
        $filter_phone1 = $this->input->post('mobile');
        if ($filter_fullname) {
            $sql .= " AND first_name like '%" . $filter_fullname . "%'";
        }
        if ($filter_username) {
            $sql .= " AND email like '%" . $filter_username . "%'";
        }
        if ($filter_phone1) {
            $sql .= " AND mobile like '%" . $filter_phone1 . "%'";
        }

        $sort_array = array(
            'id',
            'first_name',
            'email',
            'mobile',
        );
        $order = $this->input->post('order');
        if ($order) {
            $column = $order[0]['column'];
            $dir = $order[0]['dir'];
            if (isset($sort_array[$column])) {
                $f = $sort_array[$column];

                if ($dir == 'asc')
                    $sql .= " ORDER BY $f ASC";
                else
                    $sql .= " ORDER BY $f DESC";
            }

        }

        $list = $this->db->query($sql);

        $report_title = 'Members Waiting For Approval';

        $headers = array(
            "id" => "    ID    ",
            "name" => "Name                   ",
            "email" => "Email       ",
            "phone" => "Phone              "
        );

        //top-border top-bottom outline double bold color fill

        $report_data = [];

        $c = 0;
        foreach ($list->result() as $row) {
            $row = (array)$row;
            $report_data[$c]['id'] = [
                'value' => $row['id'],
                'styles' => ['center']
            ];
            $report_data[$c]['name'] = [
                'value' => $row['first_name'] . " " . $row['last_name'],
                'styles' => []
            ];
            $report_data[$c]['email'] = [
                'value' => $row['email'],
                'styles' => []
            ];
            $report_data[$c]['phone'] = [
                'value' => $row['mobile'],
                'styles' => []
            ];
            $report_data[$c]['nationality'] = [
                'value' => $row['nationality'],
                'styles' => []
            ];
            $c++;
        }


        $filters = [];
        if (count($_POST) > 0) {
            foreach ($_POST as $key => $val) {
                if (in_array($key, ['id', 'name', 'email', 'mobile', 'nationality'])) {
                    $filters[$key] = $val;
                }
            }
        } else {
            $filters = ['x' => 'No records.'];
        }

        Excel::export_excel(
            $report_title,
            $headers,
            $report_data,
            $filters
        );

    }

    public function ajax_list()
    {
        $records = array();
        $records["data"] = array();

        if ($this->input->post()) {
            $this->session->set_userdata('approved_member_filters', $this->input->post());
        }

        $sql = "SELECT
                member_dependencies.id,
                member_dependencies.user_id,
                member_dependencies.member_id, 
                member_dependencies.title,
                member_dependencies.first_name,
                member_dependencies.last_name, 
                member_dependencies.dob, 
                member_dependencies.mobile, 
                member_dependencies.email,  
                member_dependencies.nationality, 
                member_dependencies.occupation,
                member_dependencies.`status`, 
                member_companies.company_name
                FROM
                member_dependencies
                LEFT JOIN members ON member_dependencies.user_id = members.id
                LEFT JOIN member_companies ON members.corporate_id = member_companies.id
                WHERE member_dependencies.status IN (1,2) AND member_dependencies.user_type=1";

        $iTotalRecords = $this->db->query($sql)->num_rows();
        $iDisplayLength = intval($this->input->post('length'));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($this->input->post('start'));
        $sEcho = intval($this->input->post('draw'));


        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $filter_id = $this->input->post('member_id');
        $filter_fullname = $this->input->post('name');
        $filter_username = $this->input->post('email');
        $filter_phone1 = $this->input->post('mobile');
        $filter_occupation = $this->input->post('occupation');
        $filter_nationality = $this->input->post('nationality');
        $filter_corporate_id = $this->input->post('corporate_id');
        $filter_month = $this->input->post('month');
        if ($filter_id) {
            $sql .= " AND member_dependencies.member_id like '%" . $filter_id . "%'";
        }
        if ($filter_fullname) {
            $sql .= " AND member_dependencies.first_name like '%" . $filter_fullname . "%'";
        }
        if ($filter_username) {
            $sql .= " AND member_dependencies.email like '%" . $filter_username . "%'";
        }
        if ($filter_corporate_id) {
            $sql .= " AND members.corporate_id = " . $filter_corporate_id . "";
        }
        if ($filter_occupation) {
            $sql .= " AND member_dependencies.occupation like '%" . $filter_occupation . "%'";
        }
        if ($filter_nationality) {
            $sql .= " AND member_dependencies.nationality like '%" . $filter_nationality . "%'";
        }
        if ($filter_month) {
            $sql .= " AND MONTH(member_dependencies.dob) =" . $filter_month . "";
        }
        $sort_array = array(
            'id',
            'user_id',
            'first_name',
            'company_name',
            'occupation',
            'email',
            'mobile',
            'nationality'
        );
        $order = $this->input->post('order');
        if ($order) {
            $column = $order[0]['column'];
            $dir = $order[0]['dir'];
            if (isset($sort_array[$column])) {
                $f = $sort_array[$column];

                if ($dir == 'asc')
                    $sql .= " ORDER BY $f ASC";
                else
                    $sql .= " ORDER BY $f DESC";
            }

        }


        $sql2 = $sql . " LIMIT " . $iDisplayStart . "," . $iDisplayLength;
        $list = $this->db->query($sql2);


        foreach ($list->result() as $row) {

            $encry_emId = $this->encrypt_lib->encode($row->user_id);
            $action_html = '';

            $view_url = "window.location='" . base_url('member/viewProfile/' . $encry_emId) . "'";
            $download_url = base_url('member/downloadPDF/' . $encry_emId);

            $action_html .= '<a
                    href="' . $download_url . '"
                    class="btn btn-sm default btn-xs"><i class="fa fa-download"></i> Download Certificate
                </a><br>';


            $action_html .= '<button
                    onclick="' . $view_url . '"
                    class="btn btn-sm default btn-xs"><i class="fa fa-user"></i> View
                </button><br>';

            $action_html .= '<a
                    href="' . base_url('member/editProfile/' . $encry_emId) . '"
                    class="btn btn-sm default btn-xs"><i class="fa fa-edit"></i> Edit
                </a><br>';

            $status = ($row->status == 2) ? base64_encode(1) : base64_encode(2);
            $action_html .= '<a
                    href="' . base_url('member/' . (($row->status == 2) ? "viewActivateMember" : "viewDeactivateMember") . '/' . $this->encrypt_lib->encode($row->id)) . '"
                     data-target="#ajax"
                   data-toggle="modal"  class="btn btn-sm ' . (($row->status == 2) ? "green" : "red") . ' btn-xs"><i class="fa fa-edit"></i> ' . (($row->status == 2) ? "Activate" : "Deactivate") . '
                </a>';

            $memberRenewalModel = MemberRenewalModel::where(['member_id' => $row->user_id, 'is_active' => 1])->first();
            $expireDate = isset($memberRenewalModel) ? $memberRenewalModel->end_date : "";

            $date1 = new DateTime(date('Y-m-d'));  //current date or any date
            $date2 = new DateTime($expireDate);   //Future date
            $diff = $date2->diff($date1)->format("%a");  //find difference
            $days = intval($diff);   //rounding days

            $expireDate = $expireDate . " <br>($days Days)";

            $status = '';
            $records["data"][] = array(
                $row->member_id,
                $row->title . " " . $row->first_name . " " . $row->last_name,
                $row->company_name,
                $row->occupation,
                $row->dob,
                $row->email,
                $row->mobile,
                $row->nationality,
                $expireDate,
                $action_html
            );
        }

        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);

    }

    public function exportApproved()
    {
        if ($this->session->has_userdata('approved_member_filters')) {
            $_POST = $this->session->userdata('approved_member_filters');
        }

        $sql = "SELECT
                member_dependencies.id,
                member_dependencies.user_id,
                member_dependencies.member_id, 
                member_dependencies.title,
                member_dependencies.first_name,
                member_dependencies.last_name, 
                member_dependencies.mobile, 
                member_dependencies.email,  
                member_dependencies.nationality, 
                member_dependencies.occupation,
                member_dependencies.`status`, 
                member_companies.company_name
                FROM
                member_dependencies
                LEFT JOIN members ON member_dependencies.user_id = members.id
                LEFT JOIN member_companies ON members.corporate_id = member_companies.id
                WHERE member_dependencies.status =1 AND member_dependencies.user_type=1";

        $filter_id = $this->input->post('member_id');
        $filter_fullname = $this->input->post('name');
        $filter_username = $this->input->post('email');
        $filter_phone1 = $this->input->post('mobile');
        $filter_occupation = $this->input->post('occupation');
        $filter_nationality = $this->input->post('nationality');
        $filter_corporate_id = $this->input->post('corporate_id');
        if ($filter_id) {
            $sql .= " AND member_dependencies.member_id like '%" . $filter_id . "%'";
        }
        if ($filter_fullname) {
            $sql .= " AND member_dependencies.first_name like '%" . $filter_fullname . "%'";
        }
        if ($filter_username) {
            $sql .= " AND member_dependencies.email like '%" . $filter_username . "%'";
        }
        if ($filter_corporate_id) {
            $sql .= " AND members.corporate_id = " . $filter_corporate_id . "";
        }
        if ($filter_occupation) {
            $sql .= " AND member_dependencies.occupation like '%" . $filter_occupation . "%'";
        }
        if ($filter_nationality) {
            $sql .= " AND member_dependencies.nationality like '%" . $filter_nationality . "%'";
        }

        $sort_array = array(
            'id',
            'user_id',
            'first_name',
            'company_name',
            'occupation',
            'email',
            'mobile',
            'nationality'
        );
        $order = $this->input->post('order');
        if ($order) {
            $column = $order[0]['column'];
            $dir = $order[0]['dir'];
            if (isset($sort_array[$column])) {
                $f = $sort_array[$column];

                if ($dir == 'asc')
                    $sql .= " ORDER BY $f ASC";
                else
                    $sql .= " ORDER BY $f DESC";
            }

        }

        $list = $this->db->query($sql);

        $report_title = 'Approved Members';

        $headers = array(
            "member_id" => "Member ID",
            "name" => "Name",
            "company" => "Company",
            "designation" => "Designation",
            "email" => "Email",
            "phone" => "Phone",
            "nationality" => "Nationality"
        );

        //top-border top-bottom outline double bold color fill

        $report_data = [];

        $c = 0;
        foreach ($list->result() as $row) {
            $row = (array)$row;
            $report_data[$c]['member_id'] = [
                'value' => $row['member_id'],
                'styles' => ['center']
            ];
            $report_data[$c]['name'] = [
                'value' => $row['first_name'] . " " . $row['last_name'],
                'styles' => []
            ];
            $report_data[$c]['company'] = [
                'value' => $row['company_name'],
                'styles' => []
            ];
            $report_data[$c]['designation'] = [
                'value' => $row['occupation'],
                'styles' => []
            ];
            $report_data[$c]['email'] = [
                'value' => $row['email'],
                'styles' => []
            ];
            $report_data[$c]['phone'] = [
                'value' => $row['mobile'],
                'styles' => []
            ];
            $report_data[$c]['nationality'] = [
                'value' => $row['nationality'],
                'styles' => []
            ];
            $c++;
        }

        $companies = CompanyModel::where("status", 1)->get();
        $companies_array = [];
        foreach ($companies as $raw) {
            $companies_array[$raw->id] = $raw->company_name;
        }


        $filters = [];
        if (count($_POST) > 0) {
            foreach ($_POST as $key => $val) {
                if (in_array($key, ['member_id', 'name', 'corporate_id', 'occupation', 'email', 'mobile', 'nationality'])) {
                    if ($key == 'corporate_id') {
                        $filters[$key] = $companies_array[$val];
                    } else {
                        $filters[$key] = $val;
                    }

                }
            }
        } else {
            $filters = ['x' => 'No records.'];
        }

        Excel::export_excel(
            $report_title,
            $headers,
            $report_data,
            $filters
        );

    }

    public function activate($activation_code)
    {
        if ($this->adminauth->activate($activation_code)) {
            $this->load->view('admin/activation_successful');
            return;
        }

        $this->load->view('admin/activation_failed');
    }

    public function memberRenew($id)
    {
        $id = $this->encrypt_lib->decode($id);
        $start_date = $this->input->post('start_date');
        $data = $this->session->userdata('loggedUser');

        $approved_by = $data->id;

        MemberRenewalModel::where(['member_id' => $id])->update(['is_active' => 0]);

        $oneYearOn = date('Y-m-d', strtotime(date("Y-m-d", strtotime($start_date)) . " + 365 day"));

        MemberRenewalModel::create([
            'member_id' => $id,
            "start_date" => date("Y-m-d", strtotime($start_date)),
            "end_date" => $oneYearOn, "is_active" => 1,
            'approved_by' => $approved_by
        ]);

    }

    public function getLog()
    {
        $file = "./log/logs.txt";
        echo $content = file_get_contents($file);
    }

    public function loadPreferences()
    {
        $id = $this->input->get('preference_id');
        $member_id = $this->input->get('member_id');

        $member_id = $this->encrypt_lib->decode($member_id);
        if (!$id) {
            echo "Please select an option";
            return;
        }
        $pageData = null;
        $pageData['item'] = MemberPreferenceItemsModel::where(['preference_id' => $id])->get();
        $pageData['item_member'] = new Collection(MemberPreferenceCategoryItemModel::where(['preference_id' => $id, 'member_id' => $member_id])->get());

        $this->load->view('admin/member/members_preferences', $pageData);
    }

    public function company()
    {
        $data['slug'] = 'company';
        $data['title'] = 'Manage Companies';
        $data['sub_page1'] = 'Manage Company';
        $id = $this->uri->segment(3, 0);
        $id = $this->encrypt_lib->decode($id);

        if ($this->input->post()) {


            $this->form_validation->set_rules('first_name', 'first_name', 'required');
            $this->form_validation->set_rules('last_name', 'last_name', 'required');
            $this->form_validation->set_rules('company_name', 'company_name', 'required');
            $this->form_validation->set_rules('company_address', 'company_address', 'required');
            $this->form_validation->set_rules('company_number', 'company_number', 'required');
            $this->form_validation->set_rules('allocated_pax', 'allocated_pax', 'required');
            $this->form_validation->set_rules('credit_limit', 'credit_limit', 'required');
            if ($this->form_validation->run() == false) {

                $this->session->set_flashdata('error', $this->lang->line('error_top'));

            } else {

                if ($id) {
                    $company = CompanyModel::findOrFail($id);
                    $company->title = $this->input->post('title');
                    $company->first_name = $this->input->post('first_name');
                    $company->last_name = $this->input->post('last_name');
                    $company->company_name = $this->input->post('company_name');
                    $company->company_address = $this->input->post('company_address');
                    $company->company_number = $this->input->post('company_number');
                    $company->office_number = $this->input->post('company_number');
                    $company->office_address = $this->input->post('company_address');
                    $company->company_description = $this->input->post('company_description');
                    $company->occupation = $this->input->post('occupation');
                    $company->allocated_pax = $this->input->post('allocated_pax');
                    $company->credit_limit = $this->input->post('credit_limit');
                    $company->mobile = $this->input->post('mobile');
                    $company->email = $this->input->post('email');
                    $company->save();
                    redirect('member/company');
                } else {
                    $res = CompanyModel::create([
                        "title" => $this->input->post('title'),
                        "first_name" => $this->input->post('first_name'),
                        "last_name" => $this->input->post('last_name'),
                        "company_name" => $this->input->post('company_name'),
                        "company_address" => $this->input->post('company_address'),
                        "company_number" => $this->input->post('company_number'),
                        "office_number" => $this->input->post('company_number'),
                        "office_address" => $this->input->post('company_address'),
                        "company_description" => $this->input->post('company_description'),
                        "occupation" => $this->input->post('occupation'),
                        "allocated_pax" => $this->input->post('allocated_pax'),
                        "credit_limit" => $this->input->post('credit_limit'),
                        "mobile" => $this->input->post('mobile'),
                        "email" => $this->input->post('email'),
                    ]);

                    $company = CompanyModel::findOrFail($res->id);
                    $company->company_code = "EL-" . (2000 + $res->id);
                    $company->save();
                    redirect('member/company');
                }
            }

        }

        $data['btn'] = "Edit";
        $companyData = CompanyModel::where('id', $id)->first();

        if (!isset($companyData)) {
            $data['btn'] = "Add";
            $categoryData = null;
        }

        $data['company'] = $companyData;
        $data['page_content'] = $this->load->view('admin/member/company', $data, true);
        $this->parser->parse('common/master_template', $data);

    }

    public function exportCompany()
    {
        if ($this->session->has_userdata('company_filters')) {
            $_POST = $this->session->userdata('company_filters');
        }

        $sql = "SELECT * FROM member_companies WHERE status =1";

        $iTotalRecords = $this->db->query($sql)->num_rows();
        $iDisplayLength = intval($this->input->post('length'));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($this->input->post('start'));
        $sEcho = intval($this->input->post('draw'));


        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $company_name = $this->input->post('company_name');
        if ($company_name) {
            $sql .= " AND company_name like '%" . $company_name . "%'";
        }
        $company_number = $this->input->post('company_number');
        if ($company_number) {
            $sql .= " AND company_number like '%" . $company_number . "%'";
        }
        $company_company_code = $this->input->post('company_code');
        if ($company_company_code) {
            $sql .= " AND company_code like '%" . $company_company_code . "%'";
        }
        $sort_array = array(
            'id',
            'company_code',
            'first_name',
            'last_name',
            'mobile',
            'company_name',
            'company_number',
        );
        $order = $this->input->post('order');
        if ($order) {
            $column = $order[0]['column'];
            $dir = $order[0]['dir'];
            if (isset($sort_array[$column])) {
                $f = $sort_array[$column];

                if ($dir == 'asc')
                    $sql .= " ORDER BY $f ASC";
                else
                    $sql .= " ORDER BY $f DESC";
            }

        }


        $list = $this->db->query($sql);

        $report_title = 'Member Companies';

        $headers = array(
            "company_code" => "Company Code",
            "company" => "Company",
            "telephone" => "Telephone",
            "name" => "Name",
            "designation" => "Designation"
        );

        //top-border top-bottom outline double bold color fill

        $report_data = [];

        $c = 0;
        foreach ($list->result() as $row) {
            $row = (array)$row;
            $report_data[$c]['company_code'] = [
                'value' => $row['company_code'],
                'styles' => ['center']
            ];
            $report_data[$c]['company'] = [
                'value' => $row['company_name'],
                'styles' => []
            ];
            $report_data[$c]['telephone'] = [
                'value' => $row['company_number'],
                'styles' => []
            ];
            $report_data[$c]['name'] = [
                'value' => $row['first_name'] . " " . $row['last_name'],
                'styles' => []
            ];
            $report_data[$c]['designation'] = [
                'value' => $row['occupation'],
                'styles' => []
            ];
            $c++;
        }


        $filters = [];
        if (count($_POST) > 0) {
            foreach ($_POST as $key => $val) {
                if (in_array($key, ['company_code', 'company_name', 'company_number', 'mobile', 'nationality'])) {
                    $filters[$key] = $val;
                }
            }
        } else {
            $filters = ['x' => 'No records.'];
        }

        Excel::export_excel(
            $report_title,
            $headers,
            $report_data,
            $filters
        );

    }

    public function companyDelete()
    {

        $id = $this->uri->segment(3, 0);
        $id = $this->encrypt_lib->decode($id);
        $category = CompanyModel::findOrFail($id);
        $category->status = 0;
        $category->save();
        redirect('member/company');
    }

    public function ajaxGetCompany()
    {
        $records = array();
        $records["data"] = array();

        if ($this->input->post()) {
            $this->session->set_userdata('company_filters', $this->input->post());
        }

        $sql = "SELECT * FROM member_companies WHERE status =1";

        $iTotalRecords = $this->db->query($sql)->num_rows();
        $iDisplayLength = intval($this->input->post('length'));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($this->input->post('start'));
        $sEcho = intval($this->input->post('draw'));


        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;

        $company_name = $this->input->post('company_name');
        if ($company_name) {
            $sql .= " AND company_name like '%" . $company_name . "%'";
        }
        $company_number = $this->input->post('company_number');
        if ($company_number) {
            $sql .= " AND company_number like '%" . $company_number . "%'";
        }
        $company_company_code = $this->input->post('company_code');
        if ($company_company_code) {
            $sql .= " AND company_code like '%" . $company_company_code . "%'";
        }
        $sort_array = array(
            'id',
            'company_code',
            'first_name',
            'last_name',
            'mobile',
            'company_name',
            'company_number',
        );
        $order = $this->input->post('order');
        if ($order) {
            $column = $order[0]['column'];
            $dir = $order[0]['dir'];
            if (isset($sort_array[$column])) {
                $f = $sort_array[$column];

                if ($dir == 'asc')
                    $sql .= " ORDER BY $f ASC";
                else
                    $sql .= " ORDER BY $f DESC";
            }

        }


        $sql2 = $sql . " LIMIT " . $iDisplayStart . "," . $iDisplayLength;
        $list = $this->db->query($sql2);


        foreach ($list->result() as $row) {

            $encry_emId = $this->encrypt_lib->encode($row->id);
            $action_html = '';

            $edit = base_url('member/company/' . $encry_emId);
            $delete = "'" . base_url('member/companyDelete/' . $encry_emId) . "'";

            $action_html .= '<a
                    href="' . $edit . '"
                    class="btn btn-sm default btn-xs"><i class="fa fa-edit"></i> Edit
                </a>';

            $action_html .= '<button
                    onclick="deleteCat(' . $delete . ')"
                    class="btn btn-sm default btn-xs"><i class="fa fa-user"></i> Delete
                </button>';

            $status = '';

            $records["data"][] = array(
                $row->company_code,
                $row->company_name,
                $row->company_number,
                $row->first_name . " " . $row->last_name,
                $row->occupation,
                $action_html
            );
        }


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);

    }

    public function activateMember($id)
    {
        $id = $this->encrypt_lib->decode($id);
        $this->session->set_flashdata('success', $this->lang->line('member_activated'));
        $this->changeMemberStatus($id, 1);
    }

    public function deactivateMember($id)
    {
        $id = $this->encrypt_lib->decode($id);
        $this->session->set_flashdata('success', $this->lang->line('member_deactivated'));
        $this->changeMemberStatus($id, 2);
    }

    private function changeMemberStatus($id, $status)
    {
        MemberChildModel::where([
            'id' => $id
        ])->update(['status' => $status]);
        redirect('member');
    }

    public function viewActivateMember($id)
    {
        $this->showStatusEditPage($id, 1);
    }

    public function viewDeactivateMember($id)
    {
        $this->showStatusEditPage($id, 2);
    }

    private function showStatusEditPage($id, $status)
    {
        echo $this->load->view('admin/member/confirm_box', [
            'id' => $id,
            'status' => $status
        ], true);
    }

    private function prepData($prefix = "", $mem_array)
    {
        return [
            'title' => isset($mem_array[$prefix . '_title']) ? $mem_array[$prefix . '_title'] : '',
            'first_name' => isset($mem_array[$prefix . '_fname']) ? ucwords($mem_array[$prefix . '_fname']) : '',
            'last_name' => isset($mem_array[$prefix . '_lname']) ? ucwords($mem_array[$prefix . '_lname']) : '',
            'email' => isset($mem_array[$prefix . '_email']) ? $mem_array[$prefix . '_email'] : '',
            'mobile' => isset($mem_array[$prefix . '_mobile']) ? $mem_array[$prefix . '_mobile'] : '',
            'office_number' => isset($mem_array[$prefix . '_office_no']) ? $mem_array[$prefix . '_office_no'] : '',
            'dob' => (isset($mem_array[$prefix . '_dob'])) ? $mem_array[$prefix . '_dob'] : null,
            'nationality' => isset($mem_array[$prefix . '_nationality']) ? $mem_array[$prefix . '_nationality'] : '',
            'home_address' => isset($mem_array[$prefix . '_home_adrs']) ? $mem_array[$prefix . '_home_adrs'] : '',
            'office_address' => isset($mem_array[$prefix . '_off_adrs']) ? $mem_array[$prefix . '_off_adrs'] : '',
            'occupation' => isset($mem_array[$prefix . '_designation']) ? $mem_array[$prefix . '_designation'] : ''
        ];
    }

    public function uploadImage($id)
    {
        $id = $this->encrypt_lib->decode($id);

        $get_image = base64_encode(file_get_contents($_FILES['file']['tmp_name']));
        MemberChildModel::where(["user_type" => 1, "user_id" => $id])->update(["authorize_profile" => $get_image]);
    }

    public function showReceipt()
    {
        $this->load->model('PosBanquetPayment');
        $posBanquetPayment = PosBanquetPayment::first();
        // $posBanquetPayment = js$posBanquetPayment->toArray();
        $getBanquetData['printData'] = $posBanquetPayment;
        $this->load->view('printer_layouts/pos_banquet_advance', $getBanquetData);
    }


    public function sendRequest($url, $body)
    {
        //$baseUrl = constant(WEB_URL);
        $baseUrl = "http://dev.appslanka.com/elevate-web/api/";
        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $baseUrl . $url,
            CURLOPT_RETURNTRANSFER => true,
            // CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $body,
            CURLOPT_HTTPHEADER => array(
                "API-KEY: kkssws0o0oc88448kco8sgk8kgsc884wsc448coc",
                "cache-control: no-cache",
                "content-type: multipart/form-data"
            )
        ));

        $response = curl_exec($curl);
        var_dump($response);
        // $info = curl_getinfo($ch);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        }
        return false;

    }

}
