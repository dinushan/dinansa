(function($j){
     $j.fn.extend({
          center: function (options) {
               var options =  $j.extend({ // Default values
                    inside:window, // element, center into window
                    transition: 0, // millisecond, transition time
                    minX:0, // pixel, minimum left element value
                    minY:0, // pixel, minimum top element value
                    withScrolling:true, // booleen, take care of the scrollbar (scrollTop)
                    vertical:true, // booleen, center vertical
                    horizontal:true // booleen, center horizontal
               }, options);
               return this.each(function() {
                    var props = {position:'absolute'};
                    if (options.vertical) {
                         var top = ($j(options.inside).height() - $j(this).outerHeight()) / 2;
                         if (options.withScrolling) top += $j(options.inside).scrollTop() || 0;
                         top = (top > options.minY ? top : options.minY);
                         $j.extend(props, {top: top+'px'});
                    }
                    if (options.horizontal) {
                          var left = ($j(options.inside).width() - $j(this).outerWidth()) / 2;
                          if (options.withScrolling) left += $j(options.inside).scrollLeft() || 0;
                          left = (left > options.minX ? left : options.minX);
                          $j.extend(props, {left: left+'px'});
                    }
                    if (options.transition > 0) $j(this).animate(props, options.transition);
                    else $j(this).css(props);
                    return $j(this);
               });
          }
     });
})(jQuery);
