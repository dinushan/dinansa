<!-- BEGIN CONTAINER -->

<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<?php if($page_action == ''):?>
			<h3 class="page-title">
				<?php 
			if($for_system == 1){
				echo 'Front User Pages';
			}else{
				echo 'Main Pages';
			}
			?>
			</h3>
			<?php echo get_message() ?>
			<div class="row">
				<div class="col-md-12">
					<div class="table-scrollable">
						<?php if($this->adminauth->has_role('201')):?>
						<table id="tbl" class="table table-striped table-bordered table-advance table-hover">
							<colgroup>
							<col width="45" />
							<col />
							<col width="300" />
							</colgroup>
							<thead>
								<tr class="headings nodrop nodrag">
									<th >ID</th>
									<th >Page</th>
									<th align="center">Action</th>
								</tr>
							</thead>
							<tbody>
								<?php 
									if($this->uri->segment(5) == ''):
									?>
								<tr>
									<td>0</td>
									<td><strong>HOME</strong></td>
									<td><a class="btn btn-sm default btn-xs" href="<?php echo site_url('admin/'.$slug.'/pages/edit_home_page/') ?>"><i class="fa fa-edit"></i> Edit</a> </td>
										</tr>
								<?php 	
									endif;
																	
									$edit_url = site_url('admin/'.$slug.'/pages/edit');
									$delete_url = site_url('admin/'.$slug.'/pages/delete');
									$page_banner_url = site_url('admin/'.$slug.'/pages/banner');
									
									
									foreach($site_pages as $_page):			
										$pg_id = $_page['pg_id'];		
										$has_banner = $_page['has_banner'];					
                                    	$encry_hcId = $this->encrypt_lib->encode($pg_id);
										
                                   ?>
								<tr id="<?php echo $encry_hcId ?>">
									<td class="dragHandle"><?php echo $_page['pg_id'] ?></td>
									<td><strong><?php echo $_page['page_name'] ?></strong></td>
									<td ><a class="btn btn-sm default btn-xs" href="<?php echo $edit_url.'/'.$encry_hcId ?>"><i class="fa fa-edit"></i> Edit</a>
										<?php if($has_banner):?>
										<a class="btn btn-sm default btn-xs" href="<?php echo site_url('admin/'.$slug.'/pages/banner/'.$encry_hcId) ?>"> <i class="fa fa-photo"></i> Banner(<?php echo $this->query->count_all_results('site_page_banner_images',array('pg_id'=>$pg_id)) ?>)</a>
										<?php endif; ?>
										</td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						<?php endif; ?>
					</div>
				</div>
			</div>
			<script type="text/javascript">	
				jQuery(document).ready(function(e) {
                    jQuery('#tbl').tableDnD({
                        onDragStart:function(table){
                            jQuery(table).find('tr').removeClass('even');
                        },
                        onDrop: function(table, row) {
                            var order = jQuery(table).tableDnDSerialize();
                            jQuery.post("<?php echo site_url('admin/site_content/pages/sorting') ?>", order, function(theResponse){

                            });
                            jQuery('#tbl1 tbody tr:even').addClass('even');
                        },
                        dragHandle:'.dragHandle'
                    });
                });

            </script>
			<?php elseif($page_action == 'edit' || $page_action == 'add_sub'): ?>
			<?php 
			$fu='';
			if($siteData->for_system == 1){
				$pg_name = 'Front User Page ';
				$fu='/1';
			}else{
				$pg_name = 'Main Page ';
				
			}
			?>
			<h3 class="page-title"> <?php echo $pg_name ?> "
				<?php 										
			if($page_action == 'add_sub'){
				echo $siteParentData->page_name;
				echo ' : Add Page';
			}else{
				echo $siteData->page_name;	
			}
			 ?>
				"</h3>
			<div class="page-bar">				
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<a class="btn default" href="<?php echo site_url('admin/site_content/pages/index'.$fu) ?>"><i class="fa fa-chevron-left"></i> Back to
							<?php 
							if($siteData->for_system == 1){
								echo 'Front User Pages';
							}else{
								echo 'Main Pages';
							}
							?>
						</a>
					</div>
				</div>
			</div>
			<div id="messages"><?php echo get_message() ?></div>
			<div class="row">
				<div class="col-md-12">
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet light bordered"> 
						<!--<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-pencil"></i>Settings
							</div>							
						</div>-->
						<div class="portlet-body form"> 
							<!-- BEGIN FORM-->
							<?php                    
						$attr = array("id"=>"edit_form","class"=>"form-horizontal","enctype"=>"multipart/form-data");
						echo form_open(current_url(),$attr);
						?>
							<?php 
                        
                        if($page_action == 'add_sub'){	
                            $slug = "";
                            $for_system = 0;
							
							$title = '';	
							$title2 = '';
							$menu = '';	
							$description = '';
							$short_description = '';
							$meta_title = '';
							$meta_keywords = '';
							$meta_description = '';
							$og_type = '';
							$og_title = '';
							$og_description = '';
							
							$map_lat = '';
							$map_lon = '';
							$phone = '';
							$email = '';
							
                        }else
						{
									
                            $slug = $siteData->slug;
                            $for_system = $siteData->for_system;
							
							$menu = $siteData->menu;
																
							$title1 = $siteData->title1;
							$title2 = $siteData->title2;
										
							$description1 = $siteData->description1;
							$description2 = $siteData->description2;
							
							$url_key = $siteData->url_key;	
							$meta_title = $siteData->meta_title;	
							$meta_keywords = $siteData->meta_keywords;	
							$meta_description = $siteData->meta_description;	
							$og_type = $siteData->og_type;	
							$og_title = $siteData->og_title;	
							$og_description = $siteData->og_description;
							$custom_header_script = $siteData->custom_header_script;
							
							$map_lat = $siteData->map_lat;
							$map_lon = $siteData->map_lon;
							$phone = $siteData->phone;
							$email = $siteData->email;

							$marketing_phone_number = $siteData->marketing_phone_number;
							$marketing_email = $siteData->marketing_email;
							$theater_phone_number = $siteData->theater_phone_number;
							$theater_email = $siteData->theater_email;
							
							$required_entry = ' required-entry';
                        }						
					  	
						?>
							<?php if($this->adminauth->has_role('201')):?>
							<h4 class="form-section">General</h4>
							<div class="form-group">
								<label class="control-label col-md-3">Main Navigation Caption<span class="req">*</span></label>
								<div class="control-element col-md-4">
									<input name="menu" type="text" class="form-control req-entry <?php echo $required_entry ?>" value="<?php echo $page_action == "add" ? 
							set_value('menu') : 
							set_value('menu',$menu) ?>" size="15" />
									<?php echo form_error('menu'); ?> </div>
							</div>
								<div class="form-group">
									<label class="control-label col-md-3">Title<span class="req">*</span></label>
									<div class="control-element col-md-4">
										<input name="title1" type="text" class="form-control req-entry <?php echo $required_entry ?>" value="<?php echo $page_action == "add" ?
											set_value('title1') :
											set_value('title1',$title1) ?>" size="15" />
										<?php echo form_error('title1'); ?> </div>
								</div>

								<?php
								if($pg_id != '4') {
									?>
									<div class="form-group">
										<label class="control-label col-md-3">URL key<span class="req">*</span></label>
										<div class="control-element col-md-4">
											<input name="url_key" type="text"
												   class="form-control req-entry <?php echo $required_entry ?>"
												   value="<?php echo $page_action == "add" ?
													   set_value('url_key') :
													   set_value('url_key', $url_key) ?>" size="15"/>
											<?php echo form_error('url_key'); ?>
											<div class="help-block font-green">"Note: If the site is LIVE changing the
												URL may harm your ranking on search engines"
											</div>
										</div>
									</div>
									<?php

								}
							endif;//end role
							?>

								<?php
								if($pg_id == '7') {
								?>
									<h4 class="form-section">Marketing and advertising (Contact Details)</h4>
									<div class="form-group">
										<label class="control-label col-md-3">Phone Number<span class="req">*</span></label>
										<div class="control-element col-md-4">
											<input name="marketing_phone_number" type="text" class="form-control req-entry <?php echo $required_entry ?>" value="<?php echo $page_action == "add" ?
												set_value('marketing_phone_number') :
												set_value('marketing_phone_number',$marketing_phone_number) ?>" size="15" />
											<?php echo form_error('marketing_phone_number'); ?> </div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Email<span class="req">*</span></label>
										<div class="control-element col-md-4">
											<input name="marketing_email" type="text" class="form-control req-entry <?php echo $required_entry ?>" value="<?php echo $page_action == "add" ?
												set_value('marketing_email') :
												set_value('marketing_email',$marketing_email) ?>" size="15" />
											<?php echo form_error('marketing_email'); ?> </div>
									</div>

									<h4 class="form-section">Theater (Contact Details)</h4>
									<div class="form-group">
										<label class="control-label col-md-3">Phone Number<span class="req">*</span></label>
										<div class="control-element col-md-4">
											<input name="theater_phone_number" type="text" class="form-control req-entry <?php echo $required_entry ?>" value="<?php echo $page_action == "add" ?
												set_value('theater_phone_number') :
												set_value('theater_phone_number',$theater_phone_number) ?>" size="15" />
											<?php echo form_error('theater_phone_number'); ?> </div>
									</div>
									<div class="form-group">
										<label class="control-label col-md-3">Email<span class="req">*</span></label>
										<div class="control-element col-md-4">
											<input name="theater_email" type="text" class="form-control req-entry <?php echo $required_entry ?>" value="<?php echo $page_action == "add" ?
												set_value('theater_email') :
												set_value('theater_email',$theater_email) ?>" size="15" />
											<?php echo form_error('theater_email'); ?> </div>
									</div>

								<?php
									}
								?>

							
							<h4 class="form-section">Meta Settings</h4>
							<div class="form-group">
								<label class="control-label col-md-3">Meta Title</label>
								<div class="control-element col-md-4">
									<input type="text" class="form-control" value="<?php echo 
								$page_action == "add" ? 
								set_value('meta_title') : 
								set_value('meta_title',$meta_title) ?>" 
								name="meta_title" />
								</div>
							</div>
							<?php if(SHOW_META_CONTENT === TRUE):?>
							<div class="form-group">
								<label class="control-label col-md-3">Meta Keywords</label>
								<div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="meta_keywords"><?php echo $page_action == "add" ? 
								set_value('meta_keywords') : 
								set_value('meta_keywords',$meta_keywords) ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Meta Description</label>
								<div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="meta_description"><?php echo $page_action == "add" ? 
								set_value('meta_description') : 
								set_value('meta_description',$meta_description) ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">OG Type</label>
								<div class="control-element col-md-4">
									<input type="text" class="form-control" value="<?php echo 
								$page_action == "add" ? 
								set_value('og_type') : 
								set_value('og_type',$og_type) ?>" 
								name="og_type" />
									<span class="help-block font-green">The type of the object, such as 'article'</span></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">OG Title</label>
								<div class="control-element col-md-4">
									<input type="text" class="form-control" value="<?php echo 
								$page_action == "add" ? 
								set_value('og_title') : 
								set_value('og_title',$og_title) ?>" 
								name="og_title" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">OG Description</label>
								<div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="og_description"><?php echo $page_action == "add" ? 
								set_value('og_description') : 
								set_value('og_description',$og_description) ?></textarea>
								</div>
							</div>
							<?php endif;?>
							<div class="form-group">
								<label class="col-md-3 control-label">Custom Header Script(SEO)</label>
								<div class="col-md-4 col-xs-12">
                                               <textarea cols="15" rows="2" class="form-control" name="custom_header_script"><?php echo $page_action == "add" ?
													   set_value('custom_header_script') :
													   set_value('custom_header_script',$custom_header_script) ?></textarea>
								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
									</div>
								</div>
							</div>
							<script type="text/javascript">							 
								$('.required-entry').attr('required','required');					
								$('#edit_form').validate({
									errorElement: 'span', 
									errorClass: 'validation-advice', 								
									ignore: "",
									submitHandler: function (form) {
										form.submit();										
									}
								});									
							</script>
							</form>
						</div>
					</div>
				</div>
			</div>			
			<?php elseif($page_action == 'banner'): ?>
			<?php 
				$fu='';
				if($siteData->for_system == 1){
					$pg_name = 'Front User Page ';
					$fu='/1';
				}else{
					$pg_name = 'Main Page ';											
				}
				if($pg_id == -1){
					//$img_h = HOME_PG_ADVERTISEMENT_IMG_W;
					$banner_title = 'Upload Banner Images';
				}else{
					//$img_h = HOME_PG_ADVERTISEMENT_IMG_H;
					$banner_title = 'Upload Banner Images';
				}
				?>
				
			<h3 class="page-title"><?php echo $siteData->title1 ?> : <?php echo $banner_title; ?></h3>
			<div class="page-bar">				
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<a class="btn default" href="<?php echo site_url('admin/site_content/pages/index'.$fu) ?>"><i class="fa fa-chevron-left"></i> Back to
							<?php 
							if($siteData->for_system == 1){
								echo 'Front User Pages';
							}else{
								echo 'Main Pages';
							}
							?>
						</a>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12"> <?php echo get_message() ?> 
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet light bordered"> 
						<div class="portlet-body">
							<table width="100%" border="0" cellspacing="0" cellpadding="0">
								<tr>
									<td><form>
											<input id="file_upload" name="file_upload" type="file" multiple>
										</form>
										<?php
										if($pg_id == -1) {
											$img_w = HOME_BANNER_IMG_W;
											$img_h = HOME_BANNER_IMG_H;
										}
										else
										{
											$img_w = PG_IMG_W;
											$img_h = PG_IMG_H;
										}
										?>
										<span class="help-block red">Image Size:<?php echo $img_w ?>px x <?php echo $img_h ?>px, Image Type:jpg</span>
										<div id="queue"></div></td>
								</tr>
							</table>
						 
						 	<script type="text/javascript">
								<?php $timestamp = time();?>
									$sel_numImg = 0;
									$com_numImg = 0;
									jQuery('#file_upload').uploadify({
										'formData'     : {
											'user_id'     : '<?php echo $this->encrypt_lib->encode($this->adminauth->user_id) ?>',					
											'token'     : '<?php echo md5(142);?>',
										},							
										'buttonText' : 'Browse Images',										
										'queueID'  : 'queue',
										'fileTypeDesc' : 'jpg Files',
										'fileTypeExts' : '*.jpg', 
										'debug'	: false,
										'swf'      : '<?php echo base_url() ?>skin_admin/global/plugins/uploadify/uploadify.swf',
										'uploader' : '<?php echo site_url('admin/'.$slug.'/pages/picture_upload/'.$enc_pg_id); ?>',
										'onSelect' : function(file) {
											$sel_numImg++;								
										},
										'onUploadSuccess' : function(file, data, response) {
											$com_numImg++;								
											if($sel_numImg == $com_numImg ){
												window.location.reload();
											}								
										} 
									});
								
							</script>
							<div class="grid">
								<div id="thumbContent">
									<div style="height:600px;overflow:scroll; position:relative; padding:0;margin: 0;">
										<ul id="list">
											<?php
                                    $i = 1;
                                    foreach($imageList as $row):
                                        $id = $row['id'];
                                        $image = $row['image'];										
										$title = $row['title'];
										$caption = $row['caption'];
										$alt = $row['alt'];
										$position = $row['position'];
										$popup_imgtitle = $row['popup_imgtitle'];
										
                                    ?>
											<li id="recordsArray_<?php echo $id ?>">
												<div class="handle"><img src="<?php echo base_url().IMG_PATH_GRID.$image ?>" width="118" /></div>
												<div class="del"></div>
												<?php
												if($pg_id == -1){
													?>
													<div class="edit"
														 data-title="<?php echo $title ?>"
														 data-caption="<?php echo $caption ?>"
														 data-alt="<?php echo $alt ?>"
														 data-position="<?php echo $position ?>"
														 data-imgtitle="<?php echo $popup_imgtitle ?>"
													></div>
													<?php
												}
												?>
												<div class="number"><?php echo $i ?></div>
											</li>

											<?php
                                        $i++;
                                  endforeach; 
                              ?>
										</ul>
									</div>
								</div>
							</div>
							<script type="text/javascript">
					
								jQuery("#list").sortable({ opacity: 0.6, cursor: 'move',handle : '.handle', update: function() {
									var order = jQuery(this).sortable("serialize") + '&action=updateRecordsListings'; 
									jQuery.post("<?php echo site_url('admin/'.$slug.'/pages/sort_images/'.$enc_pg_id) ?>", order, function(theResponse){		
										jQuery('#thumbContent').find('li').each(function(i){
												jQuery(this).find('.number').html(i+1);
										});		
									});								 
								}});
								
								 
							   jQuery('#list .del').click(function(event){
								   var _self = jQuery(this);
								   var _id = _self.parent().attr('id');
									message = 'Are you sure you want to delete?';
									jConfirm('Are you sure you want to delete?','Confirmation',function(r){
										if( r == true ) {
											jQuery('#'+_id).remove();	
											jQuery.post("<?php echo site_url('admin/'.$slug.'/pages/delete_images/') ?>", {'id':_id}, function(theResponse){	});
											jQuery('#thumbContent').find('li').each(function(i){
													jQuery(this).find('.number').html(i+1);
											});
										}
									});
									
								});
								
								jQuery('#list .edit').click(function(event){
							var _self = jQuery(this);
							jHomeBannerCaption(_self.attr('data-title'),
											   _self.attr('data-caption')
											   ,_self.attr('data-alt')
											   ,_self.attr('data-position')
											   ,_self.attr('data-imgtitle')
											   , 'Image Info'
											   , function(value1,value2,value3,value4,value5){

								if(value1 != "" || value2 != "" || value3 != "" || value4 != "" || value5 != ""){
									jQuery.post("<?php echo site_url('admin/'.$slug.'/pages/image_caption/') ?>", 
									{'id':_self.parent().attr('id'),'title':value1,'caption':value2,'alt':value3,'position':value4,'imgtitle':value5}, function(theResponse){});
									_self.attr('data-title',value1);
									_self.attr('data-caption',value2);
									_self.attr('data-alt',value3);
									_self.attr('data-position',value4);
									_self.attr('data-imgtitle',value5);
								}
								jQuery.alerts._hide();
							});
						});
							
							</script>
						 
						</div>
					</div>
				</div>
			</div>	
			<?php endif;//$page_action ?>
		</div>
		<!-- END PAGE CONTENT--> 
	</div>
	<!-- END CONTENT --> 
	
</div>
<!-- END CONTAINER --> 

