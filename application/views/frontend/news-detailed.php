<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>



<div class="container pageContainer detailPageContainer newsPage newsDetailPage">
   <div class="row">
   <div class="col-xs-12 headWrap padding-zero-mobile">
			<h2 class="heading"><?php echo $news_details->title; ?></h2>

			<div class="newsDate"><?php echo $news_details->published_date; ?></div>
		</div>
  
        
            <div class="col-xs-12 col-md-8 pageBody">
           <div class="row">
                  <div class="col-xs-12 padding-zero-mobile">
                     <div class="newsSliderWrapper camera_wrap">
                         <?php
                         if($news_details->image1 != '')
                         {
                             echo '<div data-src="'.base_url(NEWS_IMG_PATH.$news_details->image1).'"></div>';
                         }

                         if($news_details->image2 != '')
                         {
                             echo '<div data-src="'.base_url(NEWS_IMG_PATH.$news_details->image2).'"></div>';
                         }

                         if($news_details->image3 != '')
                         {
                             echo '<div data-src="'.base_url(NEWS_IMG_PATH.$news_details->image3).'"></div>';
                         }

                         if($news_details->image4 != '')
                         {
                             echo '<div data-src="'.base_url(NEWS_IMG_PATH.$news_details->image4).'"></div>';
                         }
                         ?>
                     </div>
                  </div>
    
                  <div class="col-xs-12 breadcrumbCont">
                      <div class="breadcrumbWrap">
                          <ol class="breadcrumb">
                              <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                              <li><a href="<?php echo $sitePageData->url_key; ?>"><?php echo $sitePageData->menu; ?></a></li>
                              <li class="active"><a href=""><?php echo $news_details->title; ?></a></li>
                          </ol>
                      </div>
                      <div class="socialWrap">
                          <ul class="social-links">
                              <li><a href="#" class="facebook"></a></li>
                              <li><a href="#" class="twitter"></a></li>
                              <li><a href="#" class="pinterest"></a></li>
                              <li><a href="#" class="google"></a></li>
                              <li><a href="#" class="instragram"></a></li>
                          </ul>
                      </div>
                  </div>
                 <!--  <div class="col-xs-12 headWrap">
                     <h2 class="heading">Storyline</h2>
                  </div> -->
                  <div class="col-xs-12 detailWrap">
                     <div class="newsDetail">
                         <?php echo $news_details->description; ?>

                     </div>
                  </div>
       
              
             </div>
            </div>
            <div class="col-xs-12 col-md-4 pageSide">
        	 	<div class="postNavWrap recentPostWrap">
		            <div class="postHeading">Resent Posts</div>
		            <ul class="navList list-group">
                        <?php
                        foreach ($recent_post as $data) {
                            $title = $data['title'];
                            $url_key = $data['url_key'];
                            ?>
                            <li class="list-group-item"><a class="link" href="<?php echo base_url($url_key); ?>"><?php echo $title; ?></a></li>
                            <?php
                        }
                        ?>
		            </ul>
		        </div>
		        <div class="postNavWrap postByMonthWrap">
		            <div class="postHeading">Posts by month</div>
		            <ul class="navList list-group">
                        <?php
                        foreach ($post_by_month as $data) {
                            $published_date = $data['published_date'];
                            $url_key = $data['url_key'];

                            $timestamp = strtotime($published_date);
                            $newDate = date('F Y', $timestamp);
                            $function_month = date("Y-m", $timestamp);

                            ?>
                            <li class="list-group-item"><a onclick="getMonth('<?php echo $function_month; ?>')" class="link"><?php echo $newDate;  ?></a></li>
                            <?php
                        }
                        ?>
		            </ul>
		        </div>
            </div>
         
   </div>
</div>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>


<script>
   $(function(){
      $('.newsSliderWrapper').camera({
         navigation:false,
         playPause:false,
         time:10000,
         transPeriod: 2000,
         pauseOnClick: true,
         loader:'none',
         height:'400px'
      });
  });
      </script>