<?php include(APPPATH . 'views/frontend/inc/head.php'); ?>
<?php include(APPPATH . 'views/frontend/inc/header.php'); ?>

<div class="container pageContainer detailPageContainer">
    <div class="row">
        <div class="col-xs-12 thetresCont">

            <div class="col-xs-12 col-sm-7 col-md-8 padding-zero-mobile padding-zero-tab">
                <div class="row">
                    <div class="col-xs-12 padding-zero-mobile">
                        <div class="movieSliderWrapper camera_wrap">
                            <?php foreach ($theaterImages as $row): ?>
                                <div data-src="<?php echo base_url(THEATER_IMG_PATH . $row['image']) ?>"></div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                    <div class="col-xs-12 visible-xs">
                        <div class="theatresLogoWrap">
                            <img class="theatresLogo img-responsive" src="<?php echo $theater_details['logo']; ?>"
                                 alt="<?php echo $theater_details['theater_name']; ?>"/>
                        </div>
                        <div class="theatreDetailWrap">
                            <div class="theatreSecWrap theatreAddressWrap">
                                <address class="address">
                                    <h5 class="title"><?php echo $theater_details['theater_name']; ?></h5>
                                    <div class="desc"><?php echo $theater_details['address']; ?></div>
                                </address>
                            </div>
                            <div class="theatreSecWrap theatreContactWrap">
                                <div class="hotline">
                                    <h5 class="title">Hotline</h5>
                                    <a href="tel:<?php echo $theater_details['hotline']; ?>"><?php echo $theater_details['hotline']; ?></a>
                                </div>

                            </div>
                            <div class="theatreSecWrap theatreEmailWrap">
                                <div class="email">
                                    <a href="mailto:<?php echo $theater_details['email']; ?>"><?php echo $theater_details['email']; ?></a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12">
                            <div class="advertisementWrap">
                                <?php
                                foreach ($advertisement_images as $add) {
                                    $image = $add['web_view_image'];
                                    ?>
                                    <img class="advertisement"
                                         src="<?php echo base_url(ADVERTISEMENT_IMG_PATH . $image); ?>"/>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12">
                        <div class="breadcrumbCont clearfix">
                            <div class="breadcrumbWrap">
                                <ol class="breadcrumb">
                                    <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                                    <li>
                                        <a href="<?php echo $sitePageData->url_key; ?>"><?php echo $sitePageData->menu; ?></a>
                                    </li>
                                    <li class="active"><?php echo $theater_details['theater_name']; ?></li>
                                </ol>
                            </div>
                       
                        <div class="socialWrap">
                            <ul class="social-links">
                                <li><a href="<?php echo $theater_details['fb_link']; ?>" class="facebook"></a></li>
                                <li><a href="<?php echo $theater_details['twitter_link']; ?>" class="twitter"></a></li>
                                <li><a href="<?php echo $theater_details['pinterest_link']; ?>" class="pinterest"></a></li>
                                <li><a href="<?php echo $theater_details['google_plus_link']; ?>" class="google"></a></li>
                                <li><a href="<?php echo $theater_details['instagram_link']; ?>" class="instragram"></a></li>
                            </ul>
                        </div>
                    </div>
                     </div>
                    <div class="col-xs-12 headWrap">
                        <h2 class="heading secHeading">About <?php echo $theater_details['theater_name']; ?></h2>
                    </div>
                    <div class="col-xs-12 detailWrap">
                        <div class="storyDetail">
                            <p><?php echo $theater_details['description']; ?></p>
                        </div>
                    </div>
                    <div class="col-xs-12">
                    <div class="amenitiesWrap clearfix">
                        <h2 class="amenitiesTitle">Amenities available</h2>
                        <div class="row amenitiesRow">
                            <?php
                            foreach ($theater_details['facility'] as $fac) {
                                ?>
                                <div class="col-xs-6 col-sm-4 amenities"><span class="amenitiesIcon"><img
                                            class="img-responsive icon"
                                            src="<?php echo $fac['facility_icons']; ?>"
                                            alt=""></span><span
                                        class="amenitiesText"><?php echo $fac['facility_name']; ?></span></div>
                                <?php
                            }
                            ?>
                        </div>
                        </div>
                    </div>
                    <div class="col-xs-12 mapWrap">
                        <h2 class="mapTitle">Get Directions</h2>
                        <div id="map" class="thatreMap"></div>
                    </div>
                </div>
            </div>
            <div class="col-xs-12 col-sm-5 col-md-4 padding-zero-mobile">

                <div class="hidden-xs">
                    <div class="theatresLogoWrap">
                        <img class="theatresLogo img-responsive" src="<?php echo $theater_details['logo']; ?>"
                             alt="<?php echo $theater_details['theater_name']; ?>"/>
                    </div>
                    <div class="theatreDetailWrap">
                        <div class="theatreSecWrap theatreAddressWrap">
                            <address class="address">
                                <h5 class="title"><?php echo $theater_details['theater_name']; ?></h5>
                                <div class="desc"><?php echo $theater_details['address']; ?></div>
                            </address>
                        </div>
                        <div class="theatreSecWrap theatreContactWrap">
                            <div class="hotline">
                                <h5 class="title">Hotline</h5>
                                <a href="tel:<?php echo $theater_details['hotline']; ?>"><?php echo $theater_details['hotline']; ?></a>
                            </div>

                        </div>
                        <div class="theatreSecWrap theatreEmailWrap">
                            <div class="email">
                                <a href="mailto:<?php echo $theater_details['email']; ?>"><?php echo $theater_details['email']; ?></a>
                            </div>
                        </div>
                    </div>
                   
                        <div class="advertisementWrap">
                            <?php
                            foreach ($advertisement_images as $add) {
                                $image = $add['web_view_image'];
                                ?>
                                <img class="advertisement"
                                     src="<?php echo base_url(ADVERTISEMENT_IMG_PATH . $image); ?>"/>
                                <?php
                            }
                            ?>
                        </div>
              

                </div>
            </div>

            <?php
            if(count($nowShowing) > 0) {
                ?>
                <div class="col-xs-12 otherMoviesAtTheatre padding-zero-mobile padding-zero-tab">
                    <div class="row">
                        <div class="col-xs-12 headWrap">
                            <h3 class="heading otherMoviesTitle">Now showing at <?php echo $theater_details['theater_name']; ?></h3>
                        </div>


                        <?php
                        foreach ($nowShowing as $time) {
                            ?>
                            <div id="<?php echo 'div-' . $theater_details['theater_id'] . '-' . $time['movie_id']; ?>"
                                 class="col-xs-12 col-md-6 movieSecWrap">
                                <div class="movieSec moviesCont">
                                    <div class="moviesWrap data-image"
                                         data-image-src="<?php echo $time['portrait_image'] ?>">
                                        <div class="imageOverlay"></div>
                                        <div class="captionWidgetWrap">
                                            <div class="captionTitleWrap">
                                                <h3 class="captionTitle"><?php echo $time['movie_name']; ?></h3>
                                            </div>
                                            <div class="releaseDate">In cinemas <?php
                                                $timestamp = strtotime($time['date_release']);
                                                $newDate = date('d S F', $timestamp);
                                                echo $newDate; ?></div>
                                            <div class="iconContainer clearfix">
                                                <div class="iconWrapper">
                                                    <ul class="iconWrap">
                                                        <li>
                                                            <a href="<?php echo $time['trailer']; ?>"
                                                               class="youTubePopUp"> <i class="icon fa fa-youtube-play"
                                                                                        aria-hidden="true"></i><span
                                                                    class="icoText">Trailer</span></a></li>
                                                        <li>
                                                            <a href="<?php echo base_url('movie/' . $time['movie_url_key']); ?>"><i
                                                                    class="icon fa fa-info"
                                                                    aria-hidden="true"></i><span
                                                                    class="icoText">Info</span></a></li>

                                                    </ul>

                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="bookingSec">
                                    <h3 class="title"> Now Showing</h3>
                                    <div class="secTitle">Available Showtimes</div>
                                    <!--div class="selectDateWrap calendarWrap">
                                        <input type="text"
                                               data-theatre-id="<?php echo $theater_details['theater_id']; ?>"
                                               data-movie-id="<?php echo $time['movie_id']; ?>"
                                               class="form-control input-scope selectMovieDate"
                                               value="<?php echo date('d F Y'); ?>"/>
                                        <span class="icon calendar-icon"></span>
                                    </div-->
                                    <div class="detailBody">
                                        <div class="detailRow">


                                            <table class="detailTable">
                                                <tbody>
                                                    <tr>
                                                <?php
                                                $enc_theater_id = $this->encrypt_lib->encode($theater_details['theater_id']);
                                                foreach ($time['show_time_slots'] as $key => $slots):
                                                    if ($key != 0 && $key % 2 == 0){
                                                        echo '</tr><tr>';
                                                    }
                                                    ?>
                                                        <td>
                                                            <div class="detailSpan">
                                                                <span class="time"><?php echo $slots['time_slot']; ?></span>
                                                            </div>
                                                        </td>
                                                        <!--td>
                                                            <div class="detailSpan"><a onclick="return getTheaterBookButton()" href="<?php

                                                                $mv_id = $this->encrypt_lib->encode($time['movie_id']);
                                                                echo base_url('buy-tickets-online/?movie='.$mv_id.'&theater='.$enc_theater_id) ?>" class="btn btn-scope">BOOK</a>
                                                            </div>
                                                        </td-->

                                                    <?php
                                                endforeach;
                                                ?>
                                                <?php if ( count($time['show_time_slots']) >1 && count($time['show_time_slots']) % 2 != 0 ): ?>
                                                        <td>
                                                            <div class="detailSpan">
                                                                <span class="time">&nbsp;</span>
                                                            </div>
                                                        </td>
                                                <tr/>
                                                <?php endif; ?>
                                                </tbody>
                                                <tfoot>
                                                    <tr>
                                                        <td>
                                                            <div class="detailSpan"><a onclick="return getTheaterBookButton()" href="<?php

                                                                $mv_id = $this->encrypt_lib->encode($time['movie_id']);
                                                                echo base_url('buy-tickets-online/?movie='.$mv_id.'&theater='.$enc_theater_id) ?>" class="btn btn-scope">BOOK</a>
                                                            </div>
                                                        </td>
                                                    </tr>
                                                </tfoot>
                                            </table>

                                        </div>

                                    </div>
                                </div>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                </div>
                <?php
            }
            ?>

        </div>
    </div>

</div>

<?php
if(count($upcoming) > 0) {
?>
<div class="pageBottomSection">
    <div class="pageBottomContainer upComingMoviesAtTheatre container">
        <div class="row">
            <div class="col-xs-12 padding-zero-mobile padding-zero-tab">
                <div class="col-xs-12 headWrap">
                    <h3 class="heading otherMoviesTitle">Up Coming movies at <?php echo $theater_details['theater_name']; ?></h3>
                </div>

                <?php
                foreach ($upcoming as $up) {
                    ?>
                    <div class="col-xs-12 col-sm-6 col-lg-3 moviesCont comingMoviesCont">
                        <div class="moviesWrap">
                            <picture class="img-responsive coverImage">
                                <source srcset="<?php echo $up['portrait_image'] ?>" media="(min-width: 800px)">
                                <img srcset="<?php echo $up['portrait_image'] ?>" alt="…">
                            </picture>
                            <a href="#" class="imageOverlay"></a>
                            <div class="captionWidgetWrap">
                                <div class="captionTitleWrap">
                                    <h3 class="captionTitle"><?php echo $up['movie_name']; ?></h3>
                                </div>
                                <div class="releaseDate">In cinemas <?php
                                    $timestamp = strtotime($up['date_release']);
                                    $newDate = date('d S F', $timestamp);
                                    echo $newDate;  ?></div>
                                <div class="iconContainer clearfix">
                                    <div class="iconWrapper">
                                        <ul class="iconWrap">
                                            <li><a href="<?php echo $up['trailer'] ?>"
                                                   class="youTubePopUp"><i
                                                        class="icon fa fa-youtube-play" aria-hidden="true"></i><span
                                                        class="icoText">Trailer</span></a></li>
                                            <li><a href="<?php echo base_url('movie/'.$up['movie_url_key']); ?>"><i class="icon fa fa-info" aria-hidden="true"></i><span
                                                        class="icoText">Info</span></a></li>

                                        </ul>

                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                ?>

            </div>
        </div>
    </div>
</div>
    <?php
}
?>

<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAjM9-C9ZBJNPg1QZSwEv30028UCEgQ0P8"></script>
<script type="text/javascript">
    var lat = <?php echo $theater_details['location']['latitude']; ?>;
    var lng = <?php echo $theater_details['location']['longitude']; ?>;
</script>
<?php include(APPPATH . 'views/frontend/inc/footer.php'); ?>

