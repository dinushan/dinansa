<?php 
    ob_start();
    $PageContent = ob_get_contents();
    ob_end_clean();
    $HashID = md5($PageContent);
    $LastChangeTime = 1144055759;
    $ExpireTime = 3600; // seconds (= one hour)
    //$headers = apache_request_headers();
    header('Content-Type: text/html');
    header('Content-language: en');
    header('Cache-Control: max-age=' . $ExpireTime); // must-revalidate
    header('Expires: '.gmdate('D, d M Y H:i:s', time()+$ExpireTime).' GMT');
    header('Last-Modified: '.gmdate('D, d M Y H:i:s', $LastChangeTime).' GMT');
    header('ETag: ' . $HashID);


    if($this->mobile_detect->isMobile()){
        $class_body .= ' mobile';
    }

    ?>
    <?php include(APPPATH.'views/frontend/inc/inc-ses.php');?>
    <!DOCTYPE html>
    <html lang="en" ng-app="cinema">
    <head>
        <meta charset="utf-8">
        <title><?php if($meta_data['meta_title'] != "")
            {
                echo $meta_data['meta_title'];
            }
            else
            {
                $meta_data_default = $this->query->query_row("SELECT meta_title from site_settings WHERE conf_id = 1");
                echo $meta_data_default->meta_title;
            }
            ?></title>

        <link href="<?php echo base_url() ?>skin/css/plugins.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>skin/css/font-awesome.min.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>skin/css/magnific-popup.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>skin/css/melon.datepicker.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>skin/css/animations.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>skin/css/main.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>skin/css/slick.css" rel="stylesheet">
        <link href="<?php echo base_url() ?>skin/css/slick-theme.css" rel="stylesheet">

        <script src="<?php echo base_url() ?>skin/js/plugins.js"></script>
        <script src="<?php echo base_url() ?>skin/js/jquery.magnific-popup.js"></script>
        <script src="<?php echo base_url() ?>skin/js/picturefill.min.js"></script>
        <script src="<?php echo base_url() ?>skin/js/slick.min.js"></script>


        <!--Validation js-->
        <script type='text/javascript' src='<?php echo base_url()?>/js/validationengine/languages/jquery.validationEngine-en.js'></script>
        <script type='text/javascript' src='<?php echo base_url()?>/js/validationengine/jquery.validationEngine.js'></script>
        <script type='text/javascript' src='<?php echo base_url()?>/js/validationengine/jquery.validate.js'></script>
        <?php
        if($this->session->userdata('force_param') == "desktop"): ?>
            <meta name="viewport" content="width=1000, initial-scale=1">
        <?php elseif($this->session->userdata('force_param') == '' || $this->session->userdata('force_param') == "mobile"): ?>
            <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
        <?php endif;?>


        <?php
        //on index file robots
        $seg = $this->uri->segment(2);
        if($this->userauth->logged_in() && ($main_nav_slug == 'buy_ticket_online') && isset($_GET["movie"]))
        {
            echo '<meta name="robots" content="noindex">';
        }
        elseif ($this->userauth->logged_in() && ($this->uri->segment(2) == 'show-movie-details'))
        {
            echo '<meta name="robots" content="noindex">';
        }
        elseif ($this->userauth->logged_in() && ($main_nav_slug == 'buy_ticket_online') && isset($seg))
        {
            echo '<meta name="robots" content="noindex">';
        }

        $meta_keywords = $meta_data['meta_keywords'];
        $meta_description = $meta_data['meta_description'];

        $og_title = $meta_data['og_title'];
        $og_description = $meta_data['og_description'];
        $og_image = base_url()."images/Og-image-Logo.png";//$meta_data['og_image'];
        $og_type = $meta_data['og_type'];


        echo '<meta name="twitter:card" content="summary">';
        echo '<meta name="twitter:title" content="'.$meta_data['meta_title'].'">';
        if($meta_description != ""){
            echo '<meta name="twitter:description" content="'.$meta_description.'">';
        }
        echo '<meta name="twitter:creator" content="@scopecinema.com">';
        echo '<meta name="twitter:image" content="'.$og_image.'">';
        echo '<meta name="twitter:domain" content="scopecinema.com">';


        if($meta_keywords != ""){
            echo '<meta name="keywords" content="'.$meta_keywords.'" />'."\n";
        }
        if($meta_description != ""){
            echo '<meta name="description" content="'.$meta_description.'" />'."\n";
        }
        if($og_title != ""){
            echo '<meta property="og:title" content="'.$og_title.'" />'."\n";
        }
        if($og_type != ""){
            echo '<meta property="og:type" content="'.$og_type.'" />'."\n";
        }
        if($og_description != ""){
            echo '<meta property="og:description" content="'.$og_description.'" />'."\n";
        }
        ?>
        <!--<meta property="og:type" content="website" />-->
        <meta property="og:site_name" content="<?php echo $setting_data['site_name'] ?>" />
        <meta property="og:url" content="<?php echo base_url() ?>" />
        <meta property="og:image" content="<?php echo $og_image ?>" />
        <meta property="og:image:width" content="1200" />
        <meta property="og:image:height" content="630" />
        <meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />




        <link rel="shortcut icon" href="<?php echo base_url()?>images/favicon.png" type="image/x-icon">
        <link rel="icon" href="<?php echo base_url()?>images/favicon.png" type="image/x-icon">
        <script type="text/javascript">
            var base_url = '<?php echo base_url() ?>';
			window.socket_url = '<?php echo $this->config->item('socket_url')?>';
            var news_url = '<?php echo base_url('news/') ?>';
            var movie_times_url = '<?php echo base_url('movie-times') ?>';
            var csrf_value = '<?php echo $this->security->get_csrf_hash(); ?>';
        </script>
        <script src="<?php echo base_url() ?>skin/js/main.js"></script>

        <?php
        if($main_nav_slug == 'home')
        {
            echo $siteData->custom_header_script;
        }
        else
        {
            echo $sitePageData->custom_header_script;
        }
        ?>

        <?php if($this->settingData['google_analytics_code'] != ''):?>
        <script>
            (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
                    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
                m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
            })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

            ga('create', '<?php echo $this->settingData['google_analytics_code'] ?>', 'auto');
            ga('send', 'pageview');
        </script><?php endif; ?>
		
		<!-- Facebook Pixel Code -->
		<script>
			!function(f,b,e,v,n,t,s){if(f.fbq)return;n=f.fbq=function(){n.callMethod?
				n.callMethod.apply(n,arguments):n.queue.push(arguments)};if(!f._fbq)f._fbq=n;
				n.push=n;n.loaded=!0;n.version='2.0';n.queue=[];t=b.createElement(e);t.async=!0;
				t.src=v;s=b.getElementsByTagName(e)[0];s.parentNode.insertBefore(t,s)}(window,
					document,'script','https://connect.facebook.net/en_US/fbevents.js');
			fbq('init', '1814557978868326', {
				em: 'insert_email_variable,'
			});
			fbq('track', 'PageView');
		</script>
		<noscript><img height="1" width="1" style="display:none"
					   src="https://www.facebook.com/tr?id=1814557978868326&ev=PageView&noscript=1"
				/></noscript>
		<script>
			fbq('track', 'Purchase', {
				value: 247.35,
				currency: 'USD'
			});
		</script>
		<!-- DO NOT MODIFY -->
		<!-- End Facebook Pixel Code -->

    </head>
        