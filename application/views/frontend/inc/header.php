<body class="<?php echo $class_body ?>">

<noscript>
    <div class="alert alert-error">JavaScript is off. Please enable to view full site.</div>
</noscript>

<nav class="navbar navbar-fixed-top navbar-top"> <!-- navbar-fixed-top -->
    <div class="container-fluid">
        <noscript>
            <div id="noscript-warning">Scope Cinema works best with JavaScript enabled</div>
        </noscript>
        <div class="navbar-header">
            <button type="button" class="navbar-toggle">
                <span class="sr-only">Toggle navigation</span>
                <i class="fa fa-bars" aria-hidden="true"></i>
            </button>
            <a class="navbar-brand" href="<?php echo base_url() ?>">
                <img src="<?php echo base_url() ?>skin/images/scope-cinemas-logo.png" alt="">
            </a>
        </div>
        <div class="navOverlay"></div>
        <div id="navbar" class="navbar-collapse">
            
            <?php echo $this->site_config->get_main_navigation($main_nav_slug); ?>
            <ul class="nav navbar-nav navbar-right">
                <?php
                if ($this->userauth->logged_in()): ?>
                    <li class="dashboard <?php if($main_nav_slug == 'dashboard'){echo 'active';} ?>">
                    <a class="userLogedText" href="<?php echo site_url('user/my-bookings') ?>" title="MY BOOKINGS">My Bookings </a>
                    <a class="userLogedIcon dashIcon" href="<?php echo site_url('user/my-bookings') ?>" title="MY BOOKINGS">
                        <span class="icon"></span>
                     </a>
                    </li>
                    <li class="my_profile <?php if($main_nav_slug == 'my_profile'){echo 'active';} ?>">
                    <a class="userLogedText" href="<?php echo site_url('user/profile') ?>" title="My Profile">My Profile</a>
                    <a class="userLogedIcon profileIcon" href="<?php echo site_url('user/profile') ?>" title="My Profile"><span class="icon"></span></a>
                    </li>
                    <li class="li-nav-logout">
                    <a class="userLogedText" href="<?php echo site_url('user/logout') ?>" title="Logout">Logout</a>
                    <a class="userLogedIcon logoutIcon" href="<?php echo site_url('user/logout') ?>" title="Logout"><span class="icon"></span></a>
                    </li>
                <?php else: ?>
                    <li class="li-nav-login login"><a href="<?php echo site_url('user/login') ?>" title="Login">Login</a></li>
                <?php endif; ?>

            <?php echo $this->site_config->get_login_navigation($main_nav_slug); ?>
            </ul>

            
        </div>

    </div>
    <div class="hoverFill"></div>
</nav>
<div class="datePickerBackground"></div>
<div class="pageLoaderCont">
    <div class="loaderCont">
       <!--  <div class="loaderWrap">
            <div class="cssload-thecube">
                <div class="cssload-cube cssload-c1"></div>
                <div class="cssload-cube cssload-c2"></div>
                <div class="cssload-cube cssload-c4"></div>
                <div class="cssload-cube cssload-c3"></div>
            </div>
        </div> -->
        <div class="cssloadWrap">
            <div class="cssload-loader"></div>
        </div>
        
    </div>
</div>

<div class="appStoreWrap">
    <a href="#" class="closeAppstore">
        <span class="closer">✕</span>
    </a>
    <a href="https://www.scopecinemas.com/download-app" class="navigate-to-app">
        <div class="brand-info">
            <div class="brand-image">
                <img class="image" src="<?php echo base_url() ?>skin/images/scope-cinemas-logo.png"/>
            </div>
            <div class="app-desc">
            <span class="brand-text">Scope</span>
            <img class="image" src="<?php echo base_url() ?>skin/images/app-ratings.png"/>
            <br/>
            <span class="brand-download-info">Free - on Google Play</span>
            <div class="btn-free">FREE</div>
            </div>
        </div>
    </a>
</div>

