/*
Navicat MySQL Data Transfer

Source Server         : My Local
Source Server Version : 50625
Source Host           : localhost:3306
Source Database       : hms_elevate

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2018-01-17 21:57:33
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_restaurant_admin_userdata
-- ----------------------------
DROP TABLE IF EXISTS `auth_restaurant_admin_userdata`;
CREATE TABLE `auth_restaurant_admin_userdata` (
  `userdata_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `emp_id` varchar(50) DEFAULT NULL,
  `fullname` varchar(40) NOT NULL DEFAULT '',
  `nic` varchar(50) DEFAULT NULL,
  `email` varchar(254) NOT NULL DEFAULT '',
  `phone1` varchar(20) DEFAULT NULL,
  `phone2` varchar(20) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `department` text,
  `zone` text,
  PRIMARY KEY (`userdata_id`),
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
