<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer">
	<div class="row">
		<div class="col-xs-12 breadcrumbCont">
            <div class="breadcrumbWrap">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                    <li><a href="<?php echo base_url('user/register'); ?>">Registration</a></li>
                </ol>
            </div>
		</div>
		<div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 registerPwdCont">
            <div id="message_signUp"><?php echo get_message(); ?></div>
            <?php
            $attributes = array('id' => 'frm-signup', 'class'=>'form-type-1');
            echo form_open(getCurrentUrl(), $attributes);
            ?>


            <section class="section registerPwdWrap">

                    <div class="headingWrap">
                        <h2 class="title">Set a password to your account?</h2>
                    </div>
                    <div class="formWrap">
                        <div class="form-group">
                            <label>Enter a Password</label>
                            <input type="password" class="form-control input-scope" name="password" value="<?php echo set_value('password'); ?>">
                            <?php echo form_error('password'); ?>
                        </div>
                        <div class="form-group">
                            <label>Re-Confirm Password</label>
                            <input type="password" class="form-control input-scope" name="conf-password" value="">
                            <?php echo form_error('conf-password'); ?>
                        </div>
                        <div class="form-group">
                            <input type="hidden" name="s" value="<?php echo $this->session->session_id ?>">
                            <input type="hidden" name="action" value="signUp" />
                            <button type="submit" class="btn btn-scope btn-blue btn-sign-up">Sign Up</button>
                        </div>

                    </div>


            </section>


            </form>

		</div>
	</div>
</div>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>