<?php //include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php //include(APPPATH.'views/frontend/inc/header.php'); ?>

<!-- BEGIN CONTAINER -->

<div class="container pageContainer portlet">
	<div class="row">
		<div class="col-xs-12 breadcrumbCont">
            <div class="breadcrumbWrap">

            </div>
		</div>
		<div class="col-xs-12 buyTicketProcessWrap">
			<ul class="buyTicketProcess">
				<li  class="current" id="booking_select_tab">
					<span class="text">Summary of Booking</span>
				</li>

			</ul>
		</div>


		<div class="col-xs-12 ticketSection summerBookingSection" style="" id="booking_select_tab_div">
			<div class="row">
				<div class="col-xs-12">
					<div class="headingWrap">
						<h3 class="title">Booking Summary</h3>
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
				<div class="bookingDetails">
					<div class="remaingTime">Please note that you have 200 secs to complete the booking</div>
					<table class="bookingDetailsTable">
						<tbody>
							<tr>
								<td><span class="title">Movie Name</span></td>
								<td>Grate wall 2017</td>
							</tr>
							<tr>
								<td><span class="title">Theatre Name</span></td>
								<td>Majestic Cinema</td>
							</tr>
							<tr>
								<td><span class="title">Show Date</span></td>
								<td>17 march 2017</td>
							</tr>
							<tr>
								<td><span class="title">Show Time</span></td>
								<td>2.30 pm</td>
							</tr>
							<tr>
								<td class="valign-top"><span class="title">Seat Reserved</span></td>
								<td class="bookingSummeryTableTd">
								<div class="bookingSummeryTableWrap">
									<table class="bookingSummeryTable">
							            <thead>
							               <tr>
							                  <th>Block</th>
							                  <th>Seat</th>
							                  <th>Price (Rs)</th>
							               </tr>
							            </thead>
							            <tbody>
							               <tr>
							                  <td>ODC</td>
							                  <td>F7</td>
							                  <td>1000</td>
							               </tr>
							                <tr>
							                  <td>ODC</td>
							                  <td>F7</td>
							                  <td>1000</td>
							               </tr>
							                <tr>
							                  <td>ODC</td>
							                  <td>F7</td>
							                  <td>1000</td>
							               </tr>
							                <tr>
							                  <td>ODC</td>
							                  <td>F7</td>
							                  <td>1000</td>
							               </tr>
							               <tr class="totalRow">
							                  <td>Total</td>
							                  <td></td>
							                  <td>5000</td>
							                  
							               </tr>
							            </tbody>
							         </table>
							         </div>
								</td>
							</tr>
							<tr>
								<td><span class="title">Total Number of Tickets</span></td>
								<td>4 (Four)</td>
							</tr>
							<tr>
								<td><span class="title">Coupon Code</span></td>
								<td>
									<input type="text" name="coupon" class="couponCode input-scope" placeholder="Enter coupen code" />
								</td>
							</tr>
							<tr>
								<td></td>
								<td>
									<div class="termsWrap">
							
									     <!-- .roundedOne -->
									    <div class="roundedOne termsConditionWrap">
									      <input type="checkbox" value="None" id="roundedOne" class="termsCondition" name="check"  />
									      <label for="roundedOne"></label>
									    </div>
									    <!-- end .roundedOne -->
										<div class="textWrap">I agree to </div>&nbsp; <div class="textWrap"> <a href="#" class="link"> Terms & Condition</a></div>
									</div>

								</td>
							</tr>
						</tbody>
					</table>
					</div>
				</div>
				<div class="col-xs-12 col-md-6">
				<div class="userDetails">
					<table class="userDetailsTable">
						<tbody>
							<tr>
								<td><span class="title">Your Name</span></td>
								<td>Layout index</td>
							</tr>
							<tr>
								<td><span class="title">Phone Number</span></td>
								<td>+94 112 345 915</td>
							</tr>
							<tr>
								<td><span class="title">Email Address</span></td>
								<td>hello@layoutindex.com</td>
							</tr>
							<tr>
								<td><span class="title">Date of Birth</span></td>
								<td>13.04.1980</td>
							</tr>
							<tr>
								<td><span class="title">Address</span></td>
								<td>136/3C, Temple road, Nawala</td>
							</tr>
							<tr>
								<td><span class="title">City</span></td>
								<td>Colombo</td>
							</tr>
						</tbody>
					</table>
					</div>
				</div>

			</div>
		</div>




	</div>
</div>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>
