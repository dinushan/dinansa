

/*-----------------------------------*/

var colombo = new google.maps.LatLng(6.9199641, 79.9025541);
var defaultPin = new google.maps.LatLng(6.904934, 79.909904);
var marker;
var map;

function initialize() {
	var mapOptions = {
		zoom: 13,
		center: colombo
	};

	map = new google.maps.Map(document.getElementById('map-canvas'),
		  mapOptions);

	marker = new google.maps.Marker({
		map:map,
		draggable:true,
		position: colombo
	});
	
	google.maps.event.addListener(marker, 'dragend', function (event) {
		document.getElementById("_h_lat").value = this.getPosition().lat();
		document.getElementById("_h_lon").value = this.getPosition().lng();
	});
	google.maps.event.addListener(map, 'click', function(event) {
	   placeMarker(event.latLng);
	});
}

function placeMarker(location) {
   marker.setPosition(location);
   document.getElementById("_h_lat").value = location.lat();
   document.getElementById("_h_lon").value = location.lng();
}



/*-----------------------------------*/
