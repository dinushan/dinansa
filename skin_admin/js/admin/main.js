﻿var map;
var markers = [];
var markerCluster;
var marker_pin = base_url+"skin/images/pin.png";
var marker_pin_selected = base_url+"skin/images/pin_selected.png";
var marker_pin_airport = base_url+"skin/images/pin_airport.png";

function setLocation(url){
    window.location.href = url;
}

function createEditor(ele){
	jEditor(ele);
}

function cancelConfirm(message,title, url,ispost,ele) {
 	jConfirm( message, title,function(result){
		if(result){
			if(ispost == true){				
				jQuery.post(url,function(data){
									 
				});
			}else{
				setLocation(url);
			}
		}
	});
}
function redirectConfirm(message,title, url) {
 	jConfirm( message, title,function(result){
		if(result){			
			setLocation(url);
		}
	});
}

function setPasswordForm(v){
	if(v){
		$('.password_fieldset').show();
		$('#changepass').val('PqVXhyuWUyay06Lw4');
	}else{
		$('#changepass').val('');
		$('.password_fieldset').hide();
	}
}

function deleteConfirm(message,title, url,ispost,ele,param) {
 	jConfirm( message, title,function(result){
		if(result){
			if(ispost == true){
				jQuery(ele).fadeOut('slow');
				if(typeof(param) != 'undefined'){
					jQuery.post(url,param,function(data){
										 
					});
				}else{
					jQuery.post(url,function(data){
										 
					});
				}
			}else{
				setLocation(url);
			}
		}
	});
}




function parseDate(str,format) {
	if(typeof(format) == 'undefined'){// dd/mm/yyyy
    	var mdy = str.split('/')
    	return new Date(mdy[0], mdy[1]-1,mdy[2]);
		
	}else if(format == 'dd-mm-yyyy'){
		var mdy = str.split('-')
    	return new Date(mdy[2], mdy[1]-1,mdy[0]);
	}
}

function daydiff(first, second) {
    return (second-first)/(1000*60*60*24)
}

function init_menu(){
	/* Menu slide down and hide */
	$('.navigaion-holder .main-menu li:has(ul)').addClass('submenu');
	
	$('.navigaion-holder .main-menu .submenu li').on('mouseenter', 'a', function () {
		$(this).parent().parent().prev().addClass('hover');
	}).on('mouseleave', 'a', function () {
		$(this).parent().parent().prev().removeClass('hover');
	});	
}

function getYoutubeId(url){
	 if(url === null){ return ""; }

	  var vid;
	  var results;
	
	  results = url.match("[\\?&]v=([^&#]*)");
	
	  vid = ( results === null ) ? url : results[1];
	  
	  return vid;
}

function getScreen( url, size )
{
 
	var vid = getYoutubeId(url);	
    return "http://img.youtube.com/vi/"+vid+"/"+size+".jpg";

}


function embedYoutubeVideo(ele,url,desc){
	var vid = getYoutubeId(url);
	
	var embed_html = '<object width="100%" height="474"><param name="movie" value="http://www.youtube.com/v/'+vid+'?fs=1"></param><param name="allowFullScreen" value="true"></param><param name="allowScriptAccess" value="always"></param><embed src="http://www.youtube.com/v/'+vid+'?fs=1&rel=0" type="application/x-shockwave-flash" allowfullscreen="true" allowScriptAccess="always" width="100%" height="474"></embed></object>';

	$(ele+' .video_holder').html(embed_html);
	$(ele+' .video_wrapper .video_description').html(desc);
}

function set_select2(){
	var cb_s2_placeholder = '--Select--';
	if(jQuery('.cb_s2').has('placeholder')){
		cb_s2_placeholder = jQuery('.cb_s2').attr('placeholder');
	}
	jQuery('.cb_s2').select2({ placeholder: cb_s2_placeholder});
	
	
}
function close_popup(){
	jQuery.modal.close();	
	//$('.popup_wrapper').hide();
	window.location.hash = '';
	
	$('#frmLogin input[name="ac"]').val('');
	$('#frmLogin input[name="param"]').val('');
	$('#frmSignup input[name="ac"]').val('');
	$('#frmSignup input[name="param"]').val('');
	
	$('#popup_tm_day_plan #tm_day_plan_panel').html('');
	
}

function set_popup_tm_day_plan_size(){
	var wh = $(window).height();
	
	$('#popup_tm_day_plan').height(wh);	
}
$(window).resize(function () {
	set_popup_tm_day_plan_size();
});

function popup_tm_day_plan(url){
		
	close_popup();	
	$('#popup_tm_day_plan').modal({stageHeight:true
								  ,autoResize: true
								  ,'close':false
								  ,'maxWidth':'950px'
								  ,'position':['0px']});
	$('.popup_wrapper .validation-advice').remove();
	
	
	
	$.post(url,function(_html){
		$('#popup_tm_day_plan #tm_day_plan_panel').html(_html);	
		
		$('#tm_day_plan_panel .btn-exit').unbind('click').bind('click',function(e){
			e.preventDefault();
			close_popup();
		})
		
		$('.btn_confirm_pay').unbind('click').bind('click',function(e){
			var h_param = $('#h_param').val();
			var url = base_url+lng_code+"/pay/"+h_param;
			$('.btn_confirm_pay').attr("disabled","disabled");
			setLocation(url);	
		})
	});
	
	set_popup_tm_day_plan_size();
}


function exit_signup(){
	close_popup();	
}
function forgot_password(){
	close_popup();	
	$('#forgot_password').modal();
	$('.popup_wrapper .validation-advice').remove();
	
	$('#forgot_password_panel #loginBtn').unbind('click').bind('click',function(e){
		e.preventDefault();
		login();	
	})
	
	$("#frmFP").validate({
		rules: {
			fp_email: {
				required: true,
				email: true
			}
		},
		submitHandler: function() {
			$.post($("#frmFP").attr('action'),$("#frmFP").serialize(),
			function(jsonData){

				if(jsonData.status == 'error'){					
					$("#forgot_password_panel").find('.form_massages').html('<div class="error-msg">'+jsonData.message+'</div>');
				}else{					
					$("#forgot_password_panel").find('.form_massages').html('<div class="sccues-msg">'+jsonData.message+'</div>');
				}
			},'json');
		}
	});
}
function signup(){
	$('.frmSignup_wrapper').show();
	$('.signup_success_wrapper').hide();
				
	close_popup();	
	$('#popup_signup').modal();
	$('.popup_wrapper .validation-advice').remove();
	
	$('#signup_panel .btn-exit').unbind('click').bind('click',function(e){
		e.preventDefault();
		close_popup();
	})
	
	$("#frmSignup").validate({
		errorPlacement: function(error, element) {			
			if (element.attr("name") == "accept_terms_condition" ) {
				error.insertAfter("#lbl_signup_accept_terms_condition");
			} else {
				error.insertAfter(element);
			}
		},
		submitHandler: function() {
			$.post($("#frmSignup").attr('action'),$("#frmSignup").serialize(),
			function(jsonData){

				if(jsonData.status == 'error'){					
					$("#signup_panel").find('.form_massages').html('<div class="error-msg">'+jsonData.message+'</div>');
				}else{
					//jAlert(jsonData.message,jsonData.title);
					$('.frmSignup_wrapper').hide();
					$('.signup_success_wrapper').show();
					$('.signup_success_wrapper').html(jsonData.message);
					$("#signup_panel").find('.form_massages').html('');
					/*if(jsonData.action == 'sdf'){
						jQuery.modal.close();	
						jAlert(jsonData.message,jsonData.title);
					}
					else if(jsonData.action == 'url'){
						window.location = jsonData.redirect;	
					}
					else{
						window.location.reload();	
					}*/
				}
			},'json');
		}
	});
}


function travel_agent_signup(title){
		
	close_popup();	
	$('#popup_travel_agent_signup').modal();
	$('.popup_wrapper .validation-advice').remove();
	
	$('#popup_travel_agent_signup .title').html(title+' Sign up');
	if(title == 'Travel Agent'){
		$('#popup_travel_agent_signup #name_trv_agency').attr('placeholder','Name of the travel agency');
		$('#popup_travel_agent_signup #h_tp').val(1);
	}else{
		$('#popup_travel_agent_signup #name_trv_agency').attr('placeholder','Name of the local agency');
		$('#popup_travel_agent_signup #h_tp').val(2);
	}
	
	$('#travel_agent_signup_panel .btn-exit').unbind('click').bind('click',function(e){
		e.preventDefault();
		close_popup();
	})
	
	$("#frmTASignup").validate({
		submitHandler: function() {
			$.post($("#frmTASignup").attr('action'),$("#frmTASignup").serialize(),
			function(jsonData){

				if(jsonData.status == 'error'){					
					$("#travel_agent_signup_panel").find('.form_massages').html('<div class="error-msg">'+jsonData.message+'</div>');
				}else{
					window.location				
				}
			},'json');
		}
	});
}
function travel_agent_forgot_password(){
	close_popup();	
	$('#popup_travel_agent_forgot_password').modal();
	$('.popup_wrapper .validation-advice').remove();
	
	$('#supplier_forgot_password #loginBtn').unbind('click').bind('click',function(e){
		e.preventDefault();
		login();	
	})
	
	$("#frmTAFP").validate({
		rules: {
			fp_email: {
				required: true,
				email: true
			}
		},
		submitHandler: function() {
			$.post($("#frmTAFP").attr('action'),$("#frmTAFP").serialize(),
			function(jsonData){

				if(jsonData.status == 'error'){					
					$("#supplier_forgot_password").find('.form_massages').html('<div class="error-msg">'+jsonData.message+'</div>');
				}else{					
					$("#supplier_forgot_password").find('.form_massages').html('<div class="sccues-msg">'+jsonData.message+'</div>');
				}
			},'json');
		}
	});
}


function IsEmail(email) {
	var regex = /^([a-zA-Z0-9_\.\-\+])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$/;
	if(!regex.test(email)) {
	   return false;
	}else{
	   return true;
	}
}


function login(){	
	
	
	$('.popup_wrapper .validation-advice').remove();
	
	$('#frmLogin .btn-login').unbind('click').bind('click',function(e){
			//e.preventDefault();
		   var login_email = $('#login_email').val();
           var login_password = $('#login_password').val();
		  
		   var hasError = false;
		   
		   $('#frmLogin .validation-advice').remove();
		   if(login_email== ''){   		   		
			   $('<div for="login_email" class="validation-advice">Please enter a valid email address</div>').insertAfter('#login_email');
               hasError = true;
            }else if(IsEmail(login_email)==false){
                 $('<div for="login_email" class="validation-advice">Please enter a valid email address</div>').insertAfter('#login_email');
                 hasError = true;
            }
			
			 if(login_password.length < 6 ){ 			 
			 	$('<div for="login_password" class="validation-advice">Please enter at least 6 characters.</div>').insertAfter('#login_password');
                 hasError = true;
			 }
			 
			 if(!hasError){
				$.post($("#frmLogin").attr('action'),$("#frmLogin").serialize(),
				function(jsonData){
	
					if(jsonData.status == 'error'){					
						$("#login_panel").find('.form_massages').html('<div class="error-msg">'+jsonData.message+'</div>');
						jAlert(jsonData.message,'Error');
					}else{
						if(jsonData.action == 'sdf'){
							jQuery.modal.close();	
							jAlert(jsonData.message,jsonData.title);
						}
						else if(jsonData.action == 'url'){
							window.location = jsonData.redirect;	
						}
						else{
							window.location.reload();	
						}
					}
				},'json'); 
			 }
	
	})
}

function site_seach(){
	var strSearch = $.trim($('#frmSiteSearch input[name="s"]').val());
		
	if(strSearch != ''){
		$('#frmSiteSearch').submit();
	}	
}

$(document).ready(function () {
	init_menu();	
	set_select2();
	
	$('ul.fl').find('li:first').addClass('first');
	$('ul.fl').find('li:last').addClass('last');
	
	$('ul.oe').find('li:odd').addClass('even');
	$('ul.oe').find('li:even').addClass('odd');
	
	$('table.data-table thead').find('tr:first').addClass('first');
	$('table.data-table thead').find('tr:last').addClass('last');
	
	$('table.data-table tbody').find('tr:first').addClass('first');
	$('table.data-table tbody').find('tr:last').addClass('last');
	
	$('table.data-table tbody').find('tr:even').addClass('even');
	
	$('table.fl:first').find('tr:first').addClass('first');
	$('table.fl:first').find('tr:last').addClass('last');
	$('table.oe:first').find('tr:even').addClass('even');
	$('table.oe:first').find('tr:odd').addClass('odd');
	
	if(window.location.hash == "#login"){
		//login();
		$('#customer_login ul').height(315);
	}
	/*$('.nav-login a').click(function(e){
		e.preventDefault();
		login();	
	})*/
	
	$('#ul_customer_login .overlay_top_box').click(function(e){
		$('#login_email').focus();
		$('.overlay_customer_login').show();
	})
	
	$('.overlay_customer_login').click(function(e){
		$('.overlay_customer_login').hide();
	})
	
	login();
	$('.customer_login_pop .forgot_pw a, #_frmLogin .fp').click(function(e){
		e.preventDefault();
		$('.overlay_customer_login').hide();
		forgot_password();	
	})
	
	$('.customer_login_pop #btn-signup, #_frmLogin #btn-signup').click(function(e){
		e.preventDefault();
		$('.overlay_customer_login').click();		
		signup();	
	})
	
	
	
	$('.overview-wrapper .c2 .more_desc_btn').click(function(e){
		$('.overview-wrapper .c2 .more_desc').slideToggle('slow');
		$('.overview-wrapper .c2 .more_desc_btn').toggleClass('up');
	})	
	
	$('#slider').nivoSlider();
	
	$('.map-tab a').click(function(e){
		e.preventDefault();
		
		var centerLoc = $(this).attr('data-map_center');
		var map_zoom = Number($(this).attr('data-map_zoom'));
		
		var geocoder = new google.maps.Geocoder();
		geocoder.geocode({'address': centerLoc}, 
		function(results, status) {
			
			if (status == google.maps.GeocoderStatus.OK) {
				var center_map = results[0].geometry.location;
				map.setZoom(map_zoom);
				map.setCenter(center_map);
			}
		});			
		  
	})
	
	
	var _fh = $('.footer-wraper').height();
	var _pd_ix = $('.package-detail .package-inclusive-exclusive-wrapper').height();
	var _pd_ac =  $('.package-detail .package-action-wrapper').height();
	//,bottom:(_fh+_pd_ix+_pd_ac+50)
	$.lockfixed(".package-detail .map-holder",{offset: {top: 0,bottom:0},wrapEle:$('.day-plan-list-wrapper')});
	$.lockfixed(".package-standard_tour_summary .r001",{offset: {top: 0, bottom: 0},wrapEle:$('.tour_summery-wrapper')});
	$.lockfixed(".byot_package_page.itinerary-plan .r001",{offset: {top: 0, bottom: 0},wrapEle:$('.tour_summery-wrapper')});
	$.lockfixed(".byot_package_page.tour-summary .r001",{offset: {top: 0, bottom: 0},wrapEle:$('.tour_summery-wrapper')});
	//$('.popup_wrapper .popup-container').hide();
	//$('.popup_wrapper').hide();
	
	//Travel Agent login -----------------------------------------------------
	$('#frmTALogin #agent_login_btn').click(function(e){
		e.preventDefault();
		
		
		
		var login_email = $('#frmTALogin #ag_email').val();
        var login_password = $('#frmTALogin #ag_password').val();
		   
		 var hasError = false;
		   
	   $('#frmTALogin .validation-advice').remove();
	   if(login_email== ''){   		   		
		   $('<div for="ag_email" class="validation-advice">Please enter a valid email address</div>').insertAfter('#frmTALogin #ag_email');
		   hasError = true;
		}else if(IsEmail(login_email)==false){
			 $('<div for="ag_email" class="validation-advice">Please enter a valid email address</div>').insertAfter('#frmTALogin #ag_email');
			 hasError = true;
		}
		
		 if(login_password.length < 6 ){ 			 
			$('<div for="ag_password" class="validation-advice">Please enter at least 6 characters.</div>').insertAfter('#frmTALogin #ag_password');
			 hasError = true;
		 }
		 
		 if(!hasError){
			$.post($("#frmTALogin").attr('action'),$("#frmTALogin").serialize(),
			function(jsonData){

				if(jsonData.status == 'error'){					
					jAlert(jsonData.message,jsonData.title);
				}else{
					if(jsonData.action == 'sdf'){
						jAlert(jsonData.message,jsonData.title);
					}
					else if(jsonData.action == 'url'){
						window.location = jsonData.redirect;	
					}
					else{
						window.location.reload();	
					}
				}
			},'json'); 
		 }
		
	})
	
	$('.supplier .btn-travel-agent-signup').click(function(e){
		e.preventDefault();
		travel_agent_signup('Travel Agent');	
	})
	
	$('.supplier .btn-suplier-signup').click(function(e){
		e.preventDefault();
		travel_agent_signup('Supplier');	
	})
	
	$('.agent_login_pop .forgot_pw a').click(function(e){
		e.preventDefault();
		$('.overlay_agent_login').hide();
		travel_agent_forgot_password();	
	})
	
	
	
	$('#ul_agent_login .overlay_top_box').click(function(e){
		$('#ag_email').focus();
		$('.overlay_agent_login').show();
	})
	
	$('.overlay_agent_login').click(function(e){
		$('.overlay_agent_login').hide();
	})
	
	
	
	$('#btn_assing_travelers').click(function(){
		var frm = $('#frmAssignTravelers');
		
		$('#msg_assign_traveler_info').html('');
		
		$('#frmAssignTravelers .validation-advice').remove();
		
		var isEmpty = false;
		var hasDuplicate = false;
		
		var arr = [];		
		frm.find('select').each(function(i){
			if($(this).val() == '')
			{
				 $('<div class="validation-advice">Please seletect traveler</div>').insertAfter($(this));
				 isEmpty = true;
			}			
			arr[i] = $(this).val();					
		});
		
		if(!isEmpty){
			arr.sort();
			var last = arr[0];
			for (var i=1; i<arr.length; i++) {
			   if (arr[i] == last){
			  	 $('#msg_assign_traveler_info').html('<div class="validation-advice">Please select a traveler for single occurance</div>');	
			  	 hasDuplicate = true;
			   }
			   last = arr[i];
			}			
		}
		
		if(!isEmpty && !hasDuplicate){
			frm.submit();
		}	
	});
	
	$('#travel_agent_login a').click(function(e){
		e.preventDefault();		
	});
	
	
	
	$('.icon-captcha-refresh').click(function(){
		$(this).prev().attr('src', base_url+'captcha/new_captcha?'+Math.random());
	});
	
	$("#frmNewsSubscribe").validate({
		rules: {
			subscribeEmail: {
				required: true,
				email: true
			}
		},
		submitHandler: function() {
			$.post($("#frmNewsSubscribe").attr('action'),$("#frmNewsSubscribe").serialize(),
			function(jsonData){
				if(jsonData.status == 'error'){					
					$("#frmNewsSubscribe").find('.subscribe-msg').html('<span>'+jsonData.message+'</span>');
				}else{					
					$("#frmNewsSubscribe").find('.subscribe-msg').html('<span>'+jsonData.message+'</span>');
				}
			},'json');
		}
	});
	
	setLng();
});

function setLng(){
	jQuery('#lng_selector').select2({containerCssClass:'lng_selector',dropdownCssClass:'lng_selector-drop'});
	$('#lng_selector').on('change',function(){
		window.location = $(this).select2('val');
	});
}

function get_mag_gray_style(){
	var featureOpts = [
		{
		  featureType: 'all',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}
		,{
		  featureType: 'landscape',
		  stylers: [
			{ visibility: 'on'},{ color: '#ffffff' }	
		  ]
		}
		,{
		  featureType: 'landscape',
		  elementType: 'labels',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}
		
		,{
		  featureType: 'administrative.country',
		  elementType : 'labels',
		  stylers: [
			{ visibility: 'off'}
	
		  ]
		}
		,{
		  featureType: 'administrative.country',
		  elementType : 'geometry.stroke',
		  stylers: [
			{ visibility: 'on'},
			{ color: '#888888' },
			{ weight: 1 }
	
		  ]
		}
		,{
		  featureType: 'water',
		  stylers: [
			{ visibility: 'on' },
			{ color: '#d6d6d6' }
		  ]
		}
		,{
		  featureType: 'water',
		  elementType: 'labels',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}	
	  ];
	  
	  return featureOpts;
}

function get_mag_style(){
	var featureOpts = [
		{
		  featureType: 'all',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}
		,{
		  featureType: 'landscape',
		  stylers: [
			{ visibility: 'on'},
			{ color: '#ffffff' }
	
		  ]
		}
		,{
		  featureType: 'landscape',
		  elementType: 'labels',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}
		,{
		  featureType: 'administrative.country',
		  elementType : 'labels',
		  stylers: [
			{ visibility: 'off'}
	
		  ]
		}
		,{
		  featureType: 'administrative.country',
		  elementType : 'geometry.stroke',
		  stylers: [
			{ visibility: 'on'},
			{ color: '#888888' },
			{ weight: 1 }
	
		  ]
		}
		,{
		  featureType: 'water',
		  stylers: [
			{ visibility: 'on' },
			{ color: '#80d1f1' }
		  ]
		}
		,{
		  featureType: 'water',
		  elementType: 'labels',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}	
	  ];
	  
	  return featureOpts;
}

var currentMousePos = { x: -1, y: -1 };
$(document).mousemove(function(event) {
	currentMousePos.x = event.pageX;
	currentMousePos.y = event.pageY;
	
	var div = $('.move_on');	
	var divX = currentMousePos.x - div.width()/2;
	var divY = currentMousePos.y+20;
	
	div.css('left',divX+ 'px');
    div.css('top',+ divY+'px');
});

function setMarkers(map, locations,type,zoom) {
	
	 var geocoder = new google.maps.Geocoder();

     var pin_image = new google.maps.MarkerImage(marker_pin);
     var pin_airport_image = new google.maps.MarkerImage(marker_pin_airport);
	 
     var shape = {
          coord: [1, 1, 1, 20, 18, 20, 18 , 1],
          type: 'poly'
     };
	 
     for (var i = 0; i < locations.length; i++) {
          	var loc = locations[i];
			var markerLat = loc[0];
			var markerLng = loc[1];
		  	var country = loc[2];
			var state = loc[3];
			var city = loc[4];
			var href = loc[5];
			var day_plan = loc[6];
			if(typeof(loc[6]) == 'undefined'){
				day_plan = '';
			}
			var infoContent = loc[7];
			if(typeof(loc[7]) == 'undefined'){
				infoContent = '';
			}
			var link_target = loc[8];
			if(typeof(loc[8]) == 'undefined'){
				link_target = '_self';
			}else if(link_target == ''){
				link_target = '_self';
			}
			
			
			if(typeof(loc[9]) != 'undefined' && loc[9] == 1){
				icon = pin_airport_image;
			}else{
				icon = pin_image;
			}
			
			if(typeof(loc[10]) != 'undefined' && loc[10] != ''){
				country_code = loc[10];
			}else{
				country_code = '';
			}
			
			var address = '';
			var lbl = '';
			
			if(type == 'world'){
				 address = country;
				 lbl  = country;
			}else if(type == 'country'){
				 address = country;
				 lbl  = country;
			}else if(type == 'state'){
				 address = state+','+country;
				 lbl  = state;
			}else if(type == 'city'){
				 if(state == ''){
					 address = city+','+country;
				 }else{
					 address = city+','+state+','+country;
				 }
				 
				 lbl  = city;
			}
			
			//console.log(type);
			//console.log(lbl);
			
			var center_map = new google.maps.LatLng(markerLat,markerLng);
			

			var marker = new google.maps.Marker({
				position: center_map,
				map: map,
				icon: icon,
				country_code: country_code,
				href: href,
				infoContent: infoContent
			});
			
			//marker.set("id", '_m'+i);
			markers.push(marker);							
			
			if(infoContent != ''){
				//createInfoWindow(marker, infoContent);
				
				/*var infowindow = new google.maps.InfoWindow({
					  content: infoContent,
					  maxWidth: 500
				  });
				*/
				
				google.maps.event.addListener(marker, 'mouseout', function(e) {
					//infowindow.close(map,marker);
					hide_infowindow(map,this);
				  });
				google.maps.event.addListener(marker, 'mouseover', function(e) {
					//infowindow.open(map,marker);
					show_infowindow(map,this);									
				  });
			}
			
			 google.maps.event.addListener(marker, 'click', function() {
				//console.log(this.href);
				if(this.href != ''){
					if(link_target == '_self'){
						window.location = this.href;
					}else if(link_target == '_blank'){
						window.open(this.href, "_blank");
					}
				}								
			 });
									
		
		  var label = new Label({map: map});
		  label.set('zIndex', 1234);
		  label.bindTo('position', marker, 'position');
		  label.set('text', lbl+day_plan);
		  
		  /*google.maps.event.addListener(map, 'zoom_changed', function() {
				var zoomLevel = map.getZoom();
				//console.log(zoomLevel);
				if(zoomLevel > 6){
					label.set('text', lbl);
				}else{
					label.set('text', '');
				}							
			
		  });*/
						  
						
					
			
     }
}

//Map---------------------------------------------------------------


function tmap(ele,locations,type,centerLat,centerLng,zoom,style)
{	
	var center_map = new google.maps.LatLng(centerLat,centerLng);
	var MY_MAPTYPE_ID = 'custom_style';	
	var featureOpts = get_mag_style();
	
	if(typeof(style) != 'undefined')
	{
		if(style == 'gray'){
			var featureOpts = get_mag_gray_style();	
		}		
	}
		
	if(type == 'world')
	{
		var mapOptions = {
			zoom: zoom,
			center: center_map,
			disableDefaultUI: true,
			panControl: false,
			panControlOptions: {
			  position: google.maps.ControlPosition.LEFT_CENTER
			},
			draggable: true,
			zoomControl: true,	
			zoomControlOptions: {
			  style:google.maps.ZoomControlStyle.SMALL
			  ,position: google.maps.ControlPosition.LEFT_CENTER
			},
			minZoom:zoom,
			scrollwheel: false,
			mapTypeId: MY_MAPTYPE_ID
		};
		
		map = new google.maps.Map(document.getElementById(ele), mapOptions);
		
		
		var styledMapOptions = {name: 'Custom Style'};	
		var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);	
		map.mapTypes.set(MY_MAPTYPE_ID, customMapType);				
		setMarkers(map, locations,type,zoom);
		
		return;
	}	
				
	var mapOptions = {
		zoom: zoom,
		center: center_map,
		disableDefaultUI: true,
		panControl: false,
		panControlOptions: {
		  position: google.maps.ControlPosition.LEFT_CENTER
		},
		draggable: true,
		zoomControl: true,	
		zoomControlOptions: {
		  style:google.maps.ZoomControlStyle.SMALL
		  ,position: google.maps.ControlPosition.LEFT_CENTER
		},
		minZoom:zoom,
		scrollwheel: false,
		mapTypeId: MY_MAPTYPE_ID
	};
	
	map = new google.maps.Map(document.getElementById(ele), mapOptions);
	
	
	
	var styledMapOptions = {name: 'Custom Style'};	
	var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);	
	map.mapTypes.set(MY_MAPTYPE_ID, customMapType);	
	
	
	// Create the DIV to hold the control and
  // call the HomeControl() constructor passing
  // in this DIV.
  var homeControlDiv = document.createElement('div');
  var homeControl = new HomeControl(homeControlDiv, map);

  homeControlDiv.index = 1;
  map.controls[google.maps.ControlPosition.TOP_RIGHT].push(homeControlDiv);

				
	setMarkers(map, locations,type,zoom);	 
}

function HomeControl(controlDiv, map) {

  // Set CSS styles for the DIV containing the control
  // Setting padding to 5 px will offset the control
  // from the edge of the map
  controlDiv.style.padding = '5px';

  // Set CSS for the control border
  var controlUI = document.createElement('div');
  controlUI.style.backgroundColor = 'white';
  controlUI.style.borderStyle = 'solid';
  controlUI.style.borderWidth = '2px';
  controlUI.style.cursor = 'pointer';
  controlUI.style.textAlign = 'center';
  controlUI.title = 'Click to set the map to Home';
  controlDiv.appendChild(controlUI);

  // Set CSS for the control interior
  var controlText = document.createElement('div');
  controlText.style.fontFamily = 'Arial,sans-serif';
  controlText.style.fontSize = '12px';
  controlText.style.paddingLeft = '4px';
  controlText.style.paddingRight = '4px';
  controlText.innerHTML = '<b>Home</b>';
  controlUI.appendChild(controlText);

  // Setup the click event listeners: simply set the map to
  // Chicago
  google.maps.event.addDomListener(controlUI, 'click', function() {
    map.setCenter(chicago)
  });

}


function scrollbarWidth() {
    var div = $('<div style="width:50px;height:50px;overflow:hidden;position:absolute;top:-200px;left:-200px;"><div style="height:100px;"></div>');
    // Append our div, do our calculation and then remove it
    $('body').append(div);
    var w1 = $('div', div).innerWidth();
    div.css('overflow-y', 'scroll');
    var w2 = $('div', div).innerWidth();
    $(div).remove();
    return (w1 - w2);
}

function setToDay(){
	var num_nights = $('#dpNumNights').val(); 
	var from_day = $( "#dpFrom" ).val();	 
	var to_day = dateFormat(parseDate(from_day,'dd-mm-yyyy').getTime()+(num_nights*1000*60*60*24),'dd-mm-yyyy');
	if(from_day != "" && num_nights != ""){
		$( "#dpTo" ).html(to_day);
	}
}

function home_js(){
	
	/*$(window).resize(function(){
		var ww = $(window).width();
		var hw = $('.home_banner_wrapper').width();
		
		if(ww > hw){
			var leftPos = (((ww+scrollbarWidth()) - hw)/2);
		}else{
			var leftPos = ((hw - (ww+scrollbarWidth()))/2);
		}
		
		$('.home_banner_wrapper').css('left',leftPos);					  
	});
	
	$(window).resize();*/
	// Initialize Advanced Galleriffic Gallery
	var gallery = $('#home_banner .home_banner_wrapper').galleriffic({
		delay:                     5000,
		numThumbs:                 6,
		preloadAhead:              10,
		enableTopPager:            false,
		enableBottomPager:         false,
		maxPagesToShow:            7,
		imageContainerSel:         '#home_banner #slideshow',
		controlsContainerSel:      '#home_banner #controls',
		captionContainerSel:       '#home_banner .content',
		loadingContainerSel:       '#home_banner #loading',
		renderSSControls:          false,
		renderNavControls:         false,
		playLinkText:              'Play Slideshow',
		pauseLinkText:             'Pause Slideshow',
		prevLinkText:              '&lsaquo; Previous Photo',
		nextLinkText:              'Next Photo &rsaquo;',
		nextPageLinkText:          '',//Next &rsaquo;
		prevPageLinkText:          '',//&lsaquo; Prev
		enableHistory:             false,
		autoStart:                 true,
		syncTransitions:           true,
		defaultTransitionDuration: 2000
		
	});	

	if($("#slider_deal li").length > 1){
		$("#slider_deal").easySlider({
			auto: true,
			speed: 2000,
			pause: 2500,
			continuous: true ,
			controlsShow: false
		});
	}
	
	$( "#dpFrom" ).datepicker({
		minDate: 0,
		changeMonth: true,
		changeYear: true,
		dateFormat:'dd-mm-yy',
		numberOfMonths: 1,
		onSelect: function( selectedDate ) {
			setToDay();	
		}
	});
	$('#dpNumNights').change(function(){
		setToDay();	
	});
	
	dropdownCssClass: "bigdrop"
	
	var cb_s2_placeholder = '';
	if(jQuery('.hps_cbs2').has('placeholder')){
		cb_s2_placeholder = jQuery('.hps_cbs2').attr('placeholder');
	}
	jQuery('.hps_cbs2').select2({ placeholder: cb_s2_placeholder,containerCssClass:'hps_cbs2'});
	

	// validate the comment form when it is submitted
	$("#frnmSPackageSearch").validate({
						rules: {
							required: true
						},
						submitHandler: function() { 
							var from = $('#frnmSPackageSearch #dpFrom').val();
							var num_nights = $('#frnmSPackageSearch #dpNumNights').select2('val');
							var country = $('#frnmSPackageSearch #dpCountry').select2('val');
							var meal = $('#frnmSPackageSearch #dpMeal').select2('val');
							window.location = $('#st_main_link').attr('href')+'/'+country+'/?from='+from+'&num_nights='+num_nights+'&meal='+meal+'&search=search';
						}
	});
}


function set_small_gallery(){
	
	var onMouseOutOpacity = 0.67;
	$('#thumbs ul.thumbs li').opacityrollover({
		mouseOutOpacity:   onMouseOutOpacity,
		mouseOverOpacity:  1.0,
		fadeSpeed:         'fast',
		exemptionSelector: '.selected'
	});
	
	// Initialize Advanced Galleriffic Gallery
	var gallery = $('#thumbs').galleriffic({
		delay:                     2500,
		numThumbs:                 6,
		preloadAhead:              10,
		enableTopPager:            false,
		enableBottomPager:         true,
		maxPagesToShow:            7,
		imageContainerSel:         '#slideshow',
		controlsContainerSel:      '#controls',
		captionContainerSel:       '#caption',
		loadingContainerSel:       '#loading',
		renderSSControls:          false,
		renderNavControls:         false,
		playLinkText:              'Play Slideshow',
		pauseLinkText:             'Pause Slideshow',
		prevLinkText:              '&lsaquo; Previous Photo',
		nextLinkText:              'Next Photo &rsaquo;',
		nextPageLinkText:          '',//Next &rsaquo;
		prevPageLinkText:          '',//&lsaquo; Prev
		enableHistory:             false,
		autoStart:                 false,
		syncTransitions:           true,
		defaultTransitionDuration: 900,
		onSlideChange:             function(prevIndex, nextIndex) {
			// 'this' refers to the gallery, which is an extension of $('#thumbs')
			this.find('ul.thumbs').children()
				.eq(prevIndex).fadeTo('fast', onMouseOutOpacity).end()
				.eq(nextIndex).fadeTo('fast', 1.0);
		},
		onPageTransitionOut:       function(callback) {
			this.fadeTo('fast', 0.0, callback);
		},
		onPageTransitionIn:        function() {
			this.fadeTo('fast', 1.0);
		}
	});	
}

function extractEmails ( text ){
    return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
}

function extractNumber ( text ){
    return text.match(/([0-9_-]+){3}/gi);
}

function setSelectedMarker(strArr){
	var arr = eval(strArr);
	for(i=0;i<markers.length;i++)
	{
		if(jQuery.inArray( markers[i].country_code, arr ) != -1)
		{
			markers[i].setIcon(marker_pin_selected);
		}else{
			markers[i].setIcon(marker_pin);
		}
	}	

}
function set_best_time_to_visit(){
	
	
	$('.tab_block_container div.tabs').hide();
	$('.tab-nav li a').click(function(){
		$('.tab-nav li').removeClass('active');
		$(this).parent().addClass('active');
		var currentTab = $(this).attr('href');
		var countryList = $(this).attr('data-items');
		
		if(markers.length == 0){
			setTimeout(function(){
				setSelectedMarker(countryList);
			},1500);
		}else{
			setSelectedMarker(countryList);	
		}
		
		
		$('.tab_block_container').find('div.tabs').hide();
		$('.tab_block_container').find(currentTab+'-tab').show();
	
		//return false;
	});
	var w_hash = window.location.hash;

	if(w_hash != ''){
		if(w_hash == '#upload_images'){			
			$($('.tab-nav li:first a').attr('href')+'-tab').show();
		}else{
			$('.tab-nav li').find('a[href="'+w_hash+'"]').click();
		}
	}else{
		$('.tab-nav li:first a').click();
	}	
}