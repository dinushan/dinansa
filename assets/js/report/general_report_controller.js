/**
 * Created by LAYOUTindex on 12/13/2017.
 */
(function(window) {
    var app = angular.module('restaurant');

    app.controller('reportController', reportController);
    reportController.$inject = ['$scope','$window'];

    function reportController($scope,$window) {
        $('#date_range').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            },
            alwaysShowCalendars: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });

        $scope.types = [
            {'id':1,'name':'daily'},
            {'id':2,'name':'Monthly'},
            {'id':3,'name':'Year'}
        ];

        $scope.stotal ={
            'vat':0,
            'sub_t':0,
            's_charge':0,
            'nbt':0,
            'discount':0,
            'g_total':0
        }
        $scope.type = $scope.types[0];
        $scope.genaralsales = [];
        $scope.banquetsales = [];
        
        $scope.filter = function () {
            send();
        };

        $scope.export =function (){
            var val = $('#date_range').val();

            var from = val.split(' ')[0].trim();
            var to   = val.split(' ')[2].trim();

            var mywindow = window.open($window.base_url+'export?from='+from+'&to='+to+'&type='+$scope.type.id,"sales_report_download");
        };

        $scope.dateConvert = function(str){

            return new Date(str);
        };


        var send = function(){
            var val = $('#date_range').val();

            var from = val.split(' ')[0].trim();
            var to   = val.split(' ')[2].trim();

            $scope.stotal ={
                'vat':0,
                'sub_t':0,
                's_charge':0,
                'nbt':0,
                'discount':0,
                'g_total':0
            };



            $.ajax({
                'url':$window.base_url+'sales_report_filter',
                'method':'post',
                'data':{'from':from,'to':to,'type':$scope.type.id}
            })
                .then( function(data){

                    $scope.$apply(function(){
                        $scope.genaralsales = data.general;
                        $scope.banquetsales = data.banquet;

                        $scope.btotal={
                            'vat':0,
                            's_charge':0,
                            'nbt':0,
                            'g_total':0
                        };

                        $scope.btotal={
                            'vat':0,
                            's_charge':0,
                            'nbt':0,
                            'g_total':0
                        };

                        for (var i in data.general){
                            $scope.stotal.vat       += parseFloat(data.general[i].vat_amount);
                            $scope.stotal.sub_t     += parseFloat(data.general[i].sub_total);
                            $scope.stotal.s_charge  += parseFloat(data.general[i].service_charge);
                            $scope.stotal.nbt       += parseFloat(data.general[i].nbt_amount);
                            $scope.stotal.discount  += parseFloat(data.general[i].discount_amount);
                            $scope.stotal.g_total   += parseFloat(data.general[i].grand_total);
                        }



                        for (var j in data.banquet){
                            $scope.btotal.vat       += parseFloat(data.banquet[j].vat_amount);
                            $scope.btotal.s_charge  += parseFloat(data.banquet[j].service_charge);
                            $scope.btotal.nbt       += parseFloat(data.banquet[j].nbt_amount);
                            $scope.btotal.g_total   += parseFloat(data.banquet[j].grand_total);
                        }
                    });

                },function (error) {

                });
        };
        send();

    }
}(window));