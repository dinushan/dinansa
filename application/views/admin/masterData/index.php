
<div class="page-container">

    <?php $this->load->view('admin/inc_sidebar_navigation.php');?>

    <div class="page-content-wrapper">
		<div class="page-content">

<ul class="nav nav-tabs">
  <li role="presentation" class="active"><a>Master Data</a></li>
  <li role="presentation"><a href="<?php echo base_url()?>admin/global-facilities">Global Facilities</a></li>
  <li role="presentation"><a href="<?php echo base_url()?>admin/global-calander">Gloabal Calander</a></li>
</ul>

    <div class="panel panel-default">
        <div class="panel-heading">Master Data <a href="<?php echo base_url()?>admin/master-data/edit" class="btn btn-success">Edit</a></div>
        <div class="panel-body">
            <div class="list-group">
                <div class="d-flex w-100 justify-content-between">
                    <h3 class="mb-4">Premium Price</h3>
                    <p class="text-success"><?php echo  $masterData->premium_price == null ? 'N/A':$masterData->premium_price ?></p>
                </div>

                <div class="d-flex w-100 justify-content-between">
                    <h3 class="mb-4">Maintenance</h3>
                    <p class="text-success"><?php echo  $masterData->maintainance == null ? 'N/A':$masterData->maintainance ?></p>
                </div>

                <div class="d-flex w-100 justify-content-between">
                    <h3 class="mb-4">SMS Cost</h3>
                    <p class="text-success"><?php echo  $masterData->sms_cost == null ? 'N/A' :$masterData->sms_cost ?></p>
                </div>

                <div class="d-flex w-100 justify-content-between">
                    <h3 class="mb-4">IPG Rate</h3>
                    <p class="text-success"><?php echo  $masterData->ipg_rate == null ? 'N/A' :$masterData->ipg_rate ?></p>
                </div>

                <div class="d-flex w-100 justify-content-between">
                    <h3 class="mb-4">Booking Time</h3>
                    <p class="text-success"><?php echo  $masterData->booing_time == null ? 'N/A' :$masterData->booing_time ?></p>
                </div>

                <div class="d-flex w-100 justify-content-between">
                    <h3 class="mb-4">Advance Booking</h3>
                    <p class="text-success"><?php echo  $masterData->advance_booking ?></p>
                </div>

                <div class="d-flex w-100 justify-content-between">
                    <h3 class="mb-4">Film Levy</h3>
                    <p class="text-success"><?php echo  $masterData->film_leavy ?></p>
                </div>

                <div class="d-flex w-100 justify-content-between">
                    <h3 class="mb-4">Government Tax</h3>
                    <p class="text-success"><?php echo  $masterData->government_tax ?></p>
                </div>

                <div class="d-flex w-100 justify-content-between">
                    <h3 class="mb-4">Film Importer</h3>
                    <p class="text-success"><?php echo  $masterData->film_importer ?></p>
                </div>

                <div class="d-flex w-100 justify-content-between">
                    <h3 class="mb-4">Film Distributor</h3>
                    <p class="text-success"><?php echo  $masterData->film_distributor ?></p>
                </div>

                <div class="d-flex w-100 justify-content-between">
                    <h3 class="mb-4">Film Exibiter</h3>
                    <p class="text-success"><?php echo  $masterData->film_exibiter ?></p>
                </div>

            </div>
        </div>
    </div>
    </div>
    </div>
</div>