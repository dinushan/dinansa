<?php

use Illuminate\Database\Eloquent\Model as Eloquent;

class AppSettings extends Eloquent {

    protected $table = "app_settings";
    protected $primaryKey = "id";
    protected $fillable = ['id','option_name', 'type', 'value','notes','created_at','updated_at'];

    public static function getAllAppSettingConstants(){

        $posts =  self::all();
        $posts->each(function($post) {
            define($post->option_name, $post->value);
            define($post->option_name."_TYPE", $post->type);
        });
    }
}