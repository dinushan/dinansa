<?php

/**
 * This line is required, it must contain the label for your unique username field (what users login with)
 */
$lang['adminauth_username']			= 'Username';

/**
 * Password Complexity Labels
 */
$lang['adminauth_pwd_uppercase']		= 'Uppercase Letters';
$lang['adminauth_pwd_number']			= 'Numbers';
$lang['adminauth_pwd_special']		= 'Special Characters';
$lang['adminauth_pwd_spaces']			= 'Spaces';

/**
 * Login Error Messages
 */
$lang['adminauth_login_failed']		= 'Invalid %s or Password';
$lang['adminauth_user_inactive']		= 'You must activate this account before you can login.';
$lang['adminauth_user_locked_out']	= 'You have been locked out for %d minutes for too many invalid login attempts, please try again later.';
$lang['adminauth_pwd_expired']		= 'Your password has expired.';

/**
 * User Validation Error Messages
 */
$lang['adminauth_unique_username']	= 'The %s field must be unique.';
$lang['adminauth_password_is_valid']	= '%s does not meet the complexity requirements: ';
$lang['adminauth_username_required']	= 'The %s field is required.';
$lang['adminauth_password_required']	= 'The %s field is required.';
$lang['adminauth_passwd_complexity']	= 'Password does not meet complexity requirements: %s';
$lang['adminauth_passwd_min_length']	= 'Password must be at least %d characters.';
$lang['adminauth_passwd_max_length']	= 'Password may not be longer than %d characters.';

$lang['adminauth_unique_emp_id']	= 'The %s field must be unique.';

/**
 * Group Validation Error Messages
 */
$lang['adminauth_unique_group']		= 'The %s field must be unique.';
$lang['adminauth_groupname_required']	= 'Group name is required.';

/**
 * General Error Messages
 */
$lang['adminauth_instance_na']		= "BitAuth was unable to get the CodeIgniter instance.";
$lang['adminauth_data_error']			= 'You can\'t overwrite default BitAuth properties with custom userdata. Please change the name of the field: %s';
$lang['adminauth_enable_gmp']			= 'You must enable php_gmp to use Bitauth.';
$lang['adminauth_user_not_found']		= 'User not found: %d';
$lang['adminauth_activate_failed']	= 'Invalid activation link/code.';
$lang['adminauth_expired_datatype']	= '$user must be an array or an object in Bitauth::password_is_expired()';
$lang['adminauth_expiring_datatype']	= '$user must be an array or an object in Bitauth::password_almost_expired()';
$lang['adminauth_add_user_datatype']	= '$data must be an array or an object in Bitauth::add_user()';
$lang['adminauth_add_user_failed']	= 'Adding user failed, please notify an administrator.';
$lang['adminauth_code_not_found']		= 'Activation Code Not Found.';
$lang['adminauth_edit_user_datatype']	= '$data must be an array or an object in Bitauth::update_user()';
$lang['adminauth_edit_user_failed']	= 'Updating user failed, please notify an administrator.';
$lang['adminauth_del_user_failed']	= 'Deleting user failed, please notify an administrator.';
$lang['adminauth_set_pw_failed']		= 'Unable to set user\'s password, please notify an administrator.';
$lang['adminauth_no_default_group']	= 'Default group was either not specified or not found, please notify an administrator.';
$lang['adminauth_add_group_datatype']	= '$data must be an array or an object in Bitauth::add_group()';
$lang['adminauth_add_group_failed']	= 'Adding group failed, please notify an administrator.';
$lang['adminauth_edit_group_datatype']= '$data must be an array or an object in Bitauth::update_group()';
$lang['adminauth_edit_group_failed']	= 'Updating group failed, please notify an administrator.';
$lang['adminauth_del_group_failed']	= 'Deleting group failed, please notify an administrator.';
$lang['adminauth_lang_not_found']		= '(No language entry for "%s" found!)';


$lang['success_msg_my_account_save_password']= 'The new password has been saved.';
$lang['success_msg_user_add']				= 'The User has been saved.';
$lang['success_msg_user_update']			= 'The User has been updated.';
$lang['success_msg_user_delete']			= 'The User has been deleted.';
$lang['success_msg_user_group_add']			= 'The Group details have been saved.';
$lang['success_msg_user_group_save']		= 'The Group details have been updated.';
$lang['success_msg_user_group_delete']		= 'The Group has been deleted.';

$lang['adminauth_not_found']		= 'User not found';
$lang['adminauth_fogot_password_code_not_found']		= 'Code Not Found';
$lang['adminauth_unable_to_send_email']	= 'Unable to send an e-mail.Please try again.';
//$lang['adminauth_send_password_reset_email']	= 'You will receive an e-mail message with password reset details.';
$lang['adminauth_send_password_reset_email']	= 'You will receive an e-mail to the below address with password reset details.';
$lang['adminauth_enable_account']		= 'Please activate your account. An email has been sent to you with detailed instructions on how to activate it.';

$lang['adminauth_fogot_password_code_not_found']	= 'Link has been expired or invalid parameter';