<?php

class Migration_Add_Floor_Table extends CI_Migration
{
    public function up()
    {


        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => true,
                    'auto_increment' => true
                ),
                'name' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                ),
                'json_data' => array(
                    'type' => 'TEXT',
                ),
                'restaurant_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => true,
                ),
                'image' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '255',
                    'null' => true,
                ),
                'width' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => true,
                    'null' => true,
                ),
                'height' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => true,
                    'null' => true,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'on update' => 'NOW()',
                    'null' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
            )
        );

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('restaurant_floor');
    }

    public function down()
    {
        $this->dbforge->drop_table('restaurant_floor');
    }
}
