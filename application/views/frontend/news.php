<?php include(APPPATH . 'views/frontend/inc/head.php'); ?>
<?php include(APPPATH . 'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer newsPage newsParentPage">
    <div class="row">
        <div class="col-xs-12 breadcrumbCont">
            <?php include(APPPATH . 'views/frontend/inc/breadcrumb.php'); ?>
        </div>
        <div class="col-xs-12 headWrap">
            <h2 class="heading">News</h2>
        </div>
        <?php

        if(count($news) > 0)
        {
            ?>
        <div class="col-xs-12 col-md-8 pageBody">
            <div class="newsSectionWrap">

                <?php
                foreach ($news as $data) {
                    $title = $data['title'];
                    $published_date = $data['published_date'];
                    $short_description = $data['short_description'];
                    $url_key = $data['url_key'];
                    
                    ?>
                    <div class="newsSection">
                        <h2 class="filmTitle"><?php echo $title; ?></h2>
                        <span class="bookedDate"><?php echo $published_date; ?></span>
                        <div class="newsDetail">
                            <p>
                                <?php echo $short_description; ?>
                            </p>
                        </div>
                        <div class="buttonWrap">
                            <a href="<?php echo base_url($url_key); ?>" class="btn btn-scope btn-read-more">Read
                                More</a>
                        </div>
                    </div>
                    <?php
                }
                ?>
                
                <div class="paginationWrap">
                    <?php
                    if(count($news) > 0):
                        echo $pagination;
                    endif;
                    ?>
                </div>
                
            </div>
        </div>
        <div class="col-xs-12 col-md-4 pageSide">
            <div class="postNavWrap recentPostWrap">
                <div class="postHeading">Resent Posts</div>
                <ul class="navList list-group">
                    <?php
                    foreach ($recent_post as $data) {
                        $title = $data['title'];
                        $url_key = $data['url_key'];
                        ?>
                        <li class="list-group-item"><a class="link" href="<?php echo base_url($url_key); ?>"><?php echo $title; ?></a></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
            <div class="postNavWrap postByMonthWrap">
                <div class="postHeading">Posts by month</div>
                <ul class="navList list-group">
                    <?php
                    foreach ($post_by_month as $data) {
                        $published_date = $data['published_date'];
                        $url_key = $data['url_key'];

                        $timestamp = strtotime($published_date);
                        $newDate = date('F Y', $timestamp);
                        $function_month = date("Y-m", $timestamp);

                        ?>
                        <li class="list-group-item"><a onclick="getMonth('<?php echo $function_month; ?>')" class="link"><?php echo $newDate;  ?></a></li>
                        <?php
                    }
                    ?>
                </ul>
            </div>
        </div>

        <?php
        }
        else
        {
            echo '<div class="col-xs-12"><h3 class="noDataMessage">No News Available</h3></div>';
        }
        ?>
    </div>
</div>


<?php include(APPPATH . 'views/frontend/inc/footer.php'); ?>
<script>
    $('#dateOfBirth').datepicker({
        changeMonth: true,
        changeYear: true,
        showButtonPanel: true,
        yearRange: '-100:-20',
        dateFormat: 'dd MM yy',
        beforeShow:function(input, ins) {
                datePickerBeforeShow();
        },
        onClose:function(input, ins) {
                datePickerOnClose(input);
        }
    }).focus(function () {
        $(this).blur()
    }).datepicker('widget').wrap('<div class="ll-skin-melon dateOfBirth"/>');
</script>