var TableAjax = function () {

    var initPickers = function () {
        //init date pickers
        $('.date-picker').datepicker({
            rtl: Metronic.isRTL(),
            format: "yyyy-mm-dd",
            autoclose: true
        });
    }

    var handleRecords = function () {

        var grid = new Datatable();

        var disable_index = [];
        $("#datatable_ajax th.sorting_disabled").each(function(i){
            disable_index[i] = $(this).index();
        });



        grid.init({
            src: $("#datatable_ajax"),
            onSuccess: function (grid, response) {
                // grid:        grid object
                // response:    json object of server side ajax response
                // execute some code after table records loaded
            },
            onError: function (grid) {
                // execute some code on network or other general error
            },
            onDataLoad: function(grid) {
                // execute some code on ajax data load
            },
            loadingMessage: 'Loading...',
            dataTable: { // here you can define a typical inventory_items settings from http://datatables.net/usage/options

                // Uncomment below line("dom" parameter) to fix the dropdown overflow issue in the inventory_items cells. The default inventory_items layout
                // setup uses scrollable div(table-scrollable) with overflow:auto to enable vertical scroll(see: assets/global/scripts/inventory_items.js).
                // So when dropdowns used the scrollable div should be removed.
                //"dom": "<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'<'table-group-actions pull-right'>>r>t<'row'<'col-md-8 col-sm-12'pli><'col-md-4 col-sm-12'>>",

                "bStateSave": true, // save inventory_items state(pagination, sort, etc) in cookie.

                "lengthMenu": [
                    [10, 20, 50, 100, 150, -1],
                    [10, 20, 50, 100, 150, "All"] // change per page values here
                ],
                "pageLength": 10, // default record count per page
                "ajax": {
                    "url": $("#datatable_ajax").data('ajax_url') // ajax source
                },
                "order": [
                    [$("#datatable_ajax").data('order-col'),$("#datatable_ajax").data('order-type')]
                ]// set first column as a default sort by asc
                ,"columnDefs": [{ // define columns sorting options(by default all columns are sortable extept the first checkbox column)
                    'orderable': false,
                    'targets': disable_index
                }]
            }
        });


        // handle group actionsubmit button click
        /*
         grid.getTableWrapper().on('click', '.table-group-action-submit', function (e) {
         e.preventDefault();
         var action = $(".table-group-action-input", grid.getTableWrapper());
         if (action.val() != "" && grid.getSelectedRowsCount() > 0) {
         grid.setAjaxParam("customActionType", "group_action");
         grid.setAjaxParam("customActionName", action.val());
         grid.setAjaxParam("id", grid.getSelectedRows());
         grid.getDataTable().ajax.reload();
         grid.clearAjaxParams();
         } else if (action.val() == "") {
         Metronic.alert({
         type: 'danger',
         icon: 'warning',
         message: 'Please select an action',
         container: grid.getTableWrapper(),
         place: 'prepend'
         });
         } else if (grid.getSelectedRowsCount() === 0) {
         Metronic.alert({
         type: 'danger',
         icon: 'warning',
         message: 'No record selected',
         container: grid.getTableWrapper(),
         place: 'prepend'
         });
         }
         });
         */
    }

    return {

        //main function to initiate the module
        init: function () {

            initPickers();
            handleRecords();
        }

    };

}();