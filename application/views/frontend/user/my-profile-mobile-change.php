<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer">
	<div class="row">
		<div class="col-xs-12 breadcrumbCont">

            <div class="breadcrumbWrap">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                    <li class="active">Mobile Number Change</li>
                </ol>
            </div>

		</div>
		<div class="col-xs-12 col-md-6 col-md-offset-3 changeMobileCont">
			<div class="changeMobileWrap">
			<div class="boxHeadWrap">
			<h2 class="boxTitle">Enter your 10 digit mobile number below to change</h2>
							<div class="boxDesc">
					
				</div>
			</div>

			<div class="formWrap">

				<div id="message_signUp"><?php echo get_message(); ?></div>
				<?php
				$attributes = array('id' => 'frm-signup', 'class'=>'form-type-1');
				echo form_open(getCurrentUrl(), $attributes);
				?>
				<div class="form-group">
					<input type="text" class="form-control input-scope" placeholder="07X-XXXXXXX" name="phone" id="phone" value="<?php //echo $phone; ?>">
					<div class="formError">
						<?php echo form_error('phone'); ?>
					</div>
					
				</div>

				<div class="form-group">
					<input type="hidden" name="s" value="<?php echo $this->session->session_id ?>">
					<input type="hidden" name="action" value="signUp-mobile-validation" />
					<button type="submit" class="btn btn-scope btn-blue">VERIFY MOBILE NUMBER</button>
				</div>
				</form>
			</div>
			</div>
			<div class="changeMobileWrap">
				<div class="boxHeadWrap">
					<h2 class="boxTitle">Enter your Verification Code</h2>
					<div class="boxDesc">
						<p>4 digit number you must have received on your mobile via SMS</p>
					</div>
					
				</div>

				<div class="formWrap">

					<?php
					$attributes = array('id' => 'frm-signup', 'class'=>'form-type-1');
					echo form_open(getCurrentUrl(), $attributes);
					?>
					<div class="form-group">
						<input type="text" class="form-control input-scope" name="opt_code" id="opt_code" placeholder="4-digit OTP" value="">
						<div class="formError">
						<?php echo form_error('opt_code'); ?>
						</div>
					</div>

					<div class="form-group">
						<input type="hidden" name="s" value="<?php echo $this->session->session_id ?>">
						<input type="hidden" name="action" value="signUp-mobile-confirm-validation" />
						<button type="submit" class="btn btn-scope btn-read-more">CONFIRM VERIFICATION</button>
					</div>
					</form>
				</div>
			</div>
			
		</div>
	</div>
</div>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>