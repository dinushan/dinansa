<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>  

	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
            <h3 class="page-title">Price Modules</h3>

			<div class="page-bar">
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						
						<a href="<?php echo base_url().'admin/theater/price/movie/'.$this->encrypt_lib->encode($theater->id);?>" class="btn green"><i
								class="fa fa-plus-circle"></i> Add New</a>
						
							<a onclick="setLocation('admin/theater/audit_log')" class="btn btn-info"><i
									class="fa fa-file-o"></i> Audit Log</a>
						
						<a class="btn default" href="<?php echo site_url('admin/theater') ?>"><i
								class="fa fa-chevron-left"></i> Back</a>
					</div>
				</div>
			</div><!-- End Page-bar -->

            <div class="table-container">
                <table class="table table-striped table-bordered table-hover">
                    <thead>
                        <tr class="heading" role="row">
                            <th>ID</th>
                            <th rowspan="1" colspan="1">Date</th>
                            <th>Action</th>
                            <th>Status</th>
                        </tr>
                    </thead>

                    <tbody>
                      <?php foreach($pricemodules as $data): ?>
                        <tr>

                        </tr>
                      <?php endforeach; ?>
                    </tbody>

                </table><!-- End table -->
            </div><!-- End table-container -->

        </div>
    </div>
</div>
