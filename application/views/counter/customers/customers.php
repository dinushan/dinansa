<!-- BEGIN CONTAINER -->

<div class="page-container">
    <?php $this->load->view('counter/inc_sidebar_navigation.php'); ?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">
<?php if ($page_action == ''): ?>
    <h3 class="page-title">Customers</h3>

    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right">

            </div>
        </div>
    </div>

    <?php if(
    $this->counterauth->has_role('301')
    ):?>
    <?php echo get_message() ?>
    <div class="row">
        <div class="col-md-12">

            <div class="table-container">

                <table class="table table-striped table-bordered table-hover" id="datatable_ajax"
                       data-order-col = "0"
                       data-order-type = "asc"
                       data-ajax_url="<?php echo base_url('counter/' . $slug . '/ajax_list') ?>">
                    <colgroup>
                        <col width="40"/>
                        <col width="300">
                        <col width="300">
                        <col width="200">
                        <col width="100">

                    </colgroup>
                    <thead>
                    <tr role="row" class="heading">
                        <th>ID</th>
                        <th>First name</th>
                        <th>Last name</th>
                        <th>Mobile</th>
                        <th>Enabled</th>

                        <th class="sorting_disabled">Action</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="first_name">
                        </td>

                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="last_name">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="mobile_no">
                        </td>
                        <td>
                            <select name="enabled" class="form-control form-filter input-sm">
                                <option value="-1">All</option>
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-sm yellow filter-submit margin-bottom"><i
                                        class="fa fa-search"></i> Search
                                </button>
                            </div>
                            <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset
                            </button>
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function() {
            TableAjax.init();
        });
    </script>

    <?php endif; ?>


<?php elseif ($page_action == 'add' || $page_action == 'edit'): ?>

    <h3 class="page-title"><?php echo 'Edit "'.$user_data->first_name.'"'; ?></h3>
    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right"> <a class="btn default" href="<?php echo site_url('counter/customers') ?>"><i class="fa fa-chevron-left"></i> Back</a> </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12"> <?php echo get_message() ?>
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light bordered">
                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <?php
                    $attr = array("id"=>"edit_form","class"=>"form-horizontal","enctype"=>"multipart/form-data");
                    echo form_open(current_url(),$attr);
                    ?>
                    <?php
                    if($page_action == 'edit'){

                        $user_id 		= $user_data->user_id;
                        $first_name     = $user_data->first_name;
                        $last_name 		= $user_data->last_name;
                        $phone 		    = $user_data->phone;
                        $enabled 		= $user_data->enabled;

                    }else{
                        $user_id 		= '';
                        $first_name 	= '';
                        $last_name	 	= '';
                        $phone          = '';
                        $enabled	 	= '';

                        $required_entry = ' required-entry';

                    }

                    ?>
                    <h4 class="form-section">General</h4>
                    <div class="form-group">
                        <label class="control-label col-md-3">First Name<span class="req">*</span></label>
                        <div class="control-element col-md-4">
                            <input name="first_name" type="text" class="form-control required-entry" value="<?php echo set_value('first_name',$first_name) ?>" />
                            <?php echo form_error('first_name'); ?> </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Last Name<span class="req">*</span></label>
                        <div class="control-element col-md-4">
                            <input name="last_name" type="text" class="form-control required-entry" value="<?php echo set_value('last_name',$last_name) ?>" />
                            <?php echo form_error('last_name'); ?> </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Phone<span class="req">*</span></label>
                        <div class="control-element col-md-4">
                            <input name="phone" type="text" class="form-control required-entry" value="<?php echo set_value('phone',$phone) ?>" />
                            <?php echo form_error('phone'); ?> </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Is Enabled<span class="req">*</span></label>
                        <div class="control-element col-md-4">
                            <select name="is_enabled" class="form-control required-entry">
                                <option <?php if($enabled == '1'){ echo 'selected'; } ?> value="1">Yes</option>
                                <option <?php if($enabled == '0'){ echo 'selected'; } ?> value="0">No</option>
                            </select>
                            <?php echo form_error('phone'); ?> </div>
                    </div>

                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <button type="submit" class="btn green">Submit</button>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">
                        $('.required-entry').attr('required','required');
                        $('#edit_form').validate({
                            errorElement: 'span',
                            errorClass: 'validation-advice',
                            ignore: "",
                            submitHandler: function (form) {
                                form.submit();
                            }
                        });

                    </script>
                    </form>
                </div>
            </div>
        </div>
    </div>

<?php endif;//$page_action ?>
</div>
<!-- END PAGE CONTENT-->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER --> 