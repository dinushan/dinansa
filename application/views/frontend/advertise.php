<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer">
   <div class="row">
      <div class="col-xs-12 breadcrumbCont">
		  <?php include(APPPATH.'views/frontend/inc/breadcrumb.php'); ?>
      </div>
     <!--  <div class="col-xs-12 headWrap">
         <h2 class="heading">Advertise</h2>
      </div> -->
      
     </div>
</div>
<div class="container-fluid pageMiddleContainer advertisePage">
	<div class="row">

		<?php
		$i = 0;
		foreach ($advertise_list as $add) {
			$oddEven =($i % 2) ? 'odd':'even';
			?>
			<div class="col-xs-12 advertiseSec">
				<div class="container">
					<div class="row">
						<div class="col-xs-12 media advertiseMedia">

							<?php
							if ($oddEven == 'even')
							{
							?>
								<div class="media-left media-side media-middle">
									<a class="media-object-wrap">
										<img class="media-object"
											 src="<?php echo base_url(ADVERTISE_IMG_PATH . $add['image']) ?>" alt="...">
									</a>
								</div>
							<?php
							}
							?>

							<div class="media-body media-middle">
								<h3 class="media-heading"><?php echo $add['title']; ?></h3>
								<div class="mediaDesc"><?php echo $add['description']; ?></div>
							</div>
								<?php
								if ($oddEven == 'odd')
								{
									?>
									<div class="media-right media-side media-middle">
										<a class="media-object-wrap">
											<img class="media-object" src="<?php echo base_url(ADVERTISE_IMG_PATH . $add['image']) ?>" alt="...">
										</a>
									</div>
									<?php
								}
								?>



						</div>
					</div>
				</div>
			</div>
			<?php
			$i++;
		}
			?>
		
	</div>
</div>


<script>
	var jvalidate = $("#frm-contact").validate({
		ignore: [],
		rules: {
			name: {
				required: true
			},
			company: {
				required: true
			},
			email: {
				required: true,
				email: true
			},
			contact: {
				required: true
			},
			comment: {
				required: true
			}
		}
	});
</script>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>