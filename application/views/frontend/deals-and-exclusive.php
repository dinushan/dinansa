<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer">
	<div class="row">
		<div class="col-xs-12 breadcrumbCont">
			<?php include(APPPATH.'views/frontend/inc/breadcrumb.php'); ?>
		</div>
		 <div class="col-xs-12 headWrap">
            <h2 class="heading"><?php echo $sitePageData->title1; ?></h2>
        </div>
       <!--  <div class="col-xs-12 dealsPage">
        	<h3 class="noDataMessage">There are no deals & exclusives available</h3>
        </div> -->
		<div class="col-xs-12 dealsPage">
			<div class="row">
				<?php

            if ( count($deals_data) > 0) {
                foreach ($deals_data as $deals) {
                    ?>
                    <div class="col-xs-6 col-sm-4 col-md-3 dealsWrap">
                        <a href="#deals-modal" title="" class="dealsLink popup-modal"
                           data-deal-id="<?php echo $deals['id']; ?>">
                            <img src="<?php echo base_url(DEALS_IMG_PATH . $deals['image']); ?>" alt="">
                        </a>
                    </div>
                    <?php
                }
            }else{
                ?>
                <h4 class="text-center">There's no Deals and Exclusives</h4>
                <?php
            }
		?>
			</div>
		</div>
	</div>
</div>

<div id="deals-modal" class="deals-modal white-popup-block mfp-hide">
	<div class="dealsBody">
	<div class="row">
		<div class="col-xs-12 headerWrap">
			<h2 class="header"></h2>
		</div>
		<div class="col-xs-6 col-sm-5 col-md-4 imageWrap">
			<img src="images/offer/kotac-credit.jpg" alt="">
		</div>
		<div class="col-xs-6 col-sm-7 col-md-8 modalDescWrap">
			<div class="modalDesc">

			</div>
		</div>
	</div>
	</div>
</div>


<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>
