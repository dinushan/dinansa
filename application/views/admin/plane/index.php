

<div class="page-container">

    <?php $this->load->view('admin/inc_sidebar_navigation.php');?> 

    <div class="page-content-wrapper">
		<div class="page-content">


    <div class="panel panel-default">
        <div class="panel-heading">Planes Details <a href="<?php echo base_url()?>admin/cinama/create" class="btn btn-success">Create New</a></div>
        <div class="panel-body">
            <table class="table table-responsive">
                <thead>
                    <tr>
                        <th>Name<th>
                        <th>Description</th>
                        <th></th>
                        <th></th>
                    </tr>
                </thead>

                <tbody>
                    <?php foreach ($planes as $plane) { ?>
                        <tr>
                            <td><?php echo $plane->plane_name ?></td>
                            <td><?php echo $plane->description ?></td>
                            <td><a class="btn btn-success" href="<?php echo base_url() ?>admin/plane/seats/<?php echo $plane->id ?>">View Seat Plane</a></td>
                            <td><a class="btn btn-primary" href="<?php echo base_url() ?>admin/plane/rates/<?php echo $plane->id ?>">View Rate Plane</a></td>
                        </tr>
                    <?php }?>
                </tbody>
            </table>
        </div>
    </div>
    </div>
    </div>
</div>