<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>


<!-- BEGIN CONTAINER -->

<div class="container pageContainer portlet">
	<div class="row">
		<div class="col-xs-12 breadcrumbCont">
			<?php include(APPPATH.'views/frontend/inc/breadcrumb.php'); ?>
		</div>
		<div class="col-xs-12 buyTicketProcessWrap">
			<ul class="buyTicketProcess">
				<li id="movie_select_tab">
					<span class="text">Movie Selection</span>
				</li>
				<li  id="seat_select_tab">
					<span class="text">Seat Selection</span>
				</li>
				<li class="current" id="booking_select_tab">
					<span class="text">Payment</span>
				</li>

			</ul>
		</div>


        <div class="col-xs-12 ticketSection">
            <iframe height="400px" width="100%" src="<?php echo $ipg_url; ?>" frameborder="0"></iframe>
        </div>

	</div>
</div>


<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>
