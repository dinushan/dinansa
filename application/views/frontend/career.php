<?php include(APPPATH . 'views/frontend/inc/head.php'); ?>
<?php include(APPPATH . 'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer careerPage" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div class="col-xs-12 breadcrumbCont">
            <?php include(APPPATH . 'views/frontend/inc/breadcrumb.php'); ?>
        </div>

        <!-- <div class="col-xs-12 headWrap">
            <h2 class="heading"><?php echo $sitePageData->menu; ?></h2>
        </div> -->
    </div>
</div>

<div class="container">
    <div class="careerHead">
        <div class="row">
            <div class="col-md-6 careerHeadGrid">
                <div class="dis-table">
                    <div class="dis-t-cell">
                        <div class="logoWrap">
                            <div class="logoText">Work at</div>
                            <div class="logoImg">
                               <img class="img-responsive" src="<?php echo base_url() ?>skin/images/scope-cinemas-logo.png" alt="scope logo"/> 
                            </div>
                        </div>
                     </div>
                </div>
            </div>
            <div class="col-md-6 careerHeadGrid">
                <div class="dis-table">
                    <div class="dis-t-cell">
                        <div class="desc">
                            <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod
                            tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam,
                            quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo
                            consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse
                            cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non
                            proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="careerBody">
            <div class="col-xs-12 bodyHeadWrap">
                 <h3 class="text-center bodyHead">Current Vacancies</h3>
            </div>
           
              <?php foreach($careers as $career):  ?>
                <div class="col-sm-6 col-xs-12 careerGrid">
                    <div class="careerGridWrap">
                        <div class="careerCaption">
                            <h4 class="careerTitle"><?php echo $career->title?></h4>
                            <div class="desc">
                                <p> <?php echo $career->summery?></p>
                            </div>
                        </div>
                        <div class="btnWrap">
                              <a class="btn btn-scope btn-read-more" href="<?php echo base_url('careers/'.$career->id)?>" >Read more</a>
                        </div>
                    </div>
                </div>
             <?php endforeach; ?>
        </div>
    </div>
     
</div>

<?php include(APPPATH . 'views/frontend/inc/footer.php'); ?>