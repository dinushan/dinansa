/**
 * Created by LAYOUTindex on 11/29/2017.
 */
(function(window){
    var app = angular.module('locations',[]);

    app.directive('datatable', function ($window){

        return {
            templateUrl:$window.baseurl+'assets/js/locations/datatable.html',

            controller:function ($scope,$window) {



                $scope.views        = [
                    {id:10,val:10},
                    {id:20,val:20},
                    {id:50,val:50},
                    {id:100,val:100},
                    {id:150,val:150},
                    {id:-1,val:'All'}
                ];


                $scope.page         = 1;
                $scope.totalPages   = 0;
                $scope.view         = $scope.views[0];
                $scope.totalrecods  = 0;
                $scope.results = [];

                $scope.getStyle = function (ob){
                    var curval = parseFloat(ob[7]), minval = parseFloat(ob[8]);

                    if (curval < minval+10 || curval == minval){

                        return {'background-color':'#F59DA8','color':'#fff'};
                    }else if (curval < minval){
                        return {'background-color':'#F53D46','color':'#fff'};

                    }else{
                        return null;
                    }

                };


                $scope.prev = function (){
                    if ($scope.page <= 1){
                        $scope.page = 1;
                    }else{
                        $scope.page -= 1;

                    }
                    sendfilter();
                };

                $scope.next = function (){
                    $scope.page += 1;
                    sendfilter();
                };

                var sendfilter = function () {
                    window.jQuery.ajax({
                        'method':'POST',
                        'url':$window.ajaxUrl,
                        'data':{
                            'length':$scope.view.id,
                            'start':$scope.page
                        }
                    }).done( function (resp) {
                        $scope.$apply(function(){
                            $scope.results = resp.data;
                            $scope.totalrecods = resp.recordsTotal;
                            if ($scope.view.id != -1){
                                $scope.totalPages = Math.ceil(resp.recordsTotal/$scope.view.val);
                            }else{
                                $scope.totalPages = 1;
                            }

                        });
                    }).error( function (){

                    })
                };

                sendfilter();

                $scope.filter = function (){
                    sendfilter();
                };


            }

        }

    });
}(window));