
<html lang="en" ng-app="seat_plane">
<head>
    <meta charset="utf-8">
    <title>Scope</title>

    <link href="<?php echo base_url() ?>skin/css/plugins.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>skin/css/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>skin/css/main.css" rel="stylesheet">
    <link href="<?php echo base_url() ?>skin/sass/buy-tickets.scss" rel="stylesheet">
    <link href="<?php echo base_url() ?>skin/sass/page-default.scss" rel="stylesheet">


    <script src="<?php echo base_url() ?>skin/js/plugins.js"></script>
    <script src="<?php echo base_url()?>angular/angular.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/angular-touch/1.6.2/angular-touch.js"></script>
    <!--Validation js-->
    <script type='text/javascript' src='<?php echo base_url()?>/js/validationengine/languages/jquery.validationEngine-en.js'></script>
    <script type='text/javascript' src='<?php echo base_url()?>/js/validationengine/jquery.validationEngine.js'></script>
    <script type='text/javascript' src='<?php echo base_url()?>/js/validationengine/jquery.validate.js'></script>

    <script src="https://dev2.appslanka.com:3001/socket.io/socket.io.js"></script>

    <script type="text/javascript">
        var base_url        = '<?php echo base_url() ?>';
        window.socket_url   = '<?php echo $this->config->item('socket_url')?>';

        var seat_plane      = <?php echo json_encode($seat_plane)?>;
        var seat_plan_x_y   = <?php echo json_encode($seat_plan_x_y) ?>;
        var current_status  = <?php echo json_encode($result)?>;
        var theater         = <?php echo json_encode($theater)?>;
        var movie           = <?php echo json_encode($movie)?>;
        var movie_date      = <?php echo json_encode($movie_date) ?>;
        var time_slot       = <?php echo json_encode($time_slot)?>;
        var booking_id      = <?php echo json_encode($booking_id) ?>;
        var req_seat_count  = <?php echo json_encode($req_seat_count) ?>;
        var socket          = io('https://dev2.appslanka.com:3001',{secure: true});
        var auth_token      = <?php echo json_encode($auth_token) ?>;
        var device_type     = <?php echo json_encode($device_type) ?>;
        var token = null;


    </script>

<!--script src="<?php echo $this->config->item('socket_url')?>/socket.io/socket.io.js"></script-->

<script src="<?php echo base_url() ?>angular/seat_plane/seat_plane.js"></script>
<script src="<?php echo base_url() ?>angular/seat_plane/seat_plane_service.js"></script>
<!--script src="<?php echo base_url(); ?>js/socket.js" type="text/javascript"></script-->

</head>

<body class="mobile-seat-plan">

<script type='text/javascript'>
    function showAndroidToast(toast) {
        Android.nextScreen(toast);
    }
</script>
<div class="mobile-seat-plan">
<div class="pageLoaderCont">
    <div class="loaderCont">
        <div class="loaderWrap">
            <div class="cssload-thecube">
                <div class="cssload-cube cssload-c1"></div>
                <div class="cssload-cube cssload-c2"></div>
                <div class="cssload-cube cssload-c4"></div>
                <div class="cssload-cube cssload-c3"></div>
            </div>
        </div>
    </div>
</div>


<div class="" ng-controller="seatPlaneController" ng-cloak>
		<div class="seatSelectionWrap" id="seat_select_tab_div">

				<div class="seatStructureWrap">
                    <div class="availability-tbl-holder">
                        <div id="box-seat-plan" class="form-actions">
                            <div id="available_seats" style="color: #D91E18 !important;" class="help-block font-red-thunderbird"></div>
                            <div class="block-plan-wrapper" style="overflow:visible; border:none;">
                                <div class="block-plan-container" ng-style="{'width':'100%','height':'100%'}" id="seat-palne-836">
                                    <div ng-repeat="seatplane in seatplanes"
                                         data-id="{{seatplane.id}}"
                                         data-row="{{seatplane.row}}"
                                         data-seat_no="{{seatplane.label}}"
                                         class="row {{seatplane.row}} seat_item"
                                         ng-class="seatplane.class"
                                         ng-style="{'top':seatplane.y+'px','left':seatplane.x+'px'};"
                                         title="{{seatplane.label}}"
                                        ng-click="on_hold(seatplane)"> {{seatplane.label}}
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
				</div>

		</div>
</div>

<script src="<?php echo base_url()?>js/online-buy-tiket/seatPlane.js"></script>
<script src="<?php echo base_url()?>skin_admin/global/plugins/jquery-alerts/jquery.alerts.js" type="text/javascript"></script>
<link href="<?php echo base_url()?>skin_admin/global/plugins/jquery-alerts/jquery.alerts.css" rel="stylesheet" type="text/css"/>
</div>

</body>
</html>
