<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

/**
 * Users must be activated before they can login
 * Default: TRUE
 */
$config['require_user_activation'] = FALSE;

/**
 * Default group_id users are added to when they first register (if one isn't
 * specified)
 * Default: 2
 */
$config['default_group_id'] = 2;

/**
 * Name of the cookie where "remember me" login is kept
 * (Prefixed with the cookie_prefix value in config.php, if one is set)
 * Default: rememberme
 */
$config['remember_token_name'] = 'admin_rememberme';

/**
 * Number of seconds the remembered login is valid
 * Default: 1 week
 */
$config['remember_token_expires'] = 604800;

/**
 * Does the "remember me" expiration time update every time the user revisits
 * the site?
 * Default: TRUE
 */
$config['remember_token_updates'] = TRUE;

/**
 * Number of days before passwords expire. To disable, set to FALSE
 * Default: 90
 */
$config['pwd_max_age'] = 90;

/**
 * Number of days before password expiration to notify users their
 * password is about to expire.
 * Default: 7
 */
$config['pwd_age_notification'] = 7;

/**
 * Required minimum length of passwords
 * Default: 8
 */
$config['pwd_min_length'] = 4;

/**
 * Required maximum length of passwords. Set to 0 to disable
 * Default: 20
 */
$config['pwd_max_length'] = 20;

/**
 * Optional password complexity options. Set a number for each to
 * require that many characters, or set to 0 to disable
 * Default: 1, 1, 0, 0
 */
$config['pwd_complexity'] = array(
    'uppercase' => 0,
    'number' => 0,
    'special' => 0,
    'spaces' => 0,
);

/**
 * Which characters are included in each complexity check. Must be in
 * regex-friendly format. Using the Posix Collating Sequences should make these
 * language-independent, but are here in case you want to change them.
 */
$config['pwd_complexity_chars'] = array(
    'uppercase' => '[[:upper:]]',
    'number' => '[[:digit:]]',
    'special' => '[[:punct:]]',
    'spaces' => '\s'
);

/**
 * Number of seconds a "forgot password" code is valid for
 * Default: 3600
 */
$config['forgot_valid_for'] = 3600;

/**
 * Whether or not to log login attemps. If set to FALSE, users can no longer
 * be locked out by invalid login attempts.
 * Default: TRUE
 */
$config['log_logins'] = FALSE;

/**
 * Number of invalid logins before account is locked.
 * Set this to 0 to disable this functionality.
 * Default: 3
 */
$config['invalid_logins'] = 3;

/**
 * Number of minutes between invalid login attemps where a user will be locked
 * out
 * Default: 5
 */
$config['mins_login_attempts'] = 5;

/**
 * Number of minutes before a locked account is unlocked.
 * Default: 10
 */
$config['mins_locked_out'] = 10;

/**
 * Tables used by BitAuth
 */
$config['table'] = array(
    'users' => 'auth_restaurant_admin_users',        // Required user information (username, password, etc)
    'data' => 'auth_restaurant_admin_userdata',    // Optional user information (profile)
    'groups' => 'auth_restaurant_admin_groups',        // Groups
    'assoc' => 'auth_restaurant_admin_assoc',        // Users -> Groups associations
    'logins' => 'auth_restaurant_admin_logins'        // Record of all logins
);

/**
 * Base-2 logarithm of the iteration count used for password stretching by
 * Phpass
 * See: http://en.wikipedia.org/wiki/Key_strengthening
 * Default: 8
 */
$config['phpass_iterations'] = 8;

/**
 * Require the hashes to be portable to older systems?
 * From: http://www.openwall.com/articles/PHP-Users-Passwords
 * Unless you force the use of "portable" hashes, phpass' preferred hashing
 * method is CRYPT_BLOWFISH, with a fallback to CRYPT_EXT_DES, and then a final
 * fallback to the "portable" hashes.
 * Default: FALSE
 */
$config['phpass_portable'] = true;

/**
 * What format BitAuth stores the date as. By default, BitAuth uses DATETIME
 * fields. If you want to store date as a unix timestamp, you just need to
 * change the columns in the database, and change this line:
 * $config['date_format'] = 'U';
 * See: http://php.net/manual/en/function.date.php
 */
$config['date_format'] = 'Y-m-d H:i:s';

/**
 * Your roles. These are how you call permissions checks in your code.
 * eg: if($this->bitauth->has_role('example_role'))
 * DO NOT reorder these once they have been assigned.
 */
$config['roles'] = array(

    /**
     * THE FIRST ROLE IS ALWAYS THE ADMINISTRATOR ROLE
     * ANY USERS IN GROUPS GIVEN THIS ROLE WILL HAVE FULL ACCESS
     */

    'super_admin' => 'Super Admin',
    'pos_user' => 'POS Cashier',
    'pos_user' => 'POS User',

    '90' => 'Settings',
        '91' => 'General Settings',
        '92' => 'Discount Settings',

    '100' => 'System Admins',
        '101' => 'View Users',
        '102' => 'Add Users',
        '103' => 'Edit Users',
        '104' => 'Delete Users',

    '200' => 'Inventory',
        '201' => 'Manage Inventory Item',
        '202' => 'Manage Categories',
        '203' => 'Manage Locations',
        '204' => 'Manage Stock Keep Units (SKU)',
        '205' => 'suppliers',
        '206' => 'invoices',
        '207' => 'Storeroom Requisitions',
        '208' => 'Inter-Dept Transfers',
        '209' => 'Quotations',
        '210' => 'Goods Receive Notes',
		'211' => 'Reports',
		'212' => 'Inventory Filter',

    '300' => 'Membership',
        '301' => 'Approved Member',
        '302' => 'Waiting For Approval',
        '303' => 'Manage Companies',

    '400' => 'Restaurant Menu',
        '401' => 'Extra Modifiers Groups',
        '402' => 'Extra Modifiers Items',
        '403' => 'Menu Category',
        '404' => 'Menu Items',
        '405' => 'Brand Category',

    '500' => 'Restaurant',
        '501' => 'Table Manage',
        '502' => 'Zone',
        '504' => 'Manage Custom Discount',

    '600' => 'Promotion',
        '601' => 'Card Promotion',
        '602' => 'Member Promotion',
        '603' => 'Happy Hour Promotion',

    '700' => 'On-Loan Items',
        '701' => 'View Summary',
        '702' => 'Lend items',
        '703' => 'View On-loan Inventory',

    '800' => 'Kitchens',
        '801' => 'View Ordered Items',
        '802' => 'View Reports',

    '900' => 'Reports',
        '901' => 'Sales Report - Items',
        '902' => 'Sales Report - Payment Methods',
        '903' => 'Sales Report - Cashiers',
        '904' => 'General Sales Report',
        '905' => 'Profit Analysis',
);


$config['sitemap'] = [
    [
        'name' => 'Dashboard',
        'role_slug' => '90',
        'nav_icon' => 'fa-home',
        'nav_class' => 'dashboard',
        'route' => base_url('/')
    ],
    [
        'name' => 'Settings',
        'role_slug' => '90',
        'nav_icon' => 'fa fa-cogs',
        'nav_class' => 'settings',
        'route' => '',
        'sub-menu' =>
            [
                [
                    'name' => 'General Settings',
                    'role_slug' => '91',
                    'nav_class' => 'general_settings',
                    'nav_icon' => '',
                    'route' => base_url('settings/general')
                ],
                [
                    'name' => 'Bill/Discount Settings',
                    'role_slug' => '92',
                    'nav_class' => 'discount_settings',
                    'nav_icon' => '',
                    'route' => base_url('settings/discount')
                ],
            ]
    ],
    [
        'name' => 'Membership',
        'role_slug' => '300',
        'nav_icon' => 'fa fa-list',
        'nav_class' => 'manage_menu',
        'route' => '',
        'sub-menu' =>
            [
                [
                    'name' => 'Approved Member',
                    'role_slug' => '301',
                    'nav_class' => 'member',
                    'nav_icon' => '',
                    'route' => base_url('member')
                ],
                [
                    'name' => 'Waiting For Approval',
                    'role_slug' => '302',
                    'nav_class' => 'waitingQueue',
                    'nav_icon' => '',
                    'route' => base_url('member/waitingQueue')
                ],
                [
                    'name' => 'Manage Companies',
                    'role_slug' => '303',
                    'nav_class' => 'company',
                    'nav_icon' => '',
                    'route' => base_url('member/company')
                ]

            ]
    ],
    [
        'name' => 'Restaurant Menu',
        'role_slug' => '400',
        'nav_icon' => 'fa fa-list',
        'nav_class' => 'manage_menu',
        'route' => '',
        'sub-menu' =>
            [
                [
                    'name' => 'Extra Modifiers Groups',
                    'role_slug' => '401',
                    'nav_class' => 'extra_category',
                    'nav_icon' => '',
                    'route' => base_url('restaurant/menu/extraModifiersCategory')
                ],
                [
                    'name' => 'Extra Modifiers Items',
                    'role_slug' => '402',
                    'nav_class' => 'extra_items',
                    'nav_icon' => '',
                    'route' => base_url('restaurant/menu/extraModifiersItem')
                ],
                [
                    'name' => 'Menu Category',
                    'role_slug' => '403',
                    'nav_class' => 'menu_category hide',
                    'nav_icon' => '',
                    'route' => base_url('restaurant/menu/category')
                ],
                [
                    'name' => 'Menu Items',
                    'role_slug' => '404',
                    'nav_class' => 'menu_items hide',
                    'nav_icon' => '',
                    'route' => base_url('restaurant/menu/item')
                ],
                [
                    'name' => 'Manage Menu',
                    'role_slug' => '404',
                    'nav_class' => 'menu',
                    'nav_icon' => '',
                    'route' => base_url('restaurant/menu')
                ],
                [
                    'name' => 'Brand Category',
                    'role_slug' => '405',
                    'nav_class' => 'menu_brands',
                    'nav_icon' => '',
                    'route' => base_url('restaurant/itemBrand')
                ]

            ]
    ],
    [
        'name' => 'Inventory',
        'role_slug' => '201',
        'nav_icon' => 'fa fa-glass',
        'nav_class' => 'inventory',
        'route' => '',
        'sub-menu' => [
            [
                'name' => 'Stock Location',
                'role_slug' => '202',
                'nav_class' => 'locations',
                'nav_icon' => '',
                'route' => base_url('inventory/inventoryStorageLocations')
            ]
            ,
            [
                'name' => 'Suppliers',
                'role_slug' => '203',
                'nav_class' => 'suppliers',
                'nav_icon' => '',
                'route' => base_url('inventory/manageSuppliersController')
            ],
            [
                'name' => 'Inventory Items',
                'role_slug' => '204',
                'nav_class' => 'items',
                'nav_icon' => '',
                'route' => base_url('inventory/manageInventoryItems')
            ],
            [
                'name' => 'Purchase Order',
                'role_slug' => '205',
                'nav_class' => 'purchase_invoice',
                'nav_icon' => '',
                'route' => base_url('inventory/manageInvoices')
            ],
            [
                'name' => 'Trade units',
                'role_slug' => '206',
                'nav_class' => 'trade_units',
                'nav_icon' => '',
                'route' => base_url('inventory/manageTradeUnits')
            ],
            [
                'name' => 'Storeroom Requisitions',
                'role_slug' => '207',
                'nav_class' => 'requisition',
                'nav_icon' => '',
                'route' => base_url('inventory/requisition/list_all')
            ],
            [
                'name' => 'Inter-Dept Item Transfers',
                'role_slug' => '208',
                'nav_class' => 'inter-dept-transfer',
                'nav_icon' => '',
                'route' => base_url('inventory/requisition/all_inter_transfers')
            ],
            [
                'name' => 'Goods Receive Notes',
                'role_slug' => '210',
                'nav_class' => 'goods_recive',
                'nav_icon' => '',
                'route' => base_url('inventory/manageGoodsReceive')
            ],
			[
				'name' => 'Reports',
				'role_slug' => '211',
				'nav_class' => 'inventory_filter',
				'nav_icon' => '',
				'route' => '',
				'sub-menu' => [
					[
						'name' => 'Inventory Filter',
						'role_slug' => '212',
						'nav_class' => 'inventory_filter',
						'nav_icon' => '',
						'route' => base_url('inventory/report/inventoryFilter')
					]
				]
			]
        ]
    ],
    [
        'name' => 'On-Loan Items',
        'role_slug' => '700',
        'nav_class' => 'on-loan',
        'nav_icon' => 'fa fa-cutlery',
        'route' => '',
        'sub-menu' => [
            [
                'name' => 'View Items',
                'role_slug' => '701',
                'nav_class' => 'on-loan-items',
                'nav_icon' => '',
                'route' => base_url('inventory/onloan/list_all')
            ],
            [
                'name' => 'Lend Items',
                'role_slug' => '702',
                'nav_class' => 'lend-items',
                'nav_icon' => '',
                'route' => base_url('inventory/onloan/list_all_transfers')
            ],
            [
                'name' => 'View Inventory',
                'role_slug' => '703',
                'nav_class' => 'view-onloan-inv',
                'nav_icon' => '',
                'route' => base_url('inventory/onloan/view_inventory')
            ]
        ]
    ],
    [
        'name' => 'Restaurant Zone',
        'role_slug' => '500',
        'nav_icon' => 'fa fa-asterisk',
        'nav_class' => 'restaurant',
        'route' => '',
        'sub-menu' => [
            [
                'name' => 'Tables Manage',
                'role_slug' => '501',
                'nav_class' => 'manage',
                'nav_icon' => '',
                'route' => base_url('manage')
            ]
        ]
    ],
    [
        'name' => 'Promotion',
        'role_slug' => '600',
        'nav_icon' => 'fa fa-bullhorn',
        'nav_class' => 'promotion',
        'route' => '',
        'sub-menu' => [
            [
                'name' => 'Card Promotion',
                'role_slug' => '601',
                'nav_class' => 'card_promotion',
                'nav_icon' => '',
                'route' => base_url('promotion/card')
            ],
            [
                'name' => 'Member Promotion',
                'role_slug' => '602',
                'nav_class' => 'member_promotion',
                'nav_icon' => '',
                'route' => base_url('promotion/member')
            ],
            [
                'name' => 'Happy Hour Promotion',
                'role_slug' => '603',
                'nav_class' => 'happy_hour_promotion',
                'nav_icon' => '',
                'route' => base_url('promotion/happyHour')
            ]

        ]

    ],
    [
        'name' => 'Reports',
        'role_slug' => '900',
        'nav_icon' => 'fa fa-line-chart',
        'nav_class' => 'reports',
        'route' => '',
        'sub-menu' => [
            [
                'name' => 'Sales - Items',
                'role_slug' => '901',
                'nav_class' => 'sales-items',
                'nav_icon' => '',
                'route' => base_url('reports/sales/items')
            ],
            [
                'name' => 'Sales - Payment Methods',
                'role_slug' => '902',
                'nav_class' => 'sales-pay-methods',
                'nav_icon' => '',
                'route' => base_url('reports/sales/payment_methods')
            ],
            [
                'name' => 'Sales - Cashier Shifts',
                'role_slug' => '903',
                'nav_class' => 'sales-cashiers',
                'nav_icon' => '',
                'route' => base_url('reports/sales/cashiers')
            ],
            [
                'name' => 'General Sales Report',
                'role_slug' => '904',
                'nav_class' => 'general_report',
                'nav_icon' => '',
                'route' => base_url('reports/general')
            ],

            [
                'name' => 'Profit Analysis',
                'role_slug' => '905',
                'nav_class' => 'purchase_and_sales',
                'nav_icon' => '',
                'route' => base_url('reports/profits')
            ]
        ]
    ]
];
