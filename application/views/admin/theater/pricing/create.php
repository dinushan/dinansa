
<script>

 window.timeslot        = <?php echo json_encode($time_slots); ?>;
 window.theater         = <?php echo json_encode($theaters); ?>;
 window.movie           = <?php echo json_encode($movie); ?>;
 window.assigned        = <?php echo json_encode($assigned); ?>;
 window.priceheader     = <?php echo json_encode($priceheader); ?>;
 window.globalcalander  = <?php echo json_encode($globalCalander);?>;
 window.categories      = <?php echo json_encode($categories); ?>;
 window.seatPlane       = <?php echo json_encode($seat_plane); ?>;
 window.blackouts       = <?php echo json_encode($blackouts); ?>;

</script>

<style>
    .icheck{
        width: 15px;
        height: 15px;
        background-color: #fff;
        color:#ccc;
        font-size: 5px;
    }
</style>

<script src="<?php echo base_url() ?>angular/admin/price_module/service/service.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>angular/admin/price_module/controller/controllerCTRL.js" type="text/javascript"></script>
<style>
    .portlet.box.bg-grey-steel  > .portlet-title {
        color :#0c0c0c;

    }

    .portlet.box.bg-grey-steel{
        border: 1px solid #3d3d3d;
        border-top: 0;
    }
</style>
<div class="page-container">

    <?php $this->load->view('admin/inc_sidebar_navigation.php');?> 

    <div class="page-content-wrapper">

		<div class="page-content" ng-controller="priceModuleController" ng-cloak id="container687">

            <div class="modal fade" tabindex="-1" role="dialog" id="updateSuccess011">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <p class="text-center text-success">Successfully Updated!</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-lg btn-default" ng-click="submitClose()">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" tabindex="-1" role="dialog" id="removeSuccess011">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <p class="text-center text-success">Item Removed!</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-lg btn-default" ng-click="removeClose()">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" tabindex="-1" role="dialog" id="updateError011">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <p class="text-center text-danger">Something went wrong!</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-lg btn-default" ng-click="submitClose()">Close</button>
                        </div>
                    </div>
                </div>
            </div>

            <div class="modal fade" tabindex="-1" role="dialog" id="warningError011">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-body">
                            <p class="text-center text-danger">{{warningMessage}}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-lg btn-default" ng-click="warningClose()" >Close</button>
                        </div>
                    </div>
                </div>
            </div>


            <h3 class="page-title"><?php echo $theaters['name'];?> > <?php echo $movie->movie_name;?></h3>
                <div class="form-horizontal">

                    <div class="page-bar">
                        <div class="page-toolbar">
                            <div class="btn-group pull-right">
                                <a onclick="setLocation('<?php echo site_url('admin/theater/price/movie/'.$this->encrypt_lib->encode($priceheader->theater_id)) ?>')" class="btn default"><i class="fa fa-chevron-left"></i> Back</a>
                            </div>
                        </div>
                    </div><!-- End Page bar -->

                    <?php if(strtotime($priceheader->booking_start_date) <= strtotime(date('Y-m-d'))):?>
                        <div class="form-body bg-red-pink">
                            <div class="container">
                            <h4 class="text-center">Booking Start date is past ! Changes are not applyed to the already booked users.</h4>
                            </div>
                        </div>
                    <?php endif; ?>


                    <h4 class="form-section">General</h4>

                    <div class="form-group">
                        <div class="control-label col-md-3"></div>
                        <div class="control-element col-md-4 bg-yellow-gold">
                            <p class="text-center">Please make sure before update, movie start and end dates sheduled !</p>
                            <p class="text-center">Movie Start Date: <b><?php echo date("Y-m-d",strtotime($priceheader->start_date)) ?></b> </p>
                            <p class="text-center">Movie End Date  : <b><?php echo date("Y-m-d",strtotime($priceheader->end_date)) ?></b></p>
                        </div>
                    </div>


                    <div class="form-group">
                        <div class="control-label col-md-3" for="DatePicker"></div>
                        <div class="control-element col-md-4">
                            <multiple-date-picker
                                    month="disableDaysBefore"
                                    disallow-back-past-months="false"
                                    change-year-future="5"
                                    ng-model="selectedDays"
                                    highlight-days="highlightDays"
                                    disable-days-before="today"
                                    disable-days-after="disableDaysAfter">

                            </multiple-date-picker>
                        </div>
                    </div>  



                     <h4 class="form-section">Time Slot</h4>

                    <div class="form-group">
                        <label class="control-label col-md-3">Show Times</label>

                        <div class="mt-checkbox-inline col-md-4">
                            <label class="mt-checkbox"  ng-repeat="slot in timeSlots">
                                <input ng-change="changeTimeslots()" type="checkbox" ng-model="slot.checked" > {{ slot.time_slot | date:'hh:mm a'}}
                                <span></span>
                            </label>
                        </div>

                    </div>

                    <h4 class="form-section">Price Categoris</h4>

                    <div class="form-group" ng-repeat="pm in priceCategories">
                        <label class="control-label col-md-3" for="DatePicker">{{pm.name}}</label>
                        <div class="control-element col-md-4">
                            <input ng-change="changePriceCategories()" type="checkbox" ng-model="pm.checked" />
                        </div>
                    </div>
                    <hr/>

                    <h4 class="form-section">Seats</h4>
                    <div class="form-group">
                        <label class="control-label col-md-3">Disabled Seats</label>

                        <div class="mt-checkbox-inline col-md-4">
                            <ui-select multiple ng-model="gloabalSeatplane.selected">
                                <ui-select-match placeholder="Select types...">
                                    <span ng-bind="$item.label"></span>
                                </ui-select-match>
                                <ui-select-choices repeat="seat in theaterSeatsPlane | filter: $select.search"">
                                <div ng-bind-html="item.name | highlight: $select.search"></div>
                                {{seat.label}}
                                </ui-select-choices>
                            </ui-select>
                        </div>

                    </div>
                    <hr/>

                    <h4>Week Plan</h4>

                    <ul class="nav nav-tabs" role="tablist">
                        <li ng-click="showTab($index)" role="presentation" ng-class="w.show ? 'active':''" ng-repeat=" w in weeks track by $index"><a href="">{{w.name}}</a></li>
                    </ul>

                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane" ng-class="weeks[$index].show?'active':''" ng-repeat="weekpl in weekPlane track by $index">

                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th></th>
                                        <th ng-repeat="cat in priceCategories">
                                            <input class="form-control icheck" type="checkbox" ng-model="cat.checked" />
                                            {{cat.name}}
                                        </th>
                                    </tr>
                                </thead>
                                <tbody>
                                <tr ng-repeat="ts in weekpl track by $index">
                                    <td>
                                        <input class="form-control icheck" type="checkbox" ng-model="ts.checked" ng-checked="ts.checked"/>
                                        {{ timeSlots[ts.time_slot].time_slot | date:'hh:mm a'}}
                                    </td>
                                    <td ng-repeat="cat in ts.categories">
                                        <div class="input-group">
                                            <div class="input-group-addon">Rs</div>
                                                <input type="text" class="form-control" ng-model="cat.amount" placeholder="Amount" ng-disabled="!cat.checked || !ts.checked"/>
                                        </div>
                                    </td>
                                </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
                <hr/>


                <div class="form-actions">
                    <div class="row">
                        <div class="col-md-offset-3 col-md-9">
                            <div class="btn-group">
                                <button type="submit" data-id="1" class="btn btn-submit green" ng-click="nextClick()">Go to Schedule</button>
                            </div>
                        </div>
                    </div>
                </div>

                    <!--?php endif; ?-->

                <br/>
                <div ng-if="theaterMovieObj.length > 0" class="form">
                    <div class="form-horizontal">

                        <div class="form-actions right">

                            <!--button-- class="btn green btn-sm" ng-click="setTimeSlotPopup()" ng-disabled="selectedCount < 1">
                                Set Time Slot
                            </button-->

                            <button class="btn blue btn-sm" ng-click="saveUpdate(false)">
                                Update
                            </button>
                            <?php if($this->adminauth->has_role('902')):?>
                            <button class="btn blue btn-sm" ng-click="saveUpdate(true)">
                                Update and Confirm <b class="font-red">(<?php echo $unconfirmed ?> pending)</b>
                            </button>
                            <?php endif; ?>
                        </div>
                    </div>
                 </div>


        <br/>
        <h4 class="form-section">Movie Schedule and Prices</h4>


                <div ng-repeat="twtobj in theaterMovieObj" class="portlet box" ng-class="twtobj.enable ? 'blue' :'bg-grey-steel'" id="tempresult678">
                    <div class="portlet-title">
                        <div class=""caption>
                            <div class="mt-checkbox-inline pull-left" style="padding-top:8px;">
                                <label class="mt-checkbox">
                                    <!--input type="checkbox" ng-model="twtobj.checked" ng-change="onChangeCheck()" ng-if="twtobj.enable" class="ng-valid ng-scope ng-not-empty ng-dirty ng-valid-parse ng-touched"-->
                                    {{twtobj.date |date :'yyyy MMM EEE dd'}}
                                </label>
                            </div>
                        </div>
                        <div class="tools">
                            <a href="javascript:;" class="collapse" data-original-title="" title=""> </a>
                        </div>
                    </div>
                    <div class="portlet-body" ng-if="twtobj.enable">
                        <table class="table table-striped table-bordered table-hover">

                            <tbody ng-repeat="objT in twtobj.dataobj track by $index">
                            <tr ng-class="set_bg_class(objT)">



                                <td style="width:20px;" ng-if="objT.enable">
                                     <i ng-if="objT.enable"  data-toggle="tooltip" data-placement="left" title="close" class="fa fa-times-circle-o time-slot-remove-btn" aria-hidden="true" ng-click="remove($index,objT,twtobj)"></i>
                                </td>



                                <td ng-if="!objT.enable">
                                    <b>{{objT.movie_name}}</b>
                                </td>

                                <td> {{setTimeSlot(objT.time_slot) | date:'hh:mm a'}}</td>
                                <td ng-repeat="pmdl in objT.pricemodule">
                                    <div class="form-group">
                                        <div class="control-element col-md-12" ng-if="objT.enable">

                                            <div class="input-group">
                                                <span class="input-group-addon">{{pmdl.name}}</span>
                                                <input type="number" class="form-control input-sm" ng-model="pmdl.value" />
                                            </div>
                                        </div>
                                        <div class="control-element col-md-12" ng-if="!objT.enable">

                                            <div class="input-group">
                                                <lable class="input-group-addon">{{pmdl.name}}</lable>
                                                <p class="form-control input-sm">{{pmdl.value}}</p>

                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td ng-if="objT.enable">
                                    <div class="form-group">
                                        <div class="control-element col-md-12">
                                            <span>Disabled Seats</span>
                                            <ui-select multiple ng-model="objT.onlineSeats.selected">
                                                <ui-select-match placeholder="Select types...">
                                                    <span ng-bind="$item.label"></span>
                                                </ui-select-match>
                                                <ui-select-choices repeat="seat in theaterSeatsPlane">
                                                    {{seat.label}}
                                                </ui-select-choices>
                                            </ui-select>
                                        </div>
                                    </div>
                                </td>
                                <td ng-if="!objT.enable">
                                    <div class="form-group">
                                        <div class="control-element col-md-12">
                                            <span ng-repeat="seats in objT.onlineSeats.selected">{{seats.label}}, </span>
                                        </div>
                                    </div>
                                </td>
                            </tr>

                            </tbody>
                        </table>
                    </div>



                    <div class="modal fade" tabindex="-1" role="dialog" id="removeAlert011">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                    <h4 class="modal-title">Confirmation</h4>
                                </div>

                                <div class="modal-body">
                                    <p class="font-red">Are you sure want to remove this Slot !</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-sm btn-default" ng-click="cancelRemove()">No</button>
                                    <button type="button" class="btn btn-sm btn-primary" ng-click="confirmRemove()">Yes</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->

                    <div class="modal fade" tabindex="-1" role="dialog" id="timeSlotPopup911">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>

                                </div>
                                <div class="modal-body">
                                    <div class="form-horizontal">
                                        <div class="form-group">
                                            <label class="control-label col-md-3" for="DatePicker"></label>
                                            <div class="mt-radio-list control-element col-md-4">
                                                <div class="mt-checkbox-inline pull-left" style="padding-top: 8px;">
                                                    <label class="mt-checkbox" ng-repeat="slot in timeSlots">
                                                        <input type="checkbox" ng-model="slot.checked"  />{{ slot.time_slot | date:'hh:mm a'}}
                                                    </label>
                                                </div>
                                            </div>
                                        </div>

                                        <div class="form-group" ng-repeat="pm in pricemodule">
                                            <label class="control-label col-md-3" for="DatePicker">{{pm.name}}</label>
                                            <div class="control-element col-md-4">
                                                <input type="number" class="form-control" ng-model="pm.value" />
                                            </div>
                                        </div>


                                        <div class="form-group">
                                            <label class="control-label col-md-3">Disabled Seats</label>

                                            <div class="mt-checkbox-inline col-md-4">
                                                <ui-select multiple ng-model="gloabalSeatplane.selected">
                                                    <ui-select-match placeholder="Select types...">
                                                        <span ng-bind="$item.label"></span>
                                                    </ui-select-match>
                                                    <ui-select-choices repeat="seat in theaterSeatsPlane">
                                                        {{seat.label}}
                                                    </ui-select-choices>
                                                </ui-select>
                                            </div>

                                        </div>

                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-sm btn-default" data-dismiss="modal">Cancel</button>
                                    <button type="button" class="btn btn-sm btn-primary" ng-click="applySelectedSlots()">Apply</button>
                                </div>
                            </div><!-- /.modal-content -->
                        </div><!-- /.modal-dialog -->
                    </div><!-- /.modal -->

                    <div class="portlet-body" ng-if="!twtobj.enable">
                        <table class="table table-striped table-bordered table-hover">

                            <tbody ng-repeat="objT in twtobj.dataobj track by $index">
                            <tr  ng-class="{'bg-red-pink':!objT.enable}">
                                <td>
                                    <div ng-if="!objT.enable" class="form-control-static">{{objT.movie_name}}</div>

                                    {{setTimeSlot(objT.time_slot) | date:'hh:mm a'}}
                                </td>
                                <td ng-repeat="pmdl in objT.pricemodule">
                                    <div class="form-group">
                                        <div class="control-element col-md-12">

                                            <div class="input-group">
                                                <lable class="input-group-addon">{{pmdl.name}}</lable>
                                                <p class="form-control input-sm">{{pmdl.value}}</p>

                                            </div>
                                        </div>
                                    </div>
                                </td>
                            </tr>
                            </tbody>
                        </table>
                    </div>

                </div>
                </div>

        </div>
     </div><!-- End Page Content-->









<script src="<?php echo base_url() ?>skin_admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<link href="<?php echo base_url() ?>skin_admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>

<script src="<?php echo base_url() ?>skin_admin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<link href="<?php echo base_url() ?>skin_admin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css"/>
<script>

(function($){


$('.datepicker').datepicker({
    format: "yyyy-mm",
    startView: "months", 
    minViewMode: "months"
});

$('.time').timepicker();


}($));

</script>