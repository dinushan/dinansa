<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer">
    <div class="row">
        <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 messageBoxWrap errorMessageBoxWrap">
            <h2 class="title">Booking Error</h2>

            <span class="errorIcon"></span>
            <div class="confirmedMessage">
                <span class="name">Scope Cinemas</span> Your booking is not completed.
            </div>
            <div class="errorMessage"><?php print_r($err_msg) ?></div>
            <div class="referenceNumber"></div>
        </div>
    </div>
</div>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>

