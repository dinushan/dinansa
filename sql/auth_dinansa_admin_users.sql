/*
Navicat MySQL Data Transfer

Source Server         : My Local
Source Server Version : 50625
Source Host           : localhost:3306
Source Database       : hms_elevate

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2018-01-17 22:04:07
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_restaurant_admin_users
-- ----------------------------
DROP TABLE IF EXISTS `auth_restaurant_admin_users`;
CREATE TABLE `auth_restaurant_admin_users` (
  `user_id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(32) NOT NULL,
  `password` varchar(60) NOT NULL,
  `password_last_set` datetime NOT NULL,
  `password_never_expires` tinyint(1) NOT NULL DEFAULT '0',
  `remember_me` varchar(40) DEFAULT NULL,
  `activation_code` varchar(40) NOT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `forgot_code` varchar(40) DEFAULT NULL,
  `forgot_generated` datetime DEFAULT NULL,
  `enabled` tinyint(1) NOT NULL DEFAULT '1',
  `last_login` datetime DEFAULT NULL,
  `last_login_ip` varchar(15) DEFAULT NULL,
  PRIMARY KEY (`user_id`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auth_restaurant_admin_users
-- ----------------------------
INSERT INTO `auth_restaurant_admin_users` VALUES ('1', 'admin', '$P$BM3OxkiihioWFyWXqv1DT/oTR6Wv2G0', '2016-03-01 16:34:26', '1', 'b2a342e35457e57acd0eb8af8c60d8b7a790aaa9', '', '1', '', '2015-10-20 16:11:47', '1', '2018-01-11 13:06:01', '112.134.141.30');
INSERT INTO `auth_restaurant_admin_users` VALUES ('2', 'rmg', '$P$Bw8iiT3GhbUyhkDkkPWIR95ZqEUzHY/', '2017-11-04 08:31:43', '0', null, '', '1', null, null, '1', null, null);
INSERT INTO `auth_restaurant_admin_users` VALUES ('3', 'sameera', '$P$B6pA/eJzw30gKx0UGNFsETMM/Ik7.O.', '2017-11-04 08:32:55', '0', null, '', '1', null, null, '1', null, null);
INSERT INTO `auth_restaurant_admin_users` VALUES ('4', 'chef', '$P$BD7EOFph/E29BFx/J5FS2ia3Rb8InX/', '2017-11-14 12:51:27', '0', null, '', '1', null, null, '1', '2017-12-11 09:01:34', '124.43.124.99');
INSERT INTO `auth_restaurant_admin_users` VALUES ('5', 'Edward', '$P$B5f3cs4yb4eLwL7P0F3IJcVXbzIA600', '2017-11-17 06:49:11', '0', null, '', '1', null, null, '1', null, null);
INSERT INTO `auth_restaurant_admin_users` VALUES ('6', 'Kitchen', '$P$BaDGYTrUmLBu3dVahHMeXSIBo54Puv.', '2017-11-24 05:20:10', '0', null, '', '1', null, null, '1', '2017-12-11 09:06:24', '124.43.124.99');
INSERT INTO `auth_restaurant_admin_users` VALUES ('7', 'januka', '$P$BdR/VEZJqTcQWWUoHUnlnjqrxOjdGW/', '2018-01-03 06:40:44', '0', null, '', '1', null, null, '1', '2018-01-08 04:23:16', '124.43.121.59');
