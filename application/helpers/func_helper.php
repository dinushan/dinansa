<?php


if (!function_exists('no_access')) {
    function no_access()
    {
        $CI = &get_instance();

        $pageData['slug'] = 'no_access';
        $pageData['title'] = 'No Access';

        //$CI->load->view('admin/inc_head',$pageData);
        //$CI->load->view('admin/no_access',$pageData);
        //$CI->load->view('admin/inc_footer');
    }
}

if (!function_exists('form_multiple_checkbox')) {
    function form_multiple_checkbox($name = '', $options = array(), $selected = array(), $extra = '')
    {


        if (!is_array($selected)) {
            $selected = array($selected);
        }

        // If no selected state was submitted we will attempt to set it automatically
        if (count($selected) === 0) {
            // If the form name appears in the $_POST array we have a winner!
            if (isset($_POST[$name])) {
                $selected = array($_POST[$name]);
            }
        }

        if ($extra != '') $extra = ' ' . $extra;

        $form = '<ul' . $extra . '>' . "\n";
        foreach ($options as $key => $val) {
            $key = (string)$key;

            if ($key == '10' || $key == '90' || $key == '100' || $key == '200' ||
                $key == '300' || $key == '400' ||
                $key == '500' || $key == '600' ||
                $key == '700' || $key == '800' ||
                $key == '900' || $key == '1000' ||
                $key == '1100' || $key == '1200' || $key == '1300'
            ) {
                $form .= '<li class="t"><br /><strong>';
                $form .= (string)$val;
                $form .= '</strong></li>' . "\n";
            } else {
                $sel = (in_array($key, $selected)) ? TRUE : FALSE;
                $form .= '<li><label>' . form_checkbox($name, $key, $sel) . (string)$val . "</li>\n";
            }

        }

        $form .= '</ul>' . "\n";
        return $form;
    }
}


if (!function_exists('get_message')) {
    function get_message($v = '')
    {
        $CI = &get_instance();
        return $CI->encrypt_lib->decode($CI->session->flashdata('message' . $v));
    }
}
if (!function_exists('set_message')) {
    function set_message($message, $v = '')
    {
        $CI = &get_instance();
        $data = array(
            'message' . $v => $message
        );

        $encoded = $CI->encrypt_lib->encode($message);
        $CI->session->set_flashdata('message' . $v, $encoded);
    }
}
if (!function_exists('unset_message')) {
    function unset_message()
    {
        $CI = &get_instance();
        $data = array(
            'message' => ''
        );
        $CI->session->set_userdata($data);
    }
}


function loadTableData($tbl = "", $key = "", $cols = [], $where = [])
{
    $ci =& get_instance();
    $ci->load->database();
    $ci->db->select($key . "," . join(",", $cols));
    $results = $ci->db->get_where($tbl, $where);
    $output = [];
    foreach ($results->result() as $value) {
        $output[] = (array)$value;
    }
    return $output;
}

function createLeftMenu()
{

    $ci =& get_instance();
    $html = '';
    //$html = '<ul id="nav" class="page-sidebar-menu" data-auto-scroll="true" data-slide-speed="200">';


    //print_r($ci->adminauth->config->config['adminauth']['sitemap']);
    foreach ($ci->adminauth->config->config['adminauth']['sitemap'] as $sitemap) {

        $nav_name = $sitemap['name'];
        $nav_class = $sitemap['nav_class'];
        $nav_icon = $sitemap['nav_icon'];
        $route = $sitemap['route'];

        if (isset($sitemap['sub-menu']) && count($sitemap['sub-menu']) > 0) {

            $sub_nav = '';
            $arr_role_slug = [];
            foreach ($sitemap['sub-menu'] as $submenu) {

                if ($ci->adminauth->has_role($submenu['role_slug'])) {
                    $sub_nav .= '<li class="' . $submenu['nav_class'] . '"><a
                                        href="' . $submenu['route'] . '"><span>' . $submenu['name'] . '</span>';

                    //Add arrow for level_3
                    if (isset($submenu['sub-menu']) && count($submenu['sub-menu'])) {
                        $sub_nav .= '<span class="arrow"></span>';
                    }
                    $sub_nav .= '</a>';

                    //Set menu item for level_3
                    if (isset($submenu['sub-menu']) && count($submenu['sub-menu'])) {
                        $sub_nav .= '<ul class="sub-menu">';
                        foreach ($submenu['sub-menu'] as $leval_3) {
                            if ($ci->adminauth->has_role($leval_3['role_slug'])) {
                                $sub_nav .= '<li>
                                                <a href="' . $leval_3['route'] . '">' . $leval_3['name'] . '</a>
                                             </li>';
                            }
                        }
                        $sub_nav .= '</ul>';
                    }
                    $sub_nav .= '</li>';
                }

            }

            if ($sub_nav != '') {

                $html .= '<li class="' . $nav_class . '">
                    <a href="javascript:;">
                        <i class="fa ' . $nav_icon . '"></i><span class="title">' . $nav_name . '</span><span class="arrow "></span>
                    </a><ul class="sub-menu">' . $sub_nav . '</ul>';
            }


        }
        if (!isset($sitemap['sub-menu']) || (isset($sitemap['sub-menu']) && count($sitemap['sub-menu']) == 0)) {
            if (isset($sitemap['role_slug']) && $ci->adminauth->has_role($sitemap['role_slug'])) {
                $html .= '<li class="' . $nav_class . '">
                    <a href="' . $route . '">
                        <i class="fa ' . $nav_icon . '"></i><span class="title">' . $nav_name . '</span>
                    </a>';
            }
        }


        $html .= '</li>';
    }

    //$html .= '</ul>';

    echo $html;
}

/*function isActiveMenu($route=""){
    $ci=& get_instance();
    return ($route == $ci->uri->segment(1))? true : false;
}*/

function isActiveMenu($route = "")
{
    $ci =& get_instance();
    if (count(explode("/", $route)) > 1) {
        return ($route == ($ci->uri->segment(1) . "/" . $ci->uri->segment(2))) ? true : false;
    } else {
        return ($route == $ci->uri->segment(1)) ? true : false;
    }

}

function encode($value)
{
    $ci =& get_instance();
    return $ci->encrypt_lib->encode($value);
}

function decode($value)
{
    $ci =& get_instance();
    return $ci->encrypt_lib->decode($value);
}


function set_radio_no_post($field, $value = '', $default = FALSE)
{
    $CI =& get_instance();

    if (isset($CI->form_validation) && is_object($CI->form_validation) && $CI->form_validation->has_rule($field)) {
        return $CI->form_validation->set_radio($field, $value, $default);
    }

    // Form inputs are always strings ...
    $value = (string)$value;
    $input = $CI->input->post($field, FALSE);

    if (is_null($input) && $default) {
        return ' checked="checked"';
    }

    if (is_array($input)) {
        // Note: in_array('', array(0)) returns TRUE, do not use it
        foreach ($input as &$v) {
            if ($value == $v) {
                return ' checked="checked"';
            }
        }

        return '';
    }

    // Unchecked checkbox and radio inputs are not even submitted by browsers ...
    if (count($_POST) > 0) {
        return ($input == $value) ? ' checked="checked"' : '';
    }

    return ($default == TRUE) ? ' checked="checked"' : '';
}

function set_select_no_post($field, $value = '', $default = FALSE)
{
    $CI =& get_instance();

    if (isset($CI->form_validation) && is_object($CI->form_validation) && $CI->form_validation->has_rule($field)) {
        return $CI->form_validation->set_select($field, $value, $default);
    } elseif (($input = $CI->input->post($field, FALSE)) == NULL) {
        return ($default == TRUE) ? ' selected="selected"' : '';
    }

    if (is_null($input) && $default) {
        return ' checked="checked"';
    }

    $value = (string)$value;
    if (is_array($input)) {
        // Note: in_array('', array(0)) returns TRUE, do not use it
        foreach ($input as &$v) {
            if ($value == $v) {
                return ' selected="selected"';
            }
        }

        return '';
    }

    return ($input == $value) ? ' selected="selected"' : '';

}

function preg_grep_keys($pattern, $input, $flags = 0)
{
    return array_filter($input, function ($key) use ($pattern, $flags) {
        return preg_match($pattern, $key, $flags);
    }, ARRAY_FILTER_USE_KEY);
}

function getRefId($id)
{
    return ($id - 7777);
}

function setRefId($id)
{
    return ($id + 7777);
}

function currencyFormat($value)
{
    //return $value;
    return "Rs. " . number_format($value);
}


