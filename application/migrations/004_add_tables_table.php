<?php

class Migration_Add_Tables_Table extends CI_Migration
{
    public function up()
    {


        $this->dbforge->add_field(
            array(
                'id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => true,
                    'auto_increment' => true
                ),
                'label' => array(
                    'type' => 'VARCHAR',
                    'constraint' => '100',
                ),
                'zone_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => true,
                ),
                'restaurant_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => true,
                    'null' => true,
                ),
                'floor_id' => array(
                    'type' => 'INT',
                    'constraint' => 11,
                    'unsigned' => true,
                    'null' => true,
                ),
                'data' => array(
                    'type' => 'TEXT',
                    'null' => true,
                ),
                'created_at' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
                'updated_at' => array(
                    'type' => 'DATETIME',
                    'on update' => 'NOW()',
                    'null' => true,
                ),
                'deleted_at' => array(
                    'type' => 'DATETIME',
                    'null' => true,
                ),
            )
        );

        $this->dbforge->add_key('id', true);
        $this->dbforge->create_table('restaurant_tables');
    }

    public function down()
    {
        $this->dbforge->drop_table('restaurant_tables');
    }
}
