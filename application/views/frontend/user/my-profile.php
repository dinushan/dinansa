<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

   <div class="container pageContainer mainPageContainer myprofilePage">
      <div class="row">
         <div class="col-xs-12 breadcrumbCont">
            <div class="breadcrumbWrap">
               <ol class="breadcrumb">
                  <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                  <li class="active"><a href="#">My Profile</a></li>
               </ol>
            </div>
            <div class="socialWrap">
               <ul class="social-links">
                  <li><a href="#" class="facebook"></a></li>
                  <li><a href="#" class="twitter"></a></li>
                  <li><a href="#" class="pinterest"></a></li>
                  <li><a href="#" class="google"></a></li>
                  <li><a href="#" class="instragram"></a></li>
               </ul>
            </div>
         </div>
         <div class="col-xs-12 col-md-8 col-md-offset-2 col-lg-6 col-lg-offset-3 myProfileCont">
            <div class="myProfileWrap">
               <?php
               $COUNTRIES = unserialize(COUNTRIES);
               echo get_message();
               $attributes = array('id' => 'frm-contact', 'class'=>'form-horizontal myProfileForm');
               echo form_open(current_url(), $attributes);
               ?>
                  <div class="form-group">
                     <label for="email" class="col-sm-4 col-md-3 control-label">Email</label>
                     <div class="col-sm-8 col-md-9">
                        <input type="email" class="form-control input-scope" value="<?php echo set_value('email',$userData->email) ?>" name="email" placeholder="Email">
                        <?php echo form_error('email'); ?>
                     </div>
                  </div>
                  <div class="form-group nameGroup">
                     <label for="name" class="col-sm-4 col-md-3 control-label">Name</label>
                     <div class="col-sm-8 col-md-9">
                        <div class="nameInputWrap">
                           <input type="text" class="form-control input-scope" value="<?php echo set_value('first_name',$userData->first_name) ?>" name="first_name" placeholder="Shone">
                           <?php echo form_error('first_name'); ?>
                        </div>
                        <div class="nameInputWrap">
                           <input type="text" class="form-control input-scope" value="<?php echo $userData->last_name; ?>" name="last_name" placeholder="Martin">
                           <?php echo form_error('last_name'); ?>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="name" class="col-sm-4 col-md-3 control-label">Gender</label>
                     <div class="col-sm-8 col-md-9">
                        <ul class="genderList">
                           <li>
                              <input type="radio" <?php if($userData->gender == 'male'){ echo 'checked';} ?> name="gender" id="male" value="male">
                              <label for="male">Male</label>
                              <div class="check"></div>
                           </li>
                           <li>
                              <input type="radio" <?php if($userData->gender == 'female'){ echo 'checked';} ?> name="gender" id="female" value="female">
                              <label for="female">Female</label>
                              <div class="check"></div>
                           </li>
                        </ul>
                     </div>
                  </div>
                  <div class="form-group calendarWrap">
                     <label for="dateOfBirth" class="col-sm-4 col-md-3 control-label">Date Of Birth</label>
                     <div class="col-sm-8 col-md-9">
                        <div class="selectDateWrapper">
                           <input type="text" name="dob" id="dob" value="<?php echo $userData->dob; ?>" class="form-control input-scope" placeholder="Select date of birth">
                           <?php echo form_error('dob'); ?>
                           <span class="icon calendar-icon"></span>
                        </div>
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="nicPasport" class="col-sm-4 col-md-3 control-label">NIC/pasport</label>
                     <div class="col-sm-8 col-md-9">
                        <input type="text" class="form-control input-scope" value="<?php echo $userData->nic_or_passport; ?>" name="nic_or_passport" placeholder="111111111V">
                        <?php echo form_error('nic_or_passport'); ?>
                     </div>
                  </div>
                  <div class="form-group calendarWrap">
                     <label for="country" class="col-sm-4 col-md-3 control-label">Country</label>
                     <div class="col-sm-8 col-md-9">
                        <select name="country" class="form-control input-scope">
                           <option value="">Select Country</option>
                           <?php
                           foreach ($COUNTRIES as $index=>$value) {
                              ?>
                              <option <?php if ($userData->country == $value) {echo 'selected';} ?> value="<?php echo $value ?>"><?php echo $index ?></option>
                              <?php
                           }
                           ?>
                        </select>
                     </div>
                     <?php echo form_error('country'); ?>
                  </div>
               <div class="form-group">
                  <label for="nicPasport" class="col-sm-4 col-md-3 control-label">City</label>
                  <div class="col-sm-8 col-md-9">
                     <input type="text" class="form-control input-scope" value="<?php echo $userData->home_city; ?>" name="city" placeholder="Colombo">
                     <?php echo form_error('city'); ?>
                  </div>
               </div>
                  <div class="form-group">
                     <label for="address" class="col-sm-4 col-md-3 control-label">Address</label>
                     <div class="col-sm-8 col-md-9">
                        <input type="text" class="form-control input-scope" value="<?php echo set_value('address',$userData->home_address_line_1) ?>" name="address" placeholder="123 Street, Colombo 03">
                        <?php echo form_error('address'); ?>
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="phone" class="col-xs-12 col-sm-4 col-md-3 control-label">Phone</label>
                     <div class="col-xs-8 col-sm-4 col-md-6">
                        <input type="text" readonly value="<?php echo $userData->phone; ?>" class="form-control input-scope" name="phone" placeholder="077711111111">
                     </div>
                     <div class="col-xs-4 col-sm-4 col-md-3">
                        <a href="<?php echo site_url('user/mobile_number_change') ?>" >
                        <button type="button" class="btn btn-scope btn-read-more hidden-xs">CHANGE</button>
                        <i class="fa fa-pencil btn btn-edit visible-xs" aria-hidden="true"></i>
                        </a>
                     </div>
                  </div>

                  <div class="form-group">
                     <div class="col-sm-offset-4 col-sm-8 col-md-offset-3 col-md-9">
                        <button type="button" class="btn btn-scope btn-read-more change_password">CHANGE PASSWORD</button>
                     </div>
                  </div>

               <div class="password_box" style="display: none;">
                  <div class="form-group">
                     <label for="phone" class="col-sm-4 col-md-3 control-label">New Password</label>
                     <div class="col-sm-8 col-md-9">
                        <input type="password" class="form-control input-scope" name="new_password" placeholder="New Password">
                     </div>
                  </div>
                  <div class="form-group">
                     <label for="phone" class="col-sm-4 col-md-3 control-label">Confirm New Password</label>
                     <div class="col-sm-8 col-md-9">
                        <input type="password" class="form-control input-scope" name="confirm_password" placeholder="Confirm New Password">
                     </div>
                  </div>
               </div>

                  <div class="form-group">
                     <div class="col-sm-offset-4 col-sm-8 col-md-offset-3 col-md-9">
                        <button type="submit" class="btn btn-lg btn-block btn-blue">UPDATE</button>
                     </div>
                  </div>
               </form>
            </div>
         </div>
      </div>
   </div>

<script>
   $('#dob').datepicker({
      changeMonth: true,
      changeYear: true,
      showButtonPanel: true,
      yearRange: '-100:-16',
      dateFormat:'yy-mm-dd',
      defaultDate:'1990-01-01',
    beforeShow:function(input, ins) {
            datePickerBeforeShow();
    },
    onClose:function(input, ins) {
            datePickerOnClose(input);
    }
   }).focus(function () {
      $(this).blur()
   }).datepicker('widget').wrap('<div class="ll-skin-melon dateOfBirth"/>');

   $(document).ready(function() {
      $('.change_password').click(function() {
         $('.password_box').slideToggle("fast");
      });
   });

   /*var jvalidate = $("#frm-contact").validate({
      ignore: [],
      rules: {
         first_name: {
            required: true
         },
         last_name: {
            required: true
         },
         email: {
            required: true,
            email: true
         },
         dob: {
            required: true
         },
         address: {
            required: true
         }
      }
   });*/
</script>



<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>