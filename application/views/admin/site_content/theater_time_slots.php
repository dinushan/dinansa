<!-- BEGIN CONTAINER -->

<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<?php
		$week_segment = $this->uri->segment(3);
		$attr = array("id"=>"edit_form","class"=>"form-horizontal","enctype"=>"multipart/form-data");
		echo form_open(current_url(),$attr);
		?>
		<div class="page-content">
			<?php if($page_action == ''):?>
			<h3 class="page-title">Theater Time Slots</h3>
			<div class="page-bar">
				<div class="page-toolbar">
				</div>
			</div>
			<?php echo get_message() ?>



				<!-- Tab panes -->
				<div class="tab-content">
						<div role="tabpanel" class="tab-pane active" id="monday">
							<div class="row">
								<div class="col-md-12">
									<div class="table-responsive">
										<table id="tbl"
											   class="table table-striped table-bordered table-advance table-hover">
											<thead>
											<tr class="headings nodrop nodrag">
												<th>Time Slots</th>
											</tr>
											</thead>

											<tbody>

											<?php foreach ($arr_time_slot as $row):
													?>
													<tr class="week_row">
														<td>
                                                            <input type="hidden" name="time_slot_id[]" value="<?php echo $row['id'] ?>" />
                                                            <input type="text" name="time_slot[]" class="form-control time" placeholder="Select Time" value="<?php echo date("h:i A", strtotime($row['time_slot'])); ?>"/></td>
													</tr>
													<?php
												endforeach;
											?>
											</tbody>
										</table>
									</div>
								</div>
							</div>

							<div class="full_row clearfix">
								<input type="hidden" id="row_count" name="row_count"/>
								<button type="submit" name="submit" value="submit" class="btn green pull-right">Submit
								</button>
								<button type="button" id="del_row" name="del_row"
										class="btn btn-danger btn-row-del pull-right"><i class="fa fa-minus-circle"></i>
									Delete Row
								</button>
								<button type="button" id="add_row" name="add_row" class="btn btn green pull-right"><i
										class="fa fa-plus-circle"></i> Add Row
								</button>
							</div>
						</div>

						<script>
							$('.time').timepicker({
								defaultTime: false
							});
							var rowCount=$('.week_row').size();
							$("#row_count").val(rowCount);


							jQuery(function($){
								var $button_kf = $('#add_row');

								$button_kf.click(function(){

									var newRow = '<tr class="week_row">'+
										'<td><input type="text" name="new_time_slot[]"  class="form-control time" placeholder="Select Time"  />'+
										'</tr>';
									$(newRow).insertAfter('.week_row:last-child');
									$('.time').timepicker({
										defaultTime: false
									});

									rowCount = $('.week_row').size();
									$("#row_count").val(rowCount);

								});

								$("#del_row").click(function(){

									if($('.week_row').size()>1){
										$('.week_row').last().remove();
									}

									rowCount=$('.week_row').size();
									$("#row_count").val(rowCount);
								});

							});
						</script>

				</div>
			<?php endif;//$page_action ?>
		</div>
		<!-- END PAGE CONTENT--> 
	</div>
	</form>
	<!-- END CONTENT --> 
	
</div>
<!-- END CONTAINER -->



