<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;
/**
 * Created by PhpStorm.
 * User: LAYOUTindex
 * Date: 10/16/2017
 * Time: 8:45 PM
 */
class Subject extends Eloquent
{
    protected $table        = "subjects";
    protected $primaryKey   = "id";

}