/**
 * Created by LAYOUTindex on 12/4/2017.
 */
(function(window) {
    var app = angular.module('restaurant', []);

    app.factory('reportService',function($window,$http){

        var locations = [],itmes = [],categories =[];

        var selecttype = function (type,callback){

            if(type == -1){
                return;
            }



            $http({
                'method':'POST',
                'url':$window.filterurl+'/selectType',
                'headers':{'Content-Type':'application/x-www-form-urlencoded; charset=UTF-8'},
                'data':$.param({
                    'type'     : type
                })
            }).then( function (response){
                callback(response.data);
            })
            .catch(function (err) {

            });


        };

        var selectLocation = function (type,loction,callback) {


            $http({
                'method':'POST',
                'url':$window.filterurl+'/selectLocations',
                'headers':{'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8'},
                'data':$.param({
                    'type'     : type,
                    'location' : loction,
                })
            })
            .then(function(response){
                    callback(response.data);
            })
            .catch(function (err) {

            });


        };

        var selectCategory = function (type,loction,catid,callback){

            $http({
                'method':'POST',
                'url':$window.filterurl+'/selectCategory',
                'headers':{'Content-Type':'application/x-www-form-urlencoded;charset=UTF-8'},
                'data':$.param({
                    'type'     : type,
                    'location' : loction,
                    'category' : catid,
                })
            })
            .then(function(response){
                callback(response.data);
            })
            .catch(function (err) {

            });
        };

        var getItems = function (){
            return itmes;
        };

        var getCategories = function (){
            return categories;
        };

        var getLocations = function (){
            return locations;
        };

        return {
           'getItems'      :getItems,
           'getCategories' :getCategories,
           'getLocations'  :getLocations,
           'selecttype'    :selecttype,
           'selectLocation':selectLocation,
           'selectCategory':selectCategory

        };
    });
}(window));