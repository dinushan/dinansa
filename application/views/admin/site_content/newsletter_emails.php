<!-- BEGIN CONTAINER -->

<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<?php if($page_action == ''):?>
			<h3 class="page-title">Newsletter Emails</h3>
			
			<div class="page-bar">				
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<a onclick="setLocation('<?php echo site_url('admin/site_content/newsletter_emails/download_csv') ?>')" class="btn green"><i class="fa fa-plus-circle"></i> Download CSV</a>
					</div>
				</div>
			</div>
			
			
			<?php echo get_message() ?>
			<div class="row">
				<div class="col-md-12">
					<div class="table-scrollable">
						
						<table id="tbl_list" class="table table-striped table-bordered table-advance table-hover">
						  <colgroup>
						  <col>
						  <col width="300">
						  </colgroup>
						  <thead>
							<tr class="headings nodrop nodrag">
							  <th>Emails</th>
							  <th>Action</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
										foreach($emailList as $row):
											$email = $row['email'];
											$encry_hcId = $this->encrypt_lib->encode($email);
										?>
							<tr id="<?php echo $encry_hcId ?>">
							  <td><strong><?php echo $row['email']; ?></strong></td>
							  <td>
								<button class="btn btn-sm red btn-xs" type="button" onclick="deleteConfirm('Are you sure you want to delete?','Confirm Delete',
								'<?php echo site_url('admin/site_content/'.$slug.'/delete/'.$encry_hcId) ?>');return false;"><i class="fa fa-trash-o"></i>
								Delete</button>
							</tr>                    
							<?php endforeach; ?>
						  </tbody>
						</table>
					</div>
				</div>
			</div>

			<?php endif;//$page_action ?>
		</div>
		<!-- END PAGE CONTENT--> 
	</div>
	<!-- END CONTENT --> 
	
</div>
<!-- END CONTAINER --> 
