<?php include(APPPATH . 'views/frontend/inc/head.php'); ?>
<?php include(APPPATH . 'views/frontend/inc/header.php'); ?>

    <div class="container pageContainer mainPageContainer">
        <div class="row">
            <div class="col-xs-12 breadcrumbCont">
                <div class="breadcrumbWrap">
                    <ol class="breadcrumb">
                        <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                        <li><a href="<?php echo base_url('user/register'); ?>">Registration</a></li>
                    </ol>
                </div>
            </div>
            <div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 registerCont">
                <h4 class="registerDesc">Fill below form to Register</h4>

                <h2 class="registerTitle">Register</h2>
                <?php if ($steps == 1): ?>

                    <div class="registerWrap">

                        <div id="message_signUp"><?php echo get_message(); ?></div>
                        <?php
                        $attributes = array('id' => 'frm-signup', 'class' => 'form-type-1');
                        echo form_open(getCurrentUrl(), $attributes);
                        ?>
                        <div class="form-group">
                            <label for="first_name">First Name</label>
                            <input type="text" class="form-control input-scope" name="first_name" id="first_name"
                                   value="<?php echo set_value('first_name') ?>">
                            <?php echo form_error('first_name'); ?>
                        </div>
                        <div class="form-group">
                            <label for="last_name">Last Name</label>
                            <input type="text" class="form-control input-scope" name="last_name" id="last_name"
                                   value="<?php echo set_value('last_name') ?>">
                            <?php echo form_error('last_name'); ?>
                        </div>
                        <div class="form-group">
                            <label for="email">E-mail</label>
                            <input type="email" class="form-control input-scope" name="email" id="email"
                                   value="<?php echo set_value('email') ?>">
                            <?php echo form_error('email'); ?>
                        </div>

                        <div class="form-group">
                            <input type="hidden" name="s" value="<?php echo $this->session->session_id ?>">
                            <input type="hidden" name="action" value="signUp"/>
                            <button type="submit" onclick="userRegistrationSubmit()" class="btn btn-lg btn-block btn-blue loginBtn">Next</button>
                        </div>
                        </form>
                    </div>

                    <div class="form-group fbBtnWrap">
                        <a href="<?php echo base_url('auth_oa2/session/facebook') ?>"
                           class="fbBtn btn btn-lg btn-primary btn-block ">
                            <span class="fbIcon"></span>
                            <span class="fbBtnText">Login with facebook</span>
                        </a>
                    </div>

                    <div class="signInCont">
                        <div class="signInWrap">Existing User <a class="signInLink"
                                                                 href="<?php echo base_url('user/login') ?>"> SIGN
                                IN</a></div>
                    </div>

                <?php elseif ($steps == 2): ?>
                    <div class="registerWrap changeMobileCont">
                        <div class="changeMobileWrap">
                            <div class="boxHeadWrap">
                                <h2 class="boxTitle">Enter your 10 digit mobile number below to get started</h2>
                            </div>

                            <div class="formWrap">

                                <div id="message_signUp"><?php echo get_message(); ?></div>
                                <?php
                                $attributes = array('id' => 'frm-signup','name' => 'user_mobile_verify', 'class' => 'form-type-1 user_mobile_verify');
                                echo form_open(getCurrentUrl(), $attributes);
                                ?>
                                <div class="form-group">
                                    <input type="text" class="form-control input-scope" placeholder="07xxxxxxxx"
                                           name="phone" id="phone" value="<?php echo $phone; ?>">
                                    <?php echo form_error('phone'); ?>
                                </div>

                                <div class="form-group">
                                    <input type="hidden" name="s" value="<?php echo $this->session->session_id ?>">
                                    <input type="hidden" name="action" value="signUp-mobile-validation"/>
                                    <button type="submit" onclick="userMobileVerify()" class="btn btn-lg btn-block btn-blue loginBtn">VERIFY MOBILE
                                        NUMBER
                                    </button>
                                </div>
                                </form>
                            </div>

                        </div>
                    </div>

                <?php elseif ($steps == 21): ?>
                <div class="registerWrap changeMobileCont">
                    <div class="changeMobileWrap">

                        <div class="boxHeadWrap">
                            <h2 class="boxTitle">Enter your Verification Code sent to your mobile number</h2>
                            <!--   <div class="boxDesc">
                              <p>4 digit number you must have received on your mobile via SMS</p>
                              </div> -->
                        </div>

                        <div class="formWrap">
                            <div id="message_signUp"><?php echo get_message(); ?></div>
                            <?php
                            $attributes = array('id' => 'frm-signup', 'class' => 'form-type-1');
                            echo form_open(getCurrentUrl(), $attributes);

                            ?>
                            <div class="form-group">
                                <input type="text" class="form-control input-scope" name="opt_code" id="opt_code"
                                       placeholder="4-digit code" value="">
                                <?php echo form_error('opt_code'); ?>
                            </div>

                            <div class="form-group">
                                <input type="hidden" name="s" value="<?php echo $this->session->session_id ?>">
                                <input type="hidden" name="action" value="signUp-mobile-confirm-validation"/>
                                <button type="submit" class="btn btn-lg btn-block btn-blue loginBtn">CONFIRM
                                    VERIFICATION
                                </button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
                <?php endif; ?>

            </div>
        </div>
    </div>

    <script>
        $('#phone').keyup(function () {
            var elem = $('#phone');
            var ln =elem.val().length;

            if (ln > 0){


                if ( elem.val().slice(0, 1) != 0){

                    var starr = elem.val().split("");

                    starr.unshift(0);
                    elem.val(starr.join(''));
                }
            }


            if (ln > 10){
                var st =elem.val().slice(0, 10);
                elem.val(st);
                return;
            }
        });



        $('#opt_code').keyup(function () {
            var vl = $('#opt_code').val();
            if (parseInt(vl) === NaN){
                $('#opt_code').val();
                return;
            };
            var ln =vl.length;
            if (ln > 4){
                var st =vl.slice(0, 4);
                $('#opt_code').val(st);
                return;
            }
        });
    </script>

<?php include(APPPATH . 'views/frontend/inc/footer.php'); ?>