<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8 no-js"> <![endif]-->
<!--[if IE 9]> <html lang="en" class="ie9 no-js"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en" ng-app="cinema">
<!--<![endif]-->
<!-- BEGIN HEAD -->
<head>

<meta charset="utf-8"/>
<meta name="SKYPE_TOOLBAR" content="SKYPE_TOOLBAR_PARSER_COMPATIBLE" />
<title><?php echo SITE_NAME ?> | <?php echo $title ?></title>
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta content="width=device-width, initial-scale=1.0" name="viewport"/>
<meta http-equiv="Content-type" content="text/html; charset=utf-8">
<meta content="" name="description"/>
<meta content="" name="author"/>

<script src="<?php echo base_url() ?>js/moment.js"></script>
<script src="<?php echo base_url() ?>angular/angular.js"></script>
<script src="<?php echo base_url() ?>angular/angular-route.js"></script>


<script type = "text/javascript" src="<?php echo base_url() ?>angular/ng-select.min.js"></script>
<script type = "text/javascript" src="<?php echo base_url() ?>angular/ng-file-upload.min.js"></script>
<script type = "text/javascript" src="<?php echo base_url() ?>angular/ng-file-upload-shim.min.js"></script>
<script type = "text/javascript" src="<?php echo base_url() ?>angular/moment.js"></script>
    <script src="<?php echo base_url(); ?>angular/angular-moment.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>angular/multipleDatePicker.min.js"></script>
<script type = "text/javascript" src="<?php echo base_url() ?>angular/app.js"></script>

<!-- BEGIN GLOBAL MANDATORY STYLES -->
<link href="<?php echo base_url() ?>skin/css/ng-select.min.css" rel="stylesheet" type="text/css"/>
<link href="//fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>skin_admin/global/css/multipleDatePicker.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>skin_admin/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>skin_admin/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>skin_admin/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>skin_admin/global/plugins/uniform/css/uniform.default.css" rel="stylesheet" type="text/css"/>

   <!-- END GLOBAL MANDATORY STYLES -->
<!-- BEGIN THEME STYLES -->

<link href="<?php echo base_url() ?>skin_admin/global/css/components.css" id="style_components" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>skin_admin/global/css/plugins.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>skin_admin/global/plugins/uploadify/uploadify.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>skin_admin/global/plugins/jquery-alerts/jquery.alerts.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>skin_admin/global/plugins/select2-4.0.3/css/select2.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>skin_admin/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker.min.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>skin_admin/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css"/>

<link href="<?php echo base_url() ?>skin_admin/admin/layout/css/layout.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>skin_admin/admin/layout/css/themes/darkblue.css" rel="stylesheet" type="text/css"/>
<link href="<?php echo base_url() ?>skin_admin/admin/layout/css/custom.css" rel="stylesheet" type="text/css"/>
<!-- END THEME STYLES -->
    <link rel="shortcut icon" href="<?php echo base_url()?>images/favicon.png" type="image/x-icon">
    <link rel="icon" href="<?php echo base_url()?>images/favicon.png" type="image/x-icon">


<!-- BEGIN CORE PLUGINS -->
<!--[if lt IE 9]>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/respond.min.js"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/excanvas.min.js"></script>
<![endif]-->
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery.min.js" type="text/javascript"></script>
<!--<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>-->


<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery-migrate.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery-ui/jquery-ui.min.js" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>skin_admin/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery.blockui.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery.cokie.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/uniform/jquery.uniform.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/tinymce/tinymce.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>skin_admin/global/plugins/uploadify/jquery.uploadify.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>skin_admin/global/plugins/jquery-alerts/jquery.alerts.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>skin_admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>skin_admin/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->

<script type="text/javascript" src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery-validation/js/jquery.validate.js"></script>
<script type="text/javascript" src="<?php echo base_url() ?>skin_admin/global/plugins/jquery.tablednd.js" ></script>
<script type="text/javascript" src="<?php echo base_url() ?>skin_admin/global/plugins/select2-4.0.3/js/select2.full.min.js" ></script>

<script type="text/javascript" src="<?php echo base_url(); ?>skin_admin/global/plugins/datatables/media/js/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="<?php echo base_url(); ?>skin_admin/global/plugins/datatables/plugins/bootstrap/dataTables.bootstrap.js"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/scripts/datatable.js"></script>
<script src="<?php echo base_url(); ?>skin_admin/admin/pages/scripts/table-ajax.js"></script>

    <script type="text/javascript">
        var base_url = '<?php echo base_url() ?>';
        window.base_url = '<?php echo base_url() ?>';
        var indicatorImg = '<img src="<?php echo base_url() ?>skin_admin/images/ajax-loader.gif" />';
    </script>

<!-- BEGIN PAGE LEVEL SCRIPTS -->
<script src="<?php echo base_url(); ?>skin_admin/global/scripts/metronic.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/admin/layout/scripts/layout.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/admin/layout/scripts/quick-sidebar.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/admin/layout/scripts/demo.js" type="text/javascript"></script>
<!-- END PAGE LEVEL SCRIPTS -->

<script type="text/javascript" src="<?php echo base_url() ?>skin_admin/global/scripts/common.js"></script>

</head>
<!-- BEGIN BODY -->
<!-- DOC: Apply "page-header-fixed-mobile" and "page-footer-fixed-mobile" class to body element to force fixed header or footer in mobile devices -->
<!-- DOC: Apply "page-sidebar-closed" class to the body and "page-sidebar-menu-closed" class to the sidebar menu element to hide the sidebar by default -->
<!-- DOC: Apply "page-sidebar-hide" class to the body to make the sidebar completely hidden on toggle -->
<!-- DOC: Apply "page-sidebar-closed-hide-logo" class to the body element to make the logo hidden on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-hide" class to body element to completely hide the sidebar on sidebar toggle -->
<!-- DOC: Apply "page-sidebar-fixed" class to have fixed sidebar -->
<!-- DOC: Apply "page-footer-fixed" class to the body element to have fixed footer -->
<!-- DOC: Apply "page-sidebar-reversed" class to put the sidebar on the right side -->
<!-- DOC: Apply "page-full-width" class to the body element to have full width page without the sidebar menu -->
<body  class="page-header-fixed page-quick-sidebar-over-content page-sidebar-closed-hide-logo <?php echo $slug ?><?php if(isset($body_class)) echo(' '.$body_class);?>">

<?php $this->load->view('admin/inc_header.php');?>