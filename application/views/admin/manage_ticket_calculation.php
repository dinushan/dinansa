<!-- BEGIN CONTAINER -->
<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php');?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title">Ticket Calculations</h3>

            <div class="row">
                <div class="col-md-12">
                    <?php echo get_message() ?>
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="portlet light bordered">
                        <!--<div class="portlet-title">
                            <div class="caption">
                                <i class="fa fa-pencil"></i>Settings
                            </div>							
                        </div>-->
                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <?php
                            $attr = array("id"=>"edit_form","class"=>"form-horizontal");
                            echo form_open(current_url(),$attr);
                            ?>
                            <?php
                            $film_levy           = $TicketCalculationsData->film_levy;
                            $corporation_tax     = $TicketCalculationsData->corporation_tax;
                            $movie_hall_rate     = $TicketCalculationsData->movie_hall_rate;
                            $distributor_rate    = $TicketCalculationsData->distributor_rate;
                            $supplier_rate       = $TicketCalculationsData->supplier_rate;
                            $film_coperation_tax = $TicketCalculationsData->film_coperation_tax;

                            ?>
                            <div class="form-body">
                                <h4 class="form-section">General Settings</h4>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Film Levy<span class="req">*</span></label>
                                    <div class="control-element col-md-4">
                                        <input type="text" class="form-control required-entry"
                                               value="<?php echo set_value('film_levy',$film_levy) ?>" name="film_levy" />
                                        <?php echo form_error('film_levy', '<div class="validation-advice">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Film Coperation Tax<span class="req">*</span></label>
                                    <div class="control-element col-md-4">
                                        <input type="text" class="form-control required-entry"
                                               value="<?php echo set_value('film_coperation_tax',$film_coperation_tax) ?>" name="film_coperation_tax" />
                                        <?php echo form_error('film_coperation_tax', '<div class="validation-advice">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Entertainment Tax (%)<span class="req">*</span></label>
                                    <div class="control-element col-md-4">
                                        <input type="text" class="form-control"
                                               value="<?php echo set_value('corporation_tax',$corporation_tax) ?>" name="corporation_tax" />
                                        <?php echo form_error('corporation_tax', '<div class="validation-advice">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Movie Hall Rate (%)<span class="req">*</span></label>
                                    <div class="control-element col-md-4">
                                        <input type="text" class="form-control required-entry"
                                               value="<?php echo set_value('movie_hall_rate',$movie_hall_rate) ?>" name="movie_hall_rate" />
                                        <?php echo form_error('movie_hall_rate', '<div class="validation-advice">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Distributor Rate (%)<span class="req">*</span></label>
                                    <div class="control-element col-md-4">
                                        <input type="text" class="form-control required-entry"
                                               value="<?php echo set_value('distributor_rate',$distributor_rate) ?>" name="distributor_rate" />
                                        <?php echo form_error('distributor_rate', '<div class="validation-advice">', '</div>'); ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Supplier Rate (%)<span class="req">*</span></label>
                                    <div class="control-element col-md-4">
                                        <input type="text" class="form-control required-entry"
                                               value="<?php echo set_value('supplier_rate',$supplier_rate) ?>" name="supplier_rate" />
                                        <?php echo form_error('supplier_rate', '<div class="validation-advice">', '</div>'); ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                    </div>
                                </div>
                            </div>
                            </form>
                            <script type="text/javascript">
                                $('.required-entry').attr('required','required');
                                $('#edit_form').validate({
                                    errorElement: 'span',
                                    errorClass: 'validation-advice',
                                    ignore: "",
                                    submitHandler: function (form) {
                                        form.submit();
                                    }
                                });


                            </script>
                            <!-- END FORM-->
                        </div>
                    </div>
                    <!-- END VALIDATION STATES-->
                </div>
            </div>

        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
