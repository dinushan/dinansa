/*
Navicat MySQL Data Transfer

Source Server         : My Local
Source Server Version : 50625
Source Host           : localhost:3306
Source Database       : hms_elevate

Target Server Type    : MYSQL
Target Server Version : 50625
File Encoding         : 65001

Date: 2018-01-17 22:04:16
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for auth_restaurant_admin_userdata
-- ----------------------------
DROP TABLE IF EXISTS `auth_restaurant_admin_userdata`;
CREATE TABLE `auth_restaurant_admin_userdata` (
  `userdata_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `user_id` int(10) unsigned NOT NULL,
  `emp_id` varchar(50) DEFAULT NULL,
  `fullname` varchar(40) NOT NULL DEFAULT '',
  `nic` varchar(50) DEFAULT NULL,
  `email` varchar(254) NOT NULL DEFAULT '',
  `phone1` varchar(20) DEFAULT NULL,
  `phone2` varchar(20) DEFAULT NULL,
  `picture` varchar(255) DEFAULT NULL,
  `department` text,
  `zone` text,
  PRIMARY KEY (`userdata_id`),
  KEY `user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of auth_restaurant_admin_userdata
-- ----------------------------
INSERT INTO `auth_restaurant_admin_userdata` VALUES ('1', '1', '0000', 'Admin Elevate', '', 'sampath@layoutindex.com', '0775345678', '', null, null, null);
INSERT INTO `auth_restaurant_admin_userdata` VALUES ('2', '2', '', 'Restaurant Manager', '813456789V', 'rmg@elevate.lk', '0775345678', '', null, null, null);
INSERT INTO `auth_restaurant_admin_userdata` VALUES ('3', '3', '123456', 'Sameera', '860033254V', 'sameera@elevate.lk', '0775345678', '', null, 'a:1:{i:0;s:1:\"6\";}', null);
INSERT INTO `auth_restaurant_admin_userdata` VALUES ('4', '4', '12345', 'Mr. Rooftop Chef', '634743744734', 'sandun.liyanage@layoutindex.lk', '577537353', '', null, '[\"6\"]', null);
INSERT INTO `auth_restaurant_admin_userdata` VALUES ('5', '5', '533533', 'Edward Taylor', '88334455', 'edward@elevate.lk', '077777777', '', null, null, 'a:6:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";i:4;s:1:\"5\";i:5;s:1:\"6\";}');
INSERT INTO `auth_restaurant_admin_userdata` VALUES ('6', '6', 'rrrr', 'Mr. Kitchen Manager', '33523523555', 'kitchen@dada.com', '226262626626', '', null, null, null);
INSERT INTO `auth_restaurant_admin_userdata` VALUES ('7', '7', '0032', 'Januka', '88453423424V', 'janukab@layoutindex.com', '06664323423', '', null, null, 'a:6:{i:0;s:1:\"1\";i:1;s:1:\"2\";i:2;s:1:\"3\";i:3;s:1:\"4\";i:4;s:1:\"5\";i:5;s:1:\"6\";}');
