<style>
    .select2-container{
        width: 100% !important;
    }
</style>
<script>
    window.calender = {
        collection:<?php echo $calender ?>,
        year : <?php echo $year ?>,
        month:<?php echo $month ?>,
        theater :<?php echo $theater ?>
    };

    window.holidays = {
        types : <?php echo $holidays; ?>
    };
</script>

<script type = "text/javascript" src="<?php echo base_url() ?>angular/admin/gallery/blackout_calender.js"></script>



<div class="page-container" >

    <?php $this->load->view('admin/inc_sidebar_navigation.php');?>

    <div class="page-content-wrapper">
        <div class="page-content">

            <h3 class="page-title">Blackout Calender</h3>
            <div class="panel panel-default" ng-controller="blackoutController" ng-cloak>

                <div class="panel-body">

                    <div class="row">
                        <div class="col-xs-3">
                            <a href="<?php echo base_url()?>admin/blackout-calender/<?php echo $this->encrypt_lib->encode($theater->id); ?>/<?php echo $year-1 ?>/{{displayDT |date:'M'}}" class="btn btn-sm btn-success" >
                                <span class="glyphicon glyphicon-menu-left" aria-hidden="true"> </span>Prev
                            </a>
                        </div>
                        <div class="col-xs-6 text-center"><h2>{{displayDT |date:'yyyy'}}</h2></div>
                        <div class="col-xs-3">
                            <a href="<?php echo base_url()?>admin/blackout-calender/<?php echo $this->encrypt_lib->encode($theater->id); ?>/<?php echo $year+1 ?>/{{displayDT |date:'M'}}" class="btn btn-sm btn-success pull-right">
                                Next<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                            </a>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <div class="col-xs-3">
                            <button class="btn btn-danger btn-sm" ng-click="prevMonth()">
                                <span class="glyphicon glyphicon-menu-left" aria-hidden="true"> </span>Prev

                            </button>
                        </div>
                        <div class="col-xs-6"><center><h3>{{displayDT |date:'MMMM'}}</h3></center></div>
                        <div class="col-xs-3">
                            <button class="btn btn-danger btn-sm pull-right" ng-click="nextMonth()">
                                Next<span class="glyphicon glyphicon-menu-right" aria-hidden="true"></span>
                            </button>
                        </div>
                    </div>
                    <br/>
                    <div class="row">
                        <table class="table calender ng-animate">
                            <thead class="row">
                            <tr class="calender-row">
                                <th class="calender-col sunday-col"><center>SUN</center></th>
                                <th class="calender-col"><center><b>MON</b></center></th>
                                <th class="calender-col"><center><b>TUE</b></center></th>
                                <th class="calender-col"><center><b>WED</b></center></th>
                                <th class="calender-col"><center><b>THU</b></center></th>
                                <th class="calender-col"><center><b>FRI</b></center></th>
                                <th class="calender-col saturday-col"><center>SAT</center></th>
                            <tr>
                            </thead>
                            <tbody>
                            <tr ng-repeat="week in weeks track by $index" class="calender-row" >
                                <td class="calender-col sunday-col cal-date" ng-click="openCalenderpopup(week[0])" ng-class="week[0].class"><center>{{ week[0].date | date:'dd'}}</center></td>
                                <td class="calender-col cal-date" ng-click="openCalenderpopup(week[1])" ng-class="week[1].class"><center>{{ week[1].date | date:'dd'}}</center></td>
                                <td class="calender-col cal-date" ng-click="openCalenderpopup(week[2])" ng-class="week[2].class"><center>{{ week[2].date | date:'dd'}}</center></td>
                                <td class="calender-col cal-date" ng-click="openCalenderpopup(week[3])" ng-class="week[3].class"><center>{{ week[3].date | date:'dd'}}</center></td>
                                <td class="calender-col cal-date" ng-click="openCalenderpopup(week[4])" ng-class="week[4].class"><center>{{ week[4].date | date:'dd'}}</center></td>
                                <td class="calender-col cal-date" ng-click="openCalenderpopup(week[5])" ng-class="week[5].class"><center>{{ week[5].date | date:'dd'}}</center></td>
                                <td class="calender-col saturday-col cal-date" ng-click="openCalenderpopup(week[6])" ng-class="week[6].class"><center>{{ week[6].date | date:'dd'}}</center></td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>


                <div class="modal fade" tabindex="-1" role="dialog" id="calpopup">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-header">
                                <button type="button" class="close" aria-label="Close" ng-click="closepopup()"><span aria-hidden="true" >&times;</span></button>
                                <h4 class="modal-title"><center>{{modeldate.dateObj.date |date:'dd/MMM/yyyy'}}</center></h4>
                            </div>
                            <div class="modal-body">
                                <form class="form-horizontal">
                                    <div class="form-group">
                                        <div class="col-sm-12">

                                            <div class="input-group"><span class="input-group-addon">Select Blackout Types</span>
                                                <ui-select multiple ng-model="modeldate.dateObj.holiday" theme="bootstrap" ng-disabled="false" on-select="selectItem($item)" on-remove="onRemoved($item)" close-on-select="false" title="Choose a color">
                                                    <ui-select-match placeholder="Select types...">
                                                        <span ng-bind="$item.name"></span>
                                                    </ui-select-match>
                                                    <ui-select-choices repeat="holidayType in holidays.types">
                                                        {{holidayType.name}}
                                                    </ui-select-choices>
                                                </ui-select>
                                                <!--select ng-model="selected_type" class="js-example-basic-multiple form-control input-sm" name="holiday_type" multiple="multiple"></select-->
                                            </div>

                                        </div>


                                    </div>

                                </form>
                            </div>
                            <div class="modal-footer">
                                <button type="button" class="btn btn-default" ng-click="closepopup()">Close</button>
                                <button type="button" class="btn btn-primary" ng-click="save(modeldate)">Save changes</button>
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div><!-- /.modal -->

                <div class="modal fade" tabindex="-1" role="dialog" id="pendingpopup">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <div class="modal-body">
                                <center>Please wait..!</center>
                            </div>
                        </div>
                    </div>
                </div>

            </div><!--End panel -->
        </div>
    </div>

