<?php 

foreach($tab_movies as $k=>$row):

	$movie_name = $row['movie_name'];
	$url_key = $row['url_key'];
	$booking_start_date = $row['booking_start_date'];
	$date_release = $row['date_release'];
	$end_date = $row['end_date'];
	$landscape_image = $row['landscape_image'];

	$trailer = $row['trailer'];

	$array = $arr_divs[$mcount];
	$class = $array[$k]['class'];

	if(!isset($array[$k]['sub'])){
		$image = $row[$array[$k]['image']];
	}

	if ($image === null){
    	continue;
	}

	if (is_file(MOVIE_IMG_MEDIUM_PATH.$image))
	{

		copy(MOVIE_IMG_PATH.$image,MOVIE_IMG_MEDIUM_PATH.$image);
    	$this->img_lib->resizeImage(MOVIE_IMG_MEDIUM_PATH.$image, MOVIE_LANDSCAPE_IMG_W/4, MOVIE_LANDSCAPE_IMG_H/4,100);

	}
	

	if($mcount == 4 && $k == 2){
		echo '<div class="panelRight p-4">';
	}
	echo '<div class="'.$class.'">';
?>



	<picture class="img-responsive coverImage">
		<source srcset="<?php echo base_url(MOVIE_IMG_PATH.$image) ?>, <?php echo base_url(MOVIE_IMG_PATH.$image) ?> 2x" media="(min-width: 768px)">
		<img srcset="<?php echo base_url(MOVIE_IMG_PATH.$landscape_image) ?>" alt="<?php echo $movie_name ?>"> 
	</picture>



	<a href="<?php echo base_url('movie/'.$url_key) ?>" class="imageOverlay"></a>
	<div class="captionWidgetWrap">
		<div class="captionTitleWrap">
			<h3 class="captionTitle"><?php echo $movie_name ?></h3>
		</div>
		<div class="releaseDate">In cinemas <?php echo date('jS F',strtotime($date_release)) ?> </div>
		<div class="iconContainer clearfix">
			<div class="iconWrapper">
				<ul class="iconWrap">
					<li><a href="<?php echo $trailer ?>" class="youTubePopUp"><i class="icon fa fa-youtube-play" aria-hidden="true"></i><span class="icoText">Trailer</span></a></li>
					<li><a href="<?php echo base_url('movie/'.$url_key) ?>"><i class="icon fa fa-info" aria-hidden="true"></i><span class="icoText">Info</span></a></li>
                    <?php
                    if(strtotime($booking_start_date) <= strtotime(date('Y-m-d')) && strtotime(date('Y-m-d')) <= strtotime($end_date)):
                    ?>
					<li><a href="<?php echo base_url('buy-tickets-online/').'?movie='.$this->encrypt_lib->encode($row['id']) ?>" class="btn btn-scope now-showing-upcoming" >Book</a></li>
                    <?php endif; ?>
				</ul>
			</div>
		</div>
	</div>

</div>

<?php
if($mcount == 4 && $k == 3){
	echo '</div>';
}

?>
<?php 
endforeach;					
?>
