/**
 * Created by LAYOUTindex on 12/13/2017.
 */
(function(window) {
    var app = angular.module('restaurant');

    app.controller('reportController', reportController);
    reportController.$inject = ['$scope','$window'];

    function reportController($scope,$window) {
        $('#date_range').daterangepicker({
            locale: {
                format: 'YYYY-MM-DD'
            },
            alwaysShowCalendars: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            }
        });

        $scope.types = [
            {'id':1,'name':'daily'},
            {'id':2,'name':'Monthly'},
            {'id':3,'name':'Year'}
        ];

        $scope.s ={
            't_vat':0,
            'tsch':0,
            'tntb':0,
            'tgt':0
        };
        $scope.ptotal = 0;
        $scope.type = $scope.types[0];
        $scope.purchases = [];
        $scope.sales = [];

        $scope.filter = function () {
            send();
        };

        $scope.export =function (){
            var val = $('#date_range').val();

            var from = val.split(' ')[0].trim();
            var to   = val.split(' ')[2].trim();

            var mywindow = window.open($window.base_url+'export?from='+from+'&to='+to+'&type='+$scope.type.id,"sales_report_download");
        };

        $scope.dateConvert = function(str){

            return new Date(str);
        };


        var send = function(){
            var val = $('#date_range').val();

            var from = val.split(' ')[0].trim();
            var to   = val.split(' ')[2].trim();

            $.ajax({
                'url':$window.base_url+'filter',
                'method':'post',
                'data':{'from':from,'to':to,'type':$scope.type.id}
            })
                .then( function(data){

                    $scope.s ={
                        't_vat':0,
                        'tsch':0,
                        'tntb':0,
                        'tgt':0
                    };
                    $scope.ptotal = 0;

                    $scope.$apply(function(){
                        $scope.sales = data.sales;
                        $scope.purchases = data.purchases;


                        for (var i in data.sales){
                            $scope.s.t_vat += parseFloat(data.sales[i].vat_amount)
                            $scope.s.tsch  += parseFloat(data.sales[i].service_charge)
                            $scope.s.tntb  += parseFloat(data.sales[i].nbt_amount)
                            $scope.s.tgt   += parseFloat(data.sales[i].grand_total)
                        }

                        for(var j in data.purchases){

                            $scope.ptotal += data.purchases[j].purchases;
                        }
                    });

                },function (error) {

                });
        };
        send();

    }
}(window));