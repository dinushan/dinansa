<!-- BEGIN CONTAINER -->

<div class="page-container">
    <?php $this->load->view('counter/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <?php if ($page_action == ''): ?>
            <h3 class="page-title">Bookings</h3>
            <div class="page-bar">
                <div class="page-toolbar">
                    <div class="btn-group pull-right"> </div>
                </div>
            </div>
            <?php
            echo get_message();
            ?>
            <div class="row">
                <div class="col-md-12">

                    <div class="table-container">

                        <table class="table table-striped table-bordered table-hover" id="datatable_ajax"
                               data-order-col = "0"
                               data-order-type = "desc"
                               data-ajax_url="<?php echo base_url('counter/'.$slug . '/ajax_list') ?>">
                            <colgroup>
                                <col width="40">
                                <col width="100">
                                <col width="100">
                                <col width="100">
                                <col width="100">
                                <col>
                                <col width="100">
                                <col>
                                <col>
                                <col>
                                <col>
                                <col>
                                <col width="50">
                                <col width="100">
                                <col width="100">
                                <col width="150">
                            </colgroup>
                            <thead>
                            <tr role="row" class="heading">
                                <th>#</th>
                                <th>Reference #</th>
                                <th>Date of Purchase</th>
                                <th>Movie Date</th>
								<th>Name</th>
                                <th>Agent</th>
                                <th>Mobile</th>
                                <th>Theater</th>
								<th>Movie</th>
                                <th>Time Slots</th>
                                <th class="sorting_disabled">Tickets</th>
                                <th class="sorting_disabled">Booking Type</th>
                                <th class="sorting_disabled">Booking Method</th>
                                <th class="sorting_disabled">Payment Mode</th>
                                <th class="sorting_disabled">Price</th>
                                <th class="sorting_disabled">Action</th>
                            </tr>
                            <tr role="row" class="filter">
                                <td></td>
                                <td><input type="text" class="form-control form-filter input-sm" name="ref_no"></td>
                                <td>
                                    <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control form-filter input-sm" name="date_from" placeholder="From" />
										<span class="input-group-btn">
										    <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
                                    </div>
                                    <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control form-filter input-sm" name="date_to" placeholder="To" />
										<span class="input-group-btn">
										    <button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
                                    </div>
                                </td>
                                <td>
                                    <div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control form-filter input-sm" name="movie_date_from" placeholder="From" />
                                        <span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
                                    </div>
                                    <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control form-filter input-sm" name="movie_date_to" placeholder="To" />
                                        <span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
                                    </div>
                                </td>
								<td><input type="text" class="form-control form-filter input-sm" name="name"></td>
                                <td></td>
                                <td><input type="text" class="form-control form-filter input-sm" name="mobile"></td>                                
                                <td></td>
                                <td></td>
                                <td><?php echo form_dropdown('time_slot',$time_slot_opt,'','class="form-control form-filter input-sm"') ?></td>
                                <td></td>
                                <td>
                                    <select name="booking_type" class="form-control form-filter input-sm">
                                        <option value="-1">All</option>
                                        <option value="1">Counter</option>
                                        <option value="2">Online</option>
                                        <option value="3">FYI</option>
                                        <option value="4">Mobile</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="booking_method" class="form-control form-filter input-sm">
                                        <option value="-1">All</option>
                                        <option value="1">Normal</option>
                                        <option value="2">Advanced</option>
                                    </select>
                                </td>
                                <td><?php echo form_dropdown('pay_method',$pay_method_opt,'','class="form-control form-filter input-sm"') ?></td>
                                <td></td>

                                <td>
                                    <div class="margin-bottom-5">
                                        <button class="btn btn-sm yellow filter-submit margin-bottom"><i
                                                class="fa fa-search"></i> Search
                                        </button>
                                    </div>
                                    <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset
                                    </button>
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
            <script>

                $('.date-picker').datepicker({
                    format: "yyyy-mm-dd",
                    autoclose: true
                });

                jQuery(document).ready(function () {
                    TableAjax.init();
                });

                $('.required-entry').attr('required', 'required');
                $('#edit_form').validate({
                    errorElement: 'span',
                    errorClass: 'validation-advice',
                    ignore: "",
                    submitHandler: function (form) {
                        form.submit();
                    }
                });


            </script>

            <?php elseif ($page_action == 'view'): ?>
                <h3 class="page-title">Ticket No. : <?php echo  $ticket_info->reference_no; ?></h3>
                <div class="page-bar">
                    <div class="page-toolbar">
                        <div class="btn-group pull-right">
                            <a class="btn default" href="<?php echo site_url('counter/'.$slug . '/index') ?>"><i
                                    class="fa fa-chevron-left"></i> Back</a>

                            <?php

                            $resendSMS_url = "window.location='".base_url('counter/ticket_sales/sendsms/' . $enc_id)."'";
                            echo '<button
                                onclick="'.$resendSMS_url.'"
                                class="btn blue"><i class="fa fa-send"></i> Resend SMS
                            </button>';


                                $ticket_printing_url = "window.location='".base_url('counter/ticket_printing/index?invoice=' . $ticket_info->reference_no)."'";
                                echo '<button
                                onclick="'.$ticket_printing_url.'"
                                class="btn green"><i class="fa fa-print"></i> Print
                            </button>';

                            ?>

                            <?php
                            /*if (count($cancellation_info) == 0) :
                            ?>
                            <button id="btn-cancellation" class="btn btn-danger"><i class="fa fa-trash-o"></i> Cancellation</button>
                            <?php endif;*/ ?>

                        </div>
                    </div>
                </div>

                <div class="row">
                <div class="col-md-12"> <?php echo get_message() ?>
                <!-- BEGIN VALIDATION STATES-->


                <div class="row">
                    <div class="col-md-6">
                        <h4 class="form-section">Booking Information : <?php echo date('Y-m-d h:i A',strtotime($ticket_info->purchase_date)); ?></h4>
                        <table class="table">
                            <tr>
                                <th class="col-md-3">Reference #</th>
                                <td><?php echo  $ticket_info->reference_no; ?></td>
                            </tr>
                            <tr>
                                <th class="col-md-3">Theater</th>
                                <td><?php echo $ticket_info->theater_name; ?></td>
                            </tr>
                            <tr>
                                <th class="col-md-3">Movie</th>
                                <td><?php echo $ticket_info->movie_name; ?></td>
                            </tr>
                            <tr>
                                <th class="col-md-3">Show Time</th>
                                <td><?php echo date('Y-m-d h:i A',strtotime($ticket_info->time_slot)); ?></td>
                            </tr>
                            <tr>
                                <th class="col-md-3">Mode of Purchase</th>
                                <td><?php echo  $ticket_info->payment_method; ?></td>
                            </tr>
                            <?php
                            if($ticket_info->payment_method_id == "1"): ?>
                                <tr>
                                    <th class="col-md-3">Total Amount(Rs)</th>
                                    <td>FREE</td>
                                </tr>
                            <?php else: ?>
                                <tr>
                                    <th class="col-md-3">Amount(Rs)</th>
                                    <td><?php echo $ticket_info->total_amount; ?></td>
                                </tr>
                                <tr>
                                    <th class="col-md-3">Discount(Rs)</th>
                                    <td><?php echo number_format($ticket_info->total_discount_amount,2); ?></td>
                                </tr>
                                <tr>
                                    <th class="col-md-3">Total Amount(Rs)</th>
                                    <td><?php echo number_format($ticket_info->total_amount - $ticket_info->total_discount_amount,2); ?></td>
                                </tr>

                            <?php endif; ?>

                        </table>

                        <h4 class="form-section">Customer Information</h4>
                        <table class="table">
                            <tr>
                                <th class="col-md-3">Name</th>
                                <td><?php echo $ticket_info->customer; ?></td>
                            </tr>
                            <tr>
                                <th class="col-md-3">Mobile</th>
                                <td><?php echo $ticket_info->mobile_no; ?></td>
                            </tr>
                        </table>

                        <?php
                        /*if (count($cancellation_info) > 0) :
                        ?>
                        <h4 class="form-section">Ticket Cancellation Information</h4>
                        <table class="table">

                            <tr>
                                <th class="col-md-3">Serial #</th>
                                <td><?php echo $cancellation_info['serial_numbers'] ?></td>
                            </tr>
                            <tr>
                                <th class="col-md-3">Refund Amount(Rs)</th>
                                <td><?php echo $cancellation_info['refund_amount'] ?></td>
                            </tr>
                            <tr>
                                <th class="col-md-3">User</th>
                                <td><?php echo $cancellation_info['username'] ?></td>
                            </tr>
                            <tr>
                                <th class="col-md-3">Comment</th>
                                <td><?php echo $cancellation_info['comment'] ?></td>
                            </tr>
                        </table>
                        <?php
                        endif;*/
                        ?>
                    </div>
                    <div class="col-md-6">

                        <h4 class="form-section">Ticket Information</h4>
                        <table class="table">
                            <tr>
                                <th class="col-md-3">Category</th>
                                <th class="col-md-3">Price (Rs)</th>
                                <th class="col-md-3">Qty</th>
                                <th class="col-md-3">Total (Rs)</th>
                            </tr>

                            <?php
                            if (count($ticket_prices) > 0) :
                                foreach ($ticket_prices as $ticket_price):

                                ?>
                                    <tr>
                                        <td><?php echo $ticket_price['category']; ?></td>
                                        <td><?php echo number_format($ticket_price['price']+$ticket_price['online_charge'],2); ?></td>
                                        <td><?php echo $ticket_price['qty']; ?></td>
                                        <td><?php echo number_format(($ticket_price['price']+$ticket_price['online_charge'])*$ticket_price['qty'],2); ?></td>
                                    </tr>
                                <?php endforeach;
                            ?>
                            <?php
                            else:
                                ?>
                                <tr>
                                    <td>No Data Found</td>
                                    <td></td>
                                    <td></td>
                                </tr>
                            <?php
                            endif;
                            ?>
                        </table>

                        <h4 class="form-section">Seat Information</h4>
                        <table class="table">
                            <tr>
                                <th class="col-md-3">Serial #</th>
                                <th class="col-md-3">Seat #</th>
                            </tr>

                            <?php
                            if (count($ticket_list) > 0) :
                            foreach ($ticket_list as $ticket_item):?>
                            <tr>
                                <td><?php echo $ticket_item['serial_number']; ?></td>
                                <td><?php echo $ticket_item['label']; ?></td>
                            </tr>
                            <?php endforeach;
                            else:
                            ?>
                            <tr>
                                <td>No Data Found</td>
                                <td></td>
                                <td></td>
                            </tr>
                            <?php
                            endif;
                            ?>
                        </table>

                    </div>
                </div>


                </div>
                </div>



            <?php endif;//$page_action ?>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER --> 
