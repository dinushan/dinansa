<!-- BEGIN CONTAINER -->

<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<?php if($page_action == ''):?>
			<h3 class="page-title">Manage Holiday Types</h3>
			
			<div class="page-bar">				
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<a onclick="setLocation('<?php echo site_url('admin/'.$slug.'/add') ?>')" class="btn green"><i class="fa fa-plus-circle"></i> Add New</a>
					</div>
				</div>
			</div>
			
			
			<?php echo get_message() ?>
			<div class="row">
				<div class="col-md-12">
					<div class="table-scrollable">
						
						<table id="tbl_list" class="table table-striped table-bordered table-advance table-hover">
						  <colgroup>
						  <col>
						  <col width="300">
						  </colgroup>
						  <thead>
							<tr class="headings nodrop nodrag">
							  <th>Thearter Partners</th>
							  <th>Action</th>
							</tr>
						  </thead>
						  <tbody>
							<?php
										foreach($holiday_types_view as $row):
											$id = $row['id'];
											$encry_hcId = $this->encrypt_lib->encode($id);
										?>
							<tr id="<?php echo $encry_hcId ?>">
							  <td><strong><?php echo $row['name']; ?></strong></td>
							  <td>
								<a class="btn btn-sm default btn-xs" href="<?php echo site_url('admin/'.$slug.'/edit/'.$encry_hcId) ?>"><i class="fa fa-edit"></i> Edit</a>

								<button class="btn btn-sm red btn-xs" type="button" 
								onclick="deleteConfirm('Are you sure you want to delete?','Confirm Delete',
								'<?php echo site_url('admin/'.$slug.'/delete/'.$encry_hcId) ?>');return false;"><i class="fa fa-trash-o"></i>
								Delete</button>
							</tr>                    
							<?php endforeach; ?>
						  </tbody>
						</table>
					</div>
				</div>
			</div>
			
			<?php elseif($page_action == 'add' || $page_action == 'edit'): ?>
			<h3 class="page-title">
				<?php
				if($page_action == 'add'){
					echo 'Add New Holiday Type';
				}else{
					echo 'Edit "'.$holiday_types_data->name.'"';
				}
				?></h3>
			<div class="page-bar">				
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<a class="btn default" href="<?php echo site_url('admin/'.$slug.'/') ?>"><i class="fa fa-chevron-left"></i> Back</a>
					</div>
				</div>
			</div>
			
			<div class="row">
				<div class="col-md-12"> <?php echo get_message() ?> 
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet light bordered">
						<div class="portlet-body form"> 
							<!-- BEGIN FORM-->
							<?php                    
						$attr = array("id"=>"edit_form","class"=>"form-horizontal","enctype"=>"multipart/form-data");
						echo form_open(current_url(),$attr);
						?>
						<?php
							if($page_action == 'edit'){
								$id = $holiday_types_data->id;
								$name = $holiday_types_data->name;
								
							}else{
								$id = '';
								$name = '';
							}
						?>  					
					  	
						
							<h4 class="form-section">General</h4>
							<div class="form-group">
								<label class="control-label col-md-3">Title<span class="req">*</span></label>
								<div class="control-element col-md-4"><input name="title" type="text" class="form-control required-entry" value="<?php echo set_value('title',$name) ?>" />
								  <?php echo form_error('title', '<br /><div class="validation-advice">', '</div>'); ?></div>
							  </div>

							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
									</div>
								</div>
							</div>
							<script type="text/javascript">							 
								$('.required-entry').attr('required','required');					
								$('#edit_form').validate({
									errorElement: 'span', 
									errorClass: 'validation-advice', 								
									ignore: "",
									submitHandler: function (form) {
										form.submit();										
									}
								});									
							</script>
							</form>
						</div>
					</div>
				</div>
			</div>						
			<?php endif;//$page_action ?>
		</div>
		<!-- END PAGE CONTENT--> 
	</div>
	<!-- END CONTENT --> 
	
</div>
<!-- END CONTAINER --> 
