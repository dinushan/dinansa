<!-- BEGIN CONTAINER -->

<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <h3 class="page-title"><?php echo $page_action == "add" ? 'Add New' : 'Edit' ?> Theater</h3>
            <div class="page-bar">
                <div class="page-toolbar">
                    <div class="btn-group pull-right">
                        <a class="btn default" href="<?php echo site_url('admin/' . $slug . '/') ?>"><i
                                class="fa fa-chevron-left"></i> Back</a>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12"> <?php echo get_message() ?>
                    <!-- BEGIN VALIDATION STATES-->
                    <div class="portlet light bordered">

                        <div class="portlet-body form">
                            <!-- BEGIN FORM-->
                            <?php
                            $attr = array("id" => "edit_form", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                            echo form_open(current_url(), $attr);
                            ?>
                            <?php
                            if ($page_action == 'edit')
                            {
                                $cinema_name        = $cinema_data->name;
                                $address            = $cinema_data->address;
                                $mail               = $cinema_data->mail;
                                $hotline            = $cinema_data->hotline;
                                $longitude          = $cinema_data->longitude;
                                $latitude           = $cinema_data->latitude;
                                $logo               = $cinema_data->logo;

                                $fb_link            = $cinema_data->fb_link;
                                $twitter_link       = $cinema_data->twitter_link;
                                $pinterest_link     = $cinema_data->pinterest_link;
                                $google_plus_link   = $cinema_data->google_plus_link;
                                $instagram_link     = $cinema_data->instagram_link;

                            } else {

                                $cinema_name        = '';
                                $address            = '';
                                $mail               = '';
                                $hotline            = '';
                                $longitude          = '';
                                $latitude           = '';
                                $logo               = '';

                                $fb_link            = '';
                                $twitter_link       = '';
                                $pinterest_link     = '';
                                $google_plus_link   = '';
                                $instagram_link     = '';;

                            }
                            ?>

                            <h4 class="form-section">General</h4>

                            <div class="form-group">
                                <label class="control-label col-md-3">Name<span class="req">*</span></label>

                                <div class="control-element col-md-4"><input name="name" type="text"
                                                                             class="form-control required-entry"
                                                                             value="<?php echo set_value('name', $cinema_name) ?>"/>
                                    <?php echo form_error('name'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Address<span class="req">*</span></label>

                                <div class="control-element col-md-4"><input name="address" type="text"
                                                                             class="form-control required-entry"
                                                                             value="<?php echo set_value('address', $address) ?>"/>
                                    <?php echo form_error('address'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Email<span class="req">*</span></label>

                                <div class="control-element col-md-4"><input name="mail" type="email"
                                                                             class="form-control required-entry"
                                                                             value="<?php echo set_value('mail', $mail) ?>"/>
                                    <?php echo form_error('mail'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">HotLine<span class="req">*</span></label>

                                <div class="control-element col-md-4"><input name="hotline" type="text"
                                                                             class="form-control required-entry"
                                                                             value="<?php echo set_value('hotline', $hotline) ?>"/>
                                    <?php echo form_error('hotline'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Longitude<span class="req">*</span></label>

                                <div class="control-element col-md-4"><input name="longitude" type="text"
                                                                             class="form-control required-entry"
                                                                             value="<?php echo set_value('longitude', $longitude) ?>"/>
                                    <?php echo form_error('longitude'); ?>
                                </div>
                            </div>

                           <div class="form-group">
                                <label class="control-label col-md-3">Latitude<span class="req">*</span></label>

                                <div class="control-element col-md-4"><input name="latitude" type="text"
                                                                             class="form-control required-entry"
                                                                             value="<?php echo set_value('latitude', $latitude) ?>"/>
                                    <?php echo form_error('latitude'); ?>
                                </div>
                            </div>


                            <?php if ($page_action == 'edit' && $logo != ''): ?>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Image <span class="req">*</span></label>

                                    <div class="control-element col-md-4">
                                        <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                            <tr>
                                                <td><img
                                                        src="<?php echo base_url() . CINEMA_LOGO_IMG_PATH . $logo ?>?<?php echo time() ?>"
                                                        alt="" width="200"/></td>
                                                <td>&nbsp;</td>
                                                <td><?php list($img_width, $img_height) = getimagesize(CINEMA_LOGO_2X_IMG_PATH . $logo); ?>
                                                    <strong>Image Info</strong><br/>
                                                    width:<?php echo $img_width ?>px<br/>
                                                    height:<?php echo $img_height ?>px
                                                </td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"></td>
                                            </tr>
                                            <tr>
                                                <td colspan="3"><label>
                                                        <input name="logo_del" type="checkbox" value="1"/>
                                                        Image Delete </label>

                                                    <div class="help-block font-green">Image Size:<?php echo CINEMA_LOGO_IMG_W ?>
                                                        px &times; <?php echo CINEMA_LOGO_IMG_H ?>px, Image Type: png<br />
                                                        If you want to upload a image, first delete the existing file.
                                                    </div>
                                            </tr>
                                            <tr>
                                                <td colspan="3">&nbsp;</td>
                                            </tr>
                                        </table>
                                    </div>
                                </div>
                            <?php else: ?>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Image <span class="req">*</span></label>

                                    <div class="control-element col-md-4"><input type="file" class=" input-file" name="logo">

                                <span class="help-block font-green">Image Size:<?php echo CINEMA_LOGO_IMG_W ?>
                                    px &times; <?php echo CINEMA_LOGO_IMG_H ?>px, Image Type: png</span>
                                    </div>
                                </div>
                            <?php endif; ?>

                            <h4 class="form-section">Social Media Links</h4>

                            <div class="form-group">
                                <label class="control-label col-md-3">Facebook</label>
                                <div class="control-element col-md-4"><input name="fb_link" type="text"
                                                                             class="form-control"
                                                                             value="<?php echo set_value('fb_link', $fb_link) ?>"/>
                                    <?php echo form_error('fb_link'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Twitter</label>
                                <div class="control-element col-md-4"><input name="twitter_link" type="text"
                                                                             class="form-control"
                                                                             value="<?php echo set_value('twitter_link', $twitter_link) ?>"/>
                                    <?php echo form_error('twitter_link'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Pinterest</label>
                                <div class="control-element col-md-4"><input name="pinterest_link" type="text"
                                                                             class="form-control"
                                                                             value="<?php echo set_value('pinterest_link', $pinterest_link) ?>"/>
                                    <?php echo form_error('pinterest_link'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Google Plus</label>
                                <div class="control-element col-md-4"><input name="google_plus_link" type="text"
                                                                             class="form-control"
                                                                             value="<?php echo set_value('google_plus_link', $google_plus_link) ?>"/>
                                    <?php echo form_error('google_plus_link'); ?>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Instagram</label>
                                <div class="control-element col-md-4"><input name="instagram_link" type="text"
                                                                             class="form-control"
                                                                             value="<?php echo set_value('instagram_link', $instagram_link) ?>"/>
                                    <?php echo form_error('instagram_link'); ?>
                                </div>
                            </div>


                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <div class="btn-group">
                                            <input type="hidden" id="hidden-action" name="hidden-action" value="1">
                                            <button type="submit" data-id="1" class="btn btn-submit green">Save & Return
                                            </button>
                                            <button type="submit" data-id="2" class="btn btn-submit btn-default">Save & New
                                            </button>
                                            <button type="submit" data-id="3" class="btn btn-submit btn-default">Save & Edit
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <script type="text/javascript">

                                $('.required-entry').attr('required', 'required');
                                $('#edit_form').validate({
                                    errorElement: 'span',
                                    errorClass: 'validation-advice',
                                    ignore: "",
                                    submitHandler: function (form) {
                                        form.submit();
                                    }
                                });
                            </script>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER -->