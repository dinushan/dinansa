<!-- BEGIN CONTAINER -->

<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<h3 class="page-title">User Groups</h3>
			<div class="page-bar">				
				<div class="page-toolbar">
					<div class="btn-group pull-right">
						<a onclick="setLocation('<?php echo site_url("admin/counterusers/add_group") ?>')" class="btn green"><i class="fa fa-plus-circle"></i> Add Group</a>
					</div>
				</div>
			</div>
					
			
			<?php echo get_message() ?>
			<div class="row">
				<div class="col-md-12">
					<div class="table-scrollable">
						<table id="tbl_list" class="table table-striped table-bordered table-advance table-hover">
						  <thead>
                              <tr class="headings">
                              	<th width="41">ID</th>
                                <th width="247">Name</th>
                                <th width="518">Description</th>
                                <th width="142">Actions</th>
                              </tr>
                          </thead>
                          <tbody>            
                          	<?php 
							if( ! empty($groups)):
								foreach($groups as $_group):			
									$encry_gpId = $this->encrypt_lib->encode($_group->group_id);
							?>                 
							<tr class="body">
                                <td><?php echo $_group->group_id ?></td>
                                <td><?php echo $_group->name ?></td>
                                <td><?php echo $_group->description ?></td>
                                <td align="center">
                                <?php if($adminauth->has_role('super_admin')){?>
                                <button onclick="window.location='<?php echo base_url().'admin/counterusers/edit_group/'.$encry_gpId ?>'" class="btn btn-sm default btn-xs"><i class="fa fa-edit"></i> Edit</button>
                                <button onclick="deleteConfirm('Are you sure you want to delete?','Confirm Delete','<?php echo base_url() ?>admin/counterusers/delete_group/<?php echo $encry_gpId ?>',false);" class="btn btn-sm red btn-xs"><i class="fa fa-trash-o"></i> Delete</button>
								
                                <?php } ?></td>
                              </tr>
                              <?php 
							 		endforeach;
								endif;
							  ?>                                                         
						  </tbody>                          
                        </table>    
                  
           
					</div>
				</div>
			</div>
			
		     
			
		</div>
		<!-- END PAGE CONTENT--> 
	</div>
	<!-- END CONTENT --> 
	
</div>
<!-- END CONTAINER --> 


