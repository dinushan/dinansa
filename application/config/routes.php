<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/


$route['logout'] 						= "admin/logout";
$route['dashboard'] 					= "admin/dashboard";

$route['users'] 						= "admin/users";
$route['users/ajax_list'] 			= "admin/ajax_list";
$route['users/audit_log'] 			= "admin/audit_log";
$route['users/add'] 					= "admin/add_user";
$route['users/edit/(:any)'] 			= "admin/edit_user/$1";
$route['users/delete/(:any)'] 		= "admin/delete_user/$1";
$route['my_account'] 					= "admin/my_account";
$route['forgot_password'] 			= "admin/forgot_password";
$route['new_password/(:any)'] 		= "admin/new_password/$1";

$route['users/groups'] 				= "admin/groups";
$route['users/add_group'] 			= "admin/add_group";
$route['users/edit_group/(:any)'] 	= "admin/edit_group/$1";
$route['users/delete_group/(:any)'] 	= "admin/delete_group/$1";



//custom zone route
$route['manage/zone/list/(:any)'] = "restaurant/zone/index/$1";
$route['manage/zone/ajax_list/(:any)'] = "restaurant/zone/ajaxList/$1";
$route['manage/zone/ajax_list'] = "restaurant/zone/ajaxList";
$route['manage/zone/create/(:any)'] = "restaurant/zone/create/$1";
$route['manage/zone/store/(:any)'] = "restaurant/zone/store/$1";
$route['manage/zone/edit/(:any)'] = "restaurant/zone/edit/$1";
$route['manage/zone/update/(:any)'] = "restaurant/zone/update/$1";
$route['manage/zone/view/(:any)'] = "restaurant/zone/view/$1";
$route['manage/zone/delete/(:any)'] = "restaurant/zone/delete/$1";

//custom restaurant manage route
$route['manage'] = "restaurant/floor";
$route['manage/ajax_list'] = "restaurant/floor/ajaxList";
$route['manage/edit/(:any)'] = "restaurant/floor/edit/$1";

//custom table route
$route['manage/tables/ajax_list/(:any)'] = "restaurant/tables/ajaxList/$1";
$route['manage/tables/delete/(:any)/(:any)'] = "restaurant/tables/delete/$1/$2";
$route['manage/tables/(:any)/(:any)'] = "restaurant/tables/create/$1/$2";
$route['manage/tables/(:any)'] = "restaurant/tables/create/$1";

$route['log/(\d{4})-(\d{2})-(\d{2})'] = 'log/$1-$2-$3';
$route['log'] = 'log';

$route['default_controller'] = 'admin/index';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
