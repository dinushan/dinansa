<?php include(APPPATH . 'views/frontend/inc/head.php'); ?>
<?php include(APPPATH . 'views/frontend/inc/header.php'); ?>

    <section class="carousel">
        <div class="container-fluid">
            <div class="row">
                <div class="main-carousel clearfix">
                    <div class="camera_wrap">

                        <?php
						$home_banner_image_path = IMG_PATH_LARGE;
						if($this->mobile_detect->isMobile()){
							$home_banner_image_path = IMG_PATH_MEDIUM;
						}
						
                        foreach ($banner_images as $home)
                        {
                            $arr_title = explode(' ',trim($home['title']));

                        ?>
                        <div class="carousel-tem" data-src="<?php echo base_url($home_banner_image_path.$home['image']) ?>">
                            <div class="camera_caption fadeIn captionContainer dis-table">
                                <?php
                                if(count($arr_title) > 0) {
                                    ?>
                                    <div class="captionWrapLeft dis-t-cell">
                                        <a class="link" href="<?php echo $home['popup_imgtitle']; ?>" onclick="return getHomeBannerPink()">
                                        <div class="clearfix">
                                            <h4 class="text-top pull-right text"><?php if (!empty($arr_title[0])) {
                                                    echo $arr_title[0];
                                                } ?></h4>
                                        </div>
                                        <div class="clearfix">
                                            <h4 class="text-middle pull-right text"><?php if (!empty($arr_title[1])) {
                                                    echo $arr_title[1];
                                                } ?></h4>
                                        </div>
                                        <div class="clearfix">
                                            <h4 class="text-bottom pull-right text"><?php if (!empty($arr_title[2])) {
                                                    echo $arr_title[2];
                                                } ?></h4>
                                        </div>
                                        </a>
                                    </div>
                                    <?php
                                }
                                    ?>

                                <div class="captionWrapRight dis-t-cell">
                                    <div class="captionDetailsWrap">
                                        <div class="captionDetails">
                                            <div class="detail-text"><span class="text"><?php echo $home['caption']; ?></span>
                                            </div>
                                            <div class="detail-btn-wrap">
                                                <a href="<?php echo $home['alt']; ?>" onclick="return getHomeBannerBlue()" class="btn-more">
                                                    <span class="btn-text">
                                                        <span class="text"><?php echo $home['position']; ?></span>
                                                    </span>
                                                    <span class="btn-ico-wrap">
                                                        <span class="btn-icon"></span> 
                                                    </span>
                                                </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <?php
                        }
                        ?>


                    </div>

                    <div class="nav-body-tab">
                        <ul class="nav nav-tabs home-tab-1" role="tablist">
                            <?php if (count($now_showing_movies) > 1): ?>
                                <li role="presentation"><a href=".nowShowing" aria-controls="nowShowing" role="tab"
                                                           data-toggle="tab">NOW SHOWING</a></li>
                            <?php endif; ?>
                            <?php if (count($upcoming_movies) > 1): ?>
                                <li role="presentation"><a href=".upcomming" aria-controls="upcomming" role="tab"
                                                           data-toggle="tab">UPCOMING</a></li>
                            <?php endif; ?>
                        </ul>
                    </div>


                </div>
            </div>
        </div>
    </section>

    <section class="featuredMovies">
        <div class="tabPaneWrap">
            <div class="tab-content  home-tab-content-1">
                <?php
                $mcount = count($now_showing_movies);
                if($mcount > 1):
                ?>
                <div role="tabpanel" class="tab-pane nowShowing">
                    <div class="panelWrapper clearfix">
                        <?php
                       $tab_movies = $now_showing_movies;
                        include(APPPATH . 'views/frontend/inc-home-movie-tabs.php'); ?>
                    </div>
                </div>
                <?php endif; ?>
                <?php
                $mcount = count($upcoming_movies);
                if($mcount > 1):
                ?>
                <div role="tabpanel" class="tab-pane upcomming">
                    <div class="panelWrapper clearfix">
                        <?php

                        $tab_movies = $upcoming_movies;

                        include(APPPATH . 'views/frontend/inc-home-movie-tabs.php'); ?>
                    </div>
                </div>
                <?php endif; ?>
            </div>
        </div>
    </section>


    <!--<section class="advertisement">
        <div class="advertisementwrapper">
            <div class="advertisement-carousel clearfix">
                <div class="advertisement_camera_wrap">
                    <?php
/*                    foreach ($advertisement_images as $add) {
                        $view_image = $add['web_view_image'];
                        if($this->mobile_detect->isMobile()){
                            $view_image = $add['mobile_view_image'];
                        }

                        */?>
                        <div class="carousel-tem"
                             data-src="<?php /*echo base_url(ADVERTISEMENT_IMG_PATH.$view_image) */?>"></div>
                        <?php
/*                    }
                    */?>
                </div>
            </div>
        </div>
    </section>-->



    <section class="aboutScope animatedParent" data-sequence='500' data-appear-top-offset='-100'>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-md-8 col-md-offset-2 aboutWrap text-center">
                    <h2 class="aboutHead animated fadeInUpShort"  data-id='1'><?php echo $siteData->title1; ?></h2>
                    <div class="aboutDesc animated fadeInUpShort"  data-id='2'>
                        <p><?php echo $siteData->description; ?></p>
                    </div>
                </div>
            </div>
        </div>
    </section>



    <!--<section class="offers">
        <div class="offerHeadWrap">
            <h2 class="offerHead text-center">Deals & Exclusives</h2>
        </div>
        <div class="container">
            <div class="row">
                <div class="offersContain clearfix">
                    <?php
/*                    foreach ($deals as $deal) {
                        */?>
                        <div class="col-xs-12 col-md-6 offerWrap padding-zero-mobile">
                            <div class="offerDescWrap">
                                <div class="descHeadWrap">
                                    <h3 class="descHead"><?php /*echo $deal['deal_name']; */?></h3>
                                </div>
                                <div class="descWrap">
                                    <div class="desc">
                                        <p><?php /*echo $deal['short_description']; */?></p>
                                    </div>
                                </div>
                            </div>
                            <a href="#deals-modal" class="offerBannerWrap data-image popup-modal" data-deal-id="<?php /*echo $deal['id']; */?>" data-image-src="<?php /*echo base_url(DEALS_IMG_PATH.$deal['image']); */?>"></a>
                        </div>
                        <?php
/*                    }
                    */?>

                </div>
            </div>
        </div>
    </section>-->

    <div id="deals-modal" class="deals-modal white-popup-block mfp-hide">
        <div class="dealsBody">
            <div class="row">
                <div class="col-xs-12 headerWrap">
                    <h2 class="header">Modal dialog</h2>
                </div>
                <div class="col-xs-6 col-sm-5 col-md-4 imageWrap">
                    <img src="images/offer/kotac-credit.jpg" alt="">
                </div>
                <div class="col-xs-6 col-sm-7 col-md-8 modalDescWrap">
                    <div class="modalDesc">

                    </div>
                </div>
            </div>
        </div>
    </div>
<script>
    $('.now-showing-upcoming').click( function(e){
        ga('send', 'event', 'home', 'click', 'home_book');
    });
</script>
<?php include(APPPATH . 'views/frontend/inc/footer.php'); ?>

