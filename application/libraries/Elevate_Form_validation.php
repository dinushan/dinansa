<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Elevate_Form_validation extends CI_Form_validation {


    public function __construct($config = array()){
        parent::__construct($config);
        $CI =& get_instance();
    }

	/**
	 * My_Form_validation::adminauth_unique_username()
	 *
	 */
	public function adminauth_unique_username($username, $exclude_id = FALSE)
	{
		if( ! $this->CI->adminauth->username_is_unique($username, $exclude_id))
		{
			$this->CI->form_validation->set_message('adminauth_unique_username', $this->CI->adminauth->get_error(FALSE));
			return FALSE;
		}

		return TRUE;
	}

    public function adminauth_unique_emp_id($emp_id, $exclude_id = FALSE)
    {
        if($emp_id != '' && ! $this->CI->adminauth->emp_id_is_unique($emp_id, $exclude_id))
        {
            $this->CI->form_validation->set_message('adminauth_unique_emp_id', $this->CI->adminauth->get_error(FALSE));
            return FALSE;
        }

        return TRUE;
    }

	/**
	 * My_Form_validation::adminauth_unique_group()
	 *
	 */
	public function adminauth_unique_group($group_name, $exclude_id = FALSE)
	{
	    if( ! $this->CI->adminauth->group_is_unique($group_name, $exclude_id))
		{
			$this->CI->form_validation->set_message('adminauth_unique_group', $this->CI->adminauth->get_error(FALSE));
			return FALSE;
		}

		return TRUE;
	}

	/**
	 * MY_Form_validation::adminauth_valid_password()
	 *
	 */
	public function adminauth_valid_password($password)
	{
		if( ! $this->CI->adminauth->password_is_valid($password))
		{
			$this->CI->form_validation->set_message('adminauth_valid_password', $this->CI->adminauth->get_error(FALSE));
			return FALSE;
		}

		return TRUE;
	}
}