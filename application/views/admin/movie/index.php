<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">
<?php if ($page_action == ''): ?>
    <h3 class="page-title">Movies</h3>

    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <?php if(
                $this->adminauth->has_role('401')
                ):?>
                <a onclick="setLocation('<?php echo site_url('admin/' . $slug . '/add') ?>')" class="btn green"><i
                        class="fa fa-plus-circle"></i> Add New</a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php if(
    $this->adminauth->has_role('401')
    ):?>
    <?php echo get_message() ?>
    <div class="row">
        <div class="col-md-12">

            <div class="table-container">

                <table class="table table-striped table-bordered table-hover" id="datatable_ajax"
                       data-order-col = "0"
                       data-order-type = "asc"
                       data-ajax_url="<?php echo base_url('admin/' . $slug . '/ajax_list') ?>">
                    <colgroup>
                        <col width="40"/>
                        <col width="100">
                        <col>
                        <col width="100">
                        <col width="300">
                    </colgroup>
                    <thead>
                    <tr role="row" class="heading">
                        <th>ID</th>
                        <th>Movie Year</th>
                        <th>Movie Name</th>
                        <th>Published</th>
                        <th class="sorting_disabled">Action</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="movie_year">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="movie_name">
                        </td>
                        <td>
                            <?php
                            if(isset($_GET['status'])){
                                $status = $_GET['status'];
                            }else{
                                $status = '';
                            }
                            echo form_dropdown('status',array(''=>'All','1'=>'YES','0'=>'NO'),$status,'class="form-control form-filter input-sm"'); ?>
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-sm yellow filter-submit margin-bottom"><i
                                        class="fa fa-search"></i> Search
                                </button>
                            </div>
                            <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset
                            </button>
                        </td>
                    </tr>
                    </thead>
                    <tbody>

                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function() {
            TableAjax.init();
        });
    </script>

    <?php endif; ?>


<?php elseif ($page_action == 'add' || $page_action == 'edit'): ?>
    <h3 class="page-title"><?php echo $page_action == "add" ? 'Add New' : 'Edit' ?> Movie</h3>
    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn default" href="<?php echo site_url('admin/' . $slug . '/') ?>"><i
                        class="fa fa-chevron-left"></i> Back</a>

                <?php if($page_action == 'edit'): ?>
                    <a class="btn blue" href="<?php echo site_url('admin/'.$slug.'/photos/'.$enc_id) ?>"><i class="fa fa-plus"></i> Photos</a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12"> <?php echo get_message() ?>
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light bordered">

                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <?php
                    $attr = array("id" => "edit_form", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                    echo form_open(current_url(), $attr);
                    ?>
                    <?php
                    if ($page_action == 'edit')
                    {
                        $movie_name         =   $movie_data->movie_name;
                        $movie_year         =   $movie_data->movie_year;
                        $movie_content      =   $movie_data->movie_content;
                        $trailer            =   $movie_data->trailer;
                        $duration_h         =   $movie_data->duration_h;
                        $duration_m         =   $movie_data->duration_m;
                        $imdb_rate          =   $movie_data->imdb_rate;
                        $landscape_image    =   $movie_data->landscape_image;
                        $portrait_image     =   $movie_data->portrait_image;
                        $supplier_id        =   $movie_data->supplier_id;

                        $cast               =   $movie_data->cast;
						$directed_by        =   $movie_data->directed_by;
						$produced_by        =   $movie_data->produced_by;
						$written_by         =   $movie_data->written_by;
						$music_by           =   $movie_data->music_by;

                        $date_release       =   $movie_data->date_release;
                        $date_end           =   $movie_data->date_end;
                        $status             =   $movie_data->status;

                        $meta_title 		=   $movie_data->meta_title;
                        $meta_keywords 		=   $movie_data->meta_keywords;
                        $meta_description 	=   $movie_data->meta_description;
                        $og_type 			=   $movie_data->og_type;
                        $og_title 			=   $movie_data->og_title;
                        $og_description 	=   $movie_data->og_description;

                    } else {
                        $movie_name         =   '';
                        $movie_year         =   '';
                        $movie_content      =   '';
                        $trailer            =   '';
                        $duration_h         =   '';
                        $duration_m         =   '';
                        $imdb_rate          =   '';
                        $landscape_image    =   '';
                        $portrait_image     =   '';
                   		$genre_selected     =   array();
                        $supplier_id        =   '';

                        $cast               =   '';
						$directed_by        =   '';
						$produced_by        =   '';
						$written_by         =   '';
						$music_by           =   '';

                        $date_release       =   '';
                        $date_end           =   '';
                        $status             =   0;

                        $meta_title 		=   '';
                        $meta_keywords 		=   '';
                        $meta_description 	=   '';
                        $og_type 			=   '';
                        $og_title 			=   '';
                        $og_description 	=   '';
                    }
                    ?>

                    <h4 class="form-section">General</h4>

                    <div class="form-group">
                        <label class="control-label col-md-3">Movie name<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <input name="movie_name" type="text" class="form-control required-entry movie_name" value="<?php echo set_value('movie_name', $movie_name) ?>"/>
                            <?php echo form_error('movie_name'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Movie Year<span class="req">*</span></label>

                        <div class="control-element col-md-4"><input name="movie_year" type="text"
                                                                     class="form-control required-entry" placeholder="yyyy"
                                                                     value="<?php echo set_value('movie_year', $movie_year) ?>"/>
                            <?php echo form_error('movie_year'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Movie content<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <textarea rows="2" name="movie_content" class="form-control required-entry"><?php echo set_value('movie_content', $movie_content) ?></textarea>
                            <?php echo form_error('movie_content'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-md-3 col-xs-12 control-label">Movie Supplier<span class="req">*</span></label>
                        <div class="col-md-4 col-xs-12">
                            <?php echo form_dropdown('supplier_id', $supplier_data, set_value('supplier_id',$supplier_id),'class="form-control select"') ?>
                            <?php echo form_error('supplier_id'); ?>
                        </div>
                    </div>
					
					<div class="form-group">
                        <label class="control-label col-md-3">Trailer<span class="req">*</span></label>

                        <div class="control-element col-md-4"><input name="trailer" type="text"
                                                                     class="form-control required-entry"
                                                                     value="<?php echo set_value('trailer', $trailer) ?>"/>
                            <?php echo form_error('trailer'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Duration<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control required-entry" placeholder="Hours" name="duration_h"
                                               value="<?php echo set_value('duration_h', $duration_h) ?>"><span class="input-group-addon">h</span>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="Minutes" name="duration_m"
                                               value="<?php echo set_value('duration_m', $duration_m) ?>"><span class="input-group-addon">min</span>
                                    </div>
                                </div>
                            </div>
                            <?php echo form_error('duration_h'); ?>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="control-label col-md-3">IMDB Rate<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <div class="input-group">
                                <input name="imdb_rate" type="text"
                                class="form-control required-entry"
                                value="<?php echo set_value('imdb_rate', $imdb_rate) ?>"/><span class="input-group-addon">/10</span>
                            </div>
                            <?php echo form_error('imdb_rate'); ?>
                        </div>
                    </div>
					
					<div class="form-group">
                        <label class="control-label col-md-3">Genres<span class="req">*</span></label>
                        <div class="control-element col-md-4">
							<?php echo form_dropdown('genre[]', $genre_opt, set_value('genre[]', $genre_selected), 'class="form-control required-entry cb_s2" multiple'); ?>
							<?php echo form_error('genre[]'); ?>
						</div>
                    </div>


					<div class="form-group">
                        <label class="control-label col-md-3">Release Date<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <div class="input-group date date-picker " data-date-format="yyyy-mm-dd">
								<input type="text" class="form-control" name="date_release" value="<?php echo set_value('date_release',$date_release) ?>">
									<span class="input-group-btn">
									<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
									</span>
							</div>
							<?php echo form_error('date_release'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">End Date<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <div class="input-group date date-picker " data-date-format="yyyy-mm-dd">
                                <input type="text" class="form-control" name="date_end" value="<?php echo set_value('date_end',$date_end) ?>">
									<span class="input-group-btn">
									<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
									</span>
                            </div>
                            <?php echo form_error('date_end'); ?>
                        </div>
                    </div>


                    <?php if ($page_action == 'edit' && $landscape_image != ''): ?>
                        <div class="form-group">
                            <label class="control-label col-md-3">Landscape Image <span class="req">*</span></label>

                            <div class="control-element col-md-4">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td><img
                                                src="<?php echo base_url() . MOVIE_IMG_PATH . $landscape_image ?>?<?php echo time() ?>"
                                                alt="" width="200"/></td>
                                        <td>&nbsp;</td>
                                        <td><?php list($img_width, $img_height) = getimagesize(MOVIE_2X_IMG_PATH . $landscape_image); ?>
                                            <strong>Image Info</strong><br/>
                                            width:<?php echo $img_width ?>px<br/>
                                            height:<?php echo $img_height ?>px
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><label>
                                                <input name="landscape_image_del" type="checkbox" value="1"/>
                                                Image Delete </label>

                                            <div class="help-block font-green">Image Size:<?php echo MOVIE_LANDSCAPE_IMG_W ?>
                                                px &times; <?php echo MOVIE_LANDSCAPE_IMG_H ?>px, Image Type: jpg<br />
                                                If you want to upload a image, first delete the existing file.
                                            </div>
                                    </tr>
                                    <tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="form-group">
                            <label class="control-label col-md-3">Landscape Image <span class="req">*</span></label>

                            <div class="control-element col-md-4"><input type="file" class=" input-file" name="landscape_image">

                                <span class="help-block font-green">Image Size:<?php echo MOVIE_LANDSCAPE_IMG_W ?>
                                    px &times; <?php echo MOVIE_LANDSCAPE_IMG_H ?>px, Image Type: jpg</span>
                            </div>
                        </div>
                    <?php endif; ?>


                    <?php if ($page_action == 'edit' && $portrait_image != ''): ?>
                        <div class="form-group">
                            <label class="control-label col-md-3">Portrait Image <span class="req">*</span></label>

                            <div class="control-element col-md-4">
                                <table width="100%" border="0" cellspacing="0" cellpadding="0">
                                    <tr>
                                        <td><img
                                                src="<?php echo base_url() . MOVIE_IMG_PATH . $portrait_image ?>?<?php echo time() ?>"
                                                alt="" width="200"/></td>
                                        <td>&nbsp;</td>
                                        <td><?php list($img_width, $img_height) = getimagesize(MOVIE_2X_IMG_PATH . $portrait_image); ?>
                                            <strong>Image Info</strong><br/>
                                            width:<?php echo $img_width ?>px<br/>
                                            height:<?php echo $img_height ?>px
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"></td>
                                    </tr>
                                    <tr>
                                        <td colspan="3"><label>
                                                <input name="portrait_image_del" type="checkbox" value="1"/>
                                                Image Delete </label>

                                            <div class="help-block font-green">Image Size:<?php echo MOVIE_PORTRAIT_IMG_W ?>
                                                px &times; <?php echo MOVIE_PORTRAIT_IMG_H ?>px, Image Type: jpg<br />
                                                If you want to upload a image, first delete the existing file.
                                            </div>
                                    </tr>
                                    <tr>
                                        <td colspan="3">&nbsp;</td>
                                    </tr>
                                </table>
                            </div>
                        </div>
                    <?php else: ?>
                        <div class="form-group">
                            <label class="control-label col-md-3">Portrait Image <span class="req">*</span></label>

                            <div class="control-element col-md-4"><input type="file" class=" input-file" name="portrait_image">

                                <span class="help-block font-green">Image Size:<?php echo MOVIE_PORTRAIT_IMG_W ?>
                                    px &times; <?php echo MOVIE_PORTRAIT_IMG_H ?>px, Image Type: jpg</span>
                            </div>
                        </div>
                    <?php endif; ?>


                    <div class="form-group">
                        <label class="control-label col-md-3">Cast <span class="req">*</span></label>
                        <div class="control-element col-md-4">
                            <table class="table table-bordered table-hover tbl-cast">
                                <thead>
                                <tr>
                                    <th>Actor</th>
                                    <th>Character</th>
                                    <th style="width:10px"><span class="glyphicon glyphicon-plus addBtn"></span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $arr_cast = unserialize($cast);

                                if ($cast == "" || count($arr_cast) == 0): ?>
                                    <tr id="tr_0">
                                        <td><input type="text" name='actor[]'
                                                   class="form-control required-entry"/></td>
                                        <td><input type="text" name='character[]'
                                                   class="form-control required-entry"/></td>
                                        <td></td>
                                    </tr>
                                <?php
                                else:
                                    foreach ($arr_cast as $k=>$r):
                                        $actor = $r['actor'];
                                        $character = $r['character'];
                                        ?>
                                        <tr id='tr_<?php echo $k ?>'>
                                            <td>
                                                <input type="text" name='actor[]'
                                                       value="<?php echo $actor ?>"
                                                       class="form-control required-entry"/>
                                            </td>
                                            <td>
                                                <input type="text" name='character[]'
                                                       value="<?php echo $character ?>"
                                                       class="form-control required-entry"/>
                                            </td>
                                            <td>
                                                <?php if($k > 0):  ?>
                                                    <span class="glyphicon glyphicon-minus addBtnRemove"></span>
                                                <?php endif;  ?>
                                            </td>
                                        </tr>
                                    <?php
                                    endforeach;
                                endif;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>
					
					<div class="form-group">
						<label class="control-label col-md-3">Directed by <span class="req">*</span></label>
						<div class="control-element col-md-4">
							<table class="table table-bordered table-hover tbl-directed-by">
								<thead>
								<tr>
									<th>Name</th>									
									<th style="width:10px"><span class="glyphicon glyphicon-plus addBtn"></span></th>
								</tr>
								</thead>
								<tbody>
								<?php
								$arr_directed_by = unserialize($directed_by);
								
								if ($directed_by == "" || count($arr_directed_by) == 0): ?>
									<tr id="tr_0">
										<td><input type="text" name='directer_name[]'
												   class="form-control required-entry"/></td>										
										<td></td>
									</tr>
									<?php
								else:
									foreach ($arr_directed_by as $k=>$r):
										$directer_name = $r['name'];
										?>
										<tr id='tr_<?php echo $k ?>'>											
											<td>
												<input type="text" name='directer_name[]'
													   value="<?php echo $directer_name ?>"
													   class="form-control required-entry"/>
											</td>											
											<td>
												<?php if($k > 0):  ?>
												<span class="glyphicon glyphicon-minus addBtnRemove"></span>
												<?php endif;  ?>
											</td>
										</tr>
										<?php
									endforeach;
								endif;
								?>
								</tbody>
							</table>			
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">Produced by <span class="req">*</span></label>
						<div class="control-element col-md-4">
							<table class="table table-bordered table-hover tbl-produced-by">
								<thead>
								<tr>
									<th>Name</th>									
									<th style="width:10px"><span class="glyphicon glyphicon-plus addBtn"></span></th>
								</tr>
								</thead>
								<tbody>
								<?php
								$arr_produced_by = unserialize($produced_by);
								
								if ($produced_by == "" || count($arr_produced_by) == 0): ?>
									<tr id="tr_0">
										<td><input type="text" name='producer_name[]'
												   class="form-control required-entry"/></td>										
										<td></td>
									</tr>
									<?php
								else:
									foreach ($arr_produced_by as $k=>$r):
										$producer_name = $r['name'];
										?>
										<tr id='tr_<?php echo $k ?>'>											
											<td>
												<input type="text" name='producer_name[]'
													   value="<?php echo $producer_name ?>"
													   class="form-control required-entry"/>
											</td>											
											<td>
												<?php if($k > 0):  ?>
												<span class="glyphicon glyphicon-minus addBtnRemove"></span>
												<?php endif;  ?>
											</td>
										</tr>
										<?php
									endforeach;
								endif;
								?>
								</tbody>
							</table>			
						</div>
					</div>
					
					<div class="form-group">
						<label class="control-label col-md-3">Written by <span class="req">*</span></label>
						<div class="control-element col-md-4">
							<table class="table table-bordered table-hover tbl-written-by">
								<thead>
								<tr>
									<th>Name</th>									
									<th style="width:10px"><span class="glyphicon glyphicon-plus addBtn"></span></th>
								</tr>
								</thead>
								<tbody>
								<?php
								$arr_written_by = unserialize($written_by);
								
								if ($written_by == "" || count($arr_written_by) == 0): ?>
									<tr id="tr_0">
										<td><input type="text" name='writer_name[]'
												   class="form-control required-entry"/></td>										
										<td></td>
									</tr>
									<?php
								else:
									foreach ($arr_written_by as $k=>$r):
										$writer_name = $r['name'];
										?>
										<tr id='tr_<?php echo $k ?>'>											
											<td>
												<input type="text" name='writer_name[]'
													   value="<?php echo $writer_name ?>"
													   class="form-control required-entry"/>
											</td>											
											<td>
												<?php if($k > 0):  ?>
												<span class="glyphicon glyphicon-minus addBtnRemove"></span>
												<?php endif;  ?>
											</td>
										</tr>
										<?php
									endforeach;
								endif;
								?>
								</tbody>
							</table>			
						</div>
					</div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Music by <span class="req">*</span></label>
                        <div class="control-element col-md-4">
                            <table class="table table-bordered table-hover tbl-music-by">
                                <thead>
                                <tr>
                                    <th>Name</th>
                                    <th style="width:10px"><span class="glyphicon glyphicon-plus addBtn"></span></th>
                                </tr>
                                </thead>
                                <tbody>
                                <?php
                                $arr_music_by = unserialize($music_by);

                                if ($music_by == "" || count($arr_music_by) == 0): ?>
                                    <tr id="tr_0">
                                        <td><input type="text" name='musician_name[]'
                                                   class="form-control required-entry"/></td>
                                        <td></td>
                                    </tr>
                                <?php
                                else:
                                    foreach ($arr_music_by as $k=>$r):
                                        $musician_name = $r['name'];
                                        ?>
                                        <tr id='tr_<?php echo $k ?>'>
                                            <td>
                                                <input type="text" name='musician_name[]'
                                                       value="<?php echo $musician_name ?>"
                                                       class="form-control required-entry"/>
                                            </td>
                                            <td>
                                                <?php if($k > 0):  ?>
                                                    <span class="glyphicon glyphicon-minus addBtnRemove"></span>
                                                <?php endif;  ?>
                                            </td>
                                        </tr>
                                    <?php
                                    endforeach;
                                endif;
                                ?>
                                </tbody>
                            </table>
                        </div>
                    </div>



                    <?php if ($this->adminauth->has_role('402')): ?>
                        <div class="form-group">
                            <label class="control-label col-md-3">Is Published<span class="req">*</span></label>

                            <div class="control-element col-md-4">
                                <?php echo form_dropdown('status', unserialize(YESNO), set_value('status', $status), 'placeholder="Select" class="form-control required-entry" required="required"'); ?>
                                <?php echo form_error('status'); ?></div>
                        </div>
                    <?php endif; ?>

                    <h4 class="form-section">Meta Settings</h4>
                    <div class="form-group">
                        <label class="control-label col-md-3">Meta Title</label>
                        <div class="control-element col-md-4">
                            <input type="text" class="form-control" value="<?php echo
                            $page_action == "add" ?
                                set_value('meta_title') :
                                set_value('meta_title',$meta_title) ?>"
                                   name="meta_title" />
                        </div>
                    </div>

                    <?php if(SHOW_META_CONTENT === TRUE):?>
                        <div class="form-group">
                            <label class="control-label col-md-3">Meta Keywords</label>
                            <div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="meta_keywords"><?php echo $page_action == "add" ?
                                            set_value('meta_keywords') :
                                            set_value('meta_keywords',$meta_keywords) ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">Meta Description</label>
                            <div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="meta_description"><?php echo $page_action == "add" ?
                                            set_value('meta_description') :
                                            set_value('meta_description',$meta_description) ?></textarea>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">OG Type</label>
                            <div class="control-element col-md-4">
                                <input type="text" class="form-control" value="<?php echo
                                $page_action == "add" ?
                                    set_value('og_type') :
                                    set_value('og_type',$og_type) ?>"
                                       name="og_type" />
                                <span class="help-block font-green">The type of the object, such as 'article'</span></div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">OG Title</label>
                            <div class="control-element col-md-4">
                                <input type="text" class="form-control" value="<?php echo
                                $page_action == "add" ?
                                    set_value('og_title') :
                                    set_value('og_title',$og_title) ?>"
                                       name="og_title" />
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="control-label col-md-3">OG Description</label>
                            <div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="og_description"><?php echo $page_action == "add" ?
                                            set_value('og_description') :
                                            set_value('og_description',$og_description) ?></textarea>
                            </div>
                        </div>
                    <?php endif; ?>


                    <?php if (
                        $page_action == 'add'
                        || ($page_action == 'edit' && $this->adminauth->has_role('401') && $status == 0)
                        || ($page_action == 'edit' && $this->adminauth->has_role('402'))
                      ): ?>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <div class="btn-group">
                                    <input type="hidden" id="hidden-action" name="hidden-action" value="1">
                                    <button type="submit" data-id="1" class="btn btn-submit green">Save & Return
                                    </button>
                                    <button type="submit" data-id="2" class="btn btn-submit btn-default">Save & New
                                    </button>
                                    <button type="submit" data-id="3" class="btn btn-submit btn-default">Save & Edit
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>

                    <script type="text/javascript">


                        $('.addBtn').on('click', function () {
                            addTableRow($(this).parents('table'));


                        });

                        $('.addBtnRemove').click(function () {
                            $(this).closest('tr').remove();
                        });


                        function addTableRow(tbl) {
                            $i = tbl.find('tbody').find('tr').length;

                            if (tbl.hasClass('tbl-cast'))
                            {
                                if ($i == 0) {

                                } else {
                                    $_h = '<tr>' +
                                        '<td><input type="text"  name="actor[]" class="form-control input-md"/></td>' +
                                        '<td><input type="text"  name="character[]" class="form-control input-md"/></td>' +
                                        '<td><span class="glyphicon glyphicon-minus addBtnRemove"></span></td>' +
                                        '</tr>';
                                }

                            } else if (tbl.hasClass('tbl-directed-by'))
                            {
                                if ($i == 0) {

                                } else {
                                    $_h = '<tr>' +
                                        '<td><input type="text"  name="directer_name[]" class="form-control input-md"/></td>' +
                                        '<td><span class="glyphicon glyphicon-minus addBtnRemove"></span></td>' +
                                        '</tr>';
                                }
                            }
                            else if (tbl.hasClass('tbl-produced-by'))
                            {
                                if ($i == 0) {

                                } else {
                                    $_h = '<tr>' +
                                        '<td><input type="text"  name="producer_name[]" class="form-control input-md"/></td>' +
                                        '<td><span class="glyphicon glyphicon-minus addBtnRemove"></span></td>' +
                                        '</tr>';
                                }
                            }
                            else if (tbl.hasClass('tbl-written-by'))
                            {
                                if ($i == 0) {

                                } else {
                                    $_h = '<tr>' +
                                        '<td><input type="text"  name="writer_name[]" class="form-control input-md"/></td>' +
                                        '<td><span class="glyphicon glyphicon-minus addBtnRemove"></span></td>' +
                                        '</tr>';
                                }
                            }
                            else if (tbl.hasClass('tbl-music-by'))
                            {
                                if ($i == 0) {

                                } else {
                                    $_h = '<tr>' +
                                        '<td><input type="text"  name="musician_name[]" class="form-control input-md"/></td>' +
                                        '<td><span class="glyphicon glyphicon-minus addBtnRemove"></span></td>' +
                                        '</tr>';
                                }
                            }

                            var tempTr = $_h;
                            tbl.append(tempTr);

                            tbl.find("tr:last input:first").focus();


                            $(document.body).on('click', '.addBtnRemove', function (e) {
                                $(this).closest('tr').remove();
                            });
                        }



						$('.date-picker').datepicker({
								format: "yyyy-mm-dd",
								startDate: '1d',
								autoclose: true
							});
								
                        $('.required-entry').attr('required', 'required');
                        $('#edit_form').validate({
                            errorElement: 'span',
                            errorClass: 'validation-advice',
                            ignore: "",
                            submitHandler: function (form) {
                                form.submit();
                            }
                        });
                    </script>
                    </form>
                </div>
            </div>
        </div>
    </div>


<?php elseif($page_action == 'banner'): ?>

    <h3 class="page-title"><?php echo $movie_data->movie_name ?> : Upload Banner Images</h3>
    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn default" href="<?php echo site_url('admin/movie/edit/'.$enc_id) ?>"><i class="fa fa-chevron-left"></i> Back</a>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12"> <?php echo get_message() ?>
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light bordered">
                <div class="portlet-body">
                    <table width="100%" border="0" cellspacing="0" cellpadding="0">
                        <tr>
                            <td><form>
                                    <input id="file_upload" name="file_upload" type="file" multiple>
                                </form>
                                <span class="help-block red">Image Size:<?php echo MOVIE_GALLERY_IMG_W ?>px x <?php echo MOVIE_GALLERY_IMG_H ?>px, Image Type:jpg</span>
                                <div id="queue"></div></td>
                        </tr>
                    </table>

                    <script type="text/javascript">
                        <?php $timestamp = time();?>
                        $sel_numImg = 0;
                        $com_numImg = 0;
                        jQuery('#file_upload').uploadify({
                            'formData'     : {
                                'user_id'     : '<?php echo $this->encrypt_lib->encode($this->adminauth->user_id) ?>',
                            },
                            'buttonText' : 'Browse Images',
                            'queueID'  : 'queue',
                            'fileTypeDesc' : 'jpg Files',
                            'fileTypeExts' : '*.jpg',
                            'debug'	: false,
                            'swf'      : '<?php echo base_url() ?>skin_admin/global/plugins/uploadify/uploadify.swf',
                            'uploader' : '<?php echo site_url('admin/'.$slug.'/picture_upload/'.$enc_id); ?>',
                            'onSelect' : function(file) {
                                $sel_numImg++;
                            },
                            'onUploadSuccess' : function(file, data, response) {
                                $com_numImg++;
                                if($sel_numImg == $com_numImg ){
                                    window.location.reload();
                                }
                            }
                        });

                    </script>
                    <div class="grid">
                        <div id="thumbContent">
                            <div style="height:600px;overflow:scroll; position:relative; padding:0;margin: 0;">
                                <ul id="list">
                                    <?php
                                    $i = 1;
                                    foreach($imageList as $row):
                                        $id = $row['id'];
                                        $image = $row['image'];

                                        ?>
                                        <li id="recordsArray_<?php echo $id ?>">
                                            <div class="handle"><img src="<?php echo base_url().MOVIE_GRID_IMG_PATH.$image ?>" width="118" /></div>
                                            <div class="del"></div>
                                            <div class="number"><?php echo $i ?></div>
                                        </li>
                                        <?php
                                        $i++;
                                    endforeach;
                                    ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <script type="text/javascript">

                        jQuery("#list").sortable({ opacity: 0.6, cursor: 'move',handle : '.handle', update: function() {
                            var order = jQuery(this).sortable("serialize") + '&action=updateRecordsListings';
                            jQuery.post("<?php echo site_url('admin/'.$slug.'/sort_images/') ?>", order, function(theResponse){
                                jQuery('#thumbContent').find('li').each(function(i){
                                    jQuery(this).find('.number').html(i+1);
                                });
                            });
                        }});


                        jQuery('#list .del').click(function(event){
                            var _self = jQuery(this);
                            var _id = _self.parent().attr('id');
                            message = 'Are you sure you want to delete?';
                            jConfirm('Are you sure you want to delete?','Confirmation',function(r){
                                if( r == true ) {
                                    jQuery('#'+_id).remove();
                                    jQuery.post("<?php echo site_url('admin/'.$slug.'/delete_images/') ?>", {'id':_id}, function(theResponse){	});
                                    jQuery('#thumbContent').find('li').each(function(i){
                                        jQuery(this).find('.number').html(i+1);
                                    });
                                }
                            });

                        });


                    </script>

                </div>
            </div>
        </div>
    </div>
<?php endif;//$page_action ?>
</div>
<!-- END PAGE CONTENT-->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER -->
