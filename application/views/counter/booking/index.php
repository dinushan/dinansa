<link href="<?php echo base_url() ?>skin_admin/global/plugins/jquery.event.drag-2.2/style.css" rel="stylesheet" type="text/css"/>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery.event.drag-2.2/jquery.event.drag-2.2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery.event.drag-2.2/jquery.event.drag.live-2.2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery.event.drag-2.2/jquery.event.drop-2.2.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>skin_admin/global/plugins/jquery.event.drag-2.2/jquery.event.drop.live-2.2.js" type="text/javascript"></script>

<!-- BEGIN CONTAINER -->

<div class="page-container">
<?php $this->load->view('counter/inc_sidebar_navigation.php'); ?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">
<?php if ($page_action == ''): ?>
    <h3 class="page-title">Counter bookings</h3>

    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <?php if(
                $this->counterauth->has_role('201')
                ):?>
                <a onclick="setLocation('<?php echo site_url('counters/' . $slug . '/add') ?>')" class="btn green"><i
                        class="fa fa-plus-circle"></i> Add New</a>
                <?php endif; ?>
            </div>
        </div>
    </div>

    <?php if(
    $this->counterauth->has_role('201')
    ):?>
    <?php echo get_message() ?>
    <div class="row">
        <div class="col-md-12">

            <div class="table-container">

                <table class="table table-striped table-bordered table-hover" id="datatable_ajax"
                       data-order-col = "0"
                       data-order-type = "asc"
                       data-ajax_url="<?php echo base_url('counters/' . $slug . '/ajax_list') ?>">
                    <colgroup>
                        <col width="40"/>
                        <col width="100">
                        <col>
                        <col width="100">
                        <col width="300">
                    </colgroup>
                    <thead>
                    <tr role="row" class="heading">
                        <th>ID</th>
                        <th>Reference no</th>
                        <th>Name</th>
                        <th>Mobile</th>
                        <th class="sorting_disabled">Action</th>
                    </tr>
                    <tr role="row" class="filter">
                        <td>
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="reference_no">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="first_name">
                        </td>
                        <td>
                            <input type="text" class="form-control form-filter input-sm" name="mobile_no">
                        </td>
                        <td>
                            <div class="margin-bottom-5">
                                <button class="btn btn-sm yellow filter-submit margin-bottom"><i
                                        class="fa fa-search"></i> Search
                                </button>
                            </div>
                            <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset
                            </button>
                        </td>
                    </tr>
                    </thead>
                    <tbody>
                    </tbody>
                </table>

            </div>
        </div>
    </div>

    <script>
        jQuery(document).ready(function() {
            TableAjax.init();
        });
    </script>

    <?php endif; ?>


<?php elseif ($page_action == 'add' || $page_action == 'edit'): ?>
    <h3 class="page-title"><?php echo $page_action == "add" ? 'Add New' : 'Edit' ?> Movie</h3>
    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn default" href="<?php echo site_url('counters/' . $slug . '/') ?>"><i
                        class="fa fa-chevron-left"></i> Back</a>

                <?php /*if($page_action == 'edit'): */?><!--
                    <a class="btn blue" href="<?php /*echo site_url('counters/'.$slug.'/photos/'.$enc_id) */?>"><i class="fa fa-plus"></i> Photos</a>
                --><?php /*endif; */?>
            </div>
        </div>
    </div>

    <div class="row">
        <div class="col-md-12"> <?php echo get_message() ?>
            <!-- BEGIN VALIDATION STATES-->
            <div class="portlet light bordered">

                <div class="portlet-body form">
                    <!-- BEGIN FORM-->
                    <?php
                    $attr = array("id" => "edit_form", "class" => "form-horizontal", "enctype" => "multipart/form-data");
                    echo form_open(current_url(), $attr);
                    ?>
                    <?php
                    if ($page_action == 'edit')
                    {
                        $movie_name = $booking_data->movie_name;
                        $movie_year = $booking_data->movie_year;
                        $movie_content=$booking_data->movie_content;
                        $trailer = $booking_data->trailer;
                        $duration_h = $booking_data->duration_h;
                        $duration_m = $booking_data->duration_m;
                        $imdb_rate = $booking_data->imdb_rate;
                        $landscape_image = $booking_data->landscape_image;
                        $portrait_image = $booking_data->portrait_image;

                        $cast = $booking_data->cast;
						$directed_by = $booking_data->directed_by;
						$produced_by = $booking_data->produced_by;
						$written_by = $booking_data->written_by;
						$music_by = $booking_data->music_by;

                        $date_release = $booking_data->date_release;
                        $date_end = $booking_data->date_end;
                        $status = $booking_data->status;

                    } else {
                        $available_seats='';
                        //$theater_list=array();
                        $theater_selected='';
                        $movie_date='';

                        $last_name='';
                        $first_name='';
                        $mobile_no='';
                        $user_id='';
                        $movie_list=array();
                        $movie_selected='';
                        $time_list=array();
                        $time_selected='';
                        $time_list=array();
                        $time_selected='';
                        $seat_plan=array();
                    }
                    ?>

                    <h4 class="form-section">General</h4>

                    <div class="form-group">
                        <label class="control-label col-md-3">Reference numer<span class="req">*</span></label>

                        <div class="control-element col-md-4"><input readonly name="reference_number" type="text" id="reference_number"
                                                                     class="form-control required-entry"
                                                                     value="<?php echo set_value('reference_number', $reference_number) ?>"/>
                            <?php echo form_error('reference_number'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Customer phone<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <input  name="user_id" type="hidden" id="user_id"
                                           class="form-control required-entry"
                                           value="<?php echo set_value('user_id', $user_id) ?>"/>
                            <input name="mobile_no" type="text" id="mobile_no"
                                                                     class="form-control required-entry"
                                                                     value="<?php echo set_value('mobile_no', $mobile_no) ?>"/>
                            <?php echo form_error('mobile_no'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Customer first name<span class="req">*</span></label>

                        <div class="control-element col-md-4"><input id="first_name" name="first_name" type="text"
                                                                     class="form-control required-entry"
                                                                     value="<?php echo set_value('first_name', $first_name) ?>"/>
                            <?php echo form_error('first_name'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Customer last name<span class="req">*</span></label>

                        <div class="control-element col-md-4"><input id="last_name" name="last_name" type="text"
                                                                     class="form-control required-entry"
                                                                     value="<?php echo set_value('last_name', $last_name) ?>"/>
                            <?php echo form_error('last_name'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Offer code<span class="req">*</span></label>

                        <div class="control-element col-md-4"><input id="offer_code" name="offer_code" type="text"
                                                                     class="form-control "
                                                                     value="<?php echo set_value('offer_code', $last_name) ?>"/>
                            <?php echo form_error('offer_code'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Date<span class="req">*</span></label>

                        <div class="control-element col-md-4">
                            <div class="input-group date date-picker " data-date-format="yyyy-mm-dd">
                                <input type="text" class="form-control" id="movie_date" name="movie_date" value="<?php echo set_value('movie_date',$movie_date) ?>">
                                <span class="input-group-btn">
									<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
									</span>
                            </div>
                            <?php echo form_error('movie_date'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Theater<span class="req">*</span></label>
                        <div class="control-element col-md-4">
                            <?php echo form_dropdown('theater', $theater_list, set_value('theater', $theater_selected), 'class="form-control required-entry " id="theater" '); ?>
                            <?php echo form_error('theater'); ?>
                        </div>
                    </div>


                    <div class="form-group">
                        <label class="control-label col-md-3">Movie<span class="req">*</span></label>
                        <div class="control-element col-md-4">
                            <?php echo form_dropdown('movie', $movie_list, set_value('movie', $movie_selected), 'class="form-control required-entry " id="movie" '); ?>
                            <?php echo form_error('movie'); ?>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="control-label col-md-3">Time slot<span class="req">*</span></label>
                        <div class="control-element col-md-4">
                            <?php echo form_dropdown('time_slot', $time_list, set_value('time_slot', $time_selected), 'class="form-control required-entry "  id="time_slot" '); ?>
                            <?php echo form_error('time_slot'); ?>
                        </div>
                    </div>




                    <div class="form-group">
                        <label class="control-label col-md-3">Available seats<span class="req"></span></label>
                        <div class="control-element col-md-4" id="">
                            <input type="text" readonly="readonly" class="form-control" name="available_seats" id="available_seats" value="<?php echo set_value('available_seats',$available_seats) ?>">
                        </div>
                    </div>




                    <?php if (
                        $page_action == 'add'
                        || ($page_action == 'edit' && $this->counterauth->has_role('201') && $status == 0)
                        || ($page_action == 'edit' && $this->counterauth->has_role('202'))
                      ): ?>
                    <div class="form-actions">
                        <div class="row">
                            <div class="col-md-offset-3 col-md-9">
                                <div class="btn-group">
                                    <input type="hidden" id="hidden-action" name="hidden-action" value="1">
                                    <button type="submit" data-id="1" class="btn btn-submit green">Save & Return
                                    </button>

                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endif; ?>


                    <div id="box-seat-plan" class="form-actions">

                        <div class="block-plan-wrapper"><div class="block-plan-container"></div></div>

                        <span class="help-block">
                                       - For move multiple seats - select seats and use "a" key or "d" key move<br />
                                       - For disable seat - select a seat and press "Delete" key<br />
                                    </span>
                    </div>


                    <script type="text/javascript">


                        $("#mobile_no").select2({
                            placeholder: '--- Select Item ---',
                           // maximumSelectionLength: 1,
                            minimumInputLength: 2,
                            tags: [],
                            ajax: {
                                url: '<?php echo site_url('counters/' . $slug . '/searchuser') ?>',
                                dataType: 'json',
                                type: "GET",
                                quietMillis: 50,
                                data: function (term) {
                                    return {
                                        term: term
                                    };
                                },
                                results: function (data) {
                                    //console.log(data);

                                    return {
                                        results: $.map(data, function (item) {
                                            //console.log(item.first_name);
                                            return {
                                                text: item.first_name+' '+item.last_name+' ('+item.phone+')',
                                                slug: item.phone,
                                                id: item.phone,
                                                user_id:item.user_id,
                                                first_name:item.first_name,
                                                last_name:item.last_name,
                                               // data: [{first_name: item.first_name, last_name: item.last_name}]
                                            }
                                        })
                                    };
                                }

                            }
                        });


                        $("#mobile_no").on('change', function(e) {
                            if($(this).select2('data')){
                                $('#first_name').val($(this).select2('data')[0].first_name);
                                $('#last_name').val($(this).select2('data')[0].last_name);
                                $('#user_id').val($(this).select2('data')[0].user_id);
                            }
                            else{
                                $('#first_name').val('');
                                $('#last_name').val('');
                            }
                            //console.log($(this).select2('data')[0].first_name);
                        });



						$('.date-picker').datepicker({
								format: "yyyy-mm-dd",
								startDate: '1d',
								autoclose: true
							});
								
                        $('.required-entry').attr('required', 'required');
                        $('#edit_form').validate({
                            errorElement: 'span',
                            errorClass: 'validation-advice',
                            ignore: "",
                            submitHandler: function (form) {
                                form.submit();
                            }
                        });

                        $("#movie_date").on('change', function(e) {
                            select_movie();
                        });
                        $("#theater").on('change', function(e) {
                            select_movie();
                        });

                        function select_movie() {
                            var movie_date=$("#movie_date").val().trim();
                            var theater=$("#theater").val().trim();
                            if(movie_date!='' && theater!=''){
                                $.ajax({
                                    type: "POST",
                                    url: '<?php echo site_url('counters/' . $slug . '/select_movie') ?>',
                                    data: {theater: theater,movie_date: movie_date},
                                    dataType: "json",
                                    cache:false,
                                    success:
                                        function(data){
                                            $('#time_slot').empty();
                                            var opt = $('<option />');
                                            opt.val('');
                                            opt.text('Select time slot');

                                            $('#movie').empty();
                                            var opt = $('<option />');
                                            opt.val('');
                                            opt.text('Select movie');
                                            $('#movie').append(opt);



                                            $.each(data,function(id,val)
                                            {
                                                var opt = $('<option />');
                                                opt.val(id);
                                                opt.text(val);
                                                $('#movie').append(opt);
                                            });
                                        }
                                });// you have missed this bracket
                                return false;
                            }
                            else{
                                $('#time_slot').empty();
                                var opt = $('<option />');
                                opt.val('');
                                opt.text('Select time slot');

                                $('#movie').empty();
                                var opt = $('<option />');
                                opt.val('');
                                opt.text('Select movie');
                                $('#movie').append(opt);
                            }
                        }

                        $("#movie").on('change', function(e) {
                            var movie=$("#movie").val().trim();
                            var movie_date=$("#movie_date").val().trim();
                            var theater=$("#theater").val().trim();
                            $.ajax({
                                type: "POST",
                                url: '<?php echo site_url('counters/' . $slug . '/select_time_slots') ?>',
                                data: {movie: movie,theater: theater,movie_date: movie_date},
                                dataType: "json",
                                cache:false,
                                success:
                                    function(data){
                                        $('#time_slot').empty();
                                        var opt = $('<option />');
                                        opt.val('');
                                        opt.text('Select time slot');
                                        $('#time_slot').append(opt);

                                        $.each(data,function(id,val)
                                        {
                                            var opt = $('<option />');
                                            opt.val(id);
                                            opt.text(val);
                                            $('#time_slot').append(opt);
                                        });
                                    }
                            });// you have missed this bracket
                            return false;
                        });

                        $("#time_slot").on('change', function(e) {
                            var movie=$("#movie").val().trim();
                            var movie_date=$("#movie_date").val().trim();
                            var theater=$("#theater").val().trim();
                            var time_slot=$("#time_slot").val().trim();
                            if(movie!='' && movie_date!='' && theater!='' && time_slot!='') {
                                $.ajax({
                                    type: "POST",
                                    url: '<?php echo site_url('counters/' . $slug . '/seat_plan_data') ?>',
                                    data: {
                                        movie: movie,
                                        movie_date: movie_date,
                                        theater: theater,
                                        time_slot: time_slot
                                    },
                                    dataType: "json",
                                    cache: false,
                                    success: function (data) {
                                      //  console.log(data.seat_plan);
                                      //  set_validation(data.seat_plan);
                                        if(data.seat_plan.length > 0){
                                            set_validation(data);
                                            $('#available_seats').val(data.available_seat_count);
                                        }

                                    }
                                });// you have missed this bracket
                            }
                            return false;
                        });

                    </script>
                    </form>






                    <script type="text/javascript">
                        var el = $("#box-seat-plan");
                        var tab_st = 'draw';

                        $('#btn-draw').on('click', function () {
                            $('.nav-tabs').find('.active').removeClass('active');
                            $(this).parent('li').addClass('active');

                            $('#div-add-rows').show();

                            $('.block-plan-container').removeClass('editor-label');
                            $('.block-plan-container').addClass('editor-draw');

                            //$('.block-plan-container').find('.seat_item').addClass('d');
                            tab_st = 'draw';

                            //$( '.seat_item').removeAttr("contenteditable");
                        });
                        $('#btn-draw').click();

                        $('#btn-label').on('click', function () {
                            $('.nav-tabs').find('.active').removeClass('active');
                            $(this).parent('li').addClass('active');

                            $('#div-add-rows').hide();

                            $('.block-plan-container').addClass('editor-label');
                            $('.block-plan-container').removeClass('editor-draw');

                            //$('.block-plan-container').find('.seat_item').removeClass('d');
                            tab_st = 'label';
                            $( '.seat_item' ).removeClass("selected_seat");

                            //$( '.seat_item').attr("contenteditable","true");

                        });



                        $('.addBtn').on('click', function () {
                            addTableRow($(this).parents('table'));
                        });

                        $('.addBtnRemove').click(function () {
                            $(this).closest('tr').remove();
                        })


                        function addTableRow(tbl)
                        {
                            $i = tbl.find('tbody').find('tr').length;

                            if($i == 0){
                                $_h = '<tr>' +
                                    '<td>1</td>' +
                                    '<td><input type="text"  name="seats[]" class="form-control input-md required-entry"/></td>' +
                                    '<td></td>' +
                                    '</tr>';
                            }else{
                                $_h = '<tr>' +
                                    '<td></td>' +
                                    '<td><input type="text"  name="seats[]" class="form-control input-md required-entry"/></td>' +
                                    '<td><span class="glyphicon glyphicon-minus addBtnRemove" id="addBtn_' + $i + '"></span></td>' +
                                    '</tr>';
                            }


                            var tempTr = $_h;
                            tbl.append(tempTr);

                            var str = 'A';
                            tbl.find('tbody').find('tr').each(function(index){
                                //$(this).find('td:first').html(index+1);
                                $(this).find('td:first').html(str);

                                str = nextChar(str);
                            });



                            $(document.body).on('click', '.addBtnRemove', function (e) {
                                $(this).closest('tr').remove();
                            });
                        }


                        $('.required-entry').attr('required', 'required');

                        $('#edit_form').validate({
                            errorElement: 'span',
                            errorClass: 'validation-advice',
                            ignore: "",
                            submitHandler: function (form) {
                                set_validation('');
                            }
                        });

                        var ctrl = false;
                        var shift = false;
                        var $seats_with = 26;
                        var $space = 30;
                        var $seats = [];

                        var $seat_obj = <?php echo json_encode($seat_plan) ?>;

                        function set_validation(_seat_obj)
                        {

                            var $sc =  0;
                            $seats = [];

                            $('.tbl-block-seat').find('input[type="text"]').each(function(){
                                $sc += Number($(this).val());

                                $seats.push($(this).val());
                            })


                            $('#form-actions-save-seat').show();
                            generate_seat_plan(_seat_obj);

                        }

                        if($seat_obj.length > 0){
                            set_validation($seat_obj);
                        }

                        <?php
                        //if($stadium_ground_plan->status != 2):
                        ?>
                        function ajax_send_seat_plan()
                        {
                            jsonObj = [];

                            $( '#item-container' ).find('.seat_item').each(function(index, element)
                            {
                                $offset = $(this).position();


                                $seat_id = $(this).data('id');
                                $seat_row = $(this).data('row');
                                $seat_no = $(this).html();
                                $top = Number($offset.top);
                                $left = Number($offset.left);
                                if ( $( this ).hasClass('hide_seat') ){
                                    $seal_status = 0;
                                }else{
                                    $seal_status = 1;
                                }

                                item = {}
                                //item ["seat_id"] = $seat_id;
                                item ["seat_row"] = $seat_row;
                                item ["seat_no"] = $seat_no;
                                item ["top"] = $top;
                                item ["left"] = $left;
                                item ["status"] = $seal_status;
                                jsonObj.push(item);

                            });

                            var obj_seat = JSON.stringify($seats);
                            var obj_seat_plan = JSON.stringify(jsonObj);

                            Metronic.blockUI({
                                target: el,
                                animate: true,
                                overlayColor: 'none'
                            });

                            var theater=$("#theater").val().trim();
                            $.post(base_url+'admin/theaterSeatPlan/ajax_save_seat_plan',{
                                theater_id:theater,
                                seat:obj_seat,
                                seat_plan:obj_seat_plan
                            },function(jsonData){
                                Metronic.unblockUI(el);
                                if(jsonData['status'] == true){
                                    jAlert('Seat Plan has been updated','Success');
                                }else{
                                    jAlert(jsonData['message'],'Validation');
                                }
                            },'json');
                        }

                        function ajax_update_seat_plan()
                        {
                            jsonObj = [];

                            $( '#item-container' ).find('.seat_item').each(function(index, element)
                            {
                                $offset = $(this).position();


                                $seat_id = $(this).data('id');
                                $seat_row = $(this).data('row');
                                $seat_no = $(this).html();
                                $top = Number($offset.top);
                                $left = Number($offset.left);
                                if ( $( this ).hasClass('hide_seat') ){
                                    $seal_status = 0;
                                }else{
                                    $seal_status = 1;
                                }

                                item = {}
                                item ["seat_id"] = $seat_id;
                                item ["seat_row"] = $seat_row;
                                item ["seat_no"] = $seat_no;
                                item ["top"] = $top;
                                item ["left"] = $left;
                                item ["status"] = $seal_status;
                                jsonObj.push(item);

                            });

                            var obj_seat = JSON.stringify($seats);
                            var obj_seat_plan = JSON.stringify(jsonObj);

                            Metronic.blockUI({
                                target: el,
                                animate: true,
                                overlayColor: 'none'
                            });

                            var theater=$("#theater").val().trim();
                            $.post(base_url+'admin/theaterSeatPlan/ajax_update_seat_plan',{
                                theater_id:theater,
                                seat:obj_seat,
                                seat_plan:obj_seat_plan
                            },function(jsonData){
                                Metronic.unblockUI(el);
                                if(jsonData['status'] == true){
                                    jAlert('Seat Plan has been updated','Success');
                                }else{
                                    jAlert(jsonData['message'],'Validation');
                                }
                            },'json');
                        }

                        $('.btn-save').on('click',function()
                        {
                            ajax_send_seat_plan();
                        });

                        $('.btn-update').on('click',function()
                        {
                            ajax_update_seat_plan();
                        });
                        <?php
                        //endif;
                        ?>

                        function generate_seat_plan(data1)
                        {
                            var data= data1.seat_plan;
                            var seat_plan_booked= data1.seat_plan_booked;
                            var seat_plan_hold= data1.seat_plan_hold;
                            var seat_plan_selected=data1.seat_plan_selected;
                           //  console.log(seat_plan_booked);
                            // var data= data1.seat_plan;

                            $('.block-plan-container').html('');
                            $('#item-container').html('');


                            $html_seat_lbl = '<div id="item-container"></div>';
                            $html_seat = '';


                            var str = 'A';

                            if(data == '')
                            {
                                for($i = 0; $i < $seats.length; $i++)
                                {



                                    $top = $i * ($space);

                                    var s = $seats[$i];
                                    for($j = 0; $j < $seats[$i]; $j++)
                                    {

                                        $seat_no = str + s;
                                        $left = $j * ($seats_with);
                                        $html_seat += '<div data-id="" data-row="'+$i+'" data-seat_no="'+$seat_no+'" class="row_'+$i+' seat_item" style="top:'+$top+'px;left:'+$left+'px;" title="'+$seat_no+'">'+$seat_no+'</div>';


                                        s--;
                                    }


                                    str = nextChar(str);

                                }
                            }
                            else{

                                for($i = 0; $i < data.length; $i++)
                                {
                                    //console.log(data[$i]);
                                    $seat_id = data[$i].id;
                                    $row = data[$i].row;
                                    $top = data[$i].y;
                                    $left = data[$i].x;
                                    $seat_no = data[$i].label;
                                    $status = data[$i].status;

                                    if ( $status == 0){
                                        $seal_class = ' hide_seat';
                                    }else{
                                        $seal_class = '';
                                    }

                                   // $html_seat += '<div data-id="'+$seat_id+'" data-row="'+$row+'" data-seat_no="'+$seat_no+'" class="row_'+$row+$seal_class+' seat_item" style="top:'+$top+'px;left:'+$left+'px;" title="'+$seat_no+'">'+$seat_no+'</div>';

                                   // console.log($seat_no);
                                   // console.log(seat_plan_booked);

                                    if($.inArray($seat_no, seat_plan_booked ) !=-1 ){
                                        $html_seat += '<div data-id="'+$seat_id+'" data-row="'+$row+'" data-seat_no="'+$seat_no+'" class="row_'+$row+$seal_class+' seat_item booked_seat" style="top:'+$top+'px;left:'+$left+'px;" title="'+$seat_no+'">'+$seat_no+'</div>';
                                    }

                                   else if($.inArray($seat_no, seat_plan_hold ) !=-1 ){
                                        $html_seat += '<div data-id="'+$seat_id+'" data-row="'+$row+'" data-seat_no="'+$seat_no+'" class="row_'+$row+$seal_class+' seat_item hold_seat" style="top:'+$top+'px;left:'+$left+'px;" title="'+$seat_no+'">'+$seat_no+'</div>';
                                    }
                                    else if($.inArray($seat_no, seat_plan_selected ) !=-1 ){
                                        $html_seat += '<div data-id="'+$seat_id+'" data-row="'+$row+'" data-seat_no="'+$seat_no+'" class="row_'+$row+$seal_class+' seat_item selected_seat" style="top:'+$top+'px;left:'+$left+'px;" title="'+$seat_no+'">'+$seat_no+'</div>';
                                    }


                                    else{
                                        $html_seat += '<div data-id="'+$seat_id+'" data-row="'+$row+'" data-seat_no="'+$seat_no+'" class="row_'+$row+$seal_class+' seat_item" style="top:'+$top+'px;left:'+$left+'px;" title="'+$seat_no+'">'+$seat_no+'</div>';
                                    }
                                }
                            }


                            $maxSeatCount = Math.max.apply(Math,$seats)+100;

                            //row lable
                            /*for($i = 0; $i < $seats.length; $i++)
                             {
                             $top = $i * ($seats_with);
                             $html_seat_lbl += '<div class="seat_lbl" style="top:'+($seats_with+$top)+'px;left:0px;">'+($i+1)+'</div>';
                             }*/
                            //colum lable
                            /*for($j = 0; $j < $maxSeatCount; $j++)
                             {
                             $left = $j * ($seats_with);
                             $html_seat_lbl += '<div class="seat_lbl" style="left:'+($seats_with+$left)+'px;top:0px;">'+($j+1)+'</div>';
                             }*/

                            $('.block-plan-container').html($html_seat_lbl);
                            $('#item-container').html($html_seat);

                            $('.block-plan-container').width(($seats_with*$maxSeatCount));
                            $('#item-container').height($seats_with*$seats.length+500);

                            $( '#item-container' )
                                .drag("start",function( ev, dd ){
                                    return $('<div class="selection" />')
                                        .css('opacity', .65 )
                                        .appendTo( document.body );
                                })
                                .drag(function( ev, dd )
                                {
                                    if(tab_st == 'draw')
                                    {
                                        $( dd.proxy ).css({
                                            top: Math.min( ev.pageY, dd.startY ),
                                            left: Math.min( ev.pageX, dd.startX ),
                                            height: Math.abs( ev.pageY - dd.startY ),
                                            width: Math.abs( ev.pageX - dd.startX )
                                        });
                                    }
                                })
                                .drag("end",function( ev, dd ){
                                    $( dd.proxy ).remove();
                                });


                            $('.seat_item')
                                .click(function(){
                                    /*if(tab_st == 'draw')
                                    {
                                        $( this ).toggleClass("selected_seat");
                                    }else{
                                        $( this).attr('contenteditable','true');
                                        $( this ).focus();
                                    }*/

                                    if( !$( this ).hasClass( "booked_seat" )){
                                     if(tab_st == 'draw')
                                     {
                                     $( this ).toggleClass("selected_seat");
                                     }else{
                                     $( this).attr('contenteditable','true');
                                     $( this ).focus();
                                     }






                                        var movie=$("#movie").val().trim();
                                        var movie_date=$("#movie_date").val().trim();
                                        var theater=$("#theater").val().trim();
                                        var time_slot=$("#time_slot").val().trim();

                                       // alert($(this).hasClass('.selected_seat'));
                                       // alert(time_slot);
                                        var selected = '',status='';
                                        if($(this).hasClass('selected_seat') && !$(this).hasClass('hold_seat')){
                                            $offset = $(this).position();
                                            selected=$(this).data('seat_no');
                                            status=2;

                                           // alert(selected);
                                           // selected=$(this).data('seat_no')
                                        }

                                         if(!$(this).hasClass('selected_seat') && !$(this).hasClass('hold_seat')){
                                            selected=$(this).data('seat_no');
                                            status=0;
                                        }
                                       // console.log($(this).hasClass('selected_seat'));
                                      //  console.log($(this).hasClass('hold_seat'));

                                       // alert(status);

                                       /* $( '#item-container' ).find('.selected_seat').each(function(index, element) {
                                            $offset = $(this).position();
                                            selected.push($(this).data('seat_no'));
                                        });*/

                                        //var movie=$("#movie").val().trim();
                                        var reference_number=$("#reference_number").val().trim();
                                            if(movie!='' && movie_date!='' && theater!='' && time_slot!='' && selected!='' ) {
                                                $.ajax({
                                                    type: "POST",
                                                    url: '<?php echo site_url('counters/' . $slug . '/selected_seats') ?>',
                                                    data: {
                                                        selected: selected,
                                                        reference_number: reference_number,
                                                        movie: movie,
                                                        movie_date: movie_date,
                                                        theater: theater,
                                                        status: status,
                                                        time_slot: time_slot
                                                    },
                                                    dataType: "json",
                                                    cache: false,
                                                    success: function (data) {
                                                        $("#reference_number").val(data);
                                                        // console.log(data);
                                                    }
                                                });
                                            }
                                     }


                                })
                                .drag("init",function(){
                                    if(tab_st == 'draw')
                                    {
                                        if ( $( this ).is('.selected_seat') )
                                            return $('.selected_seat');
                                    }
                                })
                                .drag(function( ev, dd ){
                                            /* if(tab_st == 'draw')
                                             {
                                             $( this ).css({
                                             top: ( dd.offsetY / 2 ) * 2,
                                             left: ( dd.offsetX / 2 ) * 2
                                             });
                                             }*/
                                    }
                                    ,{ relative:true })
                                .mouseout(function() {
                                    $( this).removeAttr('contenteditable');
                                });




                            $('.seat_item')
                                .drop("init",function(){
                                    if(tab_st == 'draw')
                                    {
                                        if(!ctrl){
                                            $( '.seat_item' ).removeClass("selected_seat");
                                        }
                                    }
                                })
                                .drop("start",function(){
                                    //$( this ).addClass("selected");
                                })
                                .drop(function( ev, dd ){
                                    if(tab_st == 'draw')
                                    {
                                        $( this ).toggleClass("selected_seat");
                                    }
                                });

                            $.drop({ multi: true });

                            $(document).keydown(function(e)
                            {
                                console.log(e.which);
                                ctrl = false;
                                shift = false;

                                if (e.shiftKey)
                                {
                                    shift = true;
                                }

                                switch (e.which) {
                                    case 65://a key
                                        move('left');
                                        break;
                                    case 68://d key
                                        move('right');
                                        break;
                                    case 87://w key
                                        move('top');
                                        break;
                                    case 90://z key
                                        move('bottom');
                                        break;
                                    case 46://delete key
                                        hide_seat();
                                        break;
                                    case 17://ctrl key
                                        ctrl = true;
                                        break;
                                }

                            })

                        }


                        function move(p){

                            if(tab_st == 'draw')
                            {
                                var t = 1;
                                if(shift){
                                    t = 12;
                                }

                                console.log(t);
                                $( '#item-container' ).find('.selected_seat').each(function(index, element) {
                                    $offset = $(this).position();

                                    $left = Number($offset.left);
                                    $top = Number($offset.top);

                                    if(p == 'left')
                                    {
                                        $left -= t;
                                    }
                                    if(p == 'right'){
                                        $left += t;
                                    }

                                    if(p == 'top')
                                    {
                                        $top -= t;
                                    }
                                    if(p == 'bottom'){
                                        $top += t;
                                    }

                                    $(this).css('left',$left+'px');
                                    $(this).css('top',$top+'px');
                                });
                            }
                        }

                        function hide_seat()
                        {
                            if(tab_st == 'draw')
                            {
                                $( '#item-container' ).find('.selected_seat').each(function(index, element) {

                                    if ( $( this ).hasClass('hide_seat') ){
                                        $( this ).removeClass('hide_seat');
                                    }else{
                                        $( this ).addClass('hide_seat');
                                    }

                                });
                            }
                        }

                    </script>












                </div>
            </div>
        </div>
    </div>



<?php endif;//$page_action ?>
</div>
<!-- END PAGE CONTENT-->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER --> 