<?php

class Migration_Drop_Floor_Table extends CI_Migration
{
    public function up()
    {

        $this->dbforge->drop_table('restaurant_floor', TRUE);

        $this->dbforge->drop_column('restaurant_tables', 'floor_id');
        $this->dbforge->drop_column('restaurant_tables', 'data');

        $fields = array(
            'chair_count' => array(
                'type' => 'INT',
                'constraint' => 11,
                'unsigned' => true,
                'after' => 'restaurant_id',
                'default' => 0,
            )
        );
        $this->dbforge->add_column('restaurant_tables', $fields);

        $this->db->query('ALTER TABLE `restaurant_tables`
CHANGE `label` `name` varchar(100) COLLATE \'utf8_general_ci\' NOT NULL AFTER `id`');


    }

    public function down()
    {

    }
}
