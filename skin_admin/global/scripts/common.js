/*!
 * jquery.inputmask.bundle
 * http://github.com/RobinHerbots/jquery.inputmask
 * Copyright (c) 2010 - 2014 Robin Herbots
 * Licensed under the MIT license (http://www.opensource.org/licenses/mit-license.php)
 * Version: 3.1.34
 */
!function(a){function b(a){var b=document.createElement("input"),c="on"+a,d=c in b;return d||(b.setAttribute(c,"return;"),d="function"==typeof b[c]),b=null,d}function c(a){var b="text"==a||"tel"==a;if(!b){var c=document.createElement("input");c.setAttribute("type",a),b="text"===c.type,c=null}return b}function d(b,c,e){var f=e.aliases[b];return f?(f.alias&&d(f.alias,void 0,e),a.extend(!0,e,f),a.extend(!0,e,c),!0):!1}function e(b,c){function d(a){function c(a,b,c,d){this.matches=[],this.isGroup=a||!1,this.isOptional=b||!1,this.isQuantifier=c||!1,this.isAlternator=d||!1,this.quantifier={min:1,max:1}}function d(a,c,d){var e=b.definitions[c],f=0==a.matches.length;if(d=void 0!=d?d:a.matches.length,e&&!l){for(var g=e.prevalidator,h=g?g.length:0,i=1;i<e.cardinality;i++){var j=h>=i?g[i-1]:[],k=j.validator,m=j.cardinality;a.matches.splice(d++,0,{fn:k?"string"==typeof k?new RegExp(k):new function(){this.test=k}:new RegExp("."),cardinality:m?m:1,optionality:a.isOptional,newBlockMarker:f,casing:e.casing,def:e.definitionSymbol||c,placeholder:e.placeholder,mask:c})}a.matches.splice(d++,0,{fn:e.validator?"string"==typeof e.validator?new RegExp(e.validator):new function(){this.test=e.validator}:new RegExp("."),cardinality:e.cardinality,optionality:a.isOptional,newBlockMarker:f,casing:e.casing,def:e.definitionSymbol||c,placeholder:e.placeholder,mask:c})}else a.matches.splice(d++,0,{fn:null,cardinality:0,optionality:a.isOptional,newBlockMarker:f,casing:null,def:c,placeholder:void 0,mask:c}),l=!1}for(var e,f,g,h,i,j,k=/(?:[?*+]|\{[0-9\+\*]+(?:,[0-9\+\*]*)?\})\??|[^.?*+^${[]()|\\]+|./g,l=!1,m=new c,n=[],o=[];e=k.exec(a);)switch(f=e[0],f.charAt(0)){case b.optionalmarker.end:case b.groupmarker.end:if(g=n.pop(),n.length>0){if(h=n[n.length-1],h.matches.push(g),h.isAlternator){i=n.pop();for(var p=0;p<i.matches.length;p++)i.matches[p].isGroup=!1;n.length>0?(h=n[n.length-1],h.matches.push(i)):m.matches.push(i)}}else m.matches.push(g);break;case b.optionalmarker.start:n.push(new c(!1,!0));break;case b.groupmarker.start:n.push(new c(!0));break;case b.quantifiermarker.start:var q=new c(!1,!1,!0);f=f.replace(/[{}]/g,"");var r=f.split(","),s=isNaN(r[0])?r[0]:parseInt(r[0]),t=1==r.length?s:isNaN(r[1])?r[1]:parseInt(r[1]);if(("*"==t||"+"==t)&&(s="*"==t?0:1),q.quantifier={min:s,max:t},n.length>0){var u=n[n.length-1].matches;if(e=u.pop(),!e.isGroup){var v=new c(!0);v.matches.push(e),e=v}u.push(e),u.push(q)}else{if(e=m.matches.pop(),!e.isGroup){var v=new c(!0);v.matches.push(e),e=v}m.matches.push(e),m.matches.push(q)}break;case b.escapeChar:l=!0;break;case b.alternatormarker:n.length>0?(h=n[n.length-1],j=h.matches.pop()):j=m.matches.pop(),j.isAlternator?n.push(j):(i=new c(!1,!1,!1,!0),i.matches.push(j),n.push(i));break;default:if(n.length>0){if(h=n[n.length-1],h.matches.length>0&&(j=h.matches[h.matches.length-1],j.isGroup&&(j.isGroup=!1,d(j,b.groupmarker.start,0),d(j,b.groupmarker.end))),d(h,f),h.isAlternator){i=n.pop();for(var p=0;p<i.matches.length;p++)i.matches[p].isGroup=!1;n.length>0?(h=n[n.length-1],h.matches.push(i)):m.matches.push(i)}}else m.matches.length>0&&(j=m.matches[m.matches.length-1],j.isGroup&&(j.isGroup=!1,d(j,b.groupmarker.start,0),d(j,b.groupmarker.end))),d(m,f)}return m.matches.length>0&&(j=m.matches[m.matches.length-1],j.isGroup&&(j.isGroup=!1,d(j,b.groupmarker.start,0),d(j,b.groupmarker.end)),o.push(m)),o}function e(c,e){if(b.numericInput&&b.multi!==!0){c=c.split("").reverse();for(var f=0;f<c.length;f++)c[f]==b.optionalmarker.start?c[f]=b.optionalmarker.end:c[f]==b.optionalmarker.end?c[f]=b.optionalmarker.start:c[f]==b.groupmarker.start?c[f]=b.groupmarker.end:c[f]==b.groupmarker.end&&(c[f]=b.groupmarker.start);c=c.join("")}if(void 0==c||""==c)return void 0;if(1==c.length&&0==b.greedy&&0!=b.repeat&&(b.placeholder=""),b.repeat>0||"*"==b.repeat||"+"==b.repeat){var g="*"==b.repeat?0:"+"==b.repeat?1:b.repeat;c=b.groupmarker.start+c+b.groupmarker.end+b.quantifiermarker.start+g+","+b.repeat+b.quantifiermarker.end}return void 0==a.inputmask.masksCache[c]&&(a.inputmask.masksCache[c]={mask:c,maskToken:d(c),validPositions:{},_buffer:void 0,buffer:void 0,tests:{},metadata:e}),a.extend(!0,{},a.inputmask.masksCache[c])}var f=void 0;if(a.isFunction(b.mask)&&(b.mask=b.mask.call(this,b)),a.isArray(b.mask))if(c)f=[],a.each(b.mask,function(b,c){f.push(void 0==c.mask||a.isFunction(c.mask)?e(c.toString(),c):e(c.mask.toString(),c))});else{b.keepStatic=void 0==b.keepStatic?!0:b.keepStatic;var g="(";a.each(b.mask,function(b,c){g.length>1&&(g+=")|("),g+=void 0==c.mask||a.isFunction(c.mask)?c.toString():c.mask.toString()}),g+=")",f=e(g,b.mask)}else b.mask&&(f=void 0==b.mask.mask||a.isFunction(b.mask.mask)?e(b.mask.toString(),b.mask):e(b.mask.mask.toString(),b.mask));return f}function f(d,e,f){function g(a,b,c){b=b||0;var d,e,f,g=[],h=0;do{if(a===!0&&n().validPositions[h]){var i=n().validPositions[h];e=i.match,d=i.locator.slice(),g.push(c===!0?i.input:H(h,e))}else{if(b>h){var j=v(h,d,h-1);f=j[0]}else f=s(h,d,h-1);e=f.match,d=f.locator.slice(),g.push(H(h,e))}h++}while((void 0==db||db>h-1)&&null!=e.fn||null==e.fn&&""!=e.def||b>=h);return g.pop(),g}function n(){return e}function o(a){var b=n();b.buffer=void 0,b.tests={},a!==!0&&(b._buffer=void 0,b.validPositions={},b.p=0)}function p(a){var b=n(),c=-1,d=b.validPositions;void 0==a&&(a=-1);var e=c,f=c;for(var g in d){var h=parseInt(g);(-1==a||null!=d[h].match.fn)&&(a>=h&&(e=h),h>=a&&(f=h))}return c=a-e>1||a>f?e:f}function q(b,c,d){if(f.insertMode&&void 0!=n().validPositions[b]&&void 0==d){var e,g=a.extend(!0,{},n().validPositions),h=p();for(e=b;h>=e;e++)delete n().validPositions[e];n().validPositions[b]=c;var i,j=!0;for(e=b;h>=e;e++){var k=g[e];if(void 0!=k){var l=n().validPositions;i=!f.keepStatic&&(void 0!=l[e+1]&&v(e+1,l[e].locator.slice(),e).length>1||l[e]&&void 0!=l[e].alternation)?e+1:D(e),j=u(i,k.match.def)?j&&A(i,k.input,!0,!0)!==!1:null==k.match.fn}if(!j)break}if(!j)return n().validPositions=a.extend(!0,{},g),!1}else n().validPositions[b]=c;return!0}function r(a,b){var c,d=a;for(void 0!=n().validPositions[a]&&n().validPositions[a].input==f.radixPoint&&(b++,d++),c=d;b>c;c++)void 0==n().validPositions[c]||n().validPositions[c].input==f.radixPoint&&c!=p()||delete n().validPositions[c];for(c=b;c<=p();){var e=n().validPositions[c],g=n().validPositions[d];void 0!=e&&void 0==g?(u(d,e.match.def)&&A(d,e.input,!0)!==!1&&(delete n().validPositions[c],c++),d++):c++}var h=p();h>=a&&void 0!=n().validPositions[h]&&n().validPositions[h].input==f.radixPoint&&delete n().validPositions[h],o(!0)}function s(b,c,d){function e(b,c,d){for(var e=f.greedy?d:d.slice(0,1),g=!1,h=b.locator[c].toString().split(","),i=0;i<h.length;i++)if(-1!=a.inArray(h[i],e)){g=!0;break}return g}for(var g,h=v(b,c,d),i=p(),j=n().validPositions[i]||v(0)[0],k=void 0!=j.alternation?j.locator[j.alternation].split(","):[],l=0;l<h.length&&(g=h[l],!(f.greedy||g.match&&(g.match.optionality===!1||g.match.newBlockMarker===!1)&&g.match.optionalQuantifier!==!0&&(void 0==j.alternation||void 0!=g.locator[j.alternation]&&e(g,j.alternation,k))));l++);return g}function t(a){return n().validPositions[a]?n().validPositions[a].match:v(a)[0].match}function u(a,b){for(var c=!1,d=v(a),e=0;e<d.length;e++)if(d[e].match&&d[e].match.def==b){c=!0;break}return c}function v(b,c,d){function e(c,d,g,i){function l(g,i,o){if(h>1e4)return alert("jquery.inputmask: There is probably an error in your mask definition or in the code. Create an issue on github with an example of the mask you are using. "+n().mask),!0;if(h==b&&void 0==g.matches)return j.push({match:g,locator:i.reverse()}),!0;if(void 0!=g.matches){if(g.isGroup&&o!==!0){if(g=l(c.matches[m+1],i))return!0}else if(g.isOptional){var p=g;if(g=e(g,d,i,o)){var q=j[j.length-1].match,r=0==a.inArray(q,p.matches);r&&(k=!0),h=b}}else if(g.isAlternator){var s,t=g,u=[],v=j.slice(),w=i.length,x=d.length>0?d.shift():-1;if(-1==x||"string"==typeof x){var y,z=h,A=d.slice();"string"==typeof x&&(y=x.split(","));for(var B=0;B<t.matches.length;B++){j=[],g=l(t.matches[B],[B].concat(i),o)||g,s=j.slice(),h=z,j=[];for(var C=0;C<A.length;C++)d[C]=A[C];for(var D=0;D<s.length;D++)for(var E=s[D],F=0;F<u.length;F++){var G=u[F];if(E.match.mask==G.match.mask&&("string"!=typeof x||-1!=a.inArray(E.locator[w].toString(),y))){s.splice(D,1),G.locator[w]=G.locator[w]+","+E.locator[w],G.alternation=w;break}}u=u.concat(s)}"string"==typeof x&&(u=a.map(u,function(b,c){if(isFinite(c)){var d,e=b.locator[w].toString().split(",");b.locator[w]=void 0,b.alternation=void 0;for(var f=0;f<e.length;f++)d=-1!=a.inArray(e[f],y),d&&(void 0!=b.locator[w]?(b.locator[w]+=",",b.alternation=w,b.locator[w]+=e[f]):b.locator[w]=parseInt(e[f]));if(void 0!=b.locator[w])return b}})),j=v.concat(u),k=!0}else g=l(t.matches[x],[x].concat(i),o);if(g)return!0}else if(g.isQuantifier&&o!==!0){var H=g;f.greedy=f.greedy&&isFinite(H.quantifier.max);for(var I=d.length>0&&o!==!0?d.shift():0;I<(isNaN(H.quantifier.max)?I+1:H.quantifier.max)&&b>=h;I++){var J=c.matches[a.inArray(H,c.matches)-1];if(g=l(J,[I].concat(i),!0)){var q=j[j.length-1].match;q.optionalQuantifier=I>H.quantifier.min-1;var r=0==a.inArray(q,J.matches);if(r){if(I>H.quantifier.min-1){k=!0,h=b;break}return!0}return!0}}}else if(g=e(g,d,i,o))return!0}else h++}for(var m=d.length>0?d.shift():0;m<c.matches.length;m++)if(c.matches[m].isQuantifier!==!0){var o=l(c.matches[m],[m].concat(g),i);if(o&&h==b)return o;if(h>b)break}}var g=n().maskToken,h=c?d:0,i=c||[0],j=[],k=!1;if(void 0==c){for(var l,m=b-1;void 0==(l=n().validPositions[m])&&m>-1;)m--;if(void 0!=l&&m>-1)h=m,i=l.locator.slice();else{for(m=b-1;void 0==(l=n().tests[m])&&m>-1;)m--;void 0!=l&&m>-1&&(h=m,i=l[0].locator.slice())}}for(var o=i.shift();o<g.length;o++){var p=e(g[o],i,[o]);if(p&&h==b||h>b)break}return(0==j.length||k)&&j.push({match:{fn:null,cardinality:0,optionality:!0,casing:null,def:""},locator:[]}),n().tests[b]=a.extend(!0,[],j),n().tests[b]}function w(){return void 0==n()._buffer&&(n()._buffer=g(!1,1)),n()._buffer}function x(){return void 0==n().buffer&&(n().buffer=g(!0,p(),!0)),n().buffer}function y(a,b){var c=x().slice();if(a===!0)o(),a=0,b=c.length;else for(var d=a;b>d;d++)delete n().validPositions[d],delete n().tests[d];for(var d=a;b>d;d++)c[d]!=f.skipOptionalPartCharacter&&A(d,c[d],!0,!0)}function z(a,b){switch(b.casing){case"upper":a=a.toUpperCase();break;case"lower":a=a.toLowerCase()}return a}function A(b,c,d,e){function g(b,c,d,e){var g=!1;return a.each(v(b),function(h,i){for(var j=i.match,k=c?1:0,l="",m=(x(),j.cardinality);m>k;m--)l+=F(b-(m-1));if(c&&(l+=c),g=null!=j.fn?j.fn.test(l,n(),b,d,f):c!=j.def&&c!=f.skipOptionalPartCharacter||""==j.def?!1:{c:j.def,pos:b},g!==!1){var s=void 0!=g.c?g.c:c;s=s==f.skipOptionalPartCharacter&&null===j.fn?j.def:s;var t=b;if(void 0!=g.remove&&r(g.remove,g.remove+1),g.refreshFromBuffer){var u=g.refreshFromBuffer;if(d=!0,y(u===!0?u:u.start,u.end),void 0==g.pos&&void 0==g.c)return g.pos=p(),!1;if(t=void 0!=g.pos?g.pos:b,t!=b)return g=a.extend(g,A(t,s,!0)),!1}else if(g!==!0&&void 0!=g.pos&&g.pos!=b&&(t=g.pos,y(b,t),t!=b))return g=a.extend(g,A(t,s,!0)),!1;return 1!=g&&void 0==g.pos&&void 0==g.c?!1:(h>0&&o(!0),q(t,a.extend({},i,{input:z(s,j)}),e)||(g=!1),!1)}}),g}function h(b,c,d,e){var g,h,i=a.extend(!0,{},n().validPositions);for(g=p();g>=0;g--)if(n().validPositions[g]&&void 0!=n().validPositions[g].alternation){h=n().validPositions[g].alternation;break}if(void 0!=h)for(var j in n().validPositions)if(parseInt(j)>parseInt(g)&&void 0===n().validPositions[j].alternation){for(var k=n().validPositions[j],l=k.locator[h],m=n().validPositions[g].locator[h].split(","),q=0;q<m.length;q++)if(l<m[q]){for(var r,s,t=j-1;t>=0;t--)if(r=n().validPositions[t],void 0!=r){s=r.locator[h],r.locator[h]=m[q];break}if(l!=r.locator[h]){for(var u=x().slice(),v=j;v<p()+1;v++)delete n().validPositions[v],delete n().tests[v];o(!0),f.keepStatic=!f.keepStatic;for(var v=j;v<u.length;v++)u[v]!=f.skipOptionalPartCharacter&&A(p()+1,u[v],!1,!0);r.locator[h]=s;var w=A(b,c,d,e);if(f.keepStatic=!f.keepStatic,w)return w;o(),n().validPositions=a.extend(!0,{},i)}}break}return!1}function i(b,c){for(var d=n().validPositions[c],e=d.locator,f=e.length,g=b;c>g;g++)if(!B(g)){var h,i=v(g),j=-1;for(var k in i)for(var l=i[k],m=0;f>m;m++)e[m]==l.locator[m]&&m>j&&(j=m,h=l);q(g,a.extend({},h,{input:h.match.def}),!0)}}d=d===!0;for(var j=x(),k=b-1;k>-1&&(!n().validPositions[k]||null!=n().validPositions[k].match.fn);k--)void 0==n().validPositions[k]&&(!B(k)||j[k]!=H(k))&&v(k).length>1&&g(k,j[k],!0);var l=b,m=!1;if(e&&l>=C()&&o(!0),l<C()&&(m=g(l,c,d,e),!d&&m===!1)){var s=n().validPositions[l];if(!s||null!=s.match.fn||s.match.def!=c&&c!=f.skipOptionalPartCharacter){if((f.insertMode||void 0==n().validPositions[D(l)])&&!B(l))for(var t=l+1,u=D(l);u>=t;t++)if(m=g(t,c,d,e),m!==!1){i(l,t),l=t;break}}else m={caret:D(l)}}return m===!1&&f.keepStatic&&P(j)&&(m=h(b,c,d,e)),m===!0&&(m={pos:l}),m}function B(a){var b=t(a);return null!=b.fn?b.fn:!1}function C(){var a;if(db=cb.prop("maxLength"),-1==db&&(db=void 0),0==f.greedy){var b,c=p(),d=n().validPositions[c],e=void 0!=d?d.locator.slice():void 0;for(b=c+1;void 0==d||null!=d.match.fn||null==d.match.fn&&""!=d.match.def;b++)d=s(b,e,b-1),e=d.locator.slice();a=b}else a=x().length;return void 0==db||db>a?a:db}function D(a){var b=C();if(a>=b)return b;for(var c=a;++c<b&&!B(c)&&(f.nojumps!==!0||f.nojumpsThreshold>c););return c}function E(a){var b=a;if(0>=b)return 0;for(;--b>0&&!B(b););return b}function F(a){return void 0==n().validPositions[a]?H(a):n().validPositions[a].input}function G(a,b,c){a._valueSet(b.join("")),void 0!=c&&M(a,c)}function H(b,c){c=c||t(b);var d=a.isFunction(c.placeholder)?c.placeholder.call(this,f):c.placeholder;return void 0!=d?d:null==c.fn?c.def:f.placeholder.charAt(b%f.placeholder.length)}function I(b,c,d,e){var g=void 0!=e?e.slice():b._valueGet().split("");o(),c&&b._valueSet("");var h=w().slice(0,D(-1)).join(""),i=g.join("").match(new RegExp(J(h),"g"));if(i&&i.length>1&&g.splice(0,h.length),a.each(g,function(c,e){var f=p();-1==a.inArray(e,w().slice(f+1,n().p))||d?(W.call(b,void 0,!0,e.charCodeAt(0),!1,d,d?c:n().p),d=d||c>0&&c>n().p):W.call(b,void 0,!0,e.charCodeAt(0),!1,!0,f+1)}),c){var j=f.onKeyPress.call(this,void 0,x(),0,f);U(b,j),G(b,x(),a(b).is(":focus")?D(p(0)):void 0)}}function J(b){return a.inputmask.escapeRegex.call(this,b)}function K(b){if(b.data("_inputmask")&&!b.hasClass("hasDatepicker")){var c=[],d=n().validPositions;for(var e in d)d[e].match&&null!=d[e].match.fn&&c.push(d[e].input);var g=(eb?c.reverse():c).join(""),h=(eb?x().slice().reverse():x()).join("");return a.isFunction(f.onUnMask)&&(g=f.onUnMask.call(b,h,g,f)||g),g}return b[0]._valueGet()}function L(a){if(eb&&"number"==typeof a&&(!f.greedy||""!=f.placeholder)){var b=x().length;a=b-a}return a}function M(b,c,d){var e,g=b.jquery&&b.length>0?b[0]:b;if("number"!=typeof c){var h=a(g).data("_inputmask");return!a(g).is(":visible")&&h&&void 0!=h.caret?(c=h.caret.begin,d=h.caret.end):g.setSelectionRange?(c=g.selectionStart,d=g.selectionEnd):document.selection&&document.selection.createRange&&(e=document.selection.createRange(),c=0-e.duplicate().moveStart("character",-1e5),d=c+e.text.length),c=L(c),d=L(d),{begin:c,end:d}}c=L(c),d=L(d),d="number"==typeof d?d:c;var h=a(g).data("_inputmask")||{};h.caret={begin:c,end:d},a(g).data("_inputmask",h),a(g).is(":visible")&&(g.scrollLeft=g.scrollWidth,0==f.insertMode&&c==d&&d++,g.setSelectionRange?(g.selectionStart=c,g.selectionEnd=d):g.createTextRange&&(e=g.createTextRange(),e.collapse(!0),e.moveEnd("character",d),e.moveStart("character",c),e.select()))}function N(b){var c,d,e=x(),f=e.length,g=p(),h={},i=n().validPositions[g],j=void 0!=i?i.locator.slice():void 0;for(c=g+1;c<e.length;c++)d=s(c,j,c-1),j=d.locator.slice(),h[c]=a.extend(!0,{},d);var k=i&&void 0!=i.alternation?i.locator[i.alternation].split(","):[];for(c=f-1;c>g&&(d=h[c].match,(d.optionality||d.optionalQuantifier||i&&void 0!=i.alternation&&void 0!=h[c].locator[i.alternation]&&-1!=a.inArray(h[c].locator[i.alternation].toString(),k))&&e[c]==H(c,d));c--)f--;return b?{l:f,def:h[f]?h[f].match:void 0}:f}function O(b){var c=x(),d=c.slice();if(a.isFunction(f.postProcessOnBlur))f.postProcessOnBlur.call(b,d,f);else{for(var e=N(),g=d.length-1;g>e&&!B(g);g--);d.splice(e,g+1-e)}G(b,d)}function P(b){if(a.isFunction(f.isComplete))return f.isComplete.call(cb,b,f);if("*"==f.repeat)return void 0;var c=!1,d=N(!0),e=E(d.l),g=p();if(g==e&&(void 0==d.def||d.def.newBlockMarker||d.def.optionalQuantifier)){c=!0;for(var h=0;e>=h;h++){var i=B(h);if(i&&(void 0==b[h]||b[h]==H(h))||!i&&b[h]!=H(h)){c=!1;break}}}return c}function Q(a,b){return eb?a-b>1||a-b==1&&f.insertMode:b-a>1||b-a==1&&f.insertMode}function R(b){var c=a._data(b).events;a.each(c,function(b,c){a.each(c,function(a,b){if("inputmask"==b.namespace&&"setvalue"!=b.type){var c=b.handler;b.handler=function(a){return this.readOnly||this.disabled?void a.preventDefault:c.apply(this,arguments)}}})})}function S(b){function c(b){if(void 0==a.valHooks[b]||1!=a.valHooks[b].inputmaskpatch){var c=a.valHooks[b]&&a.valHooks[b].get?a.valHooks[b].get:function(a){return a.value},d=a.valHooks[b]&&a.valHooks[b].set?a.valHooks[b].set:function(a,b){return a.value=b,a};a.valHooks[b]={get:function(b){var d=a(b);if(d.data("_inputmask")){if(d.data("_inputmask").opts.autoUnmask)return d.inputmask("unmaskedvalue");var e=c(b),f=d.data("_inputmask"),g=f.maskset,h=g._buffer;return h=h?h.join(""):"",e!=h?e:""}return c(b)},set:function(b,c){var e,f=a(b),g=f.data("_inputmask");return g?(e=d(b,a.isFunction(g.opts.onBeforeMask)?g.opts.onBeforeMask.call(nb,c,g.opts)||c:c),f.triggerHandler("setvalue.inputmask")):e=d(b,c),e},inputmaskpatch:!0}}}function d(){var b=a(this),c=a(this).data("_inputmask");return c?c.opts.autoUnmask?b.inputmask("unmaskedvalue"):h.call(this)!=w().join("")?h.call(this):"":h.call(this)}function e(b){var c=a(this).data("_inputmask");c?(i.call(this,a.isFunction(c.opts.onBeforeMask)?c.opts.onBeforeMask.call(nb,b,c.opts)||b:b),a(this).triggerHandler("setvalue.inputmask")):i.call(this,b)}function g(b){a(b).bind("mouseenter.inputmask",function(){var b=a(this),c=this,d=c._valueGet();""!=d&&d!=x().join("")&&(i.call(this,a.isFunction(f.onBeforeMask)?f.onBeforeMask.call(nb,d,f)||d:d),b.trigger("setvalue"))});//!! the bound handlers are executed in the order they where bound
    var c=a._data(b).events,d=c.mouseover;if(d){for(var e=d[d.length-1],g=d.length-1;g>0;g--)d[g]=d[g-1];d[0]=e}}var h,i;if(!b._valueGet){if(Object.getOwnPropertyDescriptor){Object.getOwnPropertyDescriptor(b,"value")}document.__lookupGetter__&&b.__lookupGetter__("value")?(h=b.__lookupGetter__("value"),i=b.__lookupSetter__("value"),b.__defineGetter__("value",d),b.__defineSetter__("value",e)):(h=function(){return b.value},i=function(a){b.value=a},c(b.type),g(b)),b._valueGet=function(){return eb?h.call(this).split("").reverse().join(""):h.call(this)},b._valueSet=function(a){i.call(this,eb?a.split("").reverse().join(""):a)}}}function T(b,c,d){function e(){if(f.keepStatic){o(!0);var a,c=[];for(a=p();a>=0;a--)if(n().validPositions[a]){if(void 0!=n().validPositions[a].alternation)break;c.push(n().validPositions[a].input),delete n().validPositions[a]}if(a>0)for(;c.length>0;)n().p=D(p()),W.call(b,void 0,!0,c.pop().charCodeAt(0),!1,!1,n().p)}}if((f.numericInput||eb)&&(c==a.inputmask.keyCode.BACKSPACE?c=a.inputmask.keyCode.DELETE:c==a.inputmask.keyCode.DELETE&&(c=a.inputmask.keyCode.BACKSPACE),eb)){var g=d.end;d.end=d.begin,d.begin=g}c==a.inputmask.keyCode.BACKSPACE&&d.end-d.begin<=1?d.begin=E(d.begin):c==a.inputmask.keyCode.DELETE&&d.begin==d.end&&d.end++,r(d.begin,d.end),e();var h=p(d.begin);h<d.begin?(-1==h&&o(),n().p=D(h)):n().p=d.begin}function U(a,b,c){if(b&&b.refreshFromBuffer){var d=b.refreshFromBuffer;y(d===!0?d:d.start,d.end),o(!0),void 0!=c&&(G(a,x()),M(a,b.caret||c.begin,b.caret||c.end))}}function V(c){fb=!1;var d=this,e=a(d),g=c.keyCode,i=M(d);g==a.inputmask.keyCode.BACKSPACE||g==a.inputmask.keyCode.DELETE||h&&127==g||c.ctrlKey&&88==g&&!b("cut")?(c.preventDefault(),88==g&&(bb=x().join("")),T(d,g,i),G(d,x(),n().p),d._valueGet()==w().join("")&&e.trigger("cleared"),f.showTooltip&&e.prop("title",n().mask)):g==a.inputmask.keyCode.END||g==a.inputmask.keyCode.PAGE_DOWN?setTimeout(function(){var a=D(p());f.insertMode||a!=C()||c.shiftKey||a--,M(d,c.shiftKey?i.begin:a,a)},0):g==a.inputmask.keyCode.HOME&&!c.shiftKey||g==a.inputmask.keyCode.PAGE_UP?M(d,0,c.shiftKey?i.begin:0):g==a.inputmask.keyCode.ESCAPE||90==g&&c.ctrlKey?(I(d,!0,!1,bb.split("")),e.click()):g!=a.inputmask.keyCode.INSERT||c.shiftKey||c.ctrlKey?0!=f.insertMode||c.shiftKey||(g==a.inputmask.keyCode.RIGHT?setTimeout(function(){var a=M(d);M(d,a.begin)},0):g==a.inputmask.keyCode.LEFT&&setTimeout(function(){var a=M(d);M(d,eb?a.begin+1:a.begin-1)},0)):(f.insertMode=!f.insertMode,M(d,f.insertMode||i.begin!=C()?i.begin:i.begin-1));var j=M(d),k=f.onKeyDown.call(this,c,x(),j.begin,f);U(d,k,j),hb=-1!=a.inArray(g,f.ignorables)}function W(b,c,d,e,g,h){if(void 0==d&&fb)return!1;fb=!0;var i=this,j=a(i);b=b||window.event;var d=c?d:b.which||b.charCode||b.keyCode;if(!(c===!0||b.ctrlKey&&b.altKey)&&(b.ctrlKey||b.metaKey||hb))return!0;if(d){c!==!0&&46==d&&0==b.shiftKey&&","==f.radixPoint&&(d=44);var k,l=c?{begin:h,end:h}:M(i),m=String.fromCharCode(d),p=Q(l.begin,l.end);p&&(n().undoPositions=a.extend(!0,{},n().validPositions),T(i,a.inputmask.keyCode.DELETE,l),f.insertMode||(f.insertMode=!f.insertMode,q(l.begin,g),f.insertMode=!f.insertMode),p=!f.multi),n().writeOutBuffer=!0;var r=eb&&!p?l.end:l.begin,s=A(r,m,g);if(s!==!1){if(s!==!0&&(r=void 0!=s.pos?s.pos:r,m=void 0!=s.c?s.c:m),o(!0),void 0!=s.caret)k=s.caret;else{var t=n().validPositions;k=!f.keepStatic&&(void 0!=t[r+1]&&v(r+1,t[r].locator.slice(),r).length>1||void 0!=t[r].alternation)?r+1:D(r)}n().p=k}if(e!==!1){var u=this;if(setTimeout(function(){f.onKeyValidation.call(u,s,f)},0),n().writeOutBuffer&&s!==!1){var w=x();G(i,w,c?void 0:f.numericInput?E(k):k),c!==!0&&setTimeout(function(){P(w)===!0&&j.trigger("complete"),gb=!0,j.trigger("input")},0)}else p&&(n().buffer=void 0,n().validPositions=n().undoPositions)}else p&&(n().buffer=void 0,n().validPositions=n().undoPositions);if(f.showTooltip&&j.prop("title",n().mask),b&&1!=c){b.preventDefault();var y=M(i),z=f.onKeyPress.call(this,b,x(),y.begin,f);U(i,z,y)}}}function X(b){var c=a(this),d=this,e=b.keyCode,g=x(),h=M(d),i=f.onKeyUp.call(this,b,g,h.begin,f);U(d,i,h),e==a.inputmask.keyCode.TAB&&f.showMaskOnFocus&&(c.is(":focus")&&0==d._valueGet().length?(o(),g=x(),G(d,g),M(d,0),bb=x().join("")):(G(d,g),M(d,L(0),L(C()))))}function Y(b){if(gb===!0&&"input"==b.type)return gb=!1,!0;var c=this,d=a(c),e=c._valueGet(),g=M(c);if("propertychange"==b.type&&c._valueGet().length<=C())return!0;"paste"==b.type&&(window.clipboardData&&window.clipboardData.getData?e=e.substr(0,g.begin)+window.clipboardData.getData("Text")+e.substr(g.end,e.length):b.originalEvent&&b.originalEvent.clipboardData&&b.originalEvent.clipboardData.getData&&(e=e.substr(0,g.begin)+b.originalEvent.clipboardData.getData("text/plain")+e.substr(g.end,e.length)));var h=a.isFunction(f.onBeforePaste)?f.onBeforePaste.call(c,e,f)||e:e;return I(c,!0,!1,eb?h.split("").reverse():h.split("")),d.click(),P(x())===!0&&d.trigger("complete"),!1}function Z(b){if(gb===!0&&"input"==b.type)return gb=!1,!0;var c=this,d=M(c),e=c._valueGet();e=e.replace(new RegExp("("+J(w().join(""))+")*"),""),d.begin>e.length&&(M(c,e.length),d=M(c)),x().length-e.length!=1||e.charAt(d.begin)==x()[d.begin]||e.charAt(d.begin+1)==x()[d.begin]||B(d.begin)||(b.keyCode=a.inputmask.keyCode.BACKSPACE,V.call(c,b)),b.preventDefault()}function $(b){if(gb===!0&&"input"==b.type)return gb=!1,!0;var c=this;I(c,!1,!1);var d=n().p;G(c,x(),f.numericInput?E(d):d),P(x())===!0&&a(c).trigger("complete"),b.preventDefault()}function _(b){gb=!0;var c=this;return setTimeout(function(){M(c,M(c).begin-1);var d=a.Event("keypress");d.which=b.originalEvent.data.charCodeAt(0),fb=!1,hb=!1,W.call(c,d,void 0,void 0,!1);var e=n().p;G(c,x(),f.numericInput?E(e):e)},0),!1}function ab(b){if(cb=a(b),cb.is(":input")&&c(cb.attr("type"))){if(cb.data("_inputmask",{maskset:e,opts:f,isRTL:!1}),f.showTooltip&&cb.prop("title",n().mask),("rtl"==b.dir||f.rightAlign)&&cb.css("text-align","right"),"rtl"==b.dir||f.numericInput){b.dir="ltr",cb.removeAttr("dir");var d=cb.data("_inputmask");d.isRTL=!0,cb.data("_inputmask",d),eb=!0}cb.unbind(".inputmask"),cb.closest("form").bind("submit",function(){bb!=x().join("")&&cb.change(),cb[0]._valueGet&&cb[0]._valueGet()==w().join("")&&cb[0]._valueSet(""),f.removeMaskOnSubmit&&cb.inputmask("remove")}).bind("reset",function(){setTimeout(function(){cb.trigger("setvalue")},0)}),cb.bind("mouseenter.inputmask",function(){var b=a(this),c=this;!b.is(":focus")&&f.showMaskOnHover&&c._valueGet()!=x().join("")&&G(c,x())}).bind("blur.inputmask",function(){var b=a(this),c=this;if(b.data("_inputmask")){var d=c._valueGet(),e=x();ib=!0,bb!=x().join("")&&(b.change(),bb=x().join("")),f.clearMaskOnLostFocus&&""!=d&&(d==w().join("")?c._valueSet(""):O(c)),P(e)===!1&&(b.trigger("incomplete"),f.clearIncomplete&&(o(),f.clearMaskOnLostFocus?c._valueSet(""):(e=w().slice(),G(c,e))))}}).bind("focus.inputmask",function(){var b=a(this),c=this,d=c._valueGet();f.showMaskOnFocus&&!b.is(":focus")&&(!f.showMaskOnHover||f.showMaskOnHover&&""==d)&&c._valueGet()!=x().join("")&&G(c,x(),D(p())),bb=x().join("")}).bind("mouseleave.inputmask",function(){var b=a(this),c=this;f.clearMaskOnLostFocus&&(b.is(":focus")||c._valueGet()==b.attr("placeholder")||(c._valueGet()==w().join("")||""==c._valueGet()?c._valueSet(""):O(c)))}).bind("click.inputmask",function(){var b=a(this),c=this;if(b.is(":focus")){var d=M(c);if(d.begin==d.end)if(f.radixFocus&&""!=f.radixPoint&&-1!=a.inArray(f.radixPoint,x())&&(ib||x().join("")==w().join("")))M(c,a.inArray(f.radixPoint,x())),ib=!1;else{var e=eb?L(d.begin):d.begin,g=D(p(e));g>e?M(c,B(e)?e:D(e)):M(c,g)}}}).bind("dblclick.inputmask",function(){var a=this;setTimeout(function(){M(a,0,D(p()))},0)}).bind(m+".inputmask dragdrop.inputmask drop.inputmask",Y).bind("setvalue.inputmask",function(){var a=this;I(a,!0,!1),bb=x().join(""),(f.clearMaskOnLostFocus||f.clearIncomplete)&&a._valueGet()==w().join("")&&a._valueSet("")}).bind("cut.inputmask",function(b){gb=!0;var c=this,d=a(c),e=M(c);T(c,a.inputmask.keyCode.DELETE,e);var g=f.onKeyPress.call(this,b,x(),n().p,f);U(c,g,{begin:n().p,end:n().p}),c._valueGet()==w().join("")&&d.trigger("cleared"),f.showTooltip&&d.prop("title",n().mask)}).bind("complete.inputmask",f.oncomplete).bind("incomplete.inputmask",f.onincomplete).bind("cleared.inputmask",f.oncleared),cb.bind("keydown.inputmask",V).bind("keypress.inputmask",W).bind("keyup.inputmask",X).bind("compositionupdate.inputmask",_),"paste"===m&&cb.bind("input.inputmask",$),(i||k||j||l)&&(cb.unbind("input.inputmask"),cb.bind("input.inputmask",Z)),S(b);var g=a.isFunction(f.onBeforeMask)?f.onBeforeMask.call(b,b._valueGet(),f)||b._valueGet():b._valueGet();I(b,!0,!1,g.split("")),bb=x().join("");var h;try{h=document.activeElement}catch(q){}P(x())===!1&&f.clearIncomplete&&o(),f.clearMaskOnLostFocus?x().join("")==w().join("")?b._valueSet(""):O(b):G(b,x()),h===b&&M(b,D(p())),R(b)}}var bb,cb,db,eb=!1,fb=!1,gb=!1,hb=!1,ib=!0;if(void 0!=d)switch(d.action){case"isComplete":return cb=a(d.el),e=cb.data("_inputmask").maskset,f=cb.data("_inputmask").opts,P(d.buffer);case"unmaskedvalue":return cb=d.$input,e=cb.data("_inputmask").maskset,f=cb.data("_inputmask").opts,eb=d.$input.data("_inputmask").isRTL,K(d.$input);case"mask":bb=x().join(""),ab(d.el);break;case"format":cb=a({}),cb.data("_inputmask",{maskset:e,opts:f,isRTL:f.numericInput}),f.numericInput&&(eb=!0);var jb=(a.isFunction(f.onBeforeMask)?f.onBeforeMask.call(cb,d.value,f)||d.value:d.value).split("");return I(cb,!1,!1,eb?jb.reverse():jb),f.onKeyPress.call(this,void 0,x(),0,f),d.metadata?{value:eb?x().slice().reverse().join(""):x().join(""),metadata:cb.inputmask("getmetadata")}:eb?x().slice().reverse().join(""):x().join("");case"isValid":cb=a({}),cb.data("_inputmask",{maskset:e,opts:f,isRTL:f.numericInput}),f.numericInput&&(eb=!0);var jb=d.value.split("");I(cb,!1,!0,eb?jb.reverse():jb);for(var kb=x(),lb=N(),mb=kb.length-1;mb>lb&&!B(mb);mb--);return kb.splice(lb,mb+1-lb),P(kb)&&d.value==kb.join("");case"getemptymask":return cb=a(d.el),e=cb.data("_inputmask").maskset,f=cb.data("_inputmask").opts,w();case"remove":var nb=d.el;cb=a(nb),e=cb.data("_inputmask").maskset,f=cb.data("_inputmask").opts,nb._valueSet(K(cb)),cb.unbind(".inputmask"),cb.removeData("_inputmask");var ob;Object.getOwnPropertyDescriptor&&(ob=Object.getOwnPropertyDescriptor(nb,"value")),ob&&ob.get?nb._valueGet&&Object.defineProperty(nb,"value",{get:nb._valueGet,set:nb._valueSet}):document.__lookupGetter__&&nb.__lookupGetter__("value")&&nb._valueGet&&(nb.__defineGetter__("value",nb._valueGet),nb.__defineSetter__("value",nb._valueSet));try{delete nb._valueGet,delete nb._valueSet}catch(pb){nb._valueGet=void 0,nb._valueSet=void 0}break;case"getmetadata":if(cb=a(d.el),e=cb.data("_inputmask").maskset,f=cb.data("_inputmask").opts,a.isArray(e.metadata)){for(var qb,rb=p(),sb=rb;sb>=0;sb--)if(n().validPositions[sb]&&void 0!=n().validPositions[sb].alternation){qb=n().validPositions[sb].alternation;break}return void 0!=qb?e.metadata[n().validPositions[rb].locator[qb]]:e.metadata[0]}return e.metadata}}if(void 0===a.fn.inputmask){var g=("function"==typeof ScriptEngineMajorVersion?ScriptEngineMajorVersion()>=10:/*@cc_on (@_jscript_version >= 10) ||@*/!1,navigator.userAgent),h=null!==g.match(new RegExp("iphone","i")),i=null!==g.match(new RegExp("android.*safari.*","i")),j=null!==g.match(new RegExp("android.*chrome.*","i")),k=null!==g.match(new RegExp("android.*firefox.*","i")),l=/Kindle/i.test(g)||/Silk/i.test(g)||/KFTT/i.test(g)||/KFOT/i.test(g)||/KFJWA/i.test(g)||/KFJWI/i.test(g)||/KFSOWI/i.test(g)||/KFTHWA/i.test(g)||/KFTHWI/i.test(g)||/KFAPWA/i.test(g)||/KFAPWI/i.test(g),m=b("paste")?"paste":b("input")?"input":"propertychange";a.inputmask={defaults:{placeholder:"_",optionalmarker:{start:"[",end:"]"},quantifiermarker:{start:"{",end:"}"},groupmarker:{start:"(",end:")"},alternatormarker:"|",escapeChar:"\\",mask:null,oncomplete:a.noop,onincomplete:a.noop,oncleared:a.noop,repeat:0,greedy:!0,autoUnmask:!1,removeMaskOnSubmit:!0,clearMaskOnLostFocus:!0,insertMode:!0,clearIncomplete:!1,aliases:{},alias:null,onKeyUp:a.noop,onKeyPress:a.noop,onKeyDown:a.noop,onBeforeMask:void 0,onBeforePaste:void 0,onUnMask:void 0,showMaskOnFocus:!0,showMaskOnHover:!0,onKeyValidation:a.noop,skipOptionalPartCharacter:" ",showTooltip:!1,numericInput:!1,rightAlign:!1,radixPoint:"",radixFocus:!1,nojumps:!1,nojumpsThreshold:0,keepStatic:void 0,definitions:{9:{validator:"[0-9]",cardinality:1,definitionSymbol:"*"},a:{validator:"[A-Za-zА-яЁёÀ-ÿµ]",cardinality:1,definitionSymbol:"*"},"*":{validator:"[0-9A-Za-zА-яЁёÀ-ÿµ]",cardinality:1}},ignorables:[8,9,13,19,27,33,34,35,36,37,38,39,40,45,46,93,112,113,114,115,116,117,118,119,120,121,122,123],isComplete:void 0,postProcessOnBlur:void 0},keyCode:{ALT:18,BACKSPACE:8,CAPS_LOCK:20,COMMA:188,COMMAND:91,COMMAND_LEFT:91,COMMAND_RIGHT:93,CONTROL:17,DELETE:46,DOWN:40,END:35,ENTER:13,ESCAPE:27,HOME:36,INSERT:45,LEFT:37,MENU:93,NUMPAD_ADD:107,NUMPAD_DECIMAL:110,NUMPAD_DIVIDE:111,NUMPAD_ENTER:108,NUMPAD_MULTIPLY:106,NUMPAD_SUBTRACT:109,PAGE_DOWN:34,PAGE_UP:33,PERIOD:190,RIGHT:39,SHIFT:16,SPACE:32,TAB:9,UP:38,WINDOWS:91},masksCache:{},escapeRegex:function(a){var b=["/",".","*","+","?","|","(",")","[","]","{","}","\\"];return a.replace(new RegExp("(\\"+b.join("|\\")+")","gim"),"\\$1")},format:function(b,c,g){var h=a.extend(!0,{},a.inputmask.defaults,c);return d(h.alias,c,h),f({action:"format",value:b,metadata:g},e(h),h)},isValid:function(b,c){var g=a.extend(!0,{},a.inputmask.defaults,c);return d(g.alias,c,g),f({action:"isValid",value:b},e(g),g)}},a.fn.inputmask=function(b,c,g,h,i){function j(b,c,e){var f=a(b);f.data("inputmask-alias")&&d(f.data("inputmask-alias"),{},c);for(var g in c){var h=f.data("inputmask-"+g.toLowerCase());void 0!=h&&("mask"==g&&0==h.indexOf("[")?(c[g]=h.replace(/[\s[\]]/g,"").split("','"),c[g][0]=c[g][0].replace("'",""),c[g][c[g].length-1]=c[g][c[g].length-1].replace("'","")):c[g]="boolean"==typeof h?h:h.toString(),e&&(e[g]=c[g]))}return c}g=g||f,h=h||"_inputmask";var k,l=a.extend(!0,{},a.inputmask.defaults,c);if("string"==typeof b)switch(b){case"mask":return d(l.alias,c,l),k=e(l,g!==f),void 0==k?this:this.each(function(){g({action:"mask",el:this},a.extend(!0,{},k),j(this,l))});case"unmaskedvalue":var m=a(this);return m.data(h)?g({action:"unmaskedvalue",$input:m}):m.val();case"remove":return this.each(function(){var b=a(this);b.data(h)&&g({action:"remove",el:this})});case"getemptymask":return this.data(h)?g({action:"getemptymask",el:this}):"";case"hasMaskedValue":return this.data(h)?!this.data(h).opts.autoUnmask:!1;case"isComplete":return this.data(h)?g({action:"isComplete",buffer:this[0]._valueGet().split(""),el:this}):!0;case"getmetadata":return this.data(h)?g({action:"getmetadata",el:this}):void 0;case"_detectScope":return d(l.alias,c,l),void 0==i||d(i,c,l)||-1!=a.inArray(i,["mask","unmaskedvalue","remove","getemptymask","hasMaskedValue","isComplete","getmetadata","_detectScope"])||(l.mask=i),a.isFunction(l.mask)&&(l.mask=l.mask.call(this,l)),a.isArray(l.mask);default:return d(l.alias,c,l),d(b,c,l)||(l.mask=b),k=e(l,g!==f),void 0==k?this:this.each(function(){g({action:"mask",el:this},a.extend(!0,{},k),j(this,l))})}else{if("object"==typeof b)return l=a.extend(!0,{},a.inputmask.defaults,b),d(l.alias,b,l),k=e(l,g!==f),void 0==k?this:this.each(function(){g({action:"mask",el:this},a.extend(!0,{},k),j(this,l))});if(void 0==b)return this.each(function(){var b=a(this).attr("data-inputmask");if(b&&""!=b)try{b=b.replace(new RegExp("'","g"),'"');var e=a.parseJSON("{"+b+"}");a.extend(!0,e,c),l=a.extend(!0,{},a.inputmask.defaults,e),l=j(this,l),d(l.alias,e,l),l.alias=void 0,a(this).inputmask("mask",l,g)}catch(f){}if(a(this).attr("data-inputmask-mask")||a(this).attr("data-inputmask-alias")){l=a.extend(!0,{},a.inputmask.defaults,{});var h={};l=j(this,l,h),d(l.alias,h,l),l.alias=void 0,a(this).inputmask("mask",l,g)}})}}}return a.fn.inputmask}(jQuery),function(a){return a.extend(a.inputmask.defaults.definitions,{h:{validator:"[01][0-9]|2[0-3]",cardinality:2,prevalidator:[{validator:"[0-2]",cardinality:1}]},s:{validator:"[0-5][0-9]",cardinality:2,prevalidator:[{validator:"[0-5]",cardinality:1}]},d:{validator:"0[1-9]|[12][0-9]|3[01]",cardinality:2,prevalidator:[{validator:"[0-3]",cardinality:1}]},m:{validator:"0[1-9]|1[012]",cardinality:2,prevalidator:[{validator:"[01]",cardinality:1}]},y:{validator:"(19|20)\\d{2}",cardinality:4,prevalidator:[{validator:"[12]",cardinality:1},{validator:"(19|20)",cardinality:2},{validator:"(19|20)\\d",cardinality:3}]}}),a.extend(a.inputmask.defaults.aliases,{"dd/mm/yyyy":{mask:"1/2/y",placeholder:"dd/mm/yyyy",regex:{val1pre:new RegExp("[0-3]"),val1:new RegExp("0[1-9]|[12][0-9]|3[01]"),val2pre:function(b){var c=a.inputmask.escapeRegex.call(this,b);return new RegExp("((0[1-9]|[12][0-9]|3[01])"+c+"[01])")},val2:function(b){var c=a.inputmask.escapeRegex.call(this,b);return new RegExp("((0[1-9]|[12][0-9])"+c+"(0[1-9]|1[012]))|(30"+c+"(0[13-9]|1[012]))|(31"+c+"(0[13578]|1[02]))")}},leapday:"29/02/",separator:"/",yearrange:{minyear:1900,maxyear:2099},isInYearRange:function(a,b,c){if(isNaN(a))return!1;var d=parseInt(a.concat(b.toString().slice(a.length))),e=parseInt(a.concat(c.toString().slice(a.length)));return(isNaN(d)?!1:d>=b&&c>=d)||(isNaN(e)?!1:e>=b&&c>=e)},determinebaseyear:function(a,b,c){var d=(new Date).getFullYear();if(a>d)return a;if(d>b){for(var e=b.toString().slice(0,2),f=b.toString().slice(2,4);e+c>b;)e--;var g=e+f;return a>g?a:g}return d},onKeyUp:function(b){var c=a(this);if(b.ctrlKey&&b.keyCode==a.inputmask.keyCode.RIGHT){var d=new Date;c.val(d.getDate().toString()+(d.getMonth()+1).toString()+d.getFullYear().toString())}},definitions:{1:{validator:function(a,b,c,d,e){var f=e.regex.val1.test(a);return d||f||a.charAt(1)!=e.separator&&-1=="-./".indexOf(a.charAt(1))||!(f=e.regex.val1.test("0"+a.charAt(0)))?f:(b.buffer[c-1]="0",{refreshFromBuffer:{start:c-1,end:c},pos:c,c:a.charAt(0)})},cardinality:2,prevalidator:[{validator:function(a,b,c,d,e){isNaN(b.buffer[c+1])||(a+=b.buffer[c+1]);var f=1==a.length?e.regex.val1pre.test(a):e.regex.val1.test(a);return d||f||!(f=e.regex.val1.test("0"+a))?f:(b.buffer[c]="0",c++,{pos:c})},cardinality:1}]},2:{validator:function(a,b,c,d,e){var f=e.mask.indexOf("2")==e.mask.length-1?b.buffer.join("").substr(5,3):b.buffer.join("").substr(0,3);-1!=f.indexOf(e.placeholder[0])&&(f="01"+e.separator);var g=e.regex.val2(e.separator).test(f+a);if(!d&&!g&&(a.charAt(1)==e.separator||-1!="-./".indexOf(a.charAt(1)))&&(g=e.regex.val2(e.separator).test(f+"0"+a.charAt(0))))return b.buffer[c-1]="0",{refreshFromBuffer:{start:c-1,end:c},pos:c,c:a.charAt(0)};if(e.mask.indexOf("2")==e.mask.length-1&&g){var h=b.buffer.join("").substr(4,4)+a;if(h!=e.leapday)return!0;var i=parseInt(b.buffer.join("").substr(0,4),10);return i%4===0?i%100===0?i%400===0?!0:!1:!0:!1}return g},cardinality:2,prevalidator:[{validator:function(a,b,c,d,e){isNaN(b.buffer[c+1])||(a+=b.buffer[c+1]);var f=e.mask.indexOf("2")==e.mask.length-1?b.buffer.join("").substr(5,3):b.buffer.join("").substr(0,3);-1!=f.indexOf(e.placeholder[0])&&(f="01"+e.separator);var g=1==a.length?e.regex.val2pre(e.separator).test(f+a):e.regex.val2(e.separator).test(f+a);return d||g||!(g=e.regex.val2(e.separator).test(f+"0"+a))?g:(b.buffer[c]="0",c++,{pos:c})},cardinality:1}]},y:{validator:function(a,b,c,d,e){if(e.isInYearRange(a,e.yearrange.minyear,e.yearrange.maxyear)){var f=b.buffer.join("").substr(0,6);if(f!=e.leapday)return!0;var g=parseInt(a,10);return g%4===0?g%100===0?g%400===0?!0:!1:!0:!1}return!1},cardinality:4,prevalidator:[{validator:function(a,b,c,d,e){var f=e.isInYearRange(a,e.yearrange.minyear,e.yearrange.maxyear);if(!d&&!f){var g=e.determinebaseyear(e.yearrange.minyear,e.yearrange.maxyear,a+"0").toString().slice(0,1);if(f=e.isInYearRange(g+a,e.yearrange.minyear,e.yearrange.maxyear))return b.buffer[c++]=g.charAt(0),{pos:c};if(g=e.determinebaseyear(e.yearrange.minyear,e.yearrange.maxyear,a+"0").toString().slice(0,2),f=e.isInYearRange(g+a,e.yearrange.minyear,e.yearrange.maxyear))return b.buffer[c++]=g.charAt(0),b.buffer[c++]=g.charAt(1),{pos:c}}return f},cardinality:1},{validator:function(a,b,c,d,e){var f=e.isInYearRange(a,e.yearrange.minyear,e.yearrange.maxyear);if(!d&&!f){var g=e.determinebaseyear(e.yearrange.minyear,e.yearrange.maxyear,a).toString().slice(0,2);if(f=e.isInYearRange(a[0]+g[1]+a[1],e.yearrange.minyear,e.yearrange.maxyear))return b.buffer[c++]=g.charAt(1),{pos:c};if(g=e.determinebaseyear(e.yearrange.minyear,e.yearrange.maxyear,a).toString().slice(0,2),e.isInYearRange(g+a,e.yearrange.minyear,e.yearrange.maxyear)){var h=b.buffer.join("").substr(0,6);if(h!=e.leapday)f=!0;else{var i=parseInt(a,10);f=i%4===0?i%100===0?i%400===0?!0:!1:!0:!1}}else f=!1;if(f)return b.buffer[c-1]=g.charAt(0),b.buffer[c++]=g.charAt(1),b.buffer[c++]=a.charAt(0),{refreshFromBuffer:{start:c-3,end:c},pos:c}}return f},cardinality:2},{validator:function(a,b,c,d,e){return e.isInYearRange(a,e.yearrange.minyear,e.yearrange.maxyear)},cardinality:3}]}},insertMode:!1,autoUnmask:!1},"mm/dd/yyyy":{placeholder:"mm/dd/yyyy",alias:"dd/mm/yyyy",regex:{val2pre:function(b){var c=a.inputmask.escapeRegex.call(this,b);return new RegExp("((0[13-9]|1[012])"+c+"[0-3])|(02"+c+"[0-2])")},val2:function(b){var c=a.inputmask.escapeRegex.call(this,b);return new RegExp("((0[1-9]|1[012])"+c+"(0[1-9]|[12][0-9]))|((0[13-9]|1[012])"+c+"30)|((0[13578]|1[02])"+c+"31)")},val1pre:new RegExp("[01]"),val1:new RegExp("0[1-9]|1[012]")},leapday:"02/29/",onKeyUp:function(b){var c=a(this);if(b.ctrlKey&&b.keyCode==a.inputmask.keyCode.RIGHT){var d=new Date;c.val((d.getMonth()+1).toString()+d.getDate().toString()+d.getFullYear().toString())}}},"yyyy/mm/dd":{mask:"y/1/2",placeholder:"yyyy/mm/dd",alias:"mm/dd/yyyy",leapday:"/02/29",onKeyUp:function(b){var c=a(this);if(b.ctrlKey&&b.keyCode==a.inputmask.keyCode.RIGHT){var d=new Date;c.val(d.getFullYear().toString()+(d.getMonth()+1).toString()+d.getDate().toString())}}},"dd.mm.yyyy":{mask:"1.2.y",placeholder:"dd.mm.yyyy",leapday:"29.02.",separator:".",alias:"dd/mm/yyyy"},"dd-mm-yyyy":{mask:"1-2-y",placeholder:"dd-mm-yyyy",leapday:"29-02-",separator:"-",alias:"dd/mm/yyyy"},"mm.dd.yyyy":{mask:"1.2.y",placeholder:"mm.dd.yyyy",leapday:"02.29.",separator:".",alias:"mm/dd/yyyy"},"mm-dd-yyyy":{mask:"1-2-y",placeholder:"mm-dd-yyyy",leapday:"02-29-",separator:"-",alias:"mm/dd/yyyy"},"yyyy.mm.dd":{mask:"y.1.2",placeholder:"yyyy.mm.dd",leapday:".02.29",separator:".",alias:"yyyy/mm/dd"},"yyyy-mm-dd":{mask:"y-1-2",placeholder:"yyyy-mm-dd",leapday:"-02-29",separator:"-",alias:"yyyy/mm/dd"},datetime:{mask:"1/2/y h:s",placeholder:"dd/mm/yyyy hh:mm",alias:"dd/mm/yyyy",regex:{hrspre:new RegExp("[012]"),hrs24:new RegExp("2[0-4]|1[3-9]"),hrs:new RegExp("[01][0-9]|2[0-4]"),ampm:new RegExp("^[a|p|A|P][m|M]"),mspre:new RegExp("[0-5]"),ms:new RegExp("[0-5][0-9]")},timeseparator:":",hourFormat:"24",definitions:{h:{validator:function(a,b,c,d,e){if("24"==e.hourFormat&&24==parseInt(a,10))return b.buffer[c-1]="0",b.buffer[c]="0",{refreshFromBuffer:{start:c-1,end:c},c:"0"};var f=e.regex.hrs.test(a);if(!d&&!f&&(a.charAt(1)==e.timeseparator||-1!="-.:".indexOf(a.charAt(1)))&&(f=e.regex.hrs.test("0"+a.charAt(0))))return b.buffer[c-1]="0",b.buffer[c]=a.charAt(0),c++,{refreshFromBuffer:{start:c-2,end:c},pos:c,c:e.timeseparator};if(f&&"24"!==e.hourFormat&&e.regex.hrs24.test(a)){var g=parseInt(a,10);return 24==g?(b.buffer[c+5]="a",b.buffer[c+6]="m"):(b.buffer[c+5]="p",b.buffer[c+6]="m"),g-=12,10>g?(b.buffer[c]=g.toString(),b.buffer[c-1]="0"):(b.buffer[c]=g.toString().charAt(1),b.buffer[c-1]=g.toString().charAt(0)),{refreshFromBuffer:{start:c-1,end:c+6},c:b.buffer[c]}}return f},cardinality:2,prevalidator:[{validator:function(a,b,c,d,e){var f=e.regex.hrspre.test(a);return d||f||!(f=e.regex.hrs.test("0"+a))?f:(b.buffer[c]="0",c++,{pos:c})},cardinality:1}]},s:{validator:"[0-5][0-9]",cardinality:2,prevalidator:[{validator:function(a,b,c,d,e){var f=e.regex.mspre.test(a);return d||f||!(f=e.regex.ms.test("0"+a))?f:(b.buffer[c]="0",c++,{pos:c})},cardinality:1}]},t:{validator:function(a,b,c,d,e){return e.regex.ampm.test(a+"m")},casing:"lower",cardinality:1}},insertMode:!1,autoUnmask:!1},datetime12:{mask:"1/2/y h:s t\\m",placeholder:"dd/mm/yyyy hh:mm xm",alias:"datetime",hourFormat:"12"},"hh:mm t":{mask:"h:s t\\m",placeholder:"hh:mm xm",alias:"datetime",hourFormat:"12"},"h:s t":{mask:"h:s t\\m",placeholder:"hh:mm xm",alias:"datetime",hourFormat:"12"},"hh:mm:ss":{mask:"h:s:s",placeholder:"hh:mm:ss",alias:"datetime",autoUnmask:!1},"hh:mm":{mask:"h:s",placeholder:"hh:mm",alias:"datetime",autoUnmask:!1},date:{alias:"dd/mm/yyyy"},"mm/yyyy":{mask:"1/y",placeholder:"mm/yyyy",leapday:"donotuse",separator:"/",alias:"mm/dd/yyyy"}}),a.fn.inputmask}(jQuery),function(a){return a.extend(a.inputmask.defaults.definitions,{A:{validator:"[A-Za-zА-яЁёÀ-ÿµ]",cardinality:1,casing:"upper"},"#":{validator:"[0-9A-Za-zА-яЁёÀ-ÿµ]",cardinality:1,casing:"upper"}}),a.extend(a.inputmask.defaults.aliases,{url:{mask:"ir",placeholder:"",separator:"",defaultPrefix:"http://",regex:{urlpre1:new RegExp("[fh]"),urlpre2:new RegExp("(ft|ht)"),urlpre3:new RegExp("(ftp|htt)"),urlpre4:new RegExp("(ftp:|http|ftps)"),urlpre5:new RegExp("(ftp:/|ftps:|http:|https)"),urlpre6:new RegExp("(ftp://|ftps:/|http:/|https:)"),urlpre7:new RegExp("(ftp://|ftps://|http://|https:/)"),urlpre8:new RegExp("(ftp://|ftps://|http://|https://)")},definitions:{i:{validator:function(){return!0},cardinality:8,prevalidator:function(){for(var a=[],b=8,c=0;b>c;c++)a[c]=function(){var a=c;return{validator:function(b,c,d,e,f){if(f.regex["urlpre"+(a+1)]){var g,h=b;a+1-b.length>0&&(h=c.buffer.join("").substring(0,a+1-b.length)+""+h);var i=f.regex["urlpre"+(a+1)].test(h);if(!e&&!i){for(d-=a,g=0;g<f.defaultPrefix.length;g++)c.buffer[d]=f.defaultPrefix[g],d++;for(g=0;g<h.length-1;g++)c.buffer[d]=h[g],d++;return{pos:d}}return i}return!1},cardinality:a}}();return a}()},r:{validator:".",cardinality:50}},insertMode:!1,autoUnmask:!1},ip:{mask:"i[i[i]].i[i[i]].i[i[i]].i[i[i]]",definitions:{i:{validator:function(a,b,c){return c-1>-1&&"."!=b.buffer[c-1]?(a=b.buffer[c-1]+a,a=c-2>-1&&"."!=b.buffer[c-2]?b.buffer[c-2]+a:"0"+a):a="00"+a,new RegExp("25[0-5]|2[0-4][0-9]|[01][0-9][0-9]").test(a)},cardinality:1}}},email:{mask:"*{1,64}[.*{1,64}][.*{1,64}][.*{1,64}]@*{1,64}[.*{2,64}][.*{2,6}][.*{1,2}]",greedy:!1,onBeforePaste:function(a){return a=a.toLowerCase(),a.replace("mailto:","")},definitions:{"*":{validator:"[0-9A-Za-z!#$%&'*+/=?^_`{|}~-]",cardinality:1,casing:"lower"}}}}),a.fn.inputmask}(jQuery),function(a){return a.extend(a.inputmask.defaults.aliases,{numeric:{mask:function(a){if(0!==a.repeat&&isNaN(a.integerDigits)&&(a.integerDigits=a.repeat),a.repeat=0,a.groupSeparator==a.radixPoint&&(a.groupSeparator="."==a.radixPoint?",":","==a.radixPoint?".":"")," "===a.groupSeparator&&(a.skipOptionalPartCharacter=void 0),a.autoGroup=a.autoGroup&&""!=a.groupSeparator,a.autoGroup&&isFinite(a.integerDigits)){var b=Math.floor(a.integerDigits/a.groupSize),c=a.integerDigits%a.groupSize;a.integerDigits+=0==c?b-1:b}a.definitions[";"]=a.definitions["~"];var d=a.prefix;return d+="[+]",d+="~{1,"+a.integerDigits+"}",void 0!=a.digits&&(isNaN(a.digits)||parseInt(a.digits)>0)&&(d+=a.digitsOptional?"["+(a.decimalProtect?":":a.radixPoint)+";{"+a.digits+"}]":(a.decimalProtect?":":a.radixPoint)+";{"+a.digits+"}"),d+=a.suffix},placeholder:"",greedy:!1,digits:"*",digitsOptional:!0,groupSeparator:"",radixPoint:".",radixFocus:!0,groupSize:3,autoGroup:!1,allowPlus:!0,allowMinus:!0,integerDigits:"+",prefix:"",suffix:"",rightAlign:!0,decimalProtect:!0,postFormat:function(b,c,d,e){var f=!1,g=b[c];if(""==e.groupSeparator||-1!=a.inArray(e.radixPoint,b)&&c>=a.inArray(e.radixPoint,b)||new RegExp("[-+]").test(g))return{pos:c};var h=b.slice();g==e.groupSeparator&&(h.splice(c--,1),g=h[c]),d?h[c]="?":h.splice(c,0,"?");var i=h.join("");if(i.length>0&&e.autoGroup||d&&-1!=i.indexOf(e.groupSeparator)){var j=a.inputmask.escapeRegex.call(this,e.groupSeparator);f=0==i.indexOf(e.groupSeparator),i=i.replace(new RegExp(j,"g"),"");var k=i.split(e.radixPoint);if(i=""==e.radixPoint?i:k[0],i!=e.prefix+"?0"&&i.length>=e.groupSize+e.prefix.length){f=!0;for(var l=new RegExp("([-+]?[\\d?]+)([\\d?]{"+e.groupSize+"})");l.test(i);)i=i.replace(l,"$1"+e.groupSeparator+"$2"),i=i.replace(e.groupSeparator+e.groupSeparator,e.groupSeparator)}""!=e.radixPoint&&k.length>1&&(i+=e.radixPoint+k[1])}b.length=i.length;for(var m=0,n=i.length;n>m;m++)b[m]=i.charAt(m);var o=a.inArray("?",b);return d?b[o]=g:b.splice(o,1),{pos:o,refreshFromBuffer:f}},onKeyDown:function(b,c,d,e){if(b.keyCode==a.inputmask.keyCode.TAB&&"0"!=e.placeholder.charAt(0)){var f=a.inArray(e.radixPoint,c);if(-1!=f&&isFinite(e.digits)){for(var g=1;g<=e.digits;g++)(void 0==c[f+g]||c[f+g]==e.placeholder.charAt(0))&&(c[f+g]="0");return{refreshFromBuffer:{start:++f,end:f+e.digits}}}}else if(e.autoGroup&&(b.keyCode==a.inputmask.keyCode.DELETE||b.keyCode==a.inputmask.keyCode.BACKSPACE)){var h=e.postFormat(c,d-1,!0,e);return h.caret=h.pos+1,h}},onKeyPress:function(a,b,c,d){if(d.autoGroup){var e=d.postFormat(b,c-1,!0,d);return e.caret=e.pos+1,e}},postProcessOnBlur:function(a,b){var c=""!=b.radixPoint?a.join("").split(b.radixPoint):[a.join("")],d=c[0].match(b.regex.integerPart(b)),e=2==c.length?c[1].match(b.regex.integerNPart(b)):void 0;d&&"-0"==d[d.index]&&(void 0==e||e[e.index].match(/^0+$/))&&a.splice(0,1)},regex:{integerPart:function(){return new RegExp("[-+]?\\d+")},integerNPart:function(){return new RegExp("\\d+")}},signHandler:function(a,b,c,d,e){if(!d&&(e.allowMinus&&"-"===a||e.allowPlus&&"+"===a)){var f=b.buffer.join("").match(e.regex.integerPart(e));if(f&&f[f.index].length>0&&("0"!==f[f.index]||b.buffer&&b._buffer&&b.buffer.join("")!=b._buffer.join("")))return b.buffer[f.index]==("-"===a?"+":"-")?{pos:f.index,c:a,remove:f.index,caret:c}:b.buffer[f.index]==("-"===a?"-":"+")?{remove:f.index,caret:c-1}:{pos:f.index,c:a,caret:c+1}}return!1},radixHandler:function(b,c,d,e,f){if(!e&&b===f.radixPoint&&f.digits>0){var g=a.inArray(f.radixPoint,c.buffer),h=c.buffer.join("").match(f.regex.integerPart(f));if(-1!=g&&c.validPositions[g])return c.validPositions[g-1]?{caret:g+1}:{pos:h.index,c:h[0],caret:g+1};if(!h||"0"==h[0])return c.buffer[h?h.index:d]="0",{pos:(h?h.index:d)+1}}return!1},leadingZeroHandler:function(b,c,d,e,f){var g=c.buffer.join("").match(f.regex.integerNPart(f)),h=a.inArray(f.radixPoint,c.buffer);if(g&&!e&&(-1==h||g.index<h))if("0"==g[0]&&d>=f.prefix.length){if(-1==h||h>=d&&void 0==c.validPositions[h])return c.buffer.splice(g.index,1),d=d>g.index?d-1:g.index,{pos:d,remove:g.index};if(d>g.index&&h>=d)return c.buffer.splice(g.index,1),d=d>g.index?d-1:g.index,{pos:d,remove:g.index};if(void 0==c.validPositions[h])return c.buffer[d]=b,{refreshFromBuffer:!0}}else if("0"==b&&d<=g.index)return!1;return!0},definitions:{"~":{validator:function(b,c,d,e,f){var g=f.signHandler(b,c,d,e,f);if(!g&&(g=f.radixHandler(b,c,d,e,f),!g&&(g=e?new RegExp("[0-9"+a.inputmask.escapeRegex.call(this,f.groupSeparator)+"]").test(b):new RegExp("[0-9]").test(b),g===!0&&(g=f.leadingZeroHandler(b,c,d,e,f),g===!0)))){var h=a.inArray(f.radixPoint,c.buffer);g=f.digitsOptional===!1&&d>h&&!e?{pos:d,remove:d}:{pos:d}}return g},cardinality:1,prevalidator:null},"+":{validator:function(a,b,c,d,e){var f=e.signHandler(a,b,c,d,e);return f||(f=e.allowMinus&&"-"==a||e.allowPlus&&"+"==a),f},cardinality:1,prevalidator:null,placeholder:""},":":{validator:function(b,c,d,e,f){var g=f.signHandler(b,c,d,e,f);if(!g){var h="["+a.inputmask.escapeRegex.call(this,f.radixPoint)+"]";g=new RegExp(h).test(b),g&&c.validPositions[d]&&c.validPositions[d].match.placeholder==f.radixPoint&&(g={pos:d,remove:d})}return g},cardinality:1,prevalidator:null,placeholder:function(a){return a.radixPoint}}},insertMode:!0,autoUnmask:!1,onUnMask:function(b,c,d){var e=b.replace(d.prefix,"");return e=e.replace(d.suffix,""),e=e.replace(new RegExp(a.inputmask.escapeRegex.call(this,d.groupSeparator),"g"),"")},isComplete:function(b,c){var d=b.join(""),e=b.slice();if(c.postFormat(e,0,!0,c),e.join("")!=d)return!1;var f=d.replace(c.prefix,"");return f=f.replace(c.suffix,""),f=f.replace(new RegExp(a.inputmask.escapeRegex.call(this,c.groupSeparator),"g"),""),f=f.replace(a.inputmask.escapeRegex.call(this,c.radixPoint),"."),isFinite(f)
},onBeforeMask:function(b,c){if(""!=c.radixPoint&&isFinite(b))b=b.toString().replace(".",c.radixPoint);else{var d=b.match(/,/g),e=b.match(/\./g);e&&d?e.length>d.length?(b=b.replace(/\./g,""),b=b.replace(",",c.radixPoint)):d.length>e.length?(b=b.replace(/,/g,""),b=b.replace(".",c.radixPoint)):b=b.indexOf(".")<b.indexOf(",")?b.replace(/\./g,""):b=b.replace(/,/g,""):b=b.replace(new RegExp(a.inputmask.escapeRegex.call(this,c.groupSeparator),"g"),"")}return 0==c.digits&&(-1!=b.indexOf(".")?b=b.substring(0,b.indexOf(".")):-1!=b.indexOf(",")&&(b=b.substring(0,b.indexOf(",")))),b}},currency:{prefix:"$ ",groupSeparator:",",alias:"numeric",placeholder:"0",autoGroup:!0,digits:2,digitsOptional:!1,clearMaskOnLostFocus:!1},decimal:{alias:"numeric"},integer:{alias:"numeric",digits:"0",radixPoint:""}}),a.fn.inputmask}(jQuery),function(a){return a.extend(a.inputmask.defaults.aliases,{phone:{url:"phone-codes/phone-codes.js",maskInit:"+pp(pp)pppppppp",countrycode:"",mask:function(b){b.definitions={p:{validator:function(){return!1},cardinality:1},"#":{validator:"[0-9]",cardinality:1}};var c=[];return a.ajax({url:b.url,async:!1,dataType:"json",success:function(a){c=a},error:function(a,c,d){alert(d+" - "+b.url)}}),c=c.sort(function(a,b){return(a.mask||a)<(b.mask||b)?-1:1}),""!=b.countrycode&&(b.maskInit="+"+b.countrycode+b.maskInit.substring(3)),c.splice(0,0,b.maskInit),c},nojumps:!0,nojumpsThreshold:1,onBeforeMask:function(a,b){var c=a.replace(/^0/g,"");return(c.indexOf(b.countrycode)>1||-1==c.indexOf(b.countrycode))&&(c=b.countrycode+c),c}},phonebe:{alias:"phone",url:"phone-codes/phone-be.js",countrycode:"32",nojumpsThreshold:4}}),a.fn.inputmask}(jQuery),function(a){return a.extend(a.inputmask.defaults.aliases,{Regex:{mask:"r",greedy:!1,repeat:"*",regex:null,regexTokens:null,tokenizer:/\[\^?]?(?:[^\\\]]+|\\[\S\s]?)*]?|\\(?:0(?:[0-3][0-7]{0,2}|[4-7][0-7]?)?|[1-9][0-9]*|x[0-9A-Fa-f]{2}|u[0-9A-Fa-f]{4}|c[A-Za-z]|[\S\s]?)|\((?:\?[:=!]?)?|(?:[?*+]|\{[0-9]+(?:,[0-9]*)?\})\??|[^.?*+^${[()|\\]+|./g,quantifierFilter:/[0-9]+[^,]/,isComplete:function(a,b){return new RegExp(b.regex).test(a.join(""))},definitions:{r:{validator:function(b,c,d,e,f){function g(a,b){this.matches=[],this.isGroup=a||!1,this.isQuantifier=b||!1,this.quantifier={min:1,max:1},this.repeaterPart=void 0}function h(){var a,b,c=new g,d=[];for(f.regexTokens=[];a=f.tokenizer.exec(f.regex);)switch(b=a[0],b.charAt(0)){case"(":d.push(new g(!0));break;case")":var e=d.pop();d.length>0?d[d.length-1].matches.push(e):c.matches.push(e);break;case"{":case"+":case"*":var h=new g(!1,!0);b=b.replace(/[{}]/g,"");var i=b.split(","),j=isNaN(i[0])?i[0]:parseInt(i[0]),k=1==i.length?j:isNaN(i[1])?i[1]:parseInt(i[1]);if(h.quantifier={min:j,max:k},d.length>0){var l=d[d.length-1].matches;if(a=l.pop(),!a.isGroup){var e=new g(!0);e.matches.push(a),a=e}l.push(a),l.push(h)}else{if(a=c.matches.pop(),!a.isGroup){var e=new g(!0);e.matches.push(a),a=e}c.matches.push(a),c.matches.push(h)}break;default:d.length>0?d[d.length-1].matches.push(b):c.matches.push(b)}c.matches.length>0&&f.regexTokens.push(c)}function i(b,c){var d=!1;c&&(k+="(",m++);for(var e=0;e<b.matches.length;e++){var f=b.matches[e];if(1==f.isGroup)d=i(f,!0);else if(1==f.isQuantifier){var g=a.inArray(f,b.matches),h=b.matches[g-1],j=k;if(isNaN(f.quantifier.max)){for(;f.repeaterPart&&f.repeaterPart!=k&&f.repeaterPart.length>k.length&&!(d=i(h,!0)););d=d||i(h,!0),d&&(f.repeaterPart=k),k=j+f.quantifier.max}else{for(var l=0,o=f.quantifier.max-1;o>l&&!(d=i(h,!0));l++);k=j+"{"+f.quantifier.min+","+f.quantifier.max+"}"}}else if(void 0!=f.matches)for(var p=0;p<f.length&&!(d=i(f[p],c));p++);else{var q;if("["==f.charAt(0)){q=k,q+=f;for(var r=0;m>r;r++)q+=")";var s=new RegExp("^("+q+")$");d=s.test(n)}else for(var t=0,u=f.length;u>t;t++)if("\\"!=f.charAt(t)){q=k,q+=f.substr(0,t+1),q=q.replace(/\|$/,"");for(var r=0;m>r;r++)q+=")";var s=new RegExp("^("+q+")$");if(d=s.test(n))break}k+=f}if(d)break}return c&&(k+=")",m--),d}null==f.regexTokens&&h();var j=c.buffer.slice(),k="",l=!1,m=0;j.splice(d,0,b);for(var n=j.join(""),o=0;o<f.regexTokens.length;o++){var g=f.regexTokens[o];if(l=i(g,g.isGroup))break}return l},cardinality:1}}}}),a.fn.inputmask}(jQuery);

/*
 JavaScript autoComplete v1.0.4
 Copyright (c) 2014 Simon Steinberger / Pixabay
 GitHub: https://github.com/Pixabay/JavaScript-autoComplete
 License: http://www.opensource.org/licenses/mit-license.php
 */

var autoComplete = (function(){
    // "use strict";
    function autoComplete(options){
        if (!document.querySelector) return;

        // helpers
        function hasClass(el, className){ return el.classList ? el.classList.contains(className) : new RegExp('\\b'+ className+'\\b').test(el.className); }

        function addEvent(el, type, handler){
            if (el.attachEvent) el.attachEvent('on'+type, handler); else el.addEventListener(type, handler);
        }
        function removeEvent(el, type, handler){
            // if (el.removeEventListener) not working in IE11
            if (el.detachEvent) el.detachEvent('on'+type, handler); else el.removeEventListener(type, handler);
        }
        function live(elClass, event, cb, context){
            addEvent(context || document, event, function(e){
                var found, el = e.target || e.srcElement;
                while (el && !(found = hasClass(el, elClass))) el = el.parentElement;
                if (found) cb.call(el, e);
            });
        }

        var o = {
            selector: 0,
            source: 0,
            minChars: 3,
            delay: 150,
            offsetLeft: 0,
            offsetTop: 1,
            cache: 1,
            menuClass: '',
            renderItem: function (item, search){
                // escape special characters
                search = search.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&');
                var re = new RegExp("(" + search.split(' ').join('|') + ")", "gi");
                return '<div class="autocomplete-suggestion" data-val="' + item + '">' + item.replace(re, "<b>$1</b>") + '</div>';
            },
            onSelect: function(e, term, item){}
        };
        for (var k in options) { if (options.hasOwnProperty(k)) o[k] = options[k]; }

        // init
        var elems = typeof o.selector == 'object' ? [o.selector] : document.querySelectorAll(o.selector);
        for (var i=0; i<elems.length; i++) {
            var that = elems[i];

            // create suggestions container "sc"
            that.sc = document.createElement('div');
            that.sc.className = 'autocomplete-suggestions '+o.menuClass;

            that.autocompleteAttr = that.getAttribute('autocomplete');
            that.setAttribute('autocomplete', 'off');
            that.cache = {};
            that.last_val = '';

            that.updateSC = function(resize, next){
                var rect = that.getBoundingClientRect();
                that.sc.style.left = Math.round(rect.left + (window.pageXOffset || document.documentElement.scrollLeft) + o.offsetLeft) + 'px';
                that.sc.style.top = Math.round(rect.bottom + (window.pageYOffset || document.documentElement.scrollTop) + o.offsetTop) + 'px';
                that.sc.style.width = Math.round(rect.right - rect.left) + 'px'; // outerWidth
                if (!resize) {
                    that.sc.style.display = 'block';
                    if (!that.sc.maxHeight) { that.sc.maxHeight = parseInt((window.getComputedStyle ? getComputedStyle(that.sc, null) : that.sc.currentStyle).maxHeight); }
                    if (!that.sc.suggestionHeight) that.sc.suggestionHeight = that.sc.querySelector('.autocomplete-suggestion').offsetHeight;
                    if (that.sc.suggestionHeight)
                        if (!next) that.sc.scrollTop = 0;
                        else {
                            var scrTop = that.sc.scrollTop, selTop = next.getBoundingClientRect().top - that.sc.getBoundingClientRect().top;
                            if (selTop + that.sc.suggestionHeight - that.sc.maxHeight > 0)
                                that.sc.scrollTop = selTop + that.sc.suggestionHeight + scrTop - that.sc.maxHeight;
                            else if (selTop < 0)
                                that.sc.scrollTop = selTop + scrTop;
                        }
                }
            }
            addEvent(window, 'resize', that.updateSC);
            document.body.appendChild(that.sc);

            live('autocomplete-suggestion', 'mouseleave', function(e){
                var sel = that.sc.querySelector('.autocomplete-suggestion.selected');
                if (sel) setTimeout(function(){ sel.className = sel.className.replace('selected', ''); }, 20);
            }, that.sc);

            live('autocomplete-suggestion', 'mouseover', function(e){
                var sel = that.sc.querySelector('.autocomplete-suggestion.selected');
                if (sel) sel.className = sel.className.replace('selected', '');
                this.className += ' selected';
            }, that.sc);

            live('autocomplete-suggestion', 'mousedown', function(e){
                if (hasClass(this, 'autocomplete-suggestion')) { // else outside click
                    var v = this.getAttribute('data-val');
                    that.value = v;
                    o.onSelect(e, v, this);
                    that.sc.style.display = 'none';
                }
            }, that.sc);

            that.blurHandler = function(){
                try { var over_sb = document.querySelector('.autocomplete-suggestions:hover'); } catch(e){ var over_sb = 0; }
                if (!over_sb) {
                    that.last_val = that.value;
                    that.sc.style.display = 'none';
                    setTimeout(function(){ that.sc.style.display = 'none'; }, 350); // hide suggestions on fast input
                } else if (that !== document.activeElement) setTimeout(function(){ that.focus(); }, 20);
            };
            addEvent(that, 'blur', that.blurHandler);

            var suggest = function(data){
                var val = that.value;
                that.cache[val] = data;
                if (data.length && val.length >= o.minChars) {
                    var s = '';
                    for (var i=0;i<data.length;i++) s += o.renderItem(data[i], val);
                    that.sc.innerHTML = s;
                    that.updateSC(0);
                }
                else
                    that.sc.style.display = 'none';
            }

            that.keydownHandler = function(e){
                var key = window.event ? e.keyCode : e.which;
                // down (40), up (38)
                if ((key == 40 || key == 38) && that.sc.innerHTML) {
                    var next, sel = that.sc.querySelector('.autocomplete-suggestion.selected');
                    if (!sel) {
                        next = (key == 40) ? that.sc.querySelector('.autocomplete-suggestion') : that.sc.childNodes[that.sc.childNodes.length - 1]; // first : last
                        next.className += ' selected';
                        that.value = next.getAttribute('data-val');
                    } else {
                        next = (key == 40) ? sel.nextSibling : sel.previousSibling;
                        if (next) {
                            sel.className = sel.className.replace('selected', '');
                            next.className += ' selected';
                            that.value = next.getAttribute('data-val');
                        }
                        else { sel.className = sel.className.replace('selected', ''); that.value = that.last_val; next = 0; }
                    }
                    that.updateSC(0, next);
                    return false;
                }
                // esc
                else if (key == 27) { that.value = that.last_val; that.sc.style.display = 'none'; }
                // enter
                else if (key == 13 || key == 9) {
                    var sel = that.sc.querySelector('.autocomplete-suggestion.selected');
                    if (sel && that.sc.style.display != 'none') { o.onSelect(e, sel.getAttribute('data-val'), sel); setTimeout(function(){ that.sc.style.display = 'none'; }, 20); }
                }
            };
            addEvent(that, 'keydown', that.keydownHandler);

            that.keyupHandler = function(e){
                var key = window.event ? e.keyCode : e.which;
                if (!key || (key < 35 || key > 40) && key != 13 && key != 27) {
                    var val = that.value;
                    if (val.length >= o.minChars) {
                        if (val != that.last_val) {
                            that.last_val = val;
                            clearTimeout(that.timer);
                            if (o.cache) {
                                if (val in that.cache) { suggest(that.cache[val]); return; }
                                // no requests if previous suggestions were empty
                                for (var i=1; i<val.length-o.minChars; i++) {
                                    var part = val.slice(0, val.length-i);
                                    if (part in that.cache && !that.cache[part].length) { suggest([]); return; }
                                }
                            }
                            that.timer = setTimeout(function(){ o.source(val, suggest) }, o.delay);
                        }
                    } else {
                        that.last_val = val;
                        that.sc.style.display = 'none';
                    }
                }
            };
            addEvent(that, 'keyup', that.keyupHandler);

            that.focusHandler = function(e){
                that.last_val = '\n';
                that.keyupHandler(e)
            };
            if (!o.minChars) addEvent(that, 'focus', that.focusHandler);
        }

        // public destroy method
        this.destroy = function(){
            for (var i=0; i<elems.length; i++) {
                var that = elems[i];
                removeEvent(window, 'resize', that.updateSC);
                removeEvent(that, 'blur', that.blurHandler);
                removeEvent(that, 'focus', that.focusHandler);
                removeEvent(that, 'keydown', that.keydownHandler);
                removeEvent(that, 'keyup', that.keyupHandler);
                if (that.autocompleteAttr)
                    that.setAttribute('autocomplete', that.autocompleteAttr);
                else
                    that.removeAttribute('autocomplete');
                document.body.removeChild(that.sc);
                that = null;
            }
        };
    }
    return autoComplete;
})();

(function(){
    if (typeof define === 'function' && define.amd)
        define('autoComplete', function () { return autoComplete; });
    else if (typeof module !== 'undefined' && module.exports)
        module.exports = autoComplete;
    else
        window.autoComplete = autoComplete;
})();



function page_loader(_v){
	if(_v == 'hide'){
		$('.page_loader').hide();
	}else{
		$('.page_loader').show();
	}
}

function setLocation(url){
    window.location.href = url;
}
function popupWindow(url){
	var ww = jQuery(window).width();
	var left = (ww - 980)/2;
   window.open(''+url,'mywin',
'left='+left+',top=0,width=980,height=600,toolbar=0,resizable=1,scrollbars=1');
}

function redirectConfirm(message,title, url) {
 	jConfirm( message, title,function(result){
		if(result){			
			setLocation(url);
		}
	});
}

function parseDate(str,format) {
	if(typeof(format) == 'undefined'){// dd/mm/yyyy
    	var mdy = str.split('/')
    	return new Date(mdy[0], mdy[1]-1,mdy[2]);
		
	}else if(format == 'dd-mm-yyyy'){
		var mdy = str.split('-')
    	return new Date(mdy[2], mdy[1]-1,mdy[0]);
	}
	else if(format == 'yy-mm-dd'){
		var mdy = str.split('-')
    	return new Date(mdy[0], mdy[1]-1,mdy[2]);
	}
}

function daydiff(first, second) {
    return Math.abs(second-first)/(1000*60*60*24)
}


function deleteConfirm(message,title, url,ispost,ele)
{
 	jConfirm( message, title,function(result)
	{
		if(result){
			if(ispost == true){
				jQuery(ele).fadeOut('slow')
				jQuery.post(url,function(data){
									 
				});
			}else{
				setLocation(url);
			}
		}
	});
}

function set_select2(){
	var cb_s2_placeholder = '';
	if(jQuery('.cb_s2').has('placeholder')){
		cb_s2_placeholder = jQuery('.cb_s2').attr('placeholder');
	}
	jQuery('.cb_s2').select2({ placeholder: cb_s2_placeholder});
}

function createEditor(ele){
	jEditor(ele);
}

jQuery(document).ready(function(e) {
    jQuery('.subNav-tab').find('li:first').addClass('first');
	jQuery('.subNav-tab').find('li:last').addClass('last');
	set_select2();
	jQuery('table.oe tbody tr:even').addClass('even');
	jQuery('table.oe tbody tr:odd').addClass('odd');
	
	jQuery('table.fl tbody tr:first').addClass('first');
	jQuery('table.fl tbody tr:last').addClass('last');
		
	jQuery('.tab-info div.tabs').hide();
	jQuery('.tab_nav li a').click(function(){
		jQuery('.tab_nav li').removeClass('active');
		jQuery(this).parent().addClass('active');
		var currentTab = jQuery(this).attr('href');
		jQuery('.tab-info').find('div.tabs').hide();
		jQuery('.tab-info').find(currentTab+'-tab').show();
	
		//return false;
	});
	var w_hash = window.location.hash;

	if(w_hash != ''){
		if(w_hash == '#upload_images'){			
			jQuery(jQuery('.tab_nav li:first a').attr('href')+'-tab').show();
		}else{
			jQuery('.tab_nav li').find('a[href="'+w_hash+'"]').click();
		}
	}else{
		jQuery('.tab_nav li:first a').click();
	}

    $('.form-actions .btn-submit').click(function(){
        $data_id = $(this).data('id');
        $('#hidden-action').val($data_id);
    });
});

function setTabErrors(ele){
	jQuery(ele+' li').removeClass('tab-error');
					
	jQuery(ele).find('a').each(function(i){
		_self = jQuery(this);									   
		_link = jQuery(this).attr('href');	 						
								
		_length = jQuery(_link+'-tab').find('.validation-advice').filter(function() {
			return jQuery(this).css('display') !== 'none';
		}).length;
		
		if(_length > 0 ){
			jQuery(this).parent().addClass('tab-error');							
		}						
	 });	
}
function setReqFill(ele){
	jQuery(ele+' li').removeClass('not-completed');
					
	jQuery(ele).find('a').each(function(i){
		_self = jQuery(this);									   
		_link = jQuery(this).attr('href');	 						

		_length = jQuery(_link+'-tab').find('.req-entry').filter(function() {
			if(jQuery(this).val() == ""){
			return jQuery(this).css('display') !== 'none';
			}
						
		}).length;
		
		if(_length > 0 ){
			jQuery(this).parent().addClass('not-completed');							
		}						
	 });	
}

function get_mag_gray_style(){
	var featureOpts = [
		{
		  featureType: 'all',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}
		,{
		  featureType: 'landscape',
		  stylers: [
			{ visibility: 'on'},{ color: '#ffffff' }	
		  ]
		}
		,{
		  featureType: 'landscape',
		  elementType: 'labels',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}
		
		,{
		  featureType: 'administrative.country',
		  elementType : 'labels',
		  stylers: [
			{ visibility: 'off'}
	
		  ]
		}
		,{
		  featureType: 'administrative.country',
		  elementType : 'geometry.stroke',
		  stylers: [
			{ visibility: 'on'},
			{ color: '#888888' },
			{ weight: 1 }
	
		  ]
		}
		,{
		  featureType: 'water',
		  stylers: [
			{ visibility: 'on' },
			{ color: '#d6d6d6' }
		  ]
		}
		,{
		  featureType: 'water',
		  elementType: 'labels',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}	
	  ];
	  
	  return featureOpts;
}

function get_mag_style(){
	var featureOpts = [
		{
		  featureType: 'all',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}
		,{
		  featureType: 'landscape',
		  stylers: [
			{ visibility: 'on'},
			{ color: '#ffffff' }
	
		  ]
		}
		,{
		  featureType: 'landscape',
		  elementType: 'labels',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}
		,{
		  featureType: 'administrative.country',
		  elementType : 'labels',
		  stylers: [
			{ visibility: 'off'}
	
		  ]
		}
		,{
		  featureType: 'administrative.country',
		  elementType : 'geometry.stroke',
		  stylers: [
			{ visibility: 'on'},
			{ color: '#888888' },
			{ weight: 1 }
	
		  ]
		}
		,{
		  featureType: 'water',
		  stylers: [
			{ visibility: 'on' },
			{ color: '#80d1f1' }
		  ]
		}
		,{
		  featureType: 'water',
		  elementType: 'labels',
		  stylers: [
			{ visibility: 'off' }
		  ]
		}	
	  ];
	  
	  return featureOpts;
}

var map;


function setMarkers(map, locations,type,zoom) {
	var marker_pin = base_url+"skin/images/pin.png";
	var marker_pin_selected = base_url+"skin/images/pin_selected.png";
	var marker_pin_airport = base_url+"skin/images/pin_airport.png";

	 var geocoder = new google.maps.Geocoder();

     var pin_image = new google.maps.MarkerImage(marker_pin);
     var pin_airport_image = new google.maps.MarkerImage(marker_pin_airport);
	 
     var shape = {
          coord: [1, 1, 1, 20, 18, 20, 18 , 1],
          type: 'poly'
     };
	 
     for (var i = 0; i < locations.length; i++) {
          	var loc = locations[i];
			var markerLat = loc[0];
			var markerLng = loc[1];
		  	var country = loc[2];
			var state = loc[3];
			var city = loc[4];
		
			icon = pin_image;
			
			
			var address = '';
			var lbl = '';
			
			if(type == 'world'){
				 address = country;
				 lbl  = country;
			}else if(type == 'country'){
				 address = country;
				 lbl  = country;
			}else if(type == 'state'){
				 address = state+','+country;
				 lbl  = state;
			}else if(type == 'city'){
				 if(state == ''){
					 address = city+','+country;
				 }else{
					 address = city+','+state+','+country;
				 }
				 
				 lbl  = city;
			}
						
			//console.log(type);
			//console.log(lbl);
			
			var center_map = new google.maps.LatLng(markerLat,markerLng);
			
			var marker = new google.maps.Marker({
				position: center_map,
				map: map,
				icon: icon
			});
			
			 var label = new Label({map: map});
			  label.set('zIndex', 1234);
			  label.bindTo('position', marker, 'position');
			  label.set('text', lbl);
			  
			   	
     }
}
function country_map(ele,locations,type,centerLat,centerLng,zoom,style,panControlSt)
{
	var center_map = new google.maps.LatLng(centerLat,centerLng);
	var MY_MAPTYPE_ID = 'custom_style';	
	var featureOpts = get_mag_style();
	
	if(typeof(style) != 'undefined'){
		if(style == 'gray'){
			var featureOpts = get_mag_gray_style();	
		}		
	}
	if(typeof(panControlSt) == 'undefined'){
		panControlSt = true;
	}
		
	if(type == 'world')
	{
		var mapOptions = {
			zoom: zoom,
			center: center_map,
			disableDefaultUI: true,
			panControl: panControlSt,
			panControlOptions: {
			  position: google.maps.ControlPosition.LEFT_CENTER
			},
			draggable: true,
			zoomControl: true,	
			zoomControlOptions: {
			  style:google.maps.ZoomControlStyle.SMALL
			  ,position: google.maps.ControlPosition.LEFT_CENTER
			},
			scrollwheel: false,
			mapTypeId: MY_MAPTYPE_ID
		};
		
		map = new google.maps.Map(document.getElementById(ele), mapOptions);
		
		
		var styledMapOptions = {name: 'Custom Style'};	
		var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);	
		map.mapTypes.set(MY_MAPTYPE_ID, customMapType);				
		setMarkers(map, locations,type,zoom);
		
		
	}
	else{	
				
		var mapOptions = {
			zoom: zoom,
			center: center_map,
			disableDefaultUI: true,
			panControl: panControlSt,
			panControlOptions: {
			  position: google.maps.ControlPosition.LEFT_CENTER
			},
			draggable: true,
			zoomControl: true,	
			zoomControlOptions: {
			  style:google.maps.ZoomControlStyle.SMALL
			  ,position: google.maps.ControlPosition.LEFT_CENTER
			},
			scrollwheel: false,
			mapTypeId: MY_MAPTYPE_ID
		};
		
		map = new google.maps.Map(document.getElementById(ele), mapOptions);
		
		var styledMapOptions = {name: 'Custom Style'};	
		var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);	
		map.mapTypes.set(MY_MAPTYPE_ID, customMapType);	
			
		setMarkers(map, locations,type,zoom);	 
	}
	
	google.maps.event.addListener(map, 'center_changed', function() {
		var posMapC = this.getCenter();	
		//console.log(posMapC.lat());
		if(this.b.id == 'map-v-canvas'){	
			jQuery('#map_v_lat').val(posMapC.lat());
			jQuery('#map_v_lng').val(posMapC.lng());
		}
		else if(this.b.id == 'map-t-canvas'){								
			jQuery('#map_t_lat').val(posMapC.lat());
			jQuery('#map_t_lng').val(posMapC.lng());
		}
		else{			
			jQuery('#map_lat').val(posMapC.lat());
			jQuery('#map_lng').val(posMapC.lng());
		}
	});
	
	 google.maps.event.addListener(map, 'zoom_changed', function() {
		var zoomLevel = this.getZoom();	 
		if(this.b.id == 'map-v-canvas'){								
			jQuery('#map_v_zoom').val(zoomLevel);
		}
		else if(this.b.id == 'map-t-canvas'){								
			jQuery('#map_t_zoom').val(zoomLevel);
		}
		else{
			jQuery('#map_zoom').val(zoomLevel);
		}
	});	
}

function word_map(ele,locations,type,lat,lng,zoom){
	var map;
	//console.log(lat);
	var center_map = new google.maps.LatLng(lat,lng);
		

	//var center_map = new google.maps.LatLng(7.284459, 80.637459);
	
	var MY_MAPTYPE_ID = 'custom_style';	
	var featureOpts = get_mag_style();
	
	var mapOptions = {
		zoom: zoom,
		center: center_map,
		disableDefaultUI: true,
		zoomControl: true,
		scrollwheel: false,
		mapTypeId: MY_MAPTYPE_ID
	};
	
	map = new google.maps.Map(document.getElementById(ele), mapOptions);
	
	var styledMapOptions = {name: 'Custom Style'};	
	var customMapType = new google.maps.StyledMapType(featureOpts, styledMapOptions);	
	map.mapTypes.set(MY_MAPTYPE_ID, customMapType);
	setMarkers(map, locations,type,zoom);	 
	
	var geocoder = new google.maps.Geocoder();
    var image = new google.maps.MarkerImage(base_url+"skin/adminhtml/images/pin.png");
	
	google.maps.event.addListener(map, 'center_changed', function() {
		var posMapC = map.getCenter();
		//console.log(posMapC.lb);
		jQuery.post(base_url+'admin/settings/settings/set_world_map_position',{'world_map_lat':posMapC.lb
				,'world_map_lng':posMapC.mb});
	  });
	
	for (var i = 0; i < locations.length; i++)
	{
		var loc = locations[i];
		var country = loc[0];
		var state = loc[1];
		var city = loc[2];
		var address = '';
		var lbl = '';
		if(type == 'country'){
			 address = country;
			 lbl  = country;
		}else if(type == 'state'){
			 address = state+','+country;
			 lbl  = state;
		}else if(type == 'city'){
			 if(state == ''){
				 address = city+','+country;
			 }else{
				 address = city+','+state+','+country;
			 }
			 
			 lbl  = city;
		}
		
		geocoder.geocode({ 'address': address}, (function(name,lbl) {
				return function(results, status) {
					if (status == google.maps.GeocoderStatus.OK) {
						
						//if(maker_on == true){
							var marker = new google.maps.Marker({
								position: results[0].geometry.location,
								map: map,
								icon: image
							});
						//}
						
						
						//if(label_on == true){
						  var label = new Label({map: map});
						  label.set('zIndex', 1234);
						  label.bindTo('position', marker, 'position');
						  label.set('text', lbl);
						  
						  google.maps.event.addListener(map, 'zoom_changed', function() {
						    var zoomLevel = map.getZoom();
							//if(type != 'country'){
								if(zoomLevel > zoom - 2){
									label.set('text', lbl);
								}else{
									label.set('text', '');
								}
							//}
							
							if(map.b.id == 'map-v-canvas'){								
								jQuery('#map_v_zoom').val(zoomLevel);
							}else{
								jQuery('#map_zoom').val(zoomLevel);
							}
							
							jQuery.post(base_url+'admin/settings/settings/set_world_map_position',{'world_map_zoom':zoomLevel});
							
						  });
						  
						  
						  //label.bindTo('text', marker, 'position');	
						//}
					}
				};
			}(address,lbl)));
		}
}

function base64_decode (data) {
  // http://kevin.vanzonneveld.net
  // +   original by: Tyler Akins (http://rumkin.com)
  // +   improved by: Thunder.m
  // +      input by: Aman Gupta
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +   bugfixed by: Onno Marsman
  // +   bugfixed by: Pellentesque Malesuada
  // +   improved by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // +      input by: Brett Zamir (http://brett-zamir.me)
  // +   bugfixed by: Kevin van Zonneveld (http://kevin.vanzonneveld.net)
  // *     example 1: base64_decode('S2V2aW4gdmFuIFpvbm5ldmVsZA==');
  // *     returns 1: 'Kevin van Zonneveld'
  // mozilla has this native
  // - but breaks in 2.0.0.12!
  //if (typeof this.window['atob'] == 'function') {
  //    return atob(data);
  //}
  var b64 = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
  var o1, o2, o3, h1, h2, h3, h4, bits, i = 0,
    ac = 0,
    dec = "",
    tmp_arr = [];

  if (!data) {
    return data;
  }

  data += '';

  do { // unpack four hexets into three octets using index points in b64
    h1 = b64.indexOf(data.charAt(i++));
    h2 = b64.indexOf(data.charAt(i++));
    h3 = b64.indexOf(data.charAt(i++));
    h4 = b64.indexOf(data.charAt(i++));

    bits = h1 << 18 | h2 << 12 | h3 << 6 | h4;

    o1 = bits >> 16 & 0xff;
    o2 = bits >> 8 & 0xff;
    o3 = bits & 0xff;

    if (h3 == 64) {
      tmp_arr[ac++] = String.fromCharCode(o1);
    } else if (h4 == 64) {
      tmp_arr[ac++] = String.fromCharCode(o1, o2);
    } else {
      tmp_arr[ac++] = String.fromCharCode(o1, o2, o3);
    }
  } while (i < data.length);

  dec = tmp_arr.join('');

  return dec;
}

(function($) {
    $.fn.extend( {
        limiter: function(limit, elem) {
            $(this).on("keyup focus", function() {
                setCount(this, elem);
            });
            function setCount(src, elem) {
                var chars = src.value.length;
                if (chars > limit) {
                    src.value = src.value.substr(0, limit);
                    chars = limit;
                }
                elem.html( limit - chars );
            }
            setCount($(this)[0], elem);
        }
    });
})(jQuery);

var isCurrency_re    = /^\s*(\+|-)?((\d+(\.\d\d)?)|(\.\d\d))\s*$/;

function isCurrency (s) {
   //return String(s).search (isCurrency_re) != -1
   return (s.match(/^\d+(?:\.\d+)?$/));
}
var isEmail_re       = /^\s*[\w\-\+_]+(\.[\w\-\+_]+)*\@[\w\-\+_]+\.[\w\-\+_]+(\.[\w\-\+_]+)*\s*$/;
function isEmail (s) {
   return String(s).search (isEmail_re) != -1;
}

function extractEmails ( text ){
    return text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
}

function extractNumber ( text ){
    return text.match(/([0-9_-]+){3}/gi);
}

/**
 *  Version 2.1
 *      -Contributors: "mindinquiring" : filter to exclude any stylesheet other than print.
 *  Tested ONLY in IE 8 and FF 3.6. No official support for other browsers, but will
 *      TRY to accomodate challenges in other browsers.
 *  Example:
 *      Print Button: <div id="print_button">Print</div>
 *      Print Area  : <div class="PrintArea"> ... html ... </div>
 *      Javascript  : <script>
 *                       $("div#print_button").click(function(){
 *                           $("div.PrintArea").printArea( [OPTIONS] );
 *                       });
 *                     </script>
 *  options are passed as json (json example: {mode: "popup", popClose: false})
 *
 *  {OPTIONS} | [type]    | (default), values      | Explanation
 *  --------- | --------- | ---------------------- | -----------
 *  @mode     | [string]  | ("iframe"),"popup"     | printable window is either iframe or browser popup
 *  @popHt    | [number]  | (500)                  | popup window height
 *  @popWd    | [number]  | (400)                  | popup window width
 *  @popX     | [number]  | (500)                  | popup window screen X position
 *  @popY     | [number]  | (500)                  | popup window screen Y position
 *  @popTitle | [string]  | ('')                   | popup window title element
 *  @popClose | [boolean] | (false),true           | popup window close after printing
 *  @strict   | [boolean] | (undefined),true,false | strict or loose(Transitional) html 4.01 document standard or undefined to not include at all (only for popup option)
 */
(function($) {
    var counter = 0;
    var modes = { iframe : "iframe", popup : "popup" };
    var defaults = { mode     : modes.iframe,
                     popHt    : 600,
                     popWd    : 950,
                     popX     : 200,
                     popY     : 200,
                     popTitle : '',
                     popClose : false };

    var settings = {};//global settings

    $.fn.printArea = function( options )
        {
            $.extend( settings, defaults, options );

            counter++;
            var idPrefix = "printArea_";
            $( "[id^=" + idPrefix + "]" ).remove();
            var ele = getFormData( $(this) );

            settings.id = idPrefix + counter;

            var writeDoc;
            var printWindow;

            switch ( settings.mode )
            {
                case modes.iframe :
                    var f = new Iframe();
                    writeDoc = f.doc;
                    printWindow = f.contentWindow || f;
                    break;
                case modes.popup :
                    printWindow = new Popup();
                    writeDoc = printWindow.doc;
            }

            writeDoc.open();
            writeDoc.write( docType() + "<html>" + getHead() + getBody(ele) + "</html>" );
            writeDoc.close();

            printWindow.focus();
            printWindow.print();

            if ( settings.mode == modes.popup && settings.popClose )
                printWindow.close();
        }

    function docType()
    {
        if ( settings.mode == modes.iframe || !settings.strict ) return "";

        var standard = settings.strict == false ? " Trasitional" : "";
        var dtd = settings.strict == false ? "loose" : "strict";

        return '<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01' + standard + '//EN" "http://www.w3.org/TR/html4/' + dtd +  '.dtd">';
    }

    function getHead()
    {
        var head = "<head><title>" + settings.popTitle + "</title>";
        $(document).find("link")
            .filter(function(){
                    return $(this).attr("rel").toLowerCase() == "stylesheet";
                })
            .filter(function(){ // this filter contributed by "mindinquiring"
                    var media = $(this).attr("media");
                    return (media.toLowerCase() == "" || media.toLowerCase() == "print")
                })
            .each(function(){
                    head += '<link type="text/css" rel="stylesheet" href="' + $(this).attr("href") + '" >';
                });
        head += "</head>";
        return head;
    }

    function getBody( printElement )
    {
        return '<body><div class="' + $(printElement).attr("class") + '">' + $(printElement).html() + '</div></body>';
    }

    function getFormData( ele )
    {
        $("input,select,textarea", ele).each(function(){
            // In cases where radio, checkboxes and select elements are selected and deselected, and the print
            // button is pressed between select/deselect, the print screen shows incorrectly selected elements.
            // To ensure that the correct inputs are selected, when eventually printed, we must inspect each dom element
            var type = $(this).attr("type");
            if ( type == "radio" || type == "checkbox" )
            {
                if ( $(this).is(":not(:checked)") ) this.removeAttribute("checked");
                else this.setAttribute( "checked", true );
            }
            else if ( type == "text" )
                this.setAttribute( "value", $(this).val() );
            else if ( type == "select-multiple" || type == "select-one" )
                $(this).find( "option" ).each( function() {
                    if ( $(this).is(":not(:selected)") ) this.removeAttribute("selected");
                    else this.setAttribute( "selected", true );
                });
            else if ( type == "textarea" )
            {
                var v = $(this).attr( "value" );
                if ($.browser.mozilla)
                {
                    if (this.firstChild) this.firstChild.textContent = v;
                    else this.textContent = v;
                }
                else this.innerHTML = v;
            }
        });
        return ele;
    }

    function Iframe()
    {
        var frameId = settings.id;
        var iframeStyle = 'border:0;position:absolute;width:0px;height:0px;left:0px;top:0px;';
        var iframe;

        try
        {
            iframe = document.createElement('iframe');
            document.body.appendChild(iframe);
            $(iframe).attr({ style: iframeStyle, id: frameId, src: "" });
            iframe.doc = null;
            iframe.doc = iframe.contentDocument ? iframe.contentDocument : ( iframe.contentWindow ? iframe.contentWindow.document : iframe.document);
        }
        catch( e ) { throw e + ". iframes may not be supported in this browser."; }

        if ( iframe.doc == null ) throw "Cannot find document.";

        return iframe;
    }

    function Popup()
    {
        var windowAttr = "location=yes,statusbar=no,directories=no,menubar=no,titlebar=no,toolbar=no,dependent=no";
        windowAttr += ",width=" + settings.popWd + ",height=" + settings.popHt;
        windowAttr += ",resizable=yes,screenX=" + settings.popX + ",screenY=" + settings.popY + ",personalbar=no,scrollbars=no";

        var newWin = window.open( "", "_blank",  windowAttr );

        newWin.doc = newWin.document;

        return newWin;
    }
})(jQuery);

(function( $ ){
    $.fn.resetValue = function() {  
        return this.each(function() {
            var $this = $(this); 
            var node = this.nodeName.toLowerCase(); 
            var type = $this.attr( "type" ); 

            if( node == "input" && ( type == "text" || type == "password" ) ){
                this.value = this.defaultValue; 
            }
            else if( node == "input" && ( type == "radio" || type == "checkbox" ) ){
                this.checked = this.defaultChecked; 
            }
            else if( node == "input" && ( type == "button" || type == "submit" || type == "reset" ) ){ 
                // we really don't care 
            }
            else if( node == "select" ){
                this.selectedIndex = $this.find( "option" ).filter( function(){
                    return this.defaultSelected == true; 
                } ).index();
            }
            else if( node == "textarea" ){
                this.value = this.defaultValue; 
            }
            // not good... unknown element, guess around
            else if( this.hasOwnProperty( "defaultValue" ) ){
                this.value = this.defaultValue; 
            }
            else{
                // panic! must be some html5 crazyness
            }
        });
    }
} )(jQuery);

/*
tinymce.init({
    selector: "textarea.tinymce_editor",
    theme: "modern",

    height: 300,
    plugins: [
         "advlist autolink link lists charmap print preview hr anchor pagebreak spellchecker",
         "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime nonbreaking",
         "save table contextmenu directionality emoticons template paste textcolor"
   ],
   content_css: "css/content.css",
   toolbar: "undo redo | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link | forecolor backcolor | removeformat code"
 }); 
 */
 
 tinymce.init({
    selector: "textarea.tinymce_editor",
    theme: "modern",
	menubar: false,
    height: 150,
   plugins: [
         "paste autolink link lists textcolor hr fullscreen code"
   ],
   style_formats: [  
        {title: 'Heading', block: 'h2'},
		{title: 'Bold', inline: 'b'},
		{title: 'Italic', inline: 'i'},
		{title: 'Superscript', inline: 'sup'},
		{title: 'Subscript', inline: 'sub'},
		/*{title: 'Center',inline:'span', classes: 'text-center'},*/
    ],
	paste_as_text: true,
   content_css: base_url+"skin_admin/global/plugins/tinymce/content.css",
   toolbar: "styleselect | bold italic underline | bullist | numlist | link | code",
 });

 
 tinymce.init({
    selector: "textarea.tinymce_editor_basic",
    theme: "modern",
	menubar: false,
    height: 300,
	plugins: [
         "paste autolink link lists textcolor hr fullscreen code"
   ],
   paste_as_text: true,
   toolbar: "bold italic underline | code"
 });



function UIIdleTimeout(URL) {

    // cache a reference to the countdown element so we don't have to query the DOM for it on each ping.
    var $countdown;

    $('body').append('<div class="modal fade" id="idle-timeout-dialog" data-backdrop="static"><div class="modal-dialog modal-small"><div class="modal-content"><div class="modal-header"><h4 class="modal-title">Your session is about to expire.</h4></div><div class="modal-body"><p><i class="fa fa-warning"></i> You session will be locked in <span id="idle-timeout-counter"></span> seconds.</p><p>Do you want to continue your session?</p></div><div class="modal-footer"><button id="idle-timeout-dialog-logout" type="button" class="btn btn-default">No, Reload</button><button id="idle-timeout-dialog-keepalive" type="button" class="btn btn-primary" data-dismiss="modal">Yes, Keep Working</button></div></div></div></div>');

    // start the idle timer plugin
    $.idleTimeout('#idle-timeout-dialog', '.modal-content button:last', {
        idleAfter: 30, // 30 seconds
        timeout: 30000, //30 seconds to timeout
        pollingInterval: 5, // 5 seconds
        keepAliveURL: URL,
        serverResponseEquals: 'OK',
        onTimeout: function(){
            window.location = URL;
        },
        onIdle: function(){
            $('#idle-timeout-dialog').modal('show');
            $countdown = $('#idle-timeout-counter');

            $('#idle-timeout-dialog-keepalive').on('click', function () {
                $('#idle-timeout-dialog').modal('hide');
            });

            $('#idle-timeout-dialog-logout').on('click', function () {
                //$('#idle-timeout-dialog').modal('hide');
                //$.idleTimeout.options.onTimeout.call(this);
                window.location = base_url+"/ticket_reservation";
            });
        },
        onCountdown: function(counter){
            $countdown.html(counter); // update the counter
        }
    });

}


function reservation_block_table()
{



    if(is_block_load == false)
    {
        /*var config = $(".tbl-blocks")[ 0 ].config,
            $cell = $( 'td.random' ), // jQuery selector or DOM element of newly updated cell
           // applies or reapplies a sort to the table; use false to not update the sort
            resort = true, // or [ [0,0], [1,0] ] etc
            callback = function( table ) {
                // do something
            };
        $.tablesorter.updateCell( config, $cell, resort, callback );

        $(".tbl-blocks").trigger( 'updateCell', [ this, resort, callback ] );
        var sorting = [ [ 0 ] ];
        $(".tbl-blocks").trigger( 'sorton', [ sorting ] );*/

        $(".tbl-blocks").trigger('resetToLoadState');

        is_block_load = true;
    }




}



/*
 * increment letters
 * */
function nextChar(c) {
    var u = c.toUpperCase();
    if (same(u,'Z')){
        var txt = '';
        var i = u.length;
        while (i--) {
            txt += 'A';
        }
        return (txt+'A');
    } else {
        var p = "";
        var q = "";
        if(u.length > 1){
            p = u.substring(0, u.length - 1);
            q = String.fromCharCode(p.slice(-1).charCodeAt(0));
        }
        var l = u.slice(-1).charCodeAt(0);
        var z = nextLetter(l);
        if(z==='A'){
            return p.slice(0,-1) + nextLetter(q.slice(-1).charCodeAt(0)) + z;
        } else {
            return p + z;
        }
    }
}

function nextLetter(l){
    if(l<90){
        return String.fromCharCode(l + 1);
    }
    else{
        return 'A';
    }
}

function same(str,char){
    var i = str.length;
    while (i--) {
        if (str[i]!==char){
            return false;
        }
    }
    return true;
}


/*
 * end increment letters
 * */