<!-- BEGIN CONTAINER -->

<style>
    @media print{
        .no-print{
            display: none;
        }
        .print-only{
            display: block;
        }
    }

    @media screen {
        .print-only {
            display: none;
        }
    }
</style>

<?php



$movie_selected    = "All";
$theater_selected  = "All";
$suppler_selected  = "ALL";

$grand_total_partron = 0;
$grand_total_dr      = 0;
$grand_total_ticket  = 0;
$grand_total_levy    = 0;
$grand_total_ent     = 0;

$print_result = [];
?>

<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title"><?php echo $title ?></h3>
            <?php if($this->adminauth->has_role('1104')):?>

            <div class="row no-print">
                <div class="col-md-12">

                    <form class="form-horizontal" method="POST" action="<?php echo current_url()?>">

                        <div class="portlet box blue portlet-filter">
                            <div class="portlet-title">
                                <div class="caption">Filter</div>
                                <div class="tools">
                                    <button type="submit" class="btn btn-danger btn-sm">Show Report</button>
                                </div>
                            </div>
                            <div class="portlet-body ">
                                <div class="form-body clearfix" style="max-width:500px;">



                                    <div class="form-group">
                                        <div class="col-md-3"><label class="control-label" for="sales_report_period_type">Suppliers</label></div>
                                        <div class="col-md-9" id="match-div">
                                            <select class="form-control input-sm" name="supplier">
                                                <option value="-1">any</option>
                                                <?php foreach ($suppliers as $th): ?>
                                                    <option value="<?php echo $th->id?>" <?php if($supplier == $th->id){ $suppler_selected =$th->supplier_name;echo 'selected';}else{echo '';}  ?> >
                                                        <?php echo $th->supplier_name; ?>
                                                    </option>
                                                <?php endforeach;?>
                                            </select>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-3"><label class="control-label" for="report_from">From <span class="required">*</span></label></div>
                                        <div class="col-md-9"><input id="report_from" type="text" class="form-control form-control-inline input-medium date-picker input-sm" name="from" placeholder="From" value="<?php echo $from?>"></div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-3"><label class="control-label" for="report_to">To <span class="required">*</span></label></div>
                                        <div class="col-md-9"><input id="report_to" type="text" class="form-control form-control-inline input-medium date-picker  input-sm" name="to" placeholder="To" value="<?php echo $to ?>"></div>
                                    </div>


                                </div>
                            </div>

                        </div>
                    </form>

                    <div class="portlet light bordered">
                        <div class="dataTables_wrapper no-footer">
                            <div class="row">
                                <div class="col-md-12">
                                    <button id="exportCVS1910" type="button" class="btn btn-default pull-right">Export to Excel</button>
                                    <button type="button" class="btn btn-default pull-right" onclick="window.print()">Print</button>
                                </div>
                            </div>
                            <div class="table-container">
                                <?php $tot_tax_amount = 0; ?>

                                <div class="clearfix"></div>
                                <div class="table-scrollable">
                                    <table id="tbl-report" class="table table-striped table-bordered table-hover dataTable no-footer dtr-inline">
                                        <thead>
                                        <tr class="headings">
                                            <th>SCREEN</th>
                                            <th>MOVIE NAME</th>
                                            <th>TOTAL PATRONS</th>
                                            <th>TICKET TOTAL</th>
                                            <th>SUPPLIER NAME</th>
                                            <th>SUPLIER SHARE</th>
                                        </tr>
                                        </thead>
                                        <tbody>

                                        <?php foreach($results as $result):?>

                                            <?php
                                            $entertainment     = ($result->film_levy * $result->entartainment_tax)/110;

                                            $theater_share     = ($result->dr_collection * $result->movie_hall_rate)/100;
                                            $distributor_comm  = ($result->dr_collection * $result->distributor_rate)/100;
                                            $supplier_share    = ($result->dr_collection * $result->supplier_rate)/100;
                                            $film_levy_3_4     = ($result->total_patrons * 3.4);
                                            $ent_tax           = ($result->dr_collection * $result->supplier_rate)/100;


                                            //$total_ticket = $result->total_ticket_count;

                                            if($result->payment_method_id < 2){
                                                $film_levy_3_4     = 0;

                                            }


                                            $theater_share      < 0 ? $theater_share      = 0 : $theater_share;
                                            $distributor_comm   < 0 ? $distributor_comm   = 0 : $distributor_comm;
                                            $supplier_share     < 0 ? $supplier_share     = 0 : $supplier_share;
                                            $entertainment      < 0 ? $result->entartain      = 0 : $result->entartain =$entertainment;

                                            ?>
                                            <tr>
                                                <td><?php echo $result->theater_name; ?></td>
                                                <td><?php echo $result->movie_name; ?></td>
                                                <td><?php echo $result->total_patrons; ?></td>
                                                <td><?php echo number_format($result->ticket_total,2); ?></td>

                                                <td><?php echo $result->suplier_name; ?></td>

                                                <td><?php echo number_format($supplier_share,2); ?></td>

                                            </tr>
                                            <?php
                                        endforeach;
                                        ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div><!-- End Table Container-->
                        </div>
                    </div>
                </div>
            </div>


            <div class="container print-only">
                <div class="row">
                    <p>Supplier : <b><?php echo $suppler_selected;?></b></p>
                    <p>Date: <b><?php echo $from." to ".$to ?></b> </p>
                </div>
                <?php foreach($results as $result):?>
                    <?php

                    if (isset( $print_result[$result->supplier_id])){

                        array_push($print_result[$result->supplier_id]['data'],[
                            'movie'         => $result->movie_name,
                            'patrons'       => $result->total_patrons,
                            'total_ticket'  => $result->ticket_total,
                            'dr_collection' => $result->dr_collection,
                            'levy'          => $result->film_levy,
                            'ent_tax'       => $result->entartain
                        ]);
                    }else{
                        $print_result[$result->supplier_id] =['supplier'=>$result->suplier_name,'data'=> [[
                            'movie'          => $result->movie_name,
                            'patrons'        => $result->total_patrons,
                            'total_ticket'   =>  $result->ticket_total,
                            'dr_collection'  => $result->dr_collection,
                            'levy'           => $result->film_levy,
                            'ent_tax'        => $result->entartain
                        ]
                        ]];
                    }
                    ?>
                <?php endforeach; ?>
                <br/>
                <div class="row">
                    <div class="col-xs-3"><b>Supplier Name</b></div>

                    <div class="col-xs-3"><b>Movie</b></div>
                    <div class="col-xs-3 text-right"><b>DR Collection</b></div>

                </div>
                <hr/>
                <div class="row">
                    <?php foreach($print_result as $result){
                        $total_patrons = 0;
                        $total_ticket  = 0;
                        $total_dr      = 0;
                        $total_levy    = 0;
                        $total_ent     = 0;
                        ?>
                        <div class="row">
                            <div class="col-xs-3"><?php echo $result['supplier'];?></div>
                            <div class="col-xs-3"></div>
                            <div class="col-xs-3"></div>
                        </div>

                        <?php foreach ($result['data'] as $value){
                            $total_patrons += $value['patrons'];
                            $total_ticket  += $value['total_ticket'];
                            $total_dr      += $value['dr_collection'];
                            $total_levy    += $value['levy'];
                            $total_ent     += $value['ent_tax'];
                            ?>
                            <div class="row">
                                <div class="col-xs-3"></div>
                                <div class="col-xs-3"><?php echo $value['movie'];?></div>
                                <div class="col-xs-3 text-right"><?php echo number_format($value['dr_collection'],2);?></div>


                            </div>
                        <?php }
                        $grand_total_partron += $total_patrons;
                        $grand_total_dr      += $total_dr;
                        $grand_total_ticket  += $total_ticket;
                        $grand_total_levy    += $total_levy;
                        $grand_total_ent     += $total_ent;
                        ?>
                        <hr/>
                        <div class="row">
                            <div class="col-xs-3"></div>
                            <div class="col-xs-3"><b>Total</b></div>
                            <div class="col-xs-3 text-right"><b><?php echo number_format($total_dr,2);?></b></div>

                        </div>
                        <hr/>
                    <?php } ?>
                    <div class="row">
                        <div class="col-xs-3"></div>
                        <div class="col-xs-3"><b>Grand Total</b></div>
                        <div class="col-xs-3 text-right"><b><?php echo number_format($grand_total_dr,2);?></b></div>
                    </div>
                    <hr/>
                </div>
            </div>
            <script>
                var el = $(".portlet-body");

                $('#tournament').change(function(){

                    Metronic.blockUI({
                        target: el,
                        animate: true,
                        overlayColor: 'none'
                    });
                    var tournament_id = $(this).val();

                    $.post('<?php echo base_url('reports/municipal_council/ajax_get_matches') ?>',{tournament_id:tournament_id},function(html)
                    {
                        $('#match-div').html(html);
                        Metronic.unblockUI(el);
                    });


                });


                $('.required-entry').attr('required', 'required');
                $('#edit_form').validate({
                    errorElement: 'span',
                    errorClass: 'validation-advice',
                    ignore: "",
                    submitHandler: function (form) {
                        form.submit();
                    }
                });


                $('.date-picker').datepicker({
                    format: "yyyy-mm-dd",
                    autoclose: true
                });


            </script>
        </div>

        <script>
            jQuery(document).ready(function() {
                TableAjax.init();
            });
        </script>

        <?php endif; ?>

        <!-- END PAGE CONTENT-->
        <!-- END CONTENT -->

    </div>
    <!-- END CONTAINER -->
    <script>
        window.param ={

            'supplier' : '<?php echo $supplier; ?>',
            'from' :'<?php echo $from; ?>',
            'to' :'<?php echo $to; ?>'
        };
        (function (window,$) {
            $('#exportCVS1910').click(function(){
                var urlenc = [];

                if (window.param.supplier.trim() != ''){
                    urlenc.push('suppplier='+window.param.supplier);
                }
                if (window.param.from.trim() != ''){
                    urlenc.push('from='+window.param.from);
                }
                if (window.param.to.trim() != ''){
                    urlenc.push('to='+window.param.to);
                }


                var myWindow = window.open(window.base_url+'admin/reports/suplier-report/export?'+urlenc.join('&'));
            });
        }(window,$))
    </script>