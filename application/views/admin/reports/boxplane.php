<style>
    .availability-tbl-holder {
        width: 100%;
    }

    .small-font, .small-font label, .small-font .input-group-addon, .table.small-font thead tr th, .table.small-font thead tr td {
        font-size: 11px;
    }

    #frm_2 .select2-container{
        width: 100% !important;
    }


    .small-font .input-sm {
        height: auto;
        padding: 3px 5px;
        font-size: 12px;
    }

    span.validation-advice {
        display: none !important;
    }

    .t-b-n {
        border-top: 1px solid #fff !important;
        border-right: 1px solid #fff !important;
        border-bottom: 1px solid #fff !important;
    }

    .btnCircle {
        color: #333333;
        background-color: #E5E5E5;
        height: 24px;
        width: 24px;
        text-align: center;
        padding: 3px 9px;
        border-radius: 25px !important;
        font-weight: bold;
        display: inline-block;
        line-height: 18px;
    }

    .addBtnRemove {
        margin-top: 2px;
        color: #fff;
        background-color: #f3565d;
    }

    .seatSelectionWrap .block-plan-wrapper {
        width: 100%;
        overflow: auto;
    }

    .seatSelectionWrap .block-plan-container {
        position: relative;
        padding: 20px;
        border: 1px solid #d5d5d5;
    }

    .seatSelectionWrap #item-container {
        position: relative;
        margin: auto;
    }

    .seatSelectionWrap .block-plan-wrapper .selection {
        position: absolute;
        background: #BCE;
        background-color: #BEC;
        border-color: #8B9;
    }

    .legendsWrap .legends {
        margin: 0;
        padding: 0 0 10px 0;
    }

    .legendsWrap .legends li .icon {
        width: 10px;
        height: 10px;
        display: inline-block;
        vertical-align: middle;
        margin-right: 10px;
    }

    .seatSelectionWrap .seat_item {
        position: absolute;
        height: 24px;
        width: 24px;
        top: 0px;
        font-size: 8px;
        color: #fff;
        text-align: center;
        vertical-align: middle;
        line-height: 24px;
        border-radius: 5px !important;
    }

    .seatSelectionWrap .seat_item {
        background: #00abbd;
        cursor: pointer;
        /*
        background-image: url("../../images/seat-icon.png") !important;
        background-position: 50% 50% !important;
        background-size: cover !important;
        */
    }

    .legendsWrap .legends li {
        margin-right: 20px;
        display: inline-block;
    }

    .legendsWrap .legends li.selected .icon,
    .seatSelectionWrap .selected_seat {
        background: #84c94c;
    }

    .legendsWrap .legends li .text {
        display: inline-block;
        vertical-align: middle;
        font-size: 11px;
    }

    .legendsWrap .legends li.available .icon {
        background: #00abbd;
    }

    .legendsWrap .legends li.reserved .icon,
    .seatSelectionWrap .booked_seat {
        background: #ee0c6e;
    }

    .legendsWrap .legends li.unavailable .icon,
    .seatSelectionWrap .hide_seat,
    .seatSelectionWrap .hold_seat {
        background: #eeeded;
        color: #000;
    }

    .seatSelectionWrap .booked_seat,
    .seatSelectionWrap .hold_seat {
        cursor: default;
    }


    /*#selected_seats b{font-size: 26px;}*/

</style>
<script>
    window.moviedata      = <?php echo json_encode($theater_list); ?>;
    window.paymentmethods = <?php echo json_encode($paymentmethods); ?>;
</script>


<script src="<?php echo base_url(); ?>angular/moment.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/angular-moment.min.js" type="text/javascript"></script>




<script src="<?php echo base_url(); ?>angular/admin/reports/boxplane/services/counterservice.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/admin/reports/boxplane/controller/mainController.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/admin/reports/boxplane/controller/filterController.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/admin/reports/boxplane/controller/seatPlanController.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/admin/reports/boxplane/directive/directive.js" type="text/javascript"></script>

<div class="page-container">
<?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>
    <div class="page-content-wrapper">
        <div class="page-content">
            <h3 class="page-title"><?php echo $title ?></h3>

            <div class="row">
                <div class="col-md-12">

                    <!-- BEGIN VALIDATION STATES-->
                    <div class="portlet light bordered" ng-controller="mainController">
                        <div class="portlet-body form">
                            <div class="form-body">
                                <div class="row">
                                    <form id="frm_1" class="form-horizontal">

                                        <filter-option>

                                        </filter-option>




                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- END VALIDATION STATES-->
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>
