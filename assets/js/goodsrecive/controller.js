/**
 * Created by LAYOUTindex on 11/23/2017.
 */

(function(window) {
    var app = angular.module('restaurant');

    app.controller('goodsreciveController', goodsreciveController);

    goodsreciveController.$inject = ['$scope','$window','purchaseorderService'];

    function goodsreciveController($scope,$window,purchaseorderService) {
        //console.log($window.purchase_invoices);


        $scope.purchase_invoices = purchaseorderService.getPurchaseinvoices();


        $scope.selected_invoice = purchaseorderService.selectedOrder($scope.purchase_invoices[0]);


        $scope.deliverdate = null;

        if ($window.goodReciveNote != null){
            $('.datepicker').datepicker("update", $window.goodReciveNote.goods_recive_data);
            $scope.deliverdate = $window.goodReciveNote.goods_recive_data;
        }
        // $scope.onSelectOrder = function () {
        //
        //     $scope.selected_invoice = purchaseorderService.selectedOrder($scope.selected_invoice);
        //     //console.log($scope.selected_invoice.items)
        // };

        $scope.onReturnall = function () {

            if (this.orderitem.ischeckd){
                this.orderitem.reciveqty = 0;
            }else{
                this.orderitem.reciveqty = this.orderitem.qty_in_hand;
            }

        };
    }
}(window));