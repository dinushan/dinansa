

<div class="page-container">

    <?php $this->load->view('admin/inc_sidebar_navigation.php');?> 

    <div class="page-content-wrapper">
		<div class="page-content">
			<?php echo form_open('admin/plane/store',['class'=>'form']); ?>
				<div class="panel panel-default form-horizontal">
					<div class="panel-body">

						<div class="form-group">
							<label class="control-label col-md-3" for="name">Name<span class="req">*</span></label>
							<div class="control-element col-md-4">
								<p class="text-danger" ></p>
								<input type="text" class="form-control" name="name" value="" required>
							</div>    
						</div>	
						
						<div class="form-group">
							<label class="control-label col-md-3" for="name">Description<span class="req">*</span></label>
							<div class="control-element col-md-4">
								<p class="text-danger" ></p>
								<textarea class="form-control" name="description"></textarea>
							</div>    
						</div>	

						<div class="form-group">
							<label class="control-label col-md-3" for="name">Theaters<span class="req">*</span></label>
							<div class="control-element col-md-4">
								<p class="text-danger" ></p>
								<select class="form-control" name="theater">
									<?php foreach ($theaters as $theater) { ?>
									  <option value="<?php echo $theater->id ?>"><?php echo $theater->name ?></option>
									<?php } ?>
								</select>
							</div>
						</div>						

					</div>
					<div class="panal-footer clearfix">
						<div class="col-md-offset-3 col-md-9">
							<button type="submit" class="btn btn-primary">Save</button>
						</div>    
					</div>


				</div>
			</form>
		</div>
	</div>
</div>