<script>
    <?php if (!$edit): ?>
    window.movies  = <?php echo json_encode($movies) ?>;
    window.theater = <?php echo json_encode($theater) ?>;
    <?php endif; ?>
</script>
<script type = "text/javascript" src="<?php echo base_url() ?>skin_admin/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>


<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php');?>
    <div class="page-content-wrapper">
        <div class="page-content">
            <?php if($edit):?>
            <h3 class="page-title"><?php echo $theater['name'] ?> : Edit Movie</h3>
             <?php else: ?>
             <h3 class="page-title"><?php echo $theater['name'] ?> : Add Movie</h3>
            <?php endif; ?>
            <div class="page-bar">
                <div class="page-toolbar">
                    <?php if (!$edit): ?>
                    <div class="btn-group pull-right">
                        <a onclick="setLocation('<?php echo site_url('admin/ticket_price') ?>')" class="btn default"><i class="fa fa-chevron-left"></i> Back</a>
                    </div>
                    <?php else: ?>
                        <div class="btn-group pull-right">
                            <a onclick="setLocation('<?php echo site_url('admin/theater/price/movie/').$theater['id'] ?>')" class="btn default"><i class="fa fa-chevron-left"></i> Back</a>
                        </div>
                    <?php endif; ?>
                </div>
            </div><!-- End Page bar -->
            <?php echo get_message() ?>
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption">
                        <i class="fa fa-cogs"></i>
                        Movies
                    </div>
                </div>
                <div class="portlet-body form-horizontal">

                    <?php if ($edit): ?>
                        <?php if (strtotime($priceHeader->booking_start_date) <= strtotime(date('Y-m-d'))):?>
                            <div class="form-body bg-red-pink">
                                <br/>
                                <h4 class="text-center">Booking Start date is past ! Changes are not applyed to the early shedules and alredy booked users.</h4>
                                <br/>
                            </div>
                            <br/>
                        <?php endif; ?>
                    <?php endif; ?>

                    <div class="form-body">

                        <?php if (!$edit): ?>
                            <?php echo form_open('admin/theater/price/assign');?>
                                <input type="hidden" name="theater" value="<?php echo $theater['id']?>" />
                        <?php else: ?>
                            <?php echo form_open('admin/theater/price/update');?>
                            <input type="hidden" value="<?php echo $this->encrypt_lib->encode($priceHeader->id); ?>" name="priceheader_id" />
                        <?php endif; ?>

                        <div class="form-group">
                            <label class="control-label col-md-3">Movie</label>
                            <div class="mt-radio-list control-element col-md-4">
                                <select class="form-control cb_s2" name="movie" id="movieselector">

                                    <?php if (!$edit): ?>
                                        <?php foreach($movies as $movie): ?>
                                            <option value="<?php echo $movie->id; ?>"><?php echo $movie->movie_name; ?></option>
                                        <?php endforeach; ?>

                                        <?php else: ?>
                                            <?php foreach($movies as $movie): ?>
                                                <option value="<?php echo $movie->id; ?>" <?php echo ($movie->id== $priceHeader->movie_id)? 'selected="selected"' : '' ?>> <?php echo $movie->movie_name; ?> </option>
                                            <?php endforeach; ?>
                                        <?php endif; ?>

                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Booking Start</label>
                            <div class="control-element col-md-4">
                                <div class="input-group input-medium date" data-date-format="dd-mm-yyyy">
                                    <input id="startdate" type="text" value="<?php echo $edit ? $priceHeader->booking_start_date :(isset($movies[0]->date_release) ?$movies[0]->date_release :null) ; ?>" name="bookingstartdate"  class="form-control date-picker" readonly=""/>
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                           <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">Release Date</label>
                            <div class="control-element col-md-4">
                                <div class="input-group input-medium date" data-date-format="dd-mm-yyyy">
                                    <input id="releasedate" type="text" name="releasedate" value="<?php echo $edit? $priceHeader->start_date :(isset($movies[0]->date_release) ? $movies[0]->date_release:null) ; ?>"  class="form-control date-picker" readonly=""/>
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                           <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>

                            </div>
                        </div>

                        <div class="form-group">
                            <label class="control-label col-md-3">End Date</label>
                            <div class="control-element col-md-4">
                                <div class="input-group input-medium date" data-date-format="dd-mm-yyyy">
                                    <input id="enddate" type="text" name="enddate"  value="<?php echo $edit? $priceHeader->end_date:(isset($movies[0]->date_end) ? $movies[0]->date_end :null);?>"  class="form-control date-picker" readonly=""/>
                                    <span class="input-group-btn">
                                        <button class="btn default" type="button">
                                           <i class="fa fa-calendar"></i>
                                        </button>
                                    </span>
                                </div>

                            </div>
                        </div>



                            <div class="form-group">
                                <label class="control-label col-md-3">Film Levy</label>
                                <div class="control-element col-md-4">
                                    <input id="film_levy" type="text" name="film_levy"  value="<?php echo $coperateTax->film_levy?>"  class="form-control"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Film Coperation Tax</label>
                                <div class="control-element col-md-4">
                                    <input id="film_coperation_tax" type="text" name="film_coperation_tax"  value="<?php echo $coperateTax->film_coperation_tax?>"  class="form-control"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Entertainment Tax (%)</label>
                                <div class="control-element col-md-4">
                                    <input id="corporation_tax" type="text" name="corporation_tax"  value="<?php echo $coperateTax->corporation_tax?>"  class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Movie Hall Rate (%)</label>
                                <div class="control-element col-md-4">
                                    <input id="movie_hall_rate" type="text" name="movie_hall_rate"  value="<?php echo $coperateTax->movie_hall_rate?>"  class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Distributor Rate (%)</label>
                                <div class="control-element col-md-4">
                                    <input id="distributor_rate" type="text" name="distributor_rate"  value="<?php echo $coperateTax->distributor_rate?>"  class="form-control"/>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="control-label col-md-3">Supplier Rate (%)</label>
                                <div class="control-element col-md-4">
                                    <input id="supplier_rate" type="text" name="supplier_rate"  value="<?php echo $coperateTax->supplier_rate?>"  class="form-control date-picker" readonly=""/>
                                </div>
                            </div>



                        <div class="form-actions">
                            <div class="row">
                                <div class="col-md-offset-3 col-md-9">
                                    <div class="btn-group">
                                        <button type="submit" data-id="1" class="btn btn-submit green">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        </form>
                    </div>

                </div>

            </div>
            <?php if (!$edit):?>
            <div class="row">
                <div class="portlet box green">
                    <div class="portlet-title"></div>
                    <div class="portlet-body">
                        <div class="table-scrollable">
                            <table class="table table-striped table-hover">
                                <thead>
                                <tr>
                                    <th>Movie</th>
                                    <th>Booking Start</th>
                                    <th>Movie Release</th>
                                    <th>Movie End</th>
                                    <!--th>Pending Schedules</th-->
                                    <th>Online Seats</th>
                                </tr>
                                </thead>
                                <tbody>

                                <?php foreach($priceHeaders as $priceHeader): ?>
                                    <tr>
                                        <td><?php echo $priceHeader->movie_name?></td>
                                        <td><?php echo $priceHeader->booking_start_date?></td>
                                        <td><?php echo $priceHeader->start_date?></td>
                                        <td><?php echo $priceHeader->end_date ?></td>
                                        <!--td><!--?php echo $priceHeader->status == 0 ? 'Pending' :'Confirmed' ?--></td-->
                                        <td><?php echo $priceHeader->online_booking ?></td>
                                        <td>
                                            <?php if($priceHeader->status == 0): ?>
                                            <a href="<?php echo base_url() ?>admin/theater/price/create/<?php echo $this->encrypt_lib->encode($priceHeader->id); ?>" class="btn btn-sm default btn-xs"">
                                                <i class="fa fa-film"></i>
                                                Manage Schedule
                                            </a>
                                            <a href="<?php echo base_url() ?>admin/theater/price/edit/<?php echo $this->encrypt_lib->encode($priceHeader->id); ?>" class="btn btn-sm default btn-xs"">
                                                <i class="fa fa-edit"></i>
                                                Edit
                                            </a>
                                            <?php else: ?>
                                                <a href="<?php echo base_url() ?>admin/theater/price/create/<?php echo $this->encrypt_lib->encode($priceHeader->id); ?>" class="btn btn-sm default btn-xs"">
                                                <i class="fa fa-film"></i>
                                                   View Schedule
                                                </a>
                                            <?php endif; ?>

                                        </td>
                                    </tr>
                                <?php endforeach; ?>

                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
            <?php endif;?>
        </div>
    </div>

</div>

<script>
    (function ($) {
        $('.date-picker').datepicker({
            format:'yyyy-mm-dd'
        });


        $('#movieselector').change(function(){
            var id = $(this).val();

            for (var i in window.movies){

                if (window.movies[i].id == id){
                    $('#startdate').datepicker("update",window.movies[i].date_release);
                    $('#releasedate').datepicker("update",window.movies[i].date_release);
                    $('#enddate').datepicker("update",window.movies[i].date_end);
                }
            }

        })
    }($))
</script>