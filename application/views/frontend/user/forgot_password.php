<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer">
	<div class="row">
		<div class="col-xs-12 breadcrumbCont">

            <div class="breadcrumbWrap">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                    <li><a href="<?php echo base_url('user/forgot_password'); ?>">Forgot Password</a></li>
                </ol>
            </div>

		</div>
		<div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 forgotPwdCont">
            <div class="forgotPwdWrap">
                <div class="headingWrap">
                    <h2 class="heading">Forgot your password?</h2>
                    <div class="forgotPwdDesc">
                        <p>No problem. Just enter your mobile number below — we’ll send you a verification code to reset it.</p>
                    </div>
                </div>



                <div class="formWrap">

                    <div><?php echo get_message(); ?></div>
                    <?php
                    $attributes = array('id' => 'frm-forgotpassword', 'class'=>'form-type-1');
                    echo form_open(current_url(), $attributes);
                    ?>
                    <div class="form-group">
                        <input type="text" class="form-control input-scope" id="phone" name="phone" autocomplete="off" placeholder="07X XXXXXXX" value="<?php echo set_value('phone') ?>">
                        <?php echo form_error('phone'); ?>
                    </div>
                    <div class="form-group">
                        <input type="hidden" name="action" value="forgotPassword" />
                        <button type="submit" class="btn btn-scope btn-blue">SUBMIT</button>
                    </div>

                    </form>

                </div>
            </div>
		</div>
	</div>
</div>
<script>
    $('#phone').keyup(function () {
        var elem = $('#phone');
        var ln =elem.val().length;

        if (ln > 0){


            if ( elem.val().slice(0, 1) != 0){

                var starr = elem.val().split("");

                starr.unshift(0);
                elem.val(starr.join(''));
            }
        }


        if (ln > 10){
            var st =elem.val().slice(0, 10);
            elem.val(st);
            return
        }
    });
</script>
<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>