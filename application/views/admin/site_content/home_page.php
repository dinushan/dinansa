<!-- BEGIN CONTAINER -->

<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			
			<h3 class="page-title">Home Page</h3>
			<div class="page-bar">
				<div class="page-toolbar">
					<div class="btn-group pull-right"> <a class="btn default" href="<?php echo site_url('admin/site_content/pages/index/') ?>"><i class="fa fa-chevron-left"></i> Back to Main Pages</a> </div>
					<a onclick="setLocation('<?php echo site_url('admin/'.$slug.'/pages/home_banner/') ?>')" class="btn green"><i class="fa fa-plus-circle"></i> Add Banner Images (<?php echo $this->query->count_all_results('site_page_banner_images',array('pg_id'=>-1)) ?>)</a>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12">
					<?php echo get_message() ?> 
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet light bordered"> 
						<!--<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-pencil"></i>Settings
							</div>							
						</div>-->
						<div class="portlet-body form"> 
							<!-- BEGIN FORM-->
							<?php                    
							$attr = array("id"=>"edit_form","class"=>"form-horizontal","enctype"=>"multipart/form-data");
							echo form_open(current_url(),$attr);
							?>
							<?php
								$menu = $siteData->menu;	
								$title = $siteData->title;
								$title1 = $siteData->title1;
								$description = $siteData->description;
								
								$meta_title = $siteData->meta_title;	
								$meta_keywords = $siteData->meta_keywords;	
								$meta_description = $siteData->meta_description;	
								$og_type = $siteData->og_type;	
								$og_title = $siteData->og_title;	
								$og_description = $siteData->og_description;
								$custom_header_script   = $siteData->custom_header_script;
								
							?>
							<h4 class="form-section">General</h4>
														
							<div class="form-group">
								<label class="control-label col-md-3">Menu<span class="req">*</span></label>
								<div class="control-element col-md-4"><input name="menu" type="text" class="form-control required-entry" value="<?php echo $page_action == "add" ? 
							set_value('menu') : 
							set_value('menu',$menu) ?>" size="15" />
									<?php echo form_error('menu', '<br /><div class="validation-advice">', '</div>'); ?></div>
							</div>											
							
							
							<div class="form-group">
							  <label class="control-label col-md-3">Page Heading<span class="req">*</span></label>
							  <div class="control-element col-md-4"><input name="title"
										type="text" class="form-control required-entry"
										value="<?php echo $page_action == "add" ?
								set_value('title') :
								set_value('title',$title) ?>" size="15" />
								<?php echo form_error('title'); ?></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Title 1<span class="req">*</span></label>
								<div class="control-element col-md-4"><input name="title1" type="text" class="form-control required-entry" value="<?php echo $page_action == "add" ?
										set_value('title1') :
										set_value('title1',$title1) ?>" size="15" />
									<?php echo form_error('title1'); ?></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Description</label>
								<div class="control-element col-md-4">
									<textarea cols="15" rows="3" class="form-control" name="description"><?php echo $page_action == "add" ?
											set_value('description') :
											set_value('description',$description) ?></textarea>
								</div>
							</div>
							
							
							<h4 class="form-section">Meta Settings</h4>
							<div class="form-group">
								<label class="control-label col-md-3">Meta Title</label>
								<div class="control-element col-md-4">
									<input type="text" class="form-control" value="<?php echo 
								$page_action == "add" ? 
								set_value('meta_title') : 
								set_value('meta_title',$meta_title) ?>" 
								name="meta_title" />
								</div>
							</div>
							
							<?php if(SHOW_META_CONTENT === TRUE):?>							
							<div class="form-group">
								<label class="control-label col-md-3">Meta Keywords</label>
								<div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="meta_keywords"><?php echo $page_action == "add" ? 
								set_value('meta_keywords') : 
								set_value('meta_keywords',$meta_keywords) ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Meta Description</label>
								<div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="meta_description"><?php echo $page_action == "add" ? 
								set_value('meta_description') : 
								set_value('meta_description',$meta_description) ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">OG Type</label>
								<div class="control-element col-md-4">
									<input type="text" class="form-control" value="<?php echo 
								$page_action == "add" ? 
								set_value('og_type') : 
								set_value('og_type',$og_type) ?>" 
								name="og_type" />
									<span class="help-block font-green">The type of the object, such as 'article'</span></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">OG Title</label>
								<div class="control-element col-md-4">
									<input type="text" class="form-control" value="<?php echo 
								$page_action == "add" ? 
								set_value('og_title') : 
								set_value('og_title',$og_title) ?>" 
								name="og_title" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">OG Description</label>
								<div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="og_description"><?php echo $page_action == "add" ? 
								set_value('og_description') : 
								set_value('og_description',$og_description) ?></textarea>
								</div>
							</div>
							<?php endif; ?>
							<div class="form-group">
								<label class="col-md-3 control-label">Custom Header Script(SEO)</label>
								<div class="col-md-4 col-xs-12">
                                               <textarea cols="15" rows="2" class="form-control" name="custom_header_script"><?php echo $page_action == "add" ?
													   set_value('custom_header_script') :
													   set_value('custom_header_script',$custom_header_script) ?></textarea>
								</div>
							</div>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
									</div>
								</div>
							</div>
							<script type="text/javascript">							 
								$('.required-entry').attr('required','required');					
								$('#edit_form').validate({
									errorElement: 'span', 
									errorClass: 'validation-advice', 								
									ignore: "",
									submitHandler: function (form) {
										form.submit();										
									}
								});									
							</script>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
		<!-- END PAGE CONTENT--> 
	</div>
	<!-- END CONTENT --> 
	
</div>
<!-- END CONTAINER --> 



