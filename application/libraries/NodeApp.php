<?php

/**
 * Class NodeApp
 * Communicate with node app
 */

class NodeApp
{
    protected $client;
    public $baseUrl;

    public function __construct()
    {
         //$this->baseUrl = 'http://localhost:3000/';
        $this->baseUrl = 'http://elevate-dev.appslanka.com:3010/';
    }

    public function sendRequest($url, $body)
    {

        $curl = curl_init();

        curl_setopt_array($curl, array(
            CURLOPT_URL => $this->baseUrl . $url,
            CURLOPT_RETURNTRANSFER => true,
            // CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 60,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => json_encode($body),
            CURLOPT_HTTPHEADER => array(
                "cache-control: no-cache",
                "content-type: application/json"
            )
        ));

        $response = curl_exec($curl);
       // $info = curl_getinfo($ch);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return false;
        }
        return false;

    }


    public function fireBase($url, $body)
    {
        $client = new Client([
            // Base URI is used with relative requests
            'base_uri' => 'https://fcm.googleapis.com/fcm/',
            // You can set any number of default request options.
            'timeout' => 60.0,
        ]);

        $access_token = 'key=' . env('FCM_SERVER_KEY');
        $header = [
            'Authorization' => $access_token,
            'Content-Type' => 'application/json',
        ];

        try {
            $res = $client->request('POST',
                $url,
                [
                    'headers' => $header,
                    'json' => $body,
                ]
            );
            return ($res->getBody());

        } catch (\Exception $ee) {
            return 'Error';

        }


    }

}