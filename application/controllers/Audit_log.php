<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Audit_log extends CI_Controller {

    function __construct()
    {
        parent::__construct();
        $this->load->library('adminauth');
    }


    public function ajax_list($type)
    {
        $records = array();
        $records["data"] = array();

        if( ! $this->adminauth->logged_in())
        {
            echo json_encode($records);
            exit();

        }

        /*if ( ! $this->adminauth->has_role('11'))
        {
            echo json_encode($records);
            exit();
        }*/


        $sql = "SELECT
		*
		FROM
		log
		WHERE type='$type'";

        $iTotalRecords = count($this->query->query($sql));
        $iDisplayLength = intval($this->input->post('length'));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($this->input->post('start'));
        $sEcho = intval($this->input->post('draw'));


        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;


        $filter_fullname = $this->input->post('user_full_name');
        $filter_date_from = $this->input->post('date_from');
        $filter_date_to = $this->input->post('date_to');

        if($filter_fullname){
            $sql .= " AND user_full_name like '%".$filter_fullname."%'";
        }
        if($filter_date_from){
            $sql .= " AND log_date >= '".date('Y-m-d',strtotime($filter_date_from))."'";
        }
        if($filter_date_to){
            $sql .= " AND log_date <= '".date('Y-m-d',strtotime($filter_date_to))."'";
        }

        $sort_array = array(
            'log_date',
            'user_full_name',
            'description',
        );

        $order = $this->input->post('order');
        if($order){
            $column = $order[0]['column'];
            $dir = $order[0]['dir'];
            if(isset($sort_array[$column]))
            {
                $f = $sort_array[$column];

                if($dir == 'asc')
                    $sql .= " ORDER BY $f ASC";
                else
                    $sql .= " ORDER BY $f DESC";
            }

        }



        $sql2 = $sql . " LIMIT " . $iDisplayStart . "," . $iDisplayLength;
        $list = $this->query->query($sql2);

        foreach ($list as $row){

            $records["data"][] = array(
                $row['log_date'],
                $row['user_full_name'],
                $row['description'],
                ''
            );
        }


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);

    }
}