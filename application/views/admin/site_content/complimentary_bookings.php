<!-- BEGIN CONTAINER -->

<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php');?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">
            <?php if ($page_action == ''): ?>
            <h3 class="page-title">Complimentary Bookings</h3>
            <div class="page-bar">
                <div class="page-toolbar">
                    <div class="btn-group pull-right"> </div>
                </div>
            </div>
            <?php
            echo get_message();
            ?>
            <div class="row">
                <div class="col-md-12">

                    <div class="table-container">

                        <table class="table table-striped table-bordered table-hover" id="datatable_ajax"
                               data-order-col = "0"
                               data-order-type = "desc"
                               data-ajax_url="<?php echo base_url('admin/'.$slug . '/ajax_list') ?>">
                            <colgroup>
                                <col width="40">
                                <col width="100">
                                <col width="100">
                                <col width="200">
                                <col>
                                <col width="100">
                                <col>
                                <col width="50">
                                <col width="50">
                                <col width="100">
                                <col width="150">
                            </colgroup>
                            <thead>
                            <tr role="row" class="heading">
                                <th>#</th>
                                <th>Reference #</th>
                                <th>Date of Purchase</th>
								<th>User</th>
                                <th>Comment</th>
                                <th>Theater</th>
								<th>Movie</th>
                                <th>Movie date</th>
                                <th>Time Slots</th>
                                <th class="sorting_disabled">Tickets</th>
                                <th class="sorting_disabled">Booking Method</th>
                                <th class="sorting_disabled">Action</th>
                            </tr>
                            <tr role="row" class="filter">
                                <td></td>
                                <td><input type="text" class="form-control form-filter input-sm" name="ref_no"></td>
                                <td><div class="input-group date date-picker margin-bottom-5" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control form-filter input-sm" name="date_from" placeholder="From">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
											</span> </div>
                                    <div class="input-group date date-picker" data-date-format="dd/mm/yyyy">
                                        <input type="text" class="form-control form-filter input-sm" name="date_to" placeholder="To">
											<span class="input-group-btn">
											<button class="btn btn-sm default" type="button"><i class="fa fa-calendar"></i></button>
											</span> </div></td>
								<td><input type="text" class="form-control form-filter input-sm" name="name"></td>
                                <td><!--<input type="text" class="form-control form-filter input-sm" name="mobile">--></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>
                                    <select name="booking_method" class="form-control form-filter input-sm">
                                        <option value="-1">All</option>
                                        <option value="1">Normal</option>
                                        <option value="2">Advanced</option>
                                    </select>
                                </td>


                                <td>
                                    <div class="margin-bottom-5">
                                        <button class="btn btn-sm yellow filter-submit margin-bottom"><i
                                                class="fa fa-search"></i> Search
                                        </button>
                                    </div>
                                    <button class="btn btn-sm red filter-cancel"><i class="fa fa-times"></i> Reset
                                    </button>
                                </td>
                            </tr>
                            </thead>
                            <tbody>
                            </tbody>
                        </table>


                    </div>
                </div>
            </div>
            <script>

                $('.date-picker').datepicker({
                    format: "yyyy-mm-dd",
                    autoclose: true
                });

                jQuery(document).ready(function () {
                    TableAjax.init();
                });

                $('.required-entry').attr('required', 'required');
                $('#edit_form').validate({
                    errorElement: 'span',
                    errorClass: 'validation-advice',
                    ignore: "",
                    submitHandler: function (form) {
                        form.submit();
                    }
                });


            </script>

            <?php endif;//$page_action ?>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END CONTENT -->

</div>
<!-- END CONTAINER --> 
