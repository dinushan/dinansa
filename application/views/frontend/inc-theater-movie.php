<?php
if(!empty($th['movie'])):

?>

<div class="movieSec moviesCont">
    <div class="moviesWrap">
         <div class="movieImage data-image"  data-image-src="<?php echo $th['movie']['portrait_image']; ?>"></div>
        <a href="<?php echo base_url('movie/'.$th['movie']['movie_url_key']); ?>" class="imageOverlay"></a>
        <div class="captionWidgetWrap">
            <div class="captionTitleWrap">
                <h3 class="captionTitle"><?php echo $th['movie']['movie_name']; ?></h3>
            </div>
            <div class="releaseDate"><?php
                $newDate = date('d S F Y', strtotime($th['movie']['date_release'])).' - '.date('d S F Y', strtotime($th['movie']['date_end']));
                echo $newDate;  ?></div>
            <div class="iconContainer clearfix">
                <div class="iconWrapper">
                    <ul class="iconWrap">
                        <li><a href="<?php echo $th['movie']['trailer']; ?>"
                               class="youTubePopUp">
                                <i class="icon fa fa-youtube-play" aria-hidden="true"></i><span
                                    class="icoText">Trailer</span></a></li>
                        <li><a href="<?php echo base_url('movie/'.$th['movie']['movie_url_key']); ?>"><i class="icon fa fa-info" aria-hidden="true"></i><span
                                    class="icoText">Info</span></a></li>

                    </ul>

                </div>
            </div>
        </div>
    </div>
</div>

<div class="bookingSec">
    <?php
    if(strtotime(date('Y-m-d')) >= strtotime($th['movie']['date_release'])):
        $c_date = date('d F Y');
        ?>
        <script>
            window.movie = {
                'movie_date':<?php echo json_encode($c_date); ?>
            };
        </script>
        <h3 class="title"> Now Showing</h3>
    <?php else:
        $c_date = date('d F Y',strtotime($th['movie']['date_release']));
        ?>
        <h3 class="title"> Coming Up</h3>
    <?php endif; ?>

    <div class="secTitle">Available Showtimes</div>
    <!--div class="selectDateWrap calendarWrap">
        <input type="text" data-theatre-id="<?php echo $th['theater_id']; ?>" 
               data-movie-id="<?php echo $th['movie_id']; ?>" 
               class="form-control input-scope selectMovieDate" value="<?php echo $c_date; ?>"/>
        <span class="icon calendar-icon"></span>
    </div-->
    <div class="detailBody">
        <div class="detailRow">


            <table class="detailTable">
                <tbody>

                <?php
                $show_time_slots   = $th['movie']['show_time_slots'];
                $enc_theater_id    = $this->encrypt_lib->encode($th['theater_id']);
                $enc_movie_id      = $this->encrypt_lib->encode($th['movie_id']);
                $ind = 0;
                foreach ($show_time_slots as $key => $time):
                    $ind = $key;

                    if ($key % 2 == 0){
                    echo '<tr>';
                    }
                    ?>

                        <td>
                            <div class="detailSpan"><span class="time"><?php echo $time['time_slot']; ?></span></div>
                        </td>


                <?php
                    if ($key % 2 == 0){
                        echo '</tr>';
                    }
                endforeach;?>

                </tbody>
                <tfoot>
                    <tr>
                        <td>
                            <div class="detailSpan">
                                <a onclick="return getTheaterBookButton()" href="<?php echo base_url('buy-tickets-online/'.'?movie='.$enc_movie_id.'&theater='.$enc_theater_id) ?>" class="btn btn-scope now-showing-upcoming">
                                    BOOK
                                </a>
                            </div>
                        </td>
                    </tr>
                </tfoot>
            </table>

        </div>

    </div>
</div>
<?php endif; ?>