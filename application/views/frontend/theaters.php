<?php include(APPPATH . 'views/frontend/inc/head.php'); ?>
<?php include(APPPATH . 'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer theatresPage" xmlns="http://www.w3.org/1999/html">
    <div class="row">
        <div class="col-xs-12 breadcrumbCont">
            <?php include(APPPATH . 'views/frontend/inc/breadcrumb.php'); ?>
        </div>

        <div class="col-xs-12 headWrap">
            <h2 class="heading"><?php echo $sitePageData->menu; ?></h2>
        </div>
    </div>
</div>
<div class="container pageMiddleContainer theatresPage" >
    <div class="row">

    <?php
    foreach($theaters as $th):

        $theater_image = $th['images'];
     

    ?>
        <div id="<?php echo 'div-'.$th['theater_id'].'-'.$th['movie_id']; ?>" class="clearfix thetresCont repeatableRow animatedParent" >          
			<div class="col-xs-12 col-md-6 col-lg-7 padding-zero-mobile animated fadeInUpShort">
				<div class="sliderWrap">
					<div class="theatreImage data-image"
						 data-image-src="<?php echo $theater_image['image'] ?>">
						<a href="<?php echo base_url('theater/'.$th['theater_url_key']); ?>" class="captionWrap">
							<div class="theatreDetailWrap">
								<div class="theatreLogo"><img class="logoImg img-responsive"
															  src="<?php echo $th['logo']; ?>"
															  alt="majestic"></div>
								<div class="theatreText"><?php echo $th['theater_name']; ?></div>
							</div>
						</a>
					</div>
				</div>
			</div>
			<div class="col-xs-12 col-md-6 col-lg-5 padding-zero-mobile div-movie hidden-xs  animated fadeInUpShort">
				<?php include(APPPATH . 'views/frontend/inc-theater-movie.php'); ?>
			</div>           
        </div>

	<?php

	endforeach;
	?>

    </div>
</div>
<?php include(APPPATH . 'views/frontend/inc/footer.php'); ?>
<script>
    $(function(){

         $('.selectMovieDate').datepicker({
                minDate: 0,
                dateFormat:'dd MM yy',
                showButtonPanel: true,
                 onSelect: function(dateText, inst) {
                    var date = $(this).val();
                    var theatre_id = $(this).data('theatre-id');
                    var movie_id = $(this).data('movie-id');
                     console.log("Here");
                     
                     getTimeSlots(theatre_id,movie_id,date);

                 },
                beforeShow:function(input, ins) {
                        datePickerBeforeShow();
                },
                onClose:function(input, ins) {
                        datePickerOnClose(input);
                }

            }).focus(function () {
                  $(this).blur()
                }).datepicker('widget').wrap('<div class="ll-skin-melon"/>');
    });
</script>