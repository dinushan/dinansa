<!-- BEGIN CONTAINER -->

<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<?php if($page_action == ''):?>
			<h3 class="page-title"><?php echo $title; ?></h3>
			<div class="page-bar">
				<div class="page-toolbar">
					<div class="btn-group pull-right"> <a onclick="setLocation('<?php echo site_url('admin/'.$slug.'/add'); ?>')" class="btn green"><i class="fa fa-plus-circle"></i>Add Advertisement</a> </div>
				</div>
			</div>
			<?php echo get_message() ?>
			<div class="row">
				<div class="col-md-12">
					<div class="table-scrollable">
						<?php if($this->adminauth->has_role('601')):?>
							<?php
							$attr = array("id"=>"edit_form","class"=>"form-horizontal","enctype"=>"multipart/form-data");
							echo form_open(current_url(),$attr);
							?>
						<table id="tbl" class="table table-striped table-bordered table-advance table-hover">
							<colgroup>
							<col width="50" />
							<col width="400" />
							<col width="200"  />
							<col width="200"  />
							</colgroup>
							<thead>
								<tr class="headings nodrop nodrag">
									<th>ID</th>
									<th>Title</th>
									<th>Page</th>
									<th>Action</th>
								</tr>
							</thead>

							<tbody>
								<?php 
									$edit_url = site_url('admin/'.$slug.'/edit');
									$delete_url = site_url('admin/'.$slug.'/delete');
									
										foreach($advertise_data as $_page):
										$id = $_page['id'];
                                    	$encry_hcId = $this->encrypt_lib->encode($id);										
                                   ?>
								<tr id="<?php echo $encry_hcId ?>">
									<td><?php echo $_page['id'] ?></td>
									<td><?php echo $_page['advertisement_name'] ?></td>
									<td><?php echo $_page['page_name'] ?></td>
									<td colspan="2" align="center"><a class="btn btn-sm default btn-xs" href="<?php echo $edit_url.'/'.$encry_hcId ?>"><i class="fa fa-edit"></i> Edit</a>
										<button class="btn btn-sm red btn-xs" type="button" 
											onclick="deleteConfirm('Are you sure you want to delete?','Confirm Delete',
											'<?php echo $delete_url.'/'.$encry_hcId ?>');return false;"><i class="fa fa-trash-o"></i> Delete</button></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						</form>
						<?php endif; ?>
					</div>
				</div>
			</div>					
			<?php 

			elseif($page_action == 'add' || $page_action == 'edit'): ?>
			<h3 class="page-title"><?php
				if($page_action == 'add'){
					echo 'Add Advertisement';
				}else{
					echo 'Edit "'.$advertisement_data->advertisement_name.'"';
				}
				?></h3>
			<div class="page-bar">
				<div class="page-toolbar">
					<div class="btn-group pull-right"> <a class="btn default" href="<?php echo site_url('admin/advertisementImages/index') ?>"><i class="fa fa-chevron-left"></i> Back</a> </div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12"> <?php echo get_message() ?> 
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet light bordered"> 
						<!--<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-pencil"></i>Settings
							</div>							
						</div>-->
						<div class="portlet-body form"> 
							<!-- BEGIN FORM-->
							<?php                    
							$attr = array("id"=>"edit_form","class"=>"form-horizontal","enctype"=>"multipart/form-data");
							echo form_open(current_url(),$attr);
							?>
							<?php

							$ADVERTISEMENT_PAGES = unserialize(ADVERTISEMENT_PAGES);
							
								if($page_action == 'edit'){
									$page_id 				= $advertisement_data->page_id;
									$page_name 				= $advertisement_data->page_name;
									$advertisement_name 	= $advertisement_data->advertisement_name;
									$web_view_image 		= $advertisement_data->web_view_image;
									$mobile_view_image 		= $advertisement_data->mobile_view_image;
									
								}else{
									$page_id 					= '';
									$page_name 					= '';
									$advertisement_name 		= '';
									$web_view_image 			= '';
									$mobile_view_image 			= '';

									$required_entry = ' required-entry';
									
								}
								
							?>
							<h4 class="form-section">General</h4>
							
							<div class="form-group">
								<label class="control-label col-md-3">Advertisement Name<span class="req">*</span></label>
								<div class="control-element col-md-4">
									<input name="title" type="text" class="form-control required-entry" value="<?php echo set_value('title',$advertisement_name) ?>" />
									<?php echo form_error('title'); ?> </div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Page<span class="req">*</span></label>
								<div class="control-element col-md-4">
									<?php echo form_dropdown('page_id', $ADVERTISEMENT_PAGES,set_value('page_id',$page_id),'placeholder="Select" class="form-control required-entry pageName"');?>
									<?php echo form_error('page_id'); ?>
								</div>
							</div>

							<?php if($page_action == 'edit' && $web_view_image!= ''):?>
							<div class="form-group">
								<label class="control-label col-md-3">Image(Web View)</label>
								<div class="control-element col-md-4">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td><img src="<?php echo base_url().ADVERTISEMENT_IMG_PATH.$web_view_image ?>?<?php echo time() ?>" alt="" width="200" height="150" /></td>
											<td>&nbsp;</td>
											<td><?php list($img_width, $img_height) = getimagesize(ADVERTISEMENT_IMG_PATH.$web_view_image);?>
												<strong>Image Info</strong><br />
												width:<?php echo $img_width ?>px<br />
												height:<?php echo $img_height ?>px</td>
										</tr>
										<tr>
											<td colspan="3">&nbsp;
												<input type="hidden" name="image1" value="<?php echo $web_view_image ?>" /></td>
										</tr>
										<tr>
											<td colspan="3"><label>
													<input name="img_del1" type="checkbox" value="1" />
													Image Delete </label>
										</tr>
										<tr>
											<td colspan="3">&nbsp;</td>
										</tr>
									</table>
								</div>
							</div>
								<?php else:?>
								<div class="form-group">
									<label class="control-label col-md-3">Image(Web View)</label>
									<div class="control-element col-md-6">
										<input type="file" class=" input-file" name="image1">
										<span class="help-block font-green home">Image Size:<?php echo HOME_PG_ADVERTISEMENT_IMG_W ?>px &times; <?php echo HOME_PG_ADVERTISEMENT_IMG_H ?>px, Image Type: jpg</span>
										<span class="help-block font-green movie_detail">Image Size:<?php echo MOVIE_DETAIL_PG_ADVERTISEMENT_IMG_W ?>px &times; <?php echo MOVIE_DETAIL_PG_ADVERTISEMENT_IMG_H ?>px, Image Type: jpg</span>
									</div>
								</div>
							<?php endif;?>

							<div class="theater_detail">
							<?php if($page_action == 'edit' && $mobile_view_image!= ''):?>
								<div class="form-group">
									<label class="control-label col-md-3">Image (Mobile View)</label>
									<div class="control-element col-md-4">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td><img src="<?php echo base_url().ADVERTISEMENT_IMG_PATH.$mobile_view_image ?>?<?php echo time() ?>" alt="" width="200" height="150" /></td>
												<td>&nbsp;</td>
												<td><?php list($img_width, $img_height) = getimagesize(ADVERTISEMENT_IMG_PATH.$mobile_view_image);?>
													<strong>Image Info</strong><br />
													width:<?php echo $img_width ?>px<br />
													height:<?php echo $img_height ?>px</td>
											</tr>
											<tr>
												<td colspan="3">&nbsp;
													<input type="hidden" name="image2" value="<?php echo $mobile_view_image ?>" /></td>
											</tr>
											<tr>
												<td colspan="3"><label>
														<input name="img_del2" type="checkbox" value="1" />
														Image Delete </label>
											</tr>
											<tr>
												<td colspan="3">&nbsp;</td>
											</tr>
										</table>
									</div>
								</div>
							<?php else:?>
								<div class="form-group">
									<label class="control-label col-md-3">Image (Mobile View)</label>
									<div class="control-element col-md-6">
										<input type="file" class=" input-file" name="image2">
										<span class="help-block font-green home">Image Size:<?php echo HOME_PG_ADVERTISEMENT_IMG_MOBILE_W ?>px &times; <?php echo HOME_PG_ADVERTISEMENT_IMG_MOBILE_H ?>px, Image Type: jpg</span>
										<span class="help-block font-green movie_detail">Image Size:<?php echo MOVIE_DETAIL_PG_ADVERTISEMENT_MOBILE_IMG_W ?>px &times; <?php echo MOVIE_DETAIL_PG_ADVERTISEMENT_MOBILE_IMG_H ?>px, Image Type: jpg</span>
									</div>
								</div>
							<?php endif;?>
							</div>

							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
									</div>
								</div>
							</div>

							</form>
						</div>
					</div>
				</div>
			</div>
                <script type="text/javascript">
                    $('.required-entry').attr('required','required');
                    $('#edit_form').validate({
                        errorElement: 'span',
                        errorClass: 'validation-advice',
                        ignore: "",
                        submitHandler: function (form) {
                            form.submit();
                        }
                    });

                    $(function() {
                        if($('.pageName').val() == 1) {
                            $('.home').show();
                            $('.movie_detail').hide();
                            $('.theater_detail').show();
                        } else if($('.pageName').val() == 2) {
                            $('.home').hide();
                            $('.movie_detail').show();
                            $('.theater_detail').show();
                        }
                        else if($('.pageName').val() == 3)
                        {
                            $('.home').hide();
                            $('.movie_detail').show();
                            $('.theater_detail').hide();
                        }
                        else {
                            $('.home').hide();
                            $('.movie_detail').hide();
                        }
                        $('.pageName').change(function(){
                            if($('.pageName').val() == 1) {
                                $('.home').show();
                                $('.movie_detail').hide();
                                $('.theater_detail').show();
                            } else if($('.pageName').val() == 2) {
                                $('.home').hide();
                                $('.movie_detail').show();
                                $('.theater_detail').show();
                            }
                            else if($('.pageName').val() == 3)
                            {
                                $('.home').hide();
                                $('.movie_detail').show();
                                $('.theater_detail').hide();
                            }
                            else {
                                $('.home').hide();
                                $('.movie_detail').hide();
                            }
                        });
                    });

                </script>
			<?php endif;//$page_action ?>
		</div>
		<!-- END PAGE CONTENT--> 
	</div>
	<!-- END CONTENT --> 

</div>
<!-- END CONTAINER -->

