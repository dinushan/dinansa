<?php
use Illuminate\Database\Eloquent\Model as Eloquent;
use Illuminate\Database\Capsule\Manager as DB;


class Tutor extends Eloquent
{
    protected $table        = "tutors";
    protected $primaryKey   = "id";

}