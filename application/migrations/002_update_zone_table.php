<?php

class Migration_Update_Zone_Table extends CI_Migration
{
    public function up()
    {

        $this->db->query('ALTER TABLE zones RENAME TO restaurant_zones');

        $fields = array(
            'color' => array(
                'type' => 'VARCHAR',
                'constraint' => '10',
            ),
            'price_type' => array(
                'type' => 'VARCHAR',
                'constraint' => '20',
                'null' => true,
            ),
            'amount' => array(
                'type' => 'DECIMAL',
                'constraint' => '10,2',
                'null' => true,
                'default' => 0.00
            ),
            'created_at' => array(
                'type' => 'DATETIME',
                'null' => true,
            ),
            'updated_at' => array(
                'type' => 'DATETIME',
                'on update' => 'NOW()',
                'null' => true,
            ),
            'deleted_at' => array(
                'type' => 'DATETIME',
                'null' => true,
            ),
        );

        $this->dbforge->add_column('restaurant_zones', $fields);
        $this->dbforge->drop_column('restaurant_zones', 'table_count');
        $this->dbforge->drop_column('restaurant_zones', 'chair_count');
    }

    public function down()
    {
        $this->dbforge->drop_table('zones');
    }
}
