

<div class="page-container">
<?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

    <div class="page-content-wrapper">
        <div class="page-content">
            <div class="portlet light bordered">
                <div class="portlet-title">
                <div class="caption">
                    <i class="fa fa-info"></i>
                    <span class="caption-subject font-dark sbold uppercase"> Booking Details : <?php echo $booking->reference_no ?></span>
                </div>
                <div class="actions">
                    <div class="btn-group btn-group-devided" data-toggle="buttons">
                        <label class="btn btn-transparent dark btn-outline btn-circle btn-sm active">
                            <button class="btn btn-green" id="option1" onclick="exportCVS()"><i class="fa fa-table"></i> Export</button>
                        </label>
                    </div>
                </div>
            </div>

                <div class="portlet-body form">
                    <div class="form-horizontal" >
                        <div class="form-body">

                            <div class="form-group">
                                <label class="col-md-3 control-label">Reference No</label>
                                <div class="col-md-9">
                                    <h4><?php echo $booking->reference_no;?></h4>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Customer Name</label>
                                <div class="col-md-9">
                                    <h4><?php echo $booking->first_name.' '.$booking->last_name;?></h4>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Movie</label>
                                <div class="col-md-9">
                                    <h4><?php echo $booking->movie_name;?></h4>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Theater</label>
                                <div class="col-md-9">
                                    <h4><?php echo $booking->name;?></h4>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Purches Date</label>
                                <div class="col-md-9">
                                    <h4><?php echo $booking->created_at;?></h4>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Movie Date and Time</label>
                                <div class="col-md-9">
                                    <h4><?php echo $booking->time_slot;?></h4>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-md-3 control-label">Seats</label>
                                <div class="col-md-9">
                                    <p><?php echo implode(", ",$seats);?></p>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-md-3 control-label">Price</label>
                                <div class="col-md-9">
                                            <?php foreach ($price as $p):?>
                                                <div class="row">
                                                    <div class="col-md-2"><b>Category</b></div>
                                                    <div class="col-md-10"><?php echo $categories[$p->price_category]; ?></div>
                                                    <div class="col-md-2"><b>Price</b></div>
                                                    <div class="col-md-10"><?php echo $p->price; ?></div>
                                                    <div class="col-md-2"><b>Online Charge</b></div>
                                                    <div class="col-md-10"><?php echo $p->online_charge; ?></div>
                                                    <div class="col-md-2"><b>No of Tickets</b></div>
                                                    <div class="col-md-10"><?php echo $p->qty; ?></div>
                                                    <div class="col-md-2"><b>Total</b></div>
                                                    <div class="col-md-10"><?php echo ($p->price+$p->online_charge)*$p->qty; ?></div>
                                                </div>
                                                <hr/>
                                            <?php endforeach;?>


                                </div>
                            </div>


                        </div>
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
<iframe name="sales_report_download" style="display: none;"></iframe>

<script>
    function exportCVS() {
        window.open("<?php echo base_url('admin/reports/fyi_reports/export_details')?>","sales_report_download");

    }
</script>