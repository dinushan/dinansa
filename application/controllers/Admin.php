<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Admin extends Elevate_Controller
{

    private $pageData = [];
    /**
     * Admin::__construct()
     *
     */
    public function __construct()
    {
        parent::__construct();


        $this->pageData['slug']  = 'admin';
        $this->pageData['title'] = 'Admin User';
    }

    /**
     * Admin::convert()
     *
     */
    public function convert()
    {
        $this->load->dbforge();
        $this->dbforge->modify_column($this->adminauth->_table['groups'], array(
            'roles' => array(
                'name' => 'roles',
                'type' => 'text'
            )
        ));

        $query = $this->db->select('group_id, roles')->get($this->adminauth->_table['groups']);
        if($query && $query->num_rows())
        {
            foreach($query->result() as $row)
            {
                $this->db->where('group_id', $row->group_id)->set('roles', $this->adminauth->convert($row->roles))->update($this->adminauth->_table['groups']);
            }
        }

        echo 'Update complete.';
    }


    /**
     * Admin::index()
     *
     */
    public function index()
    {
        if( ! $this->adminauth->logged_in())
        {
			
            $this->session->set_userdata('redir', current_url());
            redirect('login');
        }
		
		$this->dashboard();
    }

    /**
    * Admin::register()
    *
    */
    public function register()
    {
        if($this->input->post())
        {
            $this->form_validation->set_rules('username', 'Username', 'trim|required|adminauth_unique_username');
            $this->form_validation->set_rules('fullname', 'Fullname');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required|adminauth_valid_password');
            $this->form_validation->set_rules('password_conf', 'Password Confirmation', 'required|matches[password]');

            if($this->form_validation->run() == TRUE)
            {
                unset($_POST['submit'], $_POST['password_conf']);
                $this->adminauth->add_user($this->input->post());
                redirect('login');
            }

        }

        $this->pageData['title'] = 'Register';
        $this->pageData['slug'] = 'user';

        $this->pageData['page_content'] = $this->load->view('admin/user/add_user', [], true);
        $this->parser->parse('common/master_template', $this->pageData);
    }
	
	/**
     * Admin::users()
     *
     */
    public function users()
    {

		if ( ! $this->adminauth->has_role('101') &&  ! $this->adminauth->has_role('102') &&  ! $this->adminauth->has_role('103'))
        {
            no_access();
            return;
        }
		
		$this->pageData['slug'] = 'users';
        $this->pageData['title'] = 'Users';

        $pageContentData['adminauth'] = $this->adminauth;
		$pageContentData['users'] = $this->adminauth->get_users();

        $this->pageData['page_content'] = $this->load->view('admin/user/users', $pageContentData, true);
        $this->parser->parse('common/master_template', $this->pageData);
		
    }

    public function ajax_list()
    {
        $records = array();
        $records["data"] = array();

        if( ! $this->adminauth->logged_in())
        {
            echo json_encode($records);

        }
        if ( ! $this->adminauth->has_role('101') &&  ! $this->adminauth->has_role('102') &&  ! $this->adminauth->has_role('103'))
        {
            echo json_encode($records);
        }


        $sql = "SELECT
		auth_restaurant_admin_users.*,
		auth_restaurant_admin_userdata.*
		FROM
		auth_restaurant_admin_users
		LEFT JOIN auth_restaurant_admin_userdata ON auth_restaurant_admin_userdata.user_id = auth_restaurant_admin_users.user_id
		WHERE auth_restaurant_admin_users.user_id <> 0";

        $iTotalRecords = count($this->query->query($sql));
        $iDisplayLength = intval($this->input->post('length'));
        $iDisplayLength = $iDisplayLength < 0 ? $iTotalRecords : $iDisplayLength;
        $iDisplayStart = intval($this->input->post('start'));
        $sEcho = intval($this->input->post('draw'));


        $end = $iDisplayStart + $iDisplayLength;
        $end = $end > $iTotalRecords ? $iTotalRecords : $end;



        $filter_fullname = $this->input->post('fullname');
        $filter_username = $this->input->post('username');
        $filter_phone1 = $this->input->post('phone1');
        if($filter_fullname){
            $sql .= " AND fullname like '%".$filter_fullname."%'";
        }
        if($filter_username){
            $sql .= " AND username like '%".$filter_username."%'";
        }
        if($filter_phone1){
            $sql .= " AND phone1 = '".$filter_phone1."'";
        }

        $sort_array = array(
            'auth_restaurant_admin_users.user_id',
            'fullname',
            'username',
            'phone1',
        );
        $order = $this->input->post('order');
        if($order){
            $column = $order[0]['column'];
            $dir = $order[0]['dir'];
            if(isset($sort_array[$column]))
            {
                $f = $sort_array[$column];

                if($dir == 'asc')
                    $sql .= " ORDER BY $f ASC";
                else
                    $sql .= " ORDER BY $f DESC";
            }

        }


        $sql2 = $sql . " LIMIT " . $iDisplayStart . "," . $iDisplayLength;
        $list = $this->query->query($sql2);

        foreach ($list as $row){

            $encry_emId = $this->encrypt_lib->encode($row['user_id']);

            $action_html = '';
            $edit_url = "window.location='".base_url('users/edit/' . $encry_emId)."'";
            $delete_url = "deleteConfirm('Are you sure you want to delete?','Confirm Delete','".base_url()."users/delete/".$encry_emId."',false);";

            if ($row['user_id'] != 1 && $this->adminauth->has_role('102')):
                $action_html .='<button
                    onclick="'.$edit_url.'"
                    class="btn btn-sm default btn-xs"><i class="fa fa-edit"></i> Edit
                </button>';
            endif;
            if ($row['user_id'] == 1 && $this->adminauth->has_role('super_admin')):
                $action_html .='<button
                    onclick="'.$edit_url.'"
                    class="btn btn-sm default btn-xs"><i class="fa fa-edit"></i> Edit
                </button>';
            endif;
            if ($row['user_id'] != 1 && $this->adminauth->has_role('103')):
                $action_html .='<button
                    onclick="'.$delete_url.'"
                    class="btn btn-sm red btn-xs"><i class="fa fa-trash-o"></i> Delete
                </button>';
            endif;

            $records["data"][] = array(
                $row['user_id'],
                $row['fullname'],
                $row['username'],
                $row['phone1'],
                $action_html
            );
        }


        $records["draw"] = $sEcho;
        $records["recordsTotal"] = $iTotalRecords;
        $records["recordsFiltered"] = $iTotalRecords;

        echo json_encode($records);

    }




    /**
    * Admin::add_user()
    *
    */
    public function add_user()
    {
			


        if ( ! $this->adminauth->has_role('102'))
        {
            no_access();
            return;
        }
		
        if($this->input->post())
        {
            $validation_msg = '';
            $this->load->library('upload');
            $this->load->library('image_lib');


            $this->form_validation->set_rules('username', 'Username', 'trim|required|adminauth_unique_username');
            $this->form_validation->set_rules('emp_id', 'Employee ID', 'trim|adminauth_unique_emp_id');
            $this->form_validation->set_rules('fullname', 'Fullname', 'trim|required');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('password', 'Password', 'required|adminauth_valid_password');
            $this->form_validation->set_rules('password_conf', 'Password Confirmation', 'required|matches[password]');
            $this->form_validation->set_rules('department[]', 'Departments');

            if($this->form_validation->run() == TRUE)
            {
				
				$fullname = $this->input->post('fullname');
				$username = $this->input->post('username');
				$password = $this->input->post('password');
				$email = $this->input->post('email');

                if (isset($_FILES['image']) && $_FILES['image']['tmp_name'] != '') {

                    $config['upload_path'] = PROFILE_IMG_PATH;
                    $config['allowed_types'] = 'jpg|jpeg';
                    $config['remove_spaces'] = true;
                    $config['thumb_marker'] = "";
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('image')) {
                        if (isset($_FILES['image']) && $_FILES['image']['tmp_name'] != '') {
                            $validation_msg .= '<div class="alert alert-danger">Picture : ' . $this->upload->display_errors() . '</div>';
                        }

                    } else {
                        $imageData = $this->upload->data();
                        $orig_name = $imageData['file_name'];
                        $_POST['picture'] = $orig_name;
                        $this->img_lib->resizeImage(PROFILE_IMG_PATH.$orig_name, PROFILE_IMG_W, PROFILE_IMG_H,100);

                    }
                }

                if($this->input->post('department')){
                    $_POST['department'] = json_encode($_POST['department']);
                }else{
                    $_POST['department'] = null;
                }

                if($this->input->post('zone')){
                    $_POST['zone'] = serialize($_POST['zone']);
                }else{
                    $_POST['zone'] = null;
                }

				
                unset($_POST['submit'], $_POST['password_conf']);
                $userData = $this->adminauth->add_user($this->input->post());
				$user_id = $userData->user_id;//------------------------------------------
			
				/*
				$subject = SITE_NAME." System User Registration";		
				$email_body = file_get_contents ('application/views/email_templates/admin/user_registration.html');					
				$email_body = str_replace ( '%name%', $fullname, $email_body );
				$email_body = str_replace ( '%base_url%', base_url(), $email_body );
				$email_body = str_replace ( '%username%', $username, $email_body );
				$email_body = str_replace ( '%password%', $password, $email_body );
				$this->common_model->email($email,$subject, $email_body,'admin_user_registration');
				*/
                $log_data = array(
                    'type' => 'user',
                    'action' => 'add',
                    'description' => "New user has been created - $fullname",
                );
	            $this->log_model->add($log_data);

				set_message($validation_msg.'<div class="alert alert-success">'.
							SUCCESS_MSG_ADD.'</div>');
                redirect('users/add');
            }

        }



        $groups = array();
        foreach($this->adminauth->get_groups() as $_group)
        {
            $groups[$_group->group_id] = $_group->name;
        }
        $pageData['groups'] = $groups;
        $pageData['adminauth'] = $this->adminauth;
        $pageData['departments'] = $this->query->get_opt("SELECT id,stock_name 
        FROM inventory_store_locations WHERE type = 2","id","stock_name",0);

        $pageData['zones'] = $this->query->get_opt("SELECT id, `name` 
        FROM restaurant_zones WHERE deleted_at IS NULL", "id", "name", 0);

        $this->pageData['title'] = 'Add User';
        $this->pageData['slug'] = 'user';
        $this->pageData['page_content'] = $this->load->view('admin/user/add_user', $pageData, true);
        $this->parser->parse('common/master_template', $this->pageData);
		
    }


    /**
    * Admin::edit_user()
    *
    */
    public function edit_user($enc_user_id)
    {
		$user_id = $this->encrypt_lib->decode($enc_user_id);


        if ( ! $this->adminauth->has_role('102'))
        {
            no_access();
            return;
        }

        if($this->input->post())
        {
            $this->load->library('upload');
            $this->load->library('image_lib');
            $validation_msg = "";

            //$this->form_validation->set_rules('username', 'Username', 'trim|required|adminauth_unique_username['.$user_id.']');
            $this->form_validation->set_rules('fullname', 'Fullname');
            $this->form_validation->set_rules('email', 'Email', 'trim|required|valid_email');
            $this->form_validation->set_rules('active', 'Active');
            $this->form_validation->set_rules('enabled', 'Enabled');
            $this->form_validation->set_rules('password_never_expires', 'Password Never Expires');
            $this->form_validation->set_rules('groups[]', 'Groups');
            $this->form_validation->set_rules('department[]', 'Departments');

            if($this->input->post('password'))
            {
                $this->form_validation->set_rules('password', 'Password', 'adminauth_valid_password');
                $this->form_validation->set_rules('password_conf', 'Password Confirmation', 'required|matches[password]');
            }

            if($this->form_validation->run() == TRUE)
            {

                if (isset($_FILES['image']) && $_FILES['image']['tmp_name'] != '') {

                    $config['upload_path'] = PROFILE_IMG_PATH;
                    $config['allowed_types'] = 'jpg|jpeg';
                    $config['remove_spaces'] = true;
                    $config['thumb_marker'] = "";
                    $this->upload->initialize($config);

                    if (!$this->upload->do_upload('image')) {
                        if (isset($_FILES['image']) && $_FILES['image']['tmp_name'] != '') {
                            $validation_msg .= '<div class="alert alert-danger">Picture : ' . $this->upload->display_errors() . '</div>';
                        }

                    } else {
                        $imageData = $this->upload->data();
                        $orig_name = $imageData['file_name'];
                        $_POST['picture'] = $orig_name;
                        $this->img_lib->resizeImage(PROFILE_IMG_PATH.$orig_name, PROFILE_IMG_W, PROFILE_IMG_H,100);

                    }
                }

                $userData = $this->adminauth->get_user_by_id($user_id);
				$username = $userData->username;

				if($this->input->post('department')){
                    $_POST['department'] = json_encode($_POST['department']);
                }else{
                    $_POST['department'] = null;
                }

                if($this->input->post('zone')){
                    $_POST['zone'] = serialize($_POST['zone']);
                }else{
                    $_POST['zone'] = null;
                }

                unset($_POST['submit'], $_POST['password_conf'], $_POST['image']);
                $this->adminauth->update_user($user_id, $this->input->post());
               
			   	set_message($validation_msg.'<div class="alert alert-success">'.
							SUCCESS_MSG_UPDATE.'</div>');

                $log_data = array(
                    'type' => 'user',
                    'action' => 'edit',
                    'description' => $userData->fullname."'s data has been updated",
                );
                $this->log_model->add($log_data);

               	redirect('users/edit/'.$enc_user_id);
            }

        }

        $groups = array();
        foreach($this->adminauth->get_groups() as $_group)
        {
            $groups[$_group->group_id] = $_group->name;
        }
		

		$pageData['adminauth'] = $this->adminauth;
		$pageData['groups'] = $groups; 	
		$pageData['user'] = $this->adminauth->get_user_by_id($user_id);
        $pageData['departments'] = $this->query->get_opt("SELECT id,stock_name 
        FROM inventory_store_locations WHERE type = 2","id","stock_name",0);

        $pageData['zones'] = $this->query->get_opt("SELECT id, `name` 
        FROM restaurant_zones WHERE deleted_at IS NULL", "id", "name", 0);

        $this->pageData['title'] = 'Edit User';
        $this->pageData['slug'] = 'user';
        $this->pageData['page_content'] = $this->load->view('admin/user/edit_user', $pageData, true);
        $this->parser->parse('common/master_template', $this->pageData);
	   
    }
		
	public function delete_user($enc_user_id)
    {
		$user_id = $this->encrypt_lib->decode($enc_user_id);

        if ( ! $this->adminauth->has_role('104'))
        {
            no_access();
            return;
        }
		
		$user = $this->adminauth->get_user_by_id($user_id);
		$username = $this->adminauth->get_user_by_id($user_id)->username;
		if(count($user) > 0){

            $log_data = array(
                'type' => 'user',
                'action' => 'delete',
                'description' => $user->fullname."'s data has been deleted",
            );
            $this->log_model->add($log_data);

            $this->adminauth->delete($user_id);
			
			set_message('<div class="alert alert-success">'.
						SUCCESS_MSG_DELETED.'</div>');
		}else{
			set_message('<div class="alert alert-danger">'.
						INVALID_PARAM.'</div>');
		}
		
        redirect('users/');
	}


    /**
     * Admin::activate()
     *
     */
     public function activate($activation_code)
     {
         if($this->adminauth->activate($activation_code))
         {
             $this->load->view('admin/activation_successful');
             return;
         }

         $this->load->view('admin/activation_failed');
     }

    /**
     * Admin::logout()
     *
     */
	
    public function logout()
    {
        $this->adminauth->logout();
        redirect('admin');
    }

    public function dashboard()
    {

        $this->pageData['slug']  = 'dashboard';
        $this->pageData['title'] = 'Dashboard';

        $this->pageData['page_content'] = $this->load->view('admin/dashboard', $this->pageData, true);
        $this->parser->parse('common/master_template', $this->pageData);

    }
	
	
	/**
     * Admin::my_account()
     *
     */
	
	public function my_account()
    {
		$user_id =  $this->adminauth->user_id;
        $validation_msg = '';

        if($this->input->post())
        {
            $this->load->library('upload');
            $this->load->library('image_lib');

            $this->form_validation->set_rules('action', 'Action', 'required');

            $image = $this->input->post('image');
            $img_del = $this->input->post('img_del');
            if ($img_del == 1) {
                $this->adminauth->update_user($user_id, array('picture' => null));
                $this->img_lib->delete_file(PROFILE_IMG_PATH . $image);
            }
			
			unset($_POST['image']);


            if (isset($_FILES['image']) && $_FILES['image']['tmp_name'] != '') {

                $config['upload_path'] = PROFILE_IMG_PATH;
                $config['allowed_types'] = 'jpg|jpeg';
                $config['remove_spaces'] = true;
                $config['thumb_marker'] = "";
                $this->upload->initialize($config);

                if (!$this->upload->do_upload('image')) {
                    if (isset($_FILES['image']) && $_FILES['image']['tmp_name'] != '') {
                        $validation_msg .= '<div class="alert alert-danger">Picture : ' . $this->upload->display_errors() . '</div>';
                    }

                } else {
                    $imageData = $this->upload->data();
                    $orig_name = $imageData['file_name'];

                    $this->img_lib->resizeImage(PROFILE_IMG_PATH.$orig_name, PROFILE_IMG_W, PROFILE_IMG_H,100);

                    $this->adminauth->update_user($user_id,array('picture'=>$orig_name));
                    $validation_msg .= '<div class="alert alert-success">Profile Picture has been updated</div>';
                }
            }

            if($this->input->post('password'))
            {
                $this->form_validation->set_rules('password', 'Password', 'adminauth_valid_password');
                $this->form_validation->set_rules('password_conf', 'Password Confirmation', 'required|matches[password]');
            }

            if($this->form_validation->run() == TRUE)
            {

                unset($_POST['submit'], $_POST['password_conf'], $_POST['action']);
                if($this->input->post('password'))
                {
                    $this->adminauth->update_user($user_id,array('password'=>$this->input->post('password')));
                    $validation_msg .= '<div class="alert alert-success">'.$this->lang->line('success_msg_my_account_save_password').'</div>';
                }

			   	set_message($validation_msg);

            }


            redirect('my_account');
        }

        $groups = array();
        foreach($this->adminauth->get_groups() as $_group)
        {
            $groups[$_group->group_id] = $_group->name;
        }

        $this->pageData['slug'] = 'my_account';
        $this->pageData['title'] = 'My Account';

        $pageContentData['adminauth'] = $this->adminauth;
        $pageContentData['user'] = $this->adminauth->get_user_by_id($user_id);
		

        $this->pageData['page_content'] = $this->load->view('admin/user/my_account', $pageContentData, true);
        $this->parser->parse('common/master_template', $this->pageData);
	   
    }
	
	public function forgot_password()
   {
				
		if($this->input->post()){	
			
			$this->form_validation->set_rules('email', 'email', 'trim|required|valid_email'); 
			 
			if($this->form_validation->run() == TRUE)
            {
				$email = $this->input->post('email');
				
				$user = $this->adminauth->get_user_by_email($email);		
				if($user !== FALSE){
					
					if($user->enabled == 0 ){
				
						set_message('<div class="alert alert-danger">'.
							$this->lang->line('adminauth_enable_account').'</div>');
						
						redirect('admin/forgot_password');
					}
					
					$forgot_code = $this->adminauth->forgot_password($user->user_id);
					
					$fullname = $user->fullname;
					$reset_url = base_url().'admin/new_password/'.$forgot_code;														
									
					$subject = SITE_NAME." System Password Reset";

                    $data = array(
                        'name' => $fullname,
                        'reset_url' => $reset_url
                    );
                    $email_template_data['mail_content'] = $this->load->view('email_templates/admin/forgot_password', $data, true);
                    $email_body = $this->load->view('email_templates/inc/email_template', $email_template_data, true);

					if($this->common_model->email($email,$subject, $email_body,'admin_user_password_reset'))
					{						
						set_message('<div class="alert alert-success">'.
							$this->lang->line('adminauth_send_password_reset_email').'</div>');
					}else
					{											
						set_message('<div class="alert alert-danger">'.
							$this->lang->line('adminauth_unable_to_send_email').'</div>');
					}			
												
				
				}else{
						
					set_message('<div class="alert alert-danger">'.
							$this->lang->line('adminauth_not_found').'</div>');
				}
				
				redirect('admin/forgot_password');
			}
			
			
		}
		
		$pageData['slug'] = 'forgot_password';   
		$pageData['title'] = 'Reset Password';  
		$pageData['adminauth'] = $this->adminauth;  		
		
		$this->load->view('admin/forgot_password',$pageData);
   }
   
   
    public function new_password($forgot_code='')
	{
		if($this->input->post()){	
			
			$this->form_validation->set_rules('password', 'Password', 'trim|required|adminauth_valid_password|matches[password_conf]');
			$this->form_validation->set_rules('password_conf', 'Confirmation Password', 'trim|required'); 
			$this->form_validation->set_error_delimiters('<li>', '</li>');
			if($this->form_validation->run() == TRUE)
            {				
				
				$user = $this->adminauth->get_user_by_forgot_code($forgot_code);
				
				if($user != FALSE){
					
					$this->adminauth->set_password($user->user_id, $this->input->post('password'));
															
					if($this->adminauth->login($user->username,$this->input->post('password'),FALSE,array('customer_type'=>0)))
					{
						set_message('<div class="alert alert-success">
									New Password has been successfully changed.</div>');
						redirect("admin/my_account");
					}
					else
					{
						set_message('<div class="alert alert-danger">'.
							$this->adminauth->get_error().'</div>');
					}
					
				}else{
					set_message('<div class="alert alert-danger">'.
							$this->lang->line('adminauth_fogot_password_code_not_found').'</div>');
				}
                
			}
			else
            {
				  set_message('<div class="alert alert-danger">'.validation_errors().'</div>');
            }
			redirect(current_url());
			
		}
		
		$pageData['slug'] = 'new_password';   
		$pageData['title'] = 'New Password';  
		$pageData['adminauth'] = $this->adminauth;  		

		
		//$this->load->view('admin/inc_head',$pageData);
		$this->load->view('admin/new_password',$pageData);
		//$this->load->view('admin/inc_footer');

    }

    /**
     * Admin::groups()
     *
     */
    public function groups()
    {

        if ( ! $this->adminauth->has_role('super_admin'))
        {
            no_access();
            return;
        }


        $pageData['adminauth'] = $this->adminauth;
        $pageData['groups'] = $this->adminauth->get_groups();

        $this->pageData['title'] = 'Groups';
        $this->pageData['slug'] = 'user';
        $this->pageData['page_content'] = $this->load->view('admin/user/groups', $pageData, true);
        $this->parser->parse('common/master_template', $this->pageData);
    }

    /**
     * Admin::add_group()
     *
     */
    public function add_group()
    {
        if( ! $this->adminauth->logged_in())
        {
            $this->session->set_userdata('redir', current_url());
            redirect('admin/login');
        }

        if ( ! $this->adminauth->has_role('super_admin'))
        {
            no_access();
            return;
        }

        if($this->input->post())
        {
            $this->form_validation->set_rules('name', 'Group Name', 'trim|required|adminauth_unique_group');
            $this->form_validation->set_rules('description', 'Description');
            $this->form_validation->set_rules('members[]', 'Members');
            $this->form_validation->set_rules('roles[]', 'Roles');

            if($this->form_validation->run() == TRUE)
            {


                unset($_POST['submit']);
                $this->adminauth->add_group($this->input->post());

                set_message('<div class="alert alert-success">'.
                    SUCCESS_MSG_ADD.'</div>');
                redirect('users/add_group/');
            }

        }

        $users = array();
        foreach($this->adminauth->get_users() as $_user)
        {
            $users[$_user->user_id] = $_user->fullname;
        }


        $pageData['adminauth'] = $this->adminauth;
        $pageData['roles'] = $this->adminauth->get_roles();
        $pageData['users'] = $users;

        $this->pageData['title'] = 'Add Group';
        $this->pageData['slug'] = 'user';
        $this->pageData['page_content'] = $this->load->view('admin/user/add_group', $pageData, true);
        $this->parser->parse('common/master_template', $this->pageData);

    }

    /**
     * Example:edit_group()
     *
     */
    public function edit_group($enc_group_id)
    {
       $group_id = $this->encrypt_lib->decode($enc_group_id);

        if ( ! $this->adminauth->has_role('super_admin'))
        {
            no_access();
            return;
        }

        if($this->input->post())
        {
            $this->form_validation->set_rules('name', 'Group Name', 'trim|required|adminauth_unique_group['.$group_id.']');

            if($this->form_validation->run() == TRUE)
            {


                unset($_POST['submit']);
                $this->adminauth->update_group($group_id, $this->input->post());

                set_message('<div class="alert alert-success">'.
                    SUCCESS_MSG_UPDATE.'</div>');
                redirect('users/edit_group/'.$enc_group_id);
            }

        }

        $users = array();
        foreach($this->adminauth->get_users() as $_user)
        {
            $users[$_user->user_id] = $_user->fullname;
        }

        $group = $this->adminauth->get_group_by_id($group_id);

        $role_list = array();
        $roles = $this->adminauth->get_roles();
        foreach($roles as $_slug => $_desc)
        {
            if($this->adminauth->has_role($_slug, $group->roles))
            {
                $role_list[] = $_slug;
            }
        }


        $pageData['adminauth'] = $this->adminauth;
        $pageData['roles'] = $roles;
        $pageData['group'] = $group;
        $pageData['group_roles'] = $role_list;
        $pageData['users'] = $users;

        $this->pageData['title'] = 'Edit Group';
        $this->pageData['slug'] = 'user';
        $this->pageData['page_content'] = $this->load->view('admin/user/edit_group', $pageData, true);
        $this->parser->parse('common/master_template', $this->pageData);

    }
    /**
     * Admin::delete_group()
     *
     */
    public function delete_group($enc_user_id)
    {
        $group_id = $this->encrypt_lib->decode($enc_user_id);
        if ( ! $this->adminauth->has_role('super_admin'))
        {
            no_access();
            return;
        }

        $group = $this->adminauth->get_group_by_id($group_id);
        if(count($group) > 0)
        {
            $this->adminauth->delete_group($group_id);
            set_message('<div class="alert alert-success">'.SUCCESS_MSG_DELETED.'</div>');
        }else{
            set_message('<div class="alert alert-danger">'.INVALID_PARAM.'</div>');
        }

        redirect('users/groups');
    }



    public function audit_log()
    {

        if ( ! $this->adminauth->has_role('99'))
        {
            no_access();
            return;
        }


        $pageData['adminauth'] = $this->adminauth;
        $pageData['page_title'] = 'Users : Audit Log';
        $pageData['type'] = 'user';

        $this->pageData['title'] = 'Users : Audit Log';
        $this->pageData['slug'] = 'user';
        $this->pageData['page_content'] = $this->load->view('admin/audit', $pageData, true);
        $this->parser->parse('common/master_template', $this->pageData);

    }



}