<!-- BEGIN CONTAINER -->
<script>
    window.moviedata = {
        price_array:<?php echo json_encode($price_array);?>,

    }
</script>
<div class="page-container">
    <?php $this->load->view('counter/inc_sidebar_navigation.php'); ?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
<div class="page-content">
<?php if  ($page_action == 'view'): ?>

    <?php if(count($customer_data)>0):?>
        <h3 class="page-title"> <?php echo $customer_data[0]['first_name']." ".$customer_data[0]['last_name']." : ".$customer_data[0]['mobile_no'] ;?></h3>
    <?php endif; ?>

    <div class="page-bar">
        <div class="page-toolbar">
            <div class="btn-group pull-right">
                <a class="btn default" href="<?php echo site_url('counter/' . $slug . '/') ?>"><i
                        class="fa fa-chevron-left"></i> Back</a>


            </div>
        </div>
    </div>

    <div class="row">

        <div class="col-md-12">


            <div class="table-container">
                <button id="btn-export-csv" type="button" class="btn btn-default pull-right" onclick="setLocation('<?php echo base_url('counter/customers/exportExcel/'.$enc_id);?>')">Export to Excel</button>

                <table id="tbl-report" class="table table-striped table-bordered table-hover">
                    <thead>
                    <tr class="headings">
                        <th>Movie name</th>
                        <th>Theater</th>
                        <th>Time slot</th>
                        <th>Total</th>
                        <th>Discount</th>
                        <th>View details</th>

                    </tr>
                    </thead>
                    <tbody>
                    <?php foreach($customer_data as $customer_dat){?>
                    <tr>

                        <td class="movie" ><?php echo $customer_dat['movie_name'];?></td>
                        <td class="theater" ><?php echo $customer_dat['theater_name'];?></td>
                        <td class="time_slot" ><?php echo $customer_dat['time_slot'];?></td>
                        <td class="total_amount" ><?php echo $customer_dat['total_amount'];?></td>
                        <td class="payment_method_id" style="display: none;"><?php echo $customer_dat['payment_method_id'];?></td>
                        <td class="total_discount_amount" ><?php echo $customer_dat['total_discount_amount'];?></td>
                        <td class="acti" >
                            <button onclick="" data-booking_price='<?php echo json_encode($customer_dat['booking_price']);?>' data-booking_seat='<?php echo json_encode($customer_dat['booking_seat']);?>' class="btn btn-sm default btn-xs view-booking"><i class="fa fa-eye"></i> View booking data</button>
                        </td>
                    </tr>
                    <?php }?>

                    </tbody>
                    <tfoot>

                    </tfoot>
                </table>

            </div>
        </div>
    </div>

<?php endif;//$page_action ?>


    <div class="modal fade" id="summeryModal" tabindex="-1" role="dialog" aria-labelledby="summeryModalLabel"
         data-backdrop="static" data-keyboard="false">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title" id="myModalLabel">Booking Details</h4>
                </div>
                <div class="modal-body"></div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>

                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $('.view-booking').click(function(){
                var html_price = '', html_selected_seat = '';


            console.log($(this).data('booking_price'));
            console.log($(this).data('booking_seat'));

            var booking_price=$(this).data('booking_price'),booking_seat=$(this).data('booking_seat');
            for (i = 0; i < booking_price.length; i++) {
                if (Number(booking_price[i].qty) > 0) {
                    html_price += "<tr><td colspan='2' align='center'>-----------------------------------------------------------------------------------------------------------------</td></tr>" +
                        "<tr>" +
                        "<th>Ticket Type</th>" +
                        "<td>" + window.moviedata.price_array[parseInt(booking_price[i].price_category)]  + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<th>Unit Price (Rs)</th>" +
                        "<td>" + booking_price[i].price + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<th>Qty</th>" +
                        "<td>" + booking_price[i].qty + "</td>" +
                        "</tr>" +
                        "<tr>" +
                        "<th>Amount (Rs)</th>" +
                        "<td>" + booking_price[i].price * booking_price[i].qty + "</td>" +
                        "</tr>";
                }
            }

            for (i = 0; i < booking_seat.length; i++) {
                html_selected_seat += "<tr>" +
                    "<th>"+ booking_seat[i].label +"</th>" +
                    "<td>" + booking_seat[i].serial_number + "</td>" +
                    "</tr>";

                }

            var dt = new Date($(this).parents('tr').find(".time_slot").text());
            //alert((date.getMonth() + 1) + '/' + date.getDate() + '/' +  date.getFullYear());

                var theater = $(this).parents('tr').find(".theater").text();
                var movie = $(this).parents('tr').find(".movie").text();
                var time_slot = (dt.getHours() < 13 ? dt.getHours() : dt.getHours() - 12) + ':' + dt.getMinutes() + ' ' + (dt.getHours() < 13 ? 'AM' : 'PM');
                var movie_date = (dt.getMonth() + 1) + '/' + dt.getDate() + '/' +  dt.getFullYear();
                var total = $(this).parents('tr').find(".total_amount").text();

                var discount=$(this).parents('tr').find(".total_discount_amount").text();
                var paid_by_by = $(this).parents('tr').find(".payment_method_id").text();

                var html = "<table class='table'>" +
                    "<tr>" +
                    "<th>Theater</th>" +
                    "<td>" + theater + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Movie</th>" +
                    "<td>" + movie + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Time slot</th>" +
                    "<td>" + time_slot + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Movie date</th>" +
                    "<td>" + movie_date + "</td>" +
                    "</tr>" +
                    "<tr>" +
                    "<th>Selected Seats</th>" +
                    "<td>" + html_selected_seat + "</td>" +
                    "</tr>" +
                    html_price +
                    "<tr>" +
                    "<td colspan='2' align='center'>-----------------------------------------------------------------------------------------------------------------</td>" +
                    "</tr>";

                if(discount >0){
                    html += "<tr>" +
                        "<th>Discount (Rs)</th>" +
                        "<td>" + Number(discount) + "</td>" +
                        "</tr>" ;
                }
                html += "<tr>" +
                    "<th>Total Amount (Rs)</th>" +
                    "<td>" + Number(total) + "</td>" +
                    "</tr>" ;

                html += "<tr>" +
                    "<th>Paid by</th>" +
                    "<td>" + paid_by_by + "</td>" +
                    "</tr>" +

                    "</table>";

                $('#summeryModal').modal('show');
                $('#summeryModal .modal-body').html(html);

        });

    </script>
</div>
<!-- END PAGE CONTENT-->
</div>
<!-- END CONTENT -->

</div>
<!-- END CONTAINER --> 