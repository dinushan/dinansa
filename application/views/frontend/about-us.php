<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

	<div class="container pageContainer mainPageContainer contactUsPage">
		<div class="row">
			<div class="col-xs-12 breadcrumbCont">
				<?php include(APPPATH.'views/frontend/inc/breadcrumb.php'); ?>
			</div>
			<div class="col-xs-12 headWrap">
				<h2 class="heading"><?php echo $sitePageData->title1; ?></h2>
			</div>

			<div class="col-md-6 col-sm-12 hidden-xs">
				<img class="img-responsive" src="images/scope-about-us.jpg" alt="">
			</div>
			<div class="col-xs-12 col-sm-12 col-md-6">
				<div class="aboutSec">
					<h2 class="aboutTitle">What is Scope all about?</h2>
					<div class="aboutDesc">
						<p>Dedicated toward raising the bar in Sri Lanka’s cinema experience, Scope aims to provide our patrons with all the flare of old-school movie theaters, coupled with state-of-the-art modernity and comfort, consistent with international standards.</p>
					</div>
				</div>
				<div class="aboutSec">
					<h3 class="aboutSecTitle">How will your Scope cinema experience differ from other cinema experiences?</h3>
					<div class="aboutDesc">
						<p>It’s all about attention to detail.</p>
						<p>From the box office toppers that we’ll be airing throughout the year, to the handpicked f & b brands, right down to the uber comfy seating – Scope is fast-tracking the future of Sri Lanka’s cinema sphere.</p>
					</div>
				</div>
			</div>
		</div>
	</div>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>