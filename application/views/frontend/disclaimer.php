<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer disclaimerPage">
   <div class="row">
      <div class="col-xs-12 breadcrumbCont">
		  <?php include(APPPATH.'views/frontend/inc/breadcrumb.php'); ?>
      </div>

	  <div class="col-xs-12 headWrap">
	         <h2 class="heading">Disclaimer</h2>
	  </div>

	  <div class="col-xs-12">
	  		<div class="disclaimerDesc">
	  			<p>This Website has been compiled in good faith by Scope cinemas and Theatres and all information and data provided on this site is for informational purposes only.
				</p>
	  			<p>Scope cinemas and Theatres do not guarantee the accuracy, validity, completeness or the suitability of any information or data on this site (www.scopecinemas.com). No representation is made or warranty given, either express or implied as to the completeness or accuracy of the information that it contains, that it will be uninterrupted or error free or that any information is free of bugs, viruses, worms, trojan horses or other harmful components.</p>
	  			<p>Although we believe that all information which currently appears on the www.scopecinemas.com web site is to the best of our knowledge valid and accurate and has been obtained from reliable sources, it is however the sole responsibility of the user to ascertain the validity and accuracy of the information provided on the www.scopecinemas.com site prior to making decisions.</p>
	  			<p>To the maximum extent permitted by law, Scope cinemas and Theatres disclaims all implied warranties with regard to information, products, services and material provided through this Website. All such information, products, services and materials are provided “as is” and “as available” without warranty of any kind.</p>
	  			<p>This Website may contain links to Web Sites operated by third parties. Such links are provided for your reference only and your use of these sites may be subject to terms and conditions posted on them. Scope cinemas and Theatres inclusion of links to other Websites does not imply any endorsement of the material on such Websites and Scope cinemas  and Theatres accepts no liability for their contents. Further, the user agrees that Scope cinemas and Theatres cannot be held responsible for any damage, claims or any mishaps that may happen to occur from any link leading outside this site.</p>
	  			<p>By accessing this Website (www.scopecinemas.com ) you agree that Scope cinemas and Theatres will not be liable for any direct, indirect or consequential loss or damages of any nature arising from the use of this Website, including information and material contained in it, from your access of other material on the internet via web links from this Website, delay or inability to use this Website or the availability and utility of the products and services.</p>
	  			<p>You further agree to indemnify, hold Scope cinemas and Theatres harmless from and covenant not to sue Scope cinemas and Theatres for any claims based on using this Website (www.scopecinemas.com).</p>
	  		
	  		</div>
	  </div>
     </div>
</div>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>