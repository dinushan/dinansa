<!-- BEGIN CONTAINER -->

<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<?php if($page_action == ''):?>
			<h3 class="page-title">Manage News</h3>
			<div class="page-bar">
				<div class="page-toolbar">
					<div class="btn-group pull-right"> <a onclick="setLocation('<?php echo site_url('admin/'.$slug.'/add'); ?>')" class="btn green"><i class="fa fa-plus-circle"></i>Add News</a> </div>
				</div>
			</div>
			<?php echo get_message() ?>
			<div class="row">
				<div class="col-md-12">
					<div class="table-scrollable">
						<?php if($this->adminauth->has_role('501')):?>
						<form action="<?php echo current_url() ?>" method="get" id="frmSearch">
						<table id="tbl" class="table table-striped table-bordered table-advance table-hover">
							<colgroup>
							<col width="150" />
							<col />
							<col width="200"  />
							</colgroup>
							<thead>
								<tr class="headings nodrop nodrag">
									<th>Published Date</th>
									<th>Title</th>
									<th>Action</th>
								</tr>
							</thead>
							<tr class="filter nodrop nodrag">
								<th><div class="field-100">
									
								</div></th>
								<th><div class="field-100">
									<input type="text" class="form-control" value="<?php echo $this->input->get('title') ?>" name="title">
								</div></th>
								<th colspan="2" class=" no-link last">
									<button onclick="frmSearch.submit();" class="btn btn-red btn-small" type="button"><span>Search</span></button>
									<button onclick="setLocation('<?php echo site_url('admin/'.$slug.'/index'); ?>')" class="btn btn-red btn-small "
									type="button"><span>Reset</span></button><input name="q" type="hidden" value="search" />
								</th>
							</tr>
							<tbody>
								<?php 
									$edit_url = site_url('admin/'.$slug.'/edit');
									$delete_url = site_url('admin/'.$slug.'/delete');
									
										foreach($newsList as $_page):
										$id = $_page['id'];	
														
                                    	$encry_hcId = $this->encrypt_lib->encode($id);										
                                   ?>
								<tr id="<?php echo $encry_hcId ?>">
									<td><?php echo $_page['published_date'] ?></td>
									<td><?php echo $_page['title'] ?></td>
									<td colspan="2" align="center"><a class="btn btn-sm default btn-xs" href="<?php echo $edit_url.'/'.$encry_hcId ?>"><i class="fa fa-edit"></i> Edit</a>
										<button class="btn btn-sm red btn-xs" type="button" 
											onclick="deleteConfirm('Are you sure you want to delete?','Confirm Delete',
											'<?php echo $delete_url.'/'.$encry_hcId ?>');return false;"><i class="fa fa-trash-o"></i> Delete</button></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						</form>
						<?php endif; ?>
					</div>
				</div>
			</div>					
			<?php 
			echo $pagination_links;
			
			
			elseif($page_action == 'add' || $page_action == 'edit'): ?>
			<h3 class="page-title"><?php
				if($page_action == 'add'){
					echo 'Add News';
				}else{
					echo 'Edit "'.$news_data->title.'"';
				}
				?></h3>
			<div class="page-bar">
				<div class="page-toolbar">
					<div class="btn-group pull-right"> <a class="btn default" href="<?php echo site_url('admin/'.$slug.'/index/') ?>"><i class="fa fa-chevron-left"></i> Back</a> </div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12"> <?php echo get_message() ?> 
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet light bordered"> 
						<!--<div class="portlet-title">
							<div class="caption">
								<i class="fa fa-pencil"></i>Settings
							</div>							
						</div>-->
						<div class="portlet-body form"> 
							<!-- BEGIN FORM-->
							<?php                    
							$attr = array("id"=>"edit_form","class"=>"form-horizontal","enctype"=>"multipart/form-data");
							echo form_open(current_url(),$attr);
							?>
							<?php
								if($page_action == 'edit'){
									$id 				= $news_data->id;
									$title 				= $news_data->title;
									$description 		= $news_data->description;
									$short_description 	= $news_data->short_description;
									$image1 			= $news_data->image1;
									$image2 			= $news_data->image2;
									$image3 			= $news_data->image3;
									$image4 			= $news_data->image4;

									$url_key		 	= $news_data->url_key;
									$published_date 	= $news_data->published_date;
									$meta_title 		= $news_data->meta_title;
									$meta_keywords 		= $news_data->meta_keywords;
									$meta_description 	= $news_data->meta_description;
									$og_type 			= $news_data->og_type;
									$og_title 			= $news_data->og_title;
									$og_description 	= $news_data->og_description;
									
								}else{
									$title 				= '';
									$description 		= '';
									$short_description 	= '';
									$image1 			= '';
									$image2 			= '';
									$image3 			= '';
									$image4 			= '';
									$type 				= '';
									$url_key			= '';
									$published_date 	= date('Y-m-d');
									$meta_title 		= '';
									$meta_keywords 		= '';
									$meta_description 	= '';
									$og_type 			= '';
									$og_title 			= '';
									$og_description 	= '';

									$required_entry = ' required-entry';
									
								}
								
							?>
							<h4 class="form-section">General</h4>
							<div class="form-group">
								<label class="control-label col-md-3">Title<span class="req">*</span></label>
								<div class="control-element col-md-4">
									<input name="title" type="text" class="form-control required-entry" value="<?php echo set_value('title',$title) ?>" />
									<?php echo form_error('title'); ?> </div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Published Date<span class="req">*</span></label>
								<div class="control-element col-md-4">
									<div class="input-group date date-picker" data-date-format="yyyy-mm-dd">
										<input name="published_date" type="text" class="form-control required-entry datepicker" value="<?php echo set_value('published_date',$published_date) ?>" />
										<span class="input-group-btn">
										<button class="btn default" type="button"><i class="fa fa-calendar"></i></button>
										</span>
									</div>									
									<?php echo form_error('published_date'); ?> </div>
							</div>

							<?php if($page_action == 'edit' && $image1!= ''):?>
							<div class="form-group">
								<div class="control-label col-md-3">&nbsp;</div>
								<div class="control-element col-md-4">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td><img src="<?php echo base_url().NEWS_IMG_PATH.$image1 ?>?<?php echo time() ?>" alt="" width="200" height="150" /></td>
											<td>&nbsp;</td>
											<td><?php list($img_width, $img_height) = getimagesize(NEWS_IMG_PATH.$image1);?>
												<strong>Image Info</strong><br />
												width:<?php echo $img_width ?>px<br />
												height:<?php echo $img_height ?>px</td>
										</tr>
										<tr>
											<td colspan="3">&nbsp;
												<input type="hidden" name="image1" value="<?php echo $image1 ?>" /></td>
										</tr>
										<tr>
											<td colspan="3"><label>
													<input name="img_del1" type="checkbox" value="1" />
													Image Delete </label>
										</tr>
										<tr>
											<td colspan="3">&nbsp;</td>
										</tr>
									</table>
								</div>
							</div>
								<?php else:?>
								<div class="form-group">
									<label class="control-label col-md-3">Image 1</label>
									<div class="control-element col-md-6">
										<input type="file" class=" input-file" name="image1">
										<span class="help-block font-green">Image Size:<?php echo NEWS_IMG_W ?>px &times; <?php echo NEWS_IMG_H ?>px, Image Type: jpg</span> </div>
								</div>
							<?php endif;?>

							<?php if($page_action == 'edit' && $image2!= ''):?>
								<div class="form-group">
									<div class="control-label col-md-3">&nbsp;</div>
									<div class="control-element col-md-4">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td><img src="<?php echo base_url().NEWS_IMG_PATH.$image2 ?>?<?php echo time() ?>" alt="" width="200" height="150" /></td>
												<td>&nbsp;</td>
												<td><?php list($img_width, $img_height) = getimagesize(NEWS_IMG_PATH.$image2);?>
													<strong>Image Info</strong><br />
													width:<?php echo $img_width ?>px<br />
													height:<?php echo $img_height ?>px</td>
											</tr>
											<tr>
												<td colspan="3">&nbsp;
													<input type="hidden" name="image2" value="<?php echo $image2 ?>" /></td>
											</tr>
											<tr>
												<td colspan="3"><label>
														<input name="img_del2" type="checkbox" value="1" />
														Image Delete </label>
											</tr>
											<tr>
												<td colspan="3">&nbsp;</td>
											</tr>
										</table>
									</div>
								</div>
							<?php else:?>
								<div class="form-group">
									<label class="control-label col-md-3">Image 2</label>
									<div class="control-element col-md-6">
										<input type="file" class=" input-file" name="image2">
										<span class="help-block font-green">Image Size:<?php echo NEWS_IMG_W ?>px &times; <?php echo NEWS_IMG_H ?>px, Image Type: jpg</span> </div>
								</div>
							<?php endif;?>

							<?php if($page_action == 'edit' && $image3!= ''):?>
								<div class="form-group">
									<div class="control-label col-md-3">&nbsp;</div>
									<div class="control-element col-md-4">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td><img src="<?php echo base_url().NEWS_IMG_PATH.$image3 ?>?<?php echo time() ?>" alt="" width="200" height="150" /></td>
												<td>&nbsp;</td>
												<td><?php list($img_width, $img_height) = getimagesize(NEWS_IMG_PATH.$image3);?>
													<strong>Image Info</strong><br />
													width:<?php echo $img_width ?>px<br />
													height:<?php echo $img_height ?>px</td>
											</tr>
											<tr>
												<td colspan="3">&nbsp;
													<input type="hidden" name="image3" value="<?php echo $image3 ?>" /></td>
											</tr>
											<tr>
												<td colspan="3"><label>
														<input name="img_del3" type="checkbox" value="1" />
														Image Delete </label>
											</tr>
											<tr>
												<td colspan="3">&nbsp;</td>
											</tr>
										</table>
									</div>
								</div>
							<?php else:?>
								<div class="form-group">
									<label class="control-label col-md-3">Image 3</label>
									<div class="control-element col-md-6">
										<input type="file" class=" input-file" name="image3">
										<span class="help-block font-green">Image Size:<?php echo NEWS_IMG_W ?>px &times; <?php echo NEWS_IMG_H ?>px, Image Type: jpg</span> </div>
								</div>
							<?php endif;?>

							<?php if($page_action == 'edit' && $image4!= ''):?>
								<div class="form-group">
									<div class="control-label col-md-3">&nbsp;</div>
									<div class="control-element col-md-4">
										<table width="100%" border="0" cellspacing="0" cellpadding="0">
											<tr>
												<td><img src="<?php echo base_url().NEWS_IMG_PATH.$image4 ?>?<?php echo time() ?>" alt="" width="200" height="150" /></td>
												<td>&nbsp;</td>
												<td><?php list($img_width, $img_height) = getimagesize(NEWS_IMG_PATH.$image4);?>
													<strong>Image Info</strong><br />
													width:<?php echo $img_width ?>px<br />
													height:<?php echo $img_height ?>px</td>
											</tr>
											<tr>
												<td colspan="3">&nbsp;
													<input type="hidden" name="image4" value="<?php echo $image4 ?>" /></td>
											</tr>
											<tr>
												<td colspan="3"><label>
														<input name="img_del4" type="checkbox" value="1" />
														Image Delete </label>
											</tr>
											<tr>
												<td colspan="3">&nbsp;</td>
											</tr>
										</table>
									</div>
								</div>
							<?php else:?>
								<div class="form-group">
									<label class="control-label col-md-3">Image 4</label>
									<div class="control-element col-md-6">
										<input type="file" class=" input-file" name="image4">
										<span class="help-block font-green">Image Size:<?php echo NEWS_IMG_W ?>px &times; <?php echo NEWS_IMG_H ?>px, Image Type: jpg</span> </div>
								</div>
							<?php endif;?>
														
							<div class="form-group">
								<label class="control-label col-md-3">Short Description<span class="req">*</span></label>
								<div class="control-element col-md-6">
									<textarea cols="15" rows="4" class="form-control" name="short_description"><?php echo $page_action == "add" ? 
														set_value('short_description') : 
														set_value('short_description',$short_description) ?></textarea>
									<?php echo form_error('short_description'); ?> </div>
							</div>
							
							<div class="form-group">
								<label class="control-label col-md-3">Description<span class="req">*</span></label>
								<div class="control-element col-md-6">
									<textarea id="description_textarea" cols="15" rows="10" class="tinymce_editor" name="description"><?php echo $page_action == "add" ? 
														set_value('description') : 
														set_value('description',$description) ?></textarea>
									<?php echo form_error('description'); ?> </div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">URL key<span class="req">*</span></label>
								<div class="control-element col-md-4">
									<input name="url_key" type="text"
										   class="form-control req-entry required-entry"
										   value="<?php echo $page_action == "add" ?
											   set_value('url_key') :
											   set_value('url_key',$url_key) ?>" size="15" />
									<?php echo form_error('url_key'); ?>
									<div class="help-block font-green">"Note: If the site is LIVE changing the URL may harm your ranking on search engines" </div>
								</div>
							</div>
							
							
							<h4 class="form-section">Meta Settings</h4>
							<div class="form-group">
								<label class="control-label col-md-3">Meta Title</label>
								<div class="control-element col-md-4">
									<input type="text" class="form-control" value="<?php echo 
								$page_action == "add" ? 
								set_value('meta_title') : 
								set_value('meta_title',$meta_title) ?>" 
								name="meta_title" />
								</div>
							</div>
							
							<?php if(SHOW_META_CONTENT === TRUE):?>							
							<div class="form-group">
								<label class="control-label col-md-3">Meta Keywords</label>
								<div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="meta_keywords"><?php echo $page_action == "add" ? 
								set_value('meta_keywords') : 
								set_value('meta_keywords',$meta_keywords) ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">Meta Description</label>
								<div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="meta_description"><?php echo $page_action == "add" ? 
								set_value('meta_description') : 
								set_value('meta_description',$meta_description) ?></textarea>
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">OG Type</label>
								<div class="control-element col-md-4">
									<input type="text" class="form-control" value="<?php echo 
								$page_action == "add" ? 
								set_value('og_type') : 
								set_value('og_type',$og_type) ?>" 
								name="og_type" />
									<span class="help-block font-green">The type of the object, such as 'article'</span></div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">OG Title</label>
								<div class="control-element col-md-4">
									<input type="text" class="form-control" value="<?php echo 
								$page_action == "add" ? 
								set_value('og_title') : 
								set_value('og_title',$og_title) ?>" 
								name="og_title" />
								</div>
							</div>
							<div class="form-group">
								<label class="control-label col-md-3">OG Description</label>
								<div class="control-element col-md-4">
									<textarea cols="15" rows="2" class="form-control" name="og_description"><?php echo $page_action == "add" ? 
								set_value('og_description') : 
								set_value('og_description',$og_description) ?></textarea>
								</div>
							</div>
							<?php endif; ?>
							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
									</div>
								</div>
							</div>
							<script type="text/javascript">							 
								$('.required-entry').attr('required','required');					
								$('#edit_form').validate({
									errorElement: 'span', 
									errorClass: 'validation-advice', 								
									ignore: "",
									submitHandler: function (form) {
										form.submit();										
									}
								});	
								
								$('.datepicker').datepicker({
									format: 'yyyy-mm-dd',
									 orientation: "left",
                					autoclose: true
									});
												
							</script>
							</form>
						</div>
					</div>
				</div>
			</div>
			<?php endif;//$page_action ?>
		</div>
		<!-- END PAGE CONTENT--> 
	</div>
	<!-- END CONTENT --> 
	
</div>
<!-- END CONTAINER --> 

