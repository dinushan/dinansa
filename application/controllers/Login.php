<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends CI_Controller
{

    private $pageData = [];

    public function __construct()
    {
        parent::__construct();

        $this->pageData['slug']  = 'login';
        $this->pageData['title'] = 'Login';
    }

    /**
     * Admin::login()
     *
     */
    public function index()
    {

        $this->pageData['error'] = '';

        if($this->input->post())
        {
            $this->form_validation->set_rules('username', 'Username', 'trim|required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('remember_me','Remember Me');

            if($this->form_validation->run() == TRUE)
            {
                // Login
                if($this->adminauth->login($this->input->post('username'), $this->input->post('password'), $this->input->post('remember_me')))
                {
                    // Redirect
                    if($redir = $this->session->userdata('redir'))
                    {
                        $this->session->unset_userdata('redir');
                    }

                    $login_user = $this->input->post('username');
                    $login_pass = $this->input->post('password');

                    redirect($redir ? $redir : 'dashboard');
                }
                else
                {
                    $this->pageData['error'] = $this->adminauth->get_error();
                }
            }
            else
            {
                $this->pageData['error'] = '<div class="alert alert-danger">'.validation_errors().'</div>';
            }
        }


        $this->load->view('admin/login', $this->pageData);
    }

}