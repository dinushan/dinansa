<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer">
	<div class="row">
		<div class="col-xs-12 breadcrumbCont">

            <div class="breadcrumbWrap">
                <ol class="breadcrumb">
                    <li><a href="<?php echo base_url(); ?>">HOME</a></li>
                    <li class="active"><a href="<?php echo base_url('user/login'); ?>">Login</a></li>
                </ol>
            </div>

		</div>
		<div class="col-xs-12 col-md-8 col-md-offset-2">
			<h4 class="loginTopMessage">Returning users please enter your login details to proceed to book your tickets online. New users can sign up and proceed with booking tickets.</h4>
		</div>
		<div class="col-xs-12 col-md-6 col-md-offset-3 col-lg-4 col-lg-offset-4 loginPanelCont">
			<?php include(APPPATH.'views/frontend/inc/login-template.php'); ?>
		</div>
	</div>
</div>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>