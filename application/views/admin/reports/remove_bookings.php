<script>
    window.result = <?php echo json_encode($result['result']); ?>;
    window.total = <?php echo json_encode($result['total']); ?>;
    window.sources = <?php echo json_encode($result['sources']); ?>;
    window.bookingtypes = <?php echo json_encode($result['booking_types']); ?>;
</script>
<script>
    (function (window) {
        var app = angular.module('cinema');

        app.factory('removebookingService', function ($window, httpService, $httpParamSerializer) {

            var removeBooking = function (booking_id, success, error) {

                httpService.post('admin/reports/remove-bookings/remove', {
                    'booking_id': booking_id
                })
                    .then(function (response) {
                        success(response.data);
                    }, function (response) {
                        error(response);
                    });
            };

            var filter = function (filter,success,error) {
                httpService.get('admin/reports/remove-bookings/filter',filter)
                    .then( function (response) {
                        success(response.data);
                    }, function (error) {
                        error(error);
                    });


            };

            var setPaginations  = function (filters,total_reocrds) {

                var size        = parseInt(filters.size);
                var total_pages = Math.floor(total_reocrds/size);
                var remainder   = (total_reocrds%size);
                var page        = parseInt(filters.page);
                var pages       = [];

                var pageclasses      = Math.ceil((page)/10);
                var nextbtn          = true;
                var previousbtn      = true;
                var totalpageClasses = total_pages/10;


                var lastpg = pageclasses*10;

                if ( total_pages <= pageclasses*10){
                    lastpg = total_pages;
                }

                if (pageclasses == 1){
                    nextbtn = true;
                    previousbtn = false;
                }

                if (pageclasses >= totalpageClasses){
                    nextbtn = false;
                    previousbtn = true;
                }

                var i=(pageclasses*10) -10;

                if (pageclasses*10 -page < 3 && pageclasses*10 -page  > -1){
                    var i=i+3;

                    if (lastpg+3 < total_pages){
                        lastpg = lastpg+3;
                    }

                }
                for ( i; i < lastpg; i++ ){
                    pages.push(i+1);
                }

                return {
                    'pages'       :pages,
                    'currentPage' :page,
                    'next'        :nextbtn,
                    'prev'        :previousbtn,
                    'last_page'   :total_pages
                };

            };

            return {
                'removeBooking': removeBooking,
                'filter': filter,
                'setPaginations':setPaginations
            }
        });

        app.controller('removeBookingController', removeBookingController);

        removeBookingController.$inject = ['$scope', '$window', 'removebookingService'];

        function removeBookingController($scope, $window, removebookingService) {

            $scope.bookings = $window.result;
            $scope.total = $window.total;



            $scope.filters = {
                'page':1,
                'size':10,
                'referenc_no':null,
                'mobile':null

            };

            $scope.pagination = removebookingService.setPaginations($scope.filters,$scope.total);

            var sources       = $window.sources;
            var bookingTypes  = $window.bookingtypes;

            $scope.onebooking = null;

            $scope.getMoveDateTime = function (dt) {
                return moment(dt).format(' YYYY-MM-DD hh : mm A') + ' at ' + moment(dt).format('hh : mm A');
            };

            $scope.getCreatedDate = function (dt) {
                return moment(dt).format('YYYY-MM-DD hh : mm A');
            };

            $scope.goFirst = function (){
                $scope.filters.page = 1;
                filter()
            };

            $scope.goPrev = function () {
                $scope.filters.page = parseInt($scope.filters.page) - 1;
                filter()
            };

            $scope.goPage = function (pg) {
                $scope.filters.page = pg;
                filter()
            };

            $scope.goNext = function () {
                $scope.filters.page = parseInt($scope.filters.page) + 1;
                filter()
            };

            $scope.goLast = function () {
                $scope.filters.page = $scope.pagination.last_page;
                filter()
            };

            $scope.getDate = function(dt){
                return moment(dt).format('DD MMM YYYY');
            };

            $scope.getTotalPrice = function (price){

                var onlineCharge = parseFloat(price.online_charge);
                var tiketPrice   = parseFloat(price.price);
                var qty = parseInt(price.qty);

                return (onlineCharge+tiketPrice)*qty;
            }

            $scope.search = function () {
                filter();
            };

            $scope.getTime = function(dt){
                return moment(dt).format('hh : mm A');
            };

            $scope.getSources = function (dt) {
                return $window.sources[dt];
            };

            $scope.getBookingTypes = function (dt) {
                return $window.bookingtypes[dt];
            };

            $scope.showSummery = function () {
                $scope.onebooking = angular.copy(this.$parent.booking);
                $('#removeModel').modal({'show':true});
            };

            $scope.getCategories = function(cat){

                var ct = {1:'Adult',2:'Child'};

                return ct[cat];
            };

            $scope.removeBooking = function (booking_id) {
                $('#removeModel').modal('hide');

                $window.Metronic.blockUI({
                    target: $("body"),
                    animate: true,
                    overlayColor: 'none'
                });
                removebookingService.removeBooking(booking_id,
                    function (resp) {
                        $scope.bookings = resp.data.result;
                        $scope.total = resp.data.total;
                        $window.Metronic.unblockUI($("body"));

                    },
                    function (err) {
                        $window.Metronic.unblockUI($("body"));
                        console.log(err);
                    }
                );

            };

            var filter = function () {

                $scope.filters.referenc_no = $scope.reference_no;
                $scope.filters.mobile      = $scope.mobile_no;
                $window.Metronic.blockUI({
                    target: $("body"),
                    animate: true,
                    overlayColor: 'none'
                });

                removebookingService.filter($scope.filters,function (resp) {

                    $scope.bookings   = resp.data.result;
                    $scope.filters    = resp.filter;
                    $scope.total      = resp.data.total;
                    $scope.pagination = removebookingService.setPaginations($scope.filters,$scope.total);

                    $window.Metronic.unblockUI($("body"));

                },function (err) {
                    $window.Metronic.unblockUI($("body"));
                });

            };


        }
    }(window));
</script>
<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>
    <div class="page-content-wrapper" ng-controller="removeBookingController">
        <div class="col-md-12 page-content">
            <!-- BEGIN EXAMPLE TABLE PORTLET-->
            <div class="portlet light bordered">
                <div class="portlet-title">
                    <div class="caption font-dark">
                        <i class="icon-settings font-dark"></i>
                        <span class="caption-subject bold uppercase">Remove Bookings</span>
                    </div>
                    <div class="tools"></div>
                </div>
                <div class="portlet-body">
                    <div id="sample_1_wrapper" class="dataTables_wrapper no-footer ">

                        <div class="row">
                            <div class="col-md-2 col-sm-6">
                                <div class="form-group">
                                    <label>
                                        #Ref No : <input type="search"
                                                         class="form-control"
                                                      placeholder="" ng-model="reference_no">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-6">
                                <div class="form-group">
                                    <label>
                                        #Mobile : <input type="search"
                                                         class="form-control"
                                                         placeholder="" ng-model="mobile_no">
                                    </label>
                                </div>
                            </div>
                            <div class="col-md-2 col-sm-6">
                                <div class="form-group">
                                    <label>
                                        <br/>
                                       <button class="btn green filter-cancel" ng-click="search()">
                                           <i class="fa fa-search" aria-hidden="true"></i> Search
                                       </button>
                                    </label>
                                </div>
                            </div>

                        </div>
                        <div class="table-scrollable">
                            <table class="table table-striped table-bordered table-hover dt-responsive dataTable no-footer dtr-inline collapsed"
                                   width="100%" id="sample_1" role="grid" aria-describedby="sample_1_info"
                                   style="width: 100%;">
                                <thead>
                                <tr role="row">
                                    <th class="all sorting_asc" tabindex="0" aria-controls="sample_1" rowspan="1"
                                        colspan="1" aria-sort="ascending"
                                        aria-label="First name: activate to sort column descending">#Ref
                                    </th>
                                    <th class="min-phone-l sorting" tabindex="0" aria-controls="sample_1"
                                        rowspan="1" colspan="1"
                                        aria-label="Last name: activate to sort column ascending">Mobile
                                    </th>
                                    <th class="min-tablet sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                        colspan="1"
                                        aria-label="Position: activate to sort column ascending">Cust Name
                                    </th>
                                    <th class="none sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                        colspan="1"
                                        aria-label="Office: activate to sort column ascending">Movie
                                    </th>
                                    <th class="none sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                        colspan="1"
                                        aria-label="Start date: activate to sort column ascending">Date Time and
                                        Venue
                                    </th>
                                    <th class="all sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                        colspan="1"
                                        aria-label="Extn.: activate to sort column ascending">Created
                                    </th>
                                    <th class="all sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                        colspan="1"
                                        aria-label="Extn.: activate to sort column ascending">Source
                                    </th>
                                    <th class="all sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                        colspan="1"
                                        aria-label="Extn.: activate to sort column ascending">Booking Type
                                    </th>
                                    <th class="all sorting" tabindex="0" aria-controls="sample_1" rowspan="1"
                                        colspan="1"
                                        aria-label="Extn.: activate to sort column ascending">Action
                                    </th>

                                </tr>
                                </thead>
                                <tbody>
                                <tr role="row" class="odd" ng-repeat="booking in bookings">
                                    <td>{{ booking.reference_no}}</td>
                                    <td>{{ booking.mobile_no}}</td>
                                    <td>{{ booking.first_name}} {{booking.last_name}}</td>
                                    <td>{{ booking.movie_name}}</td>
                                    <td>
                                        <p>{{booking.cineam_name}}</p>
                                        <p>{{ booking.theater_name}}</p>
                                        <p>{{getMoveDateTime(booking.time_slot) }}</p>
                                    </td>
                                    <td>{{ getCreatedDate(booking.created_at)}}</td>
                                    <td>{{ getSources(booking.source)}}</td>
                                    <td>{{ getBookingTypes(booking.booking_type)}}</td>
                                    <td>
                                        <button type="button" class="btn btn-sm gray filter-cancel"
                                                ng-if="booking.booking_type < 3" ng-click="showSummery()">
                                            <i class="fa fa-file-text"></i> View
                                        </button>

                                    </td>
                                </tr>

                                </tbody>
                            </table>
                        </div>
                        <div class="row">
                            <div class="col-md-5 col-sm-12">
                                <div class="dataTables_info" id="sample_1_info" role="status" aria-live="polite">
                                    Showing {{pagination.currentPage | number}} to {{pagination.currentPage+9 | number}} of {{total | number}} entries
                                </div>
                            </div>
                            <div class="col-md-7 col-sm-12">
                                <div class="dataTables_paginate paging_bootstrap_number" id="sample_1_paginate">
                                    <ul class="pagination" style="visibility: visible;">
                                        <li ng-class="pagination.prev ? '': 'disabled'" class="prev" ng-click="goFirst()">
                                            <a title="First">
                                                <i class="fa fa-angle-double-left"></i>
                                            </a>
                                        </li>
                                        <li ng-class="pagination.prev ? '': 'disabled'" class="prev" ng-click="goPrev()">
                                            <a title="Prev">
                                                <i class="fa fa-angle-left"></i>
                                            </a>
                                        </li>
                                        <li ng-class="pg == pagination.currentPage ? 'active':''" ng-repeat="pg in pagination.pages" ng-click="goPage(pg)">
                                            <a href="#">{{pg}}</a>
                                        </li>
                                        <li ng-class="pagination.next ? '': 'disabled'" class="next" ng-click="goNext()">
                                            <a title="Next">
                                                <i class="fa fa-angle-right"></i>
                                            </a>
                                        </li>
                                        <li ng-class="pagination.next ? '': 'disabled'" class="next">
                                            <a title="Last" ng-click="goLast()">
                                                <i class="fa fa-angle-double-right"></i>
                                            </a>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <!-- END EXAMPLE TABLE PORTLET-->

            <div class="modal fade bd-example-modal-lg" id="removeModel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLongTitle">Booking Summery</h5>
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <form>
                                <div class="form-group">
                                    <p class="text-lg-left"><b>Reference No:</b> {{onebooking.reference_no}}</p>

                                </div>

                                <div class="form-group">
                                    <p class="text-lg-left"><b>User Name:</b> {{onebooking.first_name}} {{onebooking.last_name}}</p>
                                </div>
                                <div class="form-group">
                                    <p class="text-lg-left"><b>User Mobile:</b> {{onebooking.mobile_no}}</p>
                                </div>
                                <div class="form-group">
                                    <p class="text-lg-left"><b>Location:</b> {{onebooking.cineam_name}}, {{onebooking.theater_name}}</p>
                                </div>
                                <div class="form-group">
                                    <p class="text-lg-left"><b>Movie Date:</b> {{getDate(onebooking.time_slot)}}</p>
                                </div>
                                <div class="form-group">
                                    <p class="text-lg-left"><b>Movie Time:</b> {{getTime(onebooking.time_slot)}}</p>
                                </div>
                                <div class="form-group">
                                    <p class="text-lg-left"><b>Move:</b> {{onebooking.movie_name}}</p>
                                </div>
                                <div class="form-group">
                                    <p class="text-lg-left"><b>Seats:</b> {{onebooking.seats}}</p>
                                </div>
                                <div class="form-group">
                                    <p class="text-lg-left"><b>Booking Type:</b> {{getBookingTypes(onebooking.booking_type)}}</p>
                                </div>
                                <div class="form-group">
                                    <p class="text-lg-left"><b>Source:</b> {{getSources(onebooking.source)}}</p>
                                </div>
                                <div class="form-group">
                                    <p class="text-lg-left"><b>Booking Made:</b> {{getDate(onebooking.created_at)}} On {{getTime(onebooking.created_at)}}</p>
                                </div>
                                <div class="form-group">
                                    <p class="text-lg-left"><b>Price Details:</b></p>
                                    <ul class="list-group">
                                        <li class="list-group-item" ng-repeat="price in onebooking.price_category">
                                            <p><b>Online Charge: </b> {{price.online_charge | currency :"Rs "}}</p>
                                            <p><b>Tiket Price: </b> {{price.price | currency :"Rs "}}</p>
                                            <p><b>Category: </b> {{getCategories(price.price_category)}}</p>
                                            <p><b>qty: </b> {{price.qty}}</p>
                                            <p><b>Total Price: </b> {{getTotalPrice(price)|currency :"Rs "}}</p>
                                        </li>
                                    </ul>
                                </div>

                            </form>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-danger uppercase" ng-click="removeBooking(onebooking.booking_id)">
                                <i class="fa fa-trash-o"></i> Remove This Booking
                            </button>
                            <button type="button" class="btn dark uppercase" data-dismiss="modal">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>


        </div>
    </div>

