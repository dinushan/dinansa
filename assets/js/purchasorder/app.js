/**
 * Created by LAYOUTindex on 11/23/2017.
 */
(function(window) {
    var app = angular.module('restaurant', []);

    app.directive('select2', function (){
        return{
            scope:{
                'selected':"=ngModel",
                'options':'@ngOptions'
            },
            compile:function compile(tElement, tAttrs, transclude) {

                return {
                    post: function preLink(scope, iElement, iAttrs, controller) {

                        console.log("compiled")
                        var result = scope.options.split(/(track by|in|for|as)/);
                        //console.log(result[4],result[8],result[6]);

                        var objlist = scope.$parent[result[6].trim()];



                        $(iElement).select2({ width: 'auto' });

                        $(iElement).on('change',function () {

                              var selecteid = $(iElement).val();
                            for (var i in objlist){
                                if (objlist[i].id == selecteid){
                                    scope.$apply(function(){
                                        scope.selected = objlist[i];
                                    });

                                    break;
                                }
                            }
                        });

                    }
                }
            }
        }
    });

    app.factory('purchaseOrderService',function($window){
        var choosedItmes = [];

        var choosableItems = [];

        var addItem = function () {
            // choosableItems = [];
            // for (var i in $window.items){
            //     if ( choosedItmes.indexOf($window.items[i].id) > -1 ){
            //
            //         continue;
            //     }
            //
            //     choosableItems.push($window.items[i]);
            // }
            //
            //
            // choosedItmes.push(choosableItems[0].id);
            return {

                'discount':0,
                'qty':1,
                'chooseitem':$window.items[0]
            }
        };

        var setSupplier = function (){

            var supplier = window.suppliers[0];
            if($window.purchaseinvoice){

                for (var i in $window.suppliers){
                    if ($window.suppliers[i].id == $window.purchaseinvoice.supplier_id){
                        supplier = $window.suppliers[i];
                        break;
                    }
                }
            }

            return supplier;
        };

        var getSelectedItems = function (){

            var selected = [];
            for (var i in $window.purches_items){

                for (var j in $window.items){

                    if ($window.items[j].id == $window.purches_items[i].item_id){
                        selected.push({
                            'discount':parseFloat($window.purches_items[i].discount_val),
                            'qty':$window.purches_items[i].qty_in_hand,
                            'chooseitem':$window.items[j]
                        });
                        //break loop2;
                    }
                }

                choosedItmes.push($window.purches_items[i].item_id);
            }

            return selected;

        };

        var removeItem = function (obj) {
            // choosedItmes.splice( choosedItmes.indexOf(obj.selectitem.id),1);
            //
            //
            // for (var i in $window.items){
            //     if ( choosedItmes.indexOf($window.items[i]) > -1 ){
            //
            //         continue;
            //     }
            //
            //     choosableItems.push($window.items[i]);
            // }
        };

        var getChoosableItems = function () {
            return  choosableItems;
        }

        return {
            'additem':addItem,
            'removeItem':removeItem,
            'getChoosableItems':getChoosableItems,
            'getSelectedItems':getSelectedItems,
            'setSupplier':setSupplier
        }
    });
}(window));