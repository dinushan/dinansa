
<style>
.content-holder{
    max-height:100px;
    overflow:hidden;

}
</style>


<!-- Career Commit here-->
<!-- Career Commit - 2 -->
<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <div class="page-content">

            <h3 class="page-title">Career Manage</h3>

            <div class="page-bar">
                <div class="page-toolbar">
                    <div class="btn-group pull-right">

                            <a onclick="setLocation('<?php echo site_url('admin/' . $slug . '/manage') ?>')" class="btn green"><i
                                    class="fa fa-plus-circle"></i> Add New</a>

                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-12">
                    <div class="table-container">
                        <table class="table table-striped table-bordered table-hover" id="tbl">
                            <thead>
                                <tr>
                                    <th>ID</th>
                                    <th>Heading</th>
                                    <th>Status</th>
                                    <th>Last Update</th>
                                    <th>Creat date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php foreach($careers as $career):?>
                                    <tr id="<?php echo $career->id ?>">
                                        <td class="dragHandle"><?php echo $career->id ?></td>
                                        <td><?php echo $career->title?></td>
                                        <td><?php echo $career->ispublished == 1 ? 'publish':'pending' ?></td>
                                        <td><?php echo $career->updated_at?></td>
                                        <td><?php echo $career->created_at ?></td>
                                        <th>
                                            <a href="<?php echo base_url('admin/CareerManager/manage/'.$career->id)?>" class="btn btn-sm default btn-xs"><i class="fa fa-edit"></i>Edit</a>
                                            <a onclick="deleteConfirm('Are you sure you want to delete?','Confirm Delete','<?php echo base_url('admin/CareerManager/delete/'.$career->id)?>',false);" class="btn btn-sm red btn-xs">
                                                <i class="fa fa-trash"></i> Delete
                                            </a>
                                        </th>
                                    </tr>    
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

        </div>
    </div>
</div>


    <script type="text/javascript">
        jQuery(document).ready(function(e) {
            jQuery('#tbl').tableDnD({
                onDragStart:function(table){
                    jQuery(table).find('tr').removeClass('even');
                },
                onDrop: function(table, row) {
                    var order = jQuery(table).tableDnDSerialize();
                    jQuery.post("<?php echo site_url('admin/CareerManager/sorting') ?>", order, function(theResponse){

                    });
                    jQuery('#tbl1 tbody tr:even').addClass('even');
                },
                dragHandle:'.dragHandle'
            });
        });

    </script>