<!-- BEGIN CONTAINER -->

<div class="page-container">
	<?php $this->load->view('admin/inc_sidebar_navigation.php');?>
	
	<!-- BEGIN CONTENT -->
	<div class="page-content-wrapper">
		<div class="page-content">
			<?php if($page_action == ''):?>
			<h3 class="page-title">Manage Deals and Exclusive</h3>
			<div class="page-bar">
				<div class="page-toolbar">
					<div class="btn-group pull-right"> <a onclick="setLocation('<?php echo site_url('admin/'.$slug.'/add'); ?>')" class="btn green"><i class="fa fa-plus-circle"></i>Add Deals and Exclusive</a> </div>
				</div>
			</div>
			<?php echo get_message() ?>
			<div class="row">
				<div class="col-md-12">
					<div class="table-scrollable">
						<?php if($this->adminauth->has_role('1301')):?>
						<form action="<?php echo current_url() ?>" method="get" id="frmSearch">
						<table id="tbl" class="table table-striped table-bordered table-advance table-hover">
							<colgroup>
							<col width="80" />
							<col />
							<col width="250"  />
							</colgroup>
							<thead>
								<tr class="headings nodrop nodrag">
									<th>ID</th>
									<th>Title</th>
									<th>Action</th>
								</tr>
							</thead>
							<tr class="filter nodrop nodrag">
								<th><div class="field-100">
									
								</div></th>
								<th><div class="field-100">
									<input type="text" class="form-control" value="<?php echo $this->input->get('title') ?>" name="title">
								</div></th>
								<th colspan="2" class=" no-link last">
									<button onclick="frmSearch.submit();" class="btn btn-red btn-small" type="button"><span>Search</span></button>
									<button onclick="setLocation('<?php echo site_url('admin/'.$slug.'/index'); ?>')" class="btn btn-red btn-small "
									type="button"><span>Reset</span></button><input name="q" type="hidden" value="search" />
								</th>
							</tr>
							<tbody>
								<?php 
									$edit_url = site_url('admin/'.$slug.'/edit');
									$delete_url = site_url('admin/'.$slug.'/delete');
									
										foreach($deal_List as $_page):
										$id = $_page['id'];
                                    	$encry_hcId = $this->encrypt_lib->encode($id);										
                                   ?>
								<tr id="<?php echo $encry_hcId ?>">
									<td><?php echo $id ?></td>
									<td><?php echo $_page['deal_name'] ?></td>
									<td colspan="2" align="center"><a class="btn btn-sm default btn-xs" href="<?php echo $edit_url.'/'.$encry_hcId ?>"><i class="fa fa-edit"></i> Edit</a>
										<button class="btn btn-sm red btn-xs" type="button" 
											onclick="deleteConfirm('Are you sure you want to delete?','Confirm Delete',
											'<?php echo $delete_url.'/'.$encry_hcId ?>');return false;"><i class="fa fa-trash-o"></i> Delete</button></td>
								</tr>
								<?php endforeach; ?>
							</tbody>
						</table>
						</form>
						<?php endif; ?>
					</div>
				</div>
			</div>					
			<?php 
			echo $pagination_links;
			
			
			elseif($page_action == 'add' || $page_action == 'edit'): ?>
			<h3 class="page-title"><?php
				if($page_action == 'add'){
					echo 'Add Deals and Exclusive';
				}else{
					echo 'Edit "'.$deals_and_exclusive_data->deal_name.'"';
				}
				?></h3>
			<div class="page-bar">
				<div class="page-toolbar">
					<div class="btn-group pull-right"> <a class="btn default" href="<?php echo site_url('admin/'.$slug.'/index/') ?>"><i class="fa fa-chevron-left"></i> Back</a> </div>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12"> <?php echo get_message() ?> 
					<!-- BEGIN VALIDATION STATES-->
					<div class="portlet light bordered">
						<div class="portlet-body form"> 
							<!-- BEGIN FORM-->
							<?php                    
							$attr = array("id"=>"edit_form","class"=>"form-horizontal","enctype"=>"multipart/form-data");
							echo form_open(current_url(),$attr);
							?>
							<?php
								if($page_action == 'edit'){

									$id 				= $deals_and_exclusive_data->id;
									$deal_name 			= $deals_and_exclusive_data->deal_name;
									$description 		= $deals_and_exclusive_data->description;
									$short_description 	= $deals_and_exclusive_data->short_description;
									$image 				= $deals_and_exclusive_data->image;
									$publish            = $deals_and_exclusive_data->publish;
									
								}else{
									$deal_name 		    = '';
									$description 		= '';
									$short_description  = '';
									$image	 			= '';
                                    $publish            = 0;
									$required_entry = ' required-entry';
									
								}
								
							?>
							<h4 class="form-section">General</h4>
							<div class="form-group">
								<label class="control-label col-md-3">Title<span class="req">*</span></label>
								<div class="control-element col-md-4">
									<input name="title" type="text" class="form-control required-entry" value="<?php echo set_value('title',$deal_name) ?>" />
									<?php echo form_error('title'); ?> </div>
							</div>

							<?php if($page_action == 'edit' && $image!= ''):?>
							<div class="form-group">
								<div class="control-label col-md-3">&nbsp;</div>
								<div class="control-element col-md-4">
									<table width="100%" border="0" cellspacing="0" cellpadding="0">
										<tr>
											<td><img src="<?php echo base_url().DEALS_IMG_PATH.$image ?>?<?php echo time() ?>" alt="" width="250" height="380" /></td>
											<td>&nbsp;</td>
											<td><?php list($img_width, $img_height) = getimagesize(DEALS_IMG_PATH.$image);?>
												<strong>Image Info</strong><br />
												width:<?php echo $img_width ?>px<br />
												height:<?php echo $img_height ?>px</td>
										</tr>
										<tr>
											<td colspan="3">&nbsp;
												<input type="hidden" name="image" value="<?php echo $image ?>" /></td>
										</tr>
										<tr>
											<td colspan="3"><label>
													<input name="img_del" type="checkbox" value="1" />
													Image Delete </label>
										</tr>
										<tr>
											<td colspan="3">&nbsp;</td>
										</tr>
									</table>
								</div>
							</div>
								<?php else:?>
								<div class="form-group">
									<label class="control-label col-md-3">Image</label>
									<div class="control-element col-md-6">
										<input type="file" class=" input-file" name="image">
										<span class="help-block font-green">Image Size:<?php echo DEALS_IMG_W ?>px &times; <?php echo DEALS_IMG_H ?>px, Image Type: jpg</span> </div>
								</div>
							<?php endif;?>

							<div class="form-group">
								<label class="control-label col-md-3">Short Description</label>
								<div class="control-element col-md-6">
											<textarea cols="15" rows="4" class="form-control" name="short_description"><?php echo $page_action == "add" ?
													set_value('short_description') :
													set_value('short_description',$short_description) ?></textarea>
								</div>
							</div>

							<div class="form-group">
								<label class="control-label col-md-3">Description<span class="req">*</span></label>
								<div class="control-element col-md-6">
									<textarea id="description_textarea" cols="15" rows="10" class="tinymce_editor" name="description"><?php echo $page_action == "add" ?
											set_value('description') :
											set_value('description',$description) ?></textarea>
									<?php echo form_error('description'); ?> </div>
							</div>

                            <div class="form-group">
                                <label class="control-label col-md-3"></label>
                                <div class="control-element col-md-6">
                                    <input type="checkbox" name="publish" class="form-control" value="1" <?php echo $publish ? 'checked="checked"' : '' ?>"/> <label>Publish</label>

                                </div>
                            </div>

							<div class="form-actions">
								<div class="row">
									<div class="col-md-offset-3 col-md-9">
										<button type="submit" class="btn green">Submit</button>
									</div>
								</div>
							</div>
							<script type="text/javascript">							 
								$('.required-entry').attr('required','required');					
								$('#edit_form').validate({
									errorElement: 'span', 
									errorClass: 'validation-advice', 								
									ignore: "",
									submitHandler: function (form) {
										form.submit();										
									}
								});	
								
								$('.datepicker').datepicker({
									format: 'yyyy-mm-dd',
									 orientation: "left",
                					autoclose: true
									});
												
							</script>
							</form>
						</div>
					</div>
				</div>
			</div>
			<?php endif;//$page_action ?>
		</div>
		<!-- END PAGE CONTENT--> 
	</div>
	<!-- END CONTENT --> 
	
</div>
<!-- END CONTAINER --> 

