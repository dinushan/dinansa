<!-- BEGIN CONTAINER -->


<script src="<?php echo base_url() ?>angular/admin/reports/sales-report-service.js" type="text/javascript"></script>
<script src="<?php echo base_url() ?>angular/admin/reports/sales-report.js" type="text/javascript"></script>

<script>

    window.period               = <?php echo json_encode($period); ?>;
    window.from                 = <?php echo json_encode($from); ?>;
    window.to                   = <?php echo json_encode($to); ?>;
    window.theaters             = <?php echo json_encode($theaters); ?>;
    window.movies               = <?php echo json_encode($movies); ?>;
    window.theater              = <?php echo json_encode($theater);?>;
    window.movie                = <?php echo json_encode($movie); ?>;
    window.counter              = <?php echo json_encode($counter); ?>;
    window.online               = <?php echo json_encode($online); ?>;
    window.fyi                  = <?php echo json_encode($fyi); ?>;
    window.results              = <?php echo json_encode($results); ?>;
    window.time_slots           = <?php echo json_encode($time_slots); ?>;
    window.time_slot            = <?php echo json_encode($time_slot); ?>;
    window.ticket_type          = <?php echo json_encode($ticket_type); ?>;
    window.booking_method       = <?php echo json_encode($booking_method); ?>;
    window.total                = <?php echo json_encode($total); ?>;
    window.report_type          = <?php echo json_encode($report_type); ?>;
    window.cur_route            ="<?php echo current_url();?>";
    window.page                 = <?php echo json_encode($page); ?>;
    window.size                 = <?php echo json_encode($size); ?>;
    window.total_records        = <?php echo json_encode($total_records); ?>;
    window.source               = <?php echo json_encode($source); ?>;
    window.available_source     = <?php echo json_encode($available_source); ?>;
</script>
<div class="page-container">
    <?php $this->load->view('admin/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper" ng-controller="salesReportController">
        <div class="page-content">
            <h3 class="page-title">Sales Report</h3>

            <?php if(
            $this->adminauth->has_role('1103')
            ):?>

            <div class="row">
                <div class="col-md-12 col-sm-12">
                    <form class="form-horizontal">
                    <div class="portlet box blue">
                        <div class="portlet-title">
                            <div class="caption">Filter</div>
                            <div class="tools">
                                <button class="btn btn-danger btn-sm" ng-click="onChangeParam()">Show Report</button>
                            </div>
                        </div>
                        <div class="portlet-body ">
                            <div class="form-body clearfix">

                                <div class="form-group">
                                    <div class="col-md-3"><label class="control-label" for="sales_report_from">Report Type<span class="required">*</span></label></div>
                                    <div class="col-md-9">
                                        <div class="mt-radio-inline">
                                            <input type="radio" name="report_type" class="form-control form-control-inline input-medium " ng-change="onChangeParam()"  ng-value=1 ng-model="param.report_type"  >Reserve Date (Sales Report)
                                            <input type="radio" name="report_type" class="form-control form-control-inline input-medium " ng-change="onChangeParam()"  ng-value=2 ng-model="param.report_type" >Purchase Date (Cash report)
                                        </div>

                                    </div>
                                </div>




                                <div class="form-group">
                                    <div class="col-md-3"><label class="control-label" for="sales_report_period_type">Period</label></div>
                                    <div class="col-md-9">
                                        <select name="period" class="form-control input-sm" ng-options="period as period.name for period in periods track by period.id" ng-model="param.selectedperiod" ng-change="onChangeParam()">

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"><label class="control-label" for="sales_report_from">From <span class="required">*</span></label></div>
                                    <div class="col-md-9"><input id="report_from" type="text" class="form-control form-control-inline input-medium date-picker required-entry  input-sm" name="from" placeholder="From" ng-model="param.from" /></div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"><label class="control-label" for="sales_report_to">To <span class="required">*</span></label></div>
                                    <div class="col-md-9"><input id="report_to" type="text" class="form-control form-control-inline input-medium date-picker required-entry  input-sm" name="to" placeholder="To" ng-model="param.to" /></div>
                                </div>


                                <div class="form-group">
                                    <div class="col-md-3"><label class="control-label" for="theater">Theater</label></div>
                                    <div class="col-md-9">
                                        <select class="form-control input-sm" ng-change="onChangeParam()"  ng-options="theater as theater.name for theater in param.theaters track by theater.theater_id" ng-model="param.theater"></select>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3"><label class="control-label" for="movie">Movie</label></div>
                                    <div class="col-md-9">
                                        <select class="form-control input-sm" ng-change="onChangeParam()" ng-options="movie as movie.movie_name for movie in param.movies track by movie.movie_id" ng-model="param.movie"></select>

                                    </div>
                                </div>



                                <div class="form-group">
                                    <div class="col-md-3"><label class="control-label" for="time_slot">Time Slot</label></div>
                                    <div class="col-md-9">
                                        <select class="form-control input-sm" ng-change="onChangeParam()" ng-options="time_slot as time_slot.value for time_slot in param.time_slots track by time_slot.id" ng-model="param.time_slot"></select>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3"><label class="control-label" for="tiket_type">Payment Type</label></div>
                                    <div class="col-md-9">

                                        <select class="form-control input-sm" ng-change="onChangeParam()" ng-options="ticket as ticket.name for ticket in param.ticket_types track by ticket.id" ng-model="param.ticket_type">

                                        </select>

                                    </div>
                                </div>

                                <div class="form-group">
                                    <div class="col-md-3"><label class="control-label" for="movie">Method</label></div>
                                    <div class="col-md-9">
                                        <select class="form-control input-sm" ng-change="onChangeParam()" ng-options="p_method as p_method.name for p_method in param.payment_methods track by p_method.id" ng-model="param.booking_method">
                                        </select>

                                    </div>
                                </div>
                                <!--div class="form-group">
                                    <div class="col-md-3"><label class="control-label" for="time_slot">Time slot</label></div>
                                    <div class="col-md-9">

                                    </div>
                                </div-->


                                <div class="form-group">
                                    <div class="col-md-3"><label class="control-label" for="show_booking_types">Booking type</label></div>
                                    <div class="col-md-9">
                                        <div class="mt-checkbox-inline">
                                            <label class="mt-checkbox">
                                                <input type="checkbox" ng-change="onChangeParam()" ng-model="param.counter" ng-true-value="true" ng-false-value="false" /> Counter
                                            </label>

                                            <label class="mt-checkbox">
                                                <input type="checkbox" ng-change="onChangeParam()" ng-model="param.online" ng-true-value="true" ng-false-value="false" /> Online
                                            </label>

                                            <label class="mt-checkbox">
                                                <input type="checkbox" ng-change="onChangeParam()" ng-model="param.fyi" ng-true-value="true" ng-false-value="false" /> FYI
                                            </label>

                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-md-3"><label class="control-label" for="show_booking_types">Source</label></div>
                                    <div class="col-md-9">
                                        <select class="form-control input-sm" ng-change="onChangeParam()" ng-options="p_source as p_source.name for p_source in param.available_source track by p_source.id" ng-model="param.source">
                                        </select>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>
                    </form>

                    <div class="table-container" style="overflow: scroll;">
                        <div class="row">
                            <div class="col-md-12">
                                <button id="exportCVS1910" type="button" class="btn btn-default pull-right" ng-click="exportCVS()">Export to Excel</button>
                            </div>
                        </div>

                        <table id="tbl-sales" class="table table-striped table-bordered table-hover table-responsive">
                            <thead>
                            <tr class="headings">
                                <th>Period</th>
                                <th>Theater</th>
                                <th>Movie</th>
                                <th>Time slot</th>
                                <th>Booking Type</th>
                                <th>Source</th>
                                <th>Booking Method</th>
                                <th>Ticket Type</th>
                                <th>Tickets bought</th>
                                <th>Category</th>
                                <th>DR Collection</th>
                                <th>Total</th>
                                <th>FYI Share</th>
                                <th>Discount</th>
                            </tr>
                            </thead>
                            <tbody>

                            <tr ng-if="param.results.length > 0" ng-repeat="result in results">
                                <td ng-if="param.selectedperiod.id == 1">{{result.period | date : 'yyyy-MM-dd'}}</td>
                                <td ng-if="param.selectedperiod.id == 2">{{result.period | date : 'yyyy, MMM'}}</td>
                                <td ng-if="param.selectedperiod.id == 3">{{result.period | date : 'yyyy'}}</td>
                                <td> <p ng-if="param.theater.theater_id == -1">---</p><p ng-if="param.theater.theater_id != -1">{{result.theater_name}}</p></td>
                                <td><p ng-if="param.movie.movie_id == -1">--</p> <p ng-if="param.movie.movie_id != -1">{{result.movie_name}}</p></td>
                                <td>{{getTimeSlot(result.time_slot)}}</td>
                                <td>{{getBookingType(result.booking_type)}}</td>
                                <td>{{getSource(result.source) }}</td>
                                <td>{{getBookingMethod(result.booking_method) }}</td>
                                <td>{{getTicketType(result.payment_method_id) }}</td>
                                <td>{{result.tickets_bought | number }}</td>
                                <td>{{result.rate_category}}</td>
                                <td>{{result.dr_collection | number :2}}</td>
                                <td>{{result.total_amount | number :2}}</td>
                                <td>{{ getFYIShare(result)| number :2}}</td>
                                <td>{{result.total_discount_amount | number :2}}</td>
                            </tr>

                                <tr ng-if="param.results.length < 1">
                                    <td colspan="12" align="center">No records found.</td>
                                </tr>

                            </tbody>
                            <tfoot>
                              <tr>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th></th>
                                  <th>{{param.total.tickets_bought | number}}</th>
                                  <th></th>
                                  <th>{{param.total.dr_total | number :2}}</th>
                                  <th>{{param.total.total_amount | number :2}}</th>
                                  <th></th>
                                  <th>{{param.total.total_discount_amount | number :2}}</th>
                              </tr>
                            </tfoot>
                        </table>


                        <div class="row">
                            <div class="col-md-8 col-sm-12">
                                <ul class="pagination">
                                    <li ng-repeat="page in param.pages" ng-class="page == param.page ?'active' :''"  ng-click="paging(page)"><a>{{page}}</a></li>

                                </ul>
                            </div>
                            <div class="col-md-4 col-sm-12">
                                Size <select class="form-control input-sm" ng-options="size for size in param.sizes" ng-model="param.size" ng-change="onChangeParam()"></select>
                            </div>

                        </div>

                    </div>


                </div>
            </div>


            <?php endif; ?>
        </div>
        <!-- END PAGE CONTENT-->
    </div>
    <!-- END CONTENT -->
    <iframe name="sales_report_download" style="display: none;"></iframe>
</div>
<!-- END CONTAINER -->