<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer mainPageContainer">
   <div class="row">
      <div class="col-xs-12 breadcrumbCont">
         <?php include(APPPATH.'views/frontend/inc/breadcrumb.php'); ?>
      </div>
      <div class="col-xs-12 headWrap">
         <h2 class="heading"><?php echo $sitePageData->title1; ?></h2>
      </div>
      <div class="col-xs-12 searchCont">
         <div class="searchWrap">
            <div class="searchFormWrap">
               <form class="searchForm form-inline">
                  <div class="form-group">
                     <label>Search Movies by</label>
                  </div>
                  <div class="form-group calendarWrap">
                     <input type="text" class="form-control input-scope searchMovieDate" placeholder="Date" value="<?php echo date('d M Y') ?>">
                     <span class="icon calendar-icon"></span>
                  </div>
                  <div class="form-group">
                      <div class="movie_select">
                      <select class="form-control input-scope selectMovie">
                        <option value="">Movie</option>
                     </select>
                      </div>
                  </div>
                  <div class="form-group">
                      <div class="theater_select">
                     <select class="form-control input-scope selectTheater">
                        <option value="">Theatre</option>
                     </select></div>
                  </div>
                  <div class="form-group">
                     <button type="button" onclick="getSearchUrl()" class="btn btn-scope">Search</button>
                  </div>
               </form>
            </div>
            <div>
            </div>
         </div>
      </div>
   </div>
</div>

    <div class="pageMiddle">
        <div class="container-fluid">

            <?php
            if($movie_times != false) {
            foreach ($movie_times as $mvie) {



                foreach ($mvie['movies'] as $movie):

                ?>

                <div class="row repeatableRow movieTimesRow">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-6 col-md-3 moviesCont">
                                <div class="moviesWrap">
                                    <picture class="img-responsive coverImage">
                                        <source srcset="<?php echo $movie['portrait_image'] ?>"
                                                media="(min-width: 800px)">
                                        <img srcset="<?php echo $movie['portrait_image'] ?>" alt="…">
                                    </picture>
                                    <a href="<?php echo base_url('movie/'.$movie['movie_url_key']); ?>" class="imageOverlay"></a>
                                    <div class="captionWidgetWrap">
                                        <div class="captionTitleWrap">
                                            <h3 class="captionTitle"><?php echo $movie['movie_name'] ?></h3>
                                        </div>
                                        <div class="releaseDate">Release <?php
                                            $timestamp = strtotime($movie['date_release']);
                                            $newDate = date('d S F Y', $timestamp);
                                            echo $newDate; ?></div>
                                        <div class="iconContainer clearfix">
                                            <div class="iconWrapper">
                                                <ul class="iconWrap">
                                                    <li><a href="<?php echo $movie['trailer'] ?>"
                                                           class="youTubePopUp"><i class="icon fa fa-youtube-play"
                                                                                   aria-hidden="true"></i><span
                                                                class="icoText">Trailer</span></a>
                                                    </li>
                                                    <li>
                                                        <a href="<?php echo base_url('movie/'.$movie['movie_url_key']); ?>"><i
                                                                class="icon fa fa-info" aria-hidden="true"></i><span
                                                                class="icoText">Info</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-md-3 col-md-push-6 amentiesCont">
                                <div class="dis-table detailTop detailTopRight">
                                    <div class="dis-t-cell hallDetailWrap">
                                        <h3 class="cinemaHall visible-xs visible-sm"><?php echo $mvie['theater_name'] ?></h3>
                                        <div class="detail detailAmenities">Theatre Amenities</div>
                                    </div>
                                </div>
                                <div class="detailBody">
                                    <div class="detailRow">
                                        <table class="detailTable amenitiesTable">
                                            <tbody>
                                            <?php
                                            foreach ($mvie['facilities'] as $fac) {
                                                ?>
                                                <tr>
                                                    <td>
                                                        <div class="detailSpan"><span class="amenitiesIcon"><img
                                                                    class="img-responsive icon"
                                                                    src="<?php echo $fac['facility_icon']; ?>" alt=""></span>
                                                        </div>
                                                    </td>
                                                    <td>
                                                        <div class="detailSpan"><span
                                                                class="amenitiesText"><?php echo $fac['facility_name']; ?></span>
                                                        </div>
                                                    </td>
                                                </tr>
                                                <?php
                                            }
                                            ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-md-6 col-md-pull-3">
                                <div class="dis-table detailTop detailTopLeft">
                                    <div class="dis-t-cell hallDetailWrap">
                                        <h3 class="cinemaHall hidden-xs hidden-sm"><?php echo $mvie['theater_name'] ?></h3>
                                        <div class="detail detailDate">Reservation from <?php
                                            $booking_start_spam = strtotime($mvie['booking_start_date']);
                                            $booking_start_date = date('d S F Y', $booking_start_spam);
                                            echo $booking_start_date; ?></div>
                                    </div>
                                </div>
                                <div class="detailBody">
                                    <div class="detailRow">
                                        <table class="detailTable">
                                            <tbody>

                                            <?php
                                            $enc_theater_id = $this->encrypt_lib->encode($mvie['id']);
                                            foreach ($movie['time_slot'] as $slots) {
                                                if ($slots['full_time_slot'] > $available) {
                                                    ?>
                                                    <tr>
                                                        <td>
                                                            <div class="detailSpan">
                                                                <span
                                                                    class="time"><?php echo $slots['time_slot']; ?></span>
                                                            </div>
                                                        </td>
                                                       <!-- <td>

                                                            <div class="detailSpan">
                                                  <span
                                                      class="lebelPrice"><?php /*echo $slots['prices_object']['0']->category_name; */?>
                                                      starting at</span>
                                                  <span
                                                      class="price">Rs. <?php /*echo $slots['prices_object']['0']->price; */?></span>
                                                            </div>

                                                        </td>
                                                        <td>

                                                            <div class="detailSpan">
                                                                <a data-theater-id="<?php /*echo $mvie['id'] */?>"
                                                                   data-movie-id="<?php /*echo $movie['id'] */?>"
                                                                   data-full-time-slot="<?php /*echo $slots['full_time_slot'] */?>"
                                                                   href=".popUpBoxWap" class="popup-box">
                                                                    <i class="icon fa fa-info"
                                                                       aria-hidden="true"></i></a>
                                                            </div>

                                                        </td>-->
                                                        <td>

                                                            <div class="detailSpan">
                                                                <a href="<?php echo base_url('buy-tickets-online/?movie=' .$this->encrypt_lib->encode($movie['id']) . '&theater=' . $enc_theater_id . '&time=' . $slots['time_slot']).'&date='.$movie['date_release'] ?>"
                                                                   class="btn btn-scope">BOOK</a>
                                                            </div>

                                                        </td>
                                                    </tr>
                                                    <?php
                                                }
                                            }
                                            ?>

                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div><!-- -repeat row end -->
                    <?php
                    endforeach;

            }
            }
            else {
            ?>
            <div class="row">
              <div class="container">
                <div class="row">
                  <div class="col-xs-12 moviesErrorWrap">
                    <div class="alert alert-danger" role="alert">
                        <h3 class="text-wrap"><span class="icon"><i class="fa fa-meh-o" aria-hidden="true"></i></span><span class="text">No Movies Available</span></h3>
                    </div>
                  </div>
                </div>
              </div>
            </div>
                
            <?php
            }
            ?>
        </div>
    </div>

<div  class="popUpBoxWap movieTimeRatesBox white-popup-block mfp-hide">


</div>


<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>
<script>
  $(function() {
  $('.popup-box').magnificPopup({
    type: 'inline',

    fixedContentPos: false,
    fixedBgPos: true,

    overflowY: 'auto',

    closeBtnInside: false,
    preloader: false,
    
    midClick: true,
    removalDelay: 300,
    mainClass: 'my-mfp-zoom-in',
       callbacks: {
             beforeOpen: function() {
               var magnificPopup = $.magnificPopup.instance;
               var cur = magnificPopup.st.el;
               
               var movieId=$(cur).data('movie-id');
               var theaterId=$(cur).data('theater-id');
               var fullTimeSlot=$(cur).data('full-time-slot');

               getMovieTimesDetails(theaterId,movieId,fullTimeSlot);
             }
           }
  });

        var today = '<?php echo date('Y-m-d') ?>';
        getMoviesOnDate(today);

        var today = '<?php echo date('Y-m-d') ?>';
        getMoviesOnDate(today);

});
</script>