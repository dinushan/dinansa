<!-- BEGIN CONTAINER -->
<script>
    window.moviedata      = <?php echo json_encode($theater_list); ?>;
    window.paymentmethods = <?php echo json_encode($paymentmethods); ?>;
</script>
<script src="<?php echo base_url() ?>angular/angular.js?v=2.1"></script>
<script src="<?php echo base_url(); ?>angular/moment.js?v=2.1" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/angular-moment.min.js?v=2.1" type="text/javascript"></script>
<script src="<?php echo $this->config->item('socket_url')?>/socket.io/socket.io.js"></script>


<script src="<?php echo base_url(); ?>angular/counter/app.js?v=2.1" type="text/javascript"></script>

<script src="<?php echo base_url(); ?>angular/counter/services/counterservice.js?v=2.1.1" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/counter/controller/mainController.js?v=2.1" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/counter/controller/filterController.js?v=2.1.2" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/counter/controller/seatPlanController.js?v=2.1.1" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>angular/counter/directive/directive.js?v=2.1" type="text/javascript"></script>

<style>
    .availability-tbl-holder {
        width: 100%;
    }

    .small-font, .small-font label, .small-font .input-group-addon, .table.small-font thead tr th, .table.small-font thead tr td {
        font-size: 11px;
    }

    #frm_2 .select2-container{
        width: 100% !important;
    }


    .small-font .input-sm {
        height: auto;
        padding: 3px 5px;
        font-size: 12px;
    }

    span.validation-advice {
        display: none !important;
    }

    .t-b-n {
        border-top: 1px solid #fff !important;
        border-right: 1px solid #fff !important;
        border-bottom: 1px solid #fff !important;
    }

    .btnCircle {
        color: #333333;
        background-color: #E5E5E5;
        height: 24px;
        width: 24px;
        text-align: center;
        padding: 3px 9px;
        border-radius: 25px !important;
        font-weight: bold;
        display: inline-block;
        line-height: 18px;
    }

    .addBtnRemove {
        margin-top: 2px;
        color: #fff;
        background-color: #f3565d;
    }

    .seatSelectionWrap .block-plan-wrapper {
        width: 100%;
        overflow: auto;
    }

    .seatSelectionWrap .block-plan-container {
        position: relative;
        padding: 20px;
        border: 1px solid #d5d5d5;
    }

    .seatSelectionWrap #item-container {
        position: relative;
        margin: auto;
    }

    .seatSelectionWrap .block-plan-wrapper .selection {
        position: absolute;
        background: #BCE;
        background-color: #BEC;
        border-color: #8B9;
    }

    .legendsWrap .legends {
        margin: 0;
        padding: 0 0 10px 0;
    }

    .legendsWrap .legends li .icon {
        width: 10px;
        height: 10px;
        display: inline-block;
        vertical-align: middle;
        margin-right: 10px;
    }

    .seatSelectionWrap .seat_item {
        position: absolute;
        height: 24px;
        width: 24px;
        top: 0px;
        font-size: 8px;
        color: #fff;
        text-align: center;
        vertical-align: middle;
        line-height: 24px;
        border-radius: 5px !important;
    }

    .seatSelectionWrap .seat_item {
        background: #00abbd;
        cursor: pointer;
        /*
        background-image: url("../../images/seat-icon.png") !important;
        background-position: 50% 50% !important;
        background-size: cover !important;
        */
    }

    .legendsWrap .legends li {
        margin-right: 20px;
        display: inline-block;
    }

    .legendsWrap .legends li.selected .icon,
    .seatSelectionWrap .selected_seat {
        background: #84c94c;
    }

    .legendsWrap .legends li .text {
        display: inline-block;
        vertical-align: middle;
        font-size: 11px;
    }

    .legendsWrap .legends li.available .icon {
        background: #00abbd;
    }

    .legendsWrap .legends li.reserved .icon,
    .seatSelectionWrap .booked_seat {
        background: #ee0c6e;
    }

    .legendsWrap .legends li.unavailable .icon,
    .seatSelectionWrap .hide_seat,
    .seatSelectionWrap .hold_seat {
        background: #eeeded;
        color: #000;
    }

    .seatSelectionWrap .booked_seat,
    .seatSelectionWrap .hold_seat {
        cursor: default;
    }


    /*#selected_seats b{font-size: 26px;}*/

</style>

<div class="page-container">
    <?php $this->load->view('counter/inc_sidebar_navigation.php'); ?>

    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper" ng-controller="mainController">
        <div class="page-content">

            <div class="row">
                <div class="col-md-12">

                    <!-- BEGIN VALIDATION STATES-->
                    <div class="portlet light bordered">
                        <div class="portlet-body form">
                            <div class="form-body">
                                <div class="row">
                                    <form id="frm_1" class="form-horizontal">

                                        <filter-option>

                                        </filter-option>




                                    </form>
                                </div>
                            </div>
                        </div>
                        <!-- END VALIDATION STATES-->
                    </div>
                </div>
            </div>


        </div>
        <!-- END PAGE CONTENT-->

        <div class="modal fade" tabindex="-1" role="dialog" aria-labelledby="summeryModalLabel" data-backdrop="static" data-keyboard="false" id="summery_699">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-hidden="true"></button>
                        <h4 class="modal-title" id="myModalLabel">Transaction Detail</h4>
                    </div>
                    <div class="modal-body">
                        <table class="table">
                            <tbody>
                                <tr>
                                    <th class="t-b-no">Mobile Number</th>
                                    <td class="t-b-no">{{user.mobile}}</td>
                                </tr>
                                <tr>
                                    <th>Name</th>
                                    <td>{{user.fname}} {{ user.lname}}</td>
                                </tr>

                                <tr>
                                    <th>Theater</th>
                                    <td>{{theater.name}}</td>
                                </tr>
                                <tr>
                                    <th>Movie</th>
                                    <td>{{movie.name}}</td>
                                </tr>
                                <tr>
                                    <th>Time slot</th>
                                    <td ng-if="timeslot !=='select time slot'">{{timeslot | amUtc| amDateFormat:'hh:mm A'}}</td>
                                </tr>
                                <tr>
                                    <th>Movie date</th>
                                    <td ng-if="timeslot !=='select time slot'">{{timeslot | amUtc| amDateFormat:'YYYY, MMM DD'}}</td>
                                </tr>
                                <tr>
                                    <th>Selected Seats</th>
                                    <td>{{ totalseats.join(',') }}</td>
                                </tr>
                                <tr>
                                    <td colspan="2" align="center">----------------------------------------------------------------------------------------------------------------------------------</td>
                                </tr>
                            </tbody>
                            <tbody ng-repeat="pr in pricecategories">
                                    <tr>
                                        <th>Ticket Type</th>
                                        <td>{{pr.category_name}}({{pr.price}})</td>
                                    </tr>
                                    <tr>
                                        <th>Unit Price (Rs)</th>
                                        <td>{{pr.price}}</td>
                                    </tr>
                                    <tr>
                                        <th>QtY</th>
                                        <td>{{pr.qty}}</td>
                                    </tr>
                                    <tr>
                                        <th>Amount (Rs)</th>
                                        <td>{{pr.price * pr.qty}}</td>
                                    </tr>


                            </tbody>
                            <tbody>
                                <tr>
                                    <td colspan="2" align="center">----------------------------------------------------------------------------------------------------------------------------------</td>
                                </tr>
                                <tr>
                                    <th>Total Amount (Rs)</th>
                                    <td>{{getTotal()}}</td>
                                </tr>
                                <tr>
                                    <th>Payment Method</th>
                                    <td>{{paymentmethod.name}}</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <div class="modal-footer">
                        <button id="btn-cancel" type="button" class="btn btn-danger" ng-click="cancelTr()">Cancel Transaction</button>
                        <button id="btn-proceed" type="button" class="btn btn-primary" ng-click="proceedtopayment()">Proceed With Transaction</button>
                    </div>
                </div>
            </div>
        </div>



    <div class="modal fade bs-modal-sm" id="modal_booking_confirmation" tabindex="-1" role="dialog" aria-hidden="true"
         data-backdrop="static" data-keyboard="false" style="display: none;">
        <div class="modal-dialog modal-sm">
            <div class="modal-content">
                <div class="modal-header">
                    <h4 class="modal-title">Booking Confirmation</h4>
                </div>
                <div class="modal-body">
                    <p>Booking has been successfully saved <br/>{{bookingsuccess}}</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn default" data-dismiss="modal">Close</button>
                    <button id="btn-print" type="button" class="btn blue" ng-click="print()">Print</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- END CONTENT -->
    </div>
</div>
<!-- END CONTAINER -->




<div class="modal fade bs-modal-sm" id="modal_error" tabindex="-1" role="dialog" aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Validation</h4>
            </div>
            <div class="modal-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn default" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>

