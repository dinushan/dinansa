<?php include(APPPATH.'views/frontend/inc/head.php'); ?>
<?php include(APPPATH.'views/frontend/inc/header.php'); ?>

<div class="container pageContainer">
   <div class="row">
       <div id="summary-print">
      <div class="col-xs-12 col-sm-8 col-sm-offset-2 col-md-6 col-md-offset-3 messageBoxWrap">
         <h2 class="title">Summary of Booking</h2>

         <span class="rightIcon"></span>
         <div class="confirmedMessage">
            <span class="name"><?php echo $this->userauth->first_name.' '.$this->userauth->last_name ?></span> Your booking has been completed
         </div>
          <div class="referenceNumber">Your reference number is <span class="number">#<?php echo $booking->reference_no;?></span></div>
          <div class="referenceNumber">Your transaction id is <span class="number">#<?php echo $booking->transaction_id;?></span></div>
      </div>
      <div class="col-xs-12 col-sm-6 col-sm-6 col-md-4 col-md-offset-2 movieSummery">
         <table class="movieSummeryTable">
               <tbody>
                  <tr>
                     <td><span class="title">Movie Name</span></td>
                     <td><?php echo $movie->movie_name;?></td>
                  </tr>
                  <tr>
                     <td><span class="title">Theatre Name</span></td>
                     <td><?php echo $theater->name;?></td>
                  </tr>
                  <tr>
                     <td><span class="title">Show Date</span></td>
                     <td><?php echo date('Y-m-d',strtotime($booking->time_slot));?></td>
                  </tr>
                  <tr>
                     <td><span class="title">Show Time</span></td>
                     <td><?php echo date('h:i A',strtotime($booking->time_slot));?></td>
                  </tr>
               </tbody>
            </table>
      </div>
      <div id="bookingSummery" class="col-xs-12 col-sm-6 col-sm-6 col-md-4 bookingSummery">
      <h4 class="title">Summary</h4>
         <table class="bookingSummeryTable">
            <thead>
               <tr>
                  <!--<th>Block</th>-->
                  <th>Seat</th>
<!--                  <th>Price (Rs)</th>
-->                  <th>Ref No</th>
               </tr>
            </thead>
            <tbody>
            <?php
            foreach($booking_seat as $bok_seat){ ?>
               <tr>
                  <td><?php echo $bok_seat['label'] ;?></td>
                  <td><?php echo $bok_seat['serial_number'] ;?></td>
               </tr>
                <?php }?>

            <tr>
                <td colspan="2"><hr/></td>
            </tr>
            <?php foreach($booking_price as $booking_pr)
            {
                $ticketPrice = $booking_pr['price']+$booking_pr['online_charge'];
                ?>
                <tr>
                    <td><?php echo $price_array[$booking_pr['price_category']];?> (qty:<?php echo $booking_pr['qty'] ;?>)</td>
                    <td><?php echo number_format($ticketPrice*$booking_pr['qty'],2) ;?></td>
                </tr>
            <?php }?>

            <?php if($booking->total_discount_amount > 0){ ?>
            <tr>
                <td>Discount amount</td>
                <td><?php echo number_format($booking->total_discount_amount,2);?></td>

            </tr>
            <?php }?>

                <tr>
                    <td><strong>Total</strong></td>
                    <td><strong><?php echo number_format($booking->total_amount,2) ;?></strong></td>

                </tr>
            </tbody>
         </table>
      </div>
   </div>
      <div class="col-xs-12 btnWrapper">
         <div id="btn-print-summary" class="btn btn-scope btn-blue">Print Booking</div>
      </div>
   </div>
</div>

<?php include(APPPATH.'views/frontend/inc/footer.php'); ?>